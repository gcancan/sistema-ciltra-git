SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		GRCC
-- Create date: 15/12/2016
-- Description:	
-- =============================================
CREATE PROCEDURE sp_getAllClasificaciones 
	-- Add the parameters for the stored procedure here
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT
AS
    BEGIN
        SET NOCOUNT ON;
        BEGIN TRY
            SELECT  idClasificacion ,
                    clasificacion
            FROM    dbo.clasificacion;
            IF @@ROWCOUNT > 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'SE ENCONTRARON CLASIFICACIONES';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = 'NO SE ENCONTRARON CLASIFICACIONES';
                END;
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
GO
