
GO
/****** Object:  StoredProcedure [dbo].[sp_getCartaPorteByIdCamion]    Script Date: 10/20/2016 12:51:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date, 06/06/2016,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_getCartaPorteByIdCamion]
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @idCamion NVARCHAR(20)
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRY
            
            SELECT TOP 1
                    NumGuiaId ,
                    SerieGuia ,
                    NumViaje ,
                    idCliente ,
                    IdEmpresa ,
                    FechaHoraViaje ,
                    idOperador ,
                    idTractor ,
                    idRemolque1 ,
                    ISNULL(idDolly, '') AS idDolly ,
                    ISNULL(idRemolque2, '') AS idRemolque2 ,
                    FechaHoraFin ,
                    EstatusGuia ,
                    UserViaje ,
                    tipoViaje ,
                    tara,
                    km
            FROM    dbo.CabGuia
            WHERE   idTractor = @idCamion
                    AND EstatusGuia = 'INICIADO'
            
            IF @@ROWCOUNT <> 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'Se encontro registros';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = 'No se encontraron registros';
                END; 
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
