
GO
/****** Object:  StoredProcedure [dbo].[sp_getCartaPorteByFecha]    Script Date: 12/29/2016 14:11:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date,29/12/2016,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[sp_getCartaPortesFinalizados]
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @fechaInicio DATETIME ,
    @fechaFin DATETIME ,
    @idCliente INT = 0
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRY
            SELECT  NumGuiaId ,
                    SerieGuia ,
                    NumViaje ,
                    --idCliente ,
                    IdEmpresa ,
                    FechaHoraViaje ,
                    idOperador ,
                    idTractor ,
                    idRemolque1 ,
                    ISNULL(idDolly, '') AS idDolly ,
                    ISNULL(idRemolque2, '') AS idRemolque2 ,
                    FechaHoraFin ,
                    EstatusGuia ,
                    tara ,
                    TipoViaje ,
                    c.* ,
                    per.* ,
                    km
            FROM    dbo.CabGuia
                    JOIN dbo.cliente_vw AS c ON c.cliente_idCliente = idCliente
                    JOIN dbo.personal_vw AS per ON dbo.CabGuia.idOperador = per.persona_idPersonal
            WHERE   FechaHoraViaje BETWEEN @fechaInicio AND @fechaFin
                    AND EstatusGuia = 'FINALIZADO'
                    AND idCliente LIKE CASE @idCliente
                                         WHEN 0 THEN '%'
                                         ELSE CAST(@idCliente AS NVARCHAR(10))
                                       END
            ORDER BY NumGuiaId DESC;
            
            IF @@ROWCOUNT <> 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'Se encontro registros';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = 'No se encontraron registros';
                END; 
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
