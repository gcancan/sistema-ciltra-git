CREATE TABLE clasificacion
    (
      idClasificacion INT PRIMARY KEY
                          IDENTITY(1, 1) ,
      clasificacion NVARCHAR(100) NOT NULL
                                  DEFAULT ''
    );
INSERT  INTO dbo.clasificacion
        ( clasificacion
        )
        SELECT DISTINCT
                clasificacion
        FROM    dbo.origen;