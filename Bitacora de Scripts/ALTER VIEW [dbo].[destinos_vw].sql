
GO

/****** Object:  View [dbo].[destinos_vw]    Script Date: 11/16/2016 12:30:07 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO
ALTER VIEW [dbo].[destinos_vw]
AS
    SELECT DISTINCT
            p.Clave AS destino_clave ,
            p.Clave_empresa AS destino_empresa_clave ,
            p.Descripcion AS destino_descripcion ,
            p.Activo AS destino_activo ,
            c.Precio_sencillo AS destino_costoSencillo ,
            c.Precio_sencillo35 AS destino_costo_sencillo35 ,
            c.Precio_full AS destino_costoFull ,
            c.km AS destino_km ,
            ISNULL(( SELECT clave
                     FROM   dbo.zonas
                     WHERE  ( idCliente = c.Clave_cliente )
                            AND ( p.km BETWEEN kmMenor AND kmMayor )
                   ), '') AS destino_claveZona ,
            c.Clave_cliente AS destino_cliente_clave ,
            p.claveExtra AS destino_claveExtra ,
            p.correo AS destino_correo ,
            estado.*,
            cli.*
    FROM    dbo.trafico_plazas_cat AS p
            INNER JOIN dbo.trafico_clientes_plazas AS c ON p.Clave = c.Clave_plaza
            JOIN dbo.estado_vw AS estado ON p.idEstado = estado.estado_idEstado
            JOIN dbo.cliente_vw AS cli ON cli.cliente_idCliente = c.Clave_cliente
    WHERE   ( p.Clave <> 0 );





GO


