USE [DBFleteraPrueba];
GO
/****** Object:  StoredProcedure [dbo].[sp_getSalidasEntradasByIdOrdenServ]    Script Date: 10/10/2016 17:23:39 ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		<Author,GRCC,Name>
-- Create date: <Create Date,2016/09/22,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_getSalidasEntradasByIdOrdenServ]
    @valor INT OUT ,
    @mensaje NVARCHAR(MAX) OUT ,
    @idOrden INT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRY	
        
        --DECLARE @idPadreOrdSer INT = 0;
        --SET @idPadreOrdSer = ( SELECT   idPadreOrdSer
        --                       FROM     dbo.CabOrdenServicio
        --                       WHERE    idOrdenSer = @idOrden
        --                     );
                             
        --IF @idPadreOrdSer = 0
        --    BEGIN
        --        SET @valor = 3;
        --        SET @mensaje = 'No se encontro registros';
        --        RETURN -1;
        --    END;
        
            SELECT TOP 1
                    c.idCargaCombustible ,
                    c.idSalidaEntrada ,
                    c.idOrdenServ ,
                    c.fechaCombustible ,
                    c.kmFinal ,
                    c.kmRecorridos ,
                    c.kmDespachador ,
                    c.ltCombustible ,
                    c.ltCombustibleUtilizado ,
                    c.ltCombustiblePTO ,
                    c.ltCombustibleEST ,
                    c.tiempoTotal ,
                    c.tiempoPTO ,
                    c.tiempoEST ,
                    c.sello1 ,
                    c.sello2 ,
                    c.selloRes ,
                    c.idDespachador ,
                    p.* ,
                    idPadreOrdSer = ( SELECT TOP 1
                                                idPadreOrdSer
                                      FROM      dbo.salidasEntradas
                                      WHERE     idSalidaEntrada = c.idSalidaEntrada
                                    ) ,
                    fechaEntrada = ( SELECT TOP 1
                                            fecha
                                     FROM   dbo.salidasEntradas
                                     WHERE  idSalidaEntrada = c.idSalidaEntrada
                                   )
            FROM    dbo.cargaCombustible AS c
                    LEFT JOIN dbo.personal_vw AS p ON p.persona_idPersonal = c.idDespachador
            WHERE   idOrdenServ = @idOrden;
            
            IF @@ROWCOUNT > 0
                BEGIN
                    SET @valor = 0;
                    SET @mensaje = 'Se encontraron registros';
                END;
            ELSE
                BEGIN
                    SET @valor = 3;
                    SET @mensaje = 'No se encontro entrada registrada para ese folio';
                END;
        END TRY
        BEGIN CATCH
            SET @valor = 1;
            SET @mensaje = ERROR_MESSAGE();
        END CATCH;
    END;
