﻿using Core.Models;
using Fletera2Entidades;
using Fletera2Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class CatLlantasSvc
    {
        public OperationResult getCatLlantasxFilter(int idSisLlanta = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new CatLlantasServicios().getCatLlantasxFilter(idSisLlanta,filtroSinWhere,OrderBy);
        }

        public OperationResult GuardaCatLlantas(ref CatLlantas cat)
        {
            return new CatLlantasServicios().GuardaCatLlantas(ref cat);
        }
    }
}
