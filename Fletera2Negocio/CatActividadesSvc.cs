﻿using Core.Models;
using Fletera2Entidades;
using Fletera2Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class CatActividadesSvc
    {
        public OperationResult getCatActividadesxFilter(int idActividad = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new CatActividadesServicios().getCatActividadesxFilter(idActividad,filtroSinWhere,OrderBy);
        }

        public OperationResult GuardaCatActividades(ref CatActividades cat)
        {
            return new CatActividadesServicios().GuardaCatActividades(ref cat);
        }
    }
}
