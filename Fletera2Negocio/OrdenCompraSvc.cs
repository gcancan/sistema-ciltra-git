﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class OrdenCompraSvc
    {
        public OperationResult getCabPreOCxFilter(int IdPreOC = 0, string filtroSinWhere = "", string Orderby = "")
        {
            return new OrdenCompraServicios().getCabPreOCxFilter(IdPreOC, filtroSinWhere, Orderby);
        }
        public OperationResult getDetPreOCxFilter(int IdPreOC = 0, string filtroSinWhere = "")
        {
            return new OrdenCompraServicios().getDetPreOCxFilter(IdPreOC, filtroSinWhere);
        }
        public OperationResult GuardaCabPreOC(ref CabPreOC folio)
        {
            return new OrdenCompraServicios().GuardaCabPreOC(ref folio);
        }
        public OperationResult GuardaDetPreOC(ref DetPreOC folio)
        {
            return new OrdenCompraServicios().GuardaDetPreOC(ref folio);
        }
    }//
}
