﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class MasterOrdServSvc_f2
    {
        public OperationResult getMasterOrdServxFilter(int idPadreOrdSer = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new MasterOrdServServicios_f2().getMasterOrdServxFilter(idPadreOrdSer, filtroSinWhere, OrderBy);
        }
        public OperationResult GuardaMasterOrdServ(ref MasterOrdServ_f2 cat)
        {
            return new MasterOrdServServicios_f2().GuardaMasterOrdServ(ref cat);
        }
    }
}
