﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class DetLiquidacionesSvc
    {

        public OperationResult getDetLiquidacionesxFilter(int idDetLiq = 0, string filtroSinWhere = "")
        {
            return new DetLiquidacionesServicios().getDetLiquidacionesxFilter(idDetLiq, filtroSinWhere);
        }
        public OperationResult GuardaCabLiquidaciones(ref DetLiquidaciones folio)
        {
            return new DetLiquidacionesServicios().GuardaCabLiquidaciones(ref folio);
        }
    }//
}
