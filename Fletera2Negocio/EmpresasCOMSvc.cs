﻿using Core.Models;
using Fletera2Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class EmpresasCOMSvc
    {
        public OperationResult getEmpresasxId(int CIDEMPRESA = 0)
        {
            return new EmpresasCOMServicios().getEmpresasxId(CIDEMPRESA);
        }
    }
}
