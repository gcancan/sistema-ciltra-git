﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class CabGuiaSvc
    {
        public OperationResult getCabGuiaxFilter(int NumGuiaId = 0, string filtroSinWhere = "", string OrderBy = "", string PreEspSelect = "")
        {
            return new CabGuiaServicios().getCabGuiaxFilter(NumGuiaId, filtroSinWhere,OrderBy,PreEspSelect);
        }

        public OperationResult getLiquidacionxFilter(string filtroSinWhere = "")
        {
            return new CabGuiaServicios().getLiquidacionxFilter(filtroSinWhere);
        }

        public OperationResult getViajePorOperadorxFilter(string filtroSinWhere, string sOrderBy)
        {
            return new CabGuiaServicios().getViajePorOperadorxFilter(filtroSinWhere, sOrderBy);
        }

        public OperationResult ActualizaCabGuia(ref CabGuia folio)
        {
            return new CabGuiaServicios().ActualizaCabGuia(ref folio);
        }
    }//
}
