﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class DetRecepcionOSSvc
    {
        public OperationResult getDetOrdenServicioxFilter(int idOrdenSer = 0, string filtroSinWhere = "")
        {
            return new DetRecepcionOSServicios().getDetOrdenServicioxFilter(idOrdenSer, filtroSinWhere);
        }

        public OperationResult GuardaDetRecepcionOS(ref DetRecepcionOS cat)
        {
            return new DetRecepcionOSServicios().GuardaDetRecepcionOS(ref cat);
        }
    }
}
