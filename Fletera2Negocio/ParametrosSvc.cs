﻿using Core.Models;
using Fletera2Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class ParametrosSvc
    {
        public OperationResult getParametrosxFiltro(int IdParametro = 0, string filtroSinWhere = "")
        {
            return new ParametrosServicios().getParametrosxFiltro(IdParametro, filtroSinWhere);
        }
    }//
}
