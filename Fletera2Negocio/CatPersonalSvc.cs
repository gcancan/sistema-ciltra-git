﻿using Core.Models;
using Fletera2Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class CatPersonalSvc
    {
        public OperationResult getCatPersonalxFiltro(string filtroConWhere = "", string Ordena = "")
        {
            return new CatPersonalServicios().getCatPersonalxFiltro(filtroConWhere, Ordena);
        }
    }//
}
