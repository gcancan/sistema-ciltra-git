﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class DetOrdenActividadProductosSvc
    {
        public OperationResult getDetOrdenActividadProductosxFilter(int idDetOrdenActividadProductos = 0, string filtroSinWhere = "")
        {
            return new DetOrdenActividadProductosServicios().getDetOrdenActividadProductosxFilter(idDetOrdenActividadProductos, filtroSinWhere);
        }

        public OperationResult GuardaDetOrdenActividadProductos(ref DetOrdenActividadProductos folio)
        {
            return new DetOrdenActividadProductosServicios().GuardaDetOrdenActividadProductos(ref folio);
        }

        public OperationResult getDOSInsumosFACxFilter(int idOrdenSer = 0, string filtroSinWhere = "")
        {
            return new DetOrdenActividadProductosServicios().getDOSInsumosFACxFilter(idOrdenSer, filtroSinWhere);
        }
    }
}
