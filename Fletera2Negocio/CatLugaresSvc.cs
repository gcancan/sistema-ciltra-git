﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;

namespace Fletera2Negocio
{
    public class CatLugaresSvc
    {
        public OperationResult getCatLugaresxFilter(int idLugar = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new CatLugaresServicios().getCatLugaresxFilter(idLugar, filtroSinWhere, OrderBy);
        }
        public OperationResult GuardaCatLugares(ref CatLugares cat)
        {
            return new CatLugaresServicios().GuardaCatLugares(ref cat);
        }
    }//
}
