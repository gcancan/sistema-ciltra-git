﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;

namespace Fletera2Negocio
{
    public class CatAlmacenSvc
    {
        public OperationResult getCatAlmacenxFilter(int idAlmacen = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new CatAlmacenServicios().getCatAlmacenxFilter(idAlmacen, filtroSinWhere, OrderBy);
        }
        public OperationResult GuardaCatAlmacen(ref CatAlmacen cat)
        {
            return new CatAlmacenServicios().GuardaCatAlmacen(ref cat);
        }
    }
}
