﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class CompGastosFoliosDinSvc
    {
        public OperationResult getCompGastosFoliosDinxId(int Folio = 0, string filtroSinWhere = "",string sOrderby = "")
        {
            return new CompGastosFoliosDinServicios().getCompGastosFoliosDinxId(Folio, filtroSinWhere,sOrderby);
        }

        public OperationResult GuardaCompGastosFoliosDin(ref CompGastosFoliosDin folio)
        {
            return new CompGastosFoliosDinServicios().GuardaCompGastosFoliosDin(ref folio);
        }
    }//
}
