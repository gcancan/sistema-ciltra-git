﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class CatConceptosLiqSvc
    {
        public OperationResult getCatConceptosLiqxId(int idConcepto = 0, string filtroSinWhere = "")
        {
            return new CatConceptosLiqServicios().getCatConceptosLiqxId(idConcepto, filtroSinWhere);
        }

        public OperationResult GuardaCatConceptosLiq(ref CatConceptosLiq cat)
        {
            return new CatConceptosLiqServicios().GuardaCatConceptosLiq(ref cat);
        }

    }//
}
