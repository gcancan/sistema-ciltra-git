﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System.Collections.Generic;

namespace Fletera2Negocio
{
    public class MovimientosSvc
    {
        public OperationResult getCabMovimientosxFilter(int NoMovimiento = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new MovimientosServicios().getCabMovimientosxFilter(NoMovimiento,filtroSinWhere,OrderBy);
        }
        public OperationResult getDetMovimientosxFilter(int NoMovimiento = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new MovimientosServicios().getDetMovimientosxFilter(NoMovimiento, filtroSinWhere, OrderBy);
        }
        public OperationResult GuardaMovimientos(ref CabMovimientos cabmov, ref List<DetMovimientos> listdetmov)
        {
            return new MovimientosServicios().GuardaMovimientos(ref cabmov, ref listdetmov);
        }
    }
}
