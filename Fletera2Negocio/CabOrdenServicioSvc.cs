﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class CabOrdenServicioSvc
    {
        public OperationResult getCabOrdenServicioxFilter(int idOrdenSer = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new CabOrdenServicioServicios().getCabOrdenServicioxFilter(idOrdenSer, filtroSinWhere, OrderBy);
        }

        public OperationResult GuardaCabOrdenServicio(ref CabOrdenServicio cat)
        {
            return new CabOrdenServicioServicios().GuardaCabOrdenServicio(ref cat);
        }

        public OperationResult GuardaOrdenServicio(ref MasterOrdServ_f2 master)
        {
            return new CabOrdenServicioServicios().GuardaOrdenServicio(ref master);
        }
    }
}
