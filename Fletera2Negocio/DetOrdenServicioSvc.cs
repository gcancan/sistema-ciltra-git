﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class DetOrdenServicioSvc
    {
        public OperationResult getDetOrdenServicioxFilter(int idOrdenSer = 0, string filtroSinWhere = "")
        {
            return new DetOrdenServicioServicios().getDetOrdenServicioxFilter(idOrdenSer, filtroSinWhere);
        }

        public OperationResult getDetalleOSVistaxFilter(int idOrdenSer = 0, string filtroSinWhere = "")
        {
            return new DetOrdenServicioServicios().getDetalleOSVistaxFilter(idOrdenSer, filtroSinWhere);
        }

        public OperationResult getDOSInsumosVistaxFilter(int idOrdenSer = 0, string filtroSinWhere = "")
        {
            return new DetOrdenServicioServicios().getDOSInsumosVistaxFilter(idOrdenSer, filtroSinWhere);
        }

        public OperationResult GuardaDetOrdenServicio(ref DetOrdenServicio cat)
        {
            return new DetOrdenServicioServicios().GuardaDetOrdenServicio(ref cat);
        }
    }
}
