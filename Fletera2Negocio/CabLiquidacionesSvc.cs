﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class CabLiquidacionesSvc
    {
        public OperationResult getCabGuiaxFilter(int idLiquidacion = 0, string filtroSinWhere = "")
        {
            return new CabLiquidacionesServicios().getCabLiquidacionesxFilter(idLiquidacion, filtroSinWhere);
        }

        public OperationResult GuardaCabLiquidaciones(ref CabLiquidaciones folio)
        {
            return new CabLiquidacionesServicios().GuardaCabLiquidaciones(ref folio);
        }
    }
}
