﻿using Core.Models;
using Fletera2Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class CatEmpresasSvc
    {
        public OperationResult getCatEmpresasxFiltro(int idEmpresa = 0, string filtroSinWhere = "")
        {
            return new CatEmpresasServicios().getCatEmpresasxFiltro(idEmpresa, filtroSinWhere);
        }
    }
}
