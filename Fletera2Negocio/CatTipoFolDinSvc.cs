﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class CatTipoFolDinSvc
    {
        public OperationResult getCatTipoFolDinxId(int idTipoFolDin = 0)
        {
            return new CatTipoFolDinServices().getCatTipoFolDinxId(idTipoFolDin);
        }
        public OperationResult GuardaCatTipoFolDin(ref CatTipoFolDin cat)
        {
            return new CatTipoFolDinServices().GuardaCatTipoFolDin(ref cat);
        }
    }//
}
