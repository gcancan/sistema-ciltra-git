﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;

namespace Fletera2Negocio
{
    public class CatSATSvc
    {
        public OperationResult getCatSATxFilter(int idClave = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new CatSATServicios().getCatSATxFilter(idClave,filtroSinWhere,OrderBy);
        }

        public OperationResult GuardaCatSAT(ref CatSAT cat)
        {
            return new CatSATServicios().GuardaCatSAT(ref cat);
        }
    }
}
