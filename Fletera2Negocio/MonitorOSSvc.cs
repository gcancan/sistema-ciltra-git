﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;


namespace Fletera2Negocio
{
    class MonitorOSSvc
    {
        public OperationResult getMonitorOSxFilter(int idOrdenSer = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new MonitorOSServicios().getMonitorOSxFilter(idOrdenSer, filtroSinWhere,OrderBy);
        }
    }
}
