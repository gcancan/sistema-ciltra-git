﻿using Fletera2Datos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class UtilSvc
    {
        public string EjecutaSentenciaSql(string ConStr, ref string[] arraySql, int Indice = 0, int PosicionIdentity = -1)
        {
            return new UtilServicios().EjecutaSentenciaSql(ConStr, ref arraySql, Indice, PosicionIdentity);
        }

        public int Consecutivo(string ConStr, string Campo, string TablaAfecta, string TablaConsulta, string FiltroAfectaConWhere = "", string FiltroConsultaConWhere = "")
        {
            return new UtilServicios().Consecutivo(ConStr, Campo, TablaAfecta, TablaConsulta, FiltroAfectaConWhere, FiltroConsultaConWhere);
        }

        public DataTable LlenaTabla(string strSQL, string ConStr = "")
        {
            return new UtilServicios().LlenaTabla(strSQL, ConStr);
        }

        public string EjecutaSentenciaSqlCOM(string ConStr, ref string[] arraySql, int Indice = 0, int PosicionIdentity = -1)
        {
            return new UtilServicios().EjecutaSentenciaSqlCOM(ConStr, ref arraySql, Indice, PosicionIdentity);
        }

        //EjecutaSentenciaSqlCOM
    }
}
