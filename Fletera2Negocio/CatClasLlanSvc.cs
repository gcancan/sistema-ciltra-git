﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;

namespace Fletera2Negocio
{
    public class CatClasLlanSvc
    {
        public OperationResult getCatClasLlanxFilter(int idClasLlan = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new CatClasLlanServicios().getCatClasLlanxFilter(idClasLlan, filtroSinWhere, OrderBy);
        }

        public OperationResult GuardaCatClasLlan(ref CatClasLlan cat)
        {
            return new CatClasLlanServicios().GuardaCatClasLlan(ref cat);
        }
    }
}
