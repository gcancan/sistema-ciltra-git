﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class CatUnidadTransSvc
    {
        public OperationResult geCatUnidadTransxFilter(string idUnidadTrans = "", string filtroSinWhere = "", string OrderBy = "")
        {
            return new CatUnidadTransServicios().geCatUnidadTransxFilter(idUnidadTrans, filtroSinWhere, OrderBy);
        }

        public OperationResult GuardaCatUnidadTrans(ref CatUnidadTrans cat)
        {
            return new CatUnidadTransSvc().GuardaCatUnidadTrans(ref cat);
        }
    }
}
