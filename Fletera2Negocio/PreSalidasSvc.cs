﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class PreSalidasSvc
    {
        public OperationResult getCabPreSalidaxFilter(int IdPreSalida = 0, string filtroSinWhere = "", string Orderby = "")
        {
            return new PreSalidasServicios().getCabPreSalidaxFilter(IdPreSalida,filtroSinWhere,Orderby);
        }
        public OperationResult getDetPreSalidaxFilter(int IdPreSalida = 0, string filtroSinWhere = "")
        {
            return new PreSalidasServicios().getDetPreSalidaxFilter(IdPreSalida, filtroSinWhere);
        }
        public OperationResult GuardaCabPreSalida(ref CabPreSalida folio)
        {
            return new PreSalidasServicios().GuardaCabPreSalida(ref folio);
        }
        public OperationResult GuardDetPreSalida(ref DetPreSalida folio)
        {
            return new PreSalidasServicios().GuardDetPreSalida(ref folio);
        }
        public OperationResult getApartadoxProd(int CIDPRODUCTO, string filtroSinWhere = "", string Orderby = "")
        {
            return new PreSalidasServicios().getApartadoxProd(CIDPRODUCTO, filtroSinWhere, Orderby);
        }
        public OperationResult getDetSalidasInsumoxFilter(int IdPreSalida = 0, string filtroSinWhere = "", string Orderby = "")
        {
            return new PreSalidasServicios().getDetSalidasInsumoxFilter(IdPreSalida, filtroSinWhere, Orderby);
        }

        public OperationResult GuardaPresalidas(ref CabPreSalida cab, ref List<DetPreSalida> listdet)
        {
            return new PreSalidasServicios().GuardaPresalidas(ref cab, ref listdet);
        }
    }//
    
}
