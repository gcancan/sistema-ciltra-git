﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;

namespace Fletera2Negocio
{
    public class MovLLantasSvc
    {
        public OperationResult getMovLLantasxFilter(int idMovLlanta = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new MovLLantasServicios().getMovLLantasxFilter(idMovLlanta,filtroSinWhere,OrderBy);
        }
        public OperationResult GuardaMovLLantas(ref MovLLantas cat)
        {
            return new MovLLantasServicios().GuardaMovLLantas(ref cat);
        }
    }
}
