﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;


namespace Fletera2Negocio
{
    public class CatLLantasProductoSvc
    {
        public OperationResult getCatLLantasProductoxFilter(int idProductoLlanta = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new CatLLantasProductoServicios().getCatLLantasProductoxFilter(idProductoLlanta,filtroSinWhere,OrderBy);
        }
        public OperationResult GuardaCatClasLlan(ref CatLLantasProducto cat)
        {
            return new CatLLantasProductoServicios().GuardaCatClasLlan(ref cat);
        }
    }
}
