﻿using Core.Models;
using Fletera2Entidades;
using Fletera2Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class CatDivisionSvc
    {
        public OperationResult getCatDivisionxFilter(int idDivision = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new CatDivisionServicios().getCatDivisionxFilter(idDivision, filtroSinWhere, OrderBy);
        }

        public OperationResult GuardaCatDivision(ref CatDivision cat)
        {
            return new CatDivisionServicios().GuardaCatDivision(ref cat);
        }



    }
}
