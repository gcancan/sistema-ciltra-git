﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class CatTipoOrdServSvc
    {
        public OperationResult getCatTipoOrdServxFilter(int idTipOrdServ = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new CatTipoOrdServServicios().getCatTipoOrdServxFilter(idTipOrdServ, filtroSinWhere, OrderBy);
        }

        public OperationResult GuardaCatTipoOrdServ(ref CatTipoOrdServ cat)
        {
            return new CatTipoOrdServServicios().GuardaCatTipoOrdServ(ref cat);
        }
    }//
}
