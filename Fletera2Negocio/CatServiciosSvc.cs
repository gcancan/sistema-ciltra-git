﻿using Core.Models;
using Fletera2Entidades;
using Fletera2Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class CatServiciosSvc
    {
        public OperationResult getCatServiciosxFilter(int idTipoServicio = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new CatServiciosServicios().getCatServiciosxFilter(idTipoServicio,filtroSinWhere,OrderBy);
        }
        public OperationResult GuardaCatServicios(ref CatServicios cat)
        {
            return new CatServiciosServicios().GuardaCatServicios(ref cat);
        }
    }
}
