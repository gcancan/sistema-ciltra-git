﻿using Core.Models;
using Fletera2Entidades;
using Fletera2Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class RelTipOrdServSvc
    {
        public OperationResult getRelTipOrdServxFilter(int idTipOrdServ = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new RelTipOrdServServicios().getRelTipOrdServxFilter(idTipOrdServ,filtroSinWhere,OrderBy);
        }

        public OperationResult GuardaRelTipOrdServ(ref RelTipOrdServ cat)
        {
            return new RelTipOrdServServicios().GuardaRelTipOrdServ(ref cat);
        }
    }
}
