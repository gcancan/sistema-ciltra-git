﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fletera2Datos;
using Fletera2Entidades;

namespace Fletera2Negocio
{
    public class FoliosDineroSvc
    {
        public OperationResult getFoliosDineroxId(int FolioDin = 0, string filtroSinWhere = "")
        {
            return new FoliosDineroServices().getFoliosDineroxId(FolioDin, filtroSinWhere);
        }

        public OperationResult GuardaFoliosDinero(ref FoliosDinero folio)
        {
            return new FoliosDineroServices().GuardaFoliosDinero(ref folio);
        }
    }//
}
