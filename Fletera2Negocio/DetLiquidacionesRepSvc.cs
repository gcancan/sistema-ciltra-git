﻿using Core.Models;
using Fletera2Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class DetLiquidacionesRepSvc
    {
        public OperationResult getDetLiqViaRepxFilter(int idLiquidacion = 0, string filtroSinWhere = "")
        {
            return new DetLiquidacionesRepServicios().getDetLiqViaRepxFilter(idLiquidacion, filtroSinWhere);
        }
        public OperationResult getDetLiqFolDinxFilter(int idLiquidacion = 0, string filtroSinWhere = "")
        {
            return new DetLiquidacionesRepServicios().getDetLiqFolDinxFilter(idLiquidacion, filtroSinWhere);
        }
        public OperationResult getDetLiqComGasRepxFilter(int idLiquidacion = 0, string filtroSinWhere = "")
        {
            return new DetLiquidacionesRepServicios().getDetLiqComGasRepxFilter(idLiquidacion, filtroSinWhere);
        }
        public OperationResult getDetLiqOtrConRepxFilter(int idLiquidacion = 0, string filtroSinWhere = "")
        {
            return new DetLiquidacionesRepServicios().getDetLiqOtrConRepxFilter(idLiquidacion, filtroSinWhere);
        }
    }//
}
