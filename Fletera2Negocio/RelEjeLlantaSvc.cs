﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
namespace Fletera2Negocio
{
    public class RelEjeLlantaSvc
    {
        public OperationResult getRelEjeLlantaxFilter(int idClasLlan = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new RelEjeLlantaServicios().getRelEjeLlantaxFilter(idClasLlan, filtroSinWhere, OrderBy);
        }
        public OperationResult GuardaRelEjeLlanta(ref RelEjeLlanta cat)
        {
            return new RelEjeLlantaServicios().GuardaRelEjeLlanta(ref cat);
        }
    }
}
