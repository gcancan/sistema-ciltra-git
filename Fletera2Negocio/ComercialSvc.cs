﻿using Core.Models;
using Fletera2Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class ComercialSvc
    {
        //PRODUCTOS
        public OperationResult getadmProductosxFilter(int CIDPRODUCTO = 0, string filtroSinWhere = "")
        {
            return new COMERCIALServicios().getadmProductosxFilter(CIDPRODUCTO, filtroSinWhere);
        }
        public OperationResult getadmClientesxFilter(int CIDCLIENTEPROVEEDOR = 0, string filtroSinWhere = "")
        {
            return new COMERCIALServicios().getadmClientesxFilter(CIDCLIENTEPROVEEDOR, filtroSinWhere);
        }

        public OperationResult getadmConceptosxFilter(int CIDCONCEPTODOCUMENTO = 0, string filtroSinWhere = "")
        {
            return new COMERCIALServicios().getadmConceptosxFilter(CIDCONCEPTODOCUMENTO, filtroSinWhere);
        }

        public OperationResult getadmCostosHistoricosxFilter(int CIDPRODUCTO = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new COMERCIALServicios().getadmCostosHistoricosxFilter(CIDPRODUCTO, filtroSinWhere, OrderBy);
        }

        public OperationResult getadmExistenciaxProd(int CIDPRODUCTO, string Fecha, int Almacen = 0,
    string filtroSinWhere = "", string OrderBy = "")
        {
            return new COMERCIALServicios().getadmExistenciaxProd(CIDPRODUCTO, Fecha, Almacen, filtroSinWhere, OrderBy);
        }
        public OperationResult getadmDocumentosxFilter(int CIDDOCUMENTO = 0, string filtroSinWhere = "")
        {
            return new COMERCIALServicios().getadmDocumentosxFilter(CIDDOCUMENTO, filtroSinWhere);
        }
        public OperationResult getadmAlmacenesxFilter(int CIDALMACEN = 0, string filtroSinWhere = "")
        {
            return new COMERCIALServicios().getadmAlmacenesxFilter(CIDALMACEN, filtroSinWhere);
        }

    }//
}
