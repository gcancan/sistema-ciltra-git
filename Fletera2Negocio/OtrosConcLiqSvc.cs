﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class OtrosConcLiqSvc
    {
        public OperationResult getOtrosConcLiqxFilter(int NumFolio = 0, string filtroSinWhere = "")
        {
            return new OtrosConcLiqServicios().getOtrosConcLiqxFilter(NumFolio, filtroSinWhere);
        }

        public OperationResult GuardaOtrosConcLiq(ref OtrosConcLiq folio)
        {
            return new OtrosConcLiqServicios().GuardaOtrosConcLiq(ref folio);
        }
    }//
}
