﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class CatTipoLlantaSvc
    {
        public OperationResult getCatTipoLlantaxFilter(int idTipoLLanta = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new CatTipoLlantaServicios().getCatTipoLlantaxFilter(idTipoLLanta, filtroSinWhere, OrderBy);
        }
        public OperationResult GuardaCatTipoLlanta(ref CatTipoLlanta cat)
        {
            return new CatTipoLlantaServicios().GuardaCatTipoLlanta(ref cat);
        }
    }
}
