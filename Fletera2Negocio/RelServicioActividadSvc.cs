﻿using Core.Models;
using Fletera2Entidades;
using Fletera2Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class RelServicioActividadSvc
    {
        public OperationResult getRelServicioActividadxFilter(int idServicio = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new RelServicioActividadServicios().getRelServicioActividadxFilter(idServicio,filtroSinWhere,OrderBy);
        }
        public OperationResult GuardaRelServicioActividad(ref RelServicioActividad cat)
        {
            return new RelServicioActividadServicios().GuardaRelServicioActividad(ref cat);
        }
    }
}
