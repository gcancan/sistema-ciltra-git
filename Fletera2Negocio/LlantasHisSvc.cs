﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;

namespace Fletera2Negocio
{
    public class LlantasHisSvc
    {
        public OperationResult getLlantasHisxFilter(int idSis = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new LlantasHisServicios().getLlantasHisxFilter(idSis, filtroSinWhere, OrderBy);
        }
        public OperationResult GuardaLlantasHis(ref LlantasHis cat)
        {
            return new LlantasHisServicios().GuardaLlantasHis(ref cat);
        }
    }//
}
