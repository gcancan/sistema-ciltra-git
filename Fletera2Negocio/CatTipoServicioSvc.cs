﻿using Core.Models;
using Fletera2Entidades;
using Fletera2Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class CatTipoServicioSvc
    {
        public OperationResult getCatTipoServicioxFilter(int idTipoServicio = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new CatTipoServicioServicios().getCatTipoServicioxFilter(idTipoServicio,filtroSinWhere,OrderBy);
        }

        public OperationResult GuardaCatTipoServicio(ref CatTipoServicio cat)
        {
            return new CatTipoServicioServicios().GuardaCatTipoServicio(ref cat);
        }
    }
}
