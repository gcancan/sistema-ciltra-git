﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class CatTipoUniTransSvc
    {
        public OperationResult getCatTipoUniTransxFilter(int idTipoUniTras = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            return new CatTipoUniTransServicios().getCatTipoUniTransxFilter(idTipoUniTras, filtroSinWhere, OrderBy);

        }

        public OperationResult GuardaCatTipoUniTrans(ref CatTipoUniTrans cat)
        {
            return new CatTipoUniTransServicios().GuardaCatTipoUniTrans(ref cat);
        }
    }
}
