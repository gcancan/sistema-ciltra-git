﻿using Core.Models;
using Fletera2Datos;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Negocio
{
    public class CatGastosSvc
    {
        public OperationResult getCatGastosxFiltro(int idGasto = 0, string filtroSinWhere = "")
        {
            return new CatGastosServicios().getCatGastosxFiltro(idGasto, filtroSinWhere);
        }

        public OperationResult GuardaCatGastos(ref CatGastos catalogo)
        {
            return new CatGastosServicios().GuardaCatGastos(ref catalogo);
        }
    }//
}
