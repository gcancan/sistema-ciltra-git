﻿app.factory('AnswerOptionValuesService', ['$q', '$http', 'Constant', function ($q, $http, Constant) {
    var service = {
        GetAllAnswerOptionValues: GetAllAnswerOptionValues,

    };



    var GetAllAnswerOptionValuesRoute = Constant.baseurlApi + 'api/AnswerType/GetAllAnswerOptionValues/';

    return service;

    function GetAllAnswerOptionValues(IdAnswerOption) {
        var deferr = $q.defer();
        var id = IdAnswerOption || 0;
        $http({
            method: 'GET',
            url: GetAllAnswerOptionValuesRoute + id,
            contentType: 'application/x-www-form-urlencoded',
        }).success(function (data) {
            deferr.resolve(data);
        }).error(function (err, status) {
            deferr.reject(err);
        });
        return deferr.promise;


    }

}]);