﻿app.factory('baseAnswerServices', ['Constant', '$http', '$q', function (Constant, http, q) {
    var service = {
        getAllBaseAnswer: getAllBaseAnswer,
        saveBaseAnswer: saveBaseAnswer,
        deleteBaseAnswer: deleteBaseAnswer
    }
    var RoutePrefix = "api/baseAnswer";

    return service;

    function getAllBaseAnswer() {
        var deferr = q.defer();
        http({
            method: 'GET',
            url: Constant.baseurlApi + RoutePrefix + "/GetAllBaseAnswerApi",
            headers: {
                common: {
                    'Accept': 'application/json, text/plain'
                }
            }
        }).success(function (data) {
            itemsArray = data;
            deferr.resolve(itemsArray);
        }).error(function (err, status) {
            deferr.reject(err);
        });

        return deferr.promise;
    }

    function saveBaseAnswer(model)
    {
        console.log(model);
        var deferred = q.defer();
        http({
            method: 'POST',
            url: Constant.baseurlApi + RoutePrefix + "/saveBaseAnswerApi/",
            data: model,
            contentType: 'application/json; charset=utf-8',
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            deferred.reject(status);
        });
        return deferred.promise;
    }

    function deleteBaseAnswer(model) {
        console.log(model);
        var deferred = q.defer();
        http({
            method: 'POST',
            url: Constant.baseurlApi + RoutePrefix + "/deleteApi/",
            data: model,
            contentType: 'application/json; charset=utf-8',
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            deferred.reject(status);
        });
        return deferred.promise;
    }

}]);