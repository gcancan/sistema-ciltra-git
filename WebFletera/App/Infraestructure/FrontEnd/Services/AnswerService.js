﻿app.factory('answerServices', ['Constant', '$http', '$q', function (Constant, http, q) {
    var service = {
        
        saveAnswer: saveAnswer
    }
    var RoutePrefix = "api/answer";

    return service;    

    function saveAnswer(model) {
        console.log(model);
        var deferred = q.defer();
        http({
            method: 'POST',
            url: Constant.baseurlApi + RoutePrefix + "/saveAnswerApi/",
            data: model,
            contentType: 'application/json; charset=utf-8',
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            deferred.reject(status);
        });
        return deferred.promise;
    }    

}]);