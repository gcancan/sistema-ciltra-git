﻿app.factory('EstadosServices', ['Constant', '$http', '$q', function (Constant, http, q) {
    var service = {
        getAllEstados: getAllEstados
    }
    var RoutePrefix = "api/Estados";
    return service;

    function getAllEstados() {
        var deferr = q.defer();
        http({
            method: 'GET',
            url: Constant.baseurlApi + RoutePrefix + "/GetAllEstadosApi",
            headers: {
                common: {
                    'Accept': 'application/json, text/plain'
                }
            }
        }).success(function (data) {
            itemsArray = data;
            deferr.resolve(itemsArray);
        }).error(function (err, status) {
            deferr.reject(err);
        });

        return deferr.promise;
    }
}]);