﻿app.factory('OptionTypeService', ['$q', '$http', 'Constant', function ($q, $http, Constant) {
    var service = {

        GetAllOptionTypes: GetAllOptionTypes

    };


    var GetAllOptionTypesRoute = Constant.baseurlApi + 'api/AnswerType/GetAllOptionTypes';


    return service;

    function GetAllOptionTypes() {
        var deferr = $q.defer();
        $http({
            method: 'GET',
            url: GetAllOptionTypesRoute
        }).success(function (data) {
            productArray = data;
            deferr.resolve(productArray);
        }).error(function (err, status) {
            deferr.reject(err);
        });
        return deferr.promise;


    }


}]);