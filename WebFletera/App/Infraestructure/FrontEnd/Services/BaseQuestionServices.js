﻿app.factory('BaseQuestionServices', ['Constant', '$http', '$q', function (Constant, http, q) {
    var service = {
        getAllBaseQuestion: getAllBaseQuestion,
        saveBaseQuestion: saveBaseQuestion,
        deleteBaseQuestion: deleteBaseQuestion
    }
    var RoutePrefix = "api/baseQuestion";

    return service;

    function getAllBaseQuestion() {
        var deferr = q.defer();
        http({
            method: 'GET',
            url: Constant.baseurlApi + RoutePrefix + "/GetAllBaseQuestionApi",
            headers: {
                common: {
                    'Accept': 'application/json, text/plain'
                }
            }
        }).success(function (data) {
            itemsArray = data;
            deferr.resolve(itemsArray);
        }).error(function (err, status) {
            deferr.reject(err);
        });

        return deferr.promise;
    }

    function saveBaseQuestion(model) {
        console.log(model);
        var deferred = q.defer();
        http({
            method: 'POST',
            url: Constant.baseurlApi + RoutePrefix + "/saveBaseQuestionApi/",
            data: model,
            contentType: 'application/json; charset=utf-8',
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            deferred.reject(status);
        });
        return deferred.promise;
    }

    function deleteBaseQuestion(model) {
        console.log(model);
        var deferred = q.defer();
        http({
            method: 'POST',
            url: Constant.baseurlApi + RoutePrefix + "/deleteApi/",
            data: model,
            contentType: 'application/json; charset=utf-8',
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            deferred.reject(status);
        });
        return deferred.promise;
    }

}]);