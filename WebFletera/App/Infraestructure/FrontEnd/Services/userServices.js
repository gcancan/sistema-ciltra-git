﻿app.factory('userService', ['Constant', '$http', '$q', function (Constant, http, q) {
    var service = {
        getAllUsers: getAllUsers,
        saveUser: saveUser,
        updateUser: updateUser,
        deleteUser: deleteUser,
        getUserInfo: getUserInfo,
        getAllProduct: getAllProduct
    }
    var RoutePrefix = "api/Account";

    return service;

    function getAllProduct() {
        var deferr = q.defer();
        http({
            method: 'GET',
            url: Constant.baseurlApi + 'api/Product' + '/GetAllProduct',
            headers: {
                common: {
                    'Accept': 'application/json, text/plain'
                }
            }
        }).success(function (data) {
            productArray = data;
            deferr.resolve(productArray);
        }).error(function (err, status) {
            deferr.reject(err);
        });
        return deferr.promise;
    }

    function getAllUsers() {
        var deferr = q.defer();
        http({
            method: 'GET',
            url: Constant.baseurlApi + RoutePrefix + "/getallusers",
            headers: {
                common: {
                    'Accept': 'application/json, text/plain'
                }
            }
        }).success(function (data) {
            itemsArray = data;
            deferr.resolve(itemsArray);
        }).error(function (err, status) {
            deferr.reject(err);
        });

        return deferr.promise;
    }

    function getUserInfo() {
        var deferr = q.defer();
        http({
            method: 'GET',
            url: Constant.baseurlApi + RoutePrefix + "/UserInfo",
            headers: {
                common: {
                    'Accept': 'application/json, text/plain'
                }
            }
        }).success(function (data) {
            itemsArray = data;
            deferr.resolve(itemsArray);
        }).error(function (err, status) {
            deferr.reject(err);
        });

        return deferr.promise;
    }

    function saveUser(model) {
        var deferred = q.defer();
        http({
            method: 'POST',
            url: Constant.baseurlApi + RoutePrefix + "/Register/",
            data: model,
            contentType: 'application/json; charset=utf-8',
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            var errorData =
                {
                    errorDescription: err,
                    errorStatus: status
                }
            deferred.reject(errorData);
        });
        return deferred.promise;
    }
    function updateUser(model) {
        var deferred = q.defer();
        http({
            method: 'POST',
            url: Constant.baseurlApi + RoutePrefix + "/Update/",
            data: model,
            contentType: 'application/json; charset=utf-8',
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            deferred.reject(err);
        });
        return deferred.promise;
    }
    function deleteUser(model) {
        var deferred = q.defer();
        http({
            method: 'POST',
            url: Constant.baseurlApi + RoutePrefix + "/deleteuser/",
            data: model,
            contentType: 'application/json',
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            deferred.reject(status);
        });
        return deferred.promise;
    }
}]);