﻿app.factory('productServices', ['$http', '$q', 'Constant', function ($http, $q, $Constant) {

    var services = {
        getAllProduct: getAllProduct,
        deletProduct: deletProduct,
        saveProduct: saveProduct
    }

    return services;

    function getAllProduct() {
        var deferr = $q.defer();
        $http({
            method: 'GET',
            url: $Constant.baseurlApi + 'api/Product' + '/GetAllProduct',
            headers: {
                common: {
                    'Accept': 'application/json, text/plain'
                }
            }
        }).success(function (data) {
            productArray = data;
            deferr.resolve(productArray);
        }).error(function (err, status) {
            deferr.reject(err);
        });
        return deferr.promise;
    }

    function saveProduct(model) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: $Constant.baseurlApi + 'api/Product' + '/SaveProduct/',
            data: model,
            contentType: 'application/json; charset=utf-8',
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            deferred.reject(status);
        });
        return deferred.promise;
    }



    function deletProduct(model) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: $Constant.baseurlApi + 'api/Product' + '/DeleteProduct/',
            data: model,
            contentType: 'application/json; charset=utf-8',
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            deferred.reject(status);
        });
        return deferred.promise;
    }

}]);