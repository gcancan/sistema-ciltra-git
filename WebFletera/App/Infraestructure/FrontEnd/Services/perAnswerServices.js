﻿app.factory('PerAnswerService', ['$http', '$q', 'Constant', function ($http, $q, Constant) {

    var services = {
        getAllPerAnswer: getAllPerAnswer,
        SavePerAnswer: SavePerAnswer
    }


    var SavePerAnswerRoute = Constant.baseurlApi + 'api/PerAnswer/SavePerAnswer';
    var getAllPerAnswerRoute = Constant.baseurlApi+'api/PerAnswer/GetAllPerAnswer';

    return services;


    function SavePerAnswer(model) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: SavePerAnswerRoute,
            data: model,
            contentType: 'application/x-www-form-urlencoded',
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            console.log(err);
            deferred.reject(status);
        });
        return deferred.promise;
    }

    function getAllPerAnswer() {
        var deferr = $q.defer();
        $http({
            method: 'GET',
            url: getAllPerAnswerRoute
        }).success(function (data) {
            perAnswerArray = data;
            deferr.resolve(perAnswerArray);
        }).error(function (err, status) {
            deferr.reject(err);
        });
        return deferr.promise;
    }
}]);