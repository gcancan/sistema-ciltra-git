﻿app.factory('GetQuestionAnswerService', ['$http', '$q', 'Constant', function ($http, $q, $Constant) {

    var services = {
        getAquestionAnswer: getAquestionAnswer
    }

    return services;

    function getAquestionAnswer(idProduct) {
        var deferr = $q.defer();
        $http({
            method: 'GET',
            url: $Constant.baseurlApi + 'api/getQuestionAnswer' + '/GetTypeQuestionsAnswer?idProduct=' + idProduct,
            data: idProduct,
            headers: {
                common: {
                    'Accept': 'application/json, text/plain'
                }
            }
        }).success(function (data) {
            productArray = data;
            deferr.resolve(productArray);
        }).error(function (err, status) {
            deferr.reject(err);
        });
        return deferr.promise;
    }

}]);