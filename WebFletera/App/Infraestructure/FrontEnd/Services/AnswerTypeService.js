﻿"use strict"
app.factory('AnswerTypeService', ['$q', '$http', 'Constant', function ($q, $http, Constant) {

    var service = {
        GetAllAnswerOption: GetAllAnswerOption,
        SaveAnswerOption: SaveAnswerOption,
        DeleteAnswerOption : DeleteAnswerOption
    };



    var SaveAnswerOptionRoute = Constant.baseurlApi + 'api/AnswerType/SaveAnswerOption';
    var GetAllAnswerOptionRoute = Constant.baseurlApi + 'api/AnswerType/GetAllAnswerOption';
    var DeleteAnswerOptionRoute = Constant.baseurlApi + 'api/AnswerType/DeleteAnswerOption/';
    return service;



    function DeleteAnswerOption(id) {
        var deferred = $q.defer();
        $http({
            method: 'DELETE',
            url: DeleteAnswerOptionRoute + id,
            contentType: 'application/x-www-form-urlencoded',
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            console.log(err);
            deferred.reject(status);
        });
        return deferred.promise;

    }

    function GetAllAnswerOption() {
        var deferr = $q.defer();
        $http({
            method: 'GET',
            url: GetAllAnswerOptionRoute,
        }).success(function (data) {
            deferr.resolve(data);
        }).error(function (err, status) {
            deferr.reject(err);
        });

        return deferr.promise;
    }

    function SaveAnswerOption(model) {
        var deferred = $q.defer();
        $http({
            method: 'POST',
            url: SaveAnswerOptionRoute,
            data: model,
            contentType: 'application/x-www-form-urlencoded',
        }).success(function (data, status, headers, cfg) {
            deferred.resolve(data);
        }).error(function (err, status) {
            console.log(err);
            deferred.reject(status);
        });
        return deferred.promise;
    }


}]);