﻿'use strict'
var app = angular.module('app', ['ngRoute', 'LocalStorageModule', 'xeditable']);

app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/Product',
        {
            controller: 'productViewModel',
            templateUrl: '/App/Modules/Administration/Product/productView/product.html'
        })
        .when('/Estado',
        {
            controller: 'EstadosController',
            templateUrl: '/App/Modules/Administration/Catalogs/Estados/EstadosViews/Estados.html',
        })

        //.when('/login',
        //{
        //    title: 'Loginssss',
        //    templateUrl: '/App/Modules/Login/View/viewLogin.html',
        //    controller: 'loginController'
        //})

        .when('/users',
        {
            controller: 'userController',
            templateUrl: '/App/Modules/Administration/User/userView/user.html'
        })


        .when("/BaseQuestion", {
            controller: 'BaseQuestionController',
            templateUrl: '/App/Modules/Administration/Catalogs/BaseQuestion/BaseQuestionViews/BaseQuestion.html',
        })


        .when("/BaseAnswer", {
            controller: 'BaseAnswerController',
            templateUrl: '/App/Modules/Administration/Catalogs/BaseAnswer/BaseAnswerViews/BaseAnswer.html',
        })

        .when("/AnswerOption", {
            controller: 'AnswerOptionController',
            templateUrl: '/App/Modules/Administration/Catalogs/AnswerType/AnswerTypeView/AnswerOption.html',
        })
        .when("/PerAnswer", {
            controller: 'PerAnswerController',
            templateUrl: '/App/Modules/Administration/Catalogs/PerAnswer(useless)/PerAnswerView/PerAnswer.html',
        })
        ;


    $routeProvider.when('/insertProduct',
       {
           controller: 'productViewModel',
           templateUrl: '/App/Modules/Administration/Product/productView/insertProduct.html'
       });

    $routeProvider.when('/Question',
    {
        controller: 'questionViewModel',
        templateUrl: '/App/Modules/Question/QuestionView/question.html'
    });



    $routeProvider.otherwise({ redirectTo: "/login" });

}]);

app.run(['authService', function (authService) {
    authService.fillAuthData();
}]);

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});

app.run(function (editableOptions) {
    editableOptions.theme = 'bs3';
});

app.run(function ($rootScope) {
    $rootScope.userNameShow = "not logged";
    $rootScope.userId = "no id";
    $rootScope.productId = "no id";
    //$rootScope.Export2Excel = false;
    $rootScope.dataToExport = [];
});

app.constant('Constant', {
    baseurlApi: "http://localhost:53664/" // Dev
    //baseurlApi: "http://192.168.1.213/SurveyService/" // QA
});



angular.module('app')
      .controller('MainCtrl', ['$scope', 'authService', '$rootScope', '$location', function ($scope, authService, $rootScope, $location) {
          //$scope.Export2Excel = false;
          //$scope.dataUser = localStorageService.get('authorizationData');
          //$scope.userName = localStorage.getItem('authData.userName');
          //$scope.exportDataToExcel = function (array) {
          //    alasql('SELECT * INTO XLSX("users.xlsx",{headers:true}) FROM ? WHERE exportData = true', [array]);
          //    };
          $scope.logOut = function () {
              $rootScope.dataToExport = [];
              $location.path('/login');
              authService.logOut();


          }

          $scope.authentication = authService.authentication;
      }]);