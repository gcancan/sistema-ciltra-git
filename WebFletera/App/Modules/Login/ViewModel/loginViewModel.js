app.controller('loginController', ['$scope', '$location', 'authService', '$rootScope', function ($scope, $location, authService, $rootScope) {

    $scope.loginData = {
        userName: "",
        password: ""
    };

    $scope.message = "";

    $scope.login = function () {

        authService.login($scope.loginData).then(function (response) {
      
           // $location.path('/Product');
            $rootScope.userNameShow = response.userName;
            $rootScope.userId = response.userId;
            //$rootScope.Export2Excel = response.exportExcel;
            $rootScope.productId = response.IdProduct;
            if (response.Role == 'user')
                $location.path("/Question");
            if (response.Role == 'Administrator')
                $location.path("/users");
        },
         function (err) {
             $scope.message = err.error_description;
         });
    };

}]);