﻿app.controller('productViewModel', ['$scope', '$filter', 'productServices', 'BaseQuestionServices', 'AnswerTypeService', function ($scope, $filter, productServices, BaseQuestionServices, AnswerTypeService) {


    function getAllProduct() {
        productServices.getAllProduct().then(function (httpData) {
            $scope.productArray = [].concat(httpData);
        }, function (reason) {
            console.log("Error" + reason);
        });
    }

    function getAllBaseQuestion() {
        BaseQuestionServices.getAllBaseQuestion().then(function (httpData) {
            $scope.arrayBaseQuestion = [].concat(httpData);
        }, function (reason) {
            //console.log("error" + reason);

        })
    };

    function GetAllAnswerOption() {
        AnswerTypeService.GetAllAnswerOption().then(function (data) {
            $scope.AnswerOptionArray = [].concat(data);
            $scope.AnswerOptionModel = $scope.AnswerOptionArray[0];
        }, function (reason) {
            $log.warn(reason);
        });

    };
    function reset() {
        $scope.newProducts = {
            name: "",
            description: "",
            theme: "",
            questionsPerPage: "",
            idProduct: 0,
            ListQuestionsPerProduct: null,
            PlaceHolder: "",
            totalQuestion: ""
        }
    }
    $scope.NewQuestionsPerProducts = [];

    $scope.sendNewProducto = function (data, product) {
        if (data != undefined && product != undefined) {
            data.idProduct = product.idProduct;
            if ($scope.validateProduct(data)) {
                saveProduct(data);
            } else {
                $('#myModalError').modal('show');
               // alert("Please check the fields!");
            }
        }
        else {
            $scope.newProducts.ListQuestionsPerProduct = $scope.NewQuestionsPerProducts;
            if ($scope.validateProduct()) {

                saveProduct($scope.newProducts);
            } else {
                $('#myModalError').modal('show');
               // alert("Please check the fields!");
            }
        }

    }
    $scope.validateProduct = function (product) {
        var result = true;
        if (product != undefined) {
            if (product.idProduct < 0) { return false }
            if (product.name.length < 1) { return false }
            if (product.description.length < 1) { return false }
            if (product.theme.length < 1) { return false }
            if (product.questionsPerPage < 0) { result = false }
        }
        else {
            if ($scope.newProducts.name.length < 1) { return false; }
            if ($scope.newProducts.description.length < 1) { return false; }
            if ($scope.newProducts.theme.length < 1) { return false; }
            if ($scope.newProducts.questionsPerPage < 1) { return false; }
            if ($scope.newProducts.ListQuestionsPerProduct < 1) { return false; }
        }
        return result;
    }

    function saveProduct(product) {
        productServices.saveProduct(product).then(function (saveResult) {
            if (saveResult.statusCode != "0") {
                $('#myModalMsg').modal('show');
                $scope.success = { msg: saveResult.message }
            }
            else {
                if (product.idProduct == 0) {
                    product.idProduct = saveResult.idIdenttity;
                    product.totalQuestion = saveResult.totalQuestion;
                    var count = 0;
                    var valor = {};
                    $('#myModalMsg').modal('show');
                    $scope.success = { msg: saveResult.message }        
                    $scope.productArray.push(product);   
                } else {
                    $('#myModalEditProduct').modal('show');
                    $scope.success = { msg: saveResult.message }
                }
                reset();
                $scope.NewQuestionsPerProducts = [];
            }
        }, function (reason) {
            console.log("error" + reason);
        });
    }


    $scope.deletProduct = function (value, index) {
        var title = "Delete '" + value.name + "'";
        var msg = "Are you sure you want to remove this Product?";
        $scope.DeletModal = {title : title, msg : msg, value:value, index:index}
        $('#myModalDelet').modal('show');   
    };

    $scope.confirm = function (value) {
        var values = value.value;
        var index = value.index;
        $('#myModalDelet').modal('hide');
        productServices.deletProduct(values)
            .then(function (saveResult) {
                if (saveResult.statusCode != 0) {
                    //alert(saveResult.message);
                    $('#myModalEditProduct').modal('show');
                    $scope.success = { msg: saveResult.message }
                }
                else {
                    //alert(saveResult.message);
                    $('#myModalEditProduct').modal('show');
                    $scope.success = { msg: saveResult.message }
                    $scope.productArray.splice(index, 1);                    
                }
            })
    }

    var valor = []
    function existsValue(valor, questionsPerProduct) {
        for (i = 0; i < valor.length; i++) {
            if (questionsPerProduct.description.description == valor[i].description && questionsPerProduct.question.question == valor[i].question) {
                return false;
            }
        }
        return true;
    }

    $scope.save = function (questionsPerProduct) {
        if ($scope.QuestionsPerProductForm.$valid) {
            if (questionsPerProduct != undefined) {
                if (questionsPerProduct.question != undefined) {

                    if (questionsPerProduct.description != undefined && questionsPerProduct.description != null) {
                        if (questionsPerProduct.AddOpenAnswer != undefined && questionsPerProduct.AddOpenAnswer == true) {

                            if ($scope.NewQuestionsPerProducts.length > 0) {
                                var record = existsValue($scope.NewQuestionsPerProducts, questionsPerProduct);

                                if (record != false) {
                                    var objetoTmp = {
                                        IdQuestionsPerProduct: 0,
                                        idAnswerOption: questionsPerProduct.description.idAnswerOption,
                                        description: questionsPerProduct.description.description,
                                        idBaseQuestion: questionsPerProduct.question.idBaseQuestion,
                                        question: questionsPerProduct.question.question,
                                        OpenAnsWer: questionsPerProduct.AddOpenAnswer,
                                        PlaceHolder: questionsPerProduct.PlaceHolder
                                    }

                                    $scope.NewQuestionsPerProducts.push(objetoTmp);
                                }
                                else { alert("The selected answer to that question already exists"); }
                            }
                            else {
                                var objetoTmp = {
                                    idAnswerOption: questionsPerProduct.description.idAnswerOption,
                                    description: questionsPerProduct.description.description,
                                    idBaseQuestion: questionsPerProduct.question.idBaseQuestion,
                                    question: questionsPerProduct.question.question,
                                    OpenAnsWer: questionsPerProduct.AddOpenAnswer,
                                    PlaceHolder: questionsPerProduct.PlaceHolder
                                }

                                $scope.NewQuestionsPerProducts.push(objetoTmp);
                            }
                        }
                        else {
                            if ($scope.NewQuestionsPerProducts.length > 0) {
                                var record = existsValue($scope.NewQuestionsPerProducts, questionsPerProduct);

                                if (record != false) {
                                    var objetoTmp = {
                                        IdQuestionsPerProduct: 0,
                                        idAnswerOption: questionsPerProduct.description.idAnswerOption,
                                        description: questionsPerProduct.description.description,
                                        idBaseQuestion: questionsPerProduct.question.idBaseQuestion,
                                        question: questionsPerProduct.question.question,
                                        OpenAnsWer: questionsPerProduct.AddOpenAnswer,
                                        PlaceHolder: questionsPerProduct.PlaceHolder
                                    }

                                    $scope.NewQuestionsPerProducts.push(objetoTmp);
                                }
                                else { alert("The selected answer to that question already exists"); }
                            }
                            else {
                                var objetoTmp = {
                                    idAnswerOption: questionsPerProduct.description.idAnswerOption,
                                    description: questionsPerProduct.description.description,
                                    idBaseQuestion: questionsPerProduct.question.idBaseQuestion,
                                    question: questionsPerProduct.question.question,
                                    OpenAnsWer: questionsPerProduct.AddOpenAnswer,
                                    PlaceHolder: questionsPerProduct.PlaceHolder
                                }

                                $scope.NewQuestionsPerProducts.push(objetoTmp);
                            }
                        }
                    }
                    else {
                        if (questionsPerProduct.AddOpenAnswer != undefined && questionsPerProduct.AddOpenAnswer == true) {
                                var objetoTmp = {
                                    idBaseQuestion: questionsPerProduct.question.idBaseQuestion,
                                    question: questionsPerProduct.question.question,
                                    OpenAnsWer: questionsPerProduct.AddOpenAnswer,
                                    PlaceHolder: questionsPerProduct.PlaceHolder
                                }

                                $scope.NewQuestionsPerProducts.push(objetoTmp);
                        }
                        else { alert("You must select at least one answer"); }
                    }                   
                }
                else { alert("You must select a Question"); }

            } else {
                alert("You must select the records");
            }
        }
    };

    $scope.DeletQuestion = function (value, index) {
        $scope.NewQuestionsPerProducts.splice(index, 1);
    };

    $scope.groups = [];
    $scope.loadGroups = function () {
        $scope.groups.length ? null : $scope.groups = $scope.AnswerOptionArray;
        return $scope.groups;
    };

    $scope.showStatus = function (QuestionsPerProduct) {
        var selected = [];
        if (QuestionsPerProduct.idAnswerOption && $scope.groups.length) {
            var selected = $filter('filter')($scope.groups, { idAnswerOption: QuestionsPerProduct.idAnswerOption });
            return selected.length ? selected[0].description : 'Not set';
        } else {
            return QuestionsPerProduct.description || 'Not set';
        }
    };

    $scope.saveNewAnswer = function (data, QuestionsPerProduct) {
        if (data.idAnswerOption > 0) {
            if (data.idAnswerOption && $scope.groups.length) {
                var selected = $filter('filter')($scope.groups, { idAnswerOption: data.idAnswerOption });
                selected.length ? selected[0].description : 'Not set';
            } else {
                QuestionsPerProduct.description || 'Not set';
            }
        }
    };


    $scope.DetailAnswer = function (QuestionPerProduct)
    {
        var OptionAnswer = [];
        $scope.OptionAnswer =   $filter('filter')($scope.AnswerOptionArray, { idAnswerOption: QuestionPerProduct.idAnswerOption });
        $('#myModalAnswer').modal('show');
    }


    $scope.insertProduct = true;

    $scope.addProduct = function () {
        $scope.insertProduct = !$scope.insertProduct;
        reset();
        $scope.NewQuestionsPerProducts = [];
    }

    $scope.answer = [];
    $scope.lstTypeAnswer = [];
    $scope.editAnswerOption = function (row) {
        $scope.newProducts = { idProducto: row.idProduct, name: row.name, description: row.description, questionsPerPage: row.questionsPerPage, theme: row.theme, totalQuestion: row.totalQuestion };
        for (var i in row.listQuestionProduct) {
            for (var j in row.listQuestionProduct[i].listAnswer ) {
                
                if (row.listQuestionProduct[i].listAnswer[j].listOpenAnswer != null) {


                    for (var o in row.listQuestionProduct[i].listAnswer[j].listOpenAnswer) {

                        var lstAnswerType = {
                            question: row.listQuestionProduct[i].question,
                            IdOpenAnswer: row.listQuestionProduct[i].listAnswer[j].listOpenAnswer[o].idOpenAnswer,
                            PlaceHolder: row.listQuestionProduct[i].listAnswer[j].listOpenAnswer[o].placeHolder

                        }
                    }
                }
                else {
                    for (var O in row.listQuestionProduct[i].listAnswer[j].listAnswerOptionDTO) {
                        for (var z in row.listQuestionProduct[i].listAnswer[j].listAnswerOptionDTO[O].lstBaseAnswers) {
                            var answer = {
                                answer: row.listQuestionProduct[i].listAnswer[j].listAnswerOptionDTO[O].lstBaseAnswers[z].answer,
                                idBaseAnswer: row.listQuestionProduct[i].listAnswer[j].listAnswerOptionDTO[O].lstBaseAnswers[z].idBaseAnswer
                            }
                            $scope.answer.push(answer);
                            answer = {};
                        }
                        var lstAnswerType = {
                            question: row.listQuestionProduct[i].question,
                            idAnswerOption: row.listQuestionProduct[i].listAnswer[j].listAnswerOptionDTO[O].idAnswerOption,
                            description: row.listQuestionProduct[i].listAnswer[j].listAnswerOptionDTO[O].description,
                            listAnswer: $scope.answer
                        }
                        $scope.answer = [];
                    }
                }                               
                $scope.NewQuestionsPerProducts.push(lstAnswerType);
                lstAnswerType = {};
            }            
        }        
        $scope.insertProduct = !$scope.insertProduct;
    };

    GetAllAnswerOption();
    getAllBaseQuestion();
    getAllProduct();

}]);
