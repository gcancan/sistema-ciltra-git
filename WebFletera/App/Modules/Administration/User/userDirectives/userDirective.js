﻿(function(){
    //export html table to pdf, excel and doc format directive
    var exportTable = function(){
        var link = function($scope, elm, attr){
            $scope.$on('export-pdf', function(e, d){
                elm.tableExport({type:'pdf', escape:'false'});
            });
            $scope.$on('export-excel', function(e, d){
                elm.tableExport({type:'excel', escape:false});
            });
            $scope.$on('export-doc', function(e, d){
                elm.tableExport({type: 'doc', escape:false});
            });
        }
        return {
            restrict: 'C',
            link: link
        }
    }
     app.directive('userDirective', exportTable);
})();

app.directive('modalDialog', function () {
    return {
        restrict: 'E',
        scope: {
            show: '='
        },
        replace: true, // Replace with the template below
        transclude: true, // we want to insert custom content inside the directive
        link: function (scope, element, attrs) {
            scope.dialogStyle = {};
            if (attrs.width)
                scope.dialogStyle.width = attrs.width;
            if (attrs.height)
                scope.dialogStyle.height = attrs.height;
            scope.hideModal = function () {
                scope.show = false;
            };
        },
        template: "<div class='ng-modal' ng-show='show'><div class='ng-modal-overlay' ng-click='hideModal()'></div><div class='ng-modal-dialog' ng-style='dialogStyle'><div class='ng-modal-close' ng-click='hideModal()'>X</div><div class='ng-modal-dialog-content' ng-transclude></div></div></div>"
    };
});

angular.module('app')

  /**
  * Directives is used to validate the multiple emails
  */
  .directive(
    'dnEmailCustomValidation',
    function () {
        return {
            require: ['^form', 'ngModel'],
            link: function ($scope, $element, attrs, ctls) {
                var $form = ctls.shift(),
                $model = ctls.shift(),
                validateMatch = function () {
                    var errorFlag = true;

                    // Email Validations
                    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    var emailIds = $model.$viewValue;
                    if (emailIds != "") {
                        var emailIdsArr = emailIds.split(",");
                        angular.forEach(emailIdsArr, function (value, key) {
                            if (!re.test(value)) {
                                errorFlag = false;
                                return false;
                            }
                        });
                    }

                    $model.$setValidity('invalid', errorFlag);
                };

                // Trigger a validation check
                $model.$viewChangeListeners.push(validateMatch);
            }
        };
    }
  );