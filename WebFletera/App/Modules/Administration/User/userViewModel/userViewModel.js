﻿app.controller('userController', ['$scope', 'userService', '$rootScope', function ($scope, userService, $rootScope) {
    //  $scope.userNameShow = $scope.$rootScope.userNameShow;
    $scope.adminSelected = false;
    $scope.modalShown = false;
    $scope.toggleModal = function() {
        $scope.modalShown = !$scope.modalShown;
    };
    $scope.checkAdmin = function (varToCheck) {
        if(varToCheck == 'user')
            $scope.adminSelected = false;
        else
            $scope.adminSelected = true;
    }

    $scope.newUser = {
        id: '',
        userName: '',
        name: '',
        password: '',
        firstLastName: '',
        secondLastName: '',
        email: "",
        role: "user",
        exportData: false,
        IdProduct: null
    };
    function resetNewUser() {
        $scope.newUser = {
            id: '',
            userName: '',
            name: '',
            password: '',
            firstLastName: '',
            secondLastName: '',
            email: "",
            role: "user",
            exportData: false,
            IdProduct: null
        };
    }
    //$scope.submitForm = function (isValid) {

    //    // check to make sure the form is completely valid
    //    if (isValid) {
    //        alert('our form is amazing');
    //    }

    //};
    $scope.editUsertoForm = function (varUser) {
        $scope.showTable = !$scope.showTable;
        varUser.password = '123456789';
        $scope.newUser = varUser;
       // userService.updateUser(varUser).then(function (httpData) {
       //     $scope.messageSuccesfullCreated = "Succesfully created";
       //     reset();
       //     $scope.errorMessageCreated = '';
       //     $('#myModalUser').modal('show');
       // },
       //function (reason) {
       //    $('#myModalError').modal('show');
       //    $scope.errorMessageCreated = "error: " + reason.errorStatus + ": " + reason.errorDescription.message + " - " + reason.errorDescription.modelState.model;
       //    $scope.messageSuccesfullCreated = '';
       //});
    }
    $scope.deletUser = function (value, index) {
        var title = "Delete '" + value.name + "'";
        var msg = "Are you sure you want to remove this User?";
        $scope.DeletModal = { title: title, msg: msg, value: value, index: index }
        $('#myModalDelet').modal('show');
    };

    $scope.confirm = function (value) {
        var index = value.index;
        $('#myModalDelet').modal('hide');
        userService.deleteUser(value.value).then(function (httpData) {
            $scope.messageSuccesfullCreated = "Succesfully deleted";
            $scope.errorMessageCreated = '';
            $('#myModalUser').modal('show');
            $scope.usersArray.splice(index, 1);
        }, function (reason) {
            console.log("error" + reason);
            $('#myModalDeletError').modal('show');
            $scope.success = { msg: "error" + reason }

        });
        //productServices.deletProduct(values)
        //    .then(function (saveResult) {
        //        if (saveResult.statusCode != 0) {
        //            //alert(saveResult.message);
        //            $('#myModalEditProduct').modal('show');
        //            $scope.success = { msg: saveResult.message }
        //        }
        //        else {
        //            //alert(saveResult.message);
        //            $('#myModalEditProduct').modal('show');
        //            $scope.success = { msg: saveResult.message }
        //            $scope.productArray.splice(index, 1);
        //        }
        //    })
    }

    $scope.showTable = false;
    function getAllProduct() {
        userService.getAllProduct().then(function (httpData) {
            $scope.productsArray = [].concat(httpData);
        }, function (reason) {
            console.log("Error" + reason);
        });
    }
    
    $scope.exportData = function (array) {
            alasql('SELECT * INTO XLSX("users.xlsx",{headers:true}) FROM ? WHERE exportData = true', [array]);
            };

    //$scope.ShowHideTable = function (valor) {
    //    $scope.showTable = !$scope.showTable;
    //};

    //$scope.mostrarCompleto = function () {

    //    userService.getUserInfo().then(function (httpData) {
    //        $scope.usersInfo = [].concat(httpData);
    //    }, function (reason) {
    //        console.log("error" + reason);

    //    });
    //};
    $scope.selectedPorduct = '';
    $scope.deleteProduct = function (product, index) {
        alert('deleted is : ' + product);
    }
    $scope.arrayProduct = [];
    $scope.addProduct = function () {
        //Your logic
        //var currentItem = $scope.selectedPorduct;
        $scope.newUser.IdProduct = $scope.newUser.IdProduct.concat($scope.selectedPorduct)
      //  $scope.newUser.IdProduct.push($scope.selectedPorduct);
    }
    $scope.CancelAddUser = function () {
        reset();
    };
    $scope.messageSuccesfullCreated = '';
    $scope.errorMessageCreated = '';
    $scope.saveUser = function () {
        if ($scope.newUser.id == "") {
            userService.saveUser($scope.newUser).then(function (httpData) {
                $scope.newUser.id = httpData;
                $scope.usersArray.push($scope.newUser);
                $scope.messageSuccesfullCreated = "Succesfully created";
                reset();
                $scope.errorMessageCreated = '';
                $('#myModalUser').modal('show');

            }, function (reason) {
                $('#myModalError').modal('show');
                $scope.errorMessageCreated = "error: " + reason.errorStatus + ": " +  reason.errorDescription.message +" - "+ reason.errorDescription.modelState.model;
                $scope.messageSuccesfullCreated = '';
            });
        }
        else {
            userService.updateUser($scope.newUser).then(function (httpData) {
                $scope.messageSuccesfullCreated = httpData;
                 reset();
                 $scope.errorMessageCreated = '';
                 $('#myModalUser').modal('show');
             },
            function (reason) {
                $('#myModalError').modal('show');
                $scope.errorMessageCreated = "error: " + reason.errorStatus + ": " + reason.errorDescription.message + " - " + reason.errorDescription.modelState.model;
                $scope.messageSuccesfullCreated = '';
            });
        }

    }
    function reset() {
        $scope.userForm.$setPristine();
        $scope.newUser = {
            id: '',
            userName: '',
            name: '',
            password: '',
            firstLastName: '',
            secondLastName: '',
            email: "",
            role: "user",
            exportData: false,
            IdProduct: null
        };
    };
    function getAllUsers() {
        userService.getAllUsers().then(function (httpData) {
            $scope.usersArray = [].concat(httpData);
            $rootScope.dataToExport = [].concat(httpData);
            $rootScope.IsLogged = true;

        }, function (reason) {
            console.log("error" + reason);

        });
    }
    getAllProduct();
    getAllUsers();
  
    $scope.roles = [
      { value: 0, text: 'Administrator' },
      { value: 1, text: 'user' }
    ];

    //$scope.updateUser = function (data, id) {
    //    userService.updateUser(data).then(function (httpData) {
    //        $scope.messageSuccesfull = httpData;
    //    },
    //    function (reason) {
    //        $scope.errorMessage = "error: " + reason.message;
    //    });
    //};

    // remove user
    $scope.removeUser = function (index, model) {
        userService.deleteUser(model).then(function (httpData) {
            $scope.usersArray.splice(index, 1);
        }, function (reason) {
            console.log("error" + reason);

        });
        
    };

    $scope.export_action = 'excel';
    $scope.exportAction = function(){ 
        switch($scope.export_action){ 
            case 'pdf': $scope.$broadcast('export-pdf', {}); 
                break; 
            case 'excel': $scope.$broadcast('export-excel', {}); 
                break; 
            case 'doc': $scope.$broadcast('export-doc', {});
                break; 
            default: console.log('no event caught'); 
        }
    }
    console.log($scope);
}]);

app.directive('xuserDirective', [function(){

    return {
    
    link: function($scope, elm, attr){
        $scope.$on('export-pdf', function(e, d){
            elm.tableExport({type:'pdf', escape:'false'});
        });
        $scope.$on('export-excel', function(e, d){
            elm.tableExport({type:'excel', escape:false});
        });
        $scope.$on('export-doc', function(e, d){
            elm.tableExport({type: 'doc', escape:false});
        });
    },
        restrict: "C"       
    }
}]);