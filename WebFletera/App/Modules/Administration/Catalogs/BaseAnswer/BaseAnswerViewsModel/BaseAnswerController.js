﻿app.controller('BaseAnswerController', ['$scope', 'baseAnswerServices', function ($scope, baseAnswerServices) {

    $scope.arrayBaseAnswer;
    recet = function () {
        $scope.BaseAnswer = {
            idBaseAnswer: 0,
            answer: '',
            idStatus: true
        }
    }
    recet();
    function getAllBaseAnswer() {
        baseAnswerServices.getAllBaseAnswer().then(function (httpData) {
            $scope.arrayBaseAnswer = [].concat(httpData);
        }, function (reason) {
            console.log("error" + reason);

        })
    };
    getAllBaseAnswer();

    $scope.saveBaseAnswer = function (data, BaseAnswer) {
        var update = false;
        if (data != null) {
            $scope.BaseAnswer = {
                idBaseAnswer: BaseAnswer.idBaseAnswer,
                answer: data.answer,
                idStatus: true
            }
            update = true
        }
        if (validar()) {

            baseAnswerServices.saveBaseAnswer($scope.BaseAnswer).then(function (httpData) {

                var res = httpData;
                console.log(res);
                if (res.statusCode == 0) {
                    if (!update) {
                        $scope.BaseAnswer.idBaseAnswer = res.idIdenttity
                        $scope.arrayBaseAnswer.push($scope.BaseAnswer);                       
                    }
                    $scope.success = { msg: res.message }
                    $('#myModalEditProduct').modal('show');
                }
                else {
                    $scope.error = { msg: res.message, detalles: null }
                    $('#myModalError').modal('show');
                }
                recet();

            }, function (reason) {
                $scope.error = { msg: reason, detalles: null }
                $('#myModalError').modal('show');
            })
        }
        else {
            $scope.error = { msg: "Please Complete the fields below", detalles: ["Answer"] }
            $('#myModalError').modal('show');
        }

    };

    function validar() {
        if ($scope.BaseAnswer.answer.length <= 0) {
            return false;
        }
        return true;
    }

    $scope.deleteBaseAnswer = function (BaseAnswer, index) {
        var title = "Delete '" + BaseAnswer.answer + "'";
        var msg = "Are you sure you want to remove this Answer?";
        $scope.DeletModal = { title: title, msg: msg, value: BaseAnswer, index: index }
        $('#myModalDelet').modal('show');
    }

    $scope.confirm = function (value) {
        var values = value.value;
        var index = value.index;
        $('#myModalDelet').modal('hide')

        baseAnswerServices.deleteBaseAnswer(values).then(function (httpData) {
            var res = httpData;
            console.log(res);
            if (res.statusCode == 0) {
                $scope.arrayBaseAnswer.splice(index, 1);
                $('#myModalEditProduct').modal('show');
                $scope.success = { msg: res.message }
            }
            else {
                $scope.error = { msg: res.message, detalles: null }
                $('#myModalError').modal('show');
            }
        }, function (reason) {
            $scope.error = { msg: reason, detalles: null }
            $('#myModalError').modal('show');
        })
    }

    $scope.closeModal = function () {
        $('#myModal').modal('toggle');
    }
}]);