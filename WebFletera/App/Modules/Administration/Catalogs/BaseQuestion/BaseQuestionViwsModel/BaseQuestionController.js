﻿app.controller('BaseQuestionController', ['$scope', 'BaseQuestionServices', function ($scope, BaseQuestionServices) {

    $scope.arrayBaseQuestion;
    recet = function () {
        $scope.BaseQuestion = {
            idBaseQuestion: 0,
            question: '',
            idStatus: true
        }
    }
    recet();
    function getAllBaseQuestion() {
        BaseQuestionServices.getAllBaseQuestion().then(function (httpData) {
            $scope.arrayBaseQuestion = [].concat(httpData);
        }, function (reason) {
            console.log("error" + reason);

        })
    };
    getAllBaseQuestion();

    $scope.saveBaseQuestion = function (data, BaseQuestion) {
        var update = false;
        if (data != null) {
            $scope.BaseQuestion = {
                idBaseQuestion: BaseQuestion.idBaseQuestion,
                question: data.question,
                idStatus: true
            }
            update = true
        }
        //return;
        if (validar()) {

            BaseQuestionServices.saveBaseQuestion($scope.BaseQuestion).then(function (httpData) {

                var res = httpData;
                console.log(res);
                if (res.statusCode == 0) {
                    if (!update) {
                        $scope.BaseQuestion.idBaseQuestion = res.idIdenttity
                        $scope.arrayBaseQuestion.push($scope.BaseQuestion);
                    }
                    $scope.success = { msg: res.message }
                    $('#myModalEditProduct').modal('show');
                    
                }
                else {
                    $scope.error = { msg: res.message, detalles: null }
                    $('#myModalError').modal('show');
                }
                recet();

            }, function (reason) {
                //console.log("error" + reason);
                $scope.error = { msg: reason, detalles: null }
                $('#myModalError').modal('show');
            })
        }
        else {
            //Please Complete the fields below
            $scope.error = { msg: "Please Complete the fields below", detalles: ["Question"] }
            $('#myModalError').modal('show');

        }

    };

    function validar() {
        if ($scope.BaseQuestion.question.length <= 0) {
            return false;
        }
        return true;
    }

    $scope.deleteBaseQuestion = function (BaseQuestion, index) {
        var title = "Delete '" + BaseQuestion.question + "'";
        var msg = "Are you sure you want to remove this Question?";
        $scope.DeletModal = { title: title, msg: msg, value: BaseQuestion, index: index }
        $('#myModalDelet').modal('show');
    }

    $scope.confirm = function (value) {
        var values = value.value;
        var index = value.index;
        $('#myModalDelet').modal('hide');

        BaseQuestionServices.deleteBaseQuestion(values).then(function (httpData) {
            var res = httpData;
            console.log(res);
            if (res.statusCode == 0) {
                $scope.arrayBaseQuestion.splice(index, 1);
                $('#myModalEditProduct').modal('show');
                $scope.success = { msg: res.message }
            }
            else {
                $scope.error = { msg: res.message, detalles: null }
                $('#myModalError').modal('show');
            }
        }, function (reason) {
            $scope.error = { msg: reason, detalles: null }
            $('#myModalError').modal('show');
        })
    }

    $scope.closeModal = function () {
        $('#myModal').modal('toggle');
    }

    $scope.showGroup = function (user) {
        if (user.group && $scope.groups.length) {
            var selected = $filter('filter')($scope.groups, { id: user.group });
            return selected.length ? selected[0].text : 'Not set';
        } else {
            return user.groupName || 'Not set';
        }
    };

}]);