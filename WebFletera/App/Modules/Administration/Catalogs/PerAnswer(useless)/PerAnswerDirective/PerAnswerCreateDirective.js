﻿"use strict";
app.directive('perAnswerCreate', ['$log', 'PerAnswerService', function ($log, PerAnswerService) {



    var controller = function ($scope) {
        $scope.BackPerAnswerArray = [];
        $scope.PerAnswerModel = {
            IdPerAnswer : 0,
            Description: null,
            AddOpenAnswer : false,
            IdStatus : true
        };

        $scope.verifyModel = function () {
            if ($scope.PerAnswerModel.Description) {
                if ($scope.PerAnswerModel.Description.length < 1) {
                    alert("Description field must be captured");
                    return false;
                }
            } else {
                alert("Description field must be captured");
                return false;
            }

            if ($scope.PerAnswerModel.AddOpenAnswer) {
                if ($scope.PerAnswerModel.PlaceHolder) {
                    if ($scope.PerAnswerModel.PlaceHolder.length < 1) {
                        alert("Place Holder is required");
                        return false;
                    }
                } else {
                    alert("Place Holder is required");
                    return false;
                }
            }

            if ($scope.BackPerAnswerArray) {
                if ($scope.BackPerAnswerArray.length < 1) {
                    alert("At least one answer option is required");
                    return false;
                }
            } else {
                alert("At least one answer option is required");
                return false;
            }

            return true;
        }


        $scope.saveAnswerType = function () {
            if ($scope.verifyModel()) {
                var model = {
                    IdPerAnswer: $scope.PerAnswerModel.IdPerAnswer,
                    Description: $scope.PerAnswerModel.Description,
                    AddOpenAnswer: $scope.PerAnswerModel.AddOpenAnswer,
                    PlaceHolder : $scope.PerAnswerModel.PlaceHolder,
                    IdStatus: true,
                    lstBackPerAnswer: $scope.BackPerAnswerArray
                }

                $scope.SendToSave(model);
            }

        };


        $scope.$watch('PerAnswerModel.AddOpenAnswer', function () {
            if (!$scope.PerAnswerModel.AddOpenAnswer) {
                $scope.PerAnswerModel.PlaceHolder = "";
            }
        });



        $scope.cancelSaveAnswerType = function () {
            $scope.BackPerAnswerArray = [];
            $scope.PerAnswerModel = { IdAnswerOption: 0, Description: null,AddOpenAnswer:false,PlaceHolder:null};
            $scope.AnswerOptionModel = $scope.AnswerOptionArray[0];
            $scope.perAnswerCreate = !$scope.perAnswerCreate;

        };


        $scope.SendToSave = function (model) {
            PerAnswerService.SavePerAnswer(model).then(function (result) {
                if (result.statusCode === 0) {
                    alert(result.message);
                    $scope.cancelSaveAnswerType();
                } else {
                    alert(result.message);
                }
            }, function (reason) {
                $log.error(reason)
            });
        }



    }


    return {
        controller:controller,
        restrict: "ACE",
        replace: true,
        templateUrl: "/App/Modules/Administration/Catalogs/PerAnswer/PerAnswerDirective/PerAnswerCreateTemplate.html"
    }
}]);