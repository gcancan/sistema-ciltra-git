﻿app.directive('perAnswerOptionsTable', ['$log', 'AnswerTypeService', function ($log,AnswerTypeService) {


    var controller = function ($scope) {

        $scope.addOptionToAnswerType = function () {
            $log.info($scope.AnswerOptionModel);
            var model = {
                idAnswerOption: $scope.AnswerOptionModel.idAnswerOption,
                leyend : $scope.AnswerOptionModel.leyend,
                description: $scope.AnswerOptionModel.description,
                optionTypeName : $scope.AnswerOptionModel.optionTypeName,
                idStatus: $scope.AnswerOptionModel.idStatus
            };
            if (selectId($scope.AnswerOptionModel.idAnswerOption) === -1) {
                $scope.BackPerAnswerArray.push(model);
            }


        }

        $scope.deleteOptionForAnswer = function (row) {
            $scope.BackPerAnswerArray.splice(selectId(row.idAnswerOption), 1);
        }

        function selectId(id) {
            for (var i = 0; i < $scope.BackPerAnswerArray.length; i++) {
                if ($scope.BackPerAnswerArray[i].idAnswerOption === id)
                    return i;

            }
            return -1;
        }
    };


    return {

        controller: controller,
        restrict: "AEC",
        replace: true,
        transclude:true,
        templateUrl: "/App/Modules/Administration/Catalogs/PerAnswer/PerAnswerDirective/PerAnswerOptionsTableTemplate.html"
    }

}]);