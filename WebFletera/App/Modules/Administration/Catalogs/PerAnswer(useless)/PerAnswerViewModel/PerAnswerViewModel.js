﻿app.controller('PerAnswerController', ['$scope', 'AnswerTypeService', function ($scope, AnswerTypeService) {

    $scope.AnswerOptionArray = [];
    $scope.PerAnswerArray = [];
    $scope.BackPerAnswerArray = [];
    $scope.AnswerOptionModel = { idAnswerOption: 0, description: null, idStatus:true };

    $scope.GetAllAnswerOption = function () {
        AnswerTypeService.GetAllAnswerOption().then(function (data) {
            $scope.AnswerOptionArray = [].concat(data);
            $scope.AnswerOptionModel = $scope.AnswerOptionArray[0];
        }, function (reason) {
            $log.warn(reason);
        });

    };

    (function InitializeVariables() {
        $scope.GetAllAnswerOption();
    })();

    $scope.newAnswerType = function () {
        $scope.perAnswerCreate = !$scope.perAnswerCreate;


    };


    $scope.editAnswerOption = function (row)
    {


    }


    $scope.deleteAnswerOption = function (row)
    {


    }



}]);