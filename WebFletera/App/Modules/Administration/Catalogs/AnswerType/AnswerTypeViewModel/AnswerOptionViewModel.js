﻿"use strict"
app.controller('AnswerOptionController', ['$scope', '$log', 'AnswerTypeService', function ($scope,$log, AnswerTypeService) {
    

    $scope.AnswerOptionArray = [];
    $scope.OptionTypesArray = [];
    $scope.AnswerOptionValuesArray = [];
    $scope.AnswerOptionModel = {IdAnswerOption:0, Leyend: null, Description: null, IdOptionType: null };

    $scope.newAnswerOption = function () {
        $scope.shAnswerOptionCreate = !$scope.shAnswerOptionCreate;
        $scope.AnswerOptionModel.IdAnswerOption = 0;

    };


    $scope.GetAllAnswerOption = function () {
        AnswerTypeService.GetAllAnswerOption().then(function (data) {
            $scope.AnswerOptionArray = [].concat(data);
        }, function (reason) {
            $log.warn(reason);
        });
    };

    (function InitializeVariables() {
        $scope.GetAllAnswerOption();
    })();






    

    $scope.deleteAnswerOption = function (row) {
        var title = "Delete '" + row.leyend + "'";
        var msg = "Are you sure you want to remove this Answer Option?";
        $scope.DeletModal = { title: title, msg: msg, value: row }
        $('#myModalDelet2').modal('show');
    };

    $scope.confirm2 = function (value) {
        var values = value.value;
        $('#myModalDelet2').modal('hide')
        AnswerTypeService.DeleteAnswerOption(values.idAnswerOption).then(function (data) {
            if (data.statusCode === 0) {
                $('#myModalEditProduct').modal('show');
                $scope.success = { msg: data.message }
                $scope.AnswerOptionArray.splice(GetIndexItem(values.idAnswerOption), 1);
            } else {
                $scope.error = { msg: data.message, detalles: null }
                $('#myModalError').modal('show');
            }
        }, function (reason) {
            $scope.error = { msg: reason.message, detalles: null }
            $('#myModalError').modal('show');
        });
    }

    $scope.editAnswerOption = function (row) {
        $scope.AnswerOptionModel = { IdAnswerOption:row.idAnswerOption, Leyend: row.leyend, Description: row.description, IdOptionType: row.idOptionType };
        $scope.AnswerOptionValuesArray = row.lstBaseAnswers;
        $scope.OptionTypeModel = $scope.OptionTypesArray[GetIndexCombo(row.idOptionType)];
        $scope.shAnswerOptionCreate = !$scope.shAnswerOptionCreate;
    };

    function GetIndexCombo(id) {
        for (var i = 0; i < $scope.OptionTypesArray.length; i++) {
            if ($scope.OptionTypesArray[i].idOptionType ===id) {
                return i;
            }
        }
        return -1;
    }

    function GetIndexItem(id) {
        for (var i = 0; i < $scope.AnswerOptionArray.length; i++) {
            if ($scope.AnswerOptionArray[i].idAnswerOption === id) {
                return i;
            }
        }
        return -1;
    }



}]);