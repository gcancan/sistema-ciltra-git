﻿app.directive('answerOptionValuesTable', ['$log', 'AnswerOptionValuesService', 'baseAnswerServices', function ($log, AnswerOptionValuesService, baseAnswerServices) {

    var link = function (scope, element, attribute) {
        //Not implementation

    }


    var controller = function ($scope) {
        $scope.BaseAnswerArray = [];
        (function initializeValues() {
            GetAllAnswerOptionValues();

            baseAnswerServices.getAllBaseAnswer().then(function (data) {
                $scope.BaseAnswerArray = [].concat(data);
                $scope.BaseAnswerModel = $scope.BaseAnswerArray[0];
            }, function (reason) {
                $log.warn("error" + reason);

            })


        })();


        function GetAllAnswerOptionValues() {
            AnswerOptionValuesService.GetAllAnswerOptionValues($scope.AnswerOptionModel.IdAnswerOption).then(function (data) {
                $scope.AnswerOptionValuesArray = [].concat(data);
                $log.info($scope.AnswerOptionValuesArray);
            }, function (reason) {
                $log.warn(reason);
            });
        }


        function verifyModel(model) {


        }

        $scope.addAnswerToOption = function () {
            var model = {
                idBaseAnswer: $scope.BaseAnswerModel.idBaseAnswer,
                answer: $scope.BaseAnswerModel.answer,
                idStatus: $scope.BaseAnswerModel.idStatus

            };
            if (selectId($scope.BaseAnswerModel.idBaseAnswer) === -1) {
                $scope.AnswerOptionValuesArray.push(model);
            }           

        }

        $scope.deleteAnswerForOption = function (row) {  
            var title = "Delete '" + row.answer + "'";
            var msg = "Are you sure you want to remove this Answer?";
            $scope.DeletModal = { title: title, msg: msg, value: row}
            $('#myModalDelet').modal('show');
        }
        $scope.confirm = function (value) {
            $scope.AnswerOptionValuesArray.splice(selectId(value.idBaseAnswer), 1);
            $('#myModalDelet').modal('hide')
        }

        function selectId(id) {
            for (var i = 0; i < $scope.AnswerOptionValuesArray.length; i++) {
                if ($scope.AnswerOptionValuesArray[i].idBaseAnswer === id)
                    return i;
                
            }
            return -1;
        }


    }

    return {
        link: link,
        controller: controller,
        restrict: 'ACE',
        replace: true,
        transclude:true,
        templateUrl: "/App/Modules/Administration/Catalogs/AnswerType/AnswerTypeDirectives/AnswerOptionValuesTableTemplate.html"

    }

}]);