﻿app.directive("answerOptionCreate", ['$log', 'OptionTypeService', 'AnswerOptionValuesService', 'AnswerTypeService', function ($log, OptionTypeService, AnswerOptionValuesService,
    AnswerTypeService) {


    var link = function (scope, element, attribute) {
        //Not implementation

    }


    var controller = function ($scope) {


        (function initializeValues() {
            OptionTypeService.GetAllOptionTypes().then(function (data) {
                $scope.OptionTypesArray = [].concat(data);
                $log.info($scope.OptionTypesArray);
                $scope.OptionTypeModel = $scope.OptionTypesArray[0];
            }, function (reason) {
                $log.warn(reason);
            });
        })();

        

        $scope.verifyModel = function (model) {
            $scope.detallesError = [];
            var respuesta = true;
            if ($scope.AnswerOptionModel.Leyend) {
                if ($scope.AnswerOptionModel.Leyend.length < 1) {
                    //alert("Name field must be captured");
                    //return false;
                    $scope.detallesError.push("Name");
                    respuesta = false;
                }
            } else {
                //alert("Name field must be captured");
                //return false;
                $scope.detallesError.push("Name");
                respuesta = false;
            }


            if ($scope.AnswerOptionModel.Description) {
                if ($scope.AnswerOptionModel.Description.length < 1) {
                    //alert("Description field must be captured");
                    //return false;
                    $scope.detallesError.push("Description");
                    respuesta = false;
                }
            } else {
                //alert("Description field must be captured");
                //return false;
                $scope.detallesError.push("Description");
                respuesta = false;
            }

            if ($scope.AnswerOptionValuesArray) {
                if ($scope.AnswerOptionValuesArray.length <= 0) {
                    //alert("At least one option must be selected");
                    //return false;
                    $scope.detallesError.push("Answers");
                    respuesta = false;
                }
            }
            if (!respuesta) {
                $scope.error = { msg: "Please Complete the fields below", detalles: $scope.detallesError }
                $('#myModalError').modal('show');
            }
            return respuesta;
        }


        $scope.cancelSaveAnswerOption = function () {
            $scope.AnswerOptionValuesArray = [];
            $scope.AnswerOptionModel = { IdAnswerOption: 0, Leyend: null, Description: null, IdOptionType: null };
            $scope.OptionTypeModel = $scope.OptionTypesArray[0];
            $scope.shAnswerOptionCreate = !$scope.shAnswerOptionCreate;

        };

        $scope.saveAnswerOption = function () {
            $scope.AnswerOptionModel.IdOptionType = $scope.OptionTypeModel.idOptionType;
            if ($scope.verifyModel($scope.AnswerOptionModel)) {
                var model = {
                    IdAnswerOption: $scope.AnswerOptionModel.IdAnswerOption,
                    Leyend: $scope.AnswerOptionModel.Leyend,
                    Description: $scope.AnswerOptionModel.Description,
                    IdOptionType: $scope.AnswerOptionModel.IdOptionType,
                    IdStatus: true,
                    lstBaseAnswers: $scope.AnswerOptionValuesArray

                };

                $scope.SendToSave(model);

            }

        }

        $scope.SendToSave = function (model) {
            AnswerTypeService.SaveAnswerOption(model).then(function (result) {
                if (result.statusCode === 0) {
                    //alert(result.message);
                    $scope.success = { msg: result.message }
                    $('#myModalEditProduct').modal('show');
                    $scope.GetAllAnswerOption();
                    $scope.AnswerOptionArray.push({
                        idAnswerOption: result.idIdenttity,
                        leyend: $scope.AnswerOptionModel.Leyend,
                        description: $scope.AnswerOptionModel.Description,
                        idOptionType: $scope.AnswerOptionModel.IdOptionType,
                        idStatus: true,
                        lstBaseAnswers: $scope.AnswerOptionValuesArray
                    });

                    //$scope.cancelSaveAnswerOption();
                } else {
                    alert(result.message);
                }
            }, function (reason) {
                $log.error(reason)
            });

        };
        $scope.cerrarModal = function () {
            $scope.cancelSaveAnswerOption();
        };


    }

    return {
        link: link,
        controller: controller,
        restrict: 'ACE',
        replace: true,
        templateUrl: "/App/Modules/Administration/Catalogs/AnswerType/AnswerTypeDirectives/AnswerOptionCreateTemplate.html"

    }

}]);