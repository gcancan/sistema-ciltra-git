﻿app.controller('EstadosController', ['$scope', 'EstadosServices', function ($scope, EstadosServices) {

    $scope.arrayEstados;
    recet = function () {
        $scope.Estado = {
            idEstado: 0,
            nombre: '',
            pais: null
        }
    }
    recet();
    function getAllEstadosA() {
        EstadosServices.getAllEstados().then(function (httpData) {
            $scope.arrayEstados = [].concat(httpData);
        }, function (reason) {
            console.log("error" + reason);

        })
    };
    getAllEstadosA();

    $scope.saveEstado = function (data, Estado) {
        var update = false;
        if (data != null) {
            $scope.Estado = {
                idEstado: Estado.idEstado,
                nombre: data.nombre,
                pais: data.pais
            }
            update = true
        }
        //return;
        if (validar()) {

            //BaseQuestionServices.saveBaseQuestion($scope.Estado).then(function (httpData) {

            //    var res = httpData;
            //    console.log(res);
            //    if (res.statusCode == 0) {
            //        if (!update) {
            //            $scope.BaseQuestion.idBaseQuestion = res.idIdenttity
            //            $scope.arrayBaseQuestion.push($scope.BaseQuestion);
            //        }
            //        $scope.success = { msg: res.message }
            //        $('#myModalEditProduct').modal('show');

            //    }
            //    else {
            //        $scope.error = { msg: res.message, detalles: null }
            //        $('#myModalError').modal('show');
            //    }
            //    recet();

            //}, function (reason) {
            //    //console.log("error" + reason);
            //    $scope.error = { msg: reason, detalles: null }
            //    $('#myModalError').modal('show');
            //})
        }
        else {
            //Please Complete the fields below
            $scope.error = { msg: "Falto Proporcionar algunos valores", detalles: ["Nombre"] }
            $('#myModalError').modal('show');

        }

    };

    function validar() {
        if ($scope.Estado.nombre == undefined) {
            return false;
        }
        if ($scope.Estado.nombre.length <= 0) {
            return false;
        }
        return true;
    }


    $scope.closeModal = function () {
        $('#myModal').modal('toggle');
    }

    $scope.showGroup = function (user) {
        if (user.group && $scope.groups.length) {
            var selected = $filter('filter')($scope.groups, { id: user.group });
            return selected.length ? selected[0].text : 'Not set';
        } else {
            return user.groupName || 'Not set';
        }
    };

}]);