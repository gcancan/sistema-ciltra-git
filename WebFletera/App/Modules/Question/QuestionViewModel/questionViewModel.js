﻿app.controller('questionViewModel', ['$rootScope', '$scope', 'GetQuestionAnswerService', 'answerServices', function ($rootScope, $scope, GetQuestionAnswerService, answerServices, $filter) {

    var idProduct = $rootScope.productId;
    $scope.question = [];
    $scope.answer = [];
    $scope.lstTypeAnswer = [];
    $scope.typeAnswer = [];
    $scope.OpenAnswer = [];
    $scope.currentPage = 0;
    $scope.pageSize;
    $scope.contadorRadios = 0;
    $scope.arrayAnswer = [];

    function getQuestionsAnswer(idProduct) {
        GetQuestionAnswerService.getAquestionAnswer(idProduct).then(function (httpData) {
            $scope.QuestionAnswerArray = [].concat(httpData);
            for (var i in $scope.QuestionAnswerArray[0].listQuestionProduct) {
                $scope.pageSize = $scope.QuestionAnswerArray[0].questionsPerPage;
                for (var j in $scope.QuestionAnswerArray[0].listQuestionProduct[i].listAnswer) {


                    if ($scope.QuestionAnswerArray[0].listQuestionProduct[i].listAnswer[j].listOpenAnswer != null) {


                        for (var o in $scope.QuestionAnswerArray[0].listQuestionProduct[i].listAnswer[j].listOpenAnswer) {

                            var lstAnswerType = {
                                IdOpenAnswer: $scope.QuestionAnswerArray[0].listQuestionProduct[i].listAnswer[j].listOpenAnswer[o].idOpenAnswer,
                                placeHolder: $scope.QuestionAnswerArray[0].listQuestionProduct[i].listAnswer[j].listOpenAnswer[o].placeHolder,
                                option: 'TextBox'

                            }
                            $scope.typeAnswer.push(lstAnswerType);
                        }

                        lstAnswerType = {};
                    }
                    else {

                        for (var y in $scope.QuestionAnswerArray[0].listQuestionProduct[i].listAnswer[j].listAnswerOptionDTO) {

                            for (var z in $scope.QuestionAnswerArray[0].listQuestionProduct[i].listAnswer[j].listAnswerOptionDTO[y].lstBaseAnswers) {

                                var answer = {
                                    answer: $scope.QuestionAnswerArray[0].listQuestionProduct[i].listAnswer[j].listAnswerOptionDTO[y].lstBaseAnswers[z].answer,
                                    idBaseAnswer: $scope.QuestionAnswerArray[0].listQuestionProduct[i].listAnswer[j].listAnswerOptionDTO[y].lstBaseAnswers[z].idBaseAnswer
                                }
                                $scope.answer.push(answer);
                                answer = {};
                            }
                            var lstAnswerType = {
                                option: $scope.QuestionAnswerArray[0].listQuestionProduct[i].listAnswer[j].listAnswerOptionDTO[y].optionTypeName,
                                listAnswer: $scope.answer
                            }
                            $scope.answer = [];
                        }
                        $scope.typeAnswer.push(lstAnswerType);
                        lstAnswerType = {};
                    }
                }
                var objetoQuestion = {
                    idAnswerOption: $scope.QuestionAnswerArray[0].listQuestionProduct[i].listAnswer[j].idAnswerOption,
                    idBackPerAnswer: $scope.QuestionAnswerArray[0].listQuestionProduct[i].listAnswer[j].idBackPerAnswer,
                    question: $scope.QuestionAnswerArray[0].listQuestionProduct[i].question,
                    listAnswerType: $scope.typeAnswer,
                    selectedAnswer: null
                }
                $scope.typeAnswer = [];
                $scope.question.push(objetoQuestion);
            }

        }, function (reason) {
            console.log("Error" + reason);
        });
    }

    $scope.numberOfPages = function () {
        return Math.ceil($scope.question.length / $scope.pageSize);
    }

    getQuestionsAnswer(idProduct);

    $scope.addArray = function (idBackPerAnswer, type, index) {
        console.log(idBackPerAnswer + " " + type + " -->" + index.toString());
        answer = {
            idAnswer: 0,
            idBaseAnswer: '',
            idBackPerAnswer: idBackPerAnswer,
            type: type,
            index: index
        }
        $scope.arrayAnswer.push(answer);

    }

    $scope.updateArray = function (baseAnswer, idBackPerAnswer, type, index) {
        //console.log(baseAnswer.idBaseAnswer + '  ' + idBackPerAnswer);

        var i = findIndex(idBackPerAnswer, type)
        $scope.arrayAnswer.splice(i, 1, {
            idAnswer: 0,
            idBackPerAnswer: idBackPerAnswer,
            idBaseAnswer: baseAnswer.idBaseAnswer,
            type: type,
            index: index
        });
        console.log($scope.arrayAnswer[i].idBackPerAnswer + " " + $scope.arrayAnswer[i].idBaseAnswer + " " + $scope.arrayAnswer[i].type);
    }

    $scope.updateArrayCheck = function (baseAnswer, idBackPerAnswer, event, type, index) {
        var check = (event.target.checked)

        if (check) {
            var i = findIndexCheck(idBackPerAnswer, "", index)
            $scope.arrayAnswer.splice(i, 1, {
                idAnswer: 0,
                idBackPerAnswer: idBackPerAnswer,
                idBaseAnswer: baseAnswer.idBaseAnswer,
                type: 'checkbox'
            });
        }
        else {
            var i = findIndexCheck(idBackPerAnswer, baseAnswer.idBaseAnswer, index)
            $scope.arrayAnswer.splice(i, 1, {
                idAnswer: 0,
                idBackPerAnswer: idBackPerAnswer,
                idBaseAnswer: "",
                type: 'checkbox'
            });
        }
        console.log($scope.arrayAnswer[i].idBackPerAnswer + " " + $scope.arrayAnswer[i].idBaseAnswer + " " + $scope.arrayAnswer[i].type);
    }
    findIndex = function (idBackPerAnswer, type) {
        for (var i = 0; i < $scope.arrayAnswer.length; i++) {
            arrary = $scope.arrayAnswer[i];
            if (arrary.idBackPerAnswer == idBackPerAnswer && arrary.type == type) {
                return i;
            }
        }

        //for (var i = 0; i < $scope.arrayAnswer.length; i++) {
        //    arrary = $scope.arrayAnswer[i];
        //    if (arrary.idBackPerAnswer == idBackPerAnswer) {
        //        return i;
        //    }
        //}
    }
    findIndexCheck = function (idBackPerAnswer, idBaseAnswer, index) {
        for (var i = 0; i < $scope.arrayAnswer.length; i++) {
            arrary = $scope.arrayAnswer[i];
            if (arrary.idBackPerAnswer == idBackPerAnswer && arrary.idBaseAnswer == idBaseAnswer) {
                return i;
                break;
            }
        }
    }

    $scope.saveAnswer = function () {
        console.log($scope.arrayAnswer)
        //return;
        if (validar()) {
            answerServices.saveAnswer($scope.arrayAnswer).then(function (httpData) {

                var res = httpData;
                //console.log(res);
                alert(res.message);
            }, function (reason) {
                console.log("error" + reason);

            })
        }
    }



}]);


app.filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});