﻿using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcesosComercial
{
    class GuardarProveedor
    {
        public static bool guardarNewProveedor(Proveedor proveedor)
        {
            //Declaraciones.ins
            int result = Declaraciones.fBuscaCteProv("P" + proveedor.idProveedor.ToString());
            //MessageBox.Show(result.ToString());
            if (hayError(result))
            {
                if (result == 3)
                {
                    if (hayError(Declaraciones.fInsertaCteProv()))
                    {
                        return false;
                    }
                    else
                    {
                        Declaraciones.fSetDatoCteProv("CCODIGOCLIENTE", "P" + proveedor.idProveedor.ToString());
                        Declaraciones.fSetDatoCteProv("CRAZONSOCIAL", proveedor.razonSocial);
                        Declaraciones.fSetDatoCteProv("CFECHAALTA", DateTime.Now.ToString("MM/dd/yyyy"));
                        Declaraciones.fSetDatoCteProv("CRFC", proveedor.RFC);
                        Declaraciones.fSetDatoCteProv("CTIPOCLIENTE", "3");//
                        Declaraciones.fSetDatoCteProv("CBANVENTACREDITO", "1");//
                        Declaraciones.fSetDatoCteProv("CBANDOMICILIO", "1");//
                        Declaraciones.fSetDatoCteProv("CLISTAPRECIOCLIENTE", "1");//CESTATUS
                        Declaraciones.fSetDatoCteProv("CESTATUS", "1");
                        // Tipo Agente 1=ventas 2= ventas/cobro 3= cobro

                        if (hayError(Declaraciones.fGuardaCteProv()))
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        internal static bool hayError(int error)
        {
            bool resultado;

            if (error == 3)
            {
                return true;
            }
            else if (error != 0)
            {
                Declaraciones.MuestraError(error);
                resultado = true;
            }
            else
            {
                resultado = false;
            }
            return resultado;
        }
    }
}
