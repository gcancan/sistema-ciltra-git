﻿using Core.Models;
using Core.Utils;
using CoreFletera.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProcesosComercial
{
    public class RealizarProcesos
    {
        Parametros parametros = null;
        public RealizarProcesos(ref bool resp)
        {
            var path = Application.StartupPath;

            string archivo = path + @"\config.ini";
            _empresa = new Util().Read("CONECTION_STRING_COMERCIAL_ATLAS", "DB_NAME", archivo);
            resp = iniciaSDK();
        }
        public RealizarProcesos(ref bool resp, Parametros parametros)
        {
            this.parametros = parametros;
            var path = Application.StartupPath;

            string archivo = path + @"\config.ini";
            _empresa = new Util().Read("CONECTION_STRING_COMERCIAL_ATLAS", "DB_NAME", archivo);
            resp = iniciaSDK();
        }
        string _empresa = string.Empty;

        public bool generarOrdenCompra(string empresa, OrdenCompra ordenCompra)
        {
            _empresa = empresa;
            bool resp = abrirEmpresa(_empresa);
            if (resp)
            {
                var r = realizarOrdenCompra(ordenCompra);
                cerrar();
                return r;
            }
            else
            {
                cerrar();
                return false;
            }
        }
        public List<OrdenCompra> newListOrdenCompra = new List<OrdenCompra>();
        public bool generarOrdenCompra(List<OrdenCompra> ListOrdenCompra)
        {
            newListOrdenCompra = new List<OrdenCompra>();
            foreach (var item in ListOrdenCompra)
            {
                //_empresa = new Util().Read("CONECTION_STRING_COMERCIAL_ATLAS", "DB_NAME", archivo);
                bool resp = abrirEmpresa(_empresa);
                if (resp)
                {
                    var r = realizarOrdenCompra(item);
                    Declaraciones.fCierraEmpresa();
                    if (!r)
                    {
                        cerrar();
                        return false;
                    }
                }
                else
                {
                    cerrar();
                    return false;
                }
            }
            cerrar();
            return true;
        }

        public bool generarSalidaEntradasAlmacen(OrdenTaller orden, List<ProductosCOM> listaProductos, Personal personalSolicita, Parametros parametros, ref PreSalidaAlmacen preSalidaAlm)
        {
            this.parametros = parametros;
            bool resp = abrirEmpresa(_empresa);
            if (resp)
            {
                var r = realizarSalidaAlmacen(orden, listaProductos, personalSolicita, ref preSalidaAlm);
                Declaraciones.fCierraEmpresa();
                if (!r)
                {
                    cerrar();
                    return false;
                }
            }
            else
            {
                cerrar();
                return false;
            }

            cerrar();
            return true;
        }
         
        private bool realizarSalidaAlmacen(OrdenTaller orden, List<ProductosCOM> listaProductos, Personal personaSolicita, ref PreSalidaAlmacen preSalidaAlm)
        {
            DateTime fecha = DateTime.Now;
            try
            {
                preSalidaAlm = new PreSalidaAlmacen
                {
                    idPreSalida = 0,
                    fecha = fecha,
                    idOrdenTrabajo = orden.idOrdenTrabajo,
                    listaProductos = listaProductos,
                    idEmpleadoSolicita = personaSolicita.clave,
                    estatus = "ENT"
                };

                Declaraciones.tDocumento ltDocto = new Declaraciones.tDocumento();
                int lIdDocto = 0;
                double lFolio = 0;
                StringBuilder lSerieDocto = new StringBuilder(12);
                lSerieDocto.Append("");
                if (hayError(Declaraciones.fSiguienteFolio(parametros.idConceptoSalAlm, lSerieDocto, ref lFolio)))
                {
                    return false;
                }
                else
                {
                    ltDocto.aCodConcepto = parametros.idConceptoSalAlm;// parametros.idConceptoSalAlm;
                    //ltDocto.aSerie = lSerieDocto.ToString();
                    //ltDocto.aFolio = lFolio;
                    ltDocto.aFecha = fecha.ToString("MM/dd/yyyy");
                    ltDocto.aCodigoCteProv = "";
                    ltDocto.aCodigoAgente = "";
                    ltDocto.aSistemaOrigen = 0;
                    ltDocto.aNumMoneda = 1;
                    ltDocto.aTipoCambio = 1;
                    ltDocto.aAfecta = 0;
                    //ltDocto.aSistemaOrigen = 205;
                    //ltDocto.aNumMoneda = 1;
                    //ltDocto.aTipoCambio = 1;
                    //ltDocto.aAfecta = 1;
                    //ltDocto.aGasto1 = 0;
                    //ltDocto.aImporte = 1;
                    if (hayError(Declaraciones.fAltaDocumento(ref lIdDocto, ref ltDocto)))
                    {
                        return false;
                    }
                    else
                    {

                        //MessageBox.Show(lSerieDocto.ToString());
                        bool r = false;
                        foreach (var item in listaProductos)
                        {
                            Declaraciones.tMovimiento ltMovto = new Declaraciones.tMovimiento();
                            int LIdMovto = 0;
                            ltMovto.aCodAlmacen = parametros.idAlmacenSal;
                            ltMovto.aConsecutivo = 1;
                            ltMovto.aCodProdSer = item.codigo;
                            ltMovto.aUnidades = double.Parse(item.cantidad.ToString());
                            //ltMovto.aPrecio = (double)item.precio;

                            if (hayError(Declaraciones.fAltaMovimiento(lIdDocto, ref LIdMovto, ref ltMovto)))
                            {
                                r = false;
                                break;
                            }
                            else
                            {
                                r = true;
                            }
                        }

                        preSalidaAlm.CIDDOCUMENTO_SAL = lIdDocto;
                        preSalidaAlm.CFOLIO_SAL = lFolio;
                        preSalidaAlm.CSERIEDOCUMENTO_SAL = lSerieDocto.ToString();

                        return realizarEntradaAlmacen(listaProductos, ref preSalidaAlm);
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return false;
            }
        }

        private bool realizarEntradaAlmacen(List<ProductosCOM> listaProductos, ref PreSalidaAlmacen preSalidaAlm)
        {            
            try
            {   
                Declaraciones.tDocumento ltDocto = new Declaraciones.tDocumento();
                int lIdDocto = 0;
                double lFolio = 0;
                StringBuilder lSerieDocto = new StringBuilder(12);
                lSerieDocto.Append("");
                if (hayError(Declaraciones.fSiguienteFolio(parametros.idConceptoEntAlm, lSerieDocto, ref lFolio)))
                {
                    return false;
                }
                else
                {
                    ltDocto.aCodConcepto = parametros.idConceptoEntAlm;
                    //ltDocto.aSerie = lSerieDocto.ToString();
                    //ltDocto.aFolio = lFolio;
                    ltDocto.aFecha = preSalidaAlm.fecha.ToString("MM/dd/yyyy");
                    ltDocto.aSistemaOrigen = 205;
                    ltDocto.aNumMoneda = 1;
                    ltDocto.aTipoCambio = 1;
                    ltDocto.aAfecta = 1;

                    if (hayError(Declaraciones.fAltaDocumento(ref lIdDocto, ref ltDocto)))
                    {
                        return false;
                    }
                    else
                    {

                        //MessageBox.Show(lSerieDocto.ToString());
                        bool r = false;
                        foreach (var item in listaProductos)
                        {
                            Declaraciones.tMovimiento ltMovto = new Declaraciones.tMovimiento();
                            int LIdMovto = 0;
                            ltMovto.aCodAlmacen = parametros.idAlmacenEnt;
                            ltMovto.aConsecutivo = 1;
                            ltMovto.aCodProdSer = item.codigo;
                            ltMovto.aUnidades = double.Parse(item.cantidad.ToString());
                            //ltMovto.aPrecio = (double)item.precio;

                            if (hayError(Declaraciones.fAltaMovimiento(lIdDocto, ref LIdMovto, ref ltMovto)))
                            {
                                r = false;
                                break;
                            }
                            else
                            {
                                r = true;
                            }
                        }

                        preSalidaAlm.CIDDOCUMENTO_ENT = lIdDocto;
                        preSalidaAlm.CFOLIO_ENT = lFolio;
                        preSalidaAlm.CSERIEDOCUMENTO_ENT = lSerieDocto.ToString();
                        return r;
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return false;
            }
        }

        public bool cerrar()
        {
            try
            {
                Declaraciones.fCierraEmpresa();
                Declaraciones.fTerminaSDK();
                KeySistema.Close();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        RegistryKey KeySistema = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Computación en Acción, SA CV\CONTPAQ I COMERCIAL");

        private int lResult = 0;
        public bool iniciaSDK()
        {
            try
            {
                object lEntrada = KeySistema.GetValue("DirectorioBase");

                lResult = Declaraciones.SetCurrentDirectory(lEntrada.ToString());

                if (lResult != 0)
                {
                    var y = Declaraciones.fInicializaSDK();
                    var s = Declaraciones.fSetNombrePAQ("CONTPAQ I COMERCIAL");
                    return true;
                }
                else
                {
                    //txtMensajes.Text = "Hubo un problema";
                    Declaraciones.MuestraError(lResult);
                    return false;
                }
            }
            catch (System.ArithmeticException ex)
            {
                MessageBox.Show("Ocurrio un error al tratar de iniciar el SDK." + Environment.NewLine
                    + ex.Message, "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return false;
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrio un error al tratar de iniciar el SDK." + Environment.NewLine
                    + e.Message, "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return false;
            }
        }//Fin método Abrir Conexión 

        string path = Application.StartupPath;
        #region Abrir Empresa
        private bool abrirEmpresa(string empresa)
        {
            lResult = Declaraciones.fAbreEmpresa(@"C:\Compac\Empresas\" + empresa);
            if (lResult == 0)
            {
                return true;
            }
            else
            {
                Declaraciones.MuestraError(lResult);
                return false;
            }
        }//Fin abrir empresa 
        #endregion
        public int folioOrdenCompra;
        public double folio;
        public string serie;
        private bool realizarOrdenCompra(OrdenCompra ordenCompra)
        {
            try
            {
                //MessageBox.Show(ordenCompra.proveedor.razonSocial);
                foreach (var item in ordenCompra.listaDetalles)
                {
                    if (!ValidarProducto.validarExistenProductos(item, _empresa, ordenCompra.isLLanta))
                    {
                        ImprimirMensaje.imprimir(new OperationResult { valor = 1, mensaje = "Ocurrio un error al dar de alta al producto" });
                        return false;
                    }
                }

                if (!GuardarProveedor.guardarNewProveedor(ordenCompra.proveedor))
                {
                    return false;
                }

                Declaraciones.tDocumento ltDocto = new Declaraciones.tDocumento();
                int lIdDocto = 0;
                double lFolio = 0;
                StringBuilder lSerieDocto = new StringBuilder(12);
                lSerieDocto.Append("");
                if (hayError(Declaraciones.fSiguienteFolio("19", lSerieDocto, ref lFolio)))
                {
                    return false;
                }
                else
                {
                    ltDocto.aCodConcepto = "19";
                    ltDocto.aSerie = lSerieDocto.ToString();
                    //ltDocto.aFolio = lFolio;
                    ltDocto.aFecha = ordenCompra.fecha.ToString("MM/dd/yyyy");
                    ltDocto.aCodigoCteProv = "P" + ordenCompra.proveedor.idProveedor.ToString();
                    //ltDocto.aCodigoAgente = chofer;
                    ltDocto.aSistemaOrigen = 205;
                    ltDocto.aNumMoneda = 1;
                    ltDocto.aTipoCambio = 1;
                    ltDocto.aImporte = 0;
                    ltDocto.aDescuentoDoc1 = 0;
                    ltDocto.aDescuentoDoc2 = 0;
                    ltDocto.aAfecta = 1;
                    //ltDocto.aReferencia = ordenCompra.empresa.nombre;

                    if (hayError(Declaraciones.fAltaDocumento(ref lIdDocto, ref ltDocto)))
                    {
                        return false;
                    }
                    else
                    {

                        //MessageBox.Show(lSerieDocto.ToString());
                        bool r = false;
                        foreach (var item in ordenCompra.listaDetalles)
                        {
                            Declaraciones.tMovimiento ltMovto = new Declaraciones.tMovimiento();
                            int LIdMovto = 0;
                            ltMovto.aCodAlmacen = "1";
                            ltMovto.aConsecutivo = 1;
                            ltMovto.aCodProdSer = ordenCompra.isLLanta ? ("LL" + item.llantaProducto.idLLantasProducto) : item.producto.codigo.ToString();
                            ltMovto.aUnidades = double.Parse(item.cantidad.ToString());
                            ltMovto.aPrecio = (double)item.costo;
                            //ltMovto.aCosto = (double)item.costo;

                            if (hayError(Declaraciones.fAltaMovimiento(lIdDocto, ref LIdMovto, ref ltMovto)))
                            {
                                r = false;
                                break;
                            }
                            else
                            {
                                r = true;
                            }
                        }
                        folioOrdenCompra = lIdDocto;
                        folio = lFolio;
                        serie = lSerieDocto.ToString();

                        ordenCompra.CIDDOCUMENTO = folioOrdenCompra;
                        ordenCompra.CFOLIO = folio;
                        ordenCompra.CSERIEDOCUMENTO = serie;
                        newListOrdenCompra.Add(ordenCompra);
                        return r;
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return false;
            }
        }
        private bool hayError(int error)
        {
            if (error != 0)
            {
                Declaraciones.MuestraError(error);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
