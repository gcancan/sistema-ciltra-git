﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProcesosComercial
{
    public static class ImprimirMensaje
    { //Exception
        public static void imprimir(OperationResult resp)
        {
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    MessageBox.Show(resp.mensaje, "Exito", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    break;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
                case ResultTypes.recordNotFound:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
                default:
                    MessageBox.Show(resp.mensaje, "Tipo de Resultado Desconocido", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
            }
        }

        public static void imprimir(Exception ex)
        {
            imprimir(new OperationResult { valor = 1, mensaje = ex.Message });
        }
    }
}
