﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CoreFletera.Models;
using Core.Models;
using CoreFletera.Interfaces;
using Core.Interfaces;

namespace ProcesosComercial
{
    public partial class ProcesosForm : MetroFramework.Forms.MetroForm
    {
        Parametros parametros = null;
        public ProcesosForm()
        {
            InitializeComponent();
        }
        public ProcesosForm(Parametros parametros)
        {
            this.parametros = parametros;
            InitializeComponent();
        }
        List<Actividad> listaActividad = new List<Actividad>();
        public ProcesosForm(List<Actividad> listaActividad)
        {
            this.listaActividad = listaActividad;
            Text = "SINCRONIZAR ACTIVADES.";
            InitializeComponent();
        }
        public OperationResult respuesta = new OperationResult();
        bool sincronizar = false;
        public OperationResult sincronizarActividad()
        {
            try
            {
                Show();
                OperationResult resp = new OperationResult();
                foreach (Actividad actividad in listaActividad)
                {
                    Actividad newActividad = actividad;
                    lblMensaje.Text = string.Format("Sincronizando... {0}", actividad.nombre);
                    resp = new ActividadSvc().saveActividad(ref newActividad);
                    if (resp.typeResult != ResultTypes.success)
                    {
                        respuesta = resp;
                        return resp;
                    }
                }
                respuesta = resp;
                return resp;
            }
            catch (Exception ex)
            {
                respuesta = new OperationResult(ex);
                return respuesta;
            }
            finally
            {
                Close();
            }
        }

        public OperationResult realizarSalidaEntradaAlmacen(OrdenTaller orden, List<ProductosCOM> listaProductos, Personal personalSolicita, string usuario)
        {
            try
            {
                lblMensaje.Text = "REALIZANDO LA SALIDA DEL ALAMACEN";
                this.Show();
                bool resp = false;
                var operacion = new RealizarProcesos(ref resp);
                if (!resp)
                {
                    return new OperationResult() { valor = 1, mensaje = "" };
                }
                else
                {
                    try
                    {
                        PreSalidaAlmacen preSalidaAlm = new PreSalidaAlmacen()
                        {
                            fecha = DateTime.Now,
                            CFOLIO_ENT = 0,
                            CFOLIO_SAL = 0,
                            CIDDOCUMENTO_ENT = 0,
                            CIDDOCUMENTO_SAL = 0,
                            CSERIEDOCUMENTO_ENT = "",
                            CSERIEDOCUMENTO_SAL = "",
                            estatus = "PEN",
                            idEmpleadoSolicita = personalSolicita.clave,
                            idOrdenTrabajo = orden.idOrdenTrabajo,
                            listaProductos = listaProductos,
                            total = 0
                        };
                        var guardo = operacion.generarSalidaEntradasAlmacen(orden, listaProductos, personalSolicita, parametros, ref preSalidaAlm);
                        if (guardo)
                        {
                            return new OperacionSvc().savePreSalidaAlm(orden, preSalidaAlm, listaProductos, personalSolicita, usuario);
                            return new OperationResult { valor = 0, mensaje = "Operacion guardad Con exito" };
                        }
                        else
                        {
                            return new OperationResult { valor = 1, mensaje = "" };
                        }
                    }
                    catch (Exception ex)
                    {
                        return new OperationResult(ex);
                    }
                }

            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Close();
            }
        }

        public OperationResult realizarPreOrdenesCompra(OrdenTaller orden, List<ProductosCOM> listaProductos, Personal personalSolicita, string usuario)
        {
            try
            {
                lblMensaje.Text = "REALIZANDO PRE ORDENES DE COMPRA";
                this.Show();

                try
                {
                    PreSalidaAlmacen preSalidaAlm = new PreSalidaAlmacen()
                    {
                        fecha = DateTime.Now,
                        CFOLIO_ENT = 0,
                        CFOLIO_SAL = 0,
                        CIDDOCUMENTO_ENT = 0,
                        CIDDOCUMENTO_SAL = 0,
                        CSERIEDOCUMENTO_ENT = "",
                        CSERIEDOCUMENTO_SAL = "",
                        estatus = "PEN",
                        idEmpleadoSolicita = personalSolicita.clave,
                        idOrdenTrabajo = orden.idOrdenTrabajo,
                        listaProductos = listaProductos,
                        total = 0
                    };
                    return new OperacionSvc().savePreSalidaAlm(orden, preSalidaAlm, listaProductos, personalSolicita, usuario);
                }
                catch (Exception ex)
                {
                    return new OperationResult(ex);
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Close();
            }
        }

        public OperationResult realizarOrdenesCompra(List<OrdenCompra> listaOrdenCom, CotizacionSolicitudOrdCompra cotizacion = null)
        {
            lblMensaje.Text = "CREANDO ORDENES DE COMPRA";
            this.Show();
            bool resp = false;
            RealizarProcesos classOrden = new RealizarProcesos(ref resp);
            if (!resp)
            {
                return new OperationResult { valor = 2, mensaje = "ERROR AL INICIAR LA INSTANCIA DEL COMERCIAL" };
            }
            else
            {
                try
                {
                    var guardo = classOrden.generarOrdenCompra(listaOrdenCom);
                    if (guardo)
                    {
                        var resp2 = new OrdenCompraSvc().saveOrdenCompraLlantas(classOrden.newListOrdenCompra);
                        if (resp2.typeResult == ResultTypes.success)
                        {
                            var respCoti = new CotizacionesSvc().saveCotizacion(ref cotizacion);

                            List<OrdenCompra> listaNueva = (List<OrdenCompra>)resp2.result;
                            resp2.result = listaNueva;



                        }
                        classOrden.cerrar();
                        return resp2;
                    }
                    else
                    {
                        classOrden.cerrar();
                        return new OperationResult { valor = 1, mensaje = "ERROR AL GUARDAR LA OPERACIÓN EN EL COMERCIAL" };
                    }

                }
                catch (Exception ex)
                {
                    return new OperationResult { valor = 1, mensaje = ex.Message };
                }
                finally
                {
                    //Cursor = Cursors.Arrow;
                    this.Close();
                }
            }
        }

        private void ProcesosForm_Load(object sender, EventArgs e)
        {
            if (sincronizar)
            {
                sincronizarActividades();
            }
        }

        private void sincronizarActividades()
        {

        }
    }
}
