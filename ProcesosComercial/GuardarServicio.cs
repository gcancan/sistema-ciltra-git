﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProcesosComercial
{
    public static class GuardarServicio
    {
        internal static Declaraciones.tProduto ltProducto = new Declaraciones.tProduto();
        internal static int lidProducto = 0;
        public static bool guardarSDK(Zona zona, eAction action)
        {
            try
            {
                ltProducto.cCodigoProducto = zona.clave.ToString();
                ltProducto.cNombreProducto = zona.descripcion;
                ltProducto.cTextoExtra1 = zona.descripcion;
                ltProducto.cFechaAltaProducto = DateTime.Now.ToString("MM/dd/yyyy");
                //Campos para poder dar Alta Unidad de Medida
                //ltProducto.cCodigoUnidadBase = "";
                ltProducto.cControlExistencia = 0;
                ltProducto.cTipoProducto = 3;

                //MessageBox.Show(action.ToString());
                switch (action)
                {
                    case eAction.NUEVO:
                        if (hayError(Declaraciones.fAltaProducto(ref lidProducto, ref ltProducto)))
                        {
                            return true;
                        }
                        else
                        {
                            MessageBox.Show("Servicio agregado al sistema CONTPAQ I COMERCIAL",
                                "Alta del servicio.",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                            return false;
                        }
                    case eAction.EDITAR:
                        int lResul = Declaraciones.fActualizaProducto(zona.clave.ToString(), ref ltProducto);
                        if (lResul == 3)
                        {
                            return guardarSDK(zona, eAction.NUEVO);
                        }
                        if (hayError(lResul))
                        {
                            //MessageBox.Show(lResul.ToString());                            
                            return true;
                        }
                        else
                        {
                            MessageBox.Show("Servicio actualizado en el sistema CONTPAQ I COMERCIAL",
                                "Editar el servicio.",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                            return false;
                        }
                }

                return false;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }

        internal static bool guardarSDKSinMensaje(ProductosCOM producto, eAction action)
        {
            try
            {
                ltProducto.cCodigoProducto = producto.codigo;
                ltProducto.cNombreProducto = producto.nombre;
                ltProducto.cTextoExtra1 = producto.nombre;
                ltProducto.cFechaAltaProducto = DateTime.Now.ToString("MM/dd/yyyy");
                //Campos para poder dar Alta Unidad de Medida
                //ltProducto.cCodigoUnidadBase = "";
                ltProducto.cControlExistencia = 0;
                ltProducto.cTipoProducto = 1;

                //MessageBox.Show(action.ToString());
                switch (action)
                {
                    case eAction.NUEVO:
                        //MessageBox.Show("ALTA: " + producto.nombre);
                        if (hayError(Declaraciones.fAltaProducto(ref lidProducto, ref ltProducto)))
                        {

                            return true;
                        }
                        else
                        {
                            //MessageBox.Show("ALTA: " + producto.nombre + "alta");
                            //MessageBox.Show("Servicio agregado al sistema CONTPAQ I COMERCIAL",
                            //    "Alta del servicio.",
                            //    MessageBoxButton.OK,
                            //    MessageBoxImage.Information);
                            return false;
                        }
                }

                return false;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }

        internal static bool guardarSDKSinMensaje(LLantasProducto llantaProducto, eAction action)
        {
            try
            {
                MessageBox.Show("ALTA:  LL" + llantaProducto.idLLantasProducto.ToString());
                ltProducto.cCodigoProducto = "LL" + llantaProducto.idLLantasProducto.ToString();
                ltProducto.cNombreProducto = "LLANTA " + llantaProducto.marca.descripcion + " " + llantaProducto.diseño.descripcion + " " + llantaProducto.medida.ToString();
                ltProducto.cTextoExtra1 = llantaProducto.marca.descripcion;
                ltProducto.cFechaAltaProducto = DateTime.Now.ToString("MM/dd/yyyy");
                //Campos para poder dar Alta Unidad de Medida
                //ltProducto.cCodigoUnidadBase = "";
                ltProducto.cControlExistencia = 0;
                ltProducto.cTipoProducto = 1;

                //MessageBox.Show(action.ToString());
                switch (action)
                {
                    case eAction.NUEVO:
                        if (hayError(Declaraciones.fAltaProducto(ref lidProducto, ref ltProducto)))
                        {
                            //MessageBox.Show("ALTA:  LL" + llantaProducto.idLLantasProducto.ToString());
                            return true;
                        }
                        else
                        {
                            //MessageBox.Show("Servicio agregado al sistema CONTPAQ I COMERCIAL",
                            //    "Alta del servicio.",
                            //    MessageBoxButton.OK,
                            //    MessageBoxImage.Information);
                            return false;
                        }
                }

                return false;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }

        public static bool guardarSDKSinMensaje(Zona zona, eAction action)
        {
            try
            {
                ltProducto.cCodigoProducto = zona.clave.ToString();
                ltProducto.cNombreProducto = zona.descripcion;
                ltProducto.cTextoExtra1 = zona.descripcion;
                ltProducto.cFechaAltaProducto = DateTime.Now.ToString("MM/dd/yyyy");
                //Campos para poder dar Alta Unidad de Medida
                //ltProducto.cCodigoUnidadBase = "";
                ltProducto.cControlExistencia = 0;
                ltProducto.cTipoProducto = 3;

                //MessageBox.Show(action.ToString());
                switch (action)
                {
                    case eAction.NUEVO:
                        if (hayError(Declaraciones.fAltaProducto(ref lidProducto, ref ltProducto)))
                        {
                            return true;
                        }
                        else
                        {
                            //MessageBox.Show("Servicio agregado al sistema CONTPAQ I COMERCIAL",
                            //    "Alta del servicio.",
                            //    MessageBoxButton.OK,
                            //    MessageBoxImage.Information);
                            return false;
                        }
                    case eAction.EDITAR:
                        int lResul = Declaraciones.fActualizaProducto(zona.clave.ToString(), ref ltProducto);
                        if (lResul == 3)
                        {
                            return guardarSDK(zona, eAction.NUEVO);
                        }
                        if (hayError(lResul))
                        {
                            //MessageBox.Show(lResul.ToString());                            
                            return true;
                        }
                        else
                        {
                            //MessageBox.Show("Servicio actualizado en el sistema CONTPAQ I COMERCIAL",
                            //    "Editar el servicio.",
                            //    MessageBoxButton.OK,
                            //    MessageBoxImage.Information);
                            return false;
                        }
                }

                return false;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }

        public static bool eliminarServicio(Zona zona)
        {
            if (hayError(Declaraciones.fBuscaProducto(zona.clave.ToString())))
            {
                return false;
            }
            else
            {
                if (hayError(Declaraciones.fEliminarProducto(zona.clave.ToString())))
                {
                    return true;
                }
                else
                {
                    MessageBox.Show("Servicio eliminado del sistema CONTPAQ I COMERCIAL",
                                "Editar el servicio.",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);

                    return false;
                }
            }
        }

        internal static bool hayError(int error)
        {
            bool resultado;

            if (error != 0)
            {
                Declaraciones.MuestraError(error);
                resultado = true;
            }
            else
            {
                resultado = false;
            }
            return resultado;
        }
    }
}
