﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class FoliosDinero
    {
        public int FolioDin { get; set; }
        public DateTime Fecha { get; set; }
        public int idEmpleadoEnt { get; set; }
        public string NombreEnt { get; set; }
        public string Estatus { get; set; }
        public int idEmpleadoRec { get; set; }
        public string NombreRec { get; set; }
        public string Concepto { get; set; }
        public int idEmpresa { get; set; }
        public string RazonSocial { get; set; }
        public int idTipoFolDin { get; set; }
        public string DescripcionFolDin { get; set; }
        public decimal Importe { get; set; }
        public decimal ImporteEnt { get; set; }
        public decimal ImporteComp { get; set; }
        public int FolioDinPadre { get; set; }
        public int idLiquidacion { get; set; }
        public int NumGuiaId { get; set; }
    }
}
