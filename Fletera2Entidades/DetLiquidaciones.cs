﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class DetLiquidaciones
    {
        public int idDetLiq { get; set; }
        public int idLiquidacion { get; set; }
        public string Tipo { get; set; }
        public int NoDocumento { get; set; }
        public string Descripcion { get; set; }
        public decimal Importe { get; set; }
        public decimal IVA { get; set; }
        public bool EsAbono { get; set; }
    }
}
