﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CatLLantasProducto
    {
        public int idProductoLlanta { get; set; }
        public int idMarca { get; set; }
        public string NombreMarca { get; set; }
        public int idDisenio { get; set; }
        public string Descripcion { get; set; }
        public string Medida { get; set; }
        public decimal Profundidad { get; set; }
        public bool Activo { get; set; }

    }
}
