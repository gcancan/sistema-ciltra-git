﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CabGuia
    {
        public int NumGuiaId { get; set; }
        public string SerieGuia { get; set; }
        public int NumViaje { get; set; }
        public int idCliente { get; set; }
        public string NomCliente { get; set; }
        public int IdEmpresa { get; set; }
        public DateTime FechaHoraViaje { get; set; }
        public int idOperador { get; set; }
        public string idTractor { get; set; }
        public string idRemolque1 { get; set; }
        public string idDolly { get; set; }
        public string idRemolque2 { get; set; }
        public DateTime FechaHoraFin { get; set; }
        public string EstatusGuia { get; set; }
        public int UserViaje { get; set; }
        public int UserFinCancela { get; set; }
        public string MotivoCancelacion { get; set; }
        public string TipoViaje { get; set; }
        public decimal tara { get; set; }
        public string NoSerie { get; set; }
        public int NoFolio { get; set; }
        public decimal km { get; set; }
        public DateTime fechaBachoco { get; set; }
        public decimal tara2 { get; set; }
        public bool externo { get; set; }
        public int NumLiquidacion { get; set; }
    }
}
