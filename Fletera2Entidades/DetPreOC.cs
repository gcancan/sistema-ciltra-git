﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class DetPreOC
    {
        public int IdPreOC { get; set; }
        public int idProducto { get; set; }
        public string CCODIGOPRODUCTO { get; set; }
        public string CNOMBREPRODUCTO { get; set; }
        public int idUnidadProd { get; set; }
        public decimal CantidadSol { get; set; }
        public decimal CantSinCosto { get; set; }
        public decimal CantFaltante { get; set; }
        public decimal Precio { get; set; }
        public decimal PorcIVA { get; set; }
        public decimal Desc1 { get; set; }
        public decimal Desc2 { get; set; }
        public bool Inserta { get; set; }
        public bool Seleccionado { get; set; }
        public decimal CantidadEnt { get; set; }

        public decimal Existencia { get; set; }
        

    }
}
