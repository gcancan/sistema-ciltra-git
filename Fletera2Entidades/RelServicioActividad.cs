﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class RelServicioActividad
    {
        public int idServicio { get; set; }
        public int idActividad { get; set; }
        public bool Activo { get; set; }

    }
}
