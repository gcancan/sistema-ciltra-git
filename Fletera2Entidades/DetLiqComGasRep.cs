﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class DetLiqComGasRep
    {
        public int idLiquidacion { get; set; }
        public int Folio { get; set; }
        public string NoDocumento { get; set; }
        public string NomGasto { get; set; }
        public decimal Importe { get; set; }
        public decimal iva { get; set; }
        public decimal Total { get; set; }
    }
}
