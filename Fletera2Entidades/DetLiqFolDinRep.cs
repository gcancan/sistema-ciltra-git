﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class DetLiqFolDinRep
    {
        public int idLiquidacion { get; set; }
        public int Folio { get; set; }
        public int idEmpleadoEnt { get; set; }
        public string NomEntrega { get; set; }
        public string Descripcion { get; set; }
        public decimal Importe { get; set; }
        public decimal iva { get; set; }
        public decimal Total { get; set; }
            
    }
}
