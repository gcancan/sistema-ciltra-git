﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CatTipoOrdServ
    {
        //DETERMINA LOS DIFERENTE TIPOS DE ORDEN DE SERVICIO (TALLER,LLANTAS,COMBUSTIBLE, LAVADERO, ETC)
        public int idTipOrdServ { get; set; }
        public string NomTipOrdServ { get; set; }
        public int NumOrden { get; set; }
        public bool bOrdenTrab { get; set; }

    }
}
