﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class DOSInsumosFAC
    {
        public int idProducto { get; set; }
        public string CNOMBREPRODUCTO { get; set; }
        public string CCODIGOPRODUCTO { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Precio { get; set; }
        public decimal Costo { get; set; }
        public string Tipo { get; set; }
    }
}
