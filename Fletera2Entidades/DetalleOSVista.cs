﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class DetalleOSVista
    {
        public bool Seleccionado { get; set; }
        public int idOrdenSer { get; set; }
        public int idOrdenTrabajo { get; set; }
        public string Actividad { get; set; }
        public string Estatus { get; set; }
        public int idPersonalResp { get; set; }
        public string NomResponsable { get; set; }
        public int idPersonalAyu1 { get; set; }
        public string NomAyudante1 { get; set; }
        public int idPersonalAyu2 { get; set; }
        public string NomAyudante2 { get; set; }
        public decimal DuracionActHr { get; set; }
        public int CIDPRODUCTO_SERV { get; set; }
        public string CCODIGOPRODUCTO_SERV { get; set; }
        public string CNOMBREPRODUCTO_SERV { get; set; }
        public decimal Cantidad_CIDPRODUCTO_SERV { get; set; }
        public decimal Precio_CIDPRODUCTO_SERV { get; set; }
        public int idServicio { get; set; }
        public int idActividad { get; set; }

    }
}
