﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class ServicioxOrdenTrabajo
    {
        public int idOrdenSer { get; set; }
        public int idOrdenTrabajo { get; set; }
        public int CIDPRODUCTO_SERV { get; set; }
        public string CCODIGOPRODUCTO_SERV { get; set; }
        public string CNOMBREPRODUCTO_SERV { get; set; }
        public decimal Cantidad_CIDPRODUCTO_SERV { get; set; }
        public decimal Precio_CIDPRODUCTO_SERV { get; set; }

    }
}
