﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CatLugares
    {
        public int idLugar { get; set; }
        public string Descripcion { get; set; }
        public int idTipOrdServ { get; set; }
        public bool Activo { get; set; }
        public string NomTabla { get; set; }

    }
}
