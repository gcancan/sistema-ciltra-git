﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class EmpresasCOM
    {
        public int CIDEMPRESA { get; set; }
        public string CNOMBREEMPRESA { get; set; }
        public string CRUTADATOS { get; set; }
    }
}
