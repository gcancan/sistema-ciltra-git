﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CatSAT
    {
        public int idClave { get; set; }
        public string Clave { get; set; }
        public string Descripcion { get; set; }
        public string Tipo { get; set; }

    }
}
