﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CatGastos
    {
        public int idGasto { get; set; }
        public string NomGasto { get; set; }
        public bool Deducible { get; set; }

        public bool ReqComprobante { get; set; }
        public bool ReqAutorizacion { get; set; }
        public bool Activo { get; set; }
    }//
}
