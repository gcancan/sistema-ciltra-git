﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CatAlmacen
    {
        public int idAlmacen { get; set; }
        public string NomAlmacen { get; set; }
        public int idSucursal { get; set; }
        public int CIDALMACEN_COM { get; set; }

    }//
}
