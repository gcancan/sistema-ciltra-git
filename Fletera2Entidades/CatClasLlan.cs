﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CatClasLlan
    {
        public int idClasLlan { get; set; }
        public string NombreClasLlan { get; set; }
        public byte NoEjes { get; set; }
        public byte NoLLantas { get; set; }

    }
}
