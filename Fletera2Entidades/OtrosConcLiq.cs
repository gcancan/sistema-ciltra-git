﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class OtrosConcLiq
    {
        public int NumFolio { get; set; }
        public int NumGuiaId { get; set; }
        public int idOperador { get; set; }
        public string NomOperador { get; set; }
        public int idConcepto { get; set; }
        public string NomConcepto { get; set; }
        public decimal Importe { get; set; }
        public bool EnParcialidades { get; set; }
        public string userCrea { get; set; }
        public string NomUserCrea { get; set; }
        
        public DateTime Fecha { get; set; }
        public int idEmpresa { get; set; }
        public string RazonSocial { get; set; }
        public int NumLiquidacion { get; set; }
        public bool Cancelado { get; set; }
        public string MotivoCancela { get; set; }
        public decimal ImporteAplicado { get; set; }
        public bool EsCargo { get; set; }
    }
}
