﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CabMovimientos
    {
        public int NoMovimiento { get; set; }
        public int IdMovimiento { get; set; }
        public string NoSerie { get; set; }
        public string TipoMovimiento { get; set; }
        public string CCODIGOC01_CLI { get; set; }
        public DateTime FechaMovimiento { get; set; }
        public decimal SubTotalGrav { get; set; }
        public decimal SubTotalExento { get; set; }
        public decimal Retencion { get; set; }
        public decimal Iva { get; set; }
        public string Estatus { get; set; }
        public decimal Descuento { get; set; }
        public string CCODIGOA01 { get; set; }
        public string Usuario { get; set; }
        public int IdEmpresa { get; set; }
        public string UserAutoriza { get; set; }
        public int CIDDOCUM02 { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public DateTime FechaEntrega { get; set; }
        public string UsuarioCrea { get; set; }
        public DateTime FechaCrea { get; set; }
        public string UsuarioModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public string UsuarioCancela { get; set; }
        public DateTime FechaCancela { get; set; }
        public int CIDMONEDA { get; set; }
        public int CTIPOCAM01 { get; set; }
        public string Vehiculo { get; set; }
        public string Chofer { get; set; }
        public string CMETODOPAG { get; set; }
        public string CNUMCTAPAG { get; set; }
        public string CREFEREN01 { get; set; }
        public string COBSERVA01 { get; set; }
        public bool TimbraFactura { get; set; }
        public bool EsExterno { get; set; }
        public bool ReqAutDcto { get; set; }
        public decimal Kilometraje { get; set; }
        public decimal CantTotal { get; set; }
    }
}
