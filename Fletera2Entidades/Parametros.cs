﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class Parametros
    {
        public int IdParametro { get; set; }
        public string idConceptoEntAlm { get; set; }
        public string idConceptoSalAlm { get; set; }
        public string idAlmacenEnt { get; set; }
        public string idAlmacenSal { get; set; }
        public int idEmpresaCOMTaller { get; set; }

        public decimal PorcUtilidad { get;set;}

    }
}
