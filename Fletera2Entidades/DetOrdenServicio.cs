﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class DetOrdenServicio
    {
        public bool Seleccionado { get; set; }
        public int idOrdenSer { get; set; }
        public int idOrdenActividad { get; set; }
        public int idServicio { get; set; }
        public int idActividad { get; set; }
        public decimal DuracionActHr { get; set; }
        public int NoPersonal { get; set; }
        public int id_proveedor { get; set; }
        public string Estatus { get; set; }
        public decimal CostoManoObra { get; set; }
        public decimal CostoProductos { get; set; }
        public DateTime FechaDiag { get; set; }
        public string UsuarioDiag { get; set; }
        public DateTime FechaAsig { get; set; }
        public string UsuarioAsig { get; set; }
        public DateTime FechaPausado { get; set; }
        public string NotaPausado { get; set; }
        public DateTime FechaTerminado { get; set; }
        public string UsuarioTerminado { get; set; }
        public int idOrdenTrabajo { get; set; }
        public int idDivision { get; set; }
        public string idLlanta { get; set; }
        public int PosLlanta { get; set; }
        public string NotaDiagnostico { get; set; }
        public bool FaltInsumo { get; set; }
        public bool Pendiente { get; set; }
        public int CIDPRODUCTO_SERV { get; set; }
        public decimal Cantidad_CIDPRODUCTO_SERV { get; set; }
        public decimal Precio_CIDPRODUCTO_SERV { get; set; }

        //---------------------------------
        public int idFamilia { get; set; }
        public string NotaRecepcion { get; set; }
        public bool isCancelado { get; set; }
        public string motivoCancelacion { get; set; }
        public int idPersonalResp { get; set; }
        public int idPersonalAyu1 { get; set; }
        public int idPersonalAyu2 { get; set; }
        public string NotaRecepcionA { get; set; }
        public string NotaRecepcionF { get; set; }
        public string FallaReportada { get; set; }
        public string NomServicio { get; set; }
        public string TipoNotaRecepcionF { get; set; }

        public string NomPersonalResp { get; set; }
        public string NomPersonalAyu1 { get; set; }
        public string NomPersonalAyu2 { get; set; }

        public int CIDPRODUCTO_ACT { get; set; }
        public DetRecepcionOS DetRec { get; set; }

    }
}
