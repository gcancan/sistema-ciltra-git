﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class DetLiqOtrConRep
    {
        public int idLiquidacion { get; set; }
        public int Folio { get; set; }
        public string Descripcion { get; set; }
        public int NumViaje { get; set; }
        public decimal Importe { get; set; }
        public decimal iva { get; set; }
        public decimal Total { get; set; }
        public bool EsCargo { get; set; }
        
    }
}
