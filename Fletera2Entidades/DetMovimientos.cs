﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class DetMovimientos
    {
        public int NoMovimiento { get; set; }
        public int CIDUNIDA01 { get; set; }
        public string CCODIGOP01_PRO { get; set; }
        public int Consecutivo { get; set; }
        public int IdMovimiento { get; set; }
        public string NoSerie { get; set; }
        public string TipoMovimiento { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Precio { get; set; }
        public decimal PorcIVA { get; set; }
        public decimal Costo { get; set; }
        public decimal PorcDescto { get; set; }
        public decimal ImpDescto { get; set; }
        public int CIDALMACEN { get; set; }
        public decimal Existencia { get; set; }
        public int CIDUNIDA01BASE { get; set; }
        public decimal CantidadBASE { get; set; }
        public decimal tFactorUnidad { get; set; }
        public bool tNoEquivalente { get; set; }
        public bool EsListaPrecios { get; set; }
        public int CIDUNIDA02 { get; set; }
        public decimal CantidadEQUI { get; set; }
        public string COBSERVA01 { get; set; }

        public bool EsServicio { get; set; }
        public string CNOMBREPRODUCTO { get; set; }
        public decimal Total { get; set; }

    }//
}
