﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class admProductos
    {
        public int CIDPRODUCTO { get; set; }
        public string CCODIGOPRODUCTO { get; set; }
        public string CNOMBREPRODUCTO { get; set; }
        public int CIDUNIDADBASE { get; set; }
        public double CPRECIO1 { get; set; }
        public double CIMPUESTO1 { get; set; }
        //public double  { get; set; }
    }
}
