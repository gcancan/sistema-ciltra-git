﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CatEmpresas
    {
        public int idEmpresa { get; set; }
        public string RazonSocial { get; set; }
        public string NomBaseDatosCOM { get; set; }
        public int CIDCLIENTEPROVEEDOR { get; set; }

    }
}
