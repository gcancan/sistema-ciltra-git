﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class admClientes
    {
        public int CIDCLIENTEPROVEEDOR { get; set; }
        public string CCODIGOCLIENTE { get; set; }
        public string CRAZONSOCIAL { get; set; }
        public string CRFC { get; set; }
        public string CNOMBRECALLE { get; set; }
        public string CNUMEROEXTERIOR { get; set; }
        public string CCOLONIA { get; set; }
        public string CMETODOPAG { get; set; }
        public string CUSOCFDI { get; set; }

    }
}
