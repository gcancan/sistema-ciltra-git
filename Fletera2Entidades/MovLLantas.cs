﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class MovLLantas
    {
        public int idMovLlanta { get; set; }
        public int TipoMov { get; set; }
        public DateTime Fecha { get; set; }
        public string Usuario { get; set; }
        public int idSisLlanta { get; set; }
        public int idtipoUbicacionOrig { get; set; }
        public string idUbicacionOrig { get; set; }
        public int PosicionOrig { get; set; }
        public int idtipoUbicacionDes { get; set; }
        public string idUbicacionDes { get; set; }
        public int PosicionDes { get; set; }
        public string Observaciones { get; set; }

    }
}
