﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class DOSInsumosVista
    {
        public int idOrdenSer { get; set; }
        public int idOrdenActividad { get; set; }
        public string NotaRecepcion { get; set; }
        public int idProducto { get; set; }
        public string CCODIGOPRODUCTO { get; set; }
        public string CNOMBREPRODUCTO { get; set; }
        public decimal Cantidad { get; set; }
        public decimal Costo { get; set; }
        public decimal CantCompra { get; set; }
        public decimal CantSalida { get; set; }

    }
}
