﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CabPreSalida
    {
        public int IdPreSalida { get; set; }
        public int idOrdenTrabajo { get; set; }
        public DateTime FecSolicitud { get; set; }
        public string UserSolicitud { get; set; }
        public string NomUserSolicitud { get; set; }
        public int IdEmpleadoEnc { get; set; }
        public string NomEmpleadoEnc { get; set; }
        public int idAlmacen { get; set; }
        public string NomAlmacen { get; set; }
        public int CIDALMACEN_COM { get; set; }
        public string Estatus { get; set; }
        public decimal SubTotal { get; set; }
        public DateTime FecCancela { get; set; }
        public string UserCancela { get; set; }
        public string MotivoCancela { get; set; }
        public DateTime FecEntrega { get; set; }
        public string UserEntrega { get; set; }
        public int IdPersonalRec { get; set; }
        public bool EntregaCambio { get; set; }
        public int idPreOC { get; set; }
        public string CSERIEDOCUMENTO_ENT { get; set; }
        public int CIDDOCUMENTO_ENT { get; set; }
        public double CFOLIO_ENT { get; set; }

        public string CSERIEDOCUMENTO_SAL { get; set; }
        public int CIDDOCUMENTO_SAL { get; set; }
        public double CFOLIO_SAL { get; set; }
        public int idOrdenSer { get; set; }
        public string idUnidadTrans { get; set; }
        public bool EsDevolucion { get; set; }


        //public int idPersonalAyu1 { get; set; }
        //public int idPersonalAyu2 { get; set; }

        //public string NomPersonalAyu1 { get; set; }
        //public string NomPersonalAyu2 { get; set; }
        //public decimal Inventario { get; set; }
    }
}
