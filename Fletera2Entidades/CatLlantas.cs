﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CatLlantas
    {
        public int idSisLlanta { get; set; }
        public string idLlanta { get; set; }
        public string NoSerie { get; set; }
        public int idEmpresa { get; set; }
        public int idProductoLlanta { get; set; }
        public int idTipoLLanta { get; set; }
        public string NomTipoLlanta { get; set; }
        public string Factura { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Costo { get; set; }
        public string RFDI { get; set; }
        public int Posicion { get; set; }
        public int idtipoUbicacion { get; set; }
        public string idUbicacion { get; set; }
        public int Año { get; set; }
        public int Mes { get; set; }
        public string Observaciones { get; set; }
        public DateTime UltFecha { get; set; }
        public string idCondicion { get; set; }
        public string NomCondicion { get; set; }
        public bool Activo { get; set; }
        public decimal ProfundidadAct { get; set; }
        public int idDisenioRev { get; set; }
        public string NomDisenioRev { get; set; }
        public int idOrdenCompra { get; set; }
        public int idDetalleCompra { get; set; }

    }
}
