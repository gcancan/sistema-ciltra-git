﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CatServicios
    {
        public int idServicio { get; set; }
        public int idTipoServicio { get; set; }
        public string NomServicio { get; set; }
        public string Descripcion { get; set; }
        public decimal CadaKms { get; set; }
        public decimal CadaTiempoDias { get; set; }
        public string Estatus { get; set; }
        public decimal Costo { get; set; }
        public int idDivision { get; set; }

    }
}
