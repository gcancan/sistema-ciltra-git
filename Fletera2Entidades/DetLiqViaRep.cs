﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class DetLiqViaRep
    {
        public int idLiquidacion { get; set; }
        public int NumViaje { get; set; }
        public string Ruta { get; set; }
        public DateTime FecSal { get; set; }
        public DateTime FecFin { get; set; }
        public string TipoViaje { get; set; }
        public string idTractor { get; set; }
        public string idRemolque1 { get; set; }
        public string idDolly { get; set; }
        public string idRemolque2 { get; set; }
        public decimal Importe { get; set; }

    }
}
