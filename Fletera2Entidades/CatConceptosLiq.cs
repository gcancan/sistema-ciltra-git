﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CatConceptosLiq
    {
        public int idConcepto { get; set; }
        public string NomConcepto { get; set; }
        public bool EsCargo { get; set; }
        public bool Activo { get; set; }
    }
}
