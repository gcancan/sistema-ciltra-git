﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CatActividades
    {
        public int idActividad { get; set; }
        public string NombreAct { get; set; }
        public decimal CostoManoObra { get; set; }
        public int idFamilia { get; set; }
        public string DuracionHoras { get; set; }
        public int idEnum { get; set; }
        public int CIDPRODUCTO { get; set; }
        public string CCODIGOPRODUCTO { get; set; }


    }
}
