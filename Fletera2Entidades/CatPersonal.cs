﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CatPersonal
    {
        public int idPersonal { get; set; }
        public int idEmpresa { get; set; }
        public string NombreCompleto { get; set; }
        public string Nombre { get; set; }
        public string ApellidoPat { get; set; }
        public string ApellidoMat { get; set; }
        public int idPuesto { get; set; }
        public int idDepto { get; set; }
        public string NomPuesto { get; set; }
        public string NomDepto { get; set; }


    }
}
