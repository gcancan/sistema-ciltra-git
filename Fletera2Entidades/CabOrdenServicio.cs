﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CabOrdenServicio
    {
        public int idOrdenSer { get; set; }
        public int IdEmpresa { get; set; }
        public string RazonSocial { get; set; }
        public string idUnidadTrans { get; set; }
        public string NomTipoUnidad { get; set; }
        public int idTipoOrden { get; set; }
        public DateTime FechaRecepcion { get; set; }
        public int idEmpleadoEntrega { get; set; }
        public string NomEmpleadoEntrega { get; set; }
        public string UsuarioRecepcion { get; set; }
        public decimal Kilometraje { get; set; }
        public string Estatus { get; set; }
        public DateTime FechaDiagnostico { get; set; }
        public DateTime FechaAsignado { get; set; }
        public DateTime FechaTerminado { get; set; }
        public DateTime FechaEntregado { get; set; }
        public string UsuarioEntrega { get; set; }
        public int idEmpleadoRecibe { get; set; }
        public string NotaFinal { get; set; }
        public string usuarioCan { get; set; }
        public DateTime FechaCancela { get; set; }
        public int idEmpleadoAutoriza { get; set; }
        public DateTime fechaCaptura { get; set; }
        public int idTipOrdServ { get; set; }
        public string NomTipOrdServ { get; set; }
        public int idTipoServicio { get; set; }
        public int idPadreOrdSer { get; set; }
        public int ClaveFicticia { get; set; }
        public string NomEmpleadoAutoriza { get; set; }
        public string NomTipoServicio { get; set; }
        public int idOperarador { get; set; }
        public string NomOperador { get; set; }
        public string UsuarioAsigna { get; set; }
        public string UsuarioTermina { get; set; }
        public List<DetOrdenServicio> listaDet { get; set; }
    }
}
