﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class admDocumentos
    {
        public int CIDDOCUMENTO { get; set; }
        public int CIDDOCUMENTODE { get; set; }
        public int CIDCONCEPTODOCUMENTO { get; set; }
        public string CSERIEDOCUMENTO { get; set; }
        public Double CFOLIO { get; set; }
        public DateTime CFECHA { get; set; }
        public int CIDCLIENTEPROVEEDOR { get; set; }
        public string CREFERENCIA { get; set; }
        public int CCANCELADO { get; set; }
        public Double CNETO { get; set; }
        public Double CIMPUESTO1 { get; set; }
        public Double CTOTAL { get; set; }
        public string CMETODOPAG { get; set; }

    }
}
