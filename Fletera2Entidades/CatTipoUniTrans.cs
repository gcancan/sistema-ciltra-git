﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CatTipoUniTrans
    {
        public int idTipoUniTras { get; set; }
        public string nomTipoUniTras { get; set; }
        public string NoLlantas { get; set; }
        public string NoEjes { get; set; }
        public string TonelajeMax { get; set; }
        public string clasificacion { get; set; }
        public bool bCombustible { get; set; }
        public int idClasLlan { get; set; }
        public string grupo { get; set; }
        public string TipoUso { get; set; }

    }
}
