﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class MasterOrdServ_f2
    {
        public int idPadreOrdSer { get; set; }
        public string idTractor { get; set; }
        public string idTolva1 { get; set; }
        public string idDolly { get; set; }
        public string idTolva2 { get; set; }
        public int idChofer { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string UserCrea { get; set; }
        public string UserModifica { get; set; }
        public DateTime FechaModifica { get; set; }
        public string UserCancela { get; set; }
        public DateTime FechaCancela { get; set; }
        public string MotivoCancela { get; set; }
        public string Estatus { get; set; }
        public string NomChofer { get; set; }

        public List<CabOrdenServicio> listaCab { get; set; }

    }
}
