﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class admCostosHistoricos
    {
        public int CIDCOSTOH { get; set; }
        public int CIDPRODUCTO { get; set; }
        public int CIDALMACEN { get; set; }
        public DateTime CFECHACOSTOH { get; set; }
        public double CCOSTOH { get; set; }
        public double CULTIMOCOSTOH { get; set; }
        public int CIDMOVIMIENTO { get; set; }

        public string CCODIGOPRODUCTO { get; set; }
        public string CNOMBREPRODUCTO { get; set; }
    }
}
