﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class ViajePorOperador
    {
        public bool Seleccionado { get; set; }
        public int idOperador { get; set; }
        public string NomOperador { get; set; }

        public int NumViaje { get; set; }
    }
}
