﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class Liquidaciones
    {
        public int NumGuiaId { get; set; }
        public int idOperador { get; set; }
        public string NomOperador { get; set; }
        public int idEmpresa { get; set; }
        public string NomEmpresa { get; set; }
        public int idCliente { get; set; }
        public string NomCliente { get; set; }
        public DateTime FechaHoraViaje { get; set; }
        public DateTime FechaHoraFin { get; set; }
        public string idTractor { get; set; }
        public string idRemolque1 { get; set; }
        public string idDolly { get; set; }
        public string idRemolque2 { get; set; }
        public int UserViaje { get; set; }
        public string nombreUsuario { get; set; }
        public string NomUsuario { get; set; }
        public string TipoViaje { get; set; }
        public decimal VolDescarga { get; set; }
        public int idCargaGasolina { get; set; }
        public decimal precioOperador { get; set; }
        public string nombreOrigen { get; set; }
        public string Destino { get; set; }
        public string Producto { get; set; }
    }
}
