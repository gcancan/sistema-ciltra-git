﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CabLiquidaciones
    {
        public int idLiquidacion { get; set; }
        public DateTime Fecha { get; set; }
        public int idOperador { get; set; }
        public string NomOperador { get; set; }
        public int idEmpresa { get; set; }
        public string RazonSocial { get; set; }
        public int NumViajes { get; set; }
        public decimal ViajesTot { get; set; }
        public decimal CombustibleTot { get; set; }
        public decimal GastosxCompTot { get; set; }
        public decimal AnticiposTot { get; set; }
        public decimal ComprobacionGastosTot { get; set; }
        public decimal OtrosConceptosTot { get; set; }
        public string Usuario { get; set; }
        public string NomUsuario { get; set; }
        public string Estatus { get; set; }
        public string Observaciones { get; set; }
        public DateTime FechaCancela { get; set; }
        public string UsuarioCancela { get; set; }
        public string NomUsuarioCancela { get; set; }

        public decimal TOTAL { get; set; }

        public DateTime FechaIni { get; set; }
        public DateTime FechaFin { get; set; }
        
            
    }
}
