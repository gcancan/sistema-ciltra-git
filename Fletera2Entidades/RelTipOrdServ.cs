﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class RelTipOrdServ
    {
        public int idTipOrdServ { get; set; }
        public string NomCatalogo { get; set; }
        public string NombreId { get; set; }
        public int IdRelacion { get; set; }

    }
}
