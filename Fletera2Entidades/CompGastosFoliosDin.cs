﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CompGastosFoliosDin
    {
        public int Folio { get; set; }
        public int Consecutivo { get; set; }
        public string NoDocumento { get; set; }
        public int idGasto { get; set; }
        public string NomGasto { get; set; }
        public DateTime Fecha { get; set; }
        public bool Deducible { get; set; }
        public int Autoriza { get; set; }
        public string NomAutoriza { get; set; }
        public string Observaciones { get; set; }
        public bool ReqComprobante { get; set; }
        public bool ReqAutorizacion { get; set; }
        public int FolioDin { get; set; }
        public int idLiquidacion { get; set; }
        public int idEmpresa { get; set; }
        public string RazonSocial { get; set; }
        public decimal Importe { get; set; }
        public decimal IVA { get; set; }

    }//
}
