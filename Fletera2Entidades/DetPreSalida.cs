﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class DetPreSalida
    {
        public bool Seleccionado { get; set; }
        public int IdPreSalida { get; set; }
        public int idProducto { get; set; }
        public string CCODIGOPRODUCTO { get; set; }
        public string CNOMBREPRODUCTO { get; set; }
        public int idUnidadProd { get; set; }
        public decimal Cantidad { get; set; }
        public decimal CantidadEnt { get; set; }
        public decimal Costo { get; set; }
        public decimal Existencia { get; set; }
        public bool Inserta { get; set; }
        public decimal CantEntregaCambio { get; set; }

    }
}
