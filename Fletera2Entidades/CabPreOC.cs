﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CabPreOC
    {
        public int IdPreOC { get; set; }
        public int idOrdenTrabajo { get; set; }
        public int idSolOC { get; set; }
        public int idCotizacionOC { get; set; }
        public int idOC { get; set; }
        public DateTime FechaPreOc { get; set; }
        public string UserSolicita { get; set; }
        public int idAlmacen { get; set; }
        public string Estatus { get; set; }
        public string UserAutorizaOC { get; set; }
        public DateTime FecAutorizaOC { get; set; }
        public string Observaciones { get; set; }
        public DateTime FecModifica { get; set; }
        public string UserModifica { get; set; }
        public decimal SubTotal { get; set; }
        public decimal IVA { get; set; }
        public DateTime FecCancela { get; set; }
        public string UserCancela { get; set; }
        public string MotivoCancela { get; set; }
        public string CSERIEDOCUMENTO { get; set; }
        public int CIDDOCUMENTO { get; set; }
        public double CFOLIO { get; set; }
        public int IdPreSalida { get; set; }

    }
}
