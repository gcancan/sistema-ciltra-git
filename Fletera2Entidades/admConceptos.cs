﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class admConceptos
    {
        public int CIDCONCEPTODOCUMENTO { get; set; }
        public string CCODIGOCONCEPTO { get; set; }
        public string CNOMBRECONCEPTO { get; set; }
        public int CIDDOCUMENTODE { get; set; }

    }
}
