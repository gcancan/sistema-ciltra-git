﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class MonitorOS
    {
        public int FOLIO { get; set; }
        public int idOrdenSer { get; set; }
        public string Tipo { get; set; }
        public string Unidad { get; set; }
        public string Chofer { get; set; }//usuario *
        public string Empresa { get; set; }
        public string Estatus { get; set; }
        public string Autorizo { get; set; }
        public string TiempoTrasncurrido { get; set; }
        public DateTime Captura { get; set; }
        public DateTime? Recepcion { get; set; }
        public string Entrego { get; set; }//usuario
        public string Recibio { get; set; }//usuario
        public DateTime? Asignado { get; set; }
        public string Asigno { get; set; }//usuario *
        public DateTime? Terminado { get; set; }
        public string Termino { get; set; }//usuario *
        public DateTime? Entregado { get; set; }
        public DateTime? Cancelado { get; set; }
        public string Cancelo { get; set; }//usuario *
        public bool externo { get; set; }
        public DateTime? CasetaEntrada { get; set; }
        public string CasetaEntradaUser { get; set; }//usuario *
        public DateTime? CasetaSalida { get; set; }
        public string CasetaSalidaUser { get; set; }//usuario *
        public DateTime? OstesIni { get; set; }
        public string OstesIniUser { get; set; }//usuario *
        public DateTime? OstesFin { get; set; }
        public string OstesFinUser { get; set; }//usuario *
    }
}
