﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class LlantasHis
    {
        public int idSis { get; set; }
        public int idtipoUbicacion { get; set; }
        public string idUbicacion { get; set; }
        public DateTime Fecha { get; set; }
        public string Usuario { get; set; }
        public int idSisLlanta { get; set; }
        public string idLlanta { get; set; }
        public bool EntSal { get; set; }
        public int idTipoDoc { get; set; }
        public string Documento { get; set; }
        public decimal Profundidad { get; set; }
        public int idCondicion { get; set; }
        public int Posicion { get; set; }

    }
}
