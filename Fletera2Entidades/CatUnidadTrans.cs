﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class CatUnidadTrans
    {
        public string idUnidadTrans { get; set; }
        public string descripcionUni { get; set; }
        public int idTipoUnidad { get; set; }
        public int idMarca { get; set; }
        public int idEmpresa { get; set; }
        public int idSucursal { get; set; }
        public int idAlmacen { get; set; }
        public string Estatus { get; set; }
        public int UltOT { get; set; }
        public string rfid { get; set; }
        public int idTipoMotor { get; set; }
        public decimal capacidadTanque { get; set; }
        public int estatusUbicacion { get; set; }
        public int estatusUbicacionAnt { get; set; }
        public string nomTipoUniTras { get; set; }

    }
}
