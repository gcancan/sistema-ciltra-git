﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Entidades
{
    public class DetRecepcionOS
    {
        public int idOrdenTrabajo { get; set; }
        public int idOrdenSer { get; set; }
        public int idFamilia { get; set; }
        public string NotaRecepcion { get; set; }
        public bool isCancelado { get; set; }
        public string motivoCancelacion { get; set; }
        public int idDivision { get; set; }
        public int idPersonalResp { get; set; }
        public int idPersonalAyu1 { get; set; }
        public int idPersonalAyu2 { get; set; }
        public string NomPersonalResp { get; set; }
        public string NomPersonalAyu1 { get; set; }
        public string NomPersonalAyu2 { get; set; }
        public string NotaRecepcionA { get; set; }
        public bool Inserta { get; set; }


    }
}
