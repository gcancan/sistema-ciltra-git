﻿using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfTableroControlUnidades
{
    /// <summary>
    /// Lógica de interacción para AddComentariosEventoTablero.xaml
    /// </summary>
    public partial class AddComentariosEventoTablero : MetroWindow
    {
        private Usuario usuario;
        private TableroControlUnidades tablero;
        public AddComentariosEventoTablero(Usuario usuario, TableroControlUnidades tablero)
        {
            InitializeComponent();
            this.usuario = usuario;
            this.tablero = tablero;
            txtComentario.Focus();
        }
        public ComentarioTablero addComentario()
        {
            try
            {
                bool? resp = ShowDialog();
                if (resp.Value && comentario != null)
                {
                    return comentario;
                }
                return null;
            }
            catch (Exception ex) 
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }
        ComentarioTablero comentario = null;
        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtComentario.Text.Trim())) return;
            comentario = new ComentarioTablero
            {
                idTablero = tablero.idTableroControlUnidades,
                usuario = usuario.nombreUsuario,
                comentario = txtComentario.Text.Trim()
            };
            var resp = new TableroControlUnidadesSvc().saveComentarioTablero(comentario);
            if (resp.typeResult == ResultTypes.success)
            {
                DialogResult = true;
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
                comentario = null;
            }
        }
    }
}
