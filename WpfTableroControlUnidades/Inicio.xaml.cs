﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using Core.Interfaces;
using Core.Utils;
using System.Diagnostics;
using System.Security.Principal;
using System.Reflection;
using MahApps.Metro.Controls;

namespace WpfTableroControlUnidades
{
    /// <summary>
    /// Lógica de interacción para Inicio.xaml
    /// </summary>
    public partial class Inicio : MetroWindow
    {
        public Inicio()
        {
            InitializeComponent();
        }

        private void btnIniciar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //MainWindow tablero = new MainWindow();
                //this.Visibility = Visibility.Hidden;
                //tablero.ShowDialog();
                //this.Visibility = Visibility.Visible;
                iniciar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnConfig_Click(object sender, RoutedEventArgs e)
        {
            editConfig config = new editConfig();
            config.ShowDialog();
        }

        private void txtPass_LostFocus(object sender, RoutedEventArgs e)
        {

        }

        private void txtPass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                iniciar();
            }
        }
        public Usuario _usuario = null;
        MainWindow mainWin;
        private void  iniciar()
        {
            try
            {
                if (string.IsNullOrEmpty(txtUsuario.Text.Trim()) || string.IsNullOrEmpty(txtPass.Password.Trim()))
                {
                    MessageBox.Show("Proporcione el nombre de usuario y contraseña", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                OperationResult serv = new UsuarioSvc().getUserByUserPass(txtUsuario.Text.Trim(), txtPass.Password.Trim());

                if (serv.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(serv.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (serv.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(serv.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                if (serv.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(serv.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                if (serv.typeResult == ResultTypes.success)
                {
                    try
                    {
                        _usuario = (Usuario)serv.result;

                        var resPrivilegio = new PrivilegioSvc().consultarPrivilegio("ACCESO_TABLERO_CONTROL_UNIDADES", _usuario.idUsuario);
                        if (resPrivilegio.typeResult == ResultTypes.success)
                        {
                            new Util().setUltimoUsuario(_usuario.nombreUsuario);
                            mainWin = new MainWindow(this);
                            mainWin.Closed += MainWin_Closed;
                            this.Hide();
                            mainWin.Owner = this;
                            try
                            {
                                mainWin.Show();
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                            }
                            finally
                            {
                                GC.WaitForPendingFinalizers();
                                GC.Collect();
                                GC.WaitForPendingFinalizers();
                                GC.Collect();
                            }
                        }
                        else if (resPrivilegio.typeResult == ResultTypes.recordNotFound)
                        {
                            MessageBox.Show("NO TIENE PRIVILEGIO PARA EL ACCESO A ESTA APLICACIÓN", "AVISO", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                        else
                        {

                        }
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(e.Message, "Error");
                    }
                    finally
                    {
                        GC.WaitForPendingFinalizers();
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        GC.Collect();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void MainWin_Closed(object sender, EventArgs e)
        {
            try
            {
                //mainWin.timer.Stop();
                mainWin = null;
                //this.Close();
                this.Visibility = Visibility.Visible;
                //txtUsuario.Clear();
                txtPass.Clear();
                txtPass.Focus();
                //mainWin = null;
                //txtUsuario.Focus();
            }
            catch (Exception ex)
            {
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtUsuario.Text = new Util().getUltimoUsuario();
            if (!string.IsNullOrEmpty(txtUsuario.Text))
                txtPass.Focus();
            else
                txtUsuario.Focus();
        }
        private bool ValidaInicioAdministrador()
        {
            if (!IsRunAsAdministrator())
            {
                var processInfo = new ProcessStartInfo(Assembly.GetExecutingAssembly().CodeBase);
                processInfo.UseShellExecute = true;
                processInfo.Verb = "runas";
                try
                {
                    Process.Start(processInfo);
                }
                catch (Exception ex)
                {
                    var s = ex;
                    MessageBox.Show("Sorry, this application must be run as Administrator.");
                }
                Application.Current.Shutdown();
                return false;
            }
            else
            {
                return true;
            }
        }
        private bool IsRunAsAdministrator()
        {
            var wi = WindowsIdentity.GetCurrent();
            var wp = new WindowsPrincipal(wi);
            return wp.IsInRole(WindowsBuiltInRole.Administrator);
        }
    }
}
