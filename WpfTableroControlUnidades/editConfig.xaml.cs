﻿using Core.Utils;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfTableroControlUnidades
{
    /// <summary>
    /// Lógica de interacción para editConfig.xaml
    /// </summary>
    public partial class editConfig : MetroWindow
    {
        SqlConnectionStringBuilder con = new SqlConnectionStringBuilder();
        public editConfig()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            loadConfig();
        }
        private void loadConfig()
        {
            con = new Util().getConectionString();
            txtNameServer.Text = con.DataSource;
            txtNameDB.Text = con.InitialCatalog;
            txtNameUser.Text = con.UserID;
            txtPass.Password = con.Password;            

            var path = System.Windows.Forms.Application.StartupPath;
            string archivo = path + @"\config.ini";
        }
        string cadena;
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                cadena = "";
            }
            else
            {
                cadena += e.Key;
                if (cadena.ToUpper() == "ADMINSYSTEM")
                {
                    gridPrincipal.IsEnabled = true;
                }
            }
        }

        private void btnProbarConexion_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Util u = new Util();
                SqlConnection s = new SqlConnection();
                s.ConnectionString = new SqlConnectionStringBuilder
                {
                    DataSource = txtNameServer.Text.Trim(),
                    InitialCatalog = txtNameDB.Text.Trim(),
                    UserID = txtNameUser.Text.Trim(),
                    Password = txtPass.Password.Trim()
                }.ToString();

                s.Open();
                MessageBox.Show("Conexion Exitosa");
                s.Close();
            }
            catch (Exception eSQL)
            {
                MessageBox.Show(eSQL.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (!validar())
            {
                MessageBox.Show(string.Format("Aun faltan campos por completar o no son validos"), "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            guardar();
            this.Close();
        }
        private bool validar()
        {
            try
            {
                if (string.IsNullOrEmpty(txtNameServer.Text))
                {
                    return false;
                }
                if (string.IsNullOrEmpty(txtNameDB.Text))
                {
                    return false;
                }
                if (string.IsNullOrEmpty(txtNameUser.Text))
                {
                    return false;
                }
                if (string.IsNullOrEmpty(txtPass.Password))
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        private void guardar()
        {
            var path = System.Windows.Forms.Application.StartupPath;
            Util u = new Util();

            string archivo = path + @"\config.ini";
            u.Write("CONECTION_STRING", "SERVER_NAME", txtNameServer.Text.Trim(), archivo);
            u.Write("CONECTION_STRING", "DB_NAME", txtNameDB.Text.Trim(), archivo);
            u.Write("CONECTION_STRING", "USER", txtNameUser.Text.Trim(), archivo);
            u.Write("CONECTION_STRING", "PASSWORD", txtPass.Password.Trim(), archivo);
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
