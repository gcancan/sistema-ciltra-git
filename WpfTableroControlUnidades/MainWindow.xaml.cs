﻿namespace WpfTableroControlUnidades
{
    using Core.Interfaces;
    using Core.Models;
    using CoreFletera.Interfaces;
    using CoreFletera.Models;
    using MahApps.Metro.Controls;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Drawing;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Threading.Tasks;
    using System.Timers;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Markup;
    using System.Windows.Media;

    public partial class MainWindow : MetroWindow
    {
        private Timer timerBusqueda;
        private int contador;
        private Inicio inicio;
        public MainWindow(Inicio inicio)
        {
            this.inicio = inicio;
            Timer timer1 = new Timer
            {
                Interval = 1000.0
            };
            this.timerBusqueda = timer1;
            this.contador = 10;
            this.InitializeComponent();
        }

        private async Task BuscarEventosTablero()
        {
            try
            {
                //Dispatcher.Invoke(() =>
                //{
                //    this.mapTableroTrayetoGranja(new List<TableroControlUnidades>());
                //    this.mapTableroTrayectoPlanta(new List<TableroControlUnidades>());
                //    this.mapTableroEnGranjas(new List<TableroControlUnidades>());
                //    this.mapTableroEnPLANTA(new List<TableroControlUnidades>());
                //    lblOperaciones.Content = String.Empty;
                //    return;
                //});
                if (cbxZonasOperativas.SelectedItems.Count == 0 || cbxOperacion.SelectedItems.Count == 0)
                {
                    Dispatcher.Invoke(() =>
                    {
                        this.mapTableroTrayetoGranja(new List<TableroControlUnidades>());
                        this.mapTableroTrayectoPlanta(new List<TableroControlUnidades>());
                        this.mapTableroEnGranjas(new List<TableroControlUnidades>());
                        this.mapTableroEnPLANTA(new List<TableroControlUnidades>());
                        lblOperaciones.Content = String.Empty;
                        return;
                    });
                }

                Dispatcher.Invoke(() =>
                {
                    lblAccion.Content = "BUSCANDO CAMBIOS EN LOS ESTADOS";
                    lblFecha.Content = DateTime.Now.ToString("HH:mm:ss");
                });
                //base.Cursor = Cursors.Wait;

                if (cbxZonasOperativas.SelectedItems.Count == 0)
                    return;
                if (cbxOperacion.SelectedItems.Count == 0)
                    return;

                List<string> listaZonas = (from s in this.cbxZonasOperativas.SelectedItems.Cast<ZonaOperativa>().ToList<ZonaOperativa>() select s.idZonaOperativa.ToString()).ToList<string>();
                List<string> listaOperaciones = (from s in this.cbxOperacion.SelectedItems.Cast<OperacionFletera>().ToList<OperacionFletera>() select s.cliente.clave.ToString()).ToList<string>();

                Dispatcher.Invoke(() =>
                {
                    lblOperaciones.Content = (string)getOperaciones(this.cbxOperacion.SelectedItems.Cast<OperacionFletera>().ToList<OperacionFletera>());
                });

                OperationResult result = new TableroControlUnidadesSvc().getTableroControlUnidadesByOperaciones(empresa.clave, listaZonas, listaOperaciones);
                Dispatcher.Invoke(() =>
                {
                    lblAccion.Content = result.mensaje.ToUpper();
                    lblFecha.Content = DateTime.Now.ToString("HH:mm:ss");
                });
                if (result.typeResult == ResultTypes.success)
                {
                    List<TableroControlUnidades> list = result.result as List<TableroControlUnidades>;
                    DateTime now = DateTime.Now;
                    foreach (TableroControlUnidades unidades in list)
                    {
                        unidades.fechaActual = new DateTime?(now);
                    }
                    Dispatcher.Invoke(() =>
                    {
                        lblAccion.Content = "ACTUALIZANDO ESTADOS";
                        lblFecha.Content = DateTime.Now.ToString("HH:mm:ss");
                        this.mapTableroTrayetoGranja(list.FindAll(s => s.ubicacion == UbicacionTablero.TRAYECTO_GRANJA && s.eventosTaller == false));
                        this.mapTableroTrayectoPlanta(list.FindAll(s => s.ubicacion == UbicacionTablero.RETORNO_PLANTA && s.eventosTaller == false));
                        this.mapTableroEnGranjas(list.FindAll(s => s.ubicacion == UbicacionTablero.GRANJA));
                        this.mapTableroEnPLANTA(list.FindAll(s => s.ubicacion == UbicacionTablero.PLANTA
                        //&& (s.fechaOS == null ? DateTime.Now.AddDays(-1) : s.fechaOS) < (s.fechaEntradaPlanta == null ? DateTime.Now : s.fechaEntradaPlanta)
                        ));

                        if (verTaller)
                            this.mapTableroEnTALLER(list.FindAll(s => (s.ubicacion == UbicacionTablero.TRAYECTO_TALLER || s.ubicacion == UbicacionTablero.TALLER ||
                            s.ubicacion == UbicacionTablero.RETORNO_PLANTA) && s.eventosTaller));

                        //&& (s.fechaOS == null ? DateTime.Now.AddDays(-1) : s.fechaOS) < (s.fechaEntradaPlanta == null ? DateTime.Now : s.fechaEntradaPlanta)));
                        return;
                    });
                }
                else if (result.typeResult == ResultTypes.recordNotFound)
                {
                    Dispatcher.Invoke(() =>
                    {
                        this.mapTableroTrayetoGranja(new List<TableroControlUnidades>());
                        this.mapTableroTrayectoPlanta(new List<TableroControlUnidades>());
                        this.mapTableroEnGranjas(new List<TableroControlUnidades>());
                        this.mapTableroEnPLANTA(new List<TableroControlUnidades>());
                        mapTableroEnTALLER(new List<TableroControlUnidades>());
                        return;
                    });
                }
                else
                {
                    Dispatcher.Invoke(() =>
                    {
                        //ImprimirMensaje.imprimir(result);
                        this.mapTableroTrayetoGranja(new List<TableroControlUnidades>());
                        this.mapTableroTrayectoPlanta(new List<TableroControlUnidades>());
                        this.mapTableroEnGranjas(new List<TableroControlUnidades>());
                        this.mapTableroEnPLANTA(new List<TableroControlUnidades>());
                        mapTableroEnTALLER(new List<TableroControlUnidades>());
                        return;
                    });
                }
            }
            catch (Exception exception)
            {
                //MessageBox.Show(exception.Message);
                Dispatcher.Invoke(() =>
                {
                    lblAccion.Content = exception.Message;
                    lblFecha.Content = DateTime.Now.ToString("HH:mm:ss");
                });
                return;
            }
            finally
            {
                //base.Cursor = Cursors.Arrow;
                Dispatcher.Invoke(() =>
                {
                    lblAccion.Content = "ESTADOS ACTUALIZADOS";
                    lblFecha.Content = DateTime.Now.ToString("HH:mm:ss");
                });
            }
        }

        private string getOperaciones(List<OperacionFletera> list)
        {
            try
            {
                string cadena = string.Empty;
                foreach (var item in list)
                {
                    cadena += item.nombre;
                }
                return cadena.Trim('-');
            }
            catch (Exception ex)
            {
                var s = ex;
                return string.Empty;
            }
        }

        private void mapTableroEnGranjas(List<TableroControlUnidades> list)
        {
            //this.ctrUnidades.lvlEnGranjas.ItemsSource = list;
            List<ListViewItem> listLvl = new List<ListViewItem>();
            ctrUnidades.lvlEnGranjas.Items.Clear();
            ctrUnidades.lblContadorGranjas.Content = list.Count;
            foreach (TableroControlUnidades item in list)
            {
                ListViewItem lvl = new ListViewItem()
                {
                    Content = item,
                    Foreground = new SolidColorBrush(item.diferenciaEngranja != "00:00" ? Colors.Red : Colors.Black),
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    FontSize = 9.5
                };

                ContextMenu menu = new ContextMenu();
                MenuItem itemMenu = new MenuItem { Header = "AGREGAR COMENTARIO", Tag = item };
                itemMenu.Click += ItemMenu_Click;
                menu.Items.Add(itemMenu);

                ToolTip too = new System.Windows.Controls.ToolTip();
                StackPanel stpContentToolTip = new StackPanel { Orientation = Orientation.Vertical };
                foreach (var comentario in item.listaComentarios)
                {
                    stpContentToolTip.Children.Add(new controlComentario(comentario.usuario, comentario.fecha, comentario.comentario));
                }
                too.Content = stpContentToolTip;
                lvl.ToolTip = too;

                lvl.ContextMenu = menu;

                listLvl.Add(lvl);
            }
            foreach (var item in listLvl)
            {
                ctrUnidades.lvlEnGranjas.Items.Add(item);
            }
            lblContadorEventos.Content = Convert.ToInt32(ctrUnidades.lblContadorTrayectoGranjas.Content) + Convert.ToInt32(ctrUnidades.lblContadorTrayectoPlanta.Content) +
                Convert.ToInt32(ctrUnidades.lblContadorGranjas.Content) + Convert.ToInt32(ctrUnidades.lblContadorPlanta.Content) + Convert.ToInt32(ctrUnidades.lblContadorTaller.Content);
        }

        private void mapTableroEnPLANTA(List<TableroControlUnidades> list)
        {
            //this.ctrUnidades.lvlEnPLANTA.ItemsSource = list;
            List<ListViewItem> listLvl = new List<ListViewItem>();
            ctrUnidades.lvlEnPLANTA.Items.Clear();
            ctrUnidades.lblContadorPlanta.Content = list.Count;
            foreach (TableroControlUnidades item in list)
            {
                ListViewItem lvl = new ListViewItem()
                {
                    Content = item,
                    FontStyle = item.fechaSalidaPlanta != null ? FontStyles.Italic : FontStyles.Normal,
                    FontWeight = item.fechaSalidaPlanta != null ? FontWeights.Medium : FontWeights.Normal,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    FontSize = 9.5
                };
                ContextMenu menu = new ContextMenu();
                MenuItem itemMenu = new MenuItem { Header = "AGREGAR COMENTARIO", Tag = item };
                itemMenu.Click += ItemMenu_Click;
                menu.Items.Add(itemMenu);

                ToolTip too = new System.Windows.Controls.ToolTip();
                StackPanel stpContentToolTip = new StackPanel { Orientation = Orientation.Vertical };
                foreach (var comentario in item.listaComentarios)
                {
                    stpContentToolTip.Children.Add(new controlComentario(comentario.usuario, comentario.fecha, comentario.comentario));
                }
                too.Content = stpContentToolTip;
                lvl.ToolTip = too;

                lvl.ContextMenu = menu;
                listLvl.Add(lvl);
            }
            foreach (var item in listLvl)
            {
                ctrUnidades.lvlEnPLANTA.Items.Add(item);
            }
            lblContadorEventos.Content = Convert.ToInt32(ctrUnidades.lblContadorTrayectoGranjas.Content) + Convert.ToInt32(ctrUnidades.lblContadorTrayectoPlanta.Content) +
                Convert.ToInt32(ctrUnidades.lblContadorGranjas.Content) + Convert.ToInt32(ctrUnidades.lblContadorPlanta.Content) + Convert.ToInt32(ctrUnidades.lblContadorTaller.Content);
        }
        private void mapTableroEnTALLER(List<TableroControlUnidades> list)
        {
            //this.ctrUnidades.lvlEnPLANTA.ItemsSource = list;
            List<ListViewItem> listLvl = new List<ListViewItem>();
            ctrUnidades.lvlTaller.Items.Clear();
            ctrUnidades.lblContadorTaller.Content = list.Count;
            foreach (TableroControlUnidades item in list)
            {
                ListViewItem lvl = new ListViewItem()
                {
                    Content = item,
                    //FontStyle = item.fechaSalidaPlanta != null ? FontStyles.Italic : FontStyles.Normal,
                    //FontWeight = item.fechaSalidaPlanta != null ? FontWeights.Medium : FontWeights.Normal,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    FontSize = 9.5
                };
                ContextMenu menu = new ContextMenu();
                MenuItem itemMenu = new MenuItem { Header = "AGREGAR COMENTARIO", Tag = item };
                itemMenu.Click += ItemMenu_Click;
                menu.Items.Add(itemMenu);

                ToolTip too = new System.Windows.Controls.ToolTip();
                StackPanel stpContentToolTip = new StackPanel { Orientation = Orientation.Vertical };
                foreach (var comentario in item.listaComentarios)
                {
                    stpContentToolTip.Children.Add(new controlComentario(comentario.usuario, comentario.fecha, comentario.comentario));
                }
                too.Content = stpContentToolTip;
                lvl.ToolTip = too;

                lvl.ContextMenu = menu;
                listLvl.Add(lvl);
            }
            foreach (var item in listLvl)
            {
                ctrUnidades.lvlTaller.Items.Add(item);
            }
            lblContadorEventos.Content = Convert.ToInt32(ctrUnidades.lblContadorTrayectoGranjas.Content) + Convert.ToInt32(ctrUnidades.lblContadorTrayectoPlanta.Content) +
                Convert.ToInt32(ctrUnidades.lblContadorGranjas.Content) + Convert.ToInt32(ctrUnidades.lblContadorPlanta.Content) + Convert.ToInt32(ctrUnidades.lblContadorTaller.Content);
        }

        private void mapTableroTrayectoPlanta(List<TableroControlUnidades> list)
        {
            //this.ctrUnidades.lvlTrayectoPlanta.ItemsSource = list;
            List<ListViewItem> listLvl = new List<ListViewItem>();
            ctrUnidades.lvlTrayectoPlanta.Items.Clear();
            ctrUnidades.lblContadorTrayectoPlanta.Content = list.Count;
            foreach (TableroControlUnidades item in list)
            {
                ListViewItem lvl = new ListViewItem()
                {
                    Content = item,
                    Foreground = new SolidColorBrush(item.diferenciaLlegadaPlanta != "00:00" ? Colors.Red : Colors.Black),
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    FontSize = 9.5
                };

                ToolTip too = new System.Windows.Controls.ToolTip();
                StackPanel stpContentToolTip = new StackPanel { Orientation = Orientation.Vertical };

                if (item.fechaEntradaTallerTrayectoPlanta != null)
                {


                    GroupBox gbxTaller = new GroupBox { Header = "TALLER" };
                    StackPanel stpContentGbxTaller = new StackPanel { Orientation = Orientation.Vertical, HorizontalAlignment = HorizontalAlignment.Left };

                    StackPanel stpEntrada = new StackPanel { Orientation = Orientation.Horizontal };
                    stpEntrada.Children.Add(
                        new Label
                        {
                            Content = "Entrada:",
                            Width = 80,
                            FontWeight = FontWeights.Medium,
                            HorizontalContentAlignment = HorizontalAlignment.Right
                        });
                    stpEntrada.Children.Add(
                        new Label
                        {
                            Content = item.fechaEntradaTallerTrayectoPlanta.Value.ToString("dd/MM HH:mm"),
                            Width = 100,
                            FontWeight = FontWeights.Bold,
                            HorizontalContentAlignment = HorizontalAlignment.Right
                        });

                    StackPanel stpSalida = new StackPanel { Orientation = Orientation.Horizontal };
                    stpSalida.Children.Add(
                        new Label
                        {
                            Content = "Salida:",
                            Width = 80,
                            FontWeight = FontWeights.Medium,
                            HorizontalContentAlignment = HorizontalAlignment.Right
                        });
                    stpSalida.Children.Add(
                        new Label
                        {
                            Content = item.fechaSalidaTallerTrayectoPlanta == null ? string.Empty : item.fechaSalidaTallerTrayectoPlanta.Value.ToString("dd/MM HH:mm"),
                            Width = 100,
                            FontWeight = FontWeights.Bold,
                            HorizontalContentAlignment = HorizontalAlignment.Right
                        });

                    StackPanel stpEstadia = new StackPanel { Orientation = Orientation.Horizontal };
                    stpEstadia.Children.Add(
                        new Label
                        {
                            Content = "Duración:",
                            Width = 80,
                            FontWeight = FontWeights.Medium,
                            HorizontalContentAlignment = HorizontalAlignment.Right
                        });
                    stpEstadia.Children.Add(
                        new Label
                        {
                            Content = item.tiempoTallerTrayectoPlanta,
                            Width = 100,
                            FontWeight = FontWeights.Bold,
                            HorizontalContentAlignment = HorizontalAlignment.Right
                        });

                    stpContentGbxTaller.Children.Add(stpEntrada);
                    stpContentGbxTaller.Children.Add(stpSalida);
                    stpContentGbxTaller.Children.Add(stpEstadia);

                    gbxTaller.Content = stpContentGbxTaller;
                    stpContentToolTip.Children.Add(gbxTaller);

                }

                ContextMenu menu = new ContextMenu();
                MenuItem itemMenu = new MenuItem { Header = "AGREGAR COMENTARIO", Tag = item };
                itemMenu.Click += ItemMenu_Click;
                menu.Items.Add(itemMenu);
                
                foreach (var comentario in item.listaComentarios)
                {
                    stpContentToolTip.Children.Add(new controlComentario(comentario.usuario, comentario.fecha, comentario.comentario));
                }
                too.Content = stpContentToolTip;
                lvl.ToolTip = too;

                lvl.ContextMenu = menu;
                listLvl.Add(lvl);
            }
            foreach (var item in listLvl)
            {
                ctrUnidades.lvlTrayectoPlanta.Items.Add(item);
            }
            lblContadorEventos.Content = Convert.ToInt32(ctrUnidades.lblContadorTrayectoGranjas.Content) + Convert.ToInt32(ctrUnidades.lblContadorTrayectoPlanta.Content) +
                Convert.ToInt32(ctrUnidades.lblContadorGranjas.Content) + Convert.ToInt32(ctrUnidades.lblContadorPlanta.Content) + Convert.ToInt32(ctrUnidades.lblContadorTaller.Content);
        }

        private void mapTableroTrayetoGranja(List<TableroControlUnidades> list)
        {
            //this.ctrUnidades.lvlDestinos.ItemsSource = list; 
            List<ListViewItem> listLvl = new List<ListViewItem>();
            ctrUnidades.lvlDestinos.Items.Clear();
            ctrUnidades.lblContadorTrayectoGranjas.Content = list.Count;
            foreach (TableroControlUnidades item in list)
            {
                ListViewItem lvl = new ListViewItem()
                {
                    Content = item,
                    Foreground = new SolidColorBrush(item.diferenciaLlegadaGranja != "00:00" ? Colors.Red : Colors.Black),
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    FontSize = 9.5,
                    //Background = System.Windows.Media.Brushes.Silver
                };

                ContextMenu menu = new ContextMenu();
                MenuItem itemMenu = new MenuItem { Header = "AGREGAR COMENTARIO", Tag = item };
                itemMenu.Click += ItemMenu_Click;
                menu.Items.Add(itemMenu);

                lvl.ContextMenu = menu;

                ToolTip too = new System.Windows.Controls.ToolTip();
                StackPanel stpContentToolTip = new StackPanel { Orientation = Orientation.Vertical };
                if (item.fechaEntradaTallerTrayectoDestino != null)
                {
                    GroupBox gbxTaller = new GroupBox { Header = "TALLER" };
                    StackPanel stpContentGbxTaller = new StackPanel { Orientation = Orientation.Vertical, HorizontalAlignment = HorizontalAlignment.Left };

                    StackPanel stpEntrada = new StackPanel { Orientation = Orientation.Horizontal };
                    stpEntrada.Children.Add(
                        new Label
                        {
                            Content = "Entrada:",
                            Width = 80,
                            FontWeight = FontWeights.Medium,
                            HorizontalContentAlignment = HorizontalAlignment.Right
                        });
                    stpEntrada.Children.Add(
                        new Label
                        {
                            Content = item.fechaEntradaTallerTrayectoDestino.Value.ToString("dd/MM HH:mm"),
                            Width = 100,
                            FontWeight = FontWeights.Bold,
                            HorizontalContentAlignment = HorizontalAlignment.Right
                        });

                    StackPanel stpSalida = new StackPanel { Orientation = Orientation.Horizontal };
                    stpSalida.Children.Add(
                        new Label
                        {
                            Content = "Salida:",
                            Width = 80,
                            FontWeight = FontWeights.Medium,
                            HorizontalContentAlignment = HorizontalAlignment.Right
                        });
                    stpSalida.Children.Add(
                        new Label
                        {
                            Content = item.fechaSalidaTallerTrayectoDestino == null ? string.Empty : item.fechaSalidaTallerTrayectoDestino.Value.ToString("dd/MM HH:mm"),
                            Width = 100,
                            FontWeight = FontWeights.Bold,
                            HorizontalContentAlignment = HorizontalAlignment.Right
                        });

                    StackPanel stpEstadia = new StackPanel { Orientation = Orientation.Horizontal };
                    stpEstadia.Children.Add(
                        new Label
                        {
                            Content = "Duración:",
                            Width = 80,
                            FontWeight = FontWeights.Medium,
                            HorizontalContentAlignment = HorizontalAlignment.Right
                        });
                    stpEstadia.Children.Add(
                        new Label
                        {
                            Content = item.tiempoTallerTrayectoDestino,
                            Width = 100,
                            FontWeight = FontWeights.Bold,
                            HorizontalContentAlignment = HorizontalAlignment.Right
                        });

                    stpContentGbxTaller.Children.Add(stpEntrada);
                    stpContentGbxTaller.Children.Add(stpSalida);
                    stpContentGbxTaller.Children.Add(stpEstadia);

                    gbxTaller.Content = stpContentGbxTaller;
                    stpContentToolTip.Children.Add(gbxTaller);
                }

                foreach (var comentario in item.listaComentarios)
                {
                    stpContentToolTip.Children.Add(new controlComentario(comentario.usuario, comentario.fecha, comentario.comentario));
                }
                too.Content = stpContentToolTip;
                lvl.ToolTip = too;

                listLvl.Add(lvl);
            }
            foreach (var item in listLvl)
            {
                ctrUnidades.lvlDestinos.Items.Add(item);
            }
            lblContadorEventos.Content = Convert.ToInt32(ctrUnidades.lblContadorTrayectoGranjas.Content) + Convert.ToInt32(ctrUnidades.lblContadorTrayectoPlanta.Content) +
                Convert.ToInt32(ctrUnidades.lblContadorGranjas.Content) + Convert.ToInt32(ctrUnidades.lblContadorPlanta.Content) + Convert.ToInt32(ctrUnidades.lblContadorTaller.Content);

        }

        private void ItemMenu_Click(object sender, RoutedEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                if (sender != null)
                {
                    MenuItem menuItem = sender as MenuItem;
                    agregarComentario(menuItem.Tag as TableroControlUnidades);

                }

            });
        }
        private async void agregarComentario(TableroControlUnidades tablero)
        {
            Dispatcher.Invoke(() =>
            {
                new AddComentariosEventoTablero(this.usuario, tablero).ShowDialog();
            });
        }

        Usuario usuario = null;
        //splashInicio splash = new splashInicio();
        bool verTaller = false;
        private async void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            //Dispatcher.Invoke(() =>
            //{               
            //    splash.Show();
            //});
            try
            {
                Cursor = Cursors.Wait;
                usuario = inicio._usuario;
                lblUsuario.Content = usuario.nombreUsuario.ToUpper();
                ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
                ctrEmpresa.loaded(usuario.empresa, (new PrivilegioSvc().consultarPrivilegio("TABLERO_MULTI_EMPRESA", usuario.idUsuario).typeResult == ResultTypes.success));

                cbxZonasOperativas.SelectedItem = cbxZonasOperativas.Items.Cast<ZonaOperativa>().ToList().Find(s => s.idZonaOperativa == usuario.zonaOperativa.idZonaOperativa);
                cbxOperacion.SelectedItem = cbxOperacion.Items.Cast<OperacionFletera>().ToList().Find(s => s.cliente.clave == usuario.cliente.clave);

                stpZonas.IsEnabled = new PrivilegioSvc().consultarPrivilegio("TABLERO_MULTI_ZONA_OPERATIVA", usuario.idUsuario).typeResult == ResultTypes.success;
                stpOperaciones.IsEnabled = new PrivilegioSvc().consultarPrivilegio("TABLERO_MULTI_ZONA_OPERACION", usuario.idUsuario).typeResult == ResultTypes.success;

                verTaller = new PrivilegioSvc().consultarPrivilegio("TABLERO_MOSTRAR_ESTADO_TALLER", usuario.idUsuario).typeResult == ResultTypes.success;
                ctrUnidades.gbxTaller.Visibility = verTaller ? Visibility.Visible : Visibility.Collapsed;

                this.lblContador.Content = $"{this.contador.ToString()}";

                await Task.Run(async () => await BuscarEventosTablero());
                //await BuscarEventosTablero();

                this.timerBusqueda.Elapsed += new ElapsedEventHandler(this.TimerBusqueda_Elapsed);
                this.timerBusqueda.Start();

            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message, "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                Dispatcher.Invoke(() =>
                {
                    lblAccion.Content = ex.Message;
                    lblFecha.Content = DateTime.Now.ToString("HH:mm:ss");
                });
            }
            finally
            {
                Cursor = Cursors.Arrow;
                //splash.Close();
            }
        }
        Empresa empresa = null;
        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            empresa = ctrEmpresa.empresaSelected;
            cargarZonasOperativas();
        }
        List<ZonaOperativa> listaZonaOpertiva = new List<ZonaOperativa>();
        void cargarZonasOperativas()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new ZonaOperativaSvc().getAllZonasOperativas();
                if (resp.typeResult == ResultTypes.success)
                {
                    listaZonaOpertiva = resp.result as List<ZonaOperativa>;
                    cbxZonasOperativas.ItemsSource = listaZonaOpertiva;
                }
                else
                {
                    cbxZonasOperativas.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {
                //ImprimirMensaje.imprimir(ex);
                Dispatcher.Invoke(() =>
                {
                    lblAccion.Content = ex.Message;
                    lblFecha.Content = DateTime.Now.ToString("HH:mm:ss");
                });
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private async void TimerBusqueda_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (this.contador <= 0)
            {
                await Dispatcher.Invoke(async () =>
                 {
                     timerBusqueda.Stop();
                     this.contador = 10;
                     await Task.Run(async () => await BuscarEventosTablero());
                     //Task.Run( await BuscarEventosTablero());
                     //await BuscarEventosTablero();
                     this.timerBusqueda.Start();
                 });
            }
            else
            {
                this.contador--;
            }
            Dispatcher.Invoke(() =>
             this.lblContador.Content = $"{this.contador.ToString()}"
            );
        }
        private void btnDetener_Click(object sender, RoutedEventArgs e)
        {
            if (btnDetener.Content as string == "DETENER")
            {
                timerBusqueda.Stop();
                btnDetener.Content = "INICIAR";
            }
            else if (btnDetener.Content as string == "INICIAR")
            {
                timerBusqueda.Start();
                btnDetener.Content = "DETENER";
            }
        }
        List<OperacionFletera> listaOperaciones = new List<OperacionFletera>();
        private void cbxZonasOperativas_ItemSelectionChanged(object sender, Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventArgs e)
        {
            try
            {
                cbxOperacion.ItemsSource = null;
                chcTodosOperacion.IsChecked = false;
                var listaId = (from s in this.cbxZonasOperativas.SelectedItems.Cast<ZonaOperativa>().ToList<ZonaOperativa>() select s.idZonaOperativa.ToString()).ToList<string>();
                if (listaId != null)
                {
                    var resp = new ClienteSvc().getClientesByZonasOperativas(listaId, empresa.clave);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listaOperaciones = resp.result as List<OperacionFletera>;
                        cbxOperacion.ItemsSource = listaOperaciones;
                    }
                    else if (resp.typeResult != ResultTypes.recordNotFound)
                    {
                        //ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception ex)
            {
                //ImprimirMensaje.imprimir(ex);
                Dispatcher.Invoke(() =>
                {
                    lblAccion.Content = ex.Message;
                    lblFecha.Content = DateTime.Now.ToString("HH:mm:ss");
                });
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void cbxOperacion_ItemSelectionChanged(object sender, Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;

            }
            catch (Exception ex)
            {
                //ImprimirMensaje.imprimir(ex);
                Dispatcher.Invoke(() =>
                {
                    lblAccion.Content = ex.Message;
                    lblFecha.Content = DateTime.Now.ToString("HH:mm:ss");
                });
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                CheckBox chc = sender as CheckBox;
                switch (chc.Name)
                {
                    case "chcTodosZonaOperativa":
                        cbxZonasOperativas.SelectedItems.Clear();
                        if (chc.IsChecked.Value)
                        {
                            foreach (var item in cbxZonasOperativas.Items)
                            {
                                cbxZonasOperativas.SelectedItems.Add(item);
                            }
                        }
                        break;
                    case "chcTodosOperacion":
                        cbxOperacion.SelectedItems.Clear();
                        if (chc.IsChecked.Value)
                        {
                            foreach (var item in cbxOperacion.Items)
                            {
                                cbxOperacion.SelectedItems.Add(item);
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                //ImprimirMensaje.imprimir(ex);
                Dispatcher.Invoke(() =>
                {
                    lblAccion.Content = ex.Message;
                    lblFecha.Content = DateTime.Now.ToString("HH:mm:ss");
                });
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
