﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class CatEmpresasServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getCatEmpresasxFiltro(int idEmpresa = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idEmpresa > 0)
                    {
                        filtro = " where idEmpresa = " + idEmpresa + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
       SELECT c.idEmpresa,
        c.RazonSocial,
        c.NomBaseDatosCOM,
        c.CIDCLIENTEPROVEEDOR
        FROM dbo.CatEmpresas c
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        connection.Open();
                        List<CatEmpresas> lista = new List<CatEmpresas>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CatEmpresas catalogo = new CatEmpresas
                                {
                                    idEmpresa = (int)sqlReader["idEmpresa"],
                                    RazonSocial = (string)sqlReader["RazonSocial"],
                                    NomBaseDatosCOM = (string)sqlReader["NomBaseDatosCOM"],
                                    CIDCLIENTEPROVEEDOR = (int)sqlReader["CIDCLIENTEPROVEEDOR"]
                                };
                                lista.Add(catalogo);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
