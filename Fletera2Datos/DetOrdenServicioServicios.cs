﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class DetOrdenServicioServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getDetOrdenServicioxFilter(int idOrdenSer = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idOrdenSer > 0)
                    {
                        filtro = " where d.idOrdenSer = " + idOrdenSer + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT  CONVERT(BIT,0) AS Seleccionado,
        d.idOrdenSer,
        d.idOrdenActividad,
        ISNULL(d.idServicio,0) AS idServicio,
        ISNULL(d.idActividad,0) AS idActividad,
        d.DuracionActHr,
        d.NoPersonal,
        d.id_proveedor,
        d.Estatus,
        d.CostoManoObra,
        d.CostoProductos,
        isnull(d.FechaDiag,getdate()) as FechaDiag,
        ISNULL(d.UsuarioDiag,'') AS UsuarioDiag,
        ISNULL(d.FechaAsig,GETDATE()) AS FechaAsig,
        ISNULL(d.UsuarioAsig,'') AS UsuarioAsig,
        ISNULL(d.FechaPausado,GETDATE()) AS FechaPausado,
        isnull(d.NotaPausado,'') as NotaPausado,
        ISNULL(d.FechaTerminado,GETDATE()) AS FechaTerminado,
        isnull(d.UsuarioTerminado,'') as UsuarioTerminado,
        d.idOrdenTrabajo,
        d.idDivision,
        isnull(d.idLlanta,0) as idLlanta,
        isnull(d.PosLlanta,0) as PosLlanta,
        isnull(d.NotaDiagnostico,'') as NotaDiagnostico,
        isnull(d.FaltInsumo,0) as FaltInsumo,
        isnull(d.Pendiente,0) as Pendiente,
        dr.idFamilia,
        dr.NotaRecepcion,
        dr.isCancelado,
        isnull(dr.motivoCancelacion,'') AS motivoCancelacion,
        dr.idPersonalResp,
        dr.idPersonalAyu1,
        dr.idPersonalAyu2,
        dr.NotaRecepcionA,
        --CASE WHEN d.idActividad > 0 THEN ca.NombreAct  ELSE dr.NotaRecepcion END AS NotaRecepcionF,
        ISNULL(CASE WHEN d.idActividad > 0 THEN ca.NombreAct  ELSE dr.NotaRecepcion END,'') AS NotaRecepcionF,
		dr.NotaRecepcionA AS FallaReportada,
		ISNULL(cs.NomServicio, '') AS NomServicio,
		CASE WHEN d.idActividad > 0 THEN 'ACTIVIDAD'  ELSE 'FALLA REPORTADA' END AS TipoNotaRecepcionF,
		ISNULL(pr.NombreCompleto,'') AS NomPersonalResp,
		ISNULL(pa1.NombreCompleto,'') AS NomPersonalAyu1,
		ISNULL(pa2.NombreCompleto,'') AS NomPersonalAyu2,
        ISNULL(ca.CIDPRODUCTO,0) AS CIDPRODUCTO_ACT,
		d.CIDPRODUCTO_SERV,
		d.Cantidad_CIDPRODUCTO_SERV,
		d.Precio_CIDPRODUCTO_SERV
        FROM dbo.DetOrdenServicio d
        INNER JOIN dbo.DetRecepcionOS dr ON dr.idOrdenSer = d.idOrdenSer AND dr.idOrdenTrabajo = d.idOrdenTrabajo
		left JOIN dbo.CatServicios cs ON d.idServicio = cs.idServicio
		LEFT JOIN dbo.CatActividades ca ON ca.idActividad = d.idActividad
        LEFT JOIN dbo.CatPersonal pr ON pr.idPersonal = dr.idPersonalResp
		LEFT JOIN dbo.CatPersonal pa1 ON pa1.idPersonal = dr.idPersonalAyu1
		LEFT JOIN dbo.CatPersonal pa2 ON pa2.idPersonal = dr.idPersonalAyu2
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        comand.Parameters.Add(new SqlParameter("@idOrdenSer", idOrdenSer));


                        connection.Open();
                        List<DetOrdenServicio> lista = new List<DetOrdenServicio>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetOrdenServicio folio = new DetOrdenServicio
                                {
                                    Seleccionado = (bool)sqlReader["Seleccionado"],
                                    idOrdenSer = (int)sqlReader["idOrdenSer"],
                                    idOrdenActividad = (int)sqlReader["idOrdenActividad"],
                                    idServicio = (int)sqlReader["idServicio"],
                                    idActividad = (int)sqlReader["idActividad"],
                                    DuracionActHr = (decimal)sqlReader["DuracionActHr"],
                                    NoPersonal = (int)sqlReader["NoPersonal"],
                                    id_proveedor = (int)sqlReader["id_proveedor"],
                                    Estatus = (string)sqlReader["Estatus"],
                                    CostoManoObra = (decimal)sqlReader["CostoManoObra"],
                                    CostoProductos = (decimal)sqlReader["CostoProductos"],
                                    FechaDiag = (DateTime)sqlReader["FechaDiag"],
                                    UsuarioDiag = (string)sqlReader["UsuarioDiag"],
                                    FechaAsig = (DateTime)sqlReader["FechaAsig"],
                                    UsuarioAsig = (string)sqlReader["UsuarioAsig"],
                                    FechaPausado = (DateTime)sqlReader["FechaPausado"],
                                    NotaPausado = (string)sqlReader["NotaPausado"],
                                    FechaTerminado = (DateTime)sqlReader["FechaTerminado"],
                                    UsuarioTerminado = (string)sqlReader["UsuarioTerminado"],
                                    idOrdenTrabajo = (int)sqlReader["idOrdenTrabajo"],
                                    idDivision = (int)sqlReader["idDivision"],
                                    idLlanta = (string)sqlReader["idLlanta"],
                                    PosLlanta = (int)sqlReader["PosLlanta"],
                                    NotaDiagnostico = (string)sqlReader["NotaDiagnostico"],
                                    FaltInsumo = (bool)sqlReader["FaltInsumo"],
                                    Pendiente = (bool)sqlReader["Pendiente"],
                                    idFamilia = (int)sqlReader["idFamilia"],
                                    NotaRecepcion = (string)sqlReader["NotaRecepcion"],
                                    isCancelado = (bool)sqlReader["isCancelado"],
                                    motivoCancelacion = (string)sqlReader["motivoCancelacion"],
                                    idPersonalResp = (int)sqlReader["idPersonalResp"],
                                    idPersonalAyu1 = (int)sqlReader["idPersonalAyu1"],
                                    idPersonalAyu2 = (int)sqlReader["idPersonalAyu2"],
                                    NotaRecepcionA = (string)sqlReader["NotaRecepcionA"],
                                    NotaRecepcionF = (string)sqlReader["NotaRecepcionF"],
                                    FallaReportada = (string)sqlReader["FallaReportada"],
                                    NomServicio = (string)sqlReader["NomServicio"],
                                    TipoNotaRecepcionF = (string)sqlReader["TipoNotaRecepcionF"],
                                    NomPersonalResp = (string)sqlReader["NomPersonalResp"],
                                    NomPersonalAyu1 = (string)sqlReader["NomPersonalAyu1"],
                                    NomPersonalAyu2 = (string)sqlReader["NomPersonalAyu2"],
                                    CIDPRODUCTO_SERV = (int)sqlReader["CIDPRODUCTO_SERV"],
                                    Cantidad_CIDPRODUCTO_SERV = (decimal)sqlReader["Cantidad_CIDPRODUCTO_SERV"],
                                    Precio_CIDPRODUCTO_SERV = (decimal)sqlReader["Precio_CIDPRODUCTO_SERV"],
                                    CIDPRODUCTO_ACT = (int)sqlReader["CIDPRODUCTO_ACT"]

                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult getDetalleOSVistaxFilter(int idOrdenSer = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idOrdenSer > 0)
                    {
                        filtro = " where dos.idOrdenSer = " + idOrdenSer + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT CONVERT(BIT,0) AS Seleccionado,
        dos.idOrdenSer,
        dos.idOrdenTrabajo,
        ros.NotaRecepcion AS Actividad,
        dos.Estatus,
        ros.idPersonalResp,
        ISNULL(pr.NombreCompleto,'') AS NomResponsable,
        ros.idPersonalAyu1,
        ISNULL(pa1.NombreCompleto,'') AS NomAyudante1,
        ros.idPersonalAyu2,
        ISNULL(pa2.NombreCompleto,'') AS NomAyudante2,
        dos.DuracionActHr,
        dos.CIDPRODUCTO_SERV,
        dos.Cantidad_CIDPRODUCTO_SERV,
        dos.Precio_CIDPRODUCTO_SERV,
        isnull(p.CCODIGOPRODUCTO,'') as CCODIGOPRODUCTO,
        isnull(p.CNOMBREPRODUCTO,'') as CNOMBREPRODUCTO,
        isnull(dos.idActividad,0) as idActividad,
        isnull(dos.idServicio,0) as idServicio
        FROM dbo.DetOrdenServicio dos
        INNER JOIN dbo.DetRecepcionOS ros ON ros.idOrdenTrabajo = dos.idOrdenTrabajo AND ros.idOrdenSer = dos.idOrdenSer
        left JOIN dbo.CatPersonal pr ON ros.idPersonalResp = pr.idPersonal
        left JOIN dbo.CatPersonal pa1 ON ros.idPersonalAyu1 = pa1.idPersonal
        left JOIN dbo.CatPersonal pa2 ON ros.idPersonalAyu2 = pa1.idPersonal
        LEFT JOIN dbo.CatProductosCOM p ON dos.CIDPRODUCTO_SERV = p.CIDPRODUCTO
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idOrdenSer", idOrdenSer));


                        connection.Open();
                        List<DetalleOSVista> lista = new List<DetalleOSVista>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetalleOSVista folio = new DetalleOSVista
                                {
                                    Seleccionado = (bool)sqlReader["Seleccionado"],
                                    idOrdenSer = (int)sqlReader["idOrdenSer"],
                                    idOrdenTrabajo = (int)sqlReader["idOrdenTrabajo"],
                                    Actividad = (string)sqlReader["Actividad"],
                                    Estatus = (string)sqlReader["Estatus"],
                                    idPersonalResp = (int)sqlReader["idPersonalResp"],
                                    NomResponsable = (string)sqlReader["NomResponsable"],
                                    idPersonalAyu1 = (int)sqlReader["idPersonalAyu1"],
                                    NomAyudante1 = (string)sqlReader["NomAyudante1"],
                                    idPersonalAyu2 = (int)sqlReader["idPersonalAyu2"],
                                    NomAyudante2 = (string)sqlReader["NomAyudante2"],
                                    DuracionActHr = (decimal)sqlReader["DuracionActHr"],
                                    CIDPRODUCTO_SERV = (int)sqlReader["CIDPRODUCTO_SERV"],
                                    CCODIGOPRODUCTO_SERV = (string)sqlReader["CCODIGOPRODUCTO"],
                                    CNOMBREPRODUCTO_SERV = (string)sqlReader["CNOMBREPRODUCTO"],
                                    Cantidad_CIDPRODUCTO_SERV = (decimal)sqlReader["Cantidad_CIDPRODUCTO_SERV"],
                                    Precio_CIDPRODUCTO_SERV = (decimal)sqlReader["Precio_CIDPRODUCTO_SERV"],
                                    idActividad = (int)sqlReader["idActividad"],
                                    idServicio = (int)sqlReader["idServicio"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult getDOSInsumosVistaxFilter(int idOrdenSer = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idOrdenSer > 0)
                    {
                        filtro = " where dos.idOrdenSer = " + idOrdenSer + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT dap.idOrdenSer,
        dap.idOrdenActividad,
        ros.NotaRecepcion,
        dap.idProducto,
        p.CCODIGOPRODUCTO,
        p.CNOMBREPRODUCTO,
        dap.Cantidad,
        dap.Costo,
        dap.CantCompra,
        dap.CantSalida
        FROM dbo.DetOrdenActividadProductos dap
        INNER JOIN dbo.DetOrdenServicio dos ON dos.idOrdenSer = dap.idOrdenSer AND dos.idOrdenActividad = dap.idOrdenActividad
        INNER JOIN dbo.DetRecepcionOS ros ON ros.idOrdenTrabajo = dos.idOrdenTrabajo AND ros.idOrdenSer = dos.idOrdenSer
        INNER JOIN dbo.CatProductosCOM p ON dap.idProducto = p.CIDPRODUCTO
        left JOIN dbo.CabPreSalida cabpresal ON cabpresal.idOrdenTrabajo = dos.idOrdenTrabajo
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idOrdenSer", idOrdenSer));


                        connection.Open();
                        List<DOSInsumosVista> lista = new List<DOSInsumosVista>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DOSInsumosVista folio = new DOSInsumosVista
                                {
                                    idOrdenSer = (int)sqlReader["idOrdenSer"],
                                    idOrdenActividad = (int)sqlReader["idOrdenActividad"],
                                    NotaRecepcion = (string)sqlReader["NotaRecepcion"],
                                    idProducto = (int)sqlReader["idProducto"],
                                    CCODIGOPRODUCTO = (string)sqlReader["CCODIGOPRODUCTO"],
                                    CNOMBREPRODUCTO = (string)sqlReader["CNOMBREPRODUCTO"],
                                    Cantidad = (decimal)sqlReader["Cantidad"],
                                    Costo = (decimal)sqlReader["Costo"],
                                    CantCompra = (decimal)sqlReader["CantCompra"],
                                    CantSalida = (decimal)sqlReader["CantSalida"]

                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaDetOrdenServicio(ref DetOrdenServicio cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idOrdenTrabajo = 0
        BEGIN
        set @id = (select ISNULL(MAX(idOrdenTrabajo) + 1, 1) from DetOrdenServicio)
        INSERT  INTO DetOrdenServicio
        ( idOrdenSer ,
          idServicio ,
          idActividad ,
          DuracionActHr ,
          NoPersonal ,
          id_proveedor ,
          Estatus ,
          CostoManoObra ,
          CostoProductos ,
          idOrdenTrabajo ,
          idDivision ,
          idLlanta ,
          PosLlanta ,
          NotaDiagnostico 
        )
VALUES  ( @idOrdenSer ,
          @idServicio ,
          @idActividad ,
          @DuracionActHr ,
          @NoPersonal ,
          @id_proveedor ,
          @Estatus ,
          @CostoManoObra ,
          @CostoProductos ,
          @id ,
          @idDivision ,
          @idLlanta ,
          @PosLlanta ,
          @NotaDiagnostico 
        );
                      
            SET @valor = 0;
            SET @mensaje = 'orden de servicio Agregado con Exito';
            --SET @id = @@IDENTITY;
            --SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  DetOrdenServicio
SET     idServicio = @idServicio ,
        idActividad = @idActividad ,
        DuracionActHr = @DuracionActHr ,
        NoPersonal = @NoPersonal ,
        id_proveedor = @id_proveedor ,
        Estatus = @Estatus ,
        CostoManoObra = @CostoManoObra ,
        CostoProductos = @CostoProductos ,
        idDivision = @idDivision ,
        idLlanta = @idLlanta ,
        PosLlanta = @PosLlanta ,
        NotaDiagnostico = @NotaDiagnostico,
        FechaAsig= @FechaAsig,
        UsuarioAsig= @UsuarioAsig,
        FechaTerminado= @FechaTerminado,
        UsuarioTerminado= @UsuarioTerminado,
        CIDPRODUCTO_SERV= @CIDPRODUCTO_SERV,
        Cantidad_CIDPRODUCTO_SERV= @Cantidad_CIDPRODUCTO_SERV,
        Precio_CIDPRODUCTO_SERV= @Precio_CIDPRODUCTO_SERV
WHERE   idOrdenTrabajo = @idOrdenTrabajo;
            SET @valor = 0;
            SET @mensaje = 'Tipo de Unidad actualizado con Exito';
            SET @id = @idOrdenTrabajo;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idOrdenSer", cat.idOrdenSer));
                        comand.Parameters.Add(new SqlParameter("idOrdenActividad", cat.idOrdenActividad));
                        comand.Parameters.Add(new SqlParameter("idServicio", cat.idServicio));
                        comand.Parameters.Add(new SqlParameter("idActividad", cat.idActividad));
                        comand.Parameters.Add(new SqlParameter("DuracionActHr", cat.DuracionActHr));
                        comand.Parameters.Add(new SqlParameter("NoPersonal", cat.NoPersonal));
                        comand.Parameters.Add(new SqlParameter("id_proveedor", cat.id_proveedor));
                        comand.Parameters.Add(new SqlParameter("Estatus", cat.Estatus));
                        comand.Parameters.Add(new SqlParameter("CostoManoObra", cat.CostoManoObra));
                        comand.Parameters.Add(new SqlParameter("CostoProductos", cat.CostoProductos));
                        comand.Parameters.Add(new SqlParameter("idOrdenTrabajo", cat.idOrdenTrabajo));
                        comand.Parameters.Add(new SqlParameter("idDivision", cat.idDivision));
                        comand.Parameters.Add(new SqlParameter("idLlanta", cat.idLlanta));
                        comand.Parameters.Add(new SqlParameter("PosLlanta", cat.PosLlanta));
                        comand.Parameters.Add(new SqlParameter("NotaDiagnostico", cat.NotaDiagnostico));
                        comand.Parameters.Add(new SqlParameter("FechaAsig", cat.FechaAsig));
                        comand.Parameters.Add(new SqlParameter("UsuarioAsig", cat.UsuarioAsig));
                        comand.Parameters.Add(new SqlParameter("FechaTerminado", cat.FechaTerminado));
                        comand.Parameters.Add(new SqlParameter("UsuarioTerminado", cat.UsuarioTerminado));
                        comand.Parameters.Add(new SqlParameter("CIDPRODUCTO_SERV", cat.CIDPRODUCTO_SERV));
                        comand.Parameters.Add(new SqlParameter("Cantidad_CIDPRODUCTO_SERV", cat.Cantidad_CIDPRODUCTO_SERV));
                        comand.Parameters.Add(new SqlParameter("Precio_CIDPRODUCTO_SERV", cat.Precio_CIDPRODUCTO_SERV));




                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idOrdenTrabajo = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
