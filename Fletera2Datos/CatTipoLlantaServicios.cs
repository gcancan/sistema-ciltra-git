﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Fletera2Datos
{
    public class CatTipoLlantaServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getCatTipoLlantaxFilter(int idTipoLLanta = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idTipoLLanta > 0)
                    {
                        filtro = " where C.idTipoLLanta = " + idTipoLLanta + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT  c.idTipoLLanta ,
        c.Descripcion
        FROM    dbo.CatTipoLlanta c
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<CatTipoLlanta> lista = new List<CatTipoLlanta>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CatTipoLlanta cat = new CatTipoLlanta
                                {
                                    idTipoLLanta = (int)sqlReader["idTipoLLanta"],
                                    Descripcion = (string)sqlReader["Descripcion"],
                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCatTipoLlanta(ref CatTipoLlanta cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idConcepto = 0
        BEGIN
       INSERT  INTO CatTipoLlanta
        ( Descripcion )
VALUES  (  @Descripcio );

                      
            SET @valor = 0;
            SET @mensaje = 'Tipo de Llanta Agregado con Exito';
            SET @id = @@IDENTITY;
            --SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
      UPDATE  CatTipoLlanta
SET     Descripcion = @Descripcion
WHERE   idTipoLLanta = @idTipoLLanta;
            SET @valor = 0;
            SET @mensaje = 'Tipo de Llanta actualizado con Exito';
            SET @id = @idTipoLLanta;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idTipoLLanta", cat.idTipoLLanta));
                        comand.Parameters.Add(new SqlParameter("Descripcion", cat.Descripcion));

                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idTipoLLanta = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

    }//
}
