﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Fletera2Datos
{
    public class CatClasLlanServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getCatClasLlanxFilter(int idClasLlan = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idClasLlan > 0)
                    {
                        filtro = " where C.idClasLlan = " + idClasLlan + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
       SELECT  c.idClasLlan ,
        c.NombreClasLlan ,
        c.NoEjes ,
        c.NoLLantas
        FROM    dbo.CatClasLlan c
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<CatClasLlan> lista = new List<CatClasLlan>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CatClasLlan cat = new CatClasLlan
                                {
                                    idClasLlan = (int)sqlReader["idClasLlan"],
                                    NombreClasLlan = (string)sqlReader["NombreClasLlan"],
                                    NoEjes = (byte)sqlReader["NoEjes"],
                                    NoLLantas = (byte)sqlReader["NoLlantas"]
                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCatClasLlan(ref CatClasLlan cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idConcepto = 0
        BEGIN
        INSERT  INTO CatClasLlan
        ( NombreClasLlan ,
          NoEjes ,
          NoLLantas)
VALUES  ( @NombreClasLlan ,
          @NoEjes ,
          @NoLLantas);
                      
            SET @valor = 0;
            SET @mensaje = 'Tipo de Llanta Agregado con Exito';
            SET @id = @@IDENTITY;
            --SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  CatClasLlan
SET     NombreClasLlan = @NombreClasLlan ,
        NoEjes = @NoEjes ,
        NoLLantas = @NoLLantas
WHERE   idClasLlan = @idClasLlan;
            SET @valor = 0;
            SET @mensaje = 'Tipo de Llanta actualizado con Exito';
            SET @id = @idClasLlan;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idClasLlan", cat.idClasLlan));
                        comand.Parameters.Add(new SqlParameter("NombreClasLlan", cat.NombreClasLlan));
                        comand.Parameters.Add(new SqlParameter("NoEjes", cat.NoEjes));
                        comand.Parameters.Add(new SqlParameter("NoLlantas", cat.NoLLantas));


                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idClasLlan = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
