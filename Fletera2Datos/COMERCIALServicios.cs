﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Fletera2Datos
{
    public class COMERCIALServicios
    {
        private string strsql;
        private string filtro;
        //PRODUCTOS
        public OperationResult getadmProductosxFilter(int CIDPRODUCTO = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactoryTaller())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (CIDPRODUCTO > 0)
                    {
                        filtro = " where p.CIDPRODUCTO = " + CIDPRODUCTO + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT p.CIDPRODUCTO,
        p.CCODIGOPRODUCTO,
        p.CNOMBREPRODUCTO,
        p.CIDUNIDADBASE,
        u.CNOMBREUNIDAD,
        p.CPRECIO1,
        p.CIMPUESTO1
        FROM dbo.admProductos p
        left JOIN dbo.admUnidadesMedidaPeso u ON p.CIDUNIDADBASE = u.CIDUNIDAD
        {0} 
        --left JOIN dbo.admClasificacionesValores cv5 ON cv5.CIDCLASIFICACION = 29 AND cv5.CIDVALORCLASIFICACION = p.CIDVALORCLASIFICACION5
        
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {


                        connection.Open();
                        List<admProductos> lista = new List<admProductos>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                admProductos folio = new admProductos
                                {
                                    CIDPRODUCTO = (int)sqlReader["CIDPRODUCTO"],
                                    CCODIGOPRODUCTO = (string)sqlReader["CCODIGOPRODUCTO"],
                                    CNOMBREPRODUCTO = (string)sqlReader["CNOMBREPRODUCTO"],
                                    CIDUNIDADBASE = (int)sqlReader["CIDUNIDADBASE"],
                                    CPRECIO1 = (double)sqlReader["CPRECIO1"],
                                    CIMPUESTO1 = (double)sqlReader["CIMPUESTO1"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult getadmClientesxFilter(int CIDCLIENTEPROVEEDOR = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactoryTaller())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (CIDCLIENTEPROVEEDOR > 0)
                    {
                        filtro = " where c.CIDCLIENTEPROVEEDOR = " + CIDCLIENTEPROVEEDOR + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT c.CIDCLIENTEPROVEEDOR,
        c.CCODIGOCLIENTE,
        c.CRAZONSOCIAL,
        c.CRFC,
        ISNULL(d.CNOMBRECALLE,'') AS CNOMBRECALLE,
        ISNULL(d.CNUMEROEXTERIOR,'') AS CNUMEROEXTERIOR,
        ISNULL(d.CCOLONIA,'') AS CCOLONIA,
        isnull(c.CMETODOPAG,'') as CMETODOPAG,
        isnull(c.CUSOCFDI,'') as CUSOCFDI
        FROM dbo.admClientes C
        left JOIN dbo.admDomicilios d ON c.CIDCLIENTEPROVEEDOR = d.CIDCATALOGO AND d.CTIPOCATALOGO = 1
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {


                        connection.Open();
                        List<admClientes> lista = new List<admClientes>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                admClientes folio = new admClientes
                                {
                                    CIDCLIENTEPROVEEDOR = (int)sqlReader["CIDCLIENTEPROVEEDOR"],
                                    CCODIGOCLIENTE = (string)sqlReader["CCODIGOCLIENTE"],
                                    CRAZONSOCIAL = (string)sqlReader["CRAZONSOCIAL"],
                                    CRFC = (string)sqlReader["CRFC"],
                                    CNOMBRECALLE = (string)sqlReader["CNOMBRECALLE"],
                                    CNUMEROEXTERIOR = (string)sqlReader["CNUMEROEXTERIOR"],
                                    CCOLONIA = (string)sqlReader["CCOLONIA"],
                                    CMETODOPAG = (string)sqlReader["CMETODOPAG"],
                                    CUSOCFDI = (string)sqlReader["CUSOCFDI"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
        public OperationResult getadmConceptosxFilter(int CIDCONCEPTODOCUMENTO = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactoryTaller())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (CIDCONCEPTODOCUMENTO > 0)
                    {
                        filtro = " where c.CIDCONCEPTODOCUMENTO = " + CIDCONCEPTODOCUMENTO + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
       SELECT c.CIDCONCEPTODOCUMENTO,
        c.CCODIGOCONCEPTO,
        c.CNOMBRECONCEPTO,
        c.CIDDOCUMENTODE
        FROM dbo.admConceptos c
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {


                        connection.Open();
                        List<admConceptos> lista = new List<admConceptos>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                admConceptos folio = new admConceptos
                                {
                                    CIDCONCEPTODOCUMENTO = (int)sqlReader["CIDCONCEPTODOCUMENTO"],
                                    CCODIGOCONCEPTO = (string)sqlReader["CCODIGOCONCEPTO"],
                                    CNOMBRECONCEPTO = ((string)sqlReader["CNOMBRECONCEPTO"]).Trim(),
                                    CIDDOCUMENTODE = (int)sqlReader["CIDDOCUMENTODE"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
        public OperationResult getadmCostosHistoricosxFilter(int CIDPRODUCTO = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactoryTaller())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (CIDPRODUCTO > 0)
                    {
                        filtro = " where c.CIDPRODUCTO = " + CIDPRODUCTO + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT c.CIDCOSTOH,
        c.CIDPRODUCTO,
        c.CIDALMACEN,
        c.CFECHACOSTOH,
        c.CCOSTOH,
        c.CULTIMOCOSTOH,
        c.CIDMOVIMIENTO,
        p.CCODIGOPRODUCTO,
        p.CNOMBREPRODUCTO
        FROM dbo.admCostosHistoricos c
        INNER JOIN dbo.admProductos p ON p.CIDPRODUCTO = c.CIDPRODUCTO
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {


                        connection.Open();
                        List<admCostosHistoricos> lista = new List<admCostosHistoricos>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                admCostosHistoricos folio = new admCostosHistoricos
                                {
                                    CIDPRODUCTO = (int)sqlReader["CIDPRODUCTO"],
                                    CCODIGOPRODUCTO = (string)sqlReader["CCODIGOPRODUCTO"],
                                    CNOMBREPRODUCTO = (string)sqlReader["CNOMBREPRODUCTO"],
                                    CIDCOSTOH = (int)sqlReader["CIDCOSTOH"],
                                    CIDALMACEN = (int)sqlReader["CIDALMACEN"],
                                    CFECHACOSTOH = (DateTime)sqlReader["CFECHACOSTOH"],
                                    CCOSTOH = (double)sqlReader["CCOSTOH"],
                                    CULTIMOCOSTOH = (double)sqlReader["CULTIMOCOSTOH"],
                                    CIDMOVIMIENTO = (int)sqlReader["CIDMOVIMIENTO"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult getadmExistenciaxProd(int CIDPRODUCTO, string Fecha, int Almacen = 0,
            string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                //aqui me quede
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactoryTaller())
                {
                    #region Consulta_Sql
       //             "where cidproducto = " & IdProducto &
       //"and mov.cfecha <= '" & FormatFecHora(Fecha.ToShortDateString, True, False) & " 23:59:59' " &
       //IIf(Almacen > 0, " AND CIDALMACEN = " & Almacen, "") &

                   filtro = "";
                    filtro = " where mov.CIDPRODUCTO = " + CIDPRODUCTO;
                    //filtro = filtro + " and mov.cfecha <= '" + Fecha + " 23:59:59'";
                    filtro = filtro + " and mov.cfecha <= '" + Fecha + "'";
                    if (Almacen >0)
                    {
                        filtro = filtro + " AND CIDALMACEN = " + Almacen;
                    }

                    if (filtroSinWhere != "")
                    {
                        filtro = filtro + " and " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        select mov.cidproducto,
        sum (case when tip.CAFECTAEXISTENCIA = 1 then (mov.CUNIDADES * 1) when tip.CAFECTAEXISTENCIA = 2 then (mov.CUNIDADES * -1) end) as Existencia 
        from admMovimientos mov 
        inner join admDocumentos doc on mov.ciddocumento = doc.ciddocumento 
        inner join admConceptos con on doc.CIDCONCEPTODOCUMENTO = con.CIDCONCEPTODOCUMENTO 
        inner join admDocumentosModelo tip on con.CIDDOCUMENTODE = tip.CIDDOCUMENTODE 
        {0}
        group by mov.cidproducto
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {


                        connection.Open();
                        List<admExistencia> lista = new List<admExistencia>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                admExistencia folio = new admExistencia
                                {
                                    cidproducto = (int)sqlReader["cidproducto"],
                                    Existencia = (double)sqlReader["Existencia"]

                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
        public OperationResult getadmDocumentosxFilter(int CIDDOCUMENTO = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactoryTaller())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (CIDDOCUMENTO > 0)
                    {
                        filtro = " where c.CIDDOCUMENTO = " + CIDDOCUMENTO + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
    SELECT  c.CIDDOCUMENTO ,
        c.CIDDOCUMENTODE ,
        c.CIDCONCEPTODOCUMENTO ,
        c.CSERIEDOCUMENTO ,
        c.CFOLIO ,
        c.CFECHA ,
        c.CIDCLIENTEPROVEEDOR ,
        c.CREFERENCIA ,
        c.CCANCELADO ,
        c.CNETO ,
        c.CIMPUESTO1 ,
        c.CTOTAL ,
        c.CMETODOPAG
    FROM    dbo.admDocumentos c
        {0} 
        --left JOIN dbo.admClasificacionesValores cv5 ON cv5.CIDCLASIFICACION = 29 AND cv5.CIDVALORCLASIFICACION = p.CIDVALORCLASIFICACION5
        
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {


                        connection.Open();
                        List<admDocumentos> lista = new List<admDocumentos>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                admDocumentos folio = new admDocumentos
                                {
                                    CIDDOCUMENTO = (int)sqlReader["CIDDOCUMENTO"],
                                    CIDDOCUMENTODE = (int)sqlReader["CIDDOCUMENTODE"],
                                    CIDCONCEPTODOCUMENTO = (int)sqlReader["CIDCONCEPTODOCUMENTO"],
                                    CSERIEDOCUMENTO = (string)sqlReader["CSERIEDOCUMENTO"],
                                    CFOLIO = (Double)sqlReader["CFOLIO"],
                                    CFECHA = (DateTime)sqlReader["CFECHA"],
                                    CIDCLIENTEPROVEEDOR = (int)sqlReader["CIDCLIENTEPROVEEDOR"],
                                    CREFERENCIA = (string)sqlReader["CREFERENCIA"],
                                    CCANCELADO = (int)sqlReader["CCANCELADO"],
                                    CNETO = (Double)sqlReader["CNETO"],
                                    CIMPUESTO1 = (Double)sqlReader["CIMPUESTO1"],
                                    CTOTAL = (Double)sqlReader["CTOTAL"],
                                    CMETODOPAG = (string)sqlReader["CMETODOPAG"]

                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
        public OperationResult getadmAlmacenesxFilter(int CIDALMACEN = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactoryTaller())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (CIDALMACEN > 0)
                    {
                        filtro = " where c.CIDALMACEN = " + CIDALMACEN + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT c.CIDALMACEN,
        c.CCODIGOALMACEN,
        c.CNOMBREALMACEN
        FROM dbo.admAlmacenes C
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {


                        connection.Open();
                        List<admAlmacenes> lista = new List<admAlmacenes>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                admAlmacenes folio = new admAlmacenes
                                {
                                    CIDALMACEN = (int)sqlReader["CIDALMACEN"],
                                    CCODIGOALMACEN = (string)sqlReader["CCODIGOALMACEN"],
                                    CNOMBREALMACEN = (string)sqlReader["CNOMBREALMACEN"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
