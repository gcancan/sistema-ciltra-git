﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Fletera2Datos
{
    public class CatTipoServicioServicios
    {
        private string strsql;
        private string filtro;

        public OperationResult getCatTipoServicioxFilter(int idTipoServicio = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idTipoServicio > 0)
                    {
                        filtro = " where C.idTipoServicio = " + idTipoServicio + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT c.idTipoServicio,
        c.NomTipoServicio
        FROM dbo.CatTipoServicio c
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<CatTipoServicio> lista = new List<CatTipoServicio>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CatTipoServicio cat = new CatTipoServicio
                                {
                                    idTipoServicio = (int)sqlReader["idTipoServicio"],
                                    NomTipoServicio = (string)sqlReader["NomTipoServicio"]
                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCatTipoServicio(ref CatTipoServicio cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
        INSERT  INTO CatTipoServicio
            ( idTipoServicio ,
              NomTipoServicio
            )
        VALUES  ( @idTipoServicio ,
              @NomTipoServicio
        );

                      
            SET @valor = 0;
            SET @mensaje = 'Tipo de Unidad Agregado con Exito';
            --SET @id = @@IDENTITY;
            SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  CatTipoServicio
SET     NomTipoServicio = @NomTipoServicio
WHERE   idTipoServicio = @idTipoServicio;

            SET @valor = 0;
            SET @mensaje = 'Tipo de Unidad actualizado con Exito';
            SET @id = @idUnidadTrans;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idTipoServicio", cat.idTipoServicio));
                        comand.Parameters.Add(new SqlParameter("NomTipoServicio", cat.NomTipoServicio));

                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idTipoServicio = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
