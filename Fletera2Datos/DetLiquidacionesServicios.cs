﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class DetLiquidacionesServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getDetLiquidacionesxFilter(int idDetLiq = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idDetLiq > 0)
                    {
                        filtro = " where idDetLiq = " + idDetLiq + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT d.idDetLiq,
        d.idLiquidacion,
        d.Tipo,
        d.NoDocumento,
        d.Descripcion,
        d.Importe,
        d.IVA,
        d.EsAbono
        FROM dbo.DetLiquidaciones d
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        comand.Parameters.Add(new SqlParameter("@idDetLiq", idDetLiq));


                        connection.Open();
                        List<DetLiquidaciones> lista = new List<DetLiquidaciones>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetLiquidaciones folio = new DetLiquidaciones
                                {
                                    idDetLiq = (int)sqlReader["idDetLiq"],
                                    idLiquidacion = (int)sqlReader["idLiquidacion"],
                                    Tipo = (string)sqlReader["Tipo"],
                                    NoDocumento = (int)sqlReader["NoDocumento"],
                                    Descripcion = (string)sqlReader["Descripcion"],
                                    Importe = (decimal)sqlReader["Importe"],
                                    IVA = (decimal)sqlReader["IVA"],
                                    EsAbono = (bool)sqlReader["EsAbono"]
                                    
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCabLiquidaciones(ref DetLiquidaciones folio)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idDetLiq = 0
        BEGIN
            insert into DetLiquidaciones
            (idLiquidacion,Tipo,NoDocumento,Descripcion,Importe,IVA,EsAbono)
            values
            (@idLiquidacion,@Tipo,@NoDocumento,@Descripcion,@Importe,@IVA,@EsAbono)
                      
            SET @valor = 0;
            SET @mensaje = 'Detalle Liquidacion Agregado con Exito';
            SET @id = @@IDENTITY;
        END;
    ELSE
        BEGIN
           UPDATE DetLiquidaciones SET 
            idLiquidacion= @idLiquidacion,
            Tipo= @Tipo,
            NoDocumento= @NoDocumento,
            Descripcion= @Descripcion,
            Importe= @Importe,
            IVA= @IVA,
            EsAbono= @EsAbono 
            WHERE idDetLiq = @idDetLiq;
            
            SET @valor = 0;
            SET @mensaje = 'Detalle Liquidacion actualizado con Exito';
            SET @id = @idDetLiq;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("@idDetLiq", folio.idDetLiq));
                        comand.Parameters.Add(new SqlParameter("@idLiquidacion", folio.idLiquidacion));
                        comand.Parameters.Add(new SqlParameter("@Tipo", folio.Tipo));
                        comand.Parameters.Add(new SqlParameter("@NoDocumento", folio.NoDocumento));
                        comand.Parameters.Add(new SqlParameter("@Descripcion", folio.Descripcion));
                        comand.Parameters.Add(new SqlParameter("@Importe", folio.Importe));
                        comand.Parameters.Add(new SqlParameter("@IVA", folio.IVA));
                        comand.Parameters.Add(new SqlParameter("@EsAbono", folio.EsAbono));

                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            folio.idLiquidacion = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

    }//
}
