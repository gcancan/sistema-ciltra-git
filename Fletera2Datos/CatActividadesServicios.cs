﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Fletera2Datos
{
    public class CatActividadesServicios
    {
        private string strsql;
        private string filtro;

        public OperationResult getCatActividadesxFilter(int idActividad = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idActividad > 0)
                    {
                        filtro = " where C.idActividad = " + idActividad + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
       SELECT  c.idActividad ,
        c.NombreAct ,
        isnull(c.CostoManoObra,0) as CostoManoObra ,
        isnull(c.idFamilia,0) as idFamilia ,
        c.DuracionHoras ,
        isnull(c.idEnum,0) as idEnum,
        isnull(c.CIDPRODUCTO,0) as CIDPRODUCTO,
        isnull(c.CCODIGOPRODUCTO,'') as CCODIGOPRODUCTO
        FROM dbo.CatActividades c
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<CatActividades> lista = new List<CatActividades>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CatActividades cat = new CatActividades
                                {
                                    idActividad = (int)sqlReader["idActividad"],
                                    NombreAct = (string)sqlReader["NombreAct"],
                                    CostoManoObra = (decimal)sqlReader["CostoManoObra"],
                                    idFamilia = (int)sqlReader["idFamilia"],
                                    DuracionHoras = (string)sqlReader["DuracionHoras"],
                                    idEnum = (int)sqlReader["idEnum"],
                                    CIDPRODUCTO = (int)sqlReader["CIDPRODUCTO"],
                                    CCODIGOPRODUCTO = (string)sqlReader["CCODIGOPRODUCTO"]
                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCatActividades(ref CatActividades cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idConcepto = 0
        BEGIN
            INSERT  INTO CatActividades
            ( idActividad ,
              NombreAct ,
              CostoManoObra ,
              idFamilia ,
              DuracionHoras ,
              idEnum)
            VALUES  ( @idActividad ,
              @NombreAct ,
              @CostoManoObra ,
              @idFamilia ,
              @DuracionHoras ,
              @idEnum);
                      
            SET @valor = 0;
            SET @mensaje = 'Actividad Agregado con Exito';
            --SET @id = @@IDENTITY;
            SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  CatActividades
SET     NombreAct = @NombreAct ,
        CostoManoObra = @CostoManoObra ,
        idFamilia = @idFamilia ,
        DuracionHoras = @DuracionHoras ,
        idEnum = @idEnum
WHERE   idActividad = @idActividad;
            SET @valor = 0;
            SET @mensaje = 'Actividad actualizado con Exito';
            SET @id = @idActividad;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idActividad", cat.idActividad));
                        comand.Parameters.Add(new SqlParameter("NombreAct", cat.NombreAct));
                        comand.Parameters.Add(new SqlParameter("CostoManoObra", cat.CostoManoObra));
                        comand.Parameters.Add(new SqlParameter("idFamilia", cat.idFamilia));
                        comand.Parameters.Add(new SqlParameter("DuracionHoras", cat.DuracionHoras));
                        comand.Parameters.Add(new SqlParameter("idEnum", cat.idEnum));

                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idActividad = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
