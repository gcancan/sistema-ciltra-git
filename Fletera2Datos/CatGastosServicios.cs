﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class CatGastosServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getCatGastosxFiltro(int idGasto = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idGasto > 0)
                    {
                        filtro = " where idGasto = " + idGasto + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT g.idGasto,
        g.NomGasto,
        g.Deducible,
        g.ReqComprobante,
        g.ReqAutorizacion,
        g.Activo
        FROM dbo.CatGastos G
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        comand.Parameters.Add(new SqlParameter("@FolioDin", idGasto));


                        connection.Open();
                        List<CatGastos> lista = new List<CatGastos>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CatGastos catalogo = new CatGastos
                                {
                                    idGasto = (int)sqlReader["idGasto"],
                                    NomGasto = (string)sqlReader["NomGasto"],
                                    Deducible = (bool)sqlReader["Deducible"],
                                    ReqComprobante = (bool)sqlReader["ReqComprobante"],
                                    ReqAutorizacion = (bool)sqlReader["ReqAutorizacion"],
                                    Activo = (bool)sqlReader["Activo"]
                                };
                                lista.Add(catalogo);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCatGastos(ref CatGastos catalogo)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idGasto = 0
        BEGIN
            insert into CatGastos
            (NomGasto,Deducible,ReqComprobante,ReqAutorizacion,Activo)
            values
            (@NomGasto,@Deducible,@ReqComprobante,@ReqAutorizacion,@Activo)
                      
            SET @valor = 0;
            SET @mensaje = 'Gasto Agregado con Exito';
            SET @id = @@IDENTITY;

        END;
    ELSE
        BEGIN
           UPDATE CatGastos SET NomGasto = @NomGasto,
            Deducible = @Deducible , 
            ReqComprobante = @ReqComprobante,  
            ReqAutorizacion = @ReqAutorizacion,  
            Activo = @Activo  
            WHERE idGasto = @idGasto;
            SET @valor = 0;
            SET @mensaje = 'Gasto actualizado con Exito';
            SET @id = @idGasto;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("@idGasto", catalogo.idGasto));
                        comand.Parameters.Add(new SqlParameter("@NomGasto", catalogo.NomGasto));
                        comand.Parameters.Add(new SqlParameter("@Deducible", catalogo.Deducible));
                        comand.Parameters.Add(new SqlParameter("@ReqAutorizacion", catalogo.ReqAutorizacion));
                        comand.Parameters.Add(new SqlParameter("@ReqComprobante", catalogo.ReqComprobante));
                        comand.Parameters.Add(new SqlParameter("@Activo", catalogo.Activo));
                        
                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            catalogo.idGasto = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }


    }//
}
