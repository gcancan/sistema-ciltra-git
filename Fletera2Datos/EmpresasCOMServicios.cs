﻿using Core.Models;
using Core.Utils;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class EmpresasCOMServicios
    {
        public OperationResult getEmpresasxId(int CIDEMPRESA = 0)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactoryCOMCONF())
                {
                    using (var comand = connection.CreateCommand())
                    {
                        comand.CommandType = CommandType.Text;
                        #region script
                        comand.CommandText = @"
BEGIN TRY
    --DECLARE @idNacionalidad INT = 1;
    --DECLARE @valor INT;
    --DECLARE @mensaje INT;
    SELECT CIDEMPRESA,
    CNOMBREEMPRESA,
    CRUTADATOS
    FROM dbo.Empresas 
    WHERE   CIDEMPRESA > 1 aND CIDEMPRESA LIKE CASE @CIDEMPRESA
                            WHEN 0 THEN '%'
                            ELSE CONVERT(NVARCHAR(5), @CIDEMPRESA)
                          END;
    IF @@ROWCOUNT > 0
        BEGIN
            SET @valor = 0;
            SET @mensaje = 'Se Encontraron Registros';
        END;
    ELSE
        BEGIN 
            SET @valor = 3;
            SET @mensaje = 'No se encontraron registros';
        END;
END TRY
BEGIN CATCH
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
END CATCH;
";
                        #endregion

                        comand.Parameters.Add(new SqlParameter("@CIDEMPRESA", CIDEMPRESA));

                        var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });

                        connection.Open();
                        var i = comand.ExecuteReader();
                        List<EmpresasCOM> listaReturn = new List<EmpresasCOM>();
                        using (var sqlReader = i)
                        {
                            while (sqlReader.Read())
                            {
                                EmpresasCOM empresa = new EmpresasCOM
                                {
                                    CIDEMPRESA = (int)sqlReader["CIDEMPRESA"],
                                    CNOMBREEMPRESA = (string)sqlReader["CNOMBREEMPRESA"],
                                    CRUTADATOS = (string)sqlReader["CRUTADATOS"]
                                };

                                listaReturn.Add(empresa);
                            }
                        }
                        return new OperationResult { valor = (int)spValor.Value, mensaje = (string)spMensaje.Value, result = listaReturn };
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
    }//
}
