﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Fletera2Datos
{
    public class CatSATServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getCatSATxFilter(int idClave = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idClave > 0)
                    {
                        filtro = " where C.idClave = " + idClave + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
       SELECT  c.idClave ,
        c.Clave ,
        c.Descripcion ,
        c.Tipo
       FROM    dbo.CatSAT c
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<CatSAT> lista = new List<CatSAT>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CatSAT cat = new CatSAT
                                {
                                    idClave = (int)sqlReader["idClave"],
                                    Clave = (string)sqlReader["Clave"],
                                    Descripcion = (string)sqlReader["Descripcion"],
                                    Tipo = (string)sqlReader["Tipo"]
                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCatSAT(ref CatSAT cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idClave = 0
        BEGIN
        INSERT  INTO CatSAT
        ( idClave ,
          Clave ,
          Descripcion ,
          Tipo
        )
VALUES  ( @idClave ,
          @Clave ,
          @Descripcion ,
          @Tipo);
                      
            SET @valor = 0;
            SET @mensaje = 'Catalogo SAT Agregado con Exito';
            SET @id = @@IDENTITY;
            --SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  CatSAT
        SET     Clave = @Clave ,
        Descripcion = @Descripcion ,
        Tipo = @Tipo
        WHERE   idClave = @idClave;
            SET @valor = 0;
            SET @mensaje = 'Catalogo SAT actualizado con Exito';
            SET @id = @idClave;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idClave", cat.idClave));
                        comand.Parameters.Add(new SqlParameter("Clave", cat.Clave));
                        comand.Parameters.Add(new SqlParameter("Descripcion", cat.Descripcion));
                        comand.Parameters.Add(new SqlParameter("Tipo", cat.Tipo));

                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idClave = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
