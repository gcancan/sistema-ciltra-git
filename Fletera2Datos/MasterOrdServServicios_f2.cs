﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class MasterOrdServServicios_f2
    {
        private string strsql;
        private string filtro;

        public OperationResult getMasterOrdServxFilter(int idPadreOrdSer = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idPadreOrdSer > 0)
                    {
                        filtro = " where C.idPadreOrdSer = " + idPadreOrdSer + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
       SELECT c.idPadreOrdSer,
        isnull(c.idTractor,'') as idTractor,
        isnull(c.idTolva1,'') as idTolva1,
        isnull(c.idDolly,'') as idDolly,
        isnull(c.idTolva2,'') as idTolva2,
        c.idChofer as idChofer,
        c.FechaCreacion,
        c.UserCrea,
        isnull(c.UserModifica,'') as UserModifica,
        isnull(c.FechaModifica,getdate()) as FechaModifica,
        isnull(c.UserCancela,'') as UserCancela,
        isnull(c.FechaCancela,getdate()) as FechaCancela,
        isnull(c.MotivoCancela,'') as MotivoCancela,
        isnull(c.Estatus,'') as Estatus,
        isnull(p.NombreCompleto,'') AS NomChofer 
        FROM dbo.MasterOrdServ c
        INNER JOIN dbo.CatPersonal p ON c.idChofer = p.idPersonal
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<MasterOrdServ_f2> lista = new List<MasterOrdServ_f2>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                MasterOrdServ_f2 cat = new MasterOrdServ_f2
                                {
                                    idPadreOrdSer = (int)sqlReader["idPadreOrdSer"],
                                    idTractor = (string)sqlReader["idTractor"],
                                    idTolva1 = (string)sqlReader["idTolva1"],
                                    idDolly = (string)sqlReader["idDolly"],
                                    idTolva2 = (string)sqlReader["idTolva2"],
                                    idChofer = (int)sqlReader["idChofer"],
                                    FechaCreacion = (DateTime)sqlReader["FechaCreacion"],
                                    UserCrea = (string)sqlReader["UserCrea"],
                                    UserModifica = (string)sqlReader["UserModifica"],
                                    FechaModifica = (DateTime)sqlReader["FechaModifica"],
                                    UserCancela = (string)sqlReader["UserCancela"],
                                    FechaCancela = (DateTime)sqlReader["FechaCancela"],
                                    MotivoCancela = (string)sqlReader["MotivoCancela"],
                                    Estatus = (string)sqlReader["Estatus"],
                                    NomChofer = (string)sqlReader["NomChofer"]
                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaMasterOrdServ(ref MasterOrdServ_f2 cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idPadreOrdSer = 0
        BEGIN
        set @id = (select ISNULL(MAX(idPadreOrdSer) + 1, 1) from MasterOrdServ)
        INSERT  INTO MasterOrdServ
        ( idPadreOrdSer ,
          idTractor ,
          idTolva1 ,
          idDolly ,
          idTolva2 ,
          idChofer ,
          FechaCreacion ,
          UserCrea ,
          UserModifica ,
          FechaModifica ,
          UserCancela ,
          FechaCancela ,
          MotivoCancela ,
          Estatus
        )
VALUES  ( @id ,
          @idTractor ,
          @idTolva1 ,
          @idDolly ,
          @idTolva2 ,
          @idChofer ,
          @FechaCreacion ,
          @UserCrea ,
          @UserModifica ,
          @FechaModifica ,
          @UserCancela ,
          @FechaCancela ,
          @MotivoCancela ,
          @Estatus
        );
                      
            SET @valor = 0;
            SET @mensaje = 'Folio Padre Agregado con Exito';
            --SET @id = @@IDENTITY;
            --SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  MasterOrdServ
SET     idTractor = @idTractor ,
        idTolva1 = @idTolva1 ,
        idDolly = @idDolly ,
        idTolva2 = @idTolva2 ,
        idChofer = @idChofer ,
        FechaCreacion = @FechaCreacion ,
        UserCrea = @UserCrea ,
        UserModifica = @UserModifica ,
        FechaModifica = @FechaModifica ,
        UserCancela = @UserCancela ,
        FechaCancela = @FechaCancela ,
        MotivoCancela = @MotivoCancela ,
        Estatus = @Estatus
WHERE   idPadreOrdSer = @idPadreOrdSer;
            SET @valor = 0;
            SET @mensaje = 'Folio Padre actualizado con Exito';
            SET @id = @idPadreOrdSer;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idPadreOrdSer", cat.idPadreOrdSer));
                        comand.Parameters.Add(new SqlParameter("idTractor", cat.idTractor));
                        comand.Parameters.Add(new SqlParameter("idTolva1", cat.idTolva1));
                        comand.Parameters.Add(new SqlParameter("idDolly", cat.idDolly));
                        comand.Parameters.Add(new SqlParameter("idTolva2", cat.idTolva2));
                        comand.Parameters.Add(new SqlParameter("idChofer", cat.idChofer));
                        comand.Parameters.Add(new SqlParameter("FechaCreacion", cat.FechaCreacion));
                        comand.Parameters.Add(new SqlParameter("UserCrea", cat.UserCrea));
                        comand.Parameters.Add(new SqlParameter("UserModifica", cat.UserModifica));
                        comand.Parameters.Add(new SqlParameter("FechaModifica", cat.FechaModifica));
                        comand.Parameters.Add(new SqlParameter("UserCancela", cat.UserCancela));
                        comand.Parameters.Add(new SqlParameter("FechaCancela", cat.FechaCancela));
                        comand.Parameters.Add(new SqlParameter("MotivoCancela", cat.MotivoCancela));
                        comand.Parameters.Add(new SqlParameter("Estatus", cat.Estatus));


                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idPadreOrdSer = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
