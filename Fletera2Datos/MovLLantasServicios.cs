﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
namespace Fletera2Datos
{
    public class MovLLantasServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getMovLLantasxFilter(int idMovLlanta = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idMovLlanta > 0)
                    {
                        filtro = " where C.idMovLlanta = " + idMovLlanta + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
       SELECT  c.idMovLlanta ,
        c.TipoMov ,
        c.Fecha ,
        c.Usuario ,
        c.idSisLlanta ,
        c.idtipoUbicacionOrig ,
        c.idUbicacionOrig ,
        c.PosicionOrig ,
        c.idtipoUbicacionDes ,
        c.idUbicacionDes ,
        c.PosicionDes ,
        c.Observaciones
       FROM    dbo.MovLLantas
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<MovLLantas> lista = new List<MovLLantas>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                MovLLantas cat = new MovLLantas
                                {
                                    idMovLlanta = (int)sqlReader["idMovLlanta"],
                                    TipoMov = (int)sqlReader["TipoMov"],
                                    Fecha = (DateTime)sqlReader["Fecha"],
                                    Usuario = (string)sqlReader["Usuario"],
                                    idSisLlanta = (int)sqlReader["idSisLlanta"],
                                    idtipoUbicacionOrig = (int)sqlReader["idtipoUbicacionOrig"],
                                    idUbicacionOrig = (string)sqlReader["idUbicacionOrig"],
                                    PosicionOrig = (int)sqlReader["PosicionOrig"],
                                    idtipoUbicacionDes = (int)sqlReader["idtipoUbicacionDes"],
                                    idUbicacionDes = (string)sqlReader["idUbicacionDes"],
                                    PosicionDes = (int)sqlReader["PosicionDes"],
                                    Observaciones = (string)sqlReader["Observaciones"]
                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaMovLLantas(ref MovLLantas cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idMovLlanta = 0
        BEGIN
        INSERT  INTO MovLLantas
        ( TipoMov ,
          Fecha ,
          Usuario ,
          idSisLlanta ,
          idtipoUbicacionOrig ,
          idUbicacionOrig ,
          PosicionOrig ,
          idtipoUbicacionDes ,
          idUbicacionDes ,
          PosicionDes ,
          Observaciones)
VALUES  ( @TipoMov ,
          @Fecha ,
          @Usuario ,
          @idSisLlanta ,
          @idtipoUbicacionOrig ,
          @idUbicacionOrig ,
          @PosicionOrig ,
          @idtipoUbicacionDes ,
          @idUbicacionDes ,
          @PosicionDes ,
          @Observaciones);
                      
            SET @valor = 0;
            SET @mensaje = 'Movimiento de Llanta Agregado con Exito';
            SET @id = @@IDENTITY;
            --SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  MovLLantas
     SET     TipoMov = @TipoMov ,
        Fecha = @Fecha ,
        Usuario = @Usuario ,
        idSisLlanta = @idSisLlanta ,
        idtipoUbicacionOrig = @idtipoUbicacionOrig ,
        idUbicacionOrig = @idUbicacionOrig ,
        PosicionOrig = @PosicionOrig ,
        idtipoUbicacionDes = @idtipoUbicacionDes ,
        idUbicacionDes = @idUbicacionDes ,
        PosicionDes = @PosicionDes ,
        Observaciones = @Observaciones
     WHERE   idMovLlanta = @idMovLlanta;
            SET @valor = 0;
            SET @mensaje = 'Movimiento de Llanta actualizado con Exito';
            SET @id = @idMovLlanta;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idMovLlanta", cat.idMovLlanta));
                        comand.Parameters.Add(new SqlParameter("TipoMov", cat.TipoMov));
                        comand.Parameters.Add(new SqlParameter("Fecha", cat.Fecha));
                        comand.Parameters.Add(new SqlParameter("Usuario", cat.Usuario));
                        comand.Parameters.Add(new SqlParameter("idSisLlanta", cat.idSisLlanta));
                        comand.Parameters.Add(new SqlParameter("idtipoUbicacionOrig", cat.idtipoUbicacionOrig));
                        comand.Parameters.Add(new SqlParameter("idUbicacionOrig", cat.idUbicacionOrig));
                        comand.Parameters.Add(new SqlParameter("PosicionOrig", cat.PosicionOrig));
                        comand.Parameters.Add(new SqlParameter("idtipoUbicacionDes", cat.idtipoUbicacionDes));
                        comand.Parameters.Add(new SqlParameter("idUbicacionDes", cat.idUbicacionDes));
                        comand.Parameters.Add(new SqlParameter("PosicionDes", cat.PosicionDes));
                        comand.Parameters.Add(new SqlParameter("Observaciones", cat.Observaciones));

                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idMovLlanta = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
