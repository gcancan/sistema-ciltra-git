﻿using Core.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class UtilServicios
    {
        DataTable DT = new DataTable();
        public string EjecutaSentenciaSql(string ConStr, ref string[] arraySql, int Indice = 0, int PosicionIdentity = -1)
        {
            SqlConnection cnSQL = null;
            SqlCommand cmSQl = null;
            SqlTransaction myTrans = null;
            bool BandConexion = false;
            int recordsAffected;
            int vIdentity = 0;
            bool BTRANS = false;
            string respuesta = "INICIANDO";
            string query2 = "Select @@Identity";

            try
            {

                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    //SaveSetting(NomAplicacion, sErrorLlave, sValorLlave, "");
                    if (Indice > 0)
                    {
                        //cnSQL = new SqlConnection(ConStr);
                        cnSQL = connection;
                        cnSQL.Open();
                        myTrans = cnSQL.BeginTransaction();
                        //int x;
                        for (int x = 0; x < arraySql.Length; x++)
                        {
                            cmSQl = new SqlCommand(arraySql[x].ToString(), cnSQL);
                            cmSQl.Transaction = myTrans;
                            BandConexion = true;

                            recordsAffected = cmSQl.ExecuteNonQuery();
                            if (x == PosicionIdentity)
                            {
                                cmSQl.CommandText = query2;
                                vIdentity = (int)cmSQl.ExecuteScalar();
                            }
                            if (!(recordsAffected >= 0))
                            {
                                BTRANS = true;
                            }
                            if (BTRANS)
                            {
                                break;
                            }

                        }//FOR
                    }
                    if (!BTRANS && BandConexion)
                    {
                        if (BandConexion)
                        {
                            myTrans.Commit();
                            if (PosicionIdentity >= 0)
                            {
                                respuesta = "EFECTUADO@" + vIdentity.ToString();
                            }
                            else
                            {
                                respuesta = "EFECTUADO";
                            }
                        }
                    }
                    else
                    {
                        myTrans.Rollback();
                        respuesta = "NOEFECTUADO";
                    }

                    if (cnSQL != null)
                    {
                        cnSQL.Close();
                    }
                    else
                    {
                        cnSQL.Dispose();
                    }
                    //cmSQl.Dispose();
                    myTrans.Dispose();
                    return respuesta;
                }



            }
            catch (SqlException )
            {
                myTrans.Rollback();
                cnSQL.Close();
                cnSQL.Dispose();
                myTrans.Dispose();
                return "ERROR";

            }
            catch (Exception)
            {
                myTrans.Rollback();
                cnSQL.Close();
                cnSQL.Dispose();
                myTrans.Dispose();
                return "ERROR";
            }
        }

        public int Consecutivo(string ConStr, string Campo, string TablaAfecta, string TablaConsulta, string FiltroAfectaConWhere = "", string FiltroConsultaConWhere = "")
        {
            string strSql = "";
            SqlConnection cnSQL = null;
            SqlCommand cmSQl = null;
            SqlDataReader drSQL = null;
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    strSql = "DECLARE @NUMSIGUIENTE AS INT " +
                   " set @numsiguiente = isnull((select top 1 isnull(" + Campo + ",0) as " + Campo + " from " + TablaConsulta + " " + FiltroConsultaConWhere + " ),0) " +
                   "  " +
                   " UPDATE " + TablaAfecta + " SET " + Campo + " = ISNULL(@NUMSIGUIENTE,0) + 1 " +
                   " " + FiltroAfectaConWhere + " " +
                   " SELECT ISNULL(@numsiguiente,0) + 1 AS " + Campo + " ,@@ROWCOUNT AS REGAFECTADOS ";

                    cnSQL = connection;
                    cnSQL.Open();
                    cmSQl = new SqlCommand(strSql, cnSQL);
                    drSQL = cmSQl.ExecuteReader();
                    if (drSQL.Read())
                    {
                        if ((int)drSQL["REGAFECTADOS"] == 0)
                        {
                            return 0;
                        }
                        else
                        {
                            return (int)drSQL[Campo];
                        }
                    }
                    else
                    {
                        return 0;
                    }

                }
            }
            catch (SqlException)
            {
                return 0;
            }
            catch (Exception)
            {
                return 0;
            }

        }

        public DataTable LlenaTabla(string strSQL, string ConStr = "")
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    //strsql = "select * from Empleados";
                    SqlCommand cmd = new SqlCommand(strSQL, connection);
                    cmd.CommandType = CommandType.Text;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    return dt;
                }

            }
            catch (Exception)
            {
                return null;
            }

        }
        public string EjecutaSentenciaSqlCOM (string ConStr, ref string[] arraySql, int Indice = 0, int PosicionIdentity = -1)
        {
            SqlConnection cnSQL = null;
            SqlCommand cmSQl = null;
            SqlTransaction myTrans = null;
            bool BandConexion = false;
            int recordsAffected;
            int vIdentity = 0;
            bool BTRANS = false;
            string respuesta = "INICIANDO";
            string query2 = "Select @@Identity";

            try
            {

                using (var connection = BoundedContextFactory.ConnectionFactoryTaller())
                {
                    //SaveSetting(NomAplicacion, sErrorLlave, sValorLlave, "");
                    if (Indice > 0)
                    {
                        //cnSQL = new SqlConnection(ConStr);
                        cnSQL = connection;
                        cnSQL.Open();
                        myTrans = cnSQL.BeginTransaction();
                        //int x;
                        for (int x = 0; x < arraySql.Length; x++)
                        {
                            cmSQl = new SqlCommand(arraySql[x].ToString(), cnSQL);
                            cmSQl.Transaction = myTrans;
                            BandConexion = true;

                            recordsAffected = cmSQl.ExecuteNonQuery();
                            if (x == PosicionIdentity)
                            {
                                cmSQl.CommandText = query2;
                                vIdentity = (int)cmSQl.ExecuteScalar();
                            }
                            if (!(recordsAffected >= 0))
                            {
                                BTRANS = true;
                            }
                            if (BTRANS)
                            {
                                break;
                            }

                        }//FOR
                    }
                    if (!BTRANS && BandConexion)
                    {
                        if (BandConexion)
                        {
                            myTrans.Commit();
                            if (PosicionIdentity >= 0)
                            {
                                respuesta = "EFECTUADO@" + vIdentity.ToString();
                            }
                            else
                            {
                                respuesta = "EFECTUADO";
                            }
                        }
                    }
                    else
                    {
                        myTrans.Rollback();
                        respuesta = "NOEFECTUADO";
                    }

                    if (cnSQL != null)
                    {
                        cnSQL.Close();
                    }
                    else
                    {
                        cnSQL.Dispose();
                    }
                    //cmSQl.Dispose();
                    myTrans.Dispose();
                    return respuesta;
                }



            }
            catch (SqlException)
            {
                myTrans.Rollback();
                cnSQL.Close();
                cnSQL.Dispose();
                myTrans.Dispose();
                return "ERROR";

            }
            catch (Exception)
            {
                myTrans.Rollback();
                cnSQL.Close();
                cnSQL.Dispose();
                myTrans.Dispose();
                return "ERROR";
            }
        }
    }//
}
