﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class CatConceptosLiqServicios
    {
        private string strsql;
        private string filtro;

        public OperationResult getCatConceptosLiqxId(int idConcepto = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idConcepto > 0)
                    {
                        filtro = " where idConcepto = " + idConcepto + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT idConcepto, 
        NomConcepto, 
        EsCargo,
        Activo  
        FROM dbo.CatConceptosLiq 
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        comand.Parameters.Add(new SqlParameter("@idConcepto", idConcepto));


                        connection.Open();
                        List<CatConceptosLiq> lista = new List<CatConceptosLiq>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CatConceptosLiq cat = new CatConceptosLiq
                                {
                                    idConcepto = (int)sqlReader["idConcepto"],
                                    NomConcepto = (string)sqlReader["NomConcepto"],
                                    EsCargo = (bool)sqlReader["EsCargo"],
                                    Activo = (bool)sqlReader["Activo"]
                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCatConceptosLiq(ref CatConceptosLiq cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idConcepto = 0
        BEGIN
            insert into CatConceptosLiq
            (NomConcepto,EsCargo,Activo)
            values
            (@NomConcepto,@EsCargo,@Activo);
                      
            SET @valor = 0;
            SET @mensaje = 'Concepto Agregado con Exito';
            SET @id = @@IDENTITY;
        END;
    ELSE
        BEGIN
           UPDATE CatConceptosLiq SET NomConcepto = @NomConcepto,
            EsCargo = EsCargo , 
            Activo =  @Activo
            WHERE idConcepto = @idConcepto;
            SET @valor = 0;
            SET @mensaje = 'Concepto actualizado con Exito';
            SET @id = @idConcepto;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("@idConcepto", cat.idConcepto));
                        comand.Parameters.Add(new SqlParameter("@NomConcepto", cat.NomConcepto));
                        comand.Parameters.Add(new SqlParameter("@EsCargo", cat.EsCargo));
                        comand.Parameters.Add(new SqlParameter("@Activo", cat.Activo));

                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idConcepto = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }



    }//
}
