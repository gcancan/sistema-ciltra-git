﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
namespace Fletera2Datos
{
    public class LlantasHisServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getLlantasHisxFilter(int idSis = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idSis > 0)
                    {
                        filtro = " where C.idSis = " + idSis + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
       SELECT  c.idSis ,
        c.idtipoUbicacion ,
        c.idUbicacion ,
        c.Fecha ,
        c.Usuario ,
        c.idSisLlanta ,
        c.idLlanta ,
        c.EntSal ,
        c.idTipoDoc ,
        c.Documento ,
        c.Profundidad ,
        c.idCondicion ,
        c.Posicion
    FROM    dbo.LlantasHis c
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<LlantasHis> lista = new List<LlantasHis>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                LlantasHis cat = new LlantasHis
                                {
                                    idSis = (int)sqlReader["idSis"],
                                    idtipoUbicacion = (int)sqlReader["idtipoUbicacion"],
                                    idUbicacion = (string)sqlReader["idUbicacion"],
                                    Fecha = (DateTime)sqlReader["Fecha"],
                                    Usuario = (string)sqlReader["Usuario"],
                                    idSisLlanta = (int)sqlReader["idSisLlanta"],
                                    idLlanta = (string)sqlReader["idLlanta"],
                                    EntSal = (bool)sqlReader["EntSal"],
                                    idTipoDoc = (int)sqlReader["idTipoDoc"],
                                    Documento = (string)sqlReader["Documento"],
                                    Profundidad = (decimal)sqlReader["Profundidad"],
                                    idCondicion = (int)sqlReader["idCondicion"],
                                    Posicion = (int)sqlReader["Posicion"]

                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaLlantasHis(ref LlantasHis cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idSis = 0
        BEGIN
        INSERT  INTO LlantasHis
        (idtipoUbicacion ,
          idUbicacion ,
          Fecha ,
          Usuario ,
          idSisLlanta ,
          idLlanta ,
          EntSal ,
          idTipoDoc ,
          Documento ,
          Profundidad ,
          idCondicion ,
          Posicion)
VALUES  ( @idtipoUbicacion ,
          @idUbicacion ,
          @Fecha ,
          @Usuario ,
          @idSisLlanta ,
          @idLlanta ,
          @EntSal ,
          @idTipoDoc ,
          @Documento ,
          @Profundidad ,
          @idCondicion ,
          @Posicion);
                      
            SET @valor = 0;
            SET @mensaje = 'Historia de Llanta Agregado con Exito';
            SET @id = @@IDENTITY;
            --SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  LlantasHis
    SET     idtipoUbicacion = @idtipoUbicacion ,
        idUbicacion = @idUbicacion ,
        Fecha = @Fecha ,
        Usuario = @Usuario ,
        idSisLlanta = @idSisLlanta ,
        idLlanta = @idLlanta ,
        EntSal = @EntSal ,
        idTipoDoc = @idTipoDoc ,
        Documento = @Documento ,
        Profundidad = @Profundidad ,
        idCondicion = @idCondicion ,
        Posicion = @Posicion
    WHERE   idSis = @idSis;
            SET @valor = 0;
            SET @mensaje = 'Historia de Llanta actualizado con Exito';
            SET @id = @idSis;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idSis", cat.idSis));
                        comand.Parameters.Add(new SqlParameter("idtipoUbicacion", cat.idtipoUbicacion));
                        comand.Parameters.Add(new SqlParameter("idUbicacion", cat.idUbicacion));
                        comand.Parameters.Add(new SqlParameter("Fecha", cat.Fecha));
                        comand.Parameters.Add(new SqlParameter("Usuario", cat.Usuario));
                        comand.Parameters.Add(new SqlParameter("idSisLlanta", cat.idSisLlanta));
                        comand.Parameters.Add(new SqlParameter("idLlanta", cat.idLlanta));
                        comand.Parameters.Add(new SqlParameter("EntSal", cat.EntSal));
                        comand.Parameters.Add(new SqlParameter("idTipoDoc", cat.idTipoDoc));
                        comand.Parameters.Add(new SqlParameter("Documento", cat.Documento));
                        comand.Parameters.Add(new SqlParameter("Profundidad", cat.Profundidad));
                        comand.Parameters.Add(new SqlParameter("idCondicion", cat.idCondicion));
                        comand.Parameters.Add(new SqlParameter("Posicion", cat.Posicion));



                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idSis = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
