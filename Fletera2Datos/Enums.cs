﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class Enums
    {
        public enum eTipoElementoViaje
        {
            Tractor = 1,
            Remolque1 = 2,
            Dolly = 3,
            Remolque2 = 4,
            Operador = 5
        }
    }
}
