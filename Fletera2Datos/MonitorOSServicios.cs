﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
namespace Fletera2Datos
{
    public class MonitorOSServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getMonitorOSxFilter(int idOrdenSer = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idOrdenSer > 0)
                    {
                        filtro = " where C.idOrdenSer = " + idOrdenSer + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

        //            SELECT c.idPadreOrdSer AS FOLIO, 
        //c.idOrdenSer AS  idOrdenSer, 
        //tos.NomTipOrdServ as Tipo, 
        //c.idUnidadTrans as Unidad, 
        //emp.RazonSocial as Empresa, 
        //c.Estatus,  
        //ISNULL((PerEnt.Nombre + ' ' + PerEnt.ApellidoPat + ' ' + PerEnt.ApellidoMat), '') AS Entrego,
        // ISNULL((PerRec.sNomUser), '') AS Recibio,
        //  ISNULL((PerAut.Nombre + ' ' + PerAut.ApellidoPat + ' ' + PerAut.ApellidoMat), '') AS Autorizo,
        //'' AS TiempoTrasncurrido,
        //c.fechaCaptura as Captura, 
        //c.FechaRecepcion as Recepcion, 
        //c.FechaAsignado as Asignado, 
        //c.FechaDiagnostico as Diagnostico, 
        //c.FechaTerminado as Terminado, 
        //c.FechaEntregado as Entregado, 
        //c.fechaCancela as Cancelado, 
        //c.externo as externo,
        //ES.fecha AS CasetaEntrada,
        //ES.fechaFin AS CasetaSalida,
        //c.estatus,
        //c.FechaRecepcion as OstesIni,
        //c.FechaTerminado as OstesFin,
        //ISNULL((PerCan.sNomUser), '') AS Cancelo
        //FROM dbo.CabOrdenServicio c
        //INNER JOIN dbo.CatEmpresas Emp ON c.IdEmpresa = emp.idEmpresa
        //left JOIN dbo.CatPersonal PerEnt ON c.idEmpleadoEntrega = PerEnt.idPersonal
        //left JOIN dbo.Usuarios PerRec ON c.UsuarioRecepcion = PerRec.sUserId
        //left JOIN dbo.CatPersonal PerAut ON c.idEmpleadoAutoriza = PerAut.idPersonal
        //INNER JOIN dbo.CatTipoOrdServ TOS ON TOS.idTipOrdServ = c.idTipOrdServ
        //LEFT JOIN dbo.salidasEntradas ES on ES.idPadreOrdSer = c.idPadreOrdSer and ES.idUnidadTrans = c.idUnidadTrans
        //left JOIN dbo.Usuarios PerCan ON c.Usuariocan = PerCan.sUserId
                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT c.idPadreOrdSer AS FOLIO, 
        c.idOrdenSer AS  idOrdenSer, 
        tos.NomTipOrdServ as Tipo, 
        c.idUnidadTrans as Unidad, 
        ISNULL((perchofer.Nombre + ' ' + perchofer.ApellidoPat + ' ' + perchofer.ApellidoMat),'') AS Chofer,
        emp.RazonSocial as Empresa, 
        c.Estatus,  
        ISNULL((PerAut.Nombre + ' ' + PerAut.ApellidoPat + ' ' + PerAut.ApellidoMat),'') AS Autorizo, 
        '' AS TiempoTrasncurrido, 
        c.fechaCaptura as Captura, 
        c.FechaRecepcion as Recepcion, 
        ISNULL((PerEnt.Nombre + ' ' + PerEnt.ApellidoPat + ' ' + PerEnt.ApellidoMat),'') AS Entrego, 
        ISNULL((perRec.Nombre + ' ' + perRec.ApellidoPat + ' ' + perRec.ApellidoMat),'') AS Recibio,
        c.FechaAsignado as Asignado, 
        ISNULL((perAsig.Nombre + ' ' + perAsig.ApellidoPat + ' ' + perAsig.ApellidoMat),'') AS Asigno,
        c.FechaDiagnostico as Diagnostico, 
        c.FechaTerminado as Terminado, 
        ISNULL((perTer.Nombre + ' ' + perTer.ApellidoPat + ' ' + perTer.ApellidoMat),'') AS Termino,
        c.FechaEntregado as Entregado, 
        c.fechaCancela as Cancelado, 
        ISNULL((percan.Nombre + ' ' + percan.ApellidoPat + ' ' + percan.ApellidoMat),'') AS Cancelo,
        c.externo as externo,
        ES.fecha AS CasetaEntrada,
        ISNULL((perCasEnt.Nombre + ' ' + perCasEnt.ApellidoPat + ' ' + perCasEnt.ApellidoMat),'') AS CasetaEntradaUser,
        ES.fechaFin AS CasetaSalida,
        ISNULL((perCasSal.Nombre + ' ' + perCasSal.ApellidoPat + ' ' + perCasSal.ApellidoMat),'') AS CasetaSalidaUser,
        c.FechaRecepcion as OstesIni,
        ISNULL((perRec.Nombre + ' ' + perRec.ApellidoPat + ' ' + perRec.ApellidoMat),'') AS OstesIniUser,
        c.FechaTerminado as OstesFin,
        ISNULL((perTer.Nombre + ' ' + perTer.ApellidoPat + ' ' + perTer.ApellidoMat),'') AS OstesFinUser
        FROM dbo.CabOrdenServicio c 
        INNER JOIN dbo.CatEmpresas Emp ON c.IdEmpresa = emp.idEmpresa 
        left JOIN dbo.CatPersonal PerEnt ON c.idEmpleadoEntrega = PerEnt.idPersonal 
        left JOIN dbo.CatPersonal PerAut ON c.idEmpleadoAutoriza = PerAut.idPersonal 
        INNER JOIN dbo.CatTipoOrdServ TOS ON TOS.idTipOrdServ = c.idTipOrdServ
        LEFT JOIN dbo.salidasEntradas ES on ES.idPadreOrdSer = c.idPadreOrdSer and ES.idUnidadTrans = c.idUnidadTrans
        LEFT JOIN dbo.usuariosSys usrCasEnt ON es.idUsuario = usrCasEnt.idUsuario
        LEFT JOIN dbo.CatPersonal perCasEnt ON percasent.idPersonal = usrCasEnt.idPersonal
        LEFT JOIN dbo.usuariosSys usrCassal ON es.idUsuarioFin = usrCassal.idUsuario
        LEFT JOIN dbo.CatPersonal perCasSal ON perCasSal.idPersonal = usrCassal.idPersonal
        left JOIN dbo.usuariosSys usrPerRec ON c.UsuarioRecepcion = usrPerRec.nombreUsuario 
        LEFT JOIN dbo.CatPersonal perRec ON perRec.idPersonal = usrPerRec.idPersonal
        left JOIN dbo.usuariosSys usrPerCan ON c.Usuariocan = usrPerCan.nombreUsuario
        LEFT JOIN dbo.CatPersonal percan ON percan.idPersonal = usrPerCan.idPersonal
        INNER JOIN dbo.MasterOrdServ m ON m.idPadreOrdSer = c.idPadreOrdSer
        LEFT JOIN dbo.CatPersonal perchofer ON perchofer.idPersonal = m.idChofer
        left JOIN dbo.usuariosSys usrPerAsig ON c.UsuarioAsigna = usrPerAsig.nombreUsuario
        LEFT JOIN dbo.CatPersonal perAsig ON perAsig.idPersonal = usrPerAsig.idPersonal
        left JOIN dbo.usuariosSys usrPerTer ON c.UsuarioTermina = usrPerTer.nombreUsuario
        LEFT JOIN dbo.CatPersonal perTer ON perTer.idPersonal = usrPerTer.idPersonal
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<MonitorOS> lista = new List<MonitorOS>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                MonitorOS cat = new MonitorOS
                                {
                                    FOLIO = (int)sqlReader["FOLIO"],
                                    idOrdenSer = (int)sqlReader["idOrdenSer"],
                                    Tipo = (string)sqlReader["Tipo"],
                                    Unidad = (string)sqlReader["Unidad"],
                                    Chofer = (string)sqlReader["Chofer"],
                                    Empresa = (string)sqlReader["Empresa"],
                                    Estatus = (string)sqlReader["Estatus"],
                                    Autorizo = (string)sqlReader["Autorizo"],
                                    TiempoTrasncurrido = (string)sqlReader["TiempoTrasncurrido"],
                                    Captura = (DateTime)sqlReader["Captura"],
                                    Recepcion = sqlReader["Recepcion"] == DBNull.Value ? (DateTime?)null : (DateTime)sqlReader["Recepcion"],
                                    Entrego = (string)sqlReader["Entrego"],
                                    Recibio = (string)sqlReader["Recibio"],
                                    Asignado = sqlReader["Asignado"] == DBNull.Value ? (DateTime?)null : (DateTime)sqlReader["Asignado"],
                                    Asigno = (string)sqlReader["Asigno"],
                                    Terminado = sqlReader["Terminado"] == DBNull.Value ? (DateTime?)null : (DateTime)sqlReader["Terminado"],
                                    Termino = (string)sqlReader["Termino"],
                                    Entregado = sqlReader["Entregado"] == DBNull.Value ? (DateTime?)null : (DateTime)sqlReader["Entregado"],
                                    Cancelado = sqlReader["Cancelado"] == DBNull.Value ? (DateTime?)null : (DateTime)sqlReader["Cancelado"],
                                    Cancelo = (string)sqlReader["Cancelo"],
                                    externo = (bool)sqlReader["externo"],
                                    CasetaEntrada = sqlReader["CasetaEntrada"] == DBNull.Value ? (DateTime?)null : (DateTime)sqlReader["CasetaEntrada"],
                                    CasetaEntradaUser = (string)sqlReader["CasetaEntradaUser"],
                                    CasetaSalida = sqlReader["CasetaSalida"] == DBNull.Value ? (DateTime?)null : (DateTime)sqlReader["CasetaSalida"],
                                    CasetaSalidaUser = (string)sqlReader["CasetaSalidaUser"],
                                    OstesIni = sqlReader["OstesIni"] == DBNull.Value ? (DateTime?)null : (DateTime)sqlReader["OstesIni"],
                                    OstesIniUser = (string)sqlReader["OstesIniUser"],
                                    OstesFin = sqlReader["OstesFin"] == DBNull.Value ? (DateTime?)null : (DateTime)sqlReader["OstesFin"],
                                    OstesFinUser = (string)sqlReader["OstesFinUser"]
                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
        public OperationResult getMonitorDetOSxFilter(int idOrdenSer = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idOrdenSer > 0)
                    {
                        filtro = " where C.idOrdenSer = " + idOrdenSer + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
       SELECT ot.idOrdenSer AS NoServicio, 
        ot.idOrdenTrabajo AS [No. Trabajo], 
        ISNULL(ot.NotaRecepcionA,'') AS Descripcion, 
        ISNULL(div.NomDivision,'') AS Division, 
        ISNULL(CASE WHEN PerRes.Nombre <> '' THEN PerRes.Nombre + ' ' + PerRes.ApellidoPat + ' ' + PerRes.ApellidoMat ELSE PerRes.NombreCompleto END,'') AS Responsable, 
        ISNULL(CASE WHEN PerAyu1.Nombre <> '' THEN PerAyu1.Nombre + ' ' + PerAyu1.ApellidoPat + ' ' + PerAyu1.ApellidoMat ELSE PerAyu1.NombreCompleto END,'') AS Ayudante1, 
        ISNULL(CASE WHEN PerAyu2.Nombre <> '' THEN PerAyu2.Nombre + ' ' + PerAyu2.ApellidoPat + ' ' + PerAyu2.ApellidoMat ELSE PerAyu2.NombreCompleto END,'') AS Ayudante2, 
        ISNULL(det.Estatus,'') AS Estatus, 
        det.FechaAsig AS Asignado, 
        det.FechaDiag AS Diagnostico , 
        det.FechaTerminado AS Terminado, 
        ISNULL(det.DuracionActHr,0) AS DuracionHR, 
        '' AS TiempoTrasncurrido 
        FROM dbo.DetRecepcionOS ot 
        left JOIN dbo.DetOrdenServicio det ON ot.idOrdenTrabajo = det.idOrdenTrabajo 
        left JOIN dbo.CatPersonal PerRes ON ot.idPersonalResp = PerRes.idPersonal 
        left JOIN dbo.CatPersonal PerAyu1 ON ot.idPersonalAyu1 = PerAyu1.idPersonal 
        left JOIN dbo.CatPersonal PerAyu2 ON ot.idPersonalAyu2 = PerAyu2.idPersonal 
        LEFT JOIN dbo.CatDivision div ON ot.idDivision = div.idDivision
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<MonitorOS> lista = new List<MonitorOS>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                MonitorOS cat = new MonitorOS
                                {
                                    FOLIO = (int)sqlReader["FOLIO"],
                                    idOrdenSer = (int)sqlReader["idOrdenSer"],
                                    Tipo = (string)sqlReader["Tipo"],
                                    Unidad = (string)sqlReader["Unidad"],
                                    Empresa = (string)sqlReader["Empresa"],
                                    Entrego = (string)sqlReader["Entrego"],
                                    Recibio = (string)sqlReader["Recibio"],
                                    Autorizo = (string)sqlReader["Autorizo"],
                                    TiempoTrasncurrido = (string)sqlReader["TiempoTrasncurrido"],
                                    Captura = (DateTime)sqlReader["Captura"],
                                    Recepcion = sqlReader["Recepcion"] == DBNull.Value ? (DateTime?)null : (DateTime)sqlReader["Recepcion"],
                                    Asignado = sqlReader["Asignado"] == DBNull.Value ? (DateTime?)null : (DateTime)sqlReader["Asignado"],
                                    Terminado = sqlReader["Terminado"] == DBNull.Value ? (DateTime?)null : (DateTime)sqlReader["Terminado"],
                                    Entregado = sqlReader["Entregado"] == DBNull.Value ? (DateTime?)null : (DateTime)sqlReader["Entregado"],
                                    Cancelado = sqlReader["Cancelado"] == DBNull.Value ? (DateTime?)null : (DateTime)sqlReader["Cancelado"],
                                    externo = (bool)sqlReader["externo"],
                                    CasetaEntrada = sqlReader["CasetaEntrada"] == DBNull.Value ? (DateTime?)null : (DateTime)sqlReader["CasetaEntrada"],
                                    CasetaSalida = sqlReader["CasetaSalida"] == DBNull.Value ? (DateTime?)null : (DateTime)sqlReader["CasetaSalida"],
                                    Estatus = (string)sqlReader["Estatus"],
                                    OstesIni = sqlReader["OstesIni"] == DBNull.Value ? (DateTime?)null : (DateTime)sqlReader["OstesIni"],
                                    OstesFin = sqlReader["OstesFin"] == DBNull.Value ? (DateTime?)null : (DateTime)sqlReader["OstesFin"]
                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
