﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Fletera2Datos
{
    public class CatLLantasProductoServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getCatLLantasProductoxFilter(int idProductoLlanta = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idProductoLlanta > 0)
                    {
                        filtro = " where C.idProductoLlanta = " + idProductoLlanta + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT  c.idProductoLlanta ,
        c.idMarca ,
        m.NombreMarca,
        c.idDisenio,
		d.Descripcion, 
		c.Medida,
		c.Profundidad,
        c.Activo
        FROM    dbo.CatLLantasProducto c
        inner join CatMarcas m on c.idmarca = m.idmarca
		INNER JOIN dbo.CatDisenios d ON d.idDisenio = c.idDisenio
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<CatLLantasProducto> lista = new List<CatLLantasProducto>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CatLLantasProducto cat = new CatLLantasProducto
                                {
                                    idProductoLlanta = (int)sqlReader["idProductoLlanta"],
                                    idMarca = (int)sqlReader["idMarca"],
                                    NombreMarca = (string)sqlReader["NombreMarca"],
                                    idDisenio = (int)sqlReader["idDisenio"],
                                    Descripcion = (string)sqlReader["Descripcion"],
                                    Medida = (string)sqlReader["Medida"],
                                    Profundidad = (decimal)sqlReader["Profundidad"],
                                    Activo = (bool)sqlReader["Activo"]
                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCatClasLlan(ref CatLLantasProducto cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idConcepto = 0
        BEGIN
        INSERT  INTO CatLLantasProducto
        ( idMarca ,
          idDisenio ,
          Medida ,
          Profundidad ,
          Activo)
VALUES  ( @idMarca ,
          @idDisenio ,
          @Medida ,
          @Profundidad ,
          @Activo);
                      
            SET @valor = 0;
            SET @mensaje = 'Llanta Agregado con Exito';
            SET @id = @@IDENTITY;
            --SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  CatLLantasProducto
SET     idMarca = @idMarca ,
        idDisenio = @idDisenio ,
        Medida = @Medida ,
        Profundidad = @Profundidad ,
        Activo = @Activo
WHERE   idProductoLlanta = @idProductoLlanta;
            SET @valor = 0;
            SET @mensaje = 'Llanta actualizado con Exito';
            SET @id = @idProductoLlanta;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idProductoLlanta", cat.idProductoLlanta));
                        comand.Parameters.Add(new SqlParameter("idMarca", cat.idMarca));
                        comand.Parameters.Add(new SqlParameter("idDisenio", cat.idDisenio));
                        comand.Parameters.Add(new SqlParameter("Medida", cat.Medida));
                        comand.Parameters.Add(new SqlParameter("Profundidad", cat.Profundidad));
                        comand.Parameters.Add(new SqlParameter("Activo", cat.Activo));


                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idProductoLlanta = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
