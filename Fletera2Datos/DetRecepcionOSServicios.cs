﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using CoreFletera.Models;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class DetRecepcionOSServicios
    {
        private string strsql;
        private string filtro;

        public OperationResult getDetOrdenServicioxFilter(int idOrdenSer = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idOrdenSer > 0)
                    {
                        filtro = " where d.idOrdenSer = " + idOrdenSer + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT d.idOrdenTrabajo,
        d.idOrdenSer,
        d.idFamilia,
        d.NotaRecepcion,
        d.isCancelado,
        isnull(d.motivoCancelacion,'') as motivoCancelacion,
        d.idDivision,
        d.idPersonalResp,
		ISNULL(pr.NombreCompleto,'') AS NomPersonalResp,
        ISNULL(d.idPersonalAyu1,0) AS idPersonalAyu1,
		ISNULL(pa1.NombreCompleto,'') AS NomPersonalAyu1,
        ISNULL(d.idPersonalAyu2,0) AS idPersonalAyu2,
		ISNULL(pa2.NombreCompleto,'') AS NomPersonalAyu2,
        ISNULL(d.NotaRecepcionA,'') AS NotaRecepcionA
        FROM dbo.DetRecepcionOS d
		LEFT JOIN dbo.CatPersonal pr ON d.idPersonalResp = pr.idPersonal
		LEFT JOIN dbo.CatPersonal pa1 ON d.idPersonalAyu1 = pa1.idPersonal
		LEFT JOIN dbo.CatPersonal pa2 ON d.idPersonalAyu2 = pa2.idPersonal
        INNER JOIN dbo.DetOrdenServicio dos ON dos.idOrdenSer = d.idOrdenSer AND dos.idOrdenTrabajo = d.idOrdenTrabajo
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        comand.Parameters.Add(new SqlParameter("@idOrdenSer", idOrdenSer));


                        connection.Open();
                        List<DetRecepcionOS> lista = new List<DetRecepcionOS>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetRecepcionOS folio = new DetRecepcionOS
                                {
                                    idOrdenTrabajo = (int)sqlReader["idOrdenTrabajo"],
                                    idOrdenSer = (int)sqlReader["idOrdenSer"],
                                    idFamilia = (int)sqlReader["idFamilia"],
                                    NotaRecepcion = (string)sqlReader["NotaRecepcion"],
                                    NotaRecepcionA = (string)sqlReader["NotaRecepcionA"],
                                    isCancelado = (bool)sqlReader["isCancelado"],
                                    motivoCancelacion = (string)sqlReader["motivoCancelacion"],
                                    idDivision = (int)sqlReader["idDivision"],
                                    idPersonalResp = (int)sqlReader["idPersonalResp"],
                                    NomPersonalResp = (string)sqlReader["NomPersonalResp"],
                                    idPersonalAyu1 = (int)sqlReader["idPersonalAyu1"],
                                    NomPersonalAyu1 = (string)sqlReader["NomPersonalAyu1"],
                                    idPersonalAyu2 = (int)sqlReader["idPersonalAyu2"],
                                    NomPersonalAyu2 = (string)sqlReader["NomPersonalAyu2"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaDetRecepcionOS(ref DetRecepcionOS cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @Inserta = 1
        BEGIN
        --set @id = (select ISNULL(MAX(idOrdenTrabajo) + 1, 1) from DetRecepcionOS)
        INSERT  INTO DetRecepcionOS
        ( idOrdenTrabajo ,
          idOrdenSer ,
          idFamilia ,
          NotaRecepcion ,
          isCancelado ,
          motivoCancelacion ,
          idDivision ,
          idPersonalResp ,
          idPersonalAyu1 ,
          idPersonalAyu2 ,
          NotaRecepcionA
        )
VALUES  ( @idOrdenTrabajo ,
          @idOrdenSer ,
          @idFamilia ,
          @NotaRecepcion ,
          @isCancelado ,
          @motivoCancelacion ,
          @idDivision ,
          @idPersonalResp ,
          @idPersonalAyu1 ,
          @idPersonalAyu2 ,
          @NotaRecepcionA
        );
                      
            SET @valor = 0;
            SET @mensaje = 'orden de servicio Agregado con Exito';
            --SET @id = @@IDENTITY;
            SET @id = @idOrdenTrabajo;
        END;
    ELSE
        BEGIN
        UPDATE  DetRecepcionOS
SET     idOrdenSer = @idOrdenSer ,
        idFamilia = @idFamilia ,
        NotaRecepcion = @NotaRecepcion ,
        isCancelado = @isCancelado ,
        motivoCancelacion = @motivoCancelacion ,
        idDivision = @idDivision ,
        idPersonalResp = @idPersonalResp ,
        idPersonalAyu1 = @idPersonalAyu1 ,
        idPersonalAyu2 = @idPersonalAyu2 ,
        NotaRecepcionA = @NotaRecepcionA
WHERE   idOrdenTrabajo = @idOrdenTrabajo;
            SET @valor = 0;
            SET @mensaje = 'Tipo de Unidad actualizado con Exito';
            SET @id = @idOrdenTrabajo;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idOrdenTrabajo", cat.idOrdenTrabajo));
                        comand.Parameters.Add(new SqlParameter("idOrdenSer", cat.idOrdenSer));
                        comand.Parameters.Add(new SqlParameter("idFamilia", cat.idFamilia));
                        comand.Parameters.Add(new SqlParameter("NotaRecepcion", cat.NotaRecepcion));
                        comand.Parameters.Add(new SqlParameter("isCancelado", cat.isCancelado));
                        comand.Parameters.Add(new SqlParameter("motivoCancelacion", cat.motivoCancelacion));
                        comand.Parameters.Add(new SqlParameter("idDivision", cat.idDivision));
                        comand.Parameters.Add(new SqlParameter("idPersonalResp", cat.idPersonalResp));
                        comand.Parameters.Add(new SqlParameter("idPersonalAyu1", cat.idPersonalAyu1));
                        comand.Parameters.Add(new SqlParameter("idPersonalAyu2", cat.idPersonalAyu2));
                        comand.Parameters.Add(new SqlParameter("NotaRecepcionA", cat.NotaRecepcionA));
                        comand.Parameters.Add(new SqlParameter("Inserta", cat.Inserta));

                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idOrdenTrabajo = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
