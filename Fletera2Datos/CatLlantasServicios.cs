﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Fletera2Datos
{
    public class CatLlantasServicios
    {
        private string strsql;
        private string filtro;

        public OperationResult getCatLlantasxFilter(int idSisLlanta = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idSisLlanta > 0)
                    {
                        filtro = " where C.idSisLlanta = " + idSisLlanta + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
       SELECT  c.idSisLlanta ,
        c.idLlanta ,
        c.NoSerie ,
        c.idEmpresa ,
        c.idProductoLlanta ,
        c.idTipoLLanta ,
        isnull(t.descripcion,'') as NomTipoLlanta,
        c.Factura ,
        c.Fecha ,
        c.Costo ,
        c.RFDI ,
        c.Posicion ,
        c.idtipoUbicacion ,
        c.idUbicacion ,
        c.Año ,
        c.Mes ,
        c.Observaciones ,
        c.UltFecha ,
        c.idCondicion ,
        isnull(co.Descripcion,'') as NomCondicion,
        c.Activo ,
        c.ProfundidadAct ,
        isnull(c.idDisenioRev,0) as idDisenioRev ,
        isnull(dr.Descripcion,'') as NomDisenioRev,
        isnull(c.idOrdenCompra,0) as idOrdenCompra,
        isnull(c.idDetalleCompra,0) as idDetalleCompra
        FROM    dbo.CatLlantas c
        left join CatTipoLlanta t on c.idTipoLLanta = t.idTipoLLanta
        left join CatCondiciones co on c.idCondicion = co.idCondicion and co.idTipOrdServ = 3
        left join CatDisenios dr on c.idDisenioRev = dr.idDisenio
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<CatLlantas> lista = new List<CatLlantas>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CatLlantas cat = new CatLlantas
                                {
                                    idSisLlanta = (int)sqlReader["idSisLlanta"],
                                    idLlanta = (string)sqlReader["idLlanta"],
                                    NoSerie = (string)sqlReader["NoSerie"],
                                    idEmpresa = (int)sqlReader["idEmpresa"],
                                    idProductoLlanta = (int)sqlReader["idProductoLlanta"],
                                    idTipoLLanta = (int)sqlReader["idTipoLLanta"],
                                    Factura = (string)sqlReader["Factura"],
                                    Fecha = (DateTime)sqlReader["Fecha"],
                                    Costo = (decimal)sqlReader["Costo"],
                                    RFDI = (string)sqlReader["RFDI"],
                                    Posicion = (int)sqlReader["Posicion"],
                                    idtipoUbicacion = (int)sqlReader["idtipoUbicacion"],
                                    idUbicacion = (string)sqlReader["idUbicacion"],
                                    Año = (int)sqlReader["Año"],
                                    Mes = (int)sqlReader["Mes"],
                                    Observaciones = (string)sqlReader["Observaciones"],
                                    UltFecha = (DateTime)sqlReader["UltFecha"],
                                    idCondicion = (string)sqlReader["idCondicion"],
                                    Activo = (bool)sqlReader["Activo"],
                                    ProfundidadAct = (decimal)sqlReader["ProfundidadAct"],
                                    idDisenioRev = (int)sqlReader["idDisenioRev"],
                                    idOrdenCompra = (int)sqlReader["idOrdenCompra"],
                                    idDetalleCompra = (int)sqlReader["idDetalleCompra"],
                                    NomTipoLlanta = (string)sqlReader["NomTipoLlanta"],
                                    NomCondicion = (string)sqlReader["NomCondicion"],
                                    NomDisenioRev = (string)sqlReader["NomDisenioRev"]
                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCatLlantas(ref CatLlantas cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idSisLlanta = 0
        BEGIN
        INSERT  INTO CatLlantas
        ( idSisLlanta ,
          idLlanta ,
          NoSerie ,
          idEmpresa ,
          idProductoLlanta ,
          idTipoLLanta ,
          Factura ,
          Fecha ,
          Costo ,
          RFDI ,
          Posicion ,
          idtipoUbicacion ,
          idUbicacion ,
          Año ,
          Mes ,
          Observaciones ,
          UltFecha ,
          idCondicion ,
          Activo ,
          ProfundidadAct ,
          idDisenioRev ,
          idOrdenCompra ,
          idDetalleCompra
        )
VALUES  ( @IDSISLLANTA ,
          @IDLLANTA ,
          @NOSERIE ,
          @IDEMPRESA ,
          @IDPRODUCTOLLANTA ,
          @IDTIPOLLANTA ,
          @FACTURA ,
          @FECHA ,
          @COSTO ,
          @RFDI ,
          @POSICION ,
          @IDTIPOUBICACION ,
          @IDUBICACION ,
          @AÑO ,
          @MES ,
          @OBSERVACIONES ,
          @ULTFECHA ,
          @IDCONDICION ,
          @ACTIVO ,
          @PROFUNDIDADACT ,
          @IDDISENIOREV ,
          @IDORDENCOMPRA ,
          @IDDETALLECOMPRA
        );
                      
            SET @valor = 0;
            SET @mensaje = 'llanta Agregado con Exito';
            SET @id = @@IDENTITY;
            --SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  CatLlantas
SET     idLlanta = @idLlanta ,
        NoSerie = @NoSerie ,
        idEmpresa = @idEmpresa ,
        idProductoLlanta = @idProductoLlanta ,
        idTipoLLanta = @idTipoLLanta ,
        Factura = @Factura ,
        Fecha = @Fecha ,
        Costo = @Costo ,
        RFDI = @RFDI ,
        Posicion = @Posicion ,
        idtipoUbicacion = @idtipoUbicacion ,
        idUbicacion = @idUbicacion ,
        Año = @Año ,
        Mes = @Mes ,
        Observaciones = @Observaciones ,
        UltFecha = @UltFecha ,
        idCondicion = @idCondicion ,
        Activo = @Activo ,
        ProfundidadAct = @ProfundidadAct ,
        idDisenioRev = @idDisenioRev 
        --idOrdenCompra = @idOrdenCompra ,
        --idDetalleCompra = @idDetalleCompra
WHERE   idSisLlanta = @idSisLlanta;
            SET @valor = 0;
            SET @mensaje = 'llanta actualizado con Exito';
            SET @id = @idSisLlanta;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idSisLlanta", cat.idSisLlanta));
                        comand.Parameters.Add(new SqlParameter("idLlanta", cat.idLlanta));
                        comand.Parameters.Add(new SqlParameter("NoSerie", cat.NoSerie));
                        comand.Parameters.Add(new SqlParameter("idEmpresa", cat.idEmpresa));
                        comand.Parameters.Add(new SqlParameter("idProductoLlanta", cat.idProductoLlanta));
                        comand.Parameters.Add(new SqlParameter("idTipoLLanta", cat.idTipoLLanta));
                        comand.Parameters.Add(new SqlParameter("Factura", cat.Factura));
                        comand.Parameters.Add(new SqlParameter("Fecha", cat.Fecha));
                        comand.Parameters.Add(new SqlParameter("Costo", cat.Costo));
                        comand.Parameters.Add(new SqlParameter("RFDI", cat.RFDI));
                        comand.Parameters.Add(new SqlParameter("Posicion", cat.Posicion));
                        comand.Parameters.Add(new SqlParameter("idtipoUbicacion", cat.idtipoUbicacion));
                        comand.Parameters.Add(new SqlParameter("idUbicacion", cat.idUbicacion));
                        comand.Parameters.Add(new SqlParameter("Año", cat.Año));
                        comand.Parameters.Add(new SqlParameter("Mes", cat.Mes));
                        comand.Parameters.Add(new SqlParameter("Observaciones", cat.Observaciones));
                        comand.Parameters.Add(new SqlParameter("UltFecha", cat.UltFecha));
                        comand.Parameters.Add(new SqlParameter("idCondicion", cat.idCondicion));
                        comand.Parameters.Add(new SqlParameter("Activo", cat.Activo));
                        comand.Parameters.Add(new SqlParameter("ProfundidadAct", cat.ProfundidadAct));
                        comand.Parameters.Add(new SqlParameter("idDisenioRev", cat.idDisenioRev));
                        comand.Parameters.Add(new SqlParameter("idOrdenCompra", cat.idOrdenCompra));
                        comand.Parameters.Add(new SqlParameter("idDetalleCompra", cat.idDetalleCompra));


                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idSisLlanta = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

    }//
}
