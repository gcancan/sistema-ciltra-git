﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class DetOrdenActividadProductosServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getDetOrdenActividadProductosxFilter(int idDetOrdenActividadProductos = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idDetOrdenActividadProductos > 0)
                    {
                        filtro = " where idDetOrdenActividadProductos = " + idDetOrdenActividadProductos + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT d.idDetOrdenActividadProductos,
        d.idOrdenSer,
        d.idOrdenActividad,
        d.idProducto,
        p.CNOMBREPRODUCTO,
        p.CCODIGOPRODUCTO,
        d.idUnidadProd,
        d.Cantidad,
        --CASE WHEN cabpresal.EsDevolucion = 1 THEN d.Cantidad * - 1 ELSE d.Cantidad * 1 END AS Cantidad,
        d.Costo,
        d.Existencia,
        d.CantCompra,
        d.CantSalida,
        ISNULL(d.Precio,0) AS Precio
        FROM dbo.DetOrdenActividadProductos d 
        INNER JOIN dbo.CatProductosCOM p ON d.idProducto = p.CIDPRODUCTO 
        INNER JOIN dbo.DetOrdenServicio dos ON dos.idOrdenSer = d.idOrdenSer AND dos.idOrdenActividad = d.idOrdenActividad
        --INNER JOIN dbo.CabPreSalida cabpresal ON cabpresal.idOrdenTrabajo = dos.idOrdenTrabajo
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        comand.Parameters.Add(new SqlParameter("@idDetLiq", idDetOrdenActividadProductos));


                        connection.Open();
                        List<DetOrdenActividadProductos> lista = new List<DetOrdenActividadProductos>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetOrdenActividadProductos folio = new DetOrdenActividadProductos
                                {
                                    idDetOrdenActividadProductos = (int)sqlReader["idDetOrdenActividadProductos"],
                                    idOrdenSer = (int)sqlReader["idOrdenSer"],
                                    idOrdenActividad = (int)sqlReader["idOrdenActividad"],
                                    idProducto = (int)sqlReader["idProducto"],
                                    CCODIGOPRODUCTO = (string)sqlReader["CCODIGOPRODUCTO"],
                                    CNOMBREPRODUCTO = (string)sqlReader["CNOMBREPRODUCTO"],
                                    idUnidadProd = (int)sqlReader["idUnidadProd"],
                                    Cantidad = (decimal)sqlReader["Cantidad"],
                                    Costo = (decimal)sqlReader["Costo"],
                                    Existencia = (decimal)sqlReader["Existencia"],
                                    CantCompra = (decimal)sqlReader["CantCompra"],
                                    CantSalida = (decimal)sqlReader["CantSalida"],
                                    Precio = (decimal)sqlReader["Precio"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
        public OperationResult GuardaDetOrdenActividadProductos(ref DetOrdenActividadProductos folio)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
           
    IF @Inserta = 1
        BEGIN
            insert into DetOrdenActividadProductos
            (idOrdenSer,idOrdenActividad,idProducto,idUnidadProd,Cantidad,Costo,
             Existencia,CantCompra,CantSalida,Precio)
            values
            (@idOrdenSer,@idOrdenActividad,@idProducto,@idUnidadProd,@Cantidad,@Costo,
             @Existencia,@CantCompra,@CantSalida,@Precio)
                      
            SET @valor = 0;
            SET @mensaje = 'Actividad Producto Agregado con Exito';
            SET @id = @@IDENTITY;

       
            
        END;
    ELSE
        BEGIN
           UPDATE DetOrdenActividadProductos SET 
            idOrdenSer= @idOrdenSer,
            idOrdenActividad= @idOrdenActividad,
            idProducto= @idProducto,
            idUnidadProd= @idUnidadProd,
            Cantidad= @Cantidad,
            Costo= @Costo,
            Existencia= @Existencia,
            CantCompra= @CantCompra,
            CantSalida= @CantSalida,
            Precio= @Precio
            WHERE idDetOrdenActividadProductos = @idDetOrdenActividadProductos;
            
            SET @valor = 0;
            SET @mensaje = 'Actividad Producto actualizado con Exito';
            SET @id = @idDetOrdenActividadProductos;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("@idDetOrdenActividadProductos", folio.idDetOrdenActividadProductos));
                        comand.Parameters.Add(new SqlParameter("@idOrdenSer", folio.idOrdenSer));
                        comand.Parameters.Add(new SqlParameter("@idOrdenActividad", folio.idOrdenActividad));
                        comand.Parameters.Add(new SqlParameter("@idProducto", folio.idProducto));
                        comand.Parameters.Add(new SqlParameter("@idUnidadProd", folio.idUnidadProd));
                        comand.Parameters.Add(new SqlParameter("@Cantidad", folio.Cantidad));
                        comand.Parameters.Add(new SqlParameter("@Costo", folio.Costo));
                        comand.Parameters.Add(new SqlParameter("@Existencia", folio.Existencia));
                        comand.Parameters.Add(new SqlParameter("@CantCompra", folio.CantCompra));
                        comand.Parameters.Add(new SqlParameter("@CantSalida", folio.CantSalida));
                        comand.Parameters.Add(new SqlParameter("@Precio", folio.Precio));
                        comand.Parameters.Add(new SqlParameter("@Inserta", folio.Inserta));

                        //var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        //var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        //var spId = comand.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            folio.idDetOrdenActividadProductos = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }


        //PARA FACTURAR
        public OperationResult getDOSInsumosFACxFilter(int idOrdenSer = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idOrdenSer > 0)
                    {
                        filtro = " where d.idOrdenSer = " + idOrdenSer + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

        //            SELECT d.idProducto,
        //p.CNOMBREPRODUCTO,
        //p.CCODIGOPRODUCTO, 
        //sum(d.Cantidad) AS Cantidad,
        //isnull(d.Precio, 0) as Precio
        //FROM dbo.DetOrdenActividadProductos d
        //INNER JOIN dbo.CatProductosCOM p ON d.idProducto = p.CIDPRODUCTO
        //INNER JOIN dbo.DetOrdenServicio dos ON dos.idOrdenSer = d.idOrdenSer AND dos.idOrdenActividad = d.idOrdenActividad
        //INNER JOIN dbo.CabPreSalida cabpresal ON cabpresal.idOrdenTrabajo = dos.idOrdenTrabajo
        //{ 0}
        //            GROUP BY d.idProducto,p.CNOMBREPRODUCTO,p.CCODIGOPRODUCTO,d.Precio
                                strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT d.idProducto,
        p.CNOMBREPRODUCTO,
        p.CCODIGOPRODUCTO,
        SUM(d.Cantidad) AS Cantidad ,
        ISNULL(d.Precio, 0) AS Precio
        FROM dbo.DetOrdenActividadProductos d 
        INNER JOIN dbo.CatProductosCOM p ON d.idProducto = p.CIDPRODUCTO 
        INNER JOIN dbo.DetOrdenServicio dos ON dos.idOrdenSer = d.idOrdenSer AND dos.idOrdenActividad = d.idOrdenActividad
        --INNER JOIN dbo.CabPreSalida cabpresal ON cabpresal.idOrdenTrabajo = dos.idOrdenTrabajo
        {0}
        GROUP BY d.idProducto ,p.CNOMBREPRODUCTO ,p.CCODIGOPRODUCTO ,d.Precio

    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idDetLiq", idDetOrdenActividadProductos));


                        connection.Open();
                        List<DOSInsumosFAC> lista = new List<DOSInsumosFAC>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DOSInsumosFAC folio = new DOSInsumosFAC
                                {
                                    idProducto = (int)sqlReader["idProducto"],
                                    CNOMBREPRODUCTO = (string)sqlReader["CNOMBREPRODUCTO"],
                                    CCODIGOPRODUCTO = (string)sqlReader["CCODIGOPRODUCTO"],
                                    Cantidad = (decimal)sqlReader["Cantidad"],
                                    Precio = (decimal)sqlReader["Precio"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
        
    }//
}
