﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class RelTipOrdServServicios
    {
        private string strsql;
        private string filtro;

        public OperationResult getRelTipOrdServxFilter(int idTipOrdServ = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idTipOrdServ > 0)
                    {
                        filtro = " where C.idTipOrdServ = " + idTipOrdServ + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
       SELECT c.idTipOrdServ,
        c.NomCatalogo,
        c.NombreId,
        c.IdRelacion
        FROM dbo.RelTipOrdServ c
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<RelTipOrdServ> lista = new List<RelTipOrdServ>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                RelTipOrdServ cat = new RelTipOrdServ
                                {
                                    idTipOrdServ = (int)sqlReader["idTipOrdServ"],
                                    NomCatalogo = (string)sqlReader["NomCatalogo"],
                                    NombreId = (string)sqlReader["NombreId"],
                                    IdRelacion = (int)sqlReader["IdRelacion"]
                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaRelTipOrdServ(ref RelTipOrdServ cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idConcepto = 0
        BEGIN
        INSERT  INTO RelTipOrdServ
        ( idTipOrdServ ,
          NomCatalogo ,
          NombreId ,
          IdRelacion
        )
VALUES  ( @idTipOrdServ ,
          @NomCatalogo ,
          @NombreId ,
          @IdRelacion
        );
                      
            SET @valor = 0;
            SET @mensaje = 'Tipo de Unidad Agregado con Exito';
            --SET @id = @@IDENTITY;
            SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  RelTipOrdServ
SET     NomCatalogo = @NomCatalogo ,
        NombreId = @NombreId ,
        IdRelacion = @IdRelacion
WHERE   idTipOrdServ = @idTipOrdServ;
            SET @valor = 0;
            SET @mensaje = 'Tipo de Unidad actualizado con Exito';
            SET @id = @idUnidadTrans;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idTipOrdServ", cat.idTipOrdServ));
                        comand.Parameters.Add(new SqlParameter("NomCatalogo", cat.NomCatalogo));
                        comand.Parameters.Add(new SqlParameter("NombreId", cat.NombreId));
                        comand.Parameters.Add(new SqlParameter("IdRelacion", cat.IdRelacion));

                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idTipOrdServ = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
