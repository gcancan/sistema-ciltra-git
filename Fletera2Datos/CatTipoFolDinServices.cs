﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class CatTipoFolDinServices
    {
        private string strsql;
        public OperationResult getCatTipoFolDinxId(int idTipoFolDin = 0)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT idTipoFolDin,
        Descripcion,
        ConViaje
        FROM CatTipoFolDin
        WHERE   idTipoFolDin LIKE CASE @idTipoFolDin
        WHEN 0 THEN '%'
        ELSE CONVERT(NVARCHAR(10), @idTipoFolDin)
        END
        ORDER BY idTipoFolDin;
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.CommandType = CommandType.Text;
                        //comand.CommandText = "sp_getDiseñosLlantaAllOrById";
                        comand.Parameters.Add(new SqlParameter("@idTipoFolDin", idTipoFolDin));

                        //SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        //SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        List<CatTipoFolDin> catlista = new List<CatTipoFolDin>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CatTipoFolDin cat = new CatTipoFolDin
                                {
                                    idTipoFolDin = (int)sqlReader["idTipoFolDin"],
                                    Descripcion = (string)sqlReader["Descripcion"],
                                    ConViaje = (bool)sqlReader["ConViaje"]
                                };
                                catlista.Add(cat);
                            }

                        }
                        return new OperationResult(comand, catlista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCatTipoFolDin(ref CatTipoFolDin cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idTipoFolDin = 0
        BEGIN
            insert into CatTipoFolDin (Descripcion, Activo,ConViaje) values (@Descripcion, @Activo,@ConViaje);

            SET @valor = 0;
            SET @mensaje = 'Catalogo Agregado con Exito';
            SET @id = @@IDENTITY;
        END;
    ELSE
        BEGIN
           UPDATE CatTipoFolDin SET Descripcion = @Descripcion,
            Activo = @Activo,
            ConViaje = @ConViaje
            WHERE idTipoFolDin = @idTipoFolDin;
            
            SET @valor = 0;
            SET @mensaje = 'Catalogo actualizado con Exito';
            SET @id = @idTipoFolDin;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("@idTipoFolDin", cat.idTipoFolDin));
                        comand.Parameters.Add(new SqlParameter("@Descripcion", cat.Descripcion));
                        comand.Parameters.Add(new SqlParameter("@Activo", cat.Activo));
                        comand.Parameters.Add(new SqlParameter("@ConViaje", cat.ConViaje));

                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idTipoFolDin = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

    }//
}
