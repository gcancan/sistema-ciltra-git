﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class DetLiquidacionesRepServicios
    {
        private string strsql;
        private string filtro;

        public OperationResult getDetLiqViaRepxFilter(int idLiquidacion = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idLiquidacion > 0)
                    {
                        filtro = " where idLiquidacion = " + idLiquidacion + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT d.idLiquidacion,
        d.NoDocumento AS NumViaje,
        d.Descripcion AS Ruta,
        v.FechaHoraViaje AS FecSal,
        v.FechaHoraFin AS FecFin,
        v.TipoViaje,
        v.idTractor,
        isnull(v.idRemolque1,'') as idRemolque1,
        isnull(v.idDolly,'') as idDolly,
        isnull(v.idRemolque2,'') as idRemolque2,
        d.Importe
        FROM dbo.DetLiquidaciones d
        left JOIN dbo.CabGuia v ON d.NoDocumento = v.NumGuiaId
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        comand.Parameters.Add(new SqlParameter("@idLiquidacion", idLiquidacion));


                        connection.Open();
                        List<DetLiqViaRep> lista = new List<DetLiqViaRep>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetLiqViaRep folio = new DetLiqViaRep
                                {
                                    idLiquidacion = (int)sqlReader["idLiquidacion"],
                                    NumViaje = (int)sqlReader["NumViaje"],
                                    Ruta = (string)sqlReader["Ruta"],
                                    FecSal = (DateTime)sqlReader["FecSal"],
                                    FecFin = (DateTime)sqlReader["FecFin"],
                                    TipoViaje = (string)sqlReader["TipoViaje"],
                                    idTractor = (string)sqlReader["idTractor"],
                                    idRemolque1 = (string)sqlReader["idRemolque1"],
                                    idDolly = (string)sqlReader["idDolly"],
                                    idRemolque2 = (string)sqlReader["idRemolque2"],
                                    Importe = (decimal)sqlReader["Importe"],
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
        public OperationResult getDetLiqFolDinxFilter(int idLiquidacion = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idLiquidacion > 0)
                    {
                        filtro = " where d.idLiquidacion = " + idLiquidacion + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT d.idLiquidacion,
        d.NoDocumento AS Folio,
        f.idEmpleadoEnt,
        pe.NombreCompleto AS NomEntrega,
        d.Descripcion,
        d.Importe,
        d.IVA,
        (d.Importe + d.iva) AS Total
        FROM dbo.DetLiquidaciones d
        INNER JOIN dbo.FoliosDinero f ON f.idLiquidacion = d.idLiquidacion AND d.NoDocumento = f.FolioDin
        INNER JOIN dbo.CatPersonal pe ON f.idEmpleadoEnt = pe.idPersonal
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        comand.Parameters.Add(new SqlParameter("@idLiquidacion", idLiquidacion));


                        connection.Open();
                        List<DetLiqFolDinRep> lista = new List<DetLiqFolDinRep>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetLiqFolDinRep folio = new DetLiqFolDinRep
                                {
                                    idLiquidacion = (int)sqlReader["idLiquidacion"],
                                    Folio = (int)sqlReader["Folio"],
                                    idEmpleadoEnt = (int)sqlReader["idEmpleadoEnt"],
                                    NomEntrega = (string)sqlReader["NomEntrega"],
                                    Descripcion = (string)sqlReader["Descripcion"],
                                    Importe = (decimal)sqlReader["Importe"],
                                    iva = (decimal)sqlReader["iva"],
                                    Total = (decimal)sqlReader["Total"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
        public OperationResult getDetLiqComGasRepxFilter(int idLiquidacion = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idLiquidacion > 0)
                    {
                        filtro = " where d.idLiquidacion = " + idLiquidacion + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT d.idLiquidacion,
        d.NoDocumento AS Folio,
        c.NoDocumento,
        cg.NomGasto,
        d.Importe,
        d.iva,
        (d.Importe+ d.IVA) AS Total
        FROM dbo.DetLiquidaciones d
        INNER JOIN dbo.CompGastosFoliosDin c ON c.idLiquidacion = d.idLiquidacion AND d.NoDocumento = c.Folio
        INNER JOIN dbo.CatGastos cg ON c.idGasto = cg.idGasto
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        comand.Parameters.Add(new SqlParameter("@idLiquidacion", idLiquidacion));


                        connection.Open();
                        List<DetLiqComGasRep> lista = new List<DetLiqComGasRep>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetLiqComGasRep folio = new DetLiqComGasRep
                                {
                                    idLiquidacion = (int)sqlReader["idLiquidacion"],
                                    Folio = (int)sqlReader["Folio"],
                                    NoDocumento = (string)sqlReader["NoDocumento"],
                                    NomGasto = (string)sqlReader["NomGasto"],
                                    Importe = (decimal)sqlReader["Importe"],
                                    iva = (decimal)sqlReader["iva"],
                                    Total = (decimal)sqlReader["Total"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
        public OperationResult getDetLiqOtrConRepxFilter(int idLiquidacion = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idLiquidacion > 0)
                    {
                        filtro = " where idLiquidacion = " + idLiquidacion + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
    SELECT d.idLiquidacion,
    d.NoDocumento AS Folio,
    d.Descripcion,
    o.NumGuiaId AS NumViaje,
    d.Importe,
    d.iva,
    (d.importe + d.iva) as Total,
    c.EsCargo
    FROM dbo.DetLiquidaciones d
    INNER JOIN dbo.OtrosConcLiq o ON d.NoDocumento = o.NumFolio AND d.idLiquidacion = o.NumLiquidacion
    INNER JOIN dbo.CatConceptosLiq c ON c.idConcepto = o.idConcepto
    {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        comand.Parameters.Add(new SqlParameter("@idLiquidacion", idLiquidacion));


                        connection.Open();
                        List<DetLiqOtrConRep> lista = new List<DetLiqOtrConRep>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetLiqOtrConRep folio = new DetLiqOtrConRep
                                {
                                    idLiquidacion = (int)sqlReader["idLiquidacion"],
                                    Folio = (int)sqlReader["Folio"],
                                    NumViaje = (int)sqlReader["NumViaje"],
                                    Descripcion = (string)sqlReader["Descripcion"],
                                    Importe = (decimal)sqlReader["Importe"],
                                    iva = (decimal)sqlReader["iva"],
                                    Total = (decimal)sqlReader["Total"],
                                    EsCargo = (bool)sqlReader["EsCargo"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }


    }//
}
