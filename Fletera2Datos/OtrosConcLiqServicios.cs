﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class OtrosConcLiqServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getOtrosConcLiqxFilter(int NumFolio = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (NumFolio > 0)
                    {
                        filtro = " where NumFolio = " + NumFolio + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT o.NumFolio,
        o.NumGuiaId,
        o.idOperador,
		op.NombreCompleto AS NomOperador,
        o.idConcepto,
		c.NomConcepto,
        o.Importe,
        o.EnParcialidades,
        o.userCrea,
        pu.NombreCompleto AS NomUserCrea,
        o.Fecha,
        o.idEmpresa,
		e.RazonSocial,
        o.NumLiquidacion,
        o.Cancelado,
        o.MotivoCancela,
        o.ImporteAplicado,
        c.EsCargo 
        FROM dbo.OtrosConcLiq o
		INNER JOIN dbo.CatPersonal op ON o.idOperador = op.idPersonal
		INNER JOIN dbo.CatEmpresas e ON e.idEmpresa = o.idEmpresa
		INNER JOIN dbo.CatConceptosLiq c ON c.idConcepto = o.idConcepto
		INNER JOIN dbo.usuariosSys u ON o.userCrea = u.nombreUsuario
		INNER JOIN dbo.CatPersonal pu ON u.idPersonal = pu.idPersonal
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        comand.Parameters.Add(new SqlParameter("@NumFolio", NumFolio));


                        connection.Open();
                        List<OtrosConcLiq> lista = new List<OtrosConcLiq>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                OtrosConcLiq folio = new OtrosConcLiq
                                {
                                    NumFolio = (int)sqlReader["NumFolio"],
                                    NumGuiaId = (int)sqlReader["NumGuiaId"],
                                    idOperador = (int)sqlReader["idOperador"],
                                    NomOperador = (string)sqlReader["NomOperador"],
                                    idConcepto = (int)sqlReader["idConcepto"],
                                    NomConcepto = (string)sqlReader["NomConcepto"],
                                    Importe = (decimal)sqlReader["Importe"],
                                    EnParcialidades = (bool)sqlReader["EnParcialidades"],
                                    userCrea = (string)sqlReader["userCrea"],
                                    NomUserCrea = (string)sqlReader["NomUserCrea"],
                                    Fecha = (DateTime)sqlReader["Fecha"],
                                    idEmpresa = (int)sqlReader["idEmpresa"],
                                    RazonSocial = (string)sqlReader["RazonSocial"],
                                    NumLiquidacion = (int)sqlReader["NumLiquidacion"],
                                    Cancelado = (bool)sqlReader["Cancelado"],
                                    MotivoCancela = (string)sqlReader["MotivoCancela"],
                                    ImporteAplicado = (decimal)sqlReader["ImporteAplicado"],
                                    EsCargo = (bool)sqlReader["EsCargo"]

                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaOtrosConcLiq(ref OtrosConcLiq folio)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @NumFolio = 0
        BEGIN
            insert into OtrosConcLiq
            (NumGuiaId,idOperador,idConcepto,Importe,EnParcialidades,userCrea,Fecha,
             idEmpresa,NumLiquidacion,Cancelado,MotivoCancela,ImporteAplicado)
            values
            (@NumGuiaId,@idOperador,@idConcepto,@Importe,@EnParcialidades,@userCrea,@Fecha,
             @idEmpresa,@NumLiquidacion,@Cancelado,@MotivoCancela,@ImporteAplicado);
                      
            SET @valor = 0;
            SET @mensaje = 'Folio Agregado con Exito';
            SET @id = @@IDENTITY;

          
            
        END;
    ELSE
        BEGIN
           UPDATE OtrosConcLiq SET 
            NumGuiaId= @NumGuiaId,
            idOperador= @idOperador,
            idConcepto= @idConcepto,
            Importe= @Importe,
            EnParcialidades= @EnParcialidades,
            userCrea= @userCrea,
            Fecha= @Fecha,
            idEmpresa= @idEmpresa,
            NumLiquidacion= @NumLiquidacion,
            Cancelado= @Cancelado,
            MotivoCancela= @MotivoCancela,
            ImporteAplicado= @ImporteAplicado
            WHERE NumFolio= @NumFolio;
            SET @valor = 0;
            SET @mensaje = 'Folio actualizado con Exito';
            SET @id = @NumFolio;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("@NumFolio", folio.NumFolio));
                        comand.Parameters.Add(new SqlParameter("@NumGuiaId", folio.NumGuiaId));
                        comand.Parameters.Add(new SqlParameter("@idOperador", folio.idOperador));
                        comand.Parameters.Add(new SqlParameter("@idConcepto", folio.idConcepto));
                        comand.Parameters.Add(new SqlParameter("@Importe", folio.Importe));
                        comand.Parameters.Add(new SqlParameter("@userCrea", folio.userCrea));
                        comand.Parameters.Add(new SqlParameter("@Fecha", folio.Fecha));
                        comand.Parameters.Add(new SqlParameter("@idEmpresa", folio.idEmpresa));
                        comand.Parameters.Add(new SqlParameter("@NumLiquidacion", folio.NumLiquidacion));
                        comand.Parameters.Add(new SqlParameter("@Cancelado", folio.Cancelado));
                        comand.Parameters.Add(new SqlParameter("@MotivoCancela", folio.MotivoCancela));
                        comand.Parameters.Add(new SqlParameter("@ImporteAplicado", folio.ImporteAplicado));
                        comand.Parameters.Add(new SqlParameter("@EnParcialidades", folio.EnParcialidades));

                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            folio.NumFolio = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
