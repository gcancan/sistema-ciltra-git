﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class CompGastosFoliosDinServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getCompGastosFoliosDinxId(int Folio = 0, string filtroSinWhere = "", string sOrderby = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (Folio > 0)
                    {
                        filtro = " where Folio = " + Folio + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT c.Folio,
        c.NoDocumento,
        c.idGasto,
        g.NomGasto,
        c.Fecha,
        c.Importe,
        c.IVA,
        c.Deducible,
        c.Autoriza,
        isnull(a.NombreCompleto,'') as NomAutoriza,
        c.Observaciones,
        c.ReqComprobante,
        c.ReqAutorizacion,
        c.FolioDin,
        isnull(c.Consecutivo,0) as Consecutivo,
        isnull(c.idLiquidacion,0) as idLiquidacion,
        isnull(c.idEmpresa,0) as idEmpresa,
        e.RazonSocial 
        FROM dbo.CompGastosFoliosDin c
        INNER JOIN dbo.CatGastos g ON g.idGasto = c.idGasto
        left join dbo.CatPersonal a on c.Autoriza = a.idpersonal
        inner join dbo.CatEmpresas e on c.idEmpresa = e.idEmpresa
        {0} 
        {1}
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro, sOrderby);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        comand.Parameters.Add(new SqlParameter("@FolioDin", Folio));


                        connection.Open();
                        List<CompGastosFoliosDin> lista = new List<CompGastosFoliosDin>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CompGastosFoliosDin folio = new CompGastosFoliosDin
                                {
                                    Folio = (int)sqlReader["Folio"],
                                    NoDocumento = (string)sqlReader["NoDocumento"],
                                    idGasto = (int)sqlReader["idGasto"],
                                    NomGasto = (string)sqlReader["NomGasto"],
                                    Fecha = (DateTime)sqlReader["Fecha"],
                                    Importe = (decimal)sqlReader["Importe"],
                                    IVA = (decimal)sqlReader["IVA"],
                                    Deducible = (bool)sqlReader["Deducible"],
                                    Autoriza = (int)sqlReader["Autoriza"],
                                    NomAutoriza = (string)sqlReader["NomAutoriza"],
                                    Observaciones = (string)sqlReader["Observaciones"],
                                    ReqComprobante = (bool)sqlReader["ReqComprobante"],
                                    ReqAutorizacion = (bool)sqlReader["ReqAutorizacion"],
                                    FolioDin = (int)sqlReader["FolioDin"],
                                    Consecutivo = (int)sqlReader["Consecutivo"],
                                    idLiquidacion = (int)sqlReader["idLiquidacion"],
                                    idEmpresa = (int)sqlReader["idEmpresa"],
                                    RazonSocial = (string)sqlReader["RazonSocial"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCompGastosFoliosDin(ref CompGastosFoliosDin folio)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @Folio = 0
        BEGIN
            insert into CompGastosFoliosDin
            (NoDocumento,idGasto,Fecha,Importe,IVA,Deducible,Autoriza,
            Observaciones,ReqComprobante,ReqAutorizacion,FolioDin,Consecutivo,idLiquidacion,idEmpresa)
            values
            (@NoDocumento,@idGasto,@Fecha,@Importe,@IVA,@Deducible,@Autoriza,
            @Observaciones,@ReqComprobante,@ReqAutorizacion,@FolioDin,@Consecutivo,@idLiquidacion,@idEmpresa)
                      
            SET @valor = 0;
            SET @mensaje = 'Comprobacion Agregado con Exito';
            SET @id = @@IDENTITY;

        END;
    ELSE
        BEGIN
           UPDATE CompGastosFoliosDin SET 
            NoDocumento= @NoDocumento,
            idGasto= @idGasto,
            Fecha= @Fecha,
            Importe= @Importe,
            IVA= @IVA,
            Deducible= @Deducible,
            Autoriza= @Autoriza,
            Observaciones= @Observaciones,
            ReqComprobante= @ReqComprobante,
            ReqAutorizacion= @ReqAutorizacion,
            FolioDin= @FolioDin,
            Consecutivo = @Consecutivo,
            idLiquidacion = @idLiquidacion,
            idEmpresa = @idEmpresa
            WHERE Folio= @Folio;
            SET @valor = 0;
            SET @mensaje = 'Comprobacion actualizado con Exito';
            SET @id = @Folio;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("@Folio", folio.Folio));
                        comand.Parameters.Add(new SqlParameter("@NoDocumento", folio.NoDocumento));
                        comand.Parameters.Add(new SqlParameter("@idGasto", folio.idGasto));
                        comand.Parameters.Add(new SqlParameter("@Fecha", folio.Fecha));
                        comand.Parameters.Add(new SqlParameter("@Importe", folio.Importe));
                        comand.Parameters.Add(new SqlParameter("@IVA", folio.IVA));
                        comand.Parameters.Add(new SqlParameter("@Deducible", folio.Deducible));
                        comand.Parameters.Add(new SqlParameter("@Autoriza", folio.Autoriza));
                        comand.Parameters.Add(new SqlParameter("@Observaciones", folio.Observaciones));
                        comand.Parameters.Add(new SqlParameter("@ReqComprobante", folio.ReqComprobante));
                        comand.Parameters.Add(new SqlParameter("@ReqAutorizacion", folio.ReqAutorizacion));
                        comand.Parameters.Add(new SqlParameter("@FolioDin", folio.FolioDin));
                        comand.Parameters.Add(new SqlParameter("@Consecutivo", folio.Consecutivo));
                        comand.Parameters.Add(new SqlParameter("@idLiquidacion", folio.idLiquidacion));
                        comand.Parameters.Add(new SqlParameter("@idEmpresa", folio.idEmpresa));




                        //var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        //var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        //var spId = comand.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            folio.Folio = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
