﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class CabOrdenServicioServicios
    {
        private string strsql;
        private string filtro;

        #region Consultas

        private string strsqlDetRec = @"
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @Inserta = 1
        BEGIN
        --set @id = (select ISNULL(MAX(idOrdenTrabajo) + 1, 1) from DetRecepcionOS)
        INSERT  INTO DetRecepcionOS
        ( idOrdenTrabajo ,
          idOrdenSer ,
          idFamilia ,
          NotaRecepcion ,
          isCancelado ,
          motivoCancelacion ,
          idDivision ,
          idPersonalResp ,
          idPersonalAyu1 ,
          idPersonalAyu2 ,
          NotaRecepcionA
        )
VALUES  ( @idOrdenTrabajo ,
          @idOrdenSer ,
          @idFamilia ,
          @NotaRecepcion ,
          @isCancelado ,
          @motivoCancelacion ,
          @idDivision ,
          @idPersonalResp ,
          @idPersonalAyu1 ,
          @idPersonalAyu2 ,
          @NotaRecepcionA
        );
                      
            SET @valor = 0;
            SET @mensaje = 'orden de servicio Agregado con Exito';
            --SET @id = @@IDENTITY;
            SET @id = @idOrdenTrabajo;
        END;
    ELSE
        BEGIN
        UPDATE  DetRecepcionOS
SET     idOrdenSer = @idOrdenSer ,
        idFamilia = @idFamilia ,
        NotaRecepcion = @NotaRecepcion ,
        isCancelado = @isCancelado ,
        motivoCancelacion = @motivoCancelacion ,
        idDivision = @idDivision ,
        idPersonalResp = @idPersonalResp ,
        idPersonalAyu1 = @idPersonalAyu1 ,
        idPersonalAyu2 = @idPersonalAyu2 ,
        NotaRecepcionA = @NotaRecepcionA
WHERE   idOrdenTrabajo = @idOrdenTrabajo;
            SET @valor = 0;
            SET @mensaje = 'Tipo de Unidad actualizado con Exito';
            SET @id = @idOrdenTrabajo;
        END;
END TRY
BEGIN CATCH
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";

        private string strsqlMaster = @"
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idPadreOrdSer = 0
        BEGIN
        set @id = (select ISNULL(MAX(idPadreOrdSer) + 1, 1) from MasterOrdServ)
        INSERT  INTO MasterOrdServ
        ( idPadreOrdSer ,
          idTractor ,
          idTolva1 ,
          idDolly ,
          idTolva2 ,
          idChofer ,
          FechaCreacion ,
          UserCrea ,
          UserModifica ,
          FechaModifica ,
          UserCancela ,
          FechaCancela ,
          MotivoCancela ,
          Estatus
        )
VALUES  ( @id ,
          @idTractor ,
          @idTolva1 ,
          @idDolly ,
          @idTolva2 ,
          @idChofer ,
          @FechaCreacion ,
          @UserCrea ,
          @UserModifica ,
          @FechaModifica ,
          @UserCancela ,
          @FechaCancela ,
          @MotivoCancela ,
          @Estatus
        );
                      
            SET @valor = 0;
            SET @mensaje = 'Folio Padre Agregado con Exito';
            --SET @id = @@IDENTITY;
            --SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  MasterOrdServ
SET     idTractor = @idTractor ,
        idTolva1 = @idTolva1 ,
        idDolly = @idDolly ,
        idTolva2 = @idTolva2 ,
        idChofer = @idChofer ,
        FechaCreacion = @FechaCreacion ,
        UserCrea = @UserCrea ,
        UserModifica = @UserModifica ,
        FechaModifica = @FechaModifica ,
        UserCancela = @UserCancela ,
        FechaCancela = @FechaCancela ,
        MotivoCancela = @MotivoCancela ,
        Estatus = @Estatus
WHERE   idPadreOrdSer = @idPadreOrdSer;
            SET @valor = 0;
            SET @mensaje = 'Folio Padre actualizado con Exito';
            SET @id = @idPadreOrdSer;
        END;
END TRY
BEGIN CATCH
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";

        private string strsqlCab = @"
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @IDORDENSER = 0
        BEGIN
        set @id = (select ISNULL(MAX(IDORDENSER) + 1, 1) from CabOrdenServicio)
        INSERT  INTO CabOrdenServicio
        (  IDORDENSER,
          IDEMPRESA ,
          IDUNIDADTRANS ,
          IDTIPOORDEN ,
          KILOMETRAJE ,
          ESTATUS ,
          IDEMPLEADOAUTORIZA ,
          FECHACAPTURA ,
          IDTIPORDSERV ,
          IDTIPOSERVICIO ,
          IDPADREORDSER
        )
VALUES  ( @id ,
          @IDEMPRESA ,
          @IDUNIDADTRANS ,
          @IDTIPOORDEN ,
          @KILOMETRAJE ,
          @ESTATUS ,
          @IDEMPLEADOAUTORIZA ,
          @FECHACAPTURA ,
          @IDTIPORDSERV ,
          @IDTIPOSERVICIO ,
          @IDPADREORDSER
        );
                      
            SET @valor = 0;
            SET @mensaje = 'orden de servicio Agregado con Exito';
            --SET @id = @@IDENTITY;
            --SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  CabOrdenServicio
SET     IDEMPRESA = @IDEMPRESA ,
        IDUNIDADTRANS = @IDUNIDADTRANS ,
        IDTIPOORDEN = @IDTIPOORDEN ,
        KILOMETRAJE = @KILOMETRAJE ,
        ESTATUS = @ESTATUS ,
        IDEMPLEADOAUTORIZA = @IDEMPLEADOAUTORIZA ,
        FECHACAPTURA = @FECHACAPTURA ,
        IDTIPORDSERV = @IDTIPORDSERV ,
        IDTIPOSERVICIO = @IDTIPOSERVICIO ,
        IDPADREORDSER = @IDPADREORDSER,
        FechaRecepcion= null,
        idEmpleadoEntrega= null,
        UsuarioRecepcion= null,
        FechaAsignado= null,
        FechaTerminado= null,
        NotaFinal= null,
        FechaEntregado= null,
        idEmpleadoRecibe= null,
        UsuarioEntrega= null
        WHERE   IDORDENSER = @IDORDENSER;
            SET @valor = 0;
            SET @mensaje = 'Tipo de Unidad actualizado con Exito';
            SET @id = @IDORDENSER;
        END;
END TRY
BEGIN CATCH
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
        private string strsqlDet = @"
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idOrdenTrabajo = 0
        BEGIN
        set @id = (select ISNULL(MAX(idOrdenTrabajo) + 1, 1) from DetOrdenServicio)
        INSERT  INTO DetOrdenServicio
        ( idOrdenSer ,
          idServicio ,
          idActividad ,
          DuracionActHr ,
          NoPersonal ,
          id_proveedor ,
          Estatus ,
          CostoManoObra ,
          CostoProductos ,
          idOrdenTrabajo ,
          idDivision ,
          idLlanta ,
          PosLlanta ,
          NotaDiagnostico 
        )
VALUES  ( @idOrdenSer ,
          @idServicio ,
          @idActividad ,
          @DuracionActHr ,
          @NoPersonal ,
          @id_proveedor ,
          @Estatus ,
          @CostoManoObra ,
          @CostoProductos ,
          @id ,
          @idDivision ,
          @idLlanta ,
          @PosLlanta ,
          @NotaDiagnostico 
        );
                      
            SET @valor = 0;
            SET @mensaje = 'orden de servicio Agregado con Exito';
            --SET @id = @@IDENTITY;
            --SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  DetOrdenServicio
SET     idServicio = @idServicio ,
        idActividad = @idActividad ,
        DuracionActHr = @DuracionActHr ,
        NoPersonal = @NoPersonal ,
        id_proveedor = @id_proveedor ,
        Estatus = @Estatus ,
        CostoManoObra = @CostoManoObra ,
        CostoProductos = @CostoProductos ,
        idDivision = @idDivision ,
        idLlanta = @idLlanta ,
        PosLlanta = @PosLlanta ,
        NotaDiagnostico = @NotaDiagnostico,
        FechaAsig= @FechaAsig,
        UsuarioAsig= @UsuarioAsig,
        FechaTerminado= @FechaTerminado,
        UsuarioTerminado= @UsuarioTerminado,
        CIDPRODUCTO_SERV= @CIDPRODUCTO_SERV,
        Cantidad_CIDPRODUCTO_SERV= @Cantidad_CIDPRODUCTO_SERV,
        Precio_CIDPRODUCTO_SERV= @Precio_CIDPRODUCTO_SERV
WHERE   idOrdenTrabajo = @idOrdenTrabajo;
            SET @valor = 0;
            SET @mensaje = 'Tipo de Unidad actualizado con Exito';
            SET @id = @idOrdenTrabajo;
        END;
END TRY
BEGIN CATCH
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
        #endregion

        public OperationResult getCabOrdenServicioxFilter(int idOrdenSer = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idOrdenSer > 0)
                    {
                        filtro = " where c.idOrdenSer = " + idOrdenSer + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT c.idOrdenSer,
        c.IdEmpresa,
        e.RazonSocial,
        c.idUnidadTrans,
        ISNULL(tu.clasificacion,'Unidad Transp') AS NomTipoUnidad,
        c.idTipoOrden,
        isnull(c.FechaRecepcion,getdate()) as FechaRecepcion,
        isnull(c.idEmpleadoEntrega,0) as idEmpleadoEntrega,
        ISNULL(pe.NombreCompleto,'') AS NomEmpleadoEntrega,
        isnull(c.UsuarioRecepcion,'') as UsuarioRecepcion,
        c.Kilometraje,
        c.Estatus,
        isnull(c.FechaDiagnostico,getdate()) as FechaDiagnostico,
        isnull(c.FechaAsignado,getdate()) as FechaAsignado,
        isnull(c.FechaTerminado,getdate()) as FechaTerminado,
        isnull(c.FechaEntregado,getdate()) as FechaEntregado,
        isnull(c.UsuarioEntrega,'') as UsuarioEntrega,
        isnull(c.idEmpleadoRecibe,0) as idEmpleadoRecibe,
        isnull(c.NotaFinal,'') as NotaFinal,
        isnull(c.usuarioCan,'') as usuarioCan,
        isnull(c.FechaCancela,getdate()) as FechaCancela,
        c.idEmpleadoAutoriza,
        c.fechaCaptura,
        c.idTipOrdServ,
        tos.NomTipOrdServ,
        c.idTipoServicio,
        c.idPadreOrdSer,
		isnull(peaut.NombreCompleto,'') AS NomEmpleadoAutoriza,
		isnull(cts.NomTipoServicio,'') as NomTipoServicio,
		mas.idChofer AS idOperarador,
		isnull(op.NombreCompleto,'') AS NomOperador,
        c.UsuarioAsigna,
        c.UsuarioTermina
        FROM  CabOrdenServicio c
        INNER JOIN dbo.CatEmpresas e ON e.idEmpresa = c.IdEmpresa
        INNER JOIN dbo.CatUnidadTrans u ON c.idUnidadTrans = u.idUnidadTrans
        INNER JOIN dbo.CatTipoUniTrans tu ON tu.idTipoUniTras = u.idTipoUnidad
        INNER JOIN dbo.CatTipoOrdServ tos ON tos.idTipOrdServ = c.idTipOrdServ
        left JOIN dbo.CatPersonal pe ON c.idEmpleadoEntrega = pe.idPersonal
		LEFT JOIN dbo.CatPersonal peaut ON c.idEmpleadoAutoriza = peaut.idPersonal
		LEFT JOIN CatTipoServicio cts ON cts.idTipoServicio = c.idTipoServicio
		LEFT JOIN dbo.MasterOrdServ mas ON mas.idPadreOrdSer = c.idPadreOrdSer
		LEFT JOIN dbo.CatPersonal op ON mas.idChofer = op.idPersonal
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        comand.Parameters.Add(new SqlParameter("@idOrdenSer", idOrdenSer));


                        connection.Open();
                        List<CabOrdenServicio> lista = new List<CabOrdenServicio>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CabOrdenServicio folio = new CabOrdenServicio
                                {
                                    idOrdenSer = (int)sqlReader["idOrdenSer"],
                                    IdEmpresa = (int)sqlReader["IdEmpresa"],
                                    RazonSocial = (string)sqlReader["RazonSocial"],
                                    idUnidadTrans = (string)sqlReader["idUnidadTrans"],
                                    NomTipoUnidad = (string)sqlReader["NomTipoUnidad"],
                                    idTipoOrden = (int)sqlReader["idTipoOrden"],
                                    FechaRecepcion = (DateTime)sqlReader["FechaRecepcion"],
                                    idEmpleadoEntrega = (int)sqlReader["idEmpleadoEntrega"],
                                    NomEmpleadoEntrega = (string)sqlReader["NomEmpleadoEntrega"],
                                    UsuarioRecepcion = (string)sqlReader["UsuarioRecepcion"],
                                    Kilometraje = (decimal)sqlReader["Kilometraje"],
                                    Estatus = (string)sqlReader["Estatus"],
                                    FechaDiagnostico = (DateTime)sqlReader["FechaDiagnostico"],
                                    FechaAsignado = (DateTime)sqlReader["FechaAsignado"],
                                    FechaTerminado = (DateTime)sqlReader["FechaTerminado"],
                                    FechaEntregado = (DateTime)sqlReader["FechaEntregado"],
                                    UsuarioEntrega = (string)sqlReader["UsuarioEntrega"],
                                    idEmpleadoRecibe = (int)sqlReader["idEmpleadoRecibe"],
                                    NotaFinal = (string)sqlReader["NotaFinal"],
                                    usuarioCan = (string)sqlReader["usuarioCan"],
                                    FechaCancela = (DateTime)sqlReader["FechaCancela"],
                                    idEmpleadoAutoriza = (int)sqlReader["idEmpleadoAutoriza"],
                                    fechaCaptura = (DateTime)sqlReader["fechaCaptura"],
                                    idTipOrdServ = (int)sqlReader["idTipOrdServ"],
                                    NomTipOrdServ = (string)sqlReader["NomTipOrdServ"],
                                    idTipoServicio = (int)sqlReader["idTipoServicio"],
                                    idPadreOrdSer = (int)sqlReader["idPadreOrdSer"],
                                    NomEmpleadoAutoriza = (string)sqlReader["NomEmpleadoAutoriza"],
                                    NomTipoServicio = (string)sqlReader["NomTipoServicio"],
                                    idOperarador = (int)sqlReader["idOperarador"],
                                    NomOperador = (string)sqlReader["NomOperador"],
                                    UsuarioAsigna = (string)sqlReader["UsuarioAsigna"],
                                    UsuarioTermina = (string)sqlReader["UsuarioTermina"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCabOrdenServicio(ref CabOrdenServicio cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @IDORDENSER = 0
        BEGIN
        set @id = (select ISNULL(MAX(IDORDENSER) + 1, 1) from CabOrdenServicio)
        INSERT  INTO CabOrdenServicio
        (  IDORDENSER,
          IDEMPRESA ,
          IDUNIDADTRANS ,
          IDTIPOORDEN ,
          KILOMETRAJE ,
          ESTATUS ,
          IDEMPLEADOAUTORIZA ,
          FECHACAPTURA ,
          IDTIPORDSERV ,
          IDTIPOSERVICIO ,
          IDPADREORDSER
        )
VALUES  ( @id ,
          @IDEMPRESA ,
          @IDUNIDADTRANS ,
          @IDTIPOORDEN ,
          @KILOMETRAJE ,
          @ESTATUS ,
          @IDEMPLEADOAUTORIZA ,
          @FECHACAPTURA ,
          @IDTIPORDSERV ,
          @IDTIPOSERVICIO ,
          @IDPADREORDSER
        );
                      
            SET @valor = 0;
            SET @mensaje = 'orden de servicio Agregado con Exito';
            --SET @id = @@IDENTITY;
            --SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  CabOrdenServicio
SET     IDEMPRESA = @IDEMPRESA ,
        IDUNIDADTRANS = @IDUNIDADTRANS ,
        IDTIPOORDEN = @IDTIPOORDEN ,
        KILOMETRAJE = @KILOMETRAJE ,
        ESTATUS = @ESTATUS ,
        IDEMPLEADOAUTORIZA = @IDEMPLEADOAUTORIZA ,
        FECHACAPTURA = @FECHACAPTURA ,
        IDTIPORDSERV = @IDTIPORDSERV ,
        IDTIPOSERVICIO = @IDTIPOSERVICIO ,
        IDPADREORDSER = @IDPADREORDSER,
        FechaRecepcion= @FechaRecepcion,
        idEmpleadoEntrega= @idEmpleadoEntrega,
        UsuarioRecepcion= @UsuarioRecepcion,
        FechaAsignado= @FechaAsignado,
        FechaTerminado= @FechaTerminado,
        NotaFinal= @NotaFinal,
        FechaEntregado= @FechaEntregado,
        idEmpleadoRecibe= @idEmpleadoRecibe,
        UsuarioEntrega= @UsuarioEntrega,
        UsuarioAsigna= @UsuarioAsigna,
        UsuarioTermina= @UsuarioTermina 
        WHERE   IDORDENSER = @IDORDENSER;
            SET @valor = 0;
            SET @mensaje = 'Tipo de Unidad actualizado con Exito';
            SET @id = @IDORDENSER;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idOrdenSer", cat.idOrdenSer));
                        comand.Parameters.Add(new SqlParameter("IdEmpresa", cat.IdEmpresa));
                        comand.Parameters.Add(new SqlParameter("idUnidadTrans", cat.idUnidadTrans));
                        comand.Parameters.Add(new SqlParameter("idTipoOrden", cat.idTipoOrden));
                        comand.Parameters.Add(new SqlParameter("Kilometraje", cat.Kilometraje));
                        comand.Parameters.Add(new SqlParameter("Estatus", cat.Estatus));
                        comand.Parameters.Add(new SqlParameter("idEmpleadoAutoriza", cat.idEmpleadoAutoriza));
                        comand.Parameters.Add(new SqlParameter("fechaCaptura", cat.fechaCaptura));
                        comand.Parameters.Add(new SqlParameter("idTipOrdServ", cat.idTipOrdServ));
                        comand.Parameters.Add(new SqlParameter("idTipoServicio", cat.idTipoServicio));
                        comand.Parameters.Add(new SqlParameter("idPadreOrdSer", cat.idPadreOrdSer));
                        comand.Parameters.Add(new SqlParameter("FechaRecepcion", cat.FechaRecepcion));
                        comand.Parameters.Add(new SqlParameter("idEmpleadoEntrega", cat.idEmpleadoEntrega));
                        comand.Parameters.Add(new SqlParameter("UsuarioRecepcion", cat.UsuarioRecepcion));

                        if (cat.Estatus == "REC")
                        {
                            comand.Parameters.Add(new SqlParameter("FechaAsignado", DBNull.Value));
                            comand.Parameters.Add(new SqlParameter("FechaTerminado", DBNull.Value));
                            comand.Parameters.Add(new SqlParameter("NotaFinal", DBNull.Value));
                            comand.Parameters.Add(new SqlParameter("FechaEntregado", DBNull.Value));
                            comand.Parameters.Add(new SqlParameter("idEmpleadoRecibe", DBNull.Value));
                            comand.Parameters.Add(new SqlParameter("UsuarioEntrega", DBNull.Value));
                            comand.Parameters.Add(new SqlParameter("UsuarioAsigna", ""));
                            comand.Parameters.Add(new SqlParameter("UsuarioTermina", ""));
                        }
                        else if (cat.Estatus == "ASIG")
                        {
                            comand.Parameters.Add(new SqlParameter("FechaAsignado", cat.FechaAsignado));
                            comand.Parameters.Add(new SqlParameter("FechaTerminado", DBNull.Value));
                            comand.Parameters.Add(new SqlParameter("NotaFinal", DBNull.Value));
                            comand.Parameters.Add(new SqlParameter("FechaEntregado", DBNull.Value));
                            comand.Parameters.Add(new SqlParameter("idEmpleadoRecibe", DBNull.Value));
                            comand.Parameters.Add(new SqlParameter("UsuarioEntrega", DBNull.Value));
                            comand.Parameters.Add(new SqlParameter("UsuarioAsigna", cat.UsuarioAsigna));
                            comand.Parameters.Add(new SqlParameter("UsuarioTermina", ""));
                        }
                        else if (cat.Estatus == "TER")
                        {
                            comand.Parameters.Add(new SqlParameter("FechaAsignado", cat.FechaAsignado));
                            comand.Parameters.Add(new SqlParameter("FechaTerminado", cat.FechaTerminado));
                            comand.Parameters.Add(new SqlParameter("NotaFinal", DBNull.Value));
                            comand.Parameters.Add(new SqlParameter("FechaEntregado", DBNull.Value));
                            comand.Parameters.Add(new SqlParameter("idEmpleadoRecibe", DBNull.Value));
                            comand.Parameters.Add(new SqlParameter("UsuarioEntrega", DBNull.Value));
                            comand.Parameters.Add(new SqlParameter("UsuarioAsigna", cat.UsuarioAsigna));
                            comand.Parameters.Add(new SqlParameter("UsuarioTermina", cat.UsuarioTermina));

                        }
                        else if (cat.Estatus == "ENT")
                        {
                            comand.Parameters.Add(new SqlParameter("FechaAsignado", cat.FechaAsignado));
                            comand.Parameters.Add(new SqlParameter("FechaTerminado", cat.FechaTerminado));
                            comand.Parameters.Add(new SqlParameter("NotaFinal", DBNull.Value));
                            comand.Parameters.Add(new SqlParameter("FechaEntregado", cat.FechaEntregado));
                            comand.Parameters.Add(new SqlParameter("idEmpleadoRecibe", cat.idEmpleadoRecibe));
                            comand.Parameters.Add(new SqlParameter("UsuarioEntrega", cat.UsuarioEntrega));

                        }

                        else
                        {
                            comand.Parameters.Add(new SqlParameter("FechaAsignado", cat.FechaAsignado));
                            comand.Parameters.Add(new SqlParameter("FechaTerminado", cat.FechaTerminado));
                            comand.Parameters.Add(new SqlParameter("NotaFinal", cat.NotaFinal));
                            comand.Parameters.Add(new SqlParameter("FechaEntregado", cat.FechaEntregado));
                            comand.Parameters.Add(new SqlParameter("idEmpleadoRecibe", cat.idEmpleadoRecibe));
                            comand.Parameters.Add(new SqlParameter("UsuarioEntrega", cat.UsuarioEntrega));
                            //if (cat.idTipoOrden == 3)
                            //{
                            //    comand.Parameters.Add(new SqlParameter("UsuarioAsigna", cat.UsuarioAsigna));
                            //    comand.Parameters.Add(new SqlParameter("UsuarioTermina", cat.UsuarioTermina));

                            //}
                        }



                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idOrdenSer = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
        //una sola transaccion CABECERO; DETALLLE; RECEPCION
        //public OperationResult GuardaOrdenServicio(ref CabOrdenServicio cab, ref List<DetOrdenServicio> det, ref List<DetRecepcionOS> detRec)
        public OperationResult GuardaOrdenServicio(ref MasterOrdServ_f2 master)
        {
            try
            {
                //int contOT = 0;
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    connection.Open();
                    using (SqlTransaction trans = connection.BeginTransaction("save"))
                    {
                        using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsqlMaster, true))
                        {
                            //SE CREA EL MASTER
                            comand.Transaction = trans;

                            comand.Parameters.Add(new SqlParameter("idPadreOrdSer", master.idPadreOrdSer));
                            comand.Parameters.Add(new SqlParameter("idTractor", master.idTractor));
                            comand.Parameters.Add(new SqlParameter("idTolva1", master.idTolva1));
                            comand.Parameters.Add(new SqlParameter("idDolly", master.idDolly));
                            comand.Parameters.Add(new SqlParameter("idTolva2", master.idTolva2));
                            comand.Parameters.Add(new SqlParameter("idChofer", master.idChofer));
                            comand.Parameters.Add(new SqlParameter("FechaCreacion", master.FechaCreacion));
                            comand.Parameters.Add(new SqlParameter("UserCrea", master.UserCrea));
                            comand.Parameters.Add(new SqlParameter("UserModifica", master.UserModifica));
                            comand.Parameters.Add(new SqlParameter("FechaModifica", master.FechaModifica));
                            comand.Parameters.Add(new SqlParameter("UserCancela", master.UserCancela));
                            comand.Parameters.Add(new SqlParameter("FechaCancela", master.FechaCancela));
                            comand.Parameters.Add(new SqlParameter("MotivoCancela", master.MotivoCancela));
                            comand.Parameters.Add(new SqlParameter("Estatus", master.Estatus));

                            comand.ExecuteNonQuery();
                            if ((int)comand.Parameters["@valor"].Value == 0)
                            {
                                master.idPadreOrdSer = (int)comand.Parameters["@id"].Value;
                                
                                foreach (var itemcab in master.listaCab)
                                {
                                    //SE CREAN LOS CABECEROS
                                    comand.CommandText = strsqlCab;
                                    comand.Parameters.Clear();
                                    foreach (var param in CrearSqlCommand.getPatametros(true))
                                    {
                                        comand.Parameters.Add(param);

                                    }
                                    itemcab.idOrdenSer = 0;
                                    comand.Parameters.Add(new SqlParameter("idOrdenSer", itemcab.idOrdenSer));
                                    comand.Parameters.Add(new SqlParameter("IdEmpresa", itemcab.IdEmpresa));
                                    comand.Parameters.Add(new SqlParameter("idUnidadTrans", itemcab.idUnidadTrans));
                                    comand.Parameters.Add(new SqlParameter("idTipoOrden", itemcab.idTipoOrden));
                                    comand.Parameters.Add(new SqlParameter("Kilometraje", itemcab.Kilometraje));
                                    comand.Parameters.Add(new SqlParameter("Estatus", itemcab.Estatus));
                                    comand.Parameters.Add(new SqlParameter("idEmpleadoAutoriza", itemcab.idEmpleadoAutoriza));
                                    comand.Parameters.Add(new SqlParameter("fechaCaptura", itemcab.fechaCaptura));
                                    comand.Parameters.Add(new SqlParameter("idTipOrdServ", itemcab.idTipOrdServ));
                                    comand.Parameters.Add(new SqlParameter("idTipoServicio", itemcab.idTipoServicio));
                                    comand.Parameters.Add(new SqlParameter("idPadreOrdSer", master.idPadreOrdSer));
                                    //comand.Parameters.Add(new SqlParameter("FechaRecepcion", itemcab.FechaRecepcion));
                                    //comand.Parameters.Add(new SqlParameter("idEmpleadoEntrega", itemcab.idEmpleadoEntrega));
                                    //comand.Parameters.Add(new SqlParameter("UsuarioRecepcion", itemcab.UsuarioRecepcion));
                                    //comand.Parameters.Add(new SqlParameter("FechaAsignado", itemcab.FechaAsignado));
                                    //comand.Parameters.Add(new SqlParameter("FechaTerminado", itemcab.FechaTerminado));
                                    //comand.Parameters.Add(new SqlParameter("NotaFinal", itemcab.NotaFinal));
                                    //comand.Parameters.Add(new SqlParameter("FechaEntregado", itemcab.FechaEntregado));
                                    //comand.Parameters.Add(new SqlParameter("idEmpleadoRecibe", itemcab.idEmpleadoRecibe));
                                    //comand.Parameters.Add(new SqlParameter("UsuarioEntrega", itemcab.UsuarioEntrega));

                                    comand.ExecuteNonQuery();
                                    if ((int)comand.Parameters["@valor"].Value == 0)
                                    {
                                        itemcab.idOrdenSer = (int)comand.Parameters["@id"].Value;
                                        
                                        //SE CREAN LOS DETALLES
                                        foreach (var itemdet in itemcab.listaDet)
                                        {
                                            comand.CommandText = strsqlDet;
                                            comand.Parameters.Clear();
                                            foreach (var param in CrearSqlCommand.getPatametros(true))
                                            {
                                                comand.Parameters.Add(param);

                                            }
                                            itemdet.idOrdenTrabajo = 0;
                                            comand.Parameters.Add(new SqlParameter("idOrdenSer", itemcab.idOrdenSer));
                                            comand.Parameters.Add(new SqlParameter("idOrdenActividad", itemdet.idOrdenActividad));
                                            comand.Parameters.Add(new SqlParameter("idServicio", itemdet.idServicio));
                                            comand.Parameters.Add(new SqlParameter("idActividad", itemdet.idActividad));
                                            comand.Parameters.Add(new SqlParameter("DuracionActHr", itemdet.DuracionActHr));
                                            comand.Parameters.Add(new SqlParameter("NoPersonal", itemdet.NoPersonal));
                                            comand.Parameters.Add(new SqlParameter("id_proveedor", itemdet.id_proveedor));
                                            comand.Parameters.Add(new SqlParameter("Estatus", itemdet.Estatus));
                                            comand.Parameters.Add(new SqlParameter("CostoManoObra", itemdet.CostoManoObra));
                                            comand.Parameters.Add(new SqlParameter("CostoProductos", itemdet.CostoProductos));
                                            comand.Parameters.Add(new SqlParameter("idOrdenTrabajo", itemdet.idOrdenTrabajo));
                                            comand.Parameters.Add(new SqlParameter("idDivision", itemdet.idDivision));
                                            comand.Parameters.Add(new SqlParameter("idLlanta", itemdet.idLlanta));
                                            comand.Parameters.Add(new SqlParameter("PosLlanta", itemdet.PosLlanta));
                                            comand.Parameters.Add(new SqlParameter("NotaDiagnostico", itemdet.NotaDiagnostico));
                                            comand.Parameters.Add(new SqlParameter("FechaAsig", itemdet.FechaAsig));
                                            comand.Parameters.Add(new SqlParameter("UsuarioAsig", itemdet.UsuarioAsig));
                                            comand.Parameters.Add(new SqlParameter("FechaTerminado", itemdet.FechaTerminado));
                                            comand.Parameters.Add(new SqlParameter("UsuarioTerminado", itemdet.UsuarioTerminado));
                                            comand.Parameters.Add(new SqlParameter("CIDPRODUCTO_SERV", itemdet.CIDPRODUCTO_SERV));
                                            comand.Parameters.Add(new SqlParameter("Cantidad_CIDPRODUCTO_SERV", itemdet.Cantidad_CIDPRODUCTO_SERV));
                                            comand.Parameters.Add(new SqlParameter("Precio_CIDPRODUCTO_SERV", itemdet.Precio_CIDPRODUCTO_SERV));

                                            comand.ExecuteNonQuery();
                                            if ((int)comand.Parameters["@valor"].Value == 0)
                                            {
                                                itemdet.idOrdenTrabajo = (int)comand.Parameters["@id"].Value;
                                                comand.CommandText = strsqlDetRec;
                                                comand.Parameters.Clear();
                                                foreach (var param in CrearSqlCommand.getPatametros(true))
                                                {
                                                    comand.Parameters.Add(param);

                                                }
                                                itemdet.DetRec.Inserta = true;
                                                comand.Parameters.Add(new SqlParameter("idOrdenTrabajo", itemdet.idOrdenTrabajo));
                                                comand.Parameters.Add(new SqlParameter("idOrdenSer", itemcab.idOrdenSer));
                                                comand.Parameters.Add(new SqlParameter("idFamilia", itemdet.DetRec.idFamilia));
                                                comand.Parameters.Add(new SqlParameter("NotaRecepcion", itemdet.DetRec.NotaRecepcion));
                                                comand.Parameters.Add(new SqlParameter("isCancelado", itemdet.DetRec.isCancelado));
                                                comand.Parameters.Add(new SqlParameter("motivoCancelacion", itemdet.DetRec.motivoCancelacion));
                                                comand.Parameters.Add(new SqlParameter("idDivision", itemdet.DetRec.idDivision));
                                                comand.Parameters.Add(new SqlParameter("idPersonalResp", itemdet.DetRec.idPersonalResp));
                                                comand.Parameters.Add(new SqlParameter("idPersonalAyu1", itemdet.DetRec.idPersonalAyu1));
                                                comand.Parameters.Add(new SqlParameter("idPersonalAyu2", itemdet.DetRec.idPersonalAyu2));
                                                comand.Parameters.Add(new SqlParameter("NotaRecepcionA", itemdet.DetRec.NotaRecepcionA));
                                                //comand.Parameters.Add(new SqlParameter("Inserta", itemdet.DetRec.Inserta));
                                                comand.Parameters.Add(new SqlParameter("Inserta", itemdet.DetRec.Inserta));

                                                comand.ExecuteNonQuery();
                                                if ((int)comand.Parameters["@valor"].Value != 0)
                                                {
                                                    trans.Rollback("save");
                                                    return new OperationResult(comand);
                                                }
                                            }
                                            else
                                            {
                                                trans.Rollback("save");
                                                return new OperationResult(comand);

                                            }
                                        }
                                    }
                                    else
                                    {
                                        trans.Rollback("save");
                                        return new OperationResult(comand);

                                    }
                                }
                            }
                            else
                            {
                                trans.Rollback("save");
                                return new OperationResult(comand);
                            }
                            trans.Commit();
                            return new OperationResult(comand);
                        }
                    }

                  
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        //una sola transaccion
    }//
}
