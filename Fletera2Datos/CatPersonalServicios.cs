﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class CatPersonalServicios
    {
        private string strsql;
        public OperationResult getCatPersonalxFiltro(string filtroSinWhere = "",string Ordena = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    if (filtroSinWhere != "")
                    {
                        filtroSinWhere = " WHERE " + filtroSinWhere;
                    }
                    if (Ordena != "")
                    {
                        filtroSinWhere = filtroSinWhere + " order by " + Ordena;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
       SELECT per.idPuesto,
        pue.NomPuesto,
        per.idDepto,
        isnull(dep.NomDepto,'') as NomDepto,
        per.idPersonal,
        per.idEmpresa,
        per.NombreCompleto,
        per.Nombre,
        per.ApellidoPat,
        per.ApellidoMat 
        FROM dbo.CatPersonal Per 
        LEFT JOIN dbo.CatPuestos pue ON pue.idPuesto = Per.idPuesto 
        LEFT JOIN dbo.CatDeptos dep ON dep.IdDepto = Per.idDepto 
        {0} 
        ; 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtroSinWhere);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {

                        //comand.Parameters.Add(new SqlParameter("@idTipoFolDin", idTipoFolDin));

                        //SqlParameter spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        //SqlParameter spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });

                        connection.Open();
                        List<CatPersonal> catlista = new List<CatPersonal>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CatPersonal cat = new CatPersonal
                                {
                                    idPersonal = (int)sqlReader["idPersonal"],
                                    idEmpresa = (int)sqlReader["idEmpresa"],
                                    NombreCompleto = (string)sqlReader["NombreCompleto"],
                                    Nombre = (string)sqlReader["Nombre"],
                                    ApellidoPat = (string)sqlReader["ApellidoPat"],
                                    ApellidoMat = (string)sqlReader["ApellidoMat"],
                                    idPuesto = (int)sqlReader["idPuesto"],
                                    idDepto = (int)sqlReader["idDepto"],
                                    NomPuesto = (string)sqlReader["NomPuesto"],
                                    NomDepto = (string)sqlReader["NomDepto"]

                                };
                                catlista.Add(cat);
                            }

                        }
                        return new OperationResult(comand, catlista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
