﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class CatTipoUniTransServicios
    {
        private string strsql;
        private string filtro;

        public OperationResult getCatTipoUniTransxFilter(int idTipoUniTras = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idTipoUniTras > 0)
                    {
                        filtro = " where C.idTipoUniTras = " + idTipoUniTras + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
       SELECT c.idTipoUniTras,
        c.nomTipoUniTras,
        c.NoLlantas,
        c.NoEjes,
        c.TonelajeMax,
        isnull(c.clasificacion,'') as clasificacion,
        isnull(c.bCombustible,0) as bCombustible,
        c.idClasLlan,
        c.grupo,
        c.TipoUso
FROM dbo.CatTipoUniTrans c
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<CatTipoUniTrans> lista = new List<CatTipoUniTrans>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CatTipoUniTrans cat = new CatTipoUniTrans
                                {
                                    idTipoUniTras = (int)sqlReader["idTipoUniTras"],
                                    nomTipoUniTras = (string)sqlReader["nomTipoUniTras"],
                                    NoLlantas = (string)sqlReader["NoLlantas"],
                                    NoEjes = (string)sqlReader["NoEjes"],
                                    TonelajeMax = (string)sqlReader["TonelajeMax"],
                                    clasificacion = (string)sqlReader["clasificacion"],
                                    bCombustible = (bool)sqlReader["bCombustible"],
                                    idClasLlan = (int)sqlReader["idClasLlan"],
                                    grupo = (string)sqlReader["grupo"],
                                    TipoUso = (string)sqlReader["TipoUso"]
                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCatTipoUniTrans(ref CatTipoUniTrans cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idConcepto = 0
        BEGIN
        INSERT  INTO CatTipoUniTrans
        ( idTipoUniTras ,
          nomTipoUniTras ,
          NoLlantas ,
          NoEjes ,
          TonelajeMax ,
          clasificacion ,
          bCombustible ,
          idClasLlan ,
          grupo ,
          TipoUso
        )
VALUES  ( @idTipoUniTras ,
          @nomTipoUniTras ,
          @NoLlantas ,
          @NoEjes ,
          @TonelajeMax ,
          @clasificacion ,
          @bCombustible ,
          @idClasLlan ,
          @grupo ,
          @TipoUso
        );
                      
            SET @valor = 0;
            SET @mensaje = 'Tipo de Unidad Agregado con Exito';
            --SET @id = @@IDENTITY;
            SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  CatTipoUniTrans
SET     nomTipoUniTras = @nomTipoUniTras ,
        NoLlantas = @NoLlantas ,
        NoEjes = @NoEjes ,
        TonelajeMax = @TonelajeMax ,
        clasificacion = @clasificacion ,
        bCombustible = @bCombustible ,
        idClasLlan = @idClasLlan ,
        grupo = @grupo ,
        TipoUso = @TipoUso
WHERE   idTipoUniTras = @idTipoUniTras;
            SET @valor = 0;
            SET @mensaje = 'Tipo de Unidad actualizado con Exito';
            SET @id = @idUnidadTrans;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idTipoUniTras", cat.idTipoUniTras));
                        comand.Parameters.Add(new SqlParameter("nomTipoUniTras", cat.nomTipoUniTras));
                        comand.Parameters.Add(new SqlParameter("NoLlantas", cat.NoLlantas));
                        comand.Parameters.Add(new SqlParameter("NoEjes", cat.NoEjes));
                        comand.Parameters.Add(new SqlParameter("TonelajeMax", cat.TonelajeMax));
                        comand.Parameters.Add(new SqlParameter("clasificacion", cat.clasificacion));
                        comand.Parameters.Add(new SqlParameter("bCombustible", cat.bCombustible));
                        comand.Parameters.Add(new SqlParameter("idClasLlan", cat.idClasLlan));
                        comand.Parameters.Add(new SqlParameter("grupo", cat.grupo));
                        comand.Parameters.Add(new SqlParameter("TipoUso", cat.TipoUso));

                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idTipoUniTras = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
