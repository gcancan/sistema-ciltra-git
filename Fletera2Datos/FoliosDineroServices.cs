﻿
using Core.Models;
using Core.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;

namespace Fletera2Datos
{
    public class FoliosDineroServices
    {
        private string strsql;
        private string filtro;
        public OperationResult getFoliosDineroxId(int FolioDin = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (FolioDin > 0)
                    {
                        filtro = " where FolioDin = " + FolioDin + (filtroSinWhere != "" ? " AND " + filtroSinWhere: filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT fd.FolioDin,
        fd.Fecha,
        fd.idEmpleadoEnt,
        pe.NombreCompleto AS NombreEnt,
        fd.idEmpleadoRec,
        pr.NombreCompleto AS NombreRec,
        fd.Estatus,
        fd.Concepto,
        fd.idEmpresa,
        em.RazonSocial,
        fd.idTipoFolDin,
        tp.Descripcion AS DescripcionFolDin,
        isnull(fd.Importe,0) as Importe,
        isnull(fd.ImporteEnt,0) as ImporteEnt,
        isnull(fd.ImporteComp,0) as ImporteComp,
        fd.FolioDinPadre,
        isnull(fd.idLiquidacion,0) as idLiquidacion, 
        isnull(fd.NumGuiaId,0) as NumGuiaId
        FROM dbo.FoliosDinero FD
        INNER JOIN dbo.CatPersonal PR ON fd.idEmpleadoRec = pr.idPersonal
        INNER JOIN dbo.CatPersonal PE ON fd.idEmpleadoEnt = pe.idPersonal
        INNER JOIN dbo.CatTipoFolDin tp ON fd.idTipoFolDin = tp.idTipoFolDin
        INNER JOIN dbo.CatEmpresas em ON em.idEmpresa = FD.idEmpresa
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection,strsql,false))
                    {
                        comand.Parameters.Add(new SqlParameter("@FolioDin", FolioDin));


                        connection.Open();
                        List<FoliosDinero> lista = new List<FoliosDinero>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                FoliosDinero folio = new FoliosDinero
                                {
                                    FolioDin = (int)sqlReader["FolioDin"],
                                    Fecha = (DateTime)sqlReader["Fecha"],
                                    idEmpleadoEnt = (int)sqlReader["idEmpleadoEnt"],
                                    NombreEnt = (string)sqlReader["NombreEnt"],
                                    idEmpleadoRec = (int)sqlReader["idEmpleadoRec"],
                                    NombreRec = (string)sqlReader["NombreRec"],
                                    Estatus = (string)sqlReader["Estatus"],
                                    Concepto = (string)sqlReader["Concepto"],
                                    idEmpresa = (int)sqlReader["idEmpresa"],
                                    RazonSocial = (string)sqlReader["RazonSocial"],
                                    idTipoFolDin = (int)sqlReader["idTipoFolDin"],
                                    DescripcionFolDin = (string)sqlReader["DescripcionFolDin"],
                                    Importe = (decimal)sqlReader["Importe"],
                                    ImporteEnt = (decimal)sqlReader["ImporteEnt"],
                                    ImporteComp = (decimal)sqlReader["ImporteComp"],
                                    FolioDinPadre = (int)sqlReader["FolioDinPadre"],
                                    idLiquidacion = (int)sqlReader["idLiquidacion"],
                                    NumGuiaId = (int)sqlReader["NumGuiaId"]

                                };
                                lista.Add(folio);
                            }
                            

                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaFoliosDinero(ref FoliosDinero folio)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @FolioDin = 0
        BEGIN
            insert into FoliosDinero
            (Fecha,idEmpleadoEnt,idEmpleadoRec,Estatus,Concepto,
            idEmpresa,idTipoFolDin,Importe,ImporteEnt,ImporteComp,FolioDinPadre,idLiquidacion,NumGuiaId)
            values
            (@Fecha,@idEmpleadoEnt,@idEmpleadoRec,@Estatus,@Concepto,
            @idEmpresa,@idTipoFolDin,@Importe,@ImporteEnt,@ImporteComp,@FolioDinPadre,@idLiquidacion,@NumGuiaId);
                      
            SET @valor = 0;
            SET @mensaje = 'Folio Agregado con Exito';
            SET @id = @@IDENTITY;

            if @FolioDinPadre = 0
            begin
                update FoliosDinero set FolioDinPadre = @id where FolioDin = @id;       
            end;
            
        END;
    ELSE
        BEGIN
           UPDATE FoliosDinero SET Estatus = @Estatus,
            ImporteEnt = ImporteEnt + @ImporteEnt, 
            ImporteComp = ImporteComp + @ImporteComp,
            idLiquidacion = @idLiquidacion 
            WHERE FolioDin = @FolioDin;
            SET @valor = 0;
            SET @mensaje = 'Folio actualizado con Exito';
            SET @id = @FolioDin;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true ))
                    {
                        comand.Parameters.Add(new SqlParameter("@FolioDin", folio.FolioDin));
                        comand.Parameters.Add(new SqlParameter("@Fecha", folio.Fecha));
                        comand.Parameters.Add(new SqlParameter("@idEmpleadoEnt", folio.idEmpleadoEnt));
                        comand.Parameters.Add(new SqlParameter("@idEmpleadoRec", folio.idEmpleadoRec));
                        comand.Parameters.Add(new SqlParameter("@Estatus", folio.Estatus));

                        comand.Parameters.Add(new SqlParameter("@Concepto", folio.Concepto));
                        comand.Parameters.Add(new SqlParameter("@idEmpresa", folio.idEmpresa));
                        comand.Parameters.Add(new SqlParameter("@idTipoFolDin", folio.idTipoFolDin));
                        comand.Parameters.Add(new SqlParameter("@Importe", folio.Importe));
                        comand.Parameters.Add(new SqlParameter("@ImporteEnt", folio.ImporteEnt));
                        comand.Parameters.Add(new SqlParameter("@ImporteComp", folio.ImporteComp));

                        comand.Parameters.Add(new SqlParameter("@FolioDinPadre", folio.FolioDinPadre));
                        comand.Parameters.Add(new SqlParameter("@idLiquidacion", folio.idLiquidacion));
                        comand.Parameters.Add(new SqlParameter("@NumGuiaId", folio.NumGuiaId));
                        

                        //var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        //var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        //var spId = comand.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            folio.FolioDin = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
