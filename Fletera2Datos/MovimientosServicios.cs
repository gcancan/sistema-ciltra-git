﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
namespace Fletera2Datos
{
    public class MovimientosServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getCabMovimientosxFilter(int NoMovimiento = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (NoMovimiento > 0)
                    {
                        filtro = " where c.NoMovimiento = " + NoMovimiento + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
    SELECT  c.NoMovimiento ,
        c.IdMovimiento ,
        c.NoSerie ,
        c.TipoMovimiento ,
        c.CCODIGOC01_CLI ,
        c.FechaMovimiento ,
        c.SubTotalGrav ,
        c.SubTotalExento ,
        c.Retencion ,
        c.Iva ,
        c.Estatus ,
        c.Descuento ,
        c.CCODIGOA01 ,
        c.Usuario ,
        c.IdEmpresa ,
        c.UserAutoriza ,
        c.CIDDOCUM02 ,
        c.FechaVencimiento ,
        c.FechaEntrega ,
        c.UsuarioCrea ,
        c.FechaCrea ,
        c.UsuarioModifica ,
        c.FechaModifica ,
        c.UsuarioCancela ,
        c.FechaCancela ,
        c.CIDMONEDA ,
        c.CTIPOCAM01 ,
        c.Vehiculo ,
        c.Chofer ,
        c.CMETODOPAG ,
        c.CNUMCTAPAG ,
        c.CREFEREN01 ,
        c.COBSERVA01 ,
        c.TimbraFactura ,
        c.EsExterno ,
        c.ReqAutDcto ,
        c.Kilometraje ,
        c.CantTotal
    FROM    dbo.CabMovimientos c
    {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        comand.Parameters.Add(new SqlParameter("@NoMovimiento", NoMovimiento));


                        connection.Open();
                        List<CabMovimientos> lista = new List<CabMovimientos>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CabMovimientos folio = new CabMovimientos
                                {
                                    NoMovimiento = (int)sqlReader["NoMovimiento"],
                                    IdMovimiento = (int)sqlReader["IdMovimiento"],
                                    NoSerie = (string)sqlReader["NoSerie"],
                                    TipoMovimiento = (string)sqlReader["TipoMovimiento"],
                                    CCODIGOC01_CLI = (string)sqlReader["CCODIGOC01_CLI"],
                                    FechaMovimiento = (DateTime)sqlReader["FechaMovimiento"],
                                    SubTotalGrav = (decimal)sqlReader["SubTotalGrav"],
                                    SubTotalExento = (decimal)sqlReader["SubTotalExento"],
                                    Retencion = (decimal)sqlReader["Retencion"],
                                    Iva = (decimal)sqlReader["Iva"],
                                    Estatus = (string)sqlReader["Estatus"],
                                    Descuento = (decimal)sqlReader["Descuento"],
                                    CCODIGOA01 = (string)sqlReader["CCODIGOA01"],
                                    Usuario = (string)sqlReader["Usuario"],
                                    IdEmpresa = (int)sqlReader["IdEmpresa"],
                                    UserAutoriza = (string)sqlReader["UserAutoriza"],
                                    CIDDOCUM02 = (int)sqlReader["CIDDOCUM02"],
                                    FechaVencimiento = (DateTime)sqlReader["FechaVencimiento"],
                                    FechaEntrega = (DateTime)sqlReader["FechaEntrega"],
                                    UsuarioCrea = (string)sqlReader["UsuarioCrea"],
                                    FechaCrea = (DateTime)sqlReader["FechaCrea"],
                                    UsuarioModifica = (string)sqlReader["UsuarioModifica"],
                                    FechaModifica = (DateTime)sqlReader["FechaModifica"],
                                    UsuarioCancela = (string)sqlReader["UsuarioCancela"],
                                    FechaCancela = (DateTime)sqlReader["FechaCancela"],
                                    CIDMONEDA = (int)sqlReader["CIDMONEDA"],
                                    CTIPOCAM01 = (int)sqlReader["CTIPOCAM01"],
                                    Vehiculo = (string)sqlReader["Vehiculo"],
                                    Chofer = (string)sqlReader["Chofer"],
                                    CMETODOPAG = (string)sqlReader["CMETODOPAG"],
                                    CNUMCTAPAG = (string)sqlReader["CNUMCTAPAG"],
                                    CREFEREN01 = (string)sqlReader["CREFEREN01"],
                                    COBSERVA01 = (string)sqlReader["COBSERVA01"],
                                    TimbraFactura = (bool)sqlReader["TimbraFactura"],
                                    EsExterno = (bool)sqlReader["EsExterno"],
                                    ReqAutDcto = (bool)sqlReader["ReqAutDcto"],
                                    Kilometraje = (decimal)sqlReader["Kilometraje"],
                                    CantTotal = (decimal)sqlReader["CantTotal"]

                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
        public OperationResult getDetMovimientosxFilter(int NoMovimiento = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (NoMovimiento > 0)
                    {
                        filtro = " where c.NoMovimiento = " + NoMovimiento + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
    SELECT  c.NoMovimiento ,
        c.CIDUNIDA01 ,
        c.CCODIGOP01_PRO ,
        c.Consecutivo ,
        c.IdMovimiento ,
        c.NoSerie ,
        c.TipoMovimiento ,
        c.Cantidad ,
        c.Precio ,
        c.PorcIVA ,
        c.Costo ,
        c.PorcDescto ,
        c.ImpDescto ,
        c.CIDALMACEN ,
        c.Existencia ,
        c.CIDUNIDA01BASE ,
        c.CantidadBASE ,
        c.tFactorUnidad ,
        c.tNoEquivalente ,
        c.EsListaPrecios ,
        c.CIDUNIDA02 ,
        c.CantidadEQUI ,
        c.COBSERVA01
    FROM    dbo.DetMovimientos c
    {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        comand.Parameters.Add(new SqlParameter("@NoMovimiento", NoMovimiento));


                        connection.Open();
                        List<DetMovimientos> lista = new List<DetMovimientos>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetMovimientos folio = new DetMovimientos
                                {
                                    NoMovimiento = (int)sqlReader["NoMovimiento"],
                                    CIDUNIDA01 = (int)sqlReader["CIDUNIDA01"],
                                    CCODIGOP01_PRO = (string)sqlReader["CCODIGOP01_PRO"],
                                    Consecutivo = (int)sqlReader["Consecutivo"],
                                    IdMovimiento = (int)sqlReader["IdMovimiento"],
                                    NoSerie = (string)sqlReader["NoSerie"],
                                    TipoMovimiento = (string)sqlReader["TipoMovimiento"],
                                    Cantidad = (decimal)sqlReader["Cantidad"],
                                    Precio = (decimal)sqlReader["Precio"],
                                    PorcIVA = (decimal)sqlReader["PorcIVA"],
                                    Costo = (decimal)sqlReader["Costo"],
                                    PorcDescto = (decimal)sqlReader["PorcDescto"],
                                    ImpDescto = (decimal)sqlReader["ImpDescto"],
                                    CIDALMACEN = (int)sqlReader["CIDALMACEN"],
                                    Existencia = (decimal)sqlReader["Existencia"],
                                    CIDUNIDA01BASE = (int)sqlReader["CIDUNIDA01BASE"],
                                    CantidadBASE = (decimal)sqlReader["CantidadBASE"],
                                    tFactorUnidad = (decimal)sqlReader["tFactorUnidad"],
                                    tNoEquivalente = (bool)sqlReader["tNoEquivalente"],
                                    EsListaPrecios = (bool)sqlReader["EsListaPrecios"],
                                    CIDUNIDA02 = (int)sqlReader["CIDUNIDA02"],
                                    CantidadEQUI = (decimal)sqlReader["CantidadEQUI"],
                                    COBSERVA01 = (string)sqlReader["COBSERVA01"],


                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaMovimientos(ref CabMovimientos cabmov, ref List<DetMovimientos> listdetmov)
        {
            try
            {
                //int contOT = 0;
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    connection.Open();
                    using (SqlTransaction trans = connection.BeginTransaction("save"))
                    {
                        using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsqlCab, true))
                        {
                            //SE CREA EL Cabecero
                            comand.Transaction = trans;
                            comand.Parameters.Add(new SqlParameter("NoMovimiento", cabmov.NoMovimiento));
                            comand.Parameters.Add(new SqlParameter("IdMovimiento", cabmov.IdMovimiento));
                            comand.Parameters.Add(new SqlParameter("NoSerie", cabmov.NoSerie));
                            comand.Parameters.Add(new SqlParameter("TipoMovimiento", cabmov.TipoMovimiento));
                            comand.Parameters.Add(new SqlParameter("CCODIGOC01_CLI", cabmov.CCODIGOC01_CLI));
                            comand.Parameters.Add(new SqlParameter("FechaMovimiento", cabmov.FechaMovimiento));
                            comand.Parameters.Add(new SqlParameter("SubTotalGrav", cabmov.SubTotalGrav));
                            comand.Parameters.Add(new SqlParameter("SubTotalExento", cabmov.SubTotalExento));
                            comand.Parameters.Add(new SqlParameter("Retencion", cabmov.Retencion));
                            comand.Parameters.Add(new SqlParameter("Iva", cabmov.Iva));
                            comand.Parameters.Add(new SqlParameter("Estatus", cabmov.Estatus));
                            comand.Parameters.Add(new SqlParameter("Descuento", cabmov.Descuento));
                            comand.Parameters.Add(new SqlParameter("CCODIGOA01", cabmov.CCODIGOA01));
                            comand.Parameters.Add(new SqlParameter("Usuario", cabmov.Usuario));
                            comand.Parameters.Add(new SqlParameter("IdEmpresa", cabmov.IdEmpresa));
                            comand.Parameters.Add(new SqlParameter("UserAutoriza", cabmov.UserAutoriza));
                            comand.Parameters.Add(new SqlParameter("CIDDOCUM02", cabmov.CIDDOCUM02));
                            comand.Parameters.Add(new SqlParameter("FechaVencimiento", cabmov.FechaVencimiento));
                            comand.Parameters.Add(new SqlParameter("FechaEntrega", cabmov.FechaEntrega));
                            comand.Parameters.Add(new SqlParameter("UsuarioCrea", cabmov.UsuarioCrea));
                            comand.Parameters.Add(new SqlParameter("FechaCrea", cabmov.FechaCrea));
                            comand.Parameters.Add(new SqlParameter("UsuarioModifica", cabmov.UsuarioModifica));
                            comand.Parameters.Add(new SqlParameter("FechaModifica", cabmov.FechaModifica));
                            comand.Parameters.Add(new SqlParameter("UsuarioCancela", cabmov.UsuarioCancela));
                            comand.Parameters.Add(new SqlParameter("FechaCancela", cabmov.FechaCancela));
                            comand.Parameters.Add(new SqlParameter("CIDMONEDA", cabmov.CIDMONEDA));
                            comand.Parameters.Add(new SqlParameter("CTIPOCAM01", cabmov.CTIPOCAM01));
                            comand.Parameters.Add(new SqlParameter("Vehiculo", cabmov.Vehiculo));
                            comand.Parameters.Add(new SqlParameter("Chofer", cabmov.Chofer));
                            comand.Parameters.Add(new SqlParameter("CMETODOPAG", cabmov.CMETODOPAG));
                            comand.Parameters.Add(new SqlParameter("CNUMCTAPAG", cabmov.CNUMCTAPAG));
                            comand.Parameters.Add(new SqlParameter("CREFEREN01", cabmov.CREFEREN01));
                            comand.Parameters.Add(new SqlParameter("COBSERVA01", cabmov.COBSERVA01));
                            comand.Parameters.Add(new SqlParameter("TimbraFactura", cabmov.TimbraFactura));
                            comand.Parameters.Add(new SqlParameter("EsExterno", cabmov.EsExterno));
                            comand.Parameters.Add(new SqlParameter("ReqAutDcto", cabmov.ReqAutDcto));
                            comand.Parameters.Add(new SqlParameter("Kilometraje", cabmov.Kilometraje));
                            comand.Parameters.Add(new SqlParameter("CantTotal", cabmov.CantTotal));

                            comand.ExecuteNonQuery();
                            if ((int)comand.Parameters["@valor"].Value == 0)
                            {
                                cabmov.NoMovimiento = (int)comand.Parameters["@id"].Value;

                                foreach (var itemdet in listdetmov)
                                {
                                    //SE CREAN LOS CABECEROS
                                    comand.CommandText = strsqlDet;
                                    comand.Parameters.Clear();
                                    foreach (var param in CrearSqlCommand.getPatametros(true))
                                    {
                                        comand.Parameters.Add(param);

                                    }
                                    itemdet.NoMovimiento = cabmov.NoMovimiento;
                                    comand.Parameters.Add(new SqlParameter("NoMovimiento", itemdet.NoMovimiento));
                                    comand.Parameters.Add(new SqlParameter("CIDUNIDA01", itemdet.CIDUNIDA01));
                                    comand.Parameters.Add(new SqlParameter("CCODIGOP01_PRO", itemdet.CCODIGOP01_PRO));
                                    comand.Parameters.Add(new SqlParameter("Consecutivo", itemdet.Consecutivo));
                                    comand.Parameters.Add(new SqlParameter("IdMovimiento", itemdet.IdMovimiento));
                                    comand.Parameters.Add(new SqlParameter("NoSerie", itemdet.NoSerie));
                                    comand.Parameters.Add(new SqlParameter("TipoMovimiento", itemdet.TipoMovimiento));
                                    comand.Parameters.Add(new SqlParameter("Cantidad", itemdet.Cantidad));
                                    comand.Parameters.Add(new SqlParameter("Precio", itemdet.Precio));
                                    comand.Parameters.Add(new SqlParameter("PorcIVA", itemdet.PorcIVA));
                                    comand.Parameters.Add(new SqlParameter("Costo", itemdet.Costo));
                                    comand.Parameters.Add(new SqlParameter("PorcDescto", itemdet.PorcDescto));
                                    comand.Parameters.Add(new SqlParameter("ImpDescto", itemdet.ImpDescto));
                                    comand.Parameters.Add(new SqlParameter("CIDALMACEN", itemdet.CIDALMACEN));
                                    comand.Parameters.Add(new SqlParameter("Existencia", itemdet.Existencia));
                                    comand.Parameters.Add(new SqlParameter("CIDUNIDA01BASE", itemdet.CIDUNIDA01BASE));
                                    comand.Parameters.Add(new SqlParameter("CantidadBASE", itemdet.CantidadBASE));
                                    comand.Parameters.Add(new SqlParameter("tFactorUnidad", itemdet.tFactorUnidad));
                                    comand.Parameters.Add(new SqlParameter("tNoEquivalente", itemdet.tNoEquivalente));
                                    comand.Parameters.Add(new SqlParameter("EsListaPrecios", itemdet.EsListaPrecios));
                                    comand.Parameters.Add(new SqlParameter("CIDUNIDA02", itemdet.CIDUNIDA02));
                                    comand.Parameters.Add(new SqlParameter("CantidadEQUI", itemdet.CantidadEQUI));
                                    comand.Parameters.Add(new SqlParameter("COBSERVA01", itemdet.COBSERVA01));

                                    

                                    comand.ExecuteNonQuery();
                                    if ((int)comand.Parameters["@valor"].Value != 0)
                                    {
                                        trans.Rollback("save");
                                        return new OperationResult(comand);
                                    }

                                    //if ((int)comand.Parameters["@valor"].Value == 0)
                                    //{
                                    //    itemdet.idOrdenSer = (int)comand.Parameters["@id"].Value;

                                    //    //SE CREAN LOS DETALLES
                                    //    foreach (var itemdet in itemdet.listaDet)
                                    //    {
                                    //        comand.CommandText = strsqlDet;
                                    //        comand.Parameters.Clear();
                                    //        foreach (var param in CrearSqlCommand.getPatametros(true))
                                    //        {
                                    //            comand.Parameters.Add(param);

                                    //        }
                                    //        itemdet.idOrdenTrabajo = 0;
                                    //        comand.Parameters.Add(new SqlParameter("idOrdenSer", itemdet.idOrdenSer));
                                    //        comand.Parameters.Add(new SqlParameter("idOrdenActividad", itemdet.idOrdenActividad));
                                    //        comand.Parameters.Add(new SqlParameter("idServicio", itemdet.idServicio));
                                    //        comand.Parameters.Add(new SqlParameter("idActividad", itemdet.idActividad));
                                    //        comand.Parameters.Add(new SqlParameter("DuracionActHr", itemdet.DuracionActHr));
                                    //        comand.Parameters.Add(new SqlParameter("NoPersonal", itemdet.NoPersonal));
                                    //        comand.Parameters.Add(new SqlParameter("id_proveedor", itemdet.id_proveedor));
                                    //        comand.Parameters.Add(new SqlParameter("Estatus", itemdet.Estatus));
                                    //        comand.Parameters.Add(new SqlParameter("CostoManoObra", itemdet.CostoManoObra));
                                    //        comand.Parameters.Add(new SqlParameter("CostoProductos", itemdet.CostoProductos));
                                    //        comand.Parameters.Add(new SqlParameter("idOrdenTrabajo", itemdet.idOrdenTrabajo));
                                    //        comand.Parameters.Add(new SqlParameter("idDivision", itemdet.idDivision));
                                    //        comand.Parameters.Add(new SqlParameter("idLlanta", itemdet.idLlanta));
                                    //        comand.Parameters.Add(new SqlParameter("PosLlanta", itemdet.PosLlanta));
                                    //        comand.Parameters.Add(new SqlParameter("NotaDiagnostico", itemdet.NotaDiagnostico));
                                    //        comand.Parameters.Add(new SqlParameter("FechaAsig", itemdet.FechaAsig));
                                    //        comand.Parameters.Add(new SqlParameter("UsuarioAsig", itemdet.UsuarioAsig));
                                    //        comand.Parameters.Add(new SqlParameter("FechaTerminado", itemdet.FechaTerminado));
                                    //        comand.Parameters.Add(new SqlParameter("UsuarioTerminado", itemdet.UsuarioTerminado));
                                    //        comand.Parameters.Add(new SqlParameter("CIDPRODUCTO_SERV", itemdet.CIDPRODUCTO_SERV));
                                    //        comand.Parameters.Add(new SqlParameter("Cantidad_CIDPRODUCTO_SERV", itemdet.Cantidad_CIDPRODUCTO_SERV));
                                    //        comand.Parameters.Add(new SqlParameter("Precio_CIDPRODUCTO_SERV", itemdet.Precio_CIDPRODUCTO_SERV));

                                    //        comand.ExecuteNonQuery();
                                    //        //if ((int)comand.Parameters["@valor"].Value == 0)
                                    //        //{
                                    //        //    itemdet.idOrdenTrabajo = (int)comand.Parameters["@id"].Value;
                                    //        //    comand.CommandText = strsqlDetRec;
                                    //        //    comand.Parameters.Clear();
                                    //        //    foreach (var param in CrearSqlCommand.getPatametros(true))
                                    //        //    {
                                    //        //        comand.Parameters.Add(param);

                                    //        //    }
                                    //        //    itemdet.DetRec.Inserta = true;
                                    //        //    comand.Parameters.Add(new SqlParameter("idOrdenTrabajo", itemdet.idOrdenTrabajo));
                                    //        //    comand.Parameters.Add(new SqlParameter("idOrdenSer", itemdet.idOrdenSer));
                                    //        //    comand.Parameters.Add(new SqlParameter("idFamilia", itemdet.DetRec.idFamilia));
                                    //        //    comand.Parameters.Add(new SqlParameter("NotaRecepcion", itemdet.DetRec.NotaRecepcion));
                                    //        //    comand.Parameters.Add(new SqlParameter("isCancelado", itemdet.DetRec.isCancelado));
                                    //        //    comand.Parameters.Add(new SqlParameter("motivoCancelacion", itemdet.DetRec.motivoCancelacion));
                                    //        //    comand.Parameters.Add(new SqlParameter("idDivision", itemdet.DetRec.idDivision));
                                    //        //    comand.Parameters.Add(new SqlParameter("idPersonalResp", itemdet.DetRec.idPersonalResp));
                                    //        //    comand.Parameters.Add(new SqlParameter("idPersonalAyu1", itemdet.DetRec.idPersonalAyu1));
                                    //        //    comand.Parameters.Add(new SqlParameter("idPersonalAyu2", itemdet.DetRec.idPersonalAyu2));
                                    //        //    comand.Parameters.Add(new SqlParameter("NotaRecepcionA", itemdet.DetRec.NotaRecepcionA));
                                    //        //    //comand.Parameters.Add(new SqlParameter("Inserta", itemdet.DetRec.Inserta));
                                    //        //    comand.Parameters.Add(new SqlParameter("Inserta", itemdet.DetRec.Inserta));

                                    //        //    comand.ExecuteNonQuery();
                                    //        //    if ((int)comand.Parameters["@valor"].Value != 0)
                                    //        //    {
                                    //        //        trans.Rollback("save");
                                    //        //        return new OperationResult(comand);
                                    //        //    }
                                    //        //}
                                    //        //else
                                    //        //{
                                    //        //    trans.Rollback("save");
                                    //        //    return new OperationResult(comand);

                                    //        //}
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    trans.Rollback("save");
                                    //    return new OperationResult(comand);

                                    //}
                                }
                            }
                            else
                            {
                                trans.Rollback("save");
                                return new OperationResult(comand);
                            }
                            trans.Commit();
                            return new OperationResult(comand);
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        #region Sqls
        private string strsqlCab = @"
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @NoMovimiento = 0
        BEGIN
        set @id = (select ISNULL(MAX(NoMovimiento) + 1, 1) from CabMovimientos)
    INSERT  INTO CabMovimientos
        ( NoMovimiento ,
          IdMovimiento ,
          NoSerie ,
          TipoMovimiento ,
          CCODIGOC01_CLI ,
          FechaMovimiento ,
          SubTotalGrav ,
          SubTotalExento ,
          Retencion ,
          Iva ,
          Estatus ,
          Descuento ,
          CCODIGOA01 ,
          Usuario ,
          IdEmpresa ,
          UserAutoriza ,
          CIDDOCUM02 ,
          FechaVencimiento ,
          FechaEntrega ,
          UsuarioCrea ,
          FechaCrea ,
          UsuarioModifica ,
          FechaModifica ,
          UsuarioCancela ,
          FechaCancela ,
          CIDMONEDA ,
          CTIPOCAM01 ,
          Vehiculo ,
          Chofer ,
          CMETODOPAG ,
          CNUMCTAPAG ,
          CREFEREN01 ,
          COBSERVA01 ,
          TimbraFactura ,
          EsExterno ,
          ReqAutDcto ,
          Kilometraje ,
          CantTotal)
    VALUES  ( @id ,
          @IdMovimiento ,
          @NoSerie ,
          @TipoMovimiento ,
          @CCODIGOC01_CLI ,
          @FechaMovimiento ,
          @SubTotalGrav ,
          @SubTotalExento ,
          @Retencion ,
          @Iva ,
          @Estatus ,
          @Descuento ,
          @CCODIGOA01 ,
          @Usuario ,
          @IdEmpresa ,
          @UserAutoriza ,
          @CIDDOCUM02 ,
          @FechaVencimiento ,
          @FechaEntrega ,
          @UsuarioCrea ,
          @FechaCrea ,
          @UsuarioModifica ,
          @FechaModifica ,
          @UsuarioCancela ,
          @FechaCancela ,
          @CIDMONEDA ,
          @CTIPOCAM01 ,
          @Vehiculo ,
          @Chofer ,
          @CMETODOPAG ,
          @CNUMCTAPAG ,
          @CREFEREN01 ,
          @COBSERVA01 ,
          @TimbraFactura ,
          @EsExterno ,
          @ReqAutDcto ,
          @Kilometraje ,
          @CantTotal);
            SET @valor = 0;
            SET @mensaje = 'Movimiento Agregado con Exito';
            --SET @id = @@IDENTITY;
            --SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
    UPDATE  CabMovimientos
    SET     IdMovimiento = @IdMovimiento ,
        NoSerie = @NoSerie ,
        TipoMovimiento = @TipoMovimiento ,
        CCODIGOC01_CLI = @CCODIGOC01_CLI ,
        FechaMovimiento = @FechaMovimiento ,
        SubTotalGrav = @SubTotalGrav ,
        SubTotalExento = @SubTotalExento ,
        Retencion = @Retencion ,
        Iva = @Iva ,
        Estatus = @Estatus ,
        Descuento = @Descuento ,
        CCODIGOA01 = @CCODIGOA01 ,
        Usuario = @Usuario ,
        IdEmpresa = @IdEmpresa ,
        UserAutoriza = @UserAutoriza ,
        CIDDOCUM02 = @CIDDOCUM02 ,
        FechaVencimiento = @FechaVencimiento ,
        FechaEntrega = @FechaEntrega ,
        UsuarioCrea = @UsuarioCrea ,
        FechaCrea = @FechaCrea ,
        UsuarioModifica = @UsuarioModifica ,
        FechaModifica = @FechaModifica ,
        UsuarioCancela = @UsuarioCancela ,
        FechaCancela = @FechaCancela ,
        CIDMONEDA = @CIDMONEDA ,
        CTIPOCAM01 = @CTIPOCAM01 ,
        Vehiculo = @Vehiculo ,
        Chofer = @Chofer ,
        CMETODOPAG = @CMETODOPAG ,
        CNUMCTAPAG = @CNUMCTAPAG ,
        CREFEREN01 = @CREFEREN01 ,
        COBSERVA01 = @COBSERVA01 ,
        TimbraFactura = @TimbraFactura ,
        EsExterno = @EsExterno ,
        ReqAutDcto = @ReqAutDcto ,
        Kilometraje = @Kilometraje ,
        CantTotal = @CantTotal
    WHERE   NoMovimiento = @NoMovimiento;

            SET @valor = 0;
            SET @mensaje = 'Movimiento actualizado con Exito';
            SET @id = @NoMovimiento;
        END;
END TRY
BEGIN CATCH
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
        
        private string strsqlDet = @"
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @NoMovimiento <> 0
        BEGIN
        --set @id = (select ISNULL(MAX(idOrdenTrabajo) + 1, 1) from DetOrdenServicio)
     INSERT  INTO DetMovimientos
        ( NoMovimiento ,
          CIDUNIDA01 ,
          CCODIGOP01_PRO ,
          Consecutivo ,
          IdMovimiento ,
          NoSerie ,
          TipoMovimiento ,
          Cantidad ,
          Precio ,
          PorcIVA ,
          Costo ,
          PorcDescto ,
          ImpDescto ,
          CIDALMACEN ,
          Existencia ,
          CIDUNIDA01BASE ,
          CantidadBASE ,
          tFactorUnidad ,
          tNoEquivalente ,
          EsListaPrecios ,
          CIDUNIDA02 ,
          CantidadEQUI ,
          COBSERVA01)
    VALUES  ( @NoMovimiento ,
          @CIDUNIDA01 ,
          @CCODIGOP01_PRO ,
          @Consecutivo ,
          @IdMovimiento ,
          @NoSerie ,
          @TipoMovimiento ,
          @Cantidad ,
          @Precio ,
          @PorcIVA ,
          @Costo ,
          @PorcDescto ,
          @ImpDescto ,
          @CIDALMACEN ,
          @Existencia ,
          @CIDUNIDA01BASE ,
          @CantidadBASE ,
          @tFactorUnidad ,
          @tNoEquivalente ,
          @EsListaPrecios ,
          @CIDUNIDA02 ,
          @CantidadEQUI ,
          @COBSERVA01);
                      
            SET @valor = 0;
            SET @mensaje = 'Detalle Movimientos Agregado con Exito';
            SET @id = @NoMovimiento;
            --SET @id = @idUnidadTrans;
        END;
END TRY
BEGIN CATCH
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
        #endregion
    }//
}
