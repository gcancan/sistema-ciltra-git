﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class CabLiquidacionesServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getCabLiquidacionesxFilter(int idLiquidacion = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idLiquidacion > 0)
                    {
                        filtro = " where idLiquidacion = " + idLiquidacion + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT L.idLiquidacion,
        l.Fecha,
        l.idOperador,
        po.NombreCompleto AS NomOperador,
        l.idEmpresa,
        e.RazonSocial,
        l.NumViajes,
        l.ViajesTot,
        l.CombustibleTot,
        l.GastosxCompTot,
        l.AnticiposTot,
        l.ComprobacionGastosTot,
        l.OtrosConceptosTot,
        l.Usuario,
        p.NombreCompleto AS NomUsuario,
        l.Estatus,
        l.Observaciones,
        l.FechaCancela,
        l.UsuarioCancela,
        ISNULL(pc.NombreCompleto,'') AS NomUsuarioCancela,
        isnull(l.FechaIni,getdate()) as FechaIni,
        isnull(l.FechaFin, getdate()) as FechaFin 
        FROM dbo.CabLiquidaciones l
        INNER JOIN dbo.CatEmpresas e ON e.idEmpresa = l.idEmpresa
        INNER JOIN dbo.usuariosSys u ON u.nombreUsuario = l.Usuario
        INNER JOIN dbo.CatPersonal p ON u.idPersonal = p.idPersonal
        LEFT JOIN dbo.usuariosSys uc ON uc.nombreUsuario = l.UsuarioCancela
        left JOIN dbo.CatPersonal pc ON uc.idPersonal = pc.idPersonal
        INNER JOIN dbo.CatPersonal po ON po.idPersonal = l.idOperador
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        comand.Parameters.Add(new SqlParameter("@NumGuiaId", idLiquidacion));


                        connection.Open();
                        List<CabLiquidaciones> lista = new List<CabLiquidaciones>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CabLiquidaciones folio = new CabLiquidaciones
                                {
                                    idLiquidacion = (int)sqlReader["idLiquidacion"],
                                    Fecha = (DateTime)sqlReader["Fecha"],
                                    idOperador = (int)sqlReader["idOperador"],
                                    NomOperador = (string)sqlReader["NomOperador"],
                                    idEmpresa = (int)sqlReader["idEmpresa"],
                                    RazonSocial = (string)sqlReader["RazonSocial"],
                                    NumViajes = (int)sqlReader["NumViajes"],
                                    ViajesTot = (decimal)sqlReader["ViajesTot"],
                                    CombustibleTot = (decimal)sqlReader["CombustibleTot"],
                                    GastosxCompTot = (decimal)sqlReader["GastosxCompTot"],
                                    AnticiposTot = (decimal)sqlReader["AnticiposTot"],
                                    ComprobacionGastosTot = (decimal)sqlReader["ComprobacionGastosTot"],
                                    OtrosConceptosTot = (decimal)sqlReader["OtrosConceptosTot"],
                                    Usuario = (string)sqlReader["Usuario"],

                                    NomUsuario = (string)sqlReader["NomUsuario"],
                                    Estatus = (string)sqlReader["Estatus"],
                                    Observaciones = (string)sqlReader["Observaciones"],
                                    FechaCancela = (DateTime)sqlReader["FechaCancela"],
                                    UsuarioCancela = (string)sqlReader["UsuarioCancela"],
                                    NomUsuarioCancela = (string)sqlReader["NomUsuarioCancela"],
                                    FechaIni = (DateTime)sqlReader["FechaIni"],
                                    FechaFin = (DateTime)sqlReader["FechaFin"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCabLiquidaciones(ref CabLiquidaciones folio)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idLiquidacion = 0
        BEGIN
            insert into CabLiquidaciones
            (Fecha,idOperador,idEmpresa,NumViajes,ViajesTot,CombustibleTot,
            GastosxCompTot,AnticiposTot,ComprobacionGastosTot,OtrosConceptosTot,
            Usuario,Estatus,Observaciones,FechaCancela,UsuarioCancela,FechaIni,FechaFin)
            values
            (@Fecha,@idOperador,@idEmpresa,@NumViajes,@ViajesTot,@CombustibleTot,
            @GastosxCompTot,@AnticiposTot,@ComprobacionGastosTot,@OtrosConceptosTot,
            @Usuario,@Estatus,@Observaciones,@FechaCancela,@UsuarioCancela,@FechaIni,@FechaFin)
                      
            SET @valor = 0;
            SET @mensaje = 'Liquidacion Agregado con Exito';
            SET @id = @@IDENTITY;

       
            
        END;
    ELSE
        BEGIN
           UPDATE CabLiquidaciones SET 
            Fecha= @Fecha,
            idOperador= @idOperador,
            idEmpresa= @idEmpresa,
            NumViajes= @NumViajes,
            ViajesTot= @ViajesTot,
            CombustibleTot= @CombustibleTot,
            GastosxCompTot= @GastosxCompTot,
            AnticiposTot= @AnticiposTot,
            ComprobacionGastosTot= @ComprobacionGastosTot,
            OtrosConceptosTot= @OtrosConceptosTot,
            Usuario= @Usuario,
            Estatus= @Estatus,
            Observaciones= @Observaciones,
            FechaCancela= @FechaCancela,
            UsuarioCancela= @UsuarioCancela
            WHERE idLiquidacion = @idLiquidacion;
            
            SET @valor = 0;
            SET @mensaje = 'Liquidacion actualizado con Exito';
            SET @id = @idLiquidacion;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("@idLiquidacion", folio.idLiquidacion));
                        comand.Parameters.Add(new SqlParameter("@Fecha", folio.Fecha));
                        comand.Parameters.Add(new SqlParameter("@idOperador", folio.idOperador));
                        comand.Parameters.Add(new SqlParameter("@idEmpresa", folio.idEmpresa));
                        comand.Parameters.Add(new SqlParameter("@NumViajes", folio.NumViajes));
                        comand.Parameters.Add(new SqlParameter("@ViajesTot", folio.ViajesTot));
                        comand.Parameters.Add(new SqlParameter("@CombustibleTot", folio.CombustibleTot));
                        comand.Parameters.Add(new SqlParameter("@GastosxCompTot", folio.GastosxCompTot));
                        comand.Parameters.Add(new SqlParameter("@AnticiposTot", folio.AnticiposTot));
                        comand.Parameters.Add(new SqlParameter("@ComprobacionGastosTot", folio.ComprobacionGastosTot));
                        comand.Parameters.Add(new SqlParameter("@OtrosConceptosTot", folio.OtrosConceptosTot));
                        comand.Parameters.Add(new SqlParameter("@Usuario", folio.Usuario));
                        comand.Parameters.Add(new SqlParameter("@Estatus", folio.Estatus));
                        comand.Parameters.Add(new SqlParameter("@Observaciones", folio.Observaciones));
                        comand.Parameters.Add(new SqlParameter("@FechaCancela", folio.FechaCancela));
                        comand.Parameters.Add(new SqlParameter("@UsuarioCancela", folio.UsuarioCancela));

                        comand.Parameters.Add(new SqlParameter("@FechaIni", folio.FechaIni));
                        comand.Parameters.Add(new SqlParameter("@FechaFin", folio.FechaFin));

                        //var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        //var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        //var spId = comand.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            folio.idLiquidacion = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
