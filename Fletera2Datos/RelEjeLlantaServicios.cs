﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;


namespace Fletera2Datos
{
    public class RelEjeLlantaServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getRelEjeLlantaxFilter(int idClasLlan = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idClasLlan > 0)
                    {
                        filtro = " where C.idClasLlan = " + idClasLlan + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
       SELECT  c.idClasLlan ,
        c.EjeNo ,
        c.NoLlantas
        FROM    dbo.RelEjeLlanta c
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<RelEjeLlanta> lista = new List<RelEjeLlanta>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                RelEjeLlanta cat = new RelEjeLlanta
                                {
                                    idClasLlan = (int)sqlReader["idClasLlan"],
                                    EjeNo = (byte)sqlReader["EjeNo"],
                                    NoLlantas = (byte)sqlReader["NoLlantas"]
                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaRelEjeLlanta(ref RelEjeLlanta cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idConcepto = 0
        BEGIN
        INSERT  INTO RelEjeLlanta
        ( idClasLlan, EjeNo, NoLlantas )
VALUES  ( @idClasLlan, @EjeNo, @NoLlantas );
                      
            SET @valor = 0;
            SET @mensaje = 'Relacion de Llanta Agregado con Exito';
            --SET @id = @@IDENTITY;
            SET @id = @idClasLlan;
        END;
    ELSE
        BEGIN
        UPDATE  RelEjeLlanta
SET     EjeNo = @EjeNo ,
        NoLlantas = @NoLlantas
WHERE   idClasLlan = @idClasLlan;
            SET @valor = 0;
            SET @mensaje = 'Relacion de Llanta actualizado con Exito';
            SET @id = @idClasLlan;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idClasLlan", cat.idClasLlan));
                        comand.Parameters.Add(new SqlParameter("EjeNo", cat.EjeNo));
                        comand.Parameters.Add(new SqlParameter("NoLlantas", cat.NoLlantas));


                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idClasLlan = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
