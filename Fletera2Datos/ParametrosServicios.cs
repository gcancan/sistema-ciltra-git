﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class ParametrosServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getParametrosxFiltro(int IdParametro = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (IdParametro > 0)
                    {
                        filtro = " where p.IdParametro = " + IdParametro + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
       SELECT p.IdParametro,
        p.idConceptoEntAlm,
        p.idConceptoSalAlm,
        p.idAlmacenEnt,
        p.idAlmacenSal,
        p.idEmpresaCOMTaller, 
        p.PorcUtilidad 
        FROM dbo.Parametros p
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        connection.Open();
                        List<Parametros> lista = new List<Parametros>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Parametros catalogo = new Parametros
                                {
                                    IdParametro = (int)sqlReader["IdParametro"],
                                    idAlmacenEnt = (string)sqlReader["idAlmacenEnt"],
                                    idAlmacenSal = (string)sqlReader["idAlmacenSal"],
                                    idConceptoEntAlm = (string)sqlReader["idConceptoEntAlm"],
                                    idConceptoSalAlm = (string)sqlReader["idConceptoSalAlm"],
                                    idEmpresaCOMTaller = (int)sqlReader["idEmpresaCOMTaller"],
                                    PorcUtilidad = (decimal)sqlReader["PorcUtilidad"]
                                };
                                lista.Add(catalogo);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
