﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Fletera2Datos
{
    public class CatDivisionServicios
    {
        private string strsql;
        private string filtro;

        public OperationResult getCatDivisionxFilter(int idDivision = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idDivision > 0)
                    {
                        filtro = " where C.idDivision = " + idDivision + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT c.idDivision,
        c.NomDivision
        FROM dbo.CatDivision c
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<CatDivision> lista = new List<CatDivision>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CatDivision cat = new CatDivision
                                {
                                    idDivision = (int)sqlReader["idDivision"],
                                    NomDivision = (string)sqlReader["NomDivision"]

                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCatDivision(ref CatDivision cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idConcepto = 0
        BEGIN
        INSERT  INTO CatDivision
        ( idDivision,NomDivision)
VALUES  ( @idDivision,@NomDivision);
                      
            SET @valor = 0;
            SET @mensaje = 'Tipo de Unidad Agregado con Exito';
            SET @id = @@IDENTITY;
            --SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  CatDivision
SET     NomDivision = @NomDivision ,
WHERE   idDivision = @idDivision;
            SET @valor = 0;
            SET @mensaje = 'Tipo de Unidad actualizado con Exito';
            SET @id = @idDivision;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idDivision", cat.idDivision));
                        comand.Parameters.Add(new SqlParameter("NomDivision", cat.NomDivision));


                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idDivision = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
