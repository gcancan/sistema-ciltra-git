﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class CatUnidadTransServicios
    {
        private string strsql;
        private string filtro;

        public OperationResult geCatUnidadTransxFilter(string idUnidadTrans = "", string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idUnidadTrans != "")
                    {
                        filtro = " where C.idUnidadTrans = '" + idUnidadTrans +  "'" + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
       SELECT c.idUnidadTrans,
        c.descripcionUni,
        c.idTipoUnidad,
        c.idMarca,
        c.idEmpresa,
        c.idSucursal,
        c.idAlmacen,
        c.Estatus,
        isnull(c.UltOT,0) as UltOT,
        isnull(c.rfid,'') as rfid,
        isnull(c.idTipoMotor,0) as idTipoMotor,
        c.capacidadTanque,
        c.estatusUbicacion,
        c.estatusUbicacionAnt,
        t.nomTipoUniTras
FROM dbo.CatUnidadTrans c
INNER JOIN dbo.CatTipoUniTrans t ON t.idTipoUniTras = c.idTipoUnidad
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<CatUnidadTrans> lista = new List<CatUnidadTrans>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CatUnidadTrans cat = new CatUnidadTrans
                                {
                                    idUnidadTrans = (string)sqlReader["idUnidadTrans"],
                                    descripcionUni = (string)sqlReader["descripcionUni"],
                                    idTipoUnidad = (int)sqlReader["idTipoUnidad"],
                                    idMarca = (int)sqlReader["idMarca"],
                                    idEmpresa = (int)sqlReader["idEmpresa"],
                                    idSucursal = (int)sqlReader["idSucursal"],
                                    idAlmacen = (int)sqlReader["idAlmacen"],
                                    Estatus = (string)sqlReader["Estatus"],
                                    UltOT = (int)sqlReader["UltOT"],
                                    rfid = (string)sqlReader["rfid"],
                                    idTipoMotor = (int)sqlReader["idTipoMotor"],
                                    capacidadTanque = (decimal)sqlReader["capacidadTanque"],
                                    estatusUbicacion = (int)sqlReader["estatusUbicacion"],
                                    estatusUbicacionAnt = (int)sqlReader["estatusUbicacionAnt"],
                                    nomTipoUniTras = (string)sqlReader["nomTipoUniTras"]

                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCatUnidadTrans(ref CatUnidadTrans cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idConcepto = 0
        BEGIN
        INSERT  INTO CatUnidadTrans
        ( idUnidadTrans ,
          descripcionUni ,
          idTipoUnidad ,
          idMarca ,
          idEmpresa ,
          idSucursal ,
          idAlmacen ,
          Estatus ,
          UltOT ,
          rfid ,
          idTipoMotor ,
          capacidadTanque ,
          estatusUbicacion ,
          estatusUbicacionAnt
        )
        VALUES  ( @idUnidadTrans ,
          @descripcionUni ,
          @idTipoUnidad ,
          @idMarca ,
          @idEmpresa ,
          @idSucursal ,
          @idAlmacen ,
          @Estatus ,
          @UltOT ,
          @rfid ,
          @idTipoMotor ,
          @capacidadTanque ,
          @estatusUbicacion ,
          @estatusUbicacionAnt
        );
                      
            SET @valor = 0;
            SET @mensaje = 'Unidad Agregado con Exito';
            --SET @id = @@IDENTITY;
            SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  CatUnidadTrans
SET     descripcionUni = @descripcionUni ,
        idTipoUnidad = @idTipoUnidad ,
        idMarca = @idMarca ,
        idEmpresa = @idEmpresa ,
        idSucursal = @idSucursal ,
        idAlmacen = @idAlmacen ,
        Estatus = @Estatus ,
        UltOT = @UltOT ,
        rfid = @rfid ,
        idTipoMotor = @idTipoMotor ,
        capacidadTanque = @capacidadTanque ,
        estatusUbicacion = @estatusUbicacion ,
        estatusUbicacionAnt = @estatusUbicacionAnt
WHERE   idUnidadTrans = @idUnidadTrans;
            SET @valor = 0;
            SET @mensaje = 'Unidad actualizado con Exito';
            SET @id = @idUnidadTrans;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idUnidadTrans", cat.idUnidadTrans));
                        comand.Parameters.Add(new SqlParameter("descripcionUni", cat.descripcionUni));
                        comand.Parameters.Add(new SqlParameter("idTipoUnidad", cat.idTipoUnidad));
                        comand.Parameters.Add(new SqlParameter("idMarca", cat.idMarca));
                        comand.Parameters.Add(new SqlParameter("idEmpresa", cat.idEmpresa));
                        comand.Parameters.Add(new SqlParameter("idSucursal", cat.idSucursal));
                        comand.Parameters.Add(new SqlParameter("idAlmacen", cat.idAlmacen));
                        comand.Parameters.Add(new SqlParameter("Estatus", cat.Estatus));
                        comand.Parameters.Add(new SqlParameter("UltOT", cat.UltOT));
                        comand.Parameters.Add(new SqlParameter("rfid", cat.rfid));
                        comand.Parameters.Add(new SqlParameter("idTipoMotor", cat.idTipoMotor));
                        comand.Parameters.Add(new SqlParameter("capacidadTanque", cat.capacidadTanque));
                        comand.Parameters.Add(new SqlParameter("estatusUbicacion", cat.estatusUbicacion));
                        comand.Parameters.Add(new SqlParameter("estatusUbicacionAnt", cat.estatusUbicacionAnt));


                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idUnidadTrans = (string)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

       
    }//
}
