﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class CatTipoOrdServServicios
    {
        private string strsql;
        private string filtro;

        public OperationResult getCatTipoOrdServxFilter(int idTipOrdServ = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idTipOrdServ > 0)
                    {
                        filtro = " where C.idTipOrdServ = " + idTipOrdServ + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
       SELECT c.idTipOrdServ,
        c.NomTipOrdServ,
        c.NumOrden,
        c.bOrdenTrab
        FROM dbo.CatTipoOrdServ c
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<CatTipoOrdServ> lista = new List<CatTipoOrdServ>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CatTipoOrdServ cat = new CatTipoOrdServ
                                {
                                    idTipOrdServ = (int)sqlReader["idTipOrdServ"],
                                    NomTipOrdServ = (string)sqlReader["NomTipOrdServ"],
                                    NumOrden = (int)sqlReader["NumOrden"],
                                    bOrdenTrab = (bool)sqlReader["bOrdenTrab"]
                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCatTipoOrdServ(ref CatTipoOrdServ cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idConcepto = 0
        BEGIN
        INSERT  INTO CatTipoOrdServ
        ( idTipOrdServ ,
          NomTipOrdServ ,
          NumOrden ,
          bOrdenTrab
        )
VALUES  ( @idTipOrdServ ,
          @NomTipOrdServ ,
          @NumOrden ,
          @bOrdenTrab
        );
            SET @valor = 0;
            SET @mensaje = 'Tipo Orden Servicio Agregado con Exito';
            --SET @id = @@IDENTITY;
            SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  CatTipoOrdServ
SET     NomTipOrdServ = @NomTipOrdServ ,
        NumOrden = @NumOrden ,
        bOrdenTrab = @bOrdenTrab
WHERE   idTipOrdServ = @idTipOrdServ;
            SET @valor = 0;
            SET @mensaje = 'Tipo Orden Servicio actualizado con Exito';
            SET @id = @idUnidadTrans;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idTipOrdServ", cat.idTipOrdServ));
                        comand.Parameters.Add(new SqlParameter("NomTipOrdServ", cat.NomTipOrdServ));
                        comand.Parameters.Add(new SqlParameter("NumOrden", cat.NumOrden));
                        comand.Parameters.Add(new SqlParameter("bOrdenTrab", cat.bOrdenTrab));


                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idTipOrdServ = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
