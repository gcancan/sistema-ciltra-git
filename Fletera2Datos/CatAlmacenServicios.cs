﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Fletera2Datos
{
    public class CatAlmacenServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getCatAlmacenxFilter(int idAlmacen = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idAlmacen > 0)
                    {
                        filtro = " where C.idAlmacen = " + idAlmacen + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
       SELECT  c.idAlmacen ,
        c.NomAlmacen ,
        c.idSucursal ,
        c.CIDALMACEN_COM
      FROM    dbo.CatAlmacen c
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<CatAlmacen> lista = new List<CatAlmacen>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CatAlmacen cat = new CatAlmacen
                                {
                                    idAlmacen = (int)sqlReader["idAlmacen"],
                                    NomAlmacen = (string)sqlReader["NomAlmacen"],
                                    idSucursal = (int)sqlReader["idSucursal"],
                                    CIDALMACEN_COM = (int)sqlReader["CIDALMACEN_COM"]
                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCatAlmacen(ref CatAlmacen cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idConcepto = 0
        BEGIN
        INSERT  INTO CatAlmacen
        ( idAlmacen ,
          NomAlmacen ,
          idSucursal ,
          CIDALMACEN_COM)
VALUES  ( @idAlmacen ,
          @NomAlmacen ,
          @idSucursal ,
          @CIDALMACEN_COM);
                      
            SET @valor = 0;
            SET @mensaje = 'Almacen Agregado con Exito';
            --SET @id = @@IDENTITY;
            SET @id = @idAlmacen;
        END;
    ELSE
        BEGIN
        UPDATE  CatAlmacen
SET     NomAlmacen = @NomAlmacen ,
        idSucursal = @idSucursal ,
        CIDALMACEN_COM = @CIDALMACEN_COM
WHERE   idAlmacen = @idAlmacen;
            SET @valor = 0;
            SET @mensaje = 'Almacen actualizado con Exito';
            SET @id = @idAlmacen;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idAlmacen", cat.idAlmacen));
                        comand.Parameters.Add(new SqlParameter("NomAlmacen", cat.NomAlmacen));
                        comand.Parameters.Add(new SqlParameter("idSucursal", cat.idSucursal));
                        comand.Parameters.Add(new SqlParameter("CIDALMACEN_COM", cat.CIDALMACEN_COM));

                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idAlmacen = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

    }
}
