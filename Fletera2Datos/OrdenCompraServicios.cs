﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class OrdenCompraServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getCabPreOCxFilter(int IdPreOC = 0, string filtroSinWhere = "", string Orderby = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (IdPreOC > 0)
                    {
                        filtro = " where IdPreOC = " + IdPreOC + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (Orderby != "")
                    {
                        filtro = filtro + " ORDER BY " + Orderby;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT c.IdPreOC,
        ISNULL(c.idOC,0) AS idOC,
        c.idOrdenTrabajo,
        isnull(c.idSolOC,0) as idSolOC,
        ISNULL(c.idCotizacionOC,0) AS idCotizacionOC,
        c.FechaPreOc,
        c.UserSolicita,
        c.idAlmacen,
        c.Estatus,
        ISNULL(c.UserAutorizaOC,'') AS UserAutorizaOC,
        ISNULL(c.FecAutorizaOC,GETDATE()) AS FecAutorizaOC,
        ISNULL(c.Observaciones,'') AS Observaciones,
        ISNULL(c.FecModifica,GETDATE()) AS FecModifica,
        ISNULL(c.UserModifica,'') AS UserModifica,
        ISNULL(c.SubTotal,0) AS SubTotal,
        ISNULL(c.IVA,0) AS IVA,
        ISNULL(c.FecCancela,GETDATE()) AS FecCancela,
        ISNULL(c.UserCancela,'') AS UserCancela,
        ISNULL(c.MotivoCancela,'') AS MotivoCancela,
        ISNULL(oc.CIDDOCUMENTO,0) AS CIDDOCUMENTO,
        ISNULL(oc.CSERIEDOCUMENTO,'') AS CSERIEDOCUMENTO,
        ISNULL(oc.CFOLIO,0) AS CFOLIO
        FROM dbo.CabPreOC c
        left JOIN dbo.ordenCompra oc ON c.idOC = oc.idOrdenCompra
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@NumGuiaId", IdPreOC));


                        connection.Open();
                        List<CabPreOC> lista = new List<CabPreOC>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CabPreOC folio = new CabPreOC
                                {
                                    IdPreOC = (int)sqlReader["IdPreOC"],
                                    idOC = (int)sqlReader["idOC"],
                                    idOrdenTrabajo = (int)sqlReader["idOrdenTrabajo"],
                                    idCotizacionOC = (int)sqlReader["idCotizacionOC"],
                                    idSolOC = (int)sqlReader["idSolOC"],
                                    FechaPreOc = (DateTime)sqlReader["FechaPreOc"],
                                    UserSolicita = (string)sqlReader["UserSolicita"],
                                    idAlmacen = (int)sqlReader["idAlmacen"],
                                    Estatus = (string)sqlReader["Estatus"],
                                    UserAutorizaOC = (string)sqlReader["UserAutorizaOC"],
                                    FecAutorizaOC = (DateTime)sqlReader["FecAutorizaOC"],
                                    Observaciones = (string)sqlReader["Observaciones"],
                                    FecModifica = (DateTime)sqlReader["FecModifica"],
                                    UserModifica = (string)sqlReader["UserModifica"],
                                    SubTotal = (decimal)sqlReader["SubTotal"],
                                    IVA = (decimal)sqlReader["IVA"],
                                    FecCancela = (DateTime)sqlReader["FecCancela"],
                                    UserCancela = (string)sqlReader["UserCancela"],
                                    MotivoCancela = (string)sqlReader["MotivoCancela"],
                                    CSERIEDOCUMENTO = (string)sqlReader["CSERIEDOCUMENTO"],
                                    CIDDOCUMENTO = (int)sqlReader["CIDDOCUMENTO"],
                                    CFOLIO = (Double)sqlReader["CFOLIO"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult getDetPreOCxFilter(int IdPreOC = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (IdPreOC > 0)
                    {
                        filtro = " where IdPreOC = " + IdPreOC + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT d.IdPreOC,
        d.idProducto,
        p.CCODIGOPRODUCTO,
        p.CNOMBREPRODUCTO,
        d.CantidadSol,
        ISNULL(d.Precio,0) AS Precio,
        ISNULL(d.CantFaltante,0) AS CantFaltante,
        isnull(idUnidadProd,0) as idUnidadProd,
        isnull(CantSinCosto,0) as CantSinCosto,
        isnull(PorcIVA,0) as PorcIVA,
        isnull(Desc1,0) as Desc1,
        isnull(Desc2,0) as Desc2,
        CONVERT(BIT,0) AS Seleccionado,
        CONVERT(decimal(18,2),0) AS CantidadEnt
        FROM dbo.DetPreOC d
        INNER JOIN dbo.CatProductosCOM p ON d.idProducto = p.CIDPRODUCTO
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@NumGuiaId", IdPreOC));


                        connection.Open();
                        List<DetPreOC> lista = new List<DetPreOC>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetPreOC folio = new DetPreOC
                                {
                                    IdPreOC = (int)sqlReader["IdPreOC"],
                                    idProducto = (int)sqlReader["idProducto"],
                                    CCODIGOPRODUCTO = (string)sqlReader["CCODIGOPRODUCTO"],
                                    CNOMBREPRODUCTO = (string)sqlReader["CNOMBREPRODUCTO"],
                                    idUnidadProd = (int)sqlReader["idUnidadProd"],
                                    CantidadSol = (decimal)sqlReader["CantidadSol"],
                                    CantSinCosto = (decimal)sqlReader["CantSinCosto"],
                                    CantFaltante = (decimal)sqlReader["CantFaltante"],
                                    Precio = (decimal)sqlReader["Precio"],
                                    PorcIVA = (decimal)sqlReader["PorcIVA"],
                                    Desc1 = (decimal)sqlReader["Desc1"],
                                    Desc2 = (decimal)sqlReader["Desc2"],
                                    Seleccionado = (bool)sqlReader["Seleccionado"],
                                    CantidadEnt = (decimal)sqlReader["CantidadEnt"],
                                    Existencia = 0m
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        //Guardar
        public OperationResult GuardaCabPreOC(ref CabPreOC folio)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    
    IF @IdPreOC = 0
        BEGIN
            insert into CabPreOC
            (idOrdenTrabajo,idSolOC,idCotizacionOC,idOC,FechaPreOc,UserSolicita,idAlmacen,Estatus,
            UserAutorizaOC,FecAutorizaOC,Observaciones,FecModifica,UserModifica,SubTotal,IVA,
            FecCancela,UserCancela,MotivoCancela,IdPreSalida)
            values
            (@idOrdenTrabajo,@idSolOC,@idCotizacionOC,@idOC,@FechaPreOc,@UserSolicita,@idAlmacen,
            @Estatus,@UserAutorizaOC,@FecAutorizaOC,@Observaciones,@FecModifica,@UserModifica,
            @SubTotal,@IVA,@FecCancela,@UserCancela,@MotivoCancela,@IdPreSalida)
                      
            SET @valor = 0;
            
            SET @mensaje = 'Pre Orden Compra Agregado con Exito';
            SET @id = @@IDENTITY;
            
        END;
    ELSE
        BEGIN
            UPDATE CabPreOC SET 
            idOrdenTrabajo= @idOrdenTrabajo,
            idSolOC= @idSolOC,
            idCotizacionOC= @idCotizacionOC,
            idOC= @idOC,
            FechaPreOc= @FechaPreOc,
            UserSolicita= @UserSolicita,
            idAlmacen= @idAlmacen,
            Estatus= @Estatus,
            UserAutorizaOC= @UserAutorizaOC,
            FecAutorizaOC= @FecAutorizaOC,
            Observaciones= @Observaciones,
            FecModifica= @FecModifica,
            UserModifica= @UserModifica,
            SubTotal= @SubTotal,
            IVA= @IVA,
            FecCancela= @FecCancela,
            UserCancela= @UserCancela,
            MotivoCancela= @MotivoCancela,
            IdPreSalida= @IdPreSalida
            WHERE IdPreOC= @IdPreOC           

            SET @valor = 0;
            SET @mensaje = 'Pre Orden Compra actualizada con Exito';
            SET @id = @IdPreOC;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("@IdPreOC", folio.IdPreOC));
                        comand.Parameters.Add(new SqlParameter("@idOrdenTrabajo", folio.idOrdenTrabajo));
                        comand.Parameters.Add(new SqlParameter("@idSolOC", folio.idSolOC));
                        comand.Parameters.Add(new SqlParameter("@idCotizacionOC", folio.idCotizacionOC));
                        comand.Parameters.Add(new SqlParameter("@idOC", folio.idOC));
                        comand.Parameters.Add(new SqlParameter("@FechaPreOc", folio.FechaPreOc));
                        comand.Parameters.Add(new SqlParameter("@UserSolicita", folio.UserSolicita));
                        comand.Parameters.Add(new SqlParameter("@idAlmacen", folio.idAlmacen));
                        comand.Parameters.Add(new SqlParameter("@Estatus", folio.Estatus));
                        comand.Parameters.Add(new SqlParameter("@UserAutorizaOC", folio.UserAutorizaOC));
                        comand.Parameters.Add(new SqlParameter("@FecAutorizaOC", folio.FecAutorizaOC));
                        comand.Parameters.Add(new SqlParameter("@Observaciones", folio.Observaciones));
                        comand.Parameters.Add(new SqlParameter("@FecModifica", folio.FecModifica));
                        comand.Parameters.Add(new SqlParameter("@UserModifica", folio.UserModifica));
                        comand.Parameters.Add(new SqlParameter("@SubTotal", folio.SubTotal));
                        comand.Parameters.Add(new SqlParameter("@IVA", folio.IVA));
                        comand.Parameters.Add(new SqlParameter("@FecCancela", folio.FecCancela));
                        comand.Parameters.Add(new SqlParameter("@UserCancela", folio.UserCancela));
                        comand.Parameters.Add(new SqlParameter("@MotivoCancela", folio.MotivoCancela));
                        comand.Parameters.Add(new SqlParameter("@IdPreSalida", folio.IdPreSalida));
                        

                        //var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        //var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        //var spId = comand.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            folio.IdPreSalida = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaDetPreOC(ref DetPreOC folio)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @Inserta = 1
        BEGIN
            insert into DetPreOC
            (IdPreOC,idProducto,idUnidadProd,CantidadSol,CantSinCosto,CantFaltante,
            Precio,PorcIVA,Desc1,Desc2)
            values
            (@IdPreOC,@idProducto,@idUnidadProd,@CantidadSol,@CantSinCosto,
            @CantFaltante,@Precio,@PorcIVA,@Desc1,@Desc2)
                      
            SET @valor = 0;
            SET @mensaje = 'Det Pre Oc Agregado con Exito';
            --SET @id = @@IDENTITY;
            
        END;
    ELSE
        BEGIN
           UPDATE DetPreOC SET 
            idProducto= @idProducto,
            idUnidadProd= @idUnidadProd,
            CantidadSol= @CantidadSol,
            CantSinCosto= @CantSinCosto,
            CantFaltante= @CantFaltante,
            Precio= @Precio,
            PorcIVA= @PorcIVA,
            Desc1= @Desc1,
            Desc2= @Desc2
            WHERE IdPreOC=@IdPreOC
            SET @valor = 0;
            SET @mensaje = 'DET Pre OC actualizado con Exito';
            SET @id = @idLiquidacion;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("@IdPreOC", folio.IdPreOC));
                        comand.Parameters.Add(new SqlParameter("@idProducto", folio.idProducto));
                        comand.Parameters.Add(new SqlParameter("@idUnidadProd", folio.idUnidadProd));
                        comand.Parameters.Add(new SqlParameter("@CantidadSol", folio.CantidadSol));
                        comand.Parameters.Add(new SqlParameter("@CantSinCosto", folio.CantSinCosto));
                        comand.Parameters.Add(new SqlParameter("@CantFaltante", folio.CantFaltante));
                        comand.Parameters.Add(new SqlParameter("@Precio", folio.Precio));
                        comand.Parameters.Add(new SqlParameter("@PorcIVA", folio.PorcIVA));
                        comand.Parameters.Add(new SqlParameter("@Desc1", folio.Desc1));
                        comand.Parameters.Add(new SqlParameter("@Desc2", folio.Desc2));
                        //var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        //var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        //var spId = comand.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            folio.IdPreOC = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
