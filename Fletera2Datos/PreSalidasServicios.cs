﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class PreSalidasServicios
    {
        private string strsql;
        private string filtro;
        //Consultar
        public OperationResult getCabPreSalidaxFilter(int IdPreSalida = 0, string filtroSinWhere = "", string Orderby = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (IdPreSalida > 0)
                    {
                        filtro = " where c.IdPreSalida = " + IdPreSalida + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }
                    if (Orderby != "")
                    {
                        filtro = filtro + " ORDER BY " + Orderby;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
            SELECT c.IdPreSalida,
            c.idOrdenTrabajo,
            c.FecSolicitud,
            c.UserSolicitud,
            ISNULL(pu.NombreCompleto,'') AS NomUserSolicitud,
            c.IdEmpleadoEnc,
            pe.NombreCompleto AS NomEmpleadoEnc,
            c.idAlmacen,
            al.NomAlmacen,
            al.CIDALMACEN_COM,
            c.Estatus,
            c.SubTotal,
            ISNULL(c.FecCancela,GETDATE()) AS FecCancela,
            ISNULL(c.UserCancela,'') AS UserCancela,
            ISNULL(c.MotivoCancela,'') AS MotivoCancela,
            ISNULL(c.FecEntrega,GETDATE()) AS FecEntrega,
            ISNULL(c.UserEntrega,'') AS UserEntrega,
            ISNULL(c.IdPersonalRec,0) AS IdPersonalRec,
            ISNULL(c.EntregaCambio,0) AS EntregaCambio,
            ISNULL(c.idPreOC,0) AS idPreOC,
            ISNULL(c.CIDDOCUMENTO_ENT,0) AS CIDDOCUMENTO_ENT,
            ISNULL(c.CSERIEDOCUMENTO_ENT,'') AS CSERIEDOCUMENTO_ENT,
            ISNULL(c.CFOLIO_ENT,0) AS CFOLIO_ENT,
            ISNULL(c.CIDDOCUMENTO_ENT,0) AS CIDDOCUMENTO_SAL,
            ISNULL(c.CSERIEDOCUMENTO_ENT,'') AS CSERIEDOCUMENTO_SAL,
            ISNULL(c.CFOLIO_ENT,0) AS CFOLIO_SAL,
            DOS.IDORDENSER,
            cabos.idUnidadTrans,
            ISNULL(detros.idPersonalAyu1,0) AS idPersonalAyu1,
			ISNULL(payu1.NombreCompleto,'') AS NomPersonalAyu1,
			ISNULL(detros.idPersonalAyu2,0) AS idPersonalAyu2,
			ISNULL(payu2.NombreCompleto,'') AS NomPersonalAyu2,
            c.EsDevolucion 
            FROM dbo.CabPreSalida c
            INNER JOIN dbo.CatPersonal pe ON c.IdEmpleadoEnc = pe.idPersonal
            INNER JOIN dbo.CatAlmacen al ON al.idAlmacen = c.idAlmacen
			LEFT JOIN dbo.usuariosSys u ON c.UserSolicitud = u.nombreUsuario
			LEFT JOIN dbo.CatPersonal pu ON pu.idPersonal = u.idPersonal
            INNER JOIN dbo.DetOrdenServicio dos ON dos.idOrdenTrabajo = c.idOrdenTrabajo
            INNER JOIN dbo.CabOrdenServicio cabos ON cabos.idOrdenSer = dos.idOrdenSer	    
			INNER JOIN dbo.DetRecepcionOS detros ON detros.idOrdenSer = dos.idOrdenSer AND detros.idOrdenTrabajo = dos.idOrdenTrabajo
			LEFT JOIN dbo.CatPersonal payu1 ON payu1.idPersonal = detros.idPersonalAyu1
			LEFT JOIN dbo.CatPersonal payu2 ON payu2.idPersonal = detros.idPersonalAyu2
            {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@NumGuiaId", IdPreOC));


                        connection.Open();
                        List<CabPreSalida> lista = new List<CabPreSalida>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CabPreSalida folio = new CabPreSalida
                                {
                                    IdPreSalida = (int)sqlReader["IdPreSalida"],
                                    idOrdenTrabajo = (int)sqlReader["idOrdenTrabajo"],
                                    FecSolicitud = (DateTime)sqlReader["FecSolicitud"],
                                    UserSolicitud = (string)sqlReader["UserSolicitud"],
                                    NomUserSolicitud = (string)sqlReader["NomUserSolicitud"],
                                    IdEmpleadoEnc = (int)sqlReader["IdEmpleadoEnc"],
                                    idAlmacen = (int)sqlReader["idAlmacen"],
                                    Estatus = (string)sqlReader["Estatus"],
                                    SubTotal = (decimal)sqlReader["SubTotal"],
                                    FecCancela = (DateTime)sqlReader["FecCancela"],
                                    UserCancela = (string)sqlReader["UserCancela"],
                                    MotivoCancela = (string)sqlReader["MotivoCancela"],
                                    FecEntrega = (DateTime)sqlReader["FecEntrega"],
                                    UserEntrega = (string)sqlReader["UserEntrega"],
                                    IdPersonalRec = (int)sqlReader["IdPersonalRec"],
                                    EntregaCambio = (bool)sqlReader["EntregaCambio"],
                                    idPreOC = (int)sqlReader["idPreOC"],
                                    CSERIEDOCUMENTO_ENT = (string)sqlReader["CSERIEDOCUMENTO_ENT"],
                                    CIDDOCUMENTO_ENT = (int)sqlReader["CIDDOCUMENTO_ENT"],
                                    CFOLIO_ENT = (Double)sqlReader["CFOLIO_ENT"],
                                    CSERIEDOCUMENTO_SAL = (string)sqlReader["CSERIEDOCUMENTO_SAL"],
                                    CIDDOCUMENTO_SAL = (int)sqlReader["CIDDOCUMENTO_SAL"],
                                    CFOLIO_SAL = (Double)sqlReader["CFOLIO_SAL"],
                                    idOrdenSer = (int)sqlReader["idOrdenSer"],
                                    idUnidadTrans = (string)sqlReader["idUnidadTrans"],
                                    EsDevolucion = (bool)sqlReader["EsDevolucion"]
                                    //idPersonalAyu1 = (int)sqlReader["idPersonalAyu1"],
                                    //idPersonalAyu2 = (int)sqlReader["idPersonalAyu2"],
                                    //NomEmpleadoEnc = (string)sqlReader["NomEmpleadoEnc"],
                                    //NomPersonalAyu1 = (string)sqlReader["NomPersonalAyu1"],
                                    //NomPersonalAyu2 = (string)sqlReader["NomPersonalAyu2"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult getDetPreSalidaxFilter(int IdPreSalida = 0, string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (IdPreSalida > 0)
                    {
                        filtro = " where d.IdPreSalida = " + IdPreSalida + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT CONVERT(BIT,0) AS Seleccionado,
        d.IdPreSalida,
        d.idProducto,
        d.idUnidadProd,
        p.CCODIGOPRODUCTO,
        p.CNOMBREPRODUCTO,
        d.Cantidad,
        isnull(d.CantidadEnt,0) as CantidadEnt,
        d.Costo,
        d.Existencia,
        d.CantEntregaCambio
        FROM dbo.detPreSalida d
        INNER JOIN dbo.CatProductosCOM p ON d.idProducto = p.CIDPRODUCTO
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@NumGuiaId", IdPreOC));


                        connection.Open();
                        List<DetPreSalida> lista = new List<DetPreSalida>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetPreSalida folio = new DetPreSalida
                                {
                                    Seleccionado = (bool)sqlReader["Seleccionado"],
                                    IdPreSalida = (int)sqlReader["IdPreSalida"],
                                    idProducto = (int)sqlReader["idProducto"],
                                    CCODIGOPRODUCTO = (string)sqlReader["CCODIGOPRODUCTO"],
                                    CNOMBREPRODUCTO = (string)sqlReader["CNOMBREPRODUCTO"],
                                    idUnidadProd = (int)sqlReader["idUnidadProd"],
                                    Cantidad = (decimal)sqlReader["Cantidad"],
                                    CantidadEnt = (decimal)sqlReader["CantidadEnt"],
                                    Costo = (decimal)sqlReader["Costo"],
                                    Existencia = (decimal)sqlReader["Existencia"],
                                    CantEntregaCambio = (decimal)sqlReader["CantEntregaCambio"]

                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        //Guardar
        public OperationResult GuardaCabPreSalida(ref CabPreSalida folio)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @IdPreSalida = 0
        BEGIN
            insert into CabPreSalida
            (idOrdenTrabajo,FecSolicitud,UserSolicitud,IdEmpleadoEnc,idAlmacen,
            Estatus,SubTotal,FecEntrega,UserEntrega,
            IdPersonalRec,EntregaCambio,idPreOC,CIDDOCUMENTO_ENT,CSERIEDOCUMENTO_ENT,CFOLIO_ENT,
            CIDDOCUMENTO_SAL,CSERIEDOCUMENTO_SAL,CFOLIO_SAL,EsDevolucion)
            values
            (@idOrdenTrabajo,@FecSolicitud,@UserSolicitud,@IdEmpleadoEnc,@idAlmacen,
            @Estatus,@SubTotal,@FecEntrega,
            @UserEntrega,@IdPersonalRec,@EntregaCambio,@idPreOC,@CIDDOCUMENTO_ENT,
            @CSERIEDOCUMENTO_ENT,@CFOLIO_ENT,
            @CIDDOCUMENTO_SAL,@CSERIEDOCUMENTO_SAL,@CFOLIO_SAL,@EsDevolucion)
                      
            SET @valor = 0;
            SET @mensaje = 'Pre Salida Agregado con Exito';
            SET @id = @@IDENTITY;
            
        END;
    ELSE
        BEGIN
            UPDATE CabPreSalida SET 
            idOrdenTrabajo= @idOrdenTrabajo,
            FecSolicitud= @FecSolicitud,
            UserSolicitud= @UserSolicitud,
            IdEmpleadoEnc= @IdEmpleadoEnc,
            idAlmacen= @idAlmacen,
            Estatus= @Estatus,
            SubTotal= @SubTotal,
            FecEntrega= @FecEntrega,
            UserEntrega= @UserEntrega,
            IdPersonalRec= @IdPersonalRec,
            EntregaCambio= @EntregaCambio,
            idPreOC= @idPreOC,
            CIDDOCUMENTO_ENT= @CIDDOCUMENTO_ENT,
            CSERIEDOCUMENTO_ENT= @CSERIEDOCUMENTO_ENT,
            CFOLIO_ENT= @CFOLIO_ENT,
            CIDDOCUMENTO_SAL= @CIDDOCUMENTO_SAL,
            CSERIEDOCUMENTO_SAL= @CSERIEDOCUMENTO_SAL,
            CFOLIO_SAL= @CFOLIO_SAL,
            EsDevolucion = @EsDevolucion 
            FROM dbo.CabPreSalida
            WHERE IdPreSalida= @IdPreSalida    
            SET @valor = 0;
            SET @mensaje = 'Pre Salida actualizado con Exito';
            SET @id = @IdPreOC;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("@IdPreSalida", folio.IdPreSalida));
                        comand.Parameters.Add(new SqlParameter("@idOrdenTrabajo", folio.idOrdenTrabajo));
                        comand.Parameters.Add(new SqlParameter("@FecSolicitud", folio.FecSolicitud));
                        comand.Parameters.Add(new SqlParameter("@UserSolicitud", folio.UserSolicitud));
                        comand.Parameters.Add(new SqlParameter("@IdEmpleadoEnc", folio.IdEmpleadoEnc));
                        comand.Parameters.Add(new SqlParameter("@idAlmacen", folio.idAlmacen));
                        comand.Parameters.Add(new SqlParameter("@Estatus", folio.Estatus));
                        comand.Parameters.Add(new SqlParameter("@SubTotal", folio.SubTotal));
                        //comand.Parameters.Add(new SqlParameter("@FecCancela", folio.FecCancela));
                        //comand.Parameters.Add(new SqlParameter("@UserCancela", folio.UserCancela));
                        //comand.Parameters.Add(new SqlParameter("@MotivoCancela", folio.MotivoCancela));
                        comand.Parameters.Add(new SqlParameter("@FecEntrega", folio.FecEntrega));
                        comand.Parameters.Add(new SqlParameter("@UserEntrega", folio.UserEntrega));
                        comand.Parameters.Add(new SqlParameter("@IdPersonalRec", folio.IdPersonalRec));
                        comand.Parameters.Add(new SqlParameter("@EntregaCambio", folio.EntregaCambio));
                        comand.Parameters.Add(new SqlParameter("@idPreOC", folio.idPreOC));
                        comand.Parameters.Add(new SqlParameter("@CIDDOCUMENTO_ENT", folio.CIDDOCUMENTO_ENT));
                        comand.Parameters.Add(new SqlParameter("@CSERIEDOCUMENTO_ENT", folio.CSERIEDOCUMENTO_ENT));
                        comand.Parameters.Add(new SqlParameter("@CFOLIO_ENT", folio.CFOLIO_ENT));
                        comand.Parameters.Add(new SqlParameter("@CIDDOCUMENTO_SAL", folio.CIDDOCUMENTO_SAL));
                        comand.Parameters.Add(new SqlParameter("@CSERIEDOCUMENTO_SAL", folio.CSERIEDOCUMENTO_SAL));
                        comand.Parameters.Add(new SqlParameter("@CFOLIO_SAL", folio.CFOLIO_SAL));
                        comand.Parameters.Add(new SqlParameter("@EsDevolucion", folio.EsDevolucion));


                        //var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        //var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        //var spId = comand.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            folio.IdPreSalida = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardDetPreSalida(ref DetPreSalida folio)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @Inserta = 1
        BEGIN
            insert into DetPreSalida
            (IdPreSalida,idProducto,idUnidadProd,Cantidad,CantidadEnt,Costo,Existencia)
            values
            (@IdPreSalida,@idProducto,@idUnidadProd,@Cantidad,@CantidadEnt,@Costo,@Existencia)
                      
            SET @valor = 0;
            SET @mensaje = 'Det Pre Salida Agregado con Exito';
            SET @id = @@IDENTITY;
            
        END;
    ELSE
        BEGIN
           UPDATE DetPreSalida SET 
            idProducto= @idProducto,
            idUnidadProd= @idUnidadProd,
            Cantidad= @Cantidad,
            CantidadEnt= @CantidadEnt,
            Costo= @Costo,
            Existencia= @Existencia
            WHERE IdPreSalida= @IdPreSalida and idProducto= @idProducto
            SET @valor = 0;
            SET @mensaje = 'Pre Salida actualizado con Exito';
            SET @id = @IdPreSalida;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("@IdPreSalida", folio.IdPreSalida));
                        comand.Parameters.Add(new SqlParameter("@idProducto", folio.idProducto));
                        comand.Parameters.Add(new SqlParameter("@idUnidadProd", folio.idUnidadProd));
                        comand.Parameters.Add(new SqlParameter("@Cantidad", folio.Cantidad));
                        comand.Parameters.Add(new SqlParameter("@CantidadEnt", folio.CantidadEnt));
                        comand.Parameters.Add(new SqlParameter("@Costo", folio.Costo));
                        comand.Parameters.Add(new SqlParameter("@Existencia", folio.Existencia));
                        comand.Parameters.Add(new SqlParameter("@Inserta", folio.Inserta));
                        //var spMensaje = comand.Parameters.Add(new SqlParameter("@mensaje", SqlDbType.NVarChar, 255) { Direction = ParameterDirection.Output });
                        //var spValor = comand.Parameters.Add(new SqlParameter("@valor", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        //var spId = comand.Parameters.Add(new SqlParameter("@id", SqlDbType.Int) { Direction = ParameterDirection.Output });
                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            folio.IdPreSalida = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult getApartadoxProd(int CIDPRODUCTO, string filtroSinWhere = "", string Orderby = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    filtro = " where D.idProducto = " + CIDPRODUCTO;
                    if (filtroSinWhere != "")
                    {
                        filtro = filtro + " AND " + filtroSinWhere;
                    }
                    
                    if (Orderby != "")
                    {
                        filtro = filtro + " ORDER BY " + Orderby;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
            SELECT d.idProducto,
            SUM(CANTIDAD) AS Apartado FROM dbo.DetPreSalida d
            INNER JOIN dbo.CabPreSalida c ON c.IdPreSalida = d.IdPreSalida
            {0} 
            GROUP BY d.idProducto

    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@NumGuiaId", IdPreOC));


                        connection.Open();
                        List<Apartados> lista = new List<Apartados>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Apartados folio = new Apartados
                                {
                                    cidproducto = (int)sqlReader["idProducto"],
                                    Apartado = (decimal)sqlReader["Apartado"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult getDetSalidasInsumoxFilter(int IdPreSalida = 0, string filtroSinWhere = "", string Orderby = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (IdPreSalida > 0)
                    {
                        filtro = " where d.IdPreSalida = " + IdPreSalida + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }
                    if (Orderby != "")
                    {
                        filtro = filtro + " ORDER BY " + Orderby;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT DETSAL.idProducto, prod.CCODIGOPRODUCTO, PROD.CNOMBREPRODUCTO,
        SUM(detsal.CantidadEnt) AS Cantidad
        FROM DBO.DETPRESALIDA DETSAL
        INNER JOIN DBO.CABPRESALIDA CABSAL ON CABSAL.IDPRESALIDA = DETSAL.IDPRESALIDA
        INNER JOIN dbo.CatProductosCOM PROD ON PROD.CIDPRODUCTO = DETSAL.idProducto
        {0} 
        GROUP BY detsal.idProducto,prod.CCODIGOPRODUCTO, PROD.CNOMBREPRODUCTO
        
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@NumGuiaId", IdPreOC));


                        connection.Open();
                        List<DetPreSalida> lista = new List<DetPreSalida>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                DetPreSalida folio = new DetPreSalida
                                {
                                    idProducto = (int)sqlReader["idProducto"],
                                    CCODIGOPRODUCTO = (string)sqlReader["CCODIGOPRODUCTO"],
                                    CNOMBREPRODUCTO = (string)sqlReader["CNOMBREPRODUCTO"],
                                    Cantidad = (decimal)sqlReader["Cantidad"]

                                    //Seleccionado = (bool)sqlReader["Seleccionado"],
                                    //IdPreSalida = (int)sqlReader["IdPreSalida"],
                                    //idProducto = (int)sqlReader["idProducto"],
                                    //CCODIGOPRODUCTO = (string)sqlReader["CCODIGOPRODUCTO"],
                                    //CNOMBREPRODUCTO = (string)sqlReader["CNOMBREPRODUCTO"],
                                    //idUnidadProd = (int)sqlReader["idUnidadProd"],
                                    //Cantidad = (decimal)sqlReader["Cantidad"],
                                    //CantidadEnt = (decimal)sqlReader["CantidadEnt"],
                                    //Costo = (decimal)sqlReader["Costo"],
                                    //Existencia = (decimal)sqlReader["Existencia"],
                                    //CantEntregaCambio = (decimal)sqlReader["CantEntregaCambio"]

                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaPresalidas(ref CabPreSalida cab, ref List<DetPreSalida> listdet)
        {
            try
            {
                //int contOT = 0;
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    connection.Open();
                    using (SqlTransaction trans = connection.BeginTransaction("save"))
                    {
                        using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsqlCab, true))
                        {
                            //SE CREA EL Cabecero
                            comand.Transaction = trans;
                            comand.Parameters.Add(new SqlParameter("@IdPreSalida", cab.IdPreSalida));
                            comand.Parameters.Add(new SqlParameter("@idOrdenTrabajo", cab.idOrdenTrabajo));
                            comand.Parameters.Add(new SqlParameter("@FecSolicitud", cab.FecSolicitud));
                            comand.Parameters.Add(new SqlParameter("@UserSolicitud", cab.UserSolicitud));
                            comand.Parameters.Add(new SqlParameter("@IdEmpleadoEnc", cab.IdEmpleadoEnc));
                            comand.Parameters.Add(new SqlParameter("@idAlmacen", cab.idAlmacen));
                            comand.Parameters.Add(new SqlParameter("@Estatus", cab.Estatus));
                            comand.Parameters.Add(new SqlParameter("@SubTotal", cab.SubTotal));
                            //comand.Parameters.Add(new SqlParameter("@FecCancela", folio.FecCancela));
                            //comand.Parameters.Add(new SqlParameter("@UserCancela", folio.UserCancela));
                            //comand.Parameters.Add(new SqlParameter("@MotivoCancela", folio.MotivoCancela));
                            comand.Parameters.Add(new SqlParameter("@FecEntrega", cab.FecEntrega));
                            comand.Parameters.Add(new SqlParameter("@UserEntrega", cab.UserEntrega));
                            comand.Parameters.Add(new SqlParameter("@IdPersonalRec", cab.IdPersonalRec));
                            comand.Parameters.Add(new SqlParameter("@EntregaCambio", cab.EntregaCambio));
                            comand.Parameters.Add(new SqlParameter("@idPreOC", cab.idPreOC));
                            comand.Parameters.Add(new SqlParameter("@CIDDOCUMENTO_ENT", cab.CIDDOCUMENTO_ENT));
                            comand.Parameters.Add(new SqlParameter("@CSERIEDOCUMENTO_ENT", cab.CSERIEDOCUMENTO_ENT));
                            comand.Parameters.Add(new SqlParameter("@CFOLIO_ENT", cab.CFOLIO_ENT));
                            comand.Parameters.Add(new SqlParameter("@CIDDOCUMENTO_SAL", cab.CIDDOCUMENTO_SAL));
                            comand.Parameters.Add(new SqlParameter("@CSERIEDOCUMENTO_SAL", cab.CSERIEDOCUMENTO_SAL));
                            comand.Parameters.Add(new SqlParameter("@CFOLIO_SAL", cab.CFOLIO_SAL));
                            comand.Parameters.Add(new SqlParameter("@EsDevolucion", cab.EsDevolucion));

                            comand.ExecuteNonQuery();
                            if ((int)comand.Parameters["@valor"].Value == 0)
                            {
                                cab.IdPreSalida = (int)comand.Parameters["@id"].Value;

                                foreach (var itemdet in listdet)
                                {
                                    //SE CREAN LOS CABECEROS
                                    comand.CommandText = strsqlDet;
                                    comand.Parameters.Clear();
                                    foreach (var param in CrearSqlCommand.getPatametros(true))
                                    {
                                        comand.Parameters.Add(param);

                                    }
                                    itemdet.IdPreSalida = cab.IdPreSalida;
                                    comand.Parameters.Add(new SqlParameter("@IdPreSalida", itemdet.IdPreSalida));
                                    comand.Parameters.Add(new SqlParameter("@idProducto", itemdet.idProducto));
                                    comand.Parameters.Add(new SqlParameter("@idUnidadProd", itemdet.idUnidadProd));
                                    comand.Parameters.Add(new SqlParameter("@Cantidad", itemdet.Cantidad));
                                    comand.Parameters.Add(new SqlParameter("@CantidadEnt", itemdet.CantidadEnt));
                                    comand.Parameters.Add(new SqlParameter("@Costo", itemdet.Costo));
                                    comand.Parameters.Add(new SqlParameter("@Existencia", itemdet.Existencia));
                                    comand.Parameters.Add(new SqlParameter("@Inserta", itemdet.Inserta));

                                    comand.ExecuteNonQuery();
                                    if ((int)comand.Parameters["@valor"].Value != 0)
                                    {
                                        trans.Rollback("save");
                                        return new OperationResult(comand);
                                    }

                                    
                                }
                            }
                            else
                            {
                                trans.Rollback("save");
                                return new OperationResult(comand);
                            }
                            trans.Commit();
                            return new OperationResult(comand);
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        private string strsqlCab = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @IdPreSalida = 0
        BEGIN
            insert into CabPreSalida
            (idOrdenTrabajo,FecSolicitud,UserSolicitud,IdEmpleadoEnc,idAlmacen,
            Estatus,SubTotal,FecEntrega,UserEntrega,
            IdPersonalRec,EntregaCambio,idPreOC,CIDDOCUMENTO_ENT,CSERIEDOCUMENTO_ENT,CFOLIO_ENT,
            CIDDOCUMENTO_SAL,CSERIEDOCUMENTO_SAL,CFOLIO_SAL,EsDevolucion)
            values
            (@idOrdenTrabajo,@FecSolicitud,@UserSolicitud,@IdEmpleadoEnc,@idAlmacen,
            @Estatus,@SubTotal,@FecEntrega,
            @UserEntrega,@IdPersonalRec,@EntregaCambio,@idPreOC,@CIDDOCUMENTO_ENT,
            @CSERIEDOCUMENTO_ENT,@CFOLIO_ENT,
            @CIDDOCUMENTO_SAL,@CSERIEDOCUMENTO_SAL,@CFOLIO_SAL,@EsDevolucion)
                      
            SET @valor = 0;
            SET @mensaje = 'Pre Salida Agregado con Exito';
            SET @id = @@IDENTITY;
            
        END;
    ELSE
        BEGIN
            UPDATE CabPreSalida SET 
            idOrdenTrabajo= @idOrdenTrabajo,
            FecSolicitud= @FecSolicitud,
            UserSolicitud= @UserSolicitud,
            IdEmpleadoEnc= @IdEmpleadoEnc,
            idAlmacen= @idAlmacen,
            Estatus= @Estatus,
            SubTotal= @SubTotal,
            FecEntrega= @FecEntrega,
            UserEntrega= @UserEntrega,
            IdPersonalRec= @IdPersonalRec,
            EntregaCambio= @EntregaCambio,
            idPreOC= @idPreOC,
            CIDDOCUMENTO_ENT= @CIDDOCUMENTO_ENT,
            CSERIEDOCUMENTO_ENT= @CSERIEDOCUMENTO_ENT,
            CFOLIO_ENT= @CFOLIO_ENT,
            CIDDOCUMENTO_SAL= @CIDDOCUMENTO_SAL,
            CSERIEDOCUMENTO_SAL= @CSERIEDOCUMENTO_SAL,
            CFOLIO_SAL= @CFOLIO_SAL,
            EsDevolucion = @EsDevolucion 
            FROM dbo.CabPreSalida
            WHERE IdPreSalida= @IdPreSalida    
            SET @valor = 0;
            SET @mensaje = 'Pre Salida actualizado con Exito';
            SET @id = @IdPreOC;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";

        private string strsqlDet = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @Inserta = 1
        BEGIN
            insert into DetPreSalida
            (IdPreSalida,idProducto,idUnidadProd,Cantidad,CantidadEnt,Costo,Existencia)
            values
            (@IdPreSalida,@idProducto,@idUnidadProd,@Cantidad,@CantidadEnt,@Costo,@Existencia)
                      
            SET @valor = 0;
            SET @mensaje = 'Det Pre Salida Agregado con Exito';
            SET @id = @@IDENTITY;
            
        END;
    ELSE
        BEGIN
           UPDATE DetPreSalida SET 
            idProducto= @idProducto,
            idUnidadProd= @idUnidadProd,
            Cantidad= @Cantidad,
            CantidadEnt= @CantidadEnt,
            Costo= @Costo,
            Existencia= @Existencia
            WHERE IdPreSalida= @IdPreSalida and idProducto= @idProducto
            SET @valor = 0;
            SET @mensaje = 'Pre Salida actualizado con Exito';
            SET @id = @IdPreSalida;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
    }//
}
