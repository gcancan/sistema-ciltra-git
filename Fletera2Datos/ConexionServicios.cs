﻿using Core.Utils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fletera2Datos
{
    public class ConexionServicios
    {
        private SqlDataAdapter da;
        public DataTable LlenaDataTable(string StrSQL)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    da = new SqlDataAdapter(StrSQL, connection);
                    da.Fill(dt);
                    return dt;
                }
            }
            catch (Exception)
            {

                return null;
            }
            finally
            {
                dt.Dispose();
                da.Dispose();
            }
        }
    }
}
