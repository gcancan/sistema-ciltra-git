﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Fletera2Datos
{
    public class CatServiciosServicios
    {
        private string strsql;
        private string filtro;

        public OperationResult getCatServiciosxFilter(int idServicio = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idServicio > 0)
                    {
                        filtro = " where C.idServicio = " + idServicio + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT  c.idServicio,
        c.idTipoServicio,
        c.NomServicio,
        c.Descripcion,
        c.CadaKms,
        c.CadaTiempoDias,
        c.Estatus,
        c.Costo,
        isnull(c.idDivision,0) as idDivision
        FROM dbo.CatServicios c
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<CatServicios> lista = new List<CatServicios>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CatServicios cat = new CatServicios
                                {
                                    idServicio = (int)sqlReader["idServicio"],
                                    idTipoServicio = (int)sqlReader["idTipoServicio"],
                                    NomServicio = (string)sqlReader["NomServicio"],
                                    Descripcion = (string)sqlReader["Descripcion"],
                                    CadaKms = (decimal)sqlReader["CadaKms"],
                                    CadaTiempoDias = (decimal)sqlReader["CadaTiempoDias"],
                                    Estatus = (string)sqlReader["Estatus"],
                                    Costo = (decimal)sqlReader["Costo"],
                                    idDivision = (int)sqlReader["idDivision"]

                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCatServicios(ref CatServicios cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
       INSERT  INTO CatServicios
        ( idServicio ,
          idTipoServicio ,
          NomServicio ,
          Descripcion ,
          CadaKms ,
          CadaTiempoDias ,
          Estatus ,
          Costo ,
          idDivisio
        )
VALUES  ( @idServicio ,
          @idTipoServicio ,
          @NomServicio ,
          @Descripcion ,
          @CadaKms ,
          @CadaTiempoDias ,
          @Estatus ,
          @Costo ,
          @idDivision
        );

                      
            SET @valor = 0;
            SET @mensaje = 'Tipo de Unidad Agregado con Exito';
            --SET @id = @@IDENTITY;
            SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
       UPDATE  CatServicios
SET     idTipoServicio = @idTipoServicio ,
        NomServicio = @NomServicio ,
        Descripcion = @Descripcion ,
        CadaKms = @CadaKms ,
        CadaTiempoDias = @CadaTiempoDias ,
        Estatus = @Estatus ,
        Costo = @Costo ,
        idDivision = @idDivision
WHERE   idServicio = @idServicio;

            SET @valor = 0;
            SET @mensaje = 'Tipo de Unidad actualizado con Exito';
            SET @id = @idUnidadTrans;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idServicio", cat.idServicio));
                        comand.Parameters.Add(new SqlParameter("idTipoServicio", cat.idTipoServicio));
                        comand.Parameters.Add(new SqlParameter("NomServicio", cat.NomServicio));
                        comand.Parameters.Add(new SqlParameter("Descripcion", cat.Descripcion));
                        comand.Parameters.Add(new SqlParameter("CadaKms", cat.CadaKms));
                        comand.Parameters.Add(new SqlParameter("CadaTiempoDias", cat.CadaTiempoDias));
                        comand.Parameters.Add(new SqlParameter("Estatus", cat.Estatus));
                        comand.Parameters.Add(new SqlParameter("Costo", cat.Costo));
                        comand.Parameters.Add(new SqlParameter("idDivision", cat.idDivision));


                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idServicio = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
