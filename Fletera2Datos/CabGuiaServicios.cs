﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Fletera2Datos.Enums;

namespace Fletera2Datos
{
    public class CabGuiaServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getCabGuiaxFilter(int NumGuiaId = 0, string filtroSinWhere = "", string OrderBy = "", string PreEspSelect = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (NumGuiaId > 0)
                    {
                        filtro = " where NumGuiaId = " + NumGuiaId + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }
                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }


                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
SELECT {1} c.NumGuiaId, 
        c.SerieGuia, 
        c.NumViaje, 
        c.idCliente,
        cli.Nombre AS NomCliente, 
        c.IdEmpresa, 
        c.FechaHoraViaje, 
        c.idOperador, 
        c.idTractor, 
        c.idRemolque1 as idRemolque1, 
        isnull(c.idDolly,'') as idDolly, 
        isnull(c.idRemolque2,'') as idRemolque2, 
        isnull(c.FechaHoraFin,getdate()) as FechaHoraFin, 
        isnull(c.EstatusGuia,'') as EstatusGuia, 
        c.UserViaje, 
        isnull(c.UserFinCancela,0) as UserFinCancela, 
        c.MotivoCancelacion, 
        c.TipoViaje, 
        c.tara, 
        isnull(c.NoSerie,'') as NoSerie, 
        isnull(c.NoFolio,0) as NoFolio, 
        c.km, 
        isnull(c.fechaBachoco,getdate()) as fechaBachoco, 
        c.tara2, 
        c.externo, 
        isnull(c.NumLiquidacion,0) as NumLiquidacion 
        FROM dbo.CabGuia c 
        INNER JOIN dbo.CatClientes cli ON cli.IdCliente = c.idCliente
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro,PreEspSelect);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        comand.Parameters.Add(new SqlParameter("@NumGuiaId", NumGuiaId));


                        connection.Open();
                        List<CabGuia> lista = new List<CabGuia>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CabGuia folio = new CabGuia
                                {
                                    NumGuiaId = (int)sqlReader["NumGuiaId"],
                                    SerieGuia = (string)sqlReader["SerieGuia"],
                                    NumViaje = (int)sqlReader["NumViaje"],
                                    idCliente = (int)sqlReader["idCliente"],
                                    NomCliente = (string)sqlReader["NomCliente"],
                                    IdEmpresa = (int)sqlReader["IdEmpresa"],
                                    FechaHoraViaje = (DateTime)sqlReader["FechaHoraViaje"],
                                    idOperador = (int)sqlReader["idOperador"],
                                    idTractor = (string)sqlReader["idTractor"],
                                    idRemolque1 = (string)sqlReader["idRemolque1"],
                                    idDolly = (string)sqlReader["idDolly"],
                                    idRemolque2 = (string)sqlReader["idRemolque2"],
                                    FechaHoraFin = (DateTime)sqlReader["FechaHoraFin"],
                                    EstatusGuia = (string)sqlReader["EstatusGuia"],
                                    UserViaje = (int)sqlReader["UserViaje"],
                                    UserFinCancela = (int)sqlReader["UserFinCancela"],
                                    MotivoCancelacion = (string)sqlReader["MotivoCancelacion"],
                                    TipoViaje = (string)sqlReader["TipoViaje"],
                                    tara = (decimal)sqlReader["tara"],
                                    NoSerie = (string)sqlReader["NoSerie"],
                                    NoFolio = (int)sqlReader["NoFolio"],
                                    km = (decimal)sqlReader["km"],
                                    fechaBachoco = (DateTime)sqlReader["fechaBachoco"],
                                    tara2 = (decimal)sqlReader["tara2"],
                                    externo = (bool)sqlReader["externo"],
                                    NumLiquidacion = (int)sqlReader["NumLiquidacion"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult getLiquidacionxFilter(string filtroSinWhere = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    filtro = " where " + filtroSinWhere;

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT distinct c.NumGuiaId, 
        c.idOperador,
        op.NombreCompleto AS NomOperador,
        c.IdEmpresa,
        em.RazonSocial AS NomEmpresa,
        c.idCliente,
        cli.Nombre AS NomCliente,
        c.FechaHoraViaje,
        c.FechaHoraFin,
        c.idTractor,
        c.idRemolque1,
        isnull(c.idDolly,'') AS idDolly,
        isnull(c.idRemolque2,'') AS idRemolque2,
        c.UserViaje,
        us.nombreUsuario,
        usp.NombreCompleto AS NomUsuario,
        c.TipoViaje,
        d.VolDescarga,
        d.idCargaGasolina,
        isnull(d.precioOperador,0) as precioOperador,
        o.nombreOrigen, 
        pl.Descripcion AS Destino,
        p.Descripcion AS Producto
        FROM dbo.DetGuia d
        INNER JOIN dbo.CabGuia c ON c.NumGuiaId = d.NumGuiaId
        INNER JOIN dbo.origenDestinos od ON od.idCliente = c.idCliente 
        AND od.idOrigen = d.idOrigen AND od.idDestino = d.IdPlazaTraf
        INNER JOIN dbo.origen o ON o.idOrigen = od.idOrigen
        INNER JOIN dbo.trafico_plazas_cat pl ON od.idDestino = pl.Clave
        INNER JOIN dbo.CatProductos p ON d.IdProdTraf = p.IdProducto
        INNER JOIN dbo.CatClientes cli ON c.idCliente = cli.IdCliente
        left JOIN dbo.usuariosSys us ON c.UserViaje = us.idUsuario
        INNER JOIN dbo.CatPersonal usp ON us.idPersonal = usp.idPersonal
        INNER JOIN dbo.CatEmpresas em ON c.IdEmpresa = em.idEmpresa
        INNER JOIN dbo.CatPersonal op ON c.idOperador = op.idPersonal
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@NumGuiaId", NumGuiaId));


                        connection.Open();
                        List<Liquidaciones> lista = new List<Liquidaciones>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                Liquidaciones folio = new Liquidaciones
                                {
                                    NumGuiaId = (int)sqlReader["NumGuiaId"],
                                    idOperador = (int)sqlReader["idOperador"],
                                    NomOperador = (string)sqlReader["NomOperador"],
                                    idEmpresa = (int)sqlReader["idEmpresa"],
                                    NomEmpresa = (string)sqlReader["NomEmpresa"],
                                    idCliente = (int)sqlReader["idCliente"],
                                    NomCliente = (string)sqlReader["NomCliente"],
                                    FechaHoraViaje = (DateTime)sqlReader["FechaHoraViaje"],
                                    FechaHoraFin = (DateTime)sqlReader["FechaHoraFin"],
                                    idTractor = (string)sqlReader["idTractor"],
                                    idRemolque1 = (string)sqlReader["idRemolque1"],
                                    idDolly = (string)sqlReader["idDolly"],
                                    idRemolque2 = (string)sqlReader["idRemolque2"],
                                    UserViaje = (int)sqlReader["UserViaje"],
                                    nombreUsuario = (string)sqlReader["nombreUsuario"],
                                    NomUsuario = (string)sqlReader["NomUsuario"],
                                    TipoViaje = (string)sqlReader["TipoViaje"],
                                    VolDescarga = (decimal)sqlReader["VolDescarga"],
                                    idCargaGasolina = (int)sqlReader["idCargaGasolina"],
                                    precioOperador = (decimal)sqlReader["precioOperador"],
                                    nombreOrigen = (string)sqlReader["nombreOrigen"],
                                    Destino = (string)sqlReader["Destino"],
                                    Producto = (string)sqlReader["Producto"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        //ViajePorOperador
        public OperationResult getViajePorOperadorxFilter(string filtroSinWhere, string sOrderBy)
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    filtro = " where " + filtroSinWhere;

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
        SELECT CONVERT(BIT,0) AS Seleccionado,
        v.idOperador, 
        p.NombreCompleto as NomOperador, 
        COUNT(v.NumViaje) AS NumViaje 
        FROM dbo.CabGuia v 
        INNER JOIN dbo.CatPersonal P ON v.idOperador = p.idPersonal 
        {0} 
         GROUP BY v.idOperador,p.NombreCompleto
        ORDER BY {1}
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro, sOrderBy);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@NumGuiaId", NumGuiaId));


                        connection.Open();
                        List<ViajePorOperador> lista = new List<ViajePorOperador>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                ViajePorOperador folio = new ViajePorOperador
                                {
                                    Seleccionado = (bool)sqlReader["Seleccionado"],
                                    idOperador = (int)sqlReader["idOperador"],
                                    NomOperador = (string)sqlReader["NomOperador"],
                                    NumViaje = (int)sqlReader["NumViaje"]
                                };
                                lista.Add(folio);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult ActualizaCabGuia(ref CabGuia folio)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @NumGuiaId > 0
        BEGIN
           UPDATE dbo.CabGuia SET 
            NumLiquidacion = @NumLiquidacion 
            WHERE NumGuiaId= @NumGuiaId;
            SET @valor = 0;
            SET @mensaje = 'Viaje actualizado con Exito';
            SET @id = @NumGuiaId;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("@NumGuiaId", folio.NumGuiaId));
                        comand.Parameters.Add(new SqlParameter("@NumLiquidacion", folio.NumLiquidacion));
                        

                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            folio.NumGuiaId = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        

    }//
}
