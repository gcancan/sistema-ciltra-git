﻿using Core.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using Fletera2Entidades;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Fletera2Datos
{
    public class CatLugaresServicios
    {
        private string strsql;
        private string filtro;
        public OperationResult getCatLugaresxFilter(int idLugar = 0, string filtroSinWhere = "", string OrderBy = "")
        {
            try
            {
                using (SqlConnection connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql

                    filtro = "";
                    if (idLugar > 0)
                    {
                        filtro = " where C.idLugar = " + idLugar + (filtroSinWhere != "" ? " AND " + filtroSinWhere : filtroSinWhere);
                    }
                    else if (filtroSinWhere != "")
                    {
                        filtro = " where " + filtroSinWhere;
                    }

                    if (OrderBy != "")
                    {
                        filtro = filtro + " ORDER BY " + OrderBy;
                    }

                    strsql = string.Format(@"BEGIN TRY
--DECLARE @idNacionalidad INT = 1;
--DECLARE @valor INT;
--DECLARE @mensaje INT;
       SELECT  c.idLugar ,
        c.Descripcion ,
        c.idTipOrdServ ,
        c.Activo ,
        c.NomTabla
      FROM    dbo.CatLugares c
        {0} 
    IF @@ROWCOUNT > 0
    BEGIN
        SET @valor = 0;
        SET @mensaje = 'Se Encontraron Registros';
    END;
    ELSE
    BEGIN
        SET @valor = 3;
        SET @mensaje = 'No se encontraron registros';
    END;
END TRY
BEGIN CATCH
SET @valor = 1;
SET @mensaje = ERROR_MESSAGE();
END CATCH;
", filtro);
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, false))
                    {
                        //comand.Parameters.Add(new SqlParameter("@idConcepto", idUnidadTrans));


                        connection.Open();
                        List<CatLugares> lista = new List<CatLugares>();
                        using (SqlDataReader sqlReader = comand.ExecuteReader())
                        {
                            while (sqlReader.Read())
                            {
                                CatLugares cat = new CatLugares
                                {
                                    idLugar = (int)sqlReader["idLugar"],
                                    Descripcion = (string)sqlReader["Descripcion"],
                                    idTipOrdServ = (int)sqlReader["idTipOrdServ"],
                                    Activo = (bool)sqlReader["Activo"],
                                    NomTabla = (string)sqlReader["NomTabla"]
                                };
                                lista.Add(cat);
                            }


                        }
                        return new OperationResult(comand, lista);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 0, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }

        public OperationResult GuardaCatLugares(ref CatLugares cat)
        {
            try
            {
                using (var connection = BoundedContextFactory.ConnectionFactory())
                {
                    #region Consulta_Sql
                    strsql = @"
BEGIN TRAN guardar;
BEGIN TRY
    --DECLARE @idNacionalidad INT;
    --DECLARE @nombre NVARCHAR(MAX);
    --DECLARE @orden INT;    
    
    --DECLARE @valor INT;    
    --DECLARE @mensaje NVARCHAR(MAX);
    --DECLARE @id INT;    
	
    IF @idConcepto = 0
        BEGIN
        INSERT  INTO CatLugares
        ( idLugar ,
          Descripcion ,
          idTipOrdServ ,
          Activo ,
          NomTabla)
VALUES  ( @idLugar ,
          @Descripcion ,
          @idTipOrdServ ,
          @Activo ,
          @NomTabla);
                      
            SET @valor = 0;
            SET @mensaje = 'Lugar de Llanta Agregado con Exito';
            SET @id = @@IDENTITY;
            --SET @id = @idUnidadTrans;
        END;
    ELSE
        BEGIN
        UPDATE  CatLugares
SET     Descripcion = @Descripcion ,
        idTipOrdServ = @idTipOrdServ ,
        Activo = @Activo ,
        NomTabla = @NomTabla
WHERE   idLugar = @idLugar;
            SET @valor = 0;
            SET @mensaje = 'Lugar de Llanta actualizado con Exito';
            SET @id = @idClasLlan;
        END;
    COMMIT TRAN guardar;        
END TRY
BEGIN CATCH
    ROLLBACK TRAN guardar;
    SET @valor = 1;
    SET @mensaje = ERROR_MESSAGE();
    SET @id = 0;
END CATCH;
";
                    #endregion

                    using (SqlCommand comand = CrearSqlCommand.getCommandText(connection, strsql, true))
                    {
                        comand.Parameters.Add(new SqlParameter("idLugar", cat.idLugar));
                        comand.Parameters.Add(new SqlParameter("Descripcion", cat.Descripcion));
                        comand.Parameters.Add(new SqlParameter("idTipOrdServ", cat.idTipOrdServ));
                        comand.Parameters.Add(new SqlParameter("Activo", cat.Activo));
                        comand.Parameters.Add(new SqlParameter("NomTabla", cat.NomTabla));



                        connection.Open();
                        var i = comand.ExecuteNonQuery();

                        if ((int)comand.Parameters["@valor"].Value == 0)
                        {
                            cat.idLugar = (int)comand.Parameters["@id"].Value;
                        }

                        return new OperationResult(comand);
                    }
                }
            }
            catch (Exception ex)
            {
                //return new OperationResult { valor = 1, mensaje = ex.Message };
                return new OperationResult(ex);
            }
        }
    }//
}
