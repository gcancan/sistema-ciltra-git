﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoreFletera.Interfaces;
using Core.Models;
using CoreFletera.Models;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for PreciosCombustibles.xaml
    /// </summary>
    public partial class PreciosCombustibles : ViewBase
    {
        public PreciosCombustibles()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                getTiposCombustible();
                nuevo();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        public override OperationResult guardar()
        {
            try
            {
                OperationResult val = validar();
                if (val.typeResult != ResultTypes.success)
                {
                    return val;
                }
                PrecioCombustible precioCombustible = mapForm();
                if (precioCombustible != null)
                {
                    OperationResult resp = new PrecioCombustibleSvc().savePrecioCombustible(ref precioCombustible);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        getTiposCombustible();
                        mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult { valor = 2, mensaje = "No se pudo realizar la operación" };
                }
                
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private PrecioCombustible mapForm()
        {
            try
            {
                return new PrecioCombustible
                {
                    idPrecioCombustible = 0,
                    tipoCombustible = (TipoCombustible)cbxTiposCombustible.SelectedItem,
                    fecha = DateTime.Now,
                    usuario = mainWindow.inicio._usuario.nombreUsuario,
                    precio = Convert.ToDecimal(txtPrecio.Text.Trim())
                };
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }

        private OperationResult validar()
        {
            try
            {
                bool bien = true;
                string cadena = "PARA PROCEDER SE REQUIERE:" + Environment.NewLine;
                if (cbxTiposCombustible.SelectedItem == null)
                {
                    cadena += " * Seleccionar el tipo de Combustible";
                    bien = false;
                }
                if (string.IsNullOrEmpty(txtPrecio.Text.Trim()))
                {
                    cadena += " * Especificar el precio";
                    bien = false;
                }
                else
                {
                    decimal valor = 0;
                    if (!decimal.TryParse(txtPrecio.Text.Trim(), out valor))
                    {
                        cadena += " * Especificar un precio valido";
                        bien = false;
                    }
                    else
                    {
                        if (valor <= 0)
                        {
                            cadena += " * El precio tiene que ser mayor que  0";
                            bien = false;
                        }
                    }
                }
                return new OperationResult
                {
                    valor = bien ? 0 : 2,
                    mensaje = bien ? "Validación correcta" : cadena,
                    result = null
                };
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public override void nuevo()
        {
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
        }

        private void getTiposCombustible()
        {
            try
            {
                OperationResult resp = new TipoCombustibleSvc().getTiposCombustibleAllOrById();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxTiposCombustible.ItemsSource = (List<TipoCombustible>)resp.result;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void cbxTiposCombustible_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxTiposCombustible.SelectedItem != null)
            {
                txtPrecio.Text = ((TipoCombustible)cbxTiposCombustible.SelectedItem).ultimoPrecio.ToString();
            }
            else
            {
                txtPrecio.Clear();
            }
        }
    }
}
