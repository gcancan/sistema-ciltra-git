﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for ReporteBachoco.xaml
    /// </summary>
    public partial class ReporteBachoco : ViewBase
    {
        int claveEmpresa = 0;
        public ReporteBachoco()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            claveEmpresa = mainWindow.inicio._usuario.personal.empresa.clave;
            mainWindow.Title = "Generar Reporte";
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
            dtpFechaInicio.SelectedDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            var v = mainWindow.inicio._usuario.personal.nombre;

            dtpFechaFin.SelectedDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            //buscarCartasPorte();

            mapOperadores();
            mapTractores();
            mapCliente();

            cbxEstatus.Items.Add("TODOS");
            cbxEstatus.Items.Add("ACTIVO");
            cbxEstatus.Items.Add("CANCELADO");
            cbxEstatus.Items.Add("FINALIZADO");
            cbxEstatus.SelectedIndex = 0;

            cbxArea.Items.Add("TODOS");
            cbxArea.Items.Add("A");
            cbxArea.Items.Add("B");
            cbxArea.Items.Add("C");
            cbxArea.Items.Add("D");
            cbxArea.Items.Add("E");
            cbxArea.SelectedIndex = 0;

            cbxMes.ItemsSource = Enum.GetValues(typeof(Mes));
            cbxMes.SelectedIndex = DateTime.Now.Month - 1;
            int anio = DateTime.Now.Year;
            for (int x = anio; x > anio - 10; x--)
            {
                cbxAnio.Items.Add(x);
            }
            cbxAnio.SelectedIndex = 0;

            int i = 0;
            foreach (Cliente item in cbxClientes.Items)
            {
                if (item.clave == mainWindow.inicio._usuario.cliente.clave)
                {
                    cbxClientes.SelectedIndex = i;
                    //cbxClientes.IsEnabled = false;
                    break;
                }
                i++;
            }
        }

        private void mapCliente()
        {
            var respCliente = new ClienteSvc().getClientesALLorById(mainWindow.inicio._usuario.personal.empresa.clave);
            List<Cliente> listCliente = (List<Cliente>)respCliente.result;
            mapComboClientes(listCliente);
        }
        private void mapComboClientes(List<Cliente> listCliente)
        {
            foreach (Cliente item in listCliente)
            {
                cbxClientes.Items.Add(item);
            }
        }

        public void mapOperadores()
        {
            var resp = new OperadorSvc().getOperadoresALLorById();

            if (resp.typeResult == ResultTypes.error)
            {
                MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (resp.typeResult == ResultTypes.warning)
            {
                MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (resp.typeResult == ResultTypes.recordNotFound)
            {
                MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (resp.typeResult == ResultTypes.success)
            {
                List<Personal> listOperadores = (List<Personal>)resp.result;
                cbxOperadores.Items.Add(new Personal { nombre = "TODOS", clave = 0 });
                foreach (Personal item in listOperadores)
                {
                    cbxOperadores.Items.Add(item);
                }
            }
            cbxOperadores.SelectedIndex = 0;
        }

        private void mapTractores()
        {
            var resp = new UnidadTransporteSvc().getUnidadesALLorById(claveEmpresa);

            if (resp.typeResult == ResultTypes.error)
            {
                MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (resp.typeResult == ResultTypes.warning)
            {
                MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (resp.typeResult == ResultTypes.recordNotFound)
            {
                MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (resp.typeResult == ResultTypes.success)
            {
                List<UnidadTransporte> list = (List<UnidadTransporte>)resp.result;
                cbxCamion.Items.Add(new UnidadTransporte { clave = "TODOS", tipoUnidad = new TipoUnidad { clave = 0, descripcion = "" } });

                mapCombos(list);

                //cbxCamion.SelectedIndex = 0;
            }
        }
        private void mapCombos(List<UnidadTransporte> list)
        {

            foreach (UnidadTransporte item in list)
            {
                if (item.tipoUnidad.clasificacion.ToUpper() == "TRACTOR")
                {
                    cbxCamion.Items.Add(item);
                }
            }
            cbxCamion.SelectedIndex = 0;
        }

        private void cbxClientes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender != null)
            {
                if (cbxClientes.SelectedItem != null)
                {
                    Cliente cliente = (Cliente)cbxClientes.SelectedItem;
                    var respZonas = new ZonasSvc().getZonasALLorById(0, ((Cliente)cbxClientes.SelectedItem).clave.ToString());
                    var _listZonas = (List<Zona>)respZonas.result;

                    lvlProductos.Items.Clear();
                    lvlSalidas.Items.Clear();
                    cbxDestino.Items.Clear();
                    cbxDestino.Items.Add(new Zona { clave = 0, descripcion = "TODOS" });
                    foreach (Zona item in _listZonas)
                    {
                        cbxDestino.Items.Add(item);
                    }
                    cbxDestino.SelectedIndex = 0;
                }
            }
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            buscarCartasPorte();
        }
        DateTime fechaInicio = new DateTime();
        DateTime fechaFin = new DateTime();
        internal void buscarCartasPorte()
        {
            try
            {
                Cursor = Cursors.Wait;
                lvlSalidas.Items.Clear();
                List<Viaje> listViaje = new List<Viaje>();
                if (tapMes.IsSelected)
                {
                    var dia = DateTime.DaysInMonth((int)(cbxAnio.SelectedItem), (int)((Mes)cbxMes.SelectedItem));
                    fechaInicio = Convert.ToDateTime(string.Format("01/{0}/{1} 00:00:00", ((int)((Mes)cbxMes.SelectedItem)).ToString(), (cbxAnio.SelectedItem)));
                    fechaFin = Convert.ToDateTime(string.Format("{0}/{1}/{2} 00:00:00", dia.ToString(), ((int)((Mes)cbxMes.SelectedItem)).ToString(), (cbxAnio.SelectedItem)));
                }
                else
                {
                    fechaInicio = dtpFechaInicio.SelectedDate.Value;
                    fechaFin = dtpFechaFin.SelectedDate.Value.AddDays(1);
                }

                OperationResult resp = new CartaPorteSvc().getCartaPorteByfiltros
                    (
                    fechaInicio,
                    fechaFin.AddDays(1),
                    cbxDestino.SelectedIndex == 0 ? "%" : ((Zona)cbxDestino.SelectedItem).clave.ToString(),
                    cbxOperadores.SelectedIndex == 0 ? "%" : ((Personal)cbxOperadores.SelectedItem).clave.ToString(),
                    cbxCamion.SelectedIndex == 0 ? "%" : ((UnidadTransporte)cbxCamion.SelectedItem).clave,
                    cbxEstatus.SelectedIndex == 0 ? "%" : cbxEstatus.SelectedItem.ToString(),
                    ((Cliente)cbxClientes.SelectedItem).clave.ToString(),
                    ((Cliente)cbxClientes.SelectedItem).empresa.clave
                    );

                if (resp.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                else if (resp.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else if (resp.typeResult == ResultTypes.success)
                {
                    foreach (var item in (List<Viaje>)resp.result)
                    {
                        listViaje.Add(item);
                    }
                }
                if (listViaje.Count == 0)
                {
                    MessageBox.Show("No se encontraron registros", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else
                {
                    mapLista(listViaje);
                }
                lvlProductos.Items.Clear();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(new OperationResult { valor = 1, mensaje = ex.Message });
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }


        }

        private bool validarIsFull(Viaje cartaPorte)
        {
            if (cartaPorte.remolque.tipoUnidad.descripcion == "TOLVA 35 TON")
            {
                return true;
            }
            if (cartaPorte.dolly != null || cartaPorte.remolque2 != null)
            {
                return true;
            }
            return false;
        }
        private TipoPrecio establecerPrecio(Viaje result)
        {
            if (result.dolly != null || result.remolque2 != null)
            {
                return TipoPrecio.FULL;
            }
            else if (result.remolque.tipoUnidad.descripcion == "TOLVA 35 TON")
            {
                return TipoPrecio.SENCILLO35;
            }
            else
            {
                return TipoPrecio.SENCILLO;
            }
        }

        private void mapLista(List<Viaje> result)
        {
            foreach (Viaje item in result)
            {
                foreach (CartaPorte det in item.listDetalles)
                {
                    det.tipoPrecio = establecerPrecio(item);
                }
                bool isFull = validarIsFull(item);
                if (!rbnTodos.IsChecked.Value)
                {
                    switch (isFull)
                    {
                        case true:
                            if (!rbnFull.IsChecked.Value)
                            {
                                continue;
                            }
                            break;
                        case false:
                            if (!rbnSencillo.IsChecked.Value)
                            {
                                continue;
                            }
                            break;
                    }

                }

                if (cbxArea.SelectedIndex != 0)
                {
                    bool resT = false;
                    foreach (var items in result)
                    {
                        bool res = false;
                        foreach (var det in item.listDetalles)
                        {
                            if (det.zonaSelect.claveZona != cbxArea.SelectedItem.ToString())
                            {
                                res = true;
                                break;
                            }
                        }
                        if (res)
                        {
                            resT = true;
                            break;
                        }
                    }
                    if (resT)
                    {
                        continue;
                    }
                }
                ListViewItem lvl = new ListViewItem();

                lvl.Content = item;
                lvl.FontWeight = FontWeights.Bold;
                //lvl.Selected += Lvl_Selected;
                switch (item.EstatusGuia.ToUpper())
                {
                    case "ACTIVO":
                        lvl.Foreground = Brushes.SteelBlue;
                        lvlSalidas.Items.Add(lvl);
                        break;
                    case "CANCELADO":
                        lvl.Foreground = Brushes.OrangeRed;
                        lvlSalidas.Items.Add(lvl);
                        break;
                    case "FINALIZADO":
                        lvl.Foreground = Brushes.Blue;
                        lvlSalidas.Items.Add(lvl);
                        break;
                    default:
                        lvlSalidas.Items.Add(lvl);
                        break;
                }

                //if (item.EstatusGuia.ToUpper() == "ACTIVO")
                //{
                //    lvl.Foreground = Brushes.SteelBlue;
                //}
                //if (item.EstatusGuia.ToUpper() == "CANCELADO")
                //{
                //    lvl.Foreground = Brushes.OrangeRed;
                //}
                //if (item.EstatusGuia.ToUpper() == "FINALIZADO")
                //{
                //    lvl.Foreground = Brushes.Blue;
                //}
                //lvlSalidas.Items.Add(lvl);
            }
        }
        private void btnGenerarReporte_Click(object sender, RoutedEventArgs e)
        {
            Cursor = Cursors.Wait;
            try
            {
                if (lvlSalidas.Items.Count <= 0)
                {
                    MessageBox.Show("Se requieren resultados para proceder", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                ReportView reporte = new ReportView(mainWindow);
                bool resp = reporte.documentoReporteViajesBachoco(
                                                                getListCartaPOrtes(),
                                                                fechaInicio, fechaFin,
                                                                (Cliente)cbxClientes.SelectedItem);
                if (resp)
                {
                    reporte.Show();
                    MessageBox.Show("Se creo correctamente el reporte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Ocurrio un error al intentar crear el reporte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private List<Viaje> getListCartaPOrtes()
        {
            List<Viaje> list = new List<Viaje>();
            foreach (ListViewItem item in lvlSalidas.Items)
            {
                list.Add((Viaje)item.Content);
            }
            return list;
        }

        private void btnReporteMensual_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lvlSalidas.Items.Count <= 0)
                {
                    MessageBox.Show("Se requieren resultados para proceder", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                Cursor = Cursors.Wait;

                ReportView reporte = new ReportView(mainWindow);
                bool resp = reporte.documentoReporteViajesBachocoMensual(getListCartaPOrtes(),
                    fechaInicio, fechaFin, (Cliente)cbxClientes.SelectedItem);
                if (resp)
                {
                    //reporte.Show();
                    MessageBox.Show("Se creo correctamente el reporte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Ocurrio un error al intentar crear el reporte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
