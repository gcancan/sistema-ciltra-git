﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
using ProcesosComercial;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editActividades.xaml
    /// </summary>
    public partial class editActividades : ViewBase
    {
        public editActividades()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {            
            cargarDivision();
            nuevo();
            cargarActividades();
        }
        List<Actividad> _listaActividades = new List<Actividad>();
        private void cargarActividades()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new ConsultasTallerSvc().getActividadesCOM(AreaTrabajo.TOTAS);
                if (resp.typeResult == ResultTypes.success)
                {
                    _listaActividades = resp.result as List<Actividad>;
                    mapLista(_listaActividades);
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    imprimir(resp);
                    IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapLista(List<Actividad> list)
        {
            lvlActividades.ItemsSource = null;
            try
            {
                List<ListViewItem> listLvl = new List<ListViewItem>();
                foreach (var item in list)
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = item;
                    //lvl.MouseDoubleClick += Lvl_MouseDoubleClick; 
                    listLvl.Add(lvl);
                }
                lvlActividades.ItemsSource = listLvl;
                CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlActividades.ItemsSource);
                view.Filter = UserFilter;
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((Actividad)(item as ListViewItem).Content).CCODIGOPRODUCTO.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Actividad)(item as ListViewItem).Content).nombre.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        public override void nuevo()
        {
            base.nuevo();
            mapActividad(null);
            if (expand.IsExpanded)
            {
                mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar| ToolbarCommands.cerrar);
            }
            else
            {
                mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
            }

        }
        private void cargarDivision()
        {
            var resp = new OrdenesTrabajoSvc().getDivisiones();
            if (resp.typeResult == ResultTypes.success)
            {
                List<Division> lista = (List<Division>)resp.result;
                cbxDivision.ItemsSource = lista;
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
            }
        }
        private void txtActividad_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    buscarActividad();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void buscarActividad()
        {
            Actividad actividad = null;
            try
            {
                if (string.IsNullOrEmpty(txtActividad.Text.Trim()))
                {
                    actividad = new selectActividadCOM(txtActividad.Text).getActividadCOM();
                }
                else
                {
                    var resp = new ConsultasTallerSvc().getActividadesCOM(AreaTrabajo.TOTAS, txtActividad.Text);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        List<Actividad> listaActividades = resp.result as List<Actividad>;
                        if (listaActividades.Count == 1)
                        {
                            actividad = listaActividades[0];
                        }
                        else
                        {
                            actividad = new selectActividadCOM(txtActividad.Text).getActividadCOM();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                mapActividad(actividad);
            }
        }
        void mapActividad(Actividad actividad)
        {
            if (actividad != null)
            {
                txtActividad.Tag = actividad;
                txtActividad.Text = actividad.nombre;
                txtActividad.IsEnabled = false;
                chcLavadero.IsChecked = actividad.lavadero;
                chcLlantera.IsChecked = actividad.llantera;
                chcTaller.IsChecked = actividad.taller;
                txtInsumo.Focus();
                stpInsumos.Children.Clear();
                OperationResult resp = new ActividadSvc().getProductosCOMByActividad(actividad.CIDPRODUCTO);
                if (resp.typeResult == ResultTypes.success)
                {
                    foreach (var item in resp.result as List<ProductosCOM>)
                    {
                        añadirProducto(item);
                    }
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    imprimir(resp);
                }
                mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            }
            else
            {
                txtActividad.Tag = null;
                txtActividad.Clear();
                txtActividad.IsEnabled = true;
                chcLavadero.IsChecked = false;
                chcLlantera.IsChecked = false;
                chcTaller.IsChecked = false;
                stpInsumos.Children.Clear();
                mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            }
        }
        private void btnCambiar_Click(object sender, RoutedEventArgs e)
        {
            mapActividad(null);
        }
        private void txtInsumo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                buscarProducto();
            }
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            buscarProducto();
        }
        void buscarProducto()
        {
            var insumo = new selectProductosCOM(txtInsumo.Text.Trim()).consultasTaller(AreaTrabajo.TOTAS, false);
            if (insumo != null)
            {
                //añadirProducto(insumo);
            }
        }
        private void añadirProducto(ProductosCOM insumo)
        {
            try
            {
                StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal, Tag = insumo };
                Label lblProducto = new Label { Content = stpContent, Tag = insumo };
                #region contenido
                Label lblNombreProducto = new Label
                {
                    Content = insumo.nombre,
                    Width = 350,
                    Tag = insumo,
                    Margin = new Thickness(2)
                };
                stpContent.Children.Add(lblNombreProducto);

                controlDecimal ctrDecimal = new controlDecimal { Tag = insumo, valor = insumo.cantidad };
                ctrDecimal.txtDecimal.Tag = insumo;
                ctrDecimal.valor = insumo.cantidad;
                ctrDecimal.txtDecimal.TextChanged += TxtDecimal_TextChanged; ;
                //ctrDecimal.DataContextChanged += CtrDecimal_DataContextChanged;
                stpContent.Children.Add(ctrDecimal);

                Button btnQuitar = new Button
                {
                    Content = "Quitar",
                    Tag = insumo,
                    Foreground = Brushes.White,
                    Background = Brushes.Orange,
                    Margin = new Thickness(2),
                    Width = 100
                };
                btnQuitar.Click += BtnQuitar_Click; ;
                stpContent.Children.Add(btnQuitar);
                #endregion


                stpInsumos.Children.Add(lblProducto);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                txtInsumo.Clear();
                txtInsumo.Focus();
            }
        }

        private void BtnQuitar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                ProductosCOM producto = btn.Tag as ProductosCOM;

                var resp = new ActividadSvc().quitarRelActividadProducto((txtActividad.Tag as Actividad).CIDPRODUCTO, producto.idProducto);
                if (resp.typeResult != ResultTypes.success)
                {
                    imprimir(resp);
                    return;
                }

                List<Label> listLBL = stpInsumos.Children.Cast<Label>().ToList();
                int i = 0;
                foreach (Label item in listLBL)
                {
                    if (producto == (item.Tag as ProductosCOM))
                    {
                        stpInsumos.Children.Remove(stpInsumos.Children[i]);
                        return;
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void TxtDecimal_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                TextBox ctrl = sender as TextBox;
                ProductosCOM producto = ctrl.Tag as ProductosCOM;
                List<Label> listLBL = stpInsumos.Children.Cast<Label>().ToList();
                //int i = 0;
                foreach (Label item in listLBL)
                {
                    ProductosCOM pro = (item.Tag as ProductosCOM);
                    if (producto == pro)
                    {
                        pro.cantidad = Convert.ToDecimal(ctrl.Text.Trim());
                    }
                    //i++;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        public override OperationResult guardar()
        {
            try
            {
                if (txtActividad.Tag == null)
                {
                    return new OperationResult { valor = 2, mensaje = "NO HAY ACTIVIDAD PARA GUARDAR" };
                }
                if (cbxDivision.SelectedItem == null)
                {
                    return new OperationResult { valor = 2, mensaje = "SELECCIONAR UNA DIVISIÓN" };
                }
                Actividad actividad = mapForm();
                if (actividad != null)
                {
                    var resp = new ActividadSvc().saveActividad(ref actividad);
                    return resp;
                }
                else
                {
                    return new OperationResult() { valor = 1, mensaje = "ERROR AL LEER LOS DATOS DE LA PANTALLA" };
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        private Actividad mapForm()
        {
            try
            {
                Actividad actividad = txtActividad.Tag as Actividad;
                actividad.division = (cbxDivision.SelectedItem as Division);
                actividad.listProductos = new List<ProductosActividad>();
                List<ProductosCOM> listaProductos = getListaProductos(); ;


                if (listaProductos != null)
                {
                    actividad.listProductosCOM = listaProductos;
                }
                else
                {
                    return null;
                }

                return actividad;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }

        private List<ProductosCOM> getListaProductos()
        {
            try
            {
                List<ProductosCOM> lista = new List<ProductosCOM>();
                foreach (Label item in stpInsumos.Children.Cast<Label>().ToList())
                {
                    lista.Add(item.Tag as ProductosCOM);
                }
                return lista;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlActividades.ItemsSource).Refresh();
        }

        private void btnSincronizar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                List<Actividad> listaActividad = getListaActividades();
                if (listaActividad != null)
                {
                    ProcesosForm procesos = new ProcesosForm(listaActividad);
                    imprimir(procesos.sincronizarActividad());
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private List<Actividad> getListaActividades()
        {
            try
            {
                List<Actividad> listaActividad = new List<Actividad>();
                foreach (Actividad actividad in _listaActividades)
                {
                    actividad.division = new Division { idDivision = 0, nombre = "SIN ASIGNAR" };
                    //actividad.estatus = "ACT";
                    listaActividad.Add(actividad);
                }
                return listaActividad;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }

        private void Expander_Collapsed(object sender, RoutedEventArgs e)
        {
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
        }

        private void Expander_Expanded(object sender, RoutedEventArgs e)
        {
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
        }
    }
}
