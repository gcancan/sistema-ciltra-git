﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
using Core.Interfaces;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for editCambioPrecios.xaml
    /// </summary>
    public partial class editCambioPrecios : ViewBase
    {
        public editCambioPrecios()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                mapComboClientes();
                nuevo();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        void mapPrecios()
        {
            foreach (var item in Enum.GetValues(typeof(eTiposPrecios)))
            {
                cbxTipoPrecio.Items.Add(item);
            }
        }
        private void mapComboClientes()
        {
            try
            {
                OperationResult resp = new ClienteSvc().getClientesALLorById(mainWindow.empresa.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxClientes.ItemsSource = (List<Cliente>)resp.result;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            txtPrecioAnterior.valor = 0;
            txtPrecioNuevo.valor = 0;
            //cbxClientes.SelectedItem = null;
            //cbxOrigen.SelectedItem = null;
            cbxTipoPrecio.SelectedItem = null;
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
        }

        private void cbxTipoPrecio_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            mapLista(new List<Zona>());
            //lvlZonas.Items.Clear();
            if (cbxTipoPrecio.SelectedItem != null)
            {
                if (cbxClientes.SelectedItem != null)
                {
                    buscarPrecios();
                }
            }
            else
            {
                mapTabla(new List<decimal>());
                txtPrecioAnterior.valor = 0; 
                txtPrecioNuevo.valor = 0;
            }
        }

        private void buscarPrecios()
        {
            OperationResult resp = new ZonasSvc().getPrecios(((Cliente)cbxClientes.SelectedItem).clave, (eTiposPrecios)cbxTipoPrecio.SelectedItem, ((OrigenPegaso)cbxOrigen.SelectedItem).idOrigen);
            if (resp.typeResult == ResultTypes.success)
            {
                mapTabla((List<decimal>)resp.result);
            }
            else
            {
                mapTabla(new List<decimal>());
            }
        }

        private void mapTabla(List<decimal> result)
        {
            lvlPrecios.Items.Clear();
            foreach (var item in result)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.Selected += Lvl_Selected;
                lvlPrecios.Items.Add(lvl);
            }
        }

        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            //lvlZonas.Items.Clear();
            mapLista(new List<Zona>());
            if (sender != null)
            {
                decimal precioAnterior = ((decimal)((ListViewItem)sender).Content);
                var resp = new ZonasSvc().getZonasByPrecioAndOrigen
                    (
                        ((Cliente)cbxClientes.SelectedItem).clave,
                        (eTiposPrecios)cbxTipoPrecio.SelectedItem,
                        ((OrigenPegaso)cbxOrigen.SelectedItem).idOrigen,
                        precioAnterior
                    );
                if (resp.typeResult == ResultTypes.success)
                {
                    mapLista((List<Zona>)resp.result);
                    txtPrecioAnterior.valor = precioAnterior;
                    txtPrecioNuevo.valor = 0;
                    txtPrecioNuevo.Focus();
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }

            }
        }

        private void mapLista(List<Zona> result)
        {
            stpGranjas.Children.Clear();
            foreach (var item in result)
            {
                //ListViewItem lvl = new ListViewItem();
                //lvl.Content = item;
                //lvlZonas.Items.Add(lvl);

                CheckBox chcGranja = new CheckBox { IsChecked = item.seleccionado, Tag = item };
                chcGranja.Unchecked += ChcGranja_Unchecked;
                chcGranja.Checked += ChcGranja_Checked;
                StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal };
                Label lblGranja = new Label
                {
                    Content = item.descripcion,
                    Width = 248,
                    Foreground = Brushes.SteelBlue,
                    FontSize = 10
                };
                Label lblZona = new Label
                {
                    Content = item.claveZona,
                    Width = 54,
                    Foreground = Brushes.SteelBlue,
                    FontSize = 10,
                    HorizontalContentAlignment = HorizontalAlignment.Center
                };
                stpContent.Children.Add(lblGranja);
                stpContent.Children.Add(lblZona);
                chcGranja.Content = stpContent;
                stpGranjas.Children.Add(chcGranja);
            }
        }

        private void ChcGranja_Checked(object sender, RoutedEventArgs e)
        {
            //try
            //{
            //    var resp = stpGranjas.Children.Cast<CheckBox>().ToList().Find(s => s.IsChecked.Value == false);
            //    if (resp == null)
            //    {
            //        chcTodos.IsChecked = true;
            //    }
            //}
            //catch (Exception ex)
            //{

            //}
        }

        private void ChcGranja_Unchecked(object sender, RoutedEventArgs e)
        {
            //todos = true;
            click = false;
            //chcTodos.IsChecked = false;
        }
        bool click = false;
        private void cbxClientes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbxTipoPrecio.SelectedItem = null;
            cbxOrigen.SelectedItem = null;
            if (cbxClientes.SelectedItem == null)
            {
                cbxOrigen.ItemsSource = null;
            }
            else
            {
                buscarOrigenes(((Cliente)cbxClientes.SelectedItem).clave);
            }
        }
        private void buscarOrigenes(int clave)
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult resp = new ZonasSvc().getOrigenByIdCliente(clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    var lista = (List<OrigenPegaso>)resp.result;
                    cbxOrigen.ItemsSource = lista;
                    if (lista.Count == 1)
                    {
                        cbxOrigen.SelectedIndex = 0;
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        public override OperationResult guardar()
        {
            List<Zona> listZ = new List<Zona>();
            //foreach (ListViewItem item in lvlZonas.Items)
            //{
            //    if (((Zona)item.Content).seleccionado)
            //    {
            //        listZ.Add((Zona)item.Content);
            //    }
            //}
            foreach (var item in stpGranjas.Children.Cast<CheckBox>().ToList())
            {
                if (item.IsChecked.Value)
                {
                    listZ.Add(item.Tag as Zona);
                }
            }
            if (listZ.Count == 0)
            {
                return new OperationResult() { valor = 2, mensaje = "No hay destinos selecionados" };
            }

            bool correcto = false;
            try
            {
                OperationResult resp = new ZonasSvc().saveNuevosPrecios(
                    ((Cliente)cbxClientes.SelectedItem).clave,
                    (eTiposPrecios)cbxTipoPrecio.SelectedItem,
                    Convert.ToDecimal(txtPrecioAnterior.valor),
                    Convert.ToDecimal(txtPrecioNuevo.valor),
                    listZ,
                    mainWindow.inicio._usuario.nombreUsuario);
                //chcTodos.IsChecked = false;
                if (resp.typeResult == ResultTypes.success)
                {
                    //lvlZonas.Items.Clear();
                    buscarPrecios();
                    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                    correcto = true;
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
            finally
            {
                if (correcto)
                {
                    //nuevo();
                }
            }
        }

        private void cbxOrigen_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbxTipoPrecio.Items.Clear();
            //lvlZonas.Items.Clear();
            mapLista(new List<Zona>());
            if (cbxOrigen.SelectedItem != null)
            {
                mapPrecios();
            }
        }

        private void chcTodos_Checked(object sender, RoutedEventArgs e)
        {
            foreach (var item in stpGranjas.Children.Cast<CheckBox>().ToList())
            {
                item.IsChecked = true;
                (item.Tag as Zona).seleccionado = true;
            }
            //foreach (ListViewItem item in lvlZonas.Items)
            //{
            //    ((Zona)item.Content).seleccionado = true;

            //}
        }

        private void chcTodos_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!click)
            {

                return;
            }



            foreach (var item in stpGranjas.Children.Cast<CheckBox>().ToList())
            {
                item.IsChecked = false;
                (item.Tag as Zona).seleccionado = false;
                click = false;
            }
            //foreach (ListViewItem item in lvlZonas.Items)
            //{
            //    ((Zona)item.Content).seleccionado = false;

            //}
        }

        private void ChcTodos_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in stpGranjas.Children.Cast<CheckBox>().ToList())
            {
                item.IsChecked = true;
                (item.Tag as Zona).seleccionado = true;
            }
        }

        private void BtnQuitarTodos_Click(object sender, RoutedEventArgs e)
        {
            foreach (var item in stpGranjas.Children.Cast<CheckBox>().ToList())
            {
                item.IsChecked = false;
                (item.Tag as Zona).seleccionado = false;
            }
        }
    }
}
