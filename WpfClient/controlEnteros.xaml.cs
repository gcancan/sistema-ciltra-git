﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for controlEnteros.xaml
    /// </summary>
    public partial class controlEnteros : UserControl
    {
        public controlEnteros()
        {
            InitializeComponent();
        }
        public bool soloLectura { get; set; }
        public HorizontalAlignment alienacionTexto { get; set; }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            txtEntero.Width = Width - 4;
            txtEntero.IsReadOnly = soloLectura;
            txtEntero.HorizontalContentAlignment = alienacionTexto;
        }

        private void txtEntero_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("[^0-9]+");
            //e.Handled = regex.IsMatch(e.Text);
        }

        public int valor
        {
            get
            {
                if (string.IsNullOrEmpty(txtEntero.Text.Trim()))
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(txtEntero.Text.Trim());
                }
            }
            set
            {
                int val = value;
                txtEntero.Text = val.ToString();
            }
        }

        internal void limpiar()
        {
            txtEntero.Clear();
        }

        private void txtEntero_GotFocus(object sender, RoutedEventArgs e)
        {
            string texto = txtEntero.Text.Trim();
            txtEntero.SelectionStart = 0;
            txtEntero.SelectionLength = texto.Length;
        }
        public void establecerMaxDigitos(int maxDigitos)
        {
            this.txtEntero.MaxLength = maxDigitos;
        }

        private void txtEntero_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key < Key.D0 || e.Key > Key.D9)
            {
                if (e.Key < Key.NumPad0 || e.Key > Key.NumPad9)
                {
                    if (e.Key == Key.OemPeriod || e.Key == Key.Decimal)
                    {
                        //if (((TextBox)sender).Text.Contains("."))
                        //{
                            e.Handled = true;
                        //}
                    }
                    else if (e.Key == Key.Tab)
                    {
                        //e.Handled = false;
                    }
                    else if (e.Key == Key.Enter)
                    {
                        //e.Handled = false;
                    }
                    else
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void txtEntero_TextChanged(object sender, TextChangedEventArgs e)
        {
            var texto = ((TextBox)sender).Text.Trim();
            if (string.IsNullOrEmpty(texto))
            {
                ((TextBox)sender).Text = "0";
                ((TextBox)sender).CaretIndex = ((TextBox)sender).Text.Length;
                return;
            }
            int val = 0;
            if (int.TryParse(texto, out val))
            {
                ((TextBox)sender).Text = val.ToString();
                ((TextBox)sender).CaretIndex = ((TextBox)sender).Text.Length;
                return;
            }
            else
            {
                ((TextBox)sender).Text = "0";
                ((TextBox)sender).CaretIndex = ((TextBox)sender).Text.Length;
                return;
            }
            
        }
    }
}
