﻿using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para SeleccionarLlanta.xaml
    /// </summary>
    public partial class SeleccionarLlanta : Window
    {
        private List<Llanta> listaLlanta;
        public SeleccionarLlanta()
        {
            InitializeComponent();
        }
        public SeleccionarLlanta(List<Llanta> listaLlanta)
        {
            this.listaLlanta = null;
            this.InitializeComponent();
            this.listaLlanta = listaLlanta;
        }

        public Llanta getLlanta()
        {
            try
            {
                if (base.ShowDialog().Value && (this.lvlLlantas.SelectedItem != null))
                {
                    return (Llanta)((ListViewItem)this.lvlLlantas.SelectedItem).Content;
                }
                return null;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return null;
            }
        }
        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            base.DialogResult = true;
        }
        private void mapLista(List<Llanta> result)
        {
            this.lvlLlantas.Items.Clear();
            List<ListViewItem> list = new List<ListViewItem>();
            foreach (Llanta llanta in result)
            {
                ListViewItem item = new ListViewItem
                {
                    Content = llanta
                };
                item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                list.Add(item);
            }
            this.lvlLlantas.ItemsSource = list;
            CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlLlantas.ItemsSource);
            defaultView.Filter = new Predicate<object>(this.UserFilter);
            this.lvlLlantas.Focus();
        }
        private void txtFind_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(this.lvlLlantas.ItemsSource).Refresh();
        }

        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtFind.Text) || ((((((Llanta)(item as ListViewItem).Content).clave.ToString().IndexOf(this.txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0) || (((Llanta)(item as ListViewItem).Content).noSerie.IndexOf(this.txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0)) || (((Llanta)(item as ListViewItem).Content).marca.descripcion.IndexOf(this.txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0)) || (((Llanta)(item as ListViewItem).Content).diseño.ToString().IndexOf(this.txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.listaLlanta != null)
            {
                this.mapLista(this.listaLlanta);
            }
        }
    }
}
