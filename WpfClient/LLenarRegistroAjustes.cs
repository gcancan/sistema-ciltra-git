﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfClient
{
    public static class LLenarRegistroAjustes
    {
        public static List<RegistroAjustes> getAjustesPersonal(Personal personalAnterior, Personal personalNuevo, string usuario)
        {
            try
            {
                List<RegistroAjustes> listaAjustes = new List<RegistroAjustes>();

                if (personalAnterior.empresa.clave != personalNuevo.empresa.clave)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = personalNuevo.clave.ToString(),
                        nombreColumna = "idEmpresa",
                        nombreTabla = "CatPersonal",
                        valorAnterior = personalAnterior.empresa.clave.ToString(),
                        valorNuevo = personalNuevo.empresa.clave.ToString(),
                        usuario = usuario
                    });
                }
                if (personalAnterior.nombres != personalNuevo.nombres)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = personalNuevo.clave.ToString(),
                        nombreColumna = "Nombre",
                        nombreTabla = "CatPersonal",
                        valorAnterior = personalAnterior.nombres,
                        valorNuevo = personalNuevo.nombres,
                        usuario = usuario
                    });
                }
                if (personalAnterior.apellidoPaterno != personalNuevo.apellidoPaterno)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = personalNuevo.clave.ToString(),
                        nombreColumna = "ApellidoPat",
                        nombreTabla = "CatPersonal",
                        valorAnterior = personalAnterior.apellidoPaterno,
                        valorNuevo = personalNuevo.apellidoPaterno,
                        usuario = usuario
                    });
                }
                if (personalAnterior.apellidoMaterno != personalNuevo.apellidoMaterno)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = personalNuevo.clave.ToString(),
                        nombreColumna = "ApellidoMat",
                        nombreTabla = "CatPersonal",
                        valorAnterior = personalAnterior.apellidoMaterno,
                        valorNuevo = personalNuevo.apellidoMaterno,
                        usuario = usuario
                    });
                }
                if ((personalAnterior.puesto == null ? 0 : personalAnterior.puesto.idPuesto) != personalNuevo.puesto.idPuesto)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = personalNuevo.clave.ToString(),
                        nombreColumna = "idPuesto",
                        nombreTabla = "CatPersonal",
                        valorAnterior = personalAnterior.puesto == null ? string.Empty : personalAnterior.puesto.idPuesto.ToString(),
                        valorNuevo = personalNuevo.puesto.idPuesto.ToString(),
                        usuario = usuario
                    });
                }
                if ((personalAnterior.departamento == null ? 0 : personalAnterior.departamento.clave) != personalNuevo.departamento.clave)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = personalNuevo.clave.ToString(),
                        nombreColumna = "idDepto",
                        nombreTabla = "CatPersonal",
                        valorAnterior = personalAnterior.departamento == null ? string.Empty : personalAnterior.departamento.clave.ToString(),
                        valorNuevo = personalNuevo.departamento.clave.ToString(),
                        usuario = usuario
                    });
                }
                if ((personalAnterior.fechaNacimiento == null ? DateTime.Now : personalAnterior.fechaNacimiento) != personalNuevo.fechaNacimiento)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = personalNuevo.clave.ToString(),
                        nombreColumna = "fechaNacimiento",
                        nombreTabla = "CatPersonal",
                        valorAnterior = personalAnterior.fechaNacimiento == null ? string.Empty : personalAnterior.fechaNacimiento.ToString(),
                        valorNuevo = personalNuevo.fechaNacimiento.ToString(),
                        usuario = usuario
                    });
                }
                if ((personalAnterior.fechaIngreso == null ? DateTime.Now : personalAnterior.fechaIngreso) != personalNuevo.fechaIngreso)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = personalNuevo.clave.ToString(),
                        nombreColumna = "fechaIngreso",
                        nombreTabla = "CatPersonal",
                        valorAnterior = personalAnterior.fechaIngreso == null ? string.Empty : personalAnterior.fechaIngreso.ToString(),
                        valorNuevo = personalNuevo.fechaIngreso.ToString(),
                        usuario = usuario
                    });
                }

                if (personalAnterior.estatusBD != personalNuevo.estatusBD)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = personalNuevo.clave.ToString(),
                        nombreColumna = "Estatus",
                        nombreTabla = "CatPersonal",
                        valorAnterior = personalAnterior.estatusBD,
                        valorNuevo = personalNuevo.estatusBD,
                        usuario = usuario
                    });
                }

                if (personalAnterior.direccion != personalNuevo.direccion)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = personalNuevo.clave.ToString(),
                        nombreColumna = "Direccion",
                        nombreTabla = "CatPersonal",
                        valorAnterior = personalAnterior.direccion,
                        valorNuevo = personalNuevo.direccion,
                        usuario = usuario
                    });
                }

                if (personalAnterior.telefono != personalNuevo.telefono)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = personalNuevo.clave.ToString(),
                        nombreColumna = "TelefonoPersonal",
                        nombreTabla = "CatPersonal",
                        valorAnterior = personalAnterior.telefono,
                        valorNuevo = personalNuevo.telefono,
                        usuario = usuario
                    });
                }

                if ((personalAnterior.colonia == null ? 0 : (personalAnterior.colonia == null ? 0 : personalAnterior.colonia.idColonia )) != (personalNuevo.colonia == null ? 0: personalNuevo.colonia.idColonia))
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = personalNuevo.clave.ToString(),
                        nombreColumna = "idColonia",
                        nombreTabla = "CatPersonal",
                        valorAnterior = personalAnterior.colonia == null ? string.Empty : personalAnterior.colonia.idColonia.ToString(),
                        valorNuevo = personalNuevo.departamento.clave.ToString(),
                        usuario = usuario
                    });
                }
                return listaAjustes;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static List<RegistroAjustes> getAjustesUnidadTransporte(UnidadTransporte unidadAnterior, UnidadTransporte unidadNuevo, string usuario)
        {
            try
            {
                List<RegistroAjustes> listaAjustes = new List<RegistroAjustes>();

                if (unidadAnterior.empresa.clave != unidadNuevo.empresa.clave)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = unidadNuevo.clave,
                        nombreColumna = "idEmpresa",
                        nombreTabla = "CatUnidadTrans",
                        valorAnterior = unidadAnterior.empresa.clave.ToString(),
                        valorNuevo = unidadNuevo.empresa.clave.ToString(),
                        usuario = usuario
                    });
                }

                if (unidadAnterior.tipoUnidad.clave != unidadNuevo.tipoUnidad.clave)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = unidadNuevo.clave,
                        nombreColumna = "idTipoUnidad",
                        nombreTabla = "CatUnidadTrans",
                        valorAnterior = unidadAnterior.tipoUnidad.clave.ToString(),
                        valorNuevo = unidadNuevo.tipoUnidad.clave.ToString(),
                        usuario = usuario
                    });
                }

                if ((unidadAnterior.marca == null ? 0 : unidadAnterior.marca.clave) != unidadNuevo.marca.clave)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = unidadNuevo.clave,
                        nombreColumna = "idMarca",
                        nombreTabla = "CatUnidadTrans",
                        valorAnterior = (unidadAnterior.marca == null ? 0 : unidadAnterior.marca.clave).ToString(),
                        valorNuevo = unidadNuevo.marca.clave.ToString(),
                        usuario = usuario
                    });
                }

                if (unidadAnterior.estatus != unidadNuevo.estatus)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = unidadNuevo.clave,
                        nombreColumna = "Estatus",
                        nombreTabla = "CatUnidadTrans",
                        valorAnterior = unidadAnterior.estatus,
                        valorNuevo = unidadNuevo.estatus,
                        usuario = usuario
                    });
                }

                if (unidadAnterior.capacidadTanque != unidadNuevo.capacidadTanque)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = unidadNuevo.clave,
                        nombreColumna = "capacidadTanque",
                        nombreTabla = "CatUnidadTrans",
                        valorAnterior = unidadAnterior.capacidadTanque.ToString(),
                        valorNuevo = unidadNuevo.capacidadTanque.ToString(),
                        usuario = usuario
                    });
                }

                if ((unidadAnterior.fechaAdquisicion == null ? DateTime.Now : unidadAnterior.fechaAdquisicion) != unidadNuevo.fechaAdquisicion)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = unidadNuevo.clave,
                        nombreColumna = "fechaAdquisicion",
                        nombreTabla = "CatUnidadTrans",
                        valorAnterior = (unidadAnterior.fechaAdquisicion == null ? "" : unidadAnterior.fechaAdquisicion.ToString()),
                        valorNuevo = unidadNuevo.fechaAdquisicion.ToString(),
                        usuario = usuario
                    });
                }

                if (unidadAnterior.tipoAdquisicion != unidadNuevo.tipoAdquisicion)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = unidadNuevo.clave,
                        nombreColumna = "tipoAdquisicion",
                        nombreTabla = "CatUnidadTrans",
                        valorAnterior = unidadAnterior.tipoAdquisicion.ToString(),
                        valorNuevo = unidadNuevo.tipoAdquisicion.ToString(),
                        usuario = usuario
                    });
                }

                if (unidadAnterior.odometro != unidadNuevo.odometro)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = unidadNuevo.clave,
                        nombreColumna = "odometro",
                        nombreTabla = "CatUnidadTrans",
                        valorAnterior = unidadAnterior.odometro.ToString(),
                        valorNuevo = unidadNuevo.odometro.ToString(),
                        usuario = usuario
                    });
                }

                if (unidadAnterior.placas != unidadNuevo.placas)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = unidadNuevo.clave,
                        nombreColumna = "placas",
                        nombreTabla = "CatUnidadTrans",
                        valorAnterior = unidadAnterior.placas.ToString(),
                        valorNuevo = unidadNuevo.placas.ToString(),
                        usuario = usuario
                    });
                }

                if (unidadAnterior.documento != unidadNuevo.documento)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = unidadNuevo.clave,
                        nombreColumna = "documento",
                        nombreTabla = "CatUnidadTrans",
                        valorAnterior = unidadAnterior.documento.ToString(),
                        valorNuevo = unidadNuevo.documento.ToString(),
                        usuario = usuario
                    });
                }

                if ((unidadAnterior.proveedor == null ? 0 : unidadAnterior.proveedor.idProveedor) != (unidadNuevo.proveedor == null ? 0 : unidadNuevo.proveedor.idProveedor))
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = unidadNuevo.clave,
                        nombreColumna = "idProveedor",
                        nombreTabla = "CatUnidadTrans",
                        valorAnterior = (unidadAnterior.proveedor == null ? 0 : unidadAnterior.proveedor.idProveedor).ToString(),
                        valorNuevo = unidadNuevo.proveedor.idProveedor.ToString(),
                        usuario = usuario
                    });
                }

                if ((unidadAnterior.VIN == null ? string.Empty: unidadAnterior.VIN) != unidadNuevo.VIN)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = unidadNuevo.clave,
                        nombreColumna = "VIN",
                        nombreTabla = "CatUnidadTrans",
                        valorAnterior = unidadAnterior.VIN == null ? string.Empty : unidadAnterior.VIN,
                        valorNuevo = unidadNuevo.VIN.ToString(),
                        usuario = usuario
                    });
                }
                return listaAjustes;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static List<RegistroAjustes> getAjustesCargaCombustible(CargaCombustible cargaAnterior, CargaCombustible cargaNuevo, string usuario)
        {
            try
            {
                List<RegistroAjustes> listaAjustes = new List<RegistroAjustes>();

                if (cargaAnterior.fechaCombustible != cargaNuevo.fechaCombustible)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = cargaNuevo.idCargaCombustible.ToString(),
                        nombreTabla = "cargaCombustible",
                        nombreColumna = "fechaCombustible",
                        valorAnterior = cargaAnterior.fechaCombustible.ToString(),
                        valorNuevo = cargaNuevo.fechaCombustible.ToString(),
                        usuario = usuario
                    });
                }
                if (cargaAnterior.kmDespachador != cargaNuevo.kmDespachador)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = cargaNuevo.idCargaCombustible.ToString(),
                        nombreTabla = "cargaCombustible",
                        nombreColumna = "kmDespachador",
                        valorAnterior = cargaAnterior.kmDespachador.ToString(),
                        valorNuevo = cargaNuevo.kmDespachador.ToString(),
                        usuario = usuario
                    });
                }
                if (cargaAnterior.ltCombustible != cargaNuevo.ltCombustible)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = cargaNuevo.idCargaCombustible.ToString(),
                        nombreTabla = "cargaCombustible",
                        nombreColumna = "ltCombustible",
                        valorAnterior = cargaAnterior.ltCombustible.ToString(),
                        valorNuevo = cargaNuevo.ltCombustible.ToString(),
                        usuario = usuario
                    });
                }

                if (cargaAnterior.sello1 != cargaNuevo.sello1)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = cargaNuevo.idCargaCombustible.ToString(),
                        nombreTabla = "cargaCombustible",
                        nombreColumna = "sello1",
                        valorAnterior = cargaAnterior.sello1.ToString(),
                        valorNuevo = cargaNuevo.sello1.ToString(),
                        usuario = usuario
                    });
                }

                if (cargaAnterior.sello2 != cargaNuevo.sello2)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = cargaNuevo.idCargaCombustible.ToString(),
                        nombreTabla = "cargaCombustible",
                        nombreColumna = "sello2",
                        valorAnterior = cargaAnterior.sello2.ToString(),
                        valorNuevo = cargaNuevo.sello2.ToString(),
                        usuario = usuario
                    });
                }

                if (cargaAnterior.selloReserva != cargaNuevo.selloReserva)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = cargaNuevo.idCargaCombustible.ToString(),
                        nombreTabla = "cargaCombustible",
                        nombreColumna = "selloRes",
                        valorAnterior = cargaAnterior.selloReserva.ToString(),
                        valorNuevo = cargaNuevo.selloReserva.ToString(),
                        usuario = usuario
                    });
                }
                if (cargaAnterior.kmRecorridos != cargaNuevo.kmRecorridos)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = cargaNuevo.idCargaCombustible.ToString(),
                        nombreTabla = "cargaCombustible",
                        nombreColumna = "kmRecorridos",
                        valorAnterior = cargaAnterior.kmRecorridos.ToString(),
                        valorNuevo = cargaNuevo.kmRecorridos.ToString(),
                        usuario = usuario
                    });
                }
                if (cargaAnterior.ltCombustibleUtilizado != cargaNuevo.ltCombustibleUtilizado)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = cargaNuevo.idCargaCombustible.ToString(),
                        nombreTabla = "cargaCombustible",
                        nombreColumna = "ltCombustibleUtilizado",
                        valorAnterior = cargaAnterior.ltCombustibleUtilizado.ToString(),
                        valorNuevo = cargaNuevo.ltCombustibleUtilizado.ToString(),
                        usuario = usuario
                    });
                }
                if (cargaAnterior.ltCombustiblePTO != cargaNuevo.ltCombustiblePTO)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = cargaNuevo.idCargaCombustible.ToString(),
                        nombreTabla = "cargaCombustible",
                        nombreColumna = "ltCombustiblePTO",
                        valorAnterior = cargaAnterior.ltCombustiblePTO.ToString(),
                        valorNuevo = cargaNuevo.ltCombustiblePTO.ToString(),
                        usuario = usuario
                    });
                }
                if (cargaAnterior.ltCombustibleEST != cargaNuevo.ltCombustibleEST)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = cargaNuevo.idCargaCombustible.ToString(),
                        nombreTabla = "cargaCombustible",
                        nombreColumna = "ltCombustibleEST",
                        valorAnterior = cargaAnterior.ltCombustibleEST.ToString(),
                        valorNuevo = cargaNuevo.ltCombustibleEST.ToString(),
                        usuario = usuario
                    });
                }
                if (cargaAnterior.tiempoTotal != cargaNuevo.tiempoTotal)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = cargaNuevo.idCargaCombustible.ToString(),
                        nombreTabla = "cargaCombustible",
                        nombreColumna = "tiempoTotal",
                        valorAnterior = cargaAnterior.tiempoTotal.ToString(),
                        valorNuevo = cargaNuevo.tiempoTotal.ToString(),
                        usuario = usuario
                    });
                }
                if (cargaAnterior.tiempoPTO != cargaNuevo.tiempoPTO)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = cargaNuevo.idCargaCombustible.ToString(),
                        nombreTabla = "cargaCombustible",
                        nombreColumna = "tiempoPTO",
                        valorAnterior = cargaAnterior.tiempoPTO.ToString(),
                        valorNuevo = cargaNuevo.tiempoPTO.ToString(),
                        usuario = usuario
                    });
                }
                if (cargaAnterior.tiempoEST != cargaNuevo.tiempoEST)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = cargaNuevo.idCargaCombustible.ToString(),
                        nombreTabla = "cargaCombustible",
                        nombreColumna = "tiempoEST",
                        valorAnterior = cargaAnterior.tiempoEST.ToString(),
                        valorNuevo = cargaNuevo.tiempoEST.ToString(),
                        usuario = usuario
                    });
                }

                if (cargaAnterior.ticketExtra != cargaNuevo.ticketExtra)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = cargaNuevo.idCargaCombustible.ToString(),
                        nombreTabla = "cargaCombustible",
                        nombreColumna = "ticketExtra",
                        valorAnterior = cargaAnterior.ticketExtra.ToString(),
                        valorNuevo = cargaNuevo.ticketExtra.ToString(),
                        usuario = usuario
                    });
                }
                if (cargaAnterior.listCombustibleExtra != cargaNuevo.listCombustibleExtra)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = cargaNuevo.idCargaCombustible.ToString(),
                        nombreTabla = "cargaCombustible",
                        nombreColumna = "ltsExtra",
                        valorAnterior = cargaAnterior.listCombustibleExtra.ToString(),
                        valorNuevo = cargaNuevo.listCombustibleExtra.ToString(),
                        usuario = usuario
                    });
                }
                if (cargaAnterior.folioImpresoExtra != cargaNuevo.folioImpresoExtra)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = cargaNuevo.idCargaCombustible.ToString(),
                        nombreTabla = "cargaCombustible",
                        nombreColumna = "folioImpresoExtra",
                        valorAnterior = cargaAnterior.folioImpresoExtra.ToString(),
                        valorNuevo = cargaNuevo.folioImpresoExtra.ToString(),
                        usuario = usuario
                    });
                }
                if (cargaAnterior.proveedorExtra != cargaNuevo.proveedorExtra)
                {
                    listaAjustes.Add(new RegistroAjustes
                    {
                        idTabla = cargaNuevo.idCargaCombustible.ToString(),
                        nombreTabla = "cargaCombustible",
                        nombreColumna = "proveedorExtra",
                        valorAnterior = cargaAnterior.proveedorExtra == null ?string.Empty : cargaAnterior.proveedorExtra.idProveedor.ToString(),
                        valorNuevo = cargaNuevo.proveedorExtra == null ? string.Empty : cargaNuevo.proveedorExtra.idProveedor.ToString(),
                        usuario = usuario
                    });
                }

                return listaAjustes;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
