﻿using Core.Utils;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using Core.Models;
using Core.Interfaces;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using time = System.Timers;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for asignacionRFID.xaml
    /// </summary>
    public partial class asignacionRFID : ViewBase
    {
        private Puerto pRFID_Secundario;
        private Puerto pRFID;
        private time.Timer timer;
        private int contador;

        public string DispString { get; private set; }

        public asignacionRFID()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            pRFID_Secundario = new Puerto();

            var res = pRFID_Secundario.abrirPuerto();
            if (!(bool)res["isOpen"])
            {
                MessageBox.Show(res["mensaje"].ToString(), "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                pRFID_Secundario.RFID.Close();
                mainWindow.scrollContainer.IsEnabled = false;
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                return;
            }
            nuevo();
            pRFID_Secundario.RFID.DataReceived += RFID_DataReceived;
        }
        public override void nuevo()
        {
            mapUnidadTransporte(null);
            mapOperador(null);
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
        }

        private void RFID_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                //timer.Stop();
                pRFID_Secundario.RFID.Close();
                iniciar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void iniciar()
        {
            try
            {
                pRFID = new Puerto();
                timer = new time.Timer { Interval = 1000 };
                Dispatcher.Invoke((Action)(() =>
                {
                    Dictionary<string, object> res = pRFID.abrirPuerto();
                    //pRFID.RFID.Open();
                    contador = new Util().getTiempoEspera();
                    if ((bool)res["isOpen"])
                    {
                        pRFID.RFID.DataReceived += RFID_DataReceived1;

                        timer.Start();
                    }
                    else
                    {
                        MessageBox.Show(res["mensaje"].ToString());
                        //  gripPrincipal.IsEnabled = false;
                        pRFID.RFID.Close();
                    }
                }));
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void RFID_DataReceived1(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {

                contador = new Util().getTiempoEspera();
                DispString = pRFID.RFID.ReadLine();
                pRFID.RFID.Close();
                obtenerRFID(DispString);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void obtenerRFID(string txt)
        {
            try
            {
                txt = txt.Replace("?", " ");
                txt = txt.Replace(")", " ");
                txt = txt.Replace("&", " ");
                txt = txt.Replace("&", " ");
                txt = txt.Replace("*", " ");
                txt = txt.Replace(" ", "");
                txt = txt.Replace("\r", "");
                txt = txt.Replace(@"\u", "");
                txt = txt.Replace(@"\", "");
                txt = txt.Trim();
                txt = Regex.Replace(txt, @"[^\w\.@-]", "",
                                    RegexOptions.None, TimeSpan.FromSeconds(1.5));

                if (txt.Length >= 24)
                {
                    Dispatcher.Invoke((Action)(() =>
                    {

                        ResultadoBusquedaRFID tipo = ResultadoBusquedaRFID.NINGUNO;
                        var resp = new RFIDSvc().findByRFID(txt, ref tipo);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            switch (tipo)
                            {
                                case ResultadoBusquedaRFID.UNIDAD_TRANSPORTE:
                                    mapUnidadTransporte((UnidadTransporte)resp.result);
                                    tabControles.SelectedIndex = 0;
                                    break;
                                case ResultadoBusquedaRFID.OPERADOR:
                                    mapOperador((Personal)resp.result);
                                    tabControles.SelectedIndex = 1;
                                    break;
                                case ResultadoBusquedaRFID.NINGUNO:
                                    mapUnidadTransporte(null);
                                    mapOperador(null);
                                    break;
                                default:
                                    break;
                            }
                        }
                        else
                        {
                            //mapUnidadTransporte(null);
                            //mapOperador(null);
                        }
                        if (tabControles.SelectedIndex == 0)
                        {
                            txtRFIDUnidad.Text = txt;
                        }
                        else
                        {
                            txtRFIDOperador.Text = txt;
                        }
                    }));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                pRFID.RFID.Open();
            }
        }

        private void mapOperador(Personal result)
        {
            if (result != null)
            {
                txtNombreOperador.Tag = result;
                txtClaveOperador.Text = result.clave.ToString();
                txtNombreOperador.Text = result.nombre;
                txtRFIDOperador.Text = result.rfid;
            }
            else
            {
                txtNombreOperador.Tag = null;
                txtClaveOperador.Clear();
                txtNombreOperador.Clear();
                txtRFIDOperador.Clear();
            }
        }

        private void txtClaveUnidad_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                OperationResult resp = new UnidadTransporteSvc().getUnidadesALLorById(0, txtClaveUnidad.Text.Trim());
                if (resp.typeResult == ResultTypes.success)
                {
                    mapUnidadTransporte(((List<UnidadTransporte>)resp.result)[0]);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }                
            }
        }

        private void mapUnidadTransporte(UnidadTransporte result)
        {
            if (result != null)
            {
                txtClaveUnidad.Tag = result;
                txtClaveUnidad.Text = result.clave;
                txtTipoUnidad.Text = result.tipoUnidad.descripcion;
                txtRFIDUnidad.Text = result.rfid;
            }
            else
            {
                txtClaveUnidad.Tag = null;
                txtClaveUnidad.Clear();
                txtTipoUnidad.Clear();
                txtRFIDUnidad.Clear();
            }
        }

        private void txtNombreOperador_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var resp = new OperadorSvc().getOperadoresByNombre(txtNombreOperador.Text.Trim());
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Personal> lisPersonal = (List<Personal>)resp.result;
                    if (lisPersonal.Count == 1)
                    {
                        mapOperador(lisPersonal[0]);
                    }
                    else
                    {
                        selectPersonal select = new selectPersonal(txtNombreOperador.Text, 0);
                        var result = select.buscarPersonal();
                        if (result != null)
                        {
                            mapOperador(result);
                        }
                    }
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    selectPersonal select = new selectPersonal(txtNombreOperador.Text, 0);
                    var result = select.buscarPersonal();
                    if (result != null)
                    {
                        mapOperador(result);
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
        }

        public override OperationResult guardar()
        {
            if (tabControles.SelectedIndex == 0)
            {
                UnidadTransporte unidadTrans = (UnidadTransporte)txtClaveUnidad.Tag;
                OperationResult resp = new RFIDSvc().asignarRFIDUnidades(ref unidadTrans, txtRFIDUnidad.Text);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapUnidadTransporte(unidadTrans);
                }
                return resp;
            }
            else if (tabControles.SelectedIndex == 1)
            {
                Personal personal = (Personal)txtNombreOperador.Tag;
                OperationResult resp = new RFIDSvc().asignarRFIDOperador(ref personal, txtRFIDOperador.Text);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapOperador(personal);
                }
                return resp;
            }
            return base.guardar();
        }
        public override void cerrar()
        {
            if (pRFID_Secundario.RFID.IsOpen)
            {
                pRFID_Secundario.RFID.Close();
            }
            if (pRFID != null)
            {
                if (pRFID.RFID.IsOpen)
                {
                    pRFID.RFID.Close();
                }
                
            }            
            base.cerrar();
        }
    }
}
