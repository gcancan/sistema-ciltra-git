﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
using MahApps.Metro.Controls;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectCorreosElectronicos.xaml
    /// </summary>
    public partial class selectCorreosElectronicos : MetroWindow
    {
        public selectCorreosElectronicos()
        {
            InitializeComponent();
        }
        private string parametro = string.Empty;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtFind.Text = parametro;
            lvlCorreos.Focus();
        }
        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlCorreos.ItemsSource).Refresh();
        }
        public ConfiguracionEnvioCorreo getCorreos()
        {
            try
            {
                buscarAllCorreos();
                bool? resp = ShowDialog();
                if (resp.Value && lvlCorreos.SelectedItem != null)
                {
                    var envioCorreo = ((ConfiguracionEnvioCorreo)((ListViewItem)lvlCorreos.SelectedItem).Content);
                    var respDet = new ConfiguracionEnvioCorreoSvc().getProcesosByIdCorreo(envioCorreo.idConfiguracionEnvioCorreo);
                    if (respDet.typeResult == ResultTypes.success)
                    {
                        envioCorreo.listaProcesosEnvioCorreo = respDet.result as List<enumProcesosEnvioCorreo>;
                    }
                    return envioCorreo;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }
        void buscarAllCorreos()
        {
            var resp = new ConfiguracionEnvioCorreoSvc().getAllCorreosElectronicos();
            if (resp.typeResult == ResultTypes.success)
            {
                mapLista(resp.result as List<ConfiguracionEnvioCorreo>);
            }
            else if (resp.typeResult != ResultTypes.recordNotFound)
            {
                imprimir(resp);
                IsEnabled = false;
            }
        }
        private void mapLista(List<ConfiguracionEnvioCorreo> list)
        {
            lvlCorreos.ItemsSource = null;
            try
            {
                List<ListViewItem> listLvl = new List<ListViewItem>();
                foreach (var item in list)
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = item;
                    lvl.MouseDoubleClick += Lvl_MouseDoubleClick; ;
                    listLvl.Add(lvl);
                }
                lvlCorreos.ItemsSource = listLvl;
                CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlCorreos.ItemsSource);
                view.Filter = UserFilter;
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((ConfiguracionEnvioCorreo)(item as ListViewItem).Content).nomDes.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((ConfiguracionEnvioCorreo)(item as ListViewItem).Content).direccion.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }
        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }
        private void lvlCorreos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }
        private void lvlActividades_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }
    }
}
