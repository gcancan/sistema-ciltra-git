﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Utils;
namespace WpfClient
{
    public class Puerto
    {
        public SerialPort RFID { get; set; }

        public Dictionary<string, object> abrirPuerto()
        {
            Dictionary<string, object> respuesta = new Dictionary<string, object>();
            try
            {
                Util util = new Util();

                RFID = new SerialPort();
                RFID = util.getSerialPort();
                if (!RFID.IsOpen)
                {
                    RFID.Close();
                    RFID.Open();
                }
                else
                {
                    RFID.Close();
                }

                respuesta.Add("isOpen", true);
                respuesta.Add("mensaje", "conexion exitosa");
                return respuesta;
            }
            catch (Exception e)
            {
                respuesta.Add("isOpen", false);
                respuesta.Add("mensaje", "Ocurrio un error al realizar la conexion " + Environment.NewLine + e.Message);
                RFID.Close();
                //abrirPuerto();
                return respuesta;
            }

        }

        public string[] getPuertos()
        {
            try
            {
                return SerialPort.GetPortNames();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
