﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editOrganigrama.xaml
    /// </summary>
    public partial class editOrganigrama : ViewBase
    {
        public editOrganigrama()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
            mapDepartamento(null);
            mapPuesto(null);
        }

        private void BtnNuevoDepto_Click(object sender, RoutedEventArgs e)
        {
            mapDepartamento(null);
        }
       
        private void BtnGuardarDepto_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (string.IsNullOrEmpty(txtNombreDepto.Text.Trim()))
                {
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "SE REQUIERE ESPECIFICAR EL NOMBRE DEL DEPARATAMENTO"));
                    txtNombreDepto.Focus();
                    return;
                }
                Departamento depto = new Departamento
                {
                    clave = ctrClaveDepartamento.Tag == null ? 0 : (ctrClaveDepartamento.Tag as Departamento).clave,
                    activo = chcActivoDepartamento.IsChecked.Value,
                    descripcion = txtNombreDepto.Text.Trim()
                };
                var resp = new DepartamentoSvc().saveDepartamento(ref depto);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapDepartamento(depto);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        void mapDepartamento(Departamento depto)
        {
            if (depto != null)
            {
                ctrClaveDepartamento.valor = depto.clave;
                ctrClaveDepartamento.Tag = depto;
                txtNombreDepto.Text = depto.descripcion;
                chcActivoDepartamento.IsChecked = depto.activo;
                stpControlesDpto.IsEnabled = false;
                btnGuardarDepto.IsEnabled = false;
                btnEditarDepartamento.Visibility = Visibility.Visible;
                btnBuscarDepartamento.Visibility = Visibility.Collapsed;
            }
            else
            {
                ctrClaveDepartamento.Tag = null;
                ctrClaveDepartamento.valor = 0;
                txtNombreDepto.Clear();
                chcActivoDepartamento.IsChecked = true;
                stpControlesDpto.IsEnabled = true;
                btnGuardarDepto.IsEnabled = true;
                btnEditarDepartamento.Visibility = Visibility.Collapsed;
                btnBuscarDepartamento.Visibility = Visibility.Visible;
                btnEditarDepartamento.IsEnabled = true;
            }
        }
        void mapPuesto(Puesto puesto)
        {
            if (puesto != null)
            {
                ctrClavePuesto.valor = puesto.idPuesto;
                ctrClavePuesto.Tag = puesto;
                txtNombrePuesto.Text = puesto.nombrePuesto;
                chcActivarPuesto.IsChecked = puesto.activo;
                stpControlesPuestos.IsEnabled = false;
                btnGuardarPuesto.IsEnabled = false;
                btnEditarPuesto.Visibility = Visibility.Visible;
                btnBuscarPuesto.Visibility = Visibility.Collapsed;
            }
            else
            {
                ctrClavePuesto.Tag = null;
                ctrClavePuesto.valor = 0;
                txtNombrePuesto.Clear();
                chcActivarPuesto.IsChecked = true;
                ctrClavePuesto.IsEnabled = true;
                btnGuardarPuesto.IsEnabled = true;
                btnEditarPuesto.Visibility = Visibility.Collapsed;
                btnBuscarPuesto.Visibility = Visibility.Visible;
                btnEditarPuesto.IsEnabled = true;
            }
        }
        private void BtnGuardarPuesto_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (string.IsNullOrEmpty(txtNombrePuesto.Text.Trim()))
                {
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "SE REQUIERE ESPECIFICAR EL NOMBRE DEL DEPARATAMENTO"));
                    txtNombrePuesto.Focus();
                    return;
                }
                Puesto puesto = new Puesto
                {
                    idPuesto = ctrClavePuesto.Tag == null ? 0 : (ctrClavePuesto.Tag as Puesto).idPuesto,
                    nombrePuesto = txtNombrePuesto.Text.Trim(),
                    activo = chcActivarPuesto.IsChecked.Value
                };
                var resp = new PuestoSvc().savePuesto(ref puesto);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapPuesto(puesto);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void BtnNuevoPuesto_Click(object sender, RoutedEventArgs e)
        {
            mapPuesto(null);
        }        
        private void BtnExportatDeptos_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var resp = new DepartamentoSvc().getALLDepartamentosById();
                if (resp.typeResult == ResultTypes.success)
                {
                    new ReportView().exportarListaDepartamentos(resp.result as List<Departamento>);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void BtnExportarPuestos_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                var resp = new PuestoSvc().getALLPuestosById();
                if (resp.typeResult == ResultTypes.success)
                {
                    new ReportView().exportarListaPuestos(resp.result as List<Puesto>);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            Departamento depto = new selectDepartamento().obtenerDepartamento();
            if (depto != null)
            {
                mapDepartamento(depto);
            }
        }
        private void BtnEditarDepartamento_Click(object sender, RoutedEventArgs e)
        {
            btnEditarDepartamento.IsEnabled = false;
            stpControlesDpto.IsEnabled = true;
            btnGuardarDepto.IsEnabled = true;
        }
        private void BtnBuscarPuesto_Click(object sender, RoutedEventArgs e)
        {
            Puesto puesto = new selectPuestos().obtenerPuesto();
            if (puesto!=null)
            {
                mapPuesto(puesto);
            }
        }
        private void BtnEditarPuesto_Click(object sender, RoutedEventArgs e)
        {
            btnEditarPuesto.IsEnabled = false;
            stpControlesPuestos.IsEnabled = true;
            btnGuardarPuesto.IsEnabled = true;
        }
    }
}
