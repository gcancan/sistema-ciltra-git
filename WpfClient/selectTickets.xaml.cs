﻿using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using static WpfClient.ImprimirMensaje;
using Core.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectTickets.xaml
    /// </summary>
    public partial class selectTickets : Window
    {
        public selectTickets()
        {
            InitializeComponent();
        }

        public Tickets buscarTicket()
        {
            try
            {
                buscarAllTickets();
                bool? res = ShowDialog();
                if (res.Value && lvlTickets.SelectedItem != null)
                {
                    return (Tickets)((ListViewItem)lvlTickets.SelectedItem).Content;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }
        public Tickets buscarTicketByFechas(int? idEmpresa = null)
        {
            try
            {
                this.idEmpresa = idEmpresa;
                bool? res = ShowDialog();
                if (res.Value && lvlTickets.SelectedItem != null)
                {
                    return (Tickets)((ListViewItem)lvlTickets.SelectedItem).Content;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }

        private void buscarAllTickets()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new CargaCombustibleSvc().getTickets();
                if (resp.typeResult == ResultTypes.success)
                {
                    mapLista(resp.result as List<Tickets>);
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void buscarAllTicketsByFechas()
        {
            try
            {
                Cursor = Cursors.Wait;

                OperationResult resp = new OperationResult();
                if (idEmpresa == null)
                {
                    resp = new CargaCombustibleSvc().getTicketsByFechas(ctrFechas.fechaInicial, ctrFechas.fechaFinal);
                }
                else
                {
                    resp = new CargaCombustibleSvc().getTicketsByFechas(ctrFechas.fechaInicial, ctrFechas.fechaFinal, idEmpresa);
                }
                
                if (resp.typeResult == ResultTypes.success)
                {
                    mapLista(resp.result as List<Tickets>);
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapLista(List<Tickets> list)
        {
            lvlTickets.ItemsSource = null;
            List<ListViewItem> lvlList = new List<ListViewItem>();
            foreach (Tickets item in list)
            {

                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick; ;
                lvl.FontWeight = FontWeights.Medium;

                lvlList.Add(lvl);
            }
            lvlTickets.ItemsSource = lvlList;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlTickets.ItemsSource);
            view.Filter = UserFilter;
            lvlTickets.Focus();
        }

        private bool UserFilter(object item)
        {
            if (string.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((Tickets)(item as ListViewItem).Content).fecha.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Tickets)(item as ListViewItem).Content).ticket.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Tickets)(item as ListViewItem).Content).folioImpreso.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Tickets)(item as ListViewItem).Content).ordenServicio.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Tickets)(item as ListViewItem).Content).unidadTransporte.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Tickets)(item as ListViewItem).Content).lts.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlTickets.ItemsSource).Refresh();
        }

        private void btnExportar_Click(object sender, RoutedEventArgs e)
        {
            List<Tickets> listaTickets = new List<Tickets>();
            foreach (var item in lvlTickets.ItemsSource as List<ListViewItem>)
            {
                listaTickets.Add(item.Content as Tickets);
            }
            if (listaTickets.Count > 0)
            {
                new ReportView().exportarTickets(listaTickets);
            }
        }
        int? idEmpresa = null;
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            buscarAllTicketsByFechas();
        }
    }
}
