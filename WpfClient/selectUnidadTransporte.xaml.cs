﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using Core.Interfaces;
using CoreFletera.Models;
using MahApps.Metro.Controls;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for dialogUnidadTransporte.xaml
    /// </summary>
    public partial class selectUnidadTransporte : MetroWindow
    {
        private List<UnidadTransporte> list = new List<UnidadTransporte>();
        private string parametro;
        private int claveEmpresa;
        string find = "";
        private Empresa empresa = new Empresa();
        public selectUnidadTransporte()
        {
            InitializeComponent();
        }

        public selectUnidadTransporte(Empresa empresa)
        {
            InitializeComponent();
            this.claveEmpresa = empresa == null ? 0 : empresa.clave;
            this.empresa = empresa;
        }

        public selectUnidadTransporte(List<UnidadTransporte> list)
        {
            InitializeComponent();
            this.list = list;
        }
        public selectUnidadTransporte(string parametro, int claveEmpresa, string find)
        {
            InitializeComponent();
            this.parametro = parametro;
            this.claveEmpresa = claveEmpresa;
            this.find = find;
        }

        public UnidadTransporte selectUnidadTransporteFind()
        {
            mapUnidades(list);
            bool? dResult = ShowDialog();

            if (dResult.Value && lvlUnidades != null && lvlUnidades.SelectedItem != null)
            {
                return (UnidadTransporte)((ListViewItem)lvlUnidades.SelectedItem).Content;
            }

            return null;

        }

        public UnidadTransporte buscarUnidades()
        {
            buscar();
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlUnidades != null && lvlUnidades.SelectedItem != null)
            {
                return (UnidadTransporte)((ListViewItem)lvlUnidades.SelectedItem).Content;
            }
            return null;
        }
        public UnidadTransporte buscarRemolques()
        {
            buscarR();
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlUnidades != null && lvlUnidades.SelectedItem != null)
            {
                return (UnidadTransporte)((ListViewItem)lvlUnidades.SelectedItem).Content;
            }
            return null;
        }
        ZonaOperativa zonaOperativa = null;
        Cliente cliente = null;
        public UnidadTransporte buscarTractores(ZonaOperativa zonaOperativa = null, Cliente cliente = null)
        {
            this.zonaOperativa = zonaOperativa;
            this.cliente = cliente;
            getTractores();
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlUnidades != null && lvlUnidades.SelectedItem != null)
            {
                return (UnidadTransporte)((ListViewItem)lvlUnidades.SelectedItem).Content;
            }
            return null;
        }
        private void getTractores()
        {
            try
            {
                Cursor = Cursors.Wait;

                OperationResult find = new OperationResult();

                if (zonaOperativa == null)
                    find = new UnidadTransporteSvc().getTractores(this.empresa == null ? 0 : empresa.clave);
                else
                    find = new UnidadTransporteSvc().getTractoresByOperacion(empresa.clave, zonaOperativa.idZonaOperativa, (cliente == null ? 0 : cliente.clave));

                if (find.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.success)
                {
                    lvlUnidades.Items.Clear();
                    List<UnidadTransporte> listUnidades = (List<UnidadTransporte>)find.result;
                    mapUnidades(listUnidades);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        public UnidadTransporte buscarTolvas(ZonaOperativa zonaOperativa = null, Cliente cliente = null)
        {
            this.zonaOperativa = zonaOperativa;
            this.cliente = cliente;
            getTolvas();
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlUnidades != null && lvlUnidades.SelectedItem != null)
            {
                return (UnidadTransporte)((ListViewItem)lvlUnidades.SelectedItem).Content;
            }
            return null;
        }
        private void getTolvas()
        {
            try
            {
                Cursor = Cursors.Wait;

                OperationResult find = new OperationResult();
                if (zonaOperativa == null)
                    find = new UnidadTransporteSvc().getTolvas(this.empresa == null ? 0 : empresa.clave);
                else
                    find = new UnidadTransporteSvc().getRemolquesByOperacion(empresa.clave, zonaOperativa.idZonaOperativa, (cliente == null ? 0 : cliente.clave));

                if (find.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.success)
                {
                    lvlUnidades.Items.Clear();
                    List<UnidadTransporte> listUnidades = (List<UnidadTransporte>)find.result;
                    mapUnidades(listUnidades);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public UnidadTransporte buscarDollys(ZonaOperativa zonaOperativa = null, Cliente cliente = null)
        {
            this.zonaOperativa = zonaOperativa;
            this.cliente = cliente;
            getDollys();
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlUnidades != null && lvlUnidades.SelectedItem != null)
            {
                return (UnidadTransporte)((ListViewItem)lvlUnidades.SelectedItem).Content;
            }
            return null;
        }
        private void getDollys()
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult find = new OperationResult();
                if (zonaOperativa == null)
                    find = new UnidadTransporteSvc().getDollys(this.empresa == null ? 0 : empresa.clave);
                else
                    find = new UnidadTransporteSvc().getDollysByOperacion(empresa.clave, zonaOperativa.idZonaOperativa, (cliente == null ? 0 : cliente.clave));

                if (find.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.success)
                {
                    lvlUnidades.Items.Clear();
                    List<UnidadTransporte> listUnidades = (List<UnidadTransporte>)find.result;
                    mapUnidades(listUnidades);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void buscarR()
        {
            try
            {
                Cursor = Cursors.Wait;
                var find = new UnidadTransporteSvc().buscarRemolques(claveEmpresa, "%" + parametro + "%");

                if (find.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.success)
                {
                    lvlUnidades.Items.Clear();
                    List<UnidadTransporte> listUnidades = (List<UnidadTransporte>)find.result;
                    mapUnidades(listUnidades);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void btnFind_Click(object sender, RoutedEventArgs e)
        {
            buscar();

        }
        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                CollectionViewSource.GetDefaultView(lvlUnidades.ItemsSource).Refresh();
            }
            catch (Exception ex)
            {

            }

        }
        private void buscar()
        {
            try
            {
                Cursor = Cursors.Wait;
                var find = new UnidadTransporteSvc().getUnidadesALLorById(claveEmpresa, "%" + parametro + "%");

                if (find.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.success)
                {
                    lvlUnidades.Items.Clear();
                    List<UnidadTransporte> listUnidades = (List<UnidadTransporte>)find.result;
                    mapUnidades(listUnidades);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }



        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtFind.Text = this.find;
            lvlUnidades.Focus();
        }

        private void mapUnidades(List<UnidadTransporte> list)
        {
            lvlUnidades.Items.Clear();
            List<ListViewItem> lvlList = new List<ListViewItem>();
            foreach (UnidadTransporte item in list)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                lvlList.Add(lvl);
                //lvlUnidades.Items.Add(lvl);
            }
            lvlUnidades.ItemsSource = lvlList;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlUnidades.ItemsSource);
            view.Filter = UserFilter;
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((UnidadTransporte)(item as ListViewItem).Content).clave.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((UnidadTransporte)(item as ListViewItem).Content).empresa.nombre.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((UnidadTransporte)(item as ListViewItem).Content).tipoUnidad.descripcion.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;

        }

        private void Lvl_MouseDoubleClick1(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void lvlUnidades_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            //if (e.Key == Key.Down || e.Key == Key.Up)
            //{
            //    lvlUnidades.Focus();
            //}
            if (e.Key == Key.Escape)
            {
                DialogResult = false;
            }
            if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }

        private void txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                lvlUnidades.Focus();
            }
        }
    }
}
