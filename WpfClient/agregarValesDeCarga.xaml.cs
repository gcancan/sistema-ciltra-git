﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for agregarValesDeCarga.xaml
    /// </summary>
    public partial class agregarValesDeCarga : ViewBase
    {
        public agregarValesDeCarga()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            controlCabezera.DataContextChanged += ControlCabezera_DataContextChanged;
            nuevo();
        }
        public override void nuevo()
        {
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo | ToolbarCommands.guardar);
            controlCabezera.nuevo();
        }

        private void ControlCabezera_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Viaje viaje = controlCabezera.viaje;
            if (viaje != null)
            {
                buscarVales(viaje);
            }
            else
            {
                mapValesCarga(null);
            }
        }

        private void buscarVales(Viaje viaje)
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult respVales = new CartaPorteSvc().getValesCargaByIdViaje(viaje);
                if (respVales.typeResult == ResultTypes.success)
                {
                    mapValesCarga((List<ValeCarga>)respVales.result);
                }
                else
                {
                    ImprimirMensaje.imprimir(respVales);
                    mainWindow.scrollContainer.IsEnabled = false;
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapValesCarga(List<ValeCarga> result)
        {
            lvlValesCarga.Items.Clear();
            if (result != null)
            {
                foreach (var item in result)
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = item;
                    lvlValesCarga.Items.Add(lvl);
                }
            }
        }
        public override OperationResult guardar()
        {
            if (controlCabezera.viaje == null)
            {
                return new OperationResult { valor = 2, mensaje = "SE REQUIERE DE UN VIAJE PARA CONTINUAR" };
            }
            List<ValeCarga> listValesCarga = mapForm();
            if (listValesCarga == null)
            {
                return new OperationResult { valor = 1, mensaje = "ERROR AL QUERER TOMAR VALORES DE LA PANTALLA" };
            }
            if (listValesCarga.Count == 0)
            {
                return new OperationResult { valor = 2, mensaje = "NO HAY VALES PARA GUARDAR" };
            }
            var resp = new ValesCargaSvc().agregarValesCarga(listValesCarga);
            if (resp.typeResult == ResultTypes.success)
            {
                controlCabezera.buscar();
                mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
            }
            return resp;
        }

        private List<ValeCarga> mapForm()
        {
            try
            {
                if (numValesCarga.Value == 0)
                {
                    return new List<ValeCarga>();
                }
                else
                {
                    List<ValeCarga> lista = new List<ValeCarga>();
                    for (int i = 1; i <= numValesCarga.Value; i++)
                    {
                        lista.Add(new ValeCarga
                        {
                            cliente = controlCabezera.viaje.cliente,
                            numGuiaId = controlCabezera.viaje.NumGuiaId
                        });
                    }
                    return lista;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
