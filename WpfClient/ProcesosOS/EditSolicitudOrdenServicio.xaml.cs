﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
using CoreFletera.Interfaces;
using ctr = MahApps.Metro.Controls;
namespace WpfClient.ProcesosOS
{
    /// <summary>
    /// Lógica de interacción para EditSolicitudOrdenServicio.xaml
    /// </summary>
    public partial class EditSolicitudOrdenServicio : ViewBase
    {
        public EditSolicitudOrdenServicio()
        {
            InitializeComponent();
        }
        Usuario usuario = null;
        Empresa empresa = null;
        bool permisoEditar = false;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ctrFolio.KeyDown += CtrFolio_KeyDown;
            this.usuario = mainWindow.usuario;
            var svcPrivilegio = new PrivilegioSvc().consultarPrivilegio("ISADMIN", usuario.idUsuario);

            ctrFolioViaje.txtEntero.KeyDown += TxtEntero_KeyDown;

            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrTractor.DataContextChanged += CtrUnidades_DataContextChanged;
            ctrRemolque.DataContextChanged += CtrUnidades_DataContextChanged;
            ctrDolly.DataContextChanged += CtrUnidades_DataContextChanged;
            ctrRemolque2.DataContextChanged += CtrUnidades_DataContextChanged;

            ctrEmpresa.loaded(this.usuario.empresa, svcPrivilegio.typeResult == ResultTypes.success);
            ctrEmpresa.lbl.Content = "EMPRESA:";

            ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
            ctrZonaOperativa.cargarControl(usuario.zonaOperativa, usuario.idUsuario);
            ctrZonaOperativa.lblZona.Content = "ZONA OPERATIVA:";

            OperationResult resp = new CargaCombustibleSvc().getTipoOrdenCombustibleExtra();
            if (resp.typeResult == ResultTypes.success)
            {
                this.cbxOpcionCombustible.ItemsSource = resp.result as List<TipoOrdenCombustible>;
            }
            nuevo();
            //cbxTipoOrdenServicio.ItemsSource = Enum.GetValues(typeof(enumTipoOrdenServicio));
            cbxTipoOrdenServicio.Items.Add(enumTipoOrden.PREVENTIVO);
            cbxTipoOrdenServicio.Items.Add(enumTipoOrden.CORRECTIVO);
            //cbxTipoOrdenServicio.SelectedIndex = 0;
            establecerImpresora();
            ctrAutoriza.personal = mainWindow.usuario.personal;

            cbxCliente.IsEnabled = (new PrivilegioSvc().consultarPrivilegio("ADMIN_COMBO_CLIENTES", usuario.idUsuario)).typeResult == ResultTypes.success;

            if (new PrivilegioSvc().consultarPrivilegio("EDITRAR_OS_V2", usuario.idUsuario).typeResult == ResultTypes.success)
            {
                permisoEditar = true;
                borderCancelarOS.Visibility = Visibility.Visible;
            }
            else
            {
                permisoEditar = false;
                borderCancelarOS.Visibility = Visibility.Collapsed;
            }
        }
        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ctrEmpresa.empresaSelected != null)
            {
                this.empresa = ctrEmpresa.empresaSelected;
                if (empresa.clave == 1)
                {
                    ctrTractor.loaded(etipoUniadBusqueda.TRACTOR, empresa,
                        (ctrZonaOperativa.zonaOperativa == null ? null : ctrZonaOperativa.zonaOperativa),
                        (cbxCliente.SelectedItem == null ? null : (cbxCliente.SelectedItem as Cliente)));

                    ctrRemolque.loaded(etipoUniadBusqueda.TOLVA, empresa,
                        (ctrZonaOperativa.zonaOperativa == null ? null : ctrZonaOperativa.zonaOperativa),
                        (cbxCliente.SelectedItem == null ? null : (cbxCliente.SelectedItem as Cliente)));

                    ctrDolly.loaded(etipoUniadBusqueda.DOLLY, empresa,
                        (ctrZonaOperativa.zonaOperativa == null ? null : ctrZonaOperativa.zonaOperativa),
                        (cbxCliente.SelectedItem == null ? null : (cbxCliente.SelectedItem as Cliente)));

                    ctrRemolque2.loaded(etipoUniadBusqueda.TOLVA, empresa,
                        (ctrZonaOperativa.zonaOperativa == null ? null : ctrZonaOperativa.zonaOperativa),
                        (cbxCliente.SelectedItem == null ? null : (cbxCliente.SelectedItem as Cliente)));

                    ctrOperador.loaded(eTipoBusquedaPersonal.OPERADORES, (empresa.clave == 5 ? new Empresa { clave = 1 } : empresa));
                    stpOperaciones.Visibility = Visibility.Visible;
                }
                else if (empresa.clave == 5)
                {
                    ctrTractor.loaded(etipoUniadBusqueda.TRACTOR, empresa);
                    ctrRemolque.loaded(etipoUniadBusqueda.TOLVA, empresa);
                    ctrDolly.loaded(etipoUniadBusqueda.DOLLY, empresa);
                    ctrRemolque2.loaded(etipoUniadBusqueda.TOLVA, empresa);

                    ctrOperador.loaded(eTipoBusquedaPersonal.OPERADORES, empresa);

                    var resp = new ClienteSvc().getClientesALLorById(empresa.clave, 0, true);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        cbxCliente.ItemsSource = resp.result as List<Cliente>;
                    }
                    stpOperaciones.Visibility = Visibility.Visible;
                }
                else
                {
                    ctrTractor.loaded(etipoUniadBusqueda.TRACTOR, empresa);
                    ctrRemolque.loaded(etipoUniadBusqueda.TOLVA, empresa);
                    ctrDolly.loaded(etipoUniadBusqueda.DOLLY, empresa);
                    ctrRemolque2.loaded(etipoUniadBusqueda.TOLVA, empresa);
                    ctrOperador.loaded(eTipoBusquedaPersonal.OPERADORES, (empresa.clave == 5 ? new Empresa { clave = 1 } : empresa));
                    stpOperaciones.Visibility = Visibility.Hidden;
                }
            }
        }
        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                Empresa empresa = ctrEmpresa.empresaSelected;
                if (empresa.clave == 1)
                {
                    var resp = new ClienteSvc().getClientesByZonasOperativas(
                    new List<string> { ctrZonaOperativa.zonaOperativa.idZonaOperativa.ToString() }, empresa.clave);

                    if (resp.typeResult == ResultTypes.success)
                    {
                        cbxCliente.ItemsSource = (resp.result as List<OperacionFletera>).Select(s => s.cliente).ToList<Cliente>();
                        cbxCliente.SelectedItem = cbxCliente.ItemsSource.Cast<Cliente>().ToList().Find(s => s.clave == mainWindow.usuario.cliente.clave);
                    }
                    else
                    {
                        cbxCliente.ItemsSource = null;
                    }
                }
                
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void cbxCliente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxCliente.SelectedItem != null)
            {
                Cliente cliente = cbxCliente.SelectedItem as Cliente;

                if (cliente != null)
                {
                    if (ctrEmpresa.empresaSelected.clave == 1)
                    {
                        ZonaOperativa zonaOperativa = ctrZonaOperativa.zonaOperativa;
                        ctrTractor.loaded(etipoUniadBusqueda.TRACTOR, empresa, zonaOperativa, cliente);
                        ctrRemolque.loaded(etipoUniadBusqueda.TOLVA, empresa, zonaOperativa, cliente);
                        ctrDolly.loaded(etipoUniadBusqueda.DOLLY, empresa, zonaOperativa, cliente);
                        ctrRemolque2.loaded(etipoUniadBusqueda.TOLVA, empresa, zonaOperativa, cliente);

                        //ctrOperador.loaded(eTipoBusquedaPersonal.OPERADORES, (empresa.clave == 5 ? new Empresa { clave = 1 } : empresa));
                        ctrOperador.loaded(eTipoBusquedaPersonal.OPERADORES, cliente.clave);
                    }
                }
            }
        }

        bool buscador = false;
        private void CtrFolio_KeyDown(object sender, KeyEventArgs e)
        {
            if (Key.Enter == e.Key)
            {
                if (ctrFolio.valor != 0 && permisoEditar)
                {
                    buscador = true;
                    buscarMasterOrdenServ(ctrFolio.valor);
                }
            }
        }
        private void buscarMasterOrdenServ(int valor)
        {
            try
            {
                nuevo();
                Cursor = Cursors.Wait;
                var resp = new SolicitudOrdenServicioSvc().getMasterOrdenServicioByFolio(valor);
                if (resp.typeResult == ResultTypes.success)
                {
                    limpiarControles();
                    mapForm(resp.result as MasterOrdServ);
                }
                else
                {
                    imprimir(resp);
                    nuevo();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
                buscador = false;
            }
        }
        private bool establecerImpresora()
        {
            try
            {
                return true;
                string str = new Core.Utils.Util().getRutasImpresora("VIAJES_ATLANTE");
                if (string.IsNullOrEmpty(str))
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "Se requiere seleccionar la impresora"
                    };
                    ImprimirMensaje.imprimir(resp);
                    System.Windows.Forms.PrintDialog dialog = new System.Windows.Forms.PrintDialog();
                    if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        new Core.Utils.Util().setRutasImpresora("VIAJES_ATLANTE", dialog.PrinterSettings.PrinterName);
                        return this.establecerImpresora();
                    }
                    return false;
                }
                this.txtNombreImpresora.Text = str;
                return true;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return false;
            }
        }
        public override void nuevo()
        {
            base.nuevo();
            limpiarUnidades();
            stpCargasCombustible.Children.Clear();
            stpActividadesCombustibleExtra.Children.Clear();

            cbxTipoOrdenServicio.SelectedItem = null;

            chcCombustible.IsChecked = false;
            chcLavadero.IsChecked = false;
            chcLlantera.IsChecked = false;
            chcTaller.IsChecked = false;
            chcCombustibleExtra.IsChecked = false;

            chcCombustible.IsEnabled = true;
            chcLavadero.IsEnabled = true;
            chcLlantera.IsEnabled = true;
            chcTaller.IsEnabled = true;
            chcCombustibleExtra.IsEnabled = true;

            cbxServicio.SelectedItem = null;
            txtFallaReportadaTaller.Clear();
            stpServicioTaller.IsEnabled = false;
            stpFallaReportadaTaller.IsEnabled = false;

            listDetOrdenServicioLavadero = new List<DetOrdenServicio>();
            listDetOrdenServicioLlantera = new List<DetOrdenServicio>();
            listDetOrdenServicioTALLER = new List<DetOrdenServicio>();
            listaCargaComExtra = new List<CargaCombustibleExtra>();
            listaCabOrdenServicio = new List<CabOrdenServicio>();
            listaCargaComExtra = new List<CargaCombustibleExtra>();

            stpControlesCabOrdenServ.Children.Clear();

            mapForm(null);
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            ctrFolio.txtEntero.Focus();
        }
        void limpiarUnidades()
        {
            ctrTractor.unidadTransporte = null;
            ctrRemolque.unidadTransporte = null;
            ctrDolly.unidadTransporte = null;
            ctrRemolque2.unidadTransporte = null;

            cbxUnidadesLavadero.Items.Clear();
            cbxUnidadesLlantera.Items.Clear();
            cbxUnidadesTALLER.Items.Clear();
            cbxUnidadesCombustibleExtra.Items.Clear();
        }
        UnidadTransporte tc = null;
        UnidadTransporte tv1 = null;
        UnidadTransporte dl = null;
        UnidadTransporte tV2 = null;
        private void CtrUnidades_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                var ctr = sender as controlUnidadesTransporte;
                if (ctr.unidadTransporte != null)
                {
                    UnidadTransporte unidad = ctr.unidadTransporte;
                    if (unidad.tipoUnidad.bCombustible)
                    {
                        if (ctrFolio.Tag == null)
                        {
                            addOrdenCombustible(unidad);
                        }
                    }

                    if (!cbxUnidadesLavadero.Items.Cast<UnidadTransporte>().ToList().Exists(s => s.clave == unidad.clave))
                    {
                        cbxUnidadesLavadero.Items.Add(unidad);
                    }
                    if (!cbxUnidadesLlantera.Items.Cast<UnidadTransporte>().ToList().Exists(s => s.clave == unidad.clave))
                    {
                        cbxUnidadesLlantera.Items.Add(unidad);
                    }
                    if (!cbxUnidadesTALLER.Items.Cast<UnidadTransporte>().ToList().Exists(s => s.clave == unidad.clave))
                    {
                        cbxUnidadesTALLER.Items.Add(unidad);
                    }
                    if (!cbxUnidadesCombustibleExtra.Items.Cast<UnidadTransporte>().ToList().Exists(s => s.clave == unidad.clave))
                    {
                        cbxUnidadesCombustibleExtra.Items.Add(unidad);
                    }

                    buscarActividadesPendiente(unidad);

                    switch (ctr.Name)
                    {
                        case "ctrTractor":
                            tc = unidad;
                            if (!buscador)
                            {
                                cargarInfoUltimoViaje();
                            }
                            ctrRemolque.txtUnidadTrans.Focus();
                            break;
                        case "ctrRemolque":
                            tv1 = unidad;
                            ctrDolly.txtUnidadTrans.Focus();
                            break;
                        case "ctrDolly":
                            dl = unidad;
                            ctrRemolque2.txtUnidadTrans.Focus();
                            break;
                        case "ctrRemolque2":
                            tV2 = unidad;
                            ctrOperador.txtPersonal.Focus();
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    UnidadTransporte unidad = null;
                    switch (ctr.Name)
                    {
                        case "ctrTractor":
                            unidad = tc;
                            break;
                        case "ctrRemolque":
                            unidad = tv1;
                            break;
                        case "ctrDolly":
                            unidad = dl;
                            break;
                        case "ctrRemolque2":
                            unidad = tV2;
                            break;
                        default:
                            break;
                    }
                    quitarActividadesPendientes(unidad);
                    cbxUnidadesLavadero.Items.Remove(unidad);
                    cbxUnidadesLlantera.Items.Remove(unidad);
                    cbxUnidadesTALLER.Items.Remove(unidad);
                    cbxUnidadesCombustibleExtra.Items.Remove(unidad);
                    removerUnidad(unidad);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void quitarActividadesPendientes(UnidadTransporte unidad)
        {
            try
            {
                foreach (StackPanel stp in stpActividadesPendientes.Children.Cast<StackPanel>().ToList().FindAll(s => (s.Tag as ActividadPendiente).unidad.clave == unidad.clave).ToList())
                {
                    stpActividadesPendientes.Children.Remove(stp);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void buscarActividadesPendiente(UnidadTransporte unidad)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new SolicitudOrdenServicioSvc().getActividadesPendientes(unidad);
                if (resp.typeResult == ResultTypes.success)
                {
                    var listaActPend = resp.result as List<ActividadPendiente>;
                    var lista = stpActividadesPendientes.Children.Cast<StackPanel>().ToList();
                    foreach (StackPanel stp in lista)
                    {
                        ActividadPendiente act = stp.Tag as ActividadPendiente;
                        if (listaActPend.Exists(s => s.idActividadPendiente == act.idActividadPendiente)) return;
                    }

                    foreach (ActividadPendiente actPend in listaActPend)
                    {
                        StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal, Tag = actPend };

                        Label lblUnidad = new Label
                        {
                            Content = actPend.unidad.clave,
                            Width = 60,
                            FontSize = 10,
                            HorizontalContentAlignment = HorizontalAlignment.Center,
                            Foreground = Brushes.SteelBlue,
                            FontWeight = FontWeights.Medium,
                            VerticalAlignment = VerticalAlignment.Center
                        };

                        TextBox txtActividad = new TextBox
                        {
                            Text = actPend.actividad != null ? actPend.actividad.nombre : actPend.servicio.nombre,
                            Width = 255,
                            BorderBrush = null,
                            Background = null,
                            Foreground = Brushes.SteelBlue,
                            FontWeight = FontWeights.Medium,
                            FontSize = 10,
                            VerticalContentAlignment = VerticalAlignment.Center,
                            TextWrapping = TextWrapping.Wrap,
                            IsReadOnly = true
                        };

                        stpContent.Children.Add(lblUnidad);
                        stpContent.Children.Add(txtActividad);
                        stpActividadesPendientes.Children.Add(stpContent);
                    }
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void addOrdenCombustible(UnidadTransporte unidad, bool habilitado = true)
        {
            CheckBox chcUnidad = new CheckBox
            {
                Tag = unidad,
                Content = unidad.clave,
                Foreground = Brushes.SteelBlue,
                FontWeight = FontWeights.Bold,
                IsChecked = chcCombustible.IsChecked.Value,
                IsEnabled = habilitado
            };
            stpCargasCombustible.Children.Add(chcUnidad);
        }
        private void removerUnidad(UnidadTransporte unidad)
        {
            stpCargasCombustible.Children.Remove(stpCargasCombustible.Children.Cast<CheckBox>().ToList().Find(s=>(s.Tag as UnidadTransporte).clave == unidad.clave));
        }
        private void expLavadero_Expanded(object sender, RoutedEventArgs e)
        {
            if (expLavadero.IsExpanded)
            {
                cargarActividadesLavadero();
            }
        }
        bool actividadesLavadero = false;
        void cargarActividadesLavadero()
        {
            if (!actividadesLavadero)
            {
                //var resp = new OrdenesTrabajoSvc().getRelacionActividad(4);
                var resp = new ActividadSvc().getAllActividadesByTipoOrdenServ(enumTipoOrdServ.LAVADERO);
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxActividadLavadero.ItemsSource = (List<Actividad>)resp.result;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
                actividadesLavadero = true;
            }
        }

        List<DetOrdenServicio> listDetOrdenServicioLavadero = new List<DetOrdenServicio>();
        private void btnAddActividadLavadero_Click(object sender, RoutedEventArgs e)
        {
            if (cbxUnidadesLavadero.SelectedItem == null)
            {
                return;
            }
            if (cbxUnidadesLavadero.SelectedItem == null)
            {
                return;
            }
            UnidadTransporte unidad = cbxUnidadesLavadero.SelectedItem as UnidadTransporte;
            if (cbxActividadLavadero.SelectedItem != null)
            {
                Actividad act = cbxActividadLavadero.SelectedItem as Actividad;
                if (act != null)
                {
                    List<DetOrdenServicio> existeDet = new List<DetOrdenServicio>();
                    if (ctrEmpresa.empresaSelected.clave == 2)
                    {
                        existeDet = listDetOrdenServicioLavadero.FindAll(s => s.actividad.idActividad == act.idActividad && s.unidad.clave == unidad.clave);
                    }
                    else
                    {
                        existeDet = listDetOrdenServicioLavadero.FindAll(s => s.unidad.clave == unidad.clave);
                    }
                    
                    if (existeDet.Count == 0)
                    {
                        DetOrdenServicio detOrden = new DetOrdenServicio
                        {
                            unidad = unidad,
                            enumTipoOrdServ = enumTipoOrdServ.LAVADERO,
                            actividad = act,
                            estatus = "PRE",
                            tipoOrdenServicio = null,
                            ordenTaller = new OrdenTaller
                            {
                                actividad = act,
                                estatus = "PRE",
                                servicio = null,
                                costoActividad = act.costo,
                                descripcionOS = act.nombre,
                                descripcionA = act.nombre,
                                clave_llanta = string.Empty
                            },
                            enumTipoOrden = enumTipoOrden.PREVENTIVO,
                            enumTipoServicio = enumTipoServicio.PREVENTIVO,
                            descripcion = act.nombre,
                            llanta = null,
                            pocision = 0
                        };
                        agregarControlesLAVADERO(detOrden);
                        listDetOrdenServicioLavadero.Add(detOrden);
                    }
                }
            }
        }
        private bool agregarControlesLAVADERO(DetOrdenServicio detOrden)
        {
            try
            {
                Cursor = Cursors.Wait;
                StackPanel stpActividad = new StackPanel { Tag = detOrden, Orientation = Orientation.Horizontal };

                Label lblUnidadTransporte = new Label
                {
                    Content = detOrden.unidad.clave,
                    Foreground = Brushes.SteelBlue,
                    Width = 100,
                    FontWeight = FontWeights.Medium,
                    VerticalContentAlignment = VerticalAlignment.Center
                };

                Label lblActividad = new Label
                {
                    Content = detOrden.actividad.nombre,
                    Foreground = Brushes.SteelBlue,
                    Width = 300,
                    FontWeight = FontWeights.Medium,
                    VerticalContentAlignment = VerticalAlignment.Center
                };
                Button btnQuitar = new Button
                {
                    Content = detOrden.idOrdenServicio == 0 ? "BORRAR" : "CANCELAR",
                    Width = 120,
                    Background = detOrden.idOrdenServicio == 0 ? Brushes.OrangeRed : Brushes.Red,
                    Foreground = Brushes.White,
                    Tag = stpActividad
                };
                btnQuitar.Click += BtnQuitar_Click;
                btnQuitar.Visibility = detOrden.estatus == "PRE" ? Visibility.Visible : Visibility.Hidden;
                stpActividad.Children.Add(lblUnidadTransporte);
                stpActividad.Children.Add(lblActividad);
                stpActividad.Children.Add(btnQuitar);

                stpActividadesLavadero.Children.Add(stpActividad);

                return true;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return false;
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void BtnQuitar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                StackPanel stp = btn.Tag as StackPanel;
                DetOrdenServicio detOrden = stp.Tag as DetOrdenServicio;
                if (btn.Content.ToString() == "CANCELAR")
                {
                    if (new cancelarOrden(detOrden.ordenTaller, base.mainWindow.usuario.nombreUsuario).cancelarOrdenTrabajo_v2().Value)
                    {
                        buscarMasterOrdenServ((ctrFolio.Tag as MasterOrdServ).idPadreOrdSer);
                    }
                }
                else
                {
                    listDetOrdenServicioLavadero.Remove(detOrden);
                    stpActividadesLavadero.Children.Remove(stp);
                }


            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        bool actividadesLlantera = false;
        private void expLlantera_Expanded(object sender, RoutedEventArgs e)
        {
            if (expLlantera.IsExpanded)
            {
                cargarActividadesLlantera();
            }
        }
        void cargarActividadesLlantera()
        {
            try
            {
                Cursor = Cursors.Wait;
                if (!actividadesLlantera)
                {
                    //var resp = new OrdenesTrabajoSvc().getRelacionActividad(3);
                    var resp = new ActividadSvc().getAllActividadesByTipoOrdenServ(enumTipoOrdServ.LLANTAS);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        cbxActividadLlantera.ItemsSource = (List<Actividad>)resp.result;
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                    actividadesLlantera = true;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void cbxUnidadesLlantera_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                cbxPosLlanta.Items.Clear();
                chcTodosPosiciones.IsChecked = false;
                cbxChcPosLlantas.Items.Clear();

                if (cbxUnidadesLlantera.SelectedItem == null)
                {
                    return;
                }

                UnidadTransporte unidad = (sender as ComboBox).SelectedItem as UnidadTransporte;
                var resp = new DiagramasLLantasSvc().getDiagramaByTipoUnidadTrans(unidad.tipoUnidad);
                if (resp.typeResult == ResultTypes.success)
                {
                    DiagramaLLantas diagrama = resp.result as DiagramaLLantas;
                    for (int i = 1; i <= diagrama.numLLantas; i++)
                    {
                        cbxPosLlanta.Items.Add(i);
                        cbxChcPosLlantas.Items.Add(i);
                    }
                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void cbxPosLlanta_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                txtLlanta.Clear();
                txtLlanta.Tag = null;
                ComboBox combo = sender as ComboBox;
                if (combo.SelectedItem != null)
                {
                    int posicion = (int)combo.SelectedItem;
                    UnidadTransporte unidad = cbxUnidadesLlantera.SelectedItem as UnidadTransporte;
                    var respLLanta = new LlantaSvc().getLLantaByPosAndUnidadTrans(posicion, unidad);
                    if (respLLanta.typeResult == ResultTypes.success)
                    {
                        Llanta llanta = (Llanta)respLLanta.result;
                        txtLlanta.Tag = llanta;
                        txtLlanta.Text = llanta.clave;
                    }
                    else if (respLLanta.typeResult == ResultTypes.error)
                    {
                        imprimir(respLLanta);
                    }

                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void cbxChcPosLlantas_ItemSelectionChanged(object sender, Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                txtLlanta.Clear();

                if (cbxUnidadesLlantera.SelectedItem == null) return;

                string cadena = string.Empty;
                //foreach (int item in cbxChcPosLlantas.SelectedItems)
                //{
                //    UnidadTransporte unidad = cbxUnidadesLlantera.SelectedItem as UnidadTransporte;
                //    var respLLanta = new LlantaSvc().getLLantaByPosAndUnidadTrans(item, unidad);
                //    if (respLLanta.typeResult == ResultTypes.success)
                //    {
                //        Llanta llanta = (Llanta)respLLanta.result;
                //        //txtLlanta.Tag = llanta;
                //        cadena += llanta.clave + ",";
                //    }
                //    else if (respLLanta.typeResult == ResultTypes.error)
                //    {
                //        imprimir(respLLanta);
                //    }
                //}
                //txtLlanta.Text = cadena.Trim(',');
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        List<DetOrdenServicio> listDetOrdenServicioLlantera = new List<DetOrdenServicio>();
        private void btnAddActividadLlantera_Click(object sender, RoutedEventArgs e)
        {
            if (cbxActividadLlantera.SelectedItem == null)
            {
                return;
            }
            //if (cbxPosLlanta.SelectedItem == null)
            //{
            //    return;
            //}
            if (cbxUnidadesLlantera.SelectedItem == null)
            {
                return;
            }
            if (cbxChcPosLlantas.Items.Count > 0)
            {
                foreach (int item in cbxChcPosLlantas.SelectedItems)
                {
                    UnidadTransporte unidad2 = cbxUnidadesLlantera.SelectedItem as UnidadTransporte;
                    Actividad act = cbxActividadLlantera.SelectedItem as Actividad;
                    if (act != null)
                    {
                        var existeDet = listDetOrdenServicioLlantera.FindAll(s => s.unidad.clave == unidad2.clave
                        && s.actividad.CIDPRODUCTO == act.CIDPRODUCTO
                        && s.pocision == item);
                        if (existeDet.Count == 0)
                        {
                            Llanta llanta = null;
                            var respLLanta = new LlantaSvc().getLLantaByPosAndUnidadTrans(item, unidad2);
                            if (respLLanta.typeResult == ResultTypes.success)
                            {
                                llanta = (Llanta)respLLanta.result;
                            }

                            DetOrdenServicio detOrden = new DetOrdenServicio
                            {
                                unidad = unidad2,
                                enumTipoOrdServ = enumTipoOrdServ.LLANTAS,
                                actividad = act,
                                estatus = "PRE",
                                tipoOrdenServicio = null,
                                ordenTaller = new OrdenTaller
                                {
                                    actividad = act,
                                    estatus = "PRE",
                                    servicio = null,
                                    clave_llanta = txtLlanta.Tag == null ? string.Empty : (txtLlanta.Tag as Llanta).clave,
                                    costoActividad = act.costo,
                                    descripcionA = act.nombre,
                                    descripcionOS = act.nombre
                                },
                                enumTipoOrden = enumTipoOrden.CORRECTIVO,
                                enumTipoServicio = enumTipoServicio.PREVENTIVO,
                                llanta = llanta,
                                pocision = item,
                                descripcion = act.nombre
                            };
                            agregarControlesLLANTERA(detOrden);
                            listDetOrdenServicioLlantera.Add(detOrden);
                        }
                    }
                }
            }
            else
            {
                UnidadTransporte unidad2 = cbxUnidadesLlantera.SelectedItem as UnidadTransporte;
                Actividad act = cbxActividadLlantera.SelectedItem as Actividad;
                if (act != null)
                {
                    var existeDet = listDetOrdenServicioLlantera.FindAll(s => s.unidad.clave == unidad2.clave
                    && s.actividad.CIDPRODUCTO == act.CIDPRODUCTO
                    && s.pocision == 0);
                    if (existeDet.Count == 0)
                    {
                        Llanta llanta = null;
                        //var respLLanta = new LlantaSvc().getLLantaByPosAndUnidadTrans(item, unidad2);
                        //if (respLLanta.typeResult == ResultTypes.success)
                        //{
                        //    llanta = (Llanta)respLLanta.result;
                        //}

                        DetOrdenServicio detOrden = new DetOrdenServicio
                        {
                            unidad = unidad2,
                            enumTipoOrdServ = enumTipoOrdServ.LLANTAS,
                            actividad = act,
                            estatus = "PRE",
                            tipoOrdenServicio = null,
                            ordenTaller = new OrdenTaller
                            {
                                actividad = act,
                                estatus = "PRE",
                                servicio = null,
                                clave_llanta = txtLlanta.Tag == null ? string.Empty : (txtLlanta.Tag as Llanta).clave,
                                costoActividad = act.costo,
                                descripcionA = act.nombre,
                                descripcionOS = act.nombre
                            },
                            enumTipoOrden = enumTipoOrden.CORRECTIVO,
                            enumTipoServicio = enumTipoServicio.PREVENTIVO,
                            llanta = llanta,
                            pocision = 0,
                            descripcion = act.nombre
                        };
                        agregarControlesLLANTERA(detOrden);
                        listDetOrdenServicioLlantera.Add(detOrden);
                    }
                }
            }
            


            return;

            UnidadTransporte unidad = cbxUnidadesLlantera.SelectedItem as UnidadTransporte;
            if (cbxActividadLlantera.SelectedItem != null)
            {
                Actividad act = cbxActividadLlantera.SelectedItem as Actividad;
                if (act != null)
                {
                    var existeDet = listDetOrdenServicioLlantera.FindAll(s => s.unidad.clave == unidad.clave
                    && s.actividad.CIDPRODUCTO == act.CIDPRODUCTO
                    && s.pocision == Convert.ToInt32(cbxPosLlanta.SelectedItem));
                    if (existeDet.Count == 0)
                    {
                        DetOrdenServicio detOrden = new DetOrdenServicio
                        {
                            unidad = unidad,
                            enumTipoOrdServ = enumTipoOrdServ.LLANTAS,
                            actividad = act,
                            estatus = "PRE",
                            tipoOrdenServicio = null,
                            ordenTaller = new OrdenTaller
                            {
                                actividad = act,
                                estatus = "PRE",
                                servicio = null,
                                clave_llanta = txtLlanta.Tag == null ? string.Empty : (txtLlanta.Tag as Llanta).clave,
                                costoActividad = act.costo,
                                descripcionA = act.nombre,
                                descripcionOS = act.nombre
                            },
                            enumTipoOrden = enumTipoOrden.CORRECTIVO,
                            enumTipoServicio = enumTipoServicio.PREVENTIVO,
                            llanta = txtLlanta.Tag == null ? null : txtLlanta.Tag as Llanta,
                            pocision = Convert.ToInt32(cbxPosLlanta.SelectedItem),
                            descripcion = act.nombre
                        };
                        agregarControlesLLANTERA(detOrden);
                        listDetOrdenServicioLlantera.Add(detOrden);
                    }
                }
            }
        }
        private bool agregarControlesLLANTERA(DetOrdenServicio detOrden)
        {
            try
            {
                Cursor = Cursors.Wait;
                StackPanel stpActividad = new StackPanel { Tag = detOrden, Orientation = Orientation.Horizontal };

                Label lblUnidadTransporte = new Label
                {
                    Content = detOrden.unidad.clave,
                    Foreground = Brushes.SteelBlue,
                    Width = 100,
                    FontWeight = FontWeights.Medium,
                    VerticalContentAlignment = VerticalAlignment.Center
                };

                Label lblActividad = new Label
                {
                    Content = detOrden.actividad.nombre,
                    Foreground = Brushes.SteelBlue,
                    Width = 300,
                    FontWeight = FontWeights.Medium,
                    VerticalContentAlignment = VerticalAlignment.Center
                };

                Label lblPosicion = new Label
                {
                    Content = detOrden.pocision,
                    Foreground = Brushes.SteelBlue,
                    Width = 100,
                    FontWeight = FontWeights.Medium,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    HorizontalContentAlignment = HorizontalAlignment.Center
                };

                Label lblLlanta = new Label
                {
                    Content = detOrden.llanta == null ? string.Empty : detOrden.llanta.clave,
                    Foreground = Brushes.SteelBlue,
                    Width = 100,
                    FontWeight = FontWeights.Medium,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    HorizontalContentAlignment = HorizontalAlignment.Center
                };

                Button btnQuitarLlantera = new Button
                {
                    Content = detOrden.idOrdenServicio == 0 ? "BORRAR" : "CANCELAR",
                    Width = 120,
                    Background = detOrden.idOrdenServicio == 0 ? Brushes.OrangeRed : Brushes.Red,
                    Foreground = Brushes.White,
                    Tag = stpActividad
                };
                btnQuitarLlantera.Click += BtnQuitarLlantera_Click;
                btnQuitarLlantera.Visibility = detOrden.estatus == "PRE" ? Visibility.Visible : Visibility.Hidden;

                stpActividad.Children.Add(lblUnidadTransporte);
                stpActividad.Children.Add(lblActividad);
                stpActividad.Children.Add(lblPosicion);
                stpActividad.Children.Add(lblLlanta);
                stpActividad.Children.Add(btnQuitarLlantera);

                stpActividadesLlantera.Children.Add(stpActividad);

                return true;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return false;
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void BtnQuitarLlantera_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                StackPanel stp = btn.Tag as StackPanel;
                DetOrdenServicio detOrden = stp.Tag as DetOrdenServicio;

                if (btn.Content.ToString() == "CANCELAR")
                {
                    if (new cancelarOrden(detOrden.ordenTaller, base.mainWindow.usuario.nombreUsuario).cancelarOrdenTrabajo_v2().Value)
                    {
                        buscarMasterOrdenServ((ctrFolio.Tag as MasterOrdServ).idPadreOrdSer);
                    }
                }
                else
                {
                    listDetOrdenServicioLlantera.Remove(detOrden);
                    stpActividadesLlantera.Children.Remove(stp);
                }

            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public bool serviciosTaller = false;
        private void expTaller_Expanded(object sender, RoutedEventArgs e)
        {
            if (expTaller.IsExpanded)
            {

            }
        }
        public void cargarServiciosTaller()
        {
            if (!serviciosTaller)
            {
                var resp = new OrdenesTrabajoSvc().getRelacionServicios(2);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Servicio> lista = (List<Servicio>)resp.result;
                    cbxServicio.ItemsSource = lista;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
                serviciosTaller = true;
            }
        }
        private void cbxTipoOrdenServicio_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender != null)
            {
                ComboBox combo = sender as ComboBox;
                if (combo.SelectedItem != null)
                {
                    enumTipoOrden enumTipoOrden = (enumTipoOrden)combo.SelectedItem;
                    switch (enumTipoOrden)
                    {
                        case enumTipoOrden.PREVENTIVO:
                            cargarServiciosTaller();
                            txtFallaReportadaTaller.Clear();
                            stpFallaReportadaTaller.IsEnabled = false;
                            stpServicioTaller.IsEnabled = true;
                            break;
                        case enumTipoOrden.CORRECTIVO:
                            cbxServicio.SelectedItem = null;
                            stpServicioTaller.IsEnabled = false;
                            stpFallaReportadaTaller.IsEnabled = true;
                            txtFallaReportadaTaller.Focus();
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    cbxServicio.SelectedItem = null;
                    txtFallaReportadaTaller.Clear();
                    stpServicioTaller.IsEnabled = false;
                    stpFallaReportadaTaller.IsEnabled = false;
                }
            }
        }
        List<DetOrdenServicio> listDetOrdenServicioTALLER = new List<DetOrdenServicio>();
        private void btnAddActividadTALLER_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (cbxTipoOrdenServicio.SelectedItem == null)
                {
                    return;
                }
                if (cbxTipoOrdenServicio.SelectedItem == null)
                {
                    return;
                }
                var tipoOrden = (enumTipoOrden)cbxTipoOrdenServicio.SelectedItem;
                switch (tipoOrden)
                {
                    case enumTipoOrden.CORRECTIVO:
                        if (string.IsNullOrEmpty(txtFallaReportadaTaller.Text.Trim()))
                            return;
                        break;
                    case enumTipoOrden.PREVENTIVO:
                        if (cbxServicio.SelectedItem == null)
                            return;
                        break;
                }
                if (cbxUnidadesTALLER.SelectedItem == null)
                {
                    return;
                }
                UnidadTransporte unidad = cbxUnidadesTALLER.SelectedItem as UnidadTransporte;
                DetOrdenServicio detOrden = new DetOrdenServicio();
                detOrden.enumTipoOrden = tipoOrden;
                switch (tipoOrden)
                {
                    case enumTipoOrden.PREVENTIVO:
                        detOrden = new DetOrdenServicio
                        {
                            unidad = unidad,
                            enumTipoOrdServ = enumTipoOrdServ.TALLER,
                            actividad = null,
                            servicio = cbxServicio.SelectedItem as Servicio,
                            estatus = "PRE",
                            tipoOrdenServicio = null,
                            ordenTaller = new OrdenTaller
                            {
                                actividad = null,
                                estatus = "PRE",
                                servicio = null,
                                clave_llanta = txtLlanta.Tag == null ? string.Empty : (txtLlanta.Tag as Llanta).clave,
                                costoActividad = 0,
                                descripcionA = txtFallaReportadaTaller.Text.Trim(),
                                descripcionOS = txtFallaReportadaTaller.Text.Trim()
                            },
                            enumTipoOrden = enumTipoOrden.PREVENTIVO,
                            enumTipoServicio = enumTipoServicio.PREVENTIVO,
                            llanta = txtLlanta.Tag == null ? null : txtLlanta.Tag as Llanta,
                            pocision = Convert.ToInt32(cbxPosLlanta.SelectedItem),
                            descripcion = (cbxServicio.SelectedItem as Servicio).nombre
                        };
                        agregarControlesTALLER(detOrden);
                        listDetOrdenServicioTALLER.Add(detOrden);
                        break;
                    case enumTipoOrden.CORRECTIVO:
                        detOrden = new DetOrdenServicio
                        {
                            unidad = unidad,
                            enumTipoOrdServ = enumTipoOrdServ.TALLER,
                            actividad = null,
                            servicio = null,
                            estatus = "PRE",
                            tipoOrdenServicio = null,
                            ordenTaller = new OrdenTaller
                            {
                                actividad = null,
                                estatus = "PRE",
                                servicio = null,
                                clave_llanta = txtLlanta.Tag == null ? string.Empty : (txtLlanta.Tag as Llanta).clave,
                                costoActividad = 0,
                                descripcionA = txtFallaReportadaTaller.Text.Trim(),
                                descripcionOS = txtFallaReportadaTaller.Text.Trim()
                            },
                            enumTipoOrden = enumTipoOrden.CORRECTIVO,
                            enumTipoServicio = enumTipoServicio.CORRECTIVO,
                            llanta = txtLlanta.Tag == null ? null : txtLlanta.Tag as Llanta,
                            pocision = Convert.ToInt32(cbxPosLlanta.SelectedItem),
                            descripcion = txtFallaReportadaTaller.Text.Trim()
                        };
                        agregarControlesTALLER(detOrden);
                        listDetOrdenServicioTALLER.Add(detOrden);
                        txtFallaReportadaTaller.Clear();
                        txtFallaReportadaTaller.Focus();
                        break;

                }

            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private bool agregarControlesTALLER(DetOrdenServicio detOrden)
        {
            try
            {
                Cursor = Cursors.Wait;
                //int idOrdenSert = 0;
                //if (listDetOrdenServicioTALLER.Count > 0)
                //{
                //    idOrdenSert = (from s in listDetOrdenServicioTALLER orderby s.idOrdenServicio descending
                //                   where (s.enumTipoOrden == detOrden.enumTipoOrden) && (s.tipoOrdenServicio == detOrden.tipoOrdenServicio) && 
                //                   (s.enumTipoServicio == detOrden.enumTipoServicio) && (s.unidad.clave == detOrden.unidad.clave)
                //                   select s.idOrdenServicio).First();
                //}
                //detOrden.idOrdenServicio = idOrdenSert;
                StackPanel stpActividad = new StackPanel { Tag = detOrden, Orientation = Orientation.Horizontal };

                Label lblUnidadTransporte = new Label
                {
                    Content = detOrden.unidad.clave,
                    Foreground = Brushes.SteelBlue,
                    Width = 100,
                    FontWeight = FontWeights.Medium,
                    VerticalContentAlignment = VerticalAlignment.Center
                };

                Label lblActividad = new Label
                {
                    Content = detOrden.descripcionDetalle,
                    Foreground = Brushes.SteelBlue,
                    Width = 550,
                    FontWeight = FontWeights.Medium,
                    VerticalContentAlignment = VerticalAlignment.Center
                };
                Button btnQuitarTALLER = new Button
                {
                    Content = detOrden.idOrdenServicio == 0 ? "BORRAR" : "CANCELAR",
                    Width = 120,
                    Background = detOrden.idOrdenServicio == 0 ? Brushes.OrangeRed : Brushes.Red,
                    Foreground = Brushes.White,
                    Tag = stpActividad
                };
                btnQuitarTALLER.Click += BtnQuitarTALLER_Click; 
                btnQuitarTALLER.Visibility= detOrden.estatus == "PRE" ? Visibility.Visible : Visibility.Hidden;

                stpActividad.Children.Add(lblUnidadTransporte);
                stpActividad.Children.Add(lblActividad);
                stpActividad.Children.Add(btnQuitarTALLER);

                stpActividadestALLER.Children.Add(stpActividad);

                return true;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return false;
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BtnQuitarTALLER_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                StackPanel stp = btn.Tag as StackPanel;
                DetOrdenServicio detOrden = stp.Tag as DetOrdenServicio;

                if (btn.Content.ToString() == "CANCELAR")
                {
                    if (new cancelarOrden(detOrden.ordenTaller, base.mainWindow.usuario.nombreUsuario).cancelarOrdenTrabajo_v2().Value)
                    {
                        buscarMasterOrdenServ((ctrFolio.Tag as MasterOrdServ).idPadreOrdSer);
                    }
                }
                else
                {
                    listDetOrdenServicioTALLER.Remove(detOrden);
                    stpActividadestALLER.Children.Remove(stp);
                }


            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void chc_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                CheckBox check = sender as CheckBox;
                switch (check.Name)
                {
                    case "chcCombustible":
                        if (check.IsChecked.Value)
                        {
                            expCombustible.IsExpanded = true;
                            stpControlesCombustible.IsEnabled = true;
                            marcarDesmarcarCargasCombustible(true);
                        }
                        else
                        {
                            marcarDesmarcarCargasCombustible(false);
                            expCombustible.IsExpanded = false;
                            stpControlesCombustible.IsEnabled = false;
                        }
                        break;
                    case "chcCombustibleExtra":
                        if (check.IsChecked.Value)
                        {
                            expCombustibleExtra.IsExpanded = true;
                            stpControlesCombustibleExtra.IsEnabled = true;

                        }
                        else
                        {
                            expCombustibleExtra.IsExpanded = false;
                            stpControlesCombustibleExtra.IsEnabled = false;
                            stpActividadesCombustibleExtra.Children.Clear();
                            listaCargaComExtra.Clear();
                            cbxOpcionCombustible.SelectedItem = null;
                        }
                        break;
                    case "chcLavadero":
                        if (check.IsChecked.Value)
                        {
                            expLavadero.IsExpanded = true;
                            stpControlesLvadero.IsEnabled = true;
                        }
                        else
                        {
                            expLavadero.IsExpanded = false;
                            stpControlesLvadero.IsEnabled = false;
                            listDetOrdenServicioLavadero.Clear();
                            stpActividadesLavadero.Children.Clear();
                        }
                        break;
                    case "chcLlantera":
                        if (check.IsChecked.Value)
                        {
                            expLlantera.IsExpanded = true;
                            stpControlesLlantera.IsEnabled = true;
                        }
                        else
                        {
                            expLlantera.IsExpanded = false;
                            stpControlesLlantera.IsEnabled = false;
                            listDetOrdenServicioLlantera.Clear();
                            stpActividadesLlantera.Children.Clear();
                        }
                        break;
                    case "chcTaller":
                        if (check.IsChecked.Value)
                        {
                            expTaller.IsExpanded = true;
                            stpControlesTaller.IsEnabled = true;
                        }
                        else
                        {
                            expTaller.IsExpanded = false;
                            stpControlesTaller.IsEnabled = false;
                            listDetOrdenServicioTALLER.Clear();
                            stpActividadestALLER.Children.Clear();
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        void marcarDesmarcarCargasCombustible(bool chec)
        {
            try
            {
                List<CheckBox> listCheck = stpCargasCombustible.Children.Cast<CheckBox>().ToList();
                foreach (var item in listCheck)
                {
                    item.IsChecked = chec;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;

                if (ctrOperador.personal == null)
                {
                    ctrOperador.txtPersonal.Focus();
                    return new OperationResult(ResultTypes.warning, "SELECCIONA UN OPERADOR");
                }

                MasterOrdServ masterOrdServ = mapForm();
                if (masterOrdServ == null)
                {
                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LA INFORMACIÓN DE LA PANTALLA.");
                }
                if (masterOrdServ.listaCabOrdenServicio.Count == 0)
                {
                    return new OperationResult(ResultTypes.warning, "NO HAY ORDENES DE SERVICIO PARA GUARDAR.");
                }
                var l = new List<CargaCombustibleExtra>();
                var resp = new SolicitudOrdenServicioSvc().saveSolicitudOrdenServicio(ref masterOrdServ, ref l, true);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(masterOrdServ);
                    buscarMasterOrdenServ(masterOrdServ.idPadreOrdSer);
                    new ventanaMasterOrdenServicio(masterOrdServ, txtNombreImpresora.Text.Trim(), l).ShowDialog();
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                }
                return resp;

                //return base.guardar();
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapForm(MasterOrdServ masterOrdServ)
        {
            chcCombustible.IsEnabled = true;
            chcLavadero.IsEnabled = true;
            chcLlantera.IsEnabled = true;
            chcTaller.IsEnabled = true;
            chcCombustibleExtra.IsEnabled = true;
            try
            {
                chcCombustible.IsChecked = true;
                chcLavadero.IsChecked = true;
                chcLlantera.IsChecked = true;
                chcTaller.IsChecked = true;
                chcCombustibleExtra.IsChecked = true;

                chcCombustible.IsChecked = false;
                chcLavadero.IsChecked = false;
                chcLlantera.IsChecked = false;
                chcTaller.IsChecked = false;
                chcCombustibleExtra.IsChecked = false;

                stpActividadesPendientes.Children.Clear();
                //ctrTractor.unidadTransporte = null;
                //ctrRemolque.unidadTransporte = null;
                //ctrDolly.unidadTransporte = null;
                //ctrRemolque2.unidadTransporte = null;
                //stpActividadesPendientes.Children.Clear();

                if (masterOrdServ != null)
                {
                    gridCaptura.IsEnabled = false;
                    buscador = false;
                    if (masterOrdServ.empresa != null)
                    {
                        ctrEmpresa.cbxEmpresa.SelectedItem = ctrEmpresa.cbxEmpresa.Items.Cast<Empresa>().ToList().Find(s => s.clave == masterOrdServ.empresa.clave);
                    }
                    if (masterOrdServ.zonaOperativa != null)
                    {
                        ctrZonaOperativa.zonaOperativa = masterOrdServ.zonaOperativa;
                    }
                    if (masterOrdServ.cliente != null)
                    {
                        cbxCliente.SelectedItem = cbxCliente.Items.Cast<Cliente>().ToList().Find(s => s.clave == masterOrdServ.cliente.clave);
                    }

                    ctrFolio.Tag = masterOrdServ;
                    ctrFolio.IsEnabled = false;
                    ctrFolio.valor = masterOrdServ.idPadreOrdSer;

                    buscador = true;
                    ctrTractor.unidadTransporte = masterOrdServ.tractor;
                    ctrRemolque.unidadTransporte = masterOrdServ.tolva1;
                    ctrDolly.unidadTransporte = masterOrdServ.dolly;
                    ctrRemolque2.unidadTransporte = masterOrdServ.tolva2;

                    txtFecha.Text = masterOrdServ.fechaCreacion.ToString("dd/MM/yyyy HH:mm");
                    ctrOperador.personal = masterOrdServ.chofer;

                    var respPersonal = new OperadorSvc().getPersonalALLorById(masterOrdServ.listaCabOrdenServicio[0].idEmpleadoAutoriza);
                    if (respPersonal.typeResult == ResultTypes.success)
                    {
                        ctrAutoriza.personal = (respPersonal.result as List<Personal>)[0];
                    }

                    listDetOrdenServicioLavadero = new List<DetOrdenServicio>();
                    stpActividadesLavadero.Children.Clear();
                    listDetOrdenServicioLlantera = new List<DetOrdenServicio>();
                    stpActividadesLlantera.Children.Clear();
                    listDetOrdenServicioTALLER = new List<DetOrdenServicio>();
                    stpActividadestALLER.Children.Clear();
                    stpControlesCabOrdenServ.Children.Clear();
                    listaCabOrdenServicio.Clear();

                    foreach (var cabOrdenes in masterOrdServ.listaCabOrdenServicio)
                    {
                        agregarControlesCabOrdenServ(cabOrdenes);
                        switch (cabOrdenes.enumTipoOrdServ)
                        {
                            case enumTipoOrdServ.COMBUSTIBLE:
                                chcCombustible.IsChecked = true;
                                chcCombustible.IsEnabled = false;
                                addOrdenCombustible(cabOrdenes.unidad, false);
                                break;
                            case enumTipoOrdServ.COMBUSTIBLE_EXTRA:
                                chcCombustibleExtra.IsChecked = true;
                                chcCombustibleExtra.IsEnabled = false;
                                cabOrdenes.cargaCombustibleExtra.idOrdenServ = cabOrdenes.idOrdenSer;
                                cabOrdenes.cargaCombustibleExtra.detOrden = cabOrdenes.listaDetOrdenServicio[0];
                                agregarControlesCombustibleEXTRA(cabOrdenes.cargaCombustibleExtra);
                                listaCargaComExtra.Add(cabOrdenes.cargaCombustibleExtra);
                                break;
                            case enumTipoOrdServ.TALLER:
                                chcTaller.IsChecked = true;
                                chcTaller.IsEnabled = false;
                                foreach (var detOrden in cabOrdenes.listaDetOrdenServicio.FindAll(s => s.idActividadPendiente == 0))
                                {
                                    agregarControlesTALLER(detOrden);
                                    listDetOrdenServicioTALLER.Add(detOrden);
                                }
                                break;
                            case enumTipoOrdServ.LLANTAS:
                                chcLlantera.IsChecked = true;
                                chcLlantera.IsEnabled = false;
                                foreach (var detOrden in cabOrdenes.listaDetOrdenServicio.FindAll(s => s.idActividadPendiente == 0))
                                {
                                    agregarControlesLLANTERA(detOrden);
                                    listDetOrdenServicioLlantera.Add(detOrden);
                                }
                                break;
                            case enumTipoOrdServ.LAVADERO:
                                chcLavadero.IsChecked = true;
                                chcLavadero.IsEnabled = false;
                                foreach (var detOrden in cabOrdenes.listaDetOrdenServicio.FindAll(s => s.idActividadPendiente == 0))
                                {
                                    agregarControlesLAVADERO(detOrden);
                                    listDetOrdenServicioLavadero.Add(detOrden);
                                }
                                break;
                            case enumTipoOrdServ.RESGUARDO:
                                break;
                            default:
                                break;
                        }
                    }
                    ctrKm.valor = masterOrdServ.km;
                }
                else
                {
                    gridCaptura.IsEnabled = true;
                    ctrFolio.Tag = null;
                    ctrFolio.IsEnabled = true;
                    ctrFolio.valor = 0;
                    txtFecha.Clear();
                    ctrOperador.limpiar();
                    stpActividadesPendientes.Children.Clear();
                    ctrKm.valor = 0;
                    //ctrAutoriza.limpiar();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        public MasterOrdServ mapForm()
        {
            try
            {
                MasterOrdServ masterOrdServ = new MasterOrdServ
                {
                    idPadreOrdSer = ctrFolio.Tag == null ? 0 : (ctrFolio.Tag as MasterOrdServ).idPadreOrdSer,
                    empresa = ctrEmpresa.empresaSelected,
                    zonaOperativa = ctrEmpresa.empresaSelected.clave == 2 ? null : ctrZonaOperativa.zonaOperativa,
                    cliente = ctrEmpresa.empresaSelected.clave == 2 ? null : cbxCliente.SelectedItem as Cliente,
                    tractor = ctrTractor.unidadTransporte,
                    tolva1 = ctrRemolque.unidadTransporte == null ? null : ctrRemolque.unidadTransporte,
                    dolly = ctrDolly.unidadTransporte == null ? null : ctrDolly.unidadTransporte,
                    tolva2 = ctrRemolque2.unidadTransporte == null ? null : ctrRemolque2.unidadTransporte,
                    chofer = ctrOperador.personal,
                    fechaCreacion = DateTime.Now,
                    userCrea = mainWindow.usuario.nombreUsuario,
                    fechaModificacion = null,
                    userModifica = string.Empty,
                    fechaCancela = null,
                    userCancela = string.Empty,
                    estatus = "CAP",
                    motivoCancela = string.Empty,
                    km = ctrKm.valor
                };

                masterOrdServ.listaCabOrdenServicio = getListaCabOrdenServicio();

                return masterOrdServ;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private List<CabOrdenServicio> getListaCabOrdenServicio()
        {
            List<CabOrdenServicio> listaCabOrdenServicio = new List<CabOrdenServicio>();
            List<DetOrdenServicio> listaDetOrdenServicio = new List<DetOrdenServicio>();

            if (ctrFolio.Tag == null)
            {

            }

            foreach (Control item in stpCargasCombustible.Children)
            {
                if (item is CheckBox)
                {
                    CheckBox chc = item as CheckBox;
                    if (chc.IsChecked.Value && chc.IsEnabled == true)
                    {
                        listaDetOrdenServicio.Add(new DetOrdenServicio
                        {
                            enumTipoOrden = enumTipoOrden.PREVENTIVO,
                            enumTipoOrdServ = enumTipoOrdServ.COMBUSTIBLE,
                            enumTipoServicio = enumTipoServicio.RECARGA,
                            unidad = chc.Tag as UnidadTransporte,
                            servicio = new Servicio
                            {
                                idServicio = 3,
                                nombre = "CARGA DE COMBUSTIBLE",
                                descipcion = "CARGA DE COMBUSTIBLE"
                            },
                            actividad = null,
                            estatus = "PRE",
                            ordenTaller = new OrdenTaller
                            {
                                servicio = new Servicio
                                {
                                    idServicio = 3,
                                    nombre = "CARGA DE COMBUSTIBLE",
                                    descipcion = "CARGA DE COMBUSTIBLE"
                                },
                                actividad = null,
                                estatus = "PRE",
                                descripcionA = "CARGA DE COMBUSTIBLE",
                                descripcionOS = "CARGA DE COMBUSTIBLE"
                            }
                        });
                    }
                }
            }

            foreach (var cargaExtra in listaCargaComExtra)
            {
                listaDetOrdenServicio.Add(new DetOrdenServicio
                {
                    enumTipoOrden = enumTipoOrden.PREVENTIVO,
                    enumTipoOrdServ = enumTipoOrdServ.COMBUSTIBLE_EXTRA,
                    enumTipoServicio = enumTipoServicio.RECARGA,
                    unidad = cargaExtra.unidadTransporte,
                    servicio = new Servicio
                    {
                        idServicio = 15,
                        nombre = "CARGA DE COMBUSTIBLE EXTRA",
                        descipcion = "CARGA DE COMBUSTIBLE EXTRA"
                    },
                    actividad = null,
                    estatus = "PRE",
                    ordenTaller = new OrdenTaller
                    {
                        servicio = new Servicio
                        {
                            idServicio = 15,
                            nombre = "CARGA DE COMBUSTIBLE EXTRA",
                            descipcion = "CARGA DE COMBUSTIBLE EXTRA"
                        },
                        actividad = null,
                        estatus = "PRE",
                        descripcionA = "CARGA DE COMBUSTIBLE EXTRA",
                        descripcionOS = "CARGA DE COMBUSTIBLE EXTRA"
                    },
                    cargaCombustibleExtra = cargaExtra,
                    idOrdenServicio = cargaExtra.idOrdenServ,
                    idOrdenTrabajo = cargaExtra.detOrden == null ? 0 : cargaExtra.detOrden.idOrdenTrabajo,
                    idOrdenActividad = cargaExtra.detOrden == null ? 0 : cargaExtra.detOrden.idOrdenActividad

                });
            }

            foreach (var item in listDetOrdenServicioLavadero)
            {
                listaDetOrdenServicio.Add(item);
            }
            foreach (var item in listDetOrdenServicioLlantera)
            {
                listaDetOrdenServicio.Add(item);
            }
            foreach (var item in listDetOrdenServicioTALLER)
            {
                listaDetOrdenServicio.Add(item);
            }

            if (ctrFolio.Tag == null)
            {
                if (chcLavadero.IsChecked.Value)
                {
                    foreach (var item in convertirActividadesPendientesEnDetOrdenServicio(enumTipoOrdServ.LAVADERO))
                    {
                        listaDetOrdenServicio.Add(item);
                    }
                }
                if (chcLlantera.IsChecked.Value)
                {
                    foreach (var item in convertirActividadesPendientesEnDetOrdenServicio(enumTipoOrdServ.LLANTAS))
                    {
                        listaDetOrdenServicio.Add(item);
                    }
                }
                if (chcTaller.IsChecked.Value)
                {
                    foreach (var item in convertirActividadesPendientesEnDetOrdenServicio(enumTipoOrdServ.TALLER))
                    {
                        listaDetOrdenServicio.Add(item);
                    }
                }
            }

            listaCabOrdenServicio = (from det in listaDetOrdenServicio
                                     group det by new { det.enumTipoOrden, det.enumTipoOrdServ, det.enumTipoServicio, det.unidad.clave, det.cargaCombustibleExtra } into grupoDetalle
                                     select new CabOrdenServicio
                                     {
                                         idOrdenSer = (from s in grupoDetalle orderby s.idOrdenServicio descending select s.idOrdenServicio).First(),
                                         empresa = ctrEmpresa.empresaSelected,
                                         zonaOperativa = ctrEmpresa.empresaSelected.clave == 2 ? null : ctrZonaOperativa.zonaOperativa,
                                         cliente = ctrEmpresa.empresaSelected.clave == 2 ? null : cbxCliente.SelectedItem as Cliente,
                                         estatus = "PRE",
                                         externo = false,
                                         fechaAsignado = null,
                                         fechaCancela = null,
                                         fechaCaptura = DateTime.Now,
                                         fechaDiagnostico = null,
                                         fechaEntregado = null,
                                         fechaRecepcion = null,
                                         fechaTerminado = null,
                                         idEmpleadoAutoriza = ctrAutoriza.personal.clave,
                                         cveEmpleadoAutoriza = ctrAutoriza.personal.cveAgente == null ? string.Empty : ctrAutoriza.personal.cveAgente,
                                         cveUEN = null,
                                         cveSucCliente = null,
                                         idEmpleadoEntrega = 0,
                                         idEmpleadoRecibe = 0,
                                         km = "0",
                                         notaFinal = string.Empty,
                                         enumTipoOrden = (from s in grupoDetalle select s.enumTipoOrden).First(),
                                         enumtipoServicio = (from s in grupoDetalle select s.enumTipoServicio).First(),
                                         enumTipoOrdServ = (from s in grupoDetalle select s.enumTipoOrdServ).First(),
                                         unidad = (from s in grupoDetalle select s.unidad).First(),
                                         usuarioAsigna = string.Empty,
                                         usuarioCancela = string.Empty,
                                         usuarioEntrega = string.Empty,
                                         usuarioRecepcion = string.Empty,
                                         usuarioTermina = string.Empty,
                                         listaDetOrdenServicio = grupoDetalle.ToList<DetOrdenServicio>(),
                                         cargaCombustibleExtra = (from s in grupoDetalle select s.cargaCombustibleExtra).First()
                                     }).ToList<CabOrdenServicio>();

            return listaCabOrdenServicio;
        }
        bool requiereViaje = false;
        private void cbxOpcionCombustible_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender != null)
            {
                ComboBox combo = sender as ComboBox;
                if (combo.SelectedItem != null)
                {
                    TipoOrdenCombustible tipoOrden = combo.SelectedItem as TipoOrdenCombustible;
                    if (tipoOrden.requiereViaje)
                    {
                        ctrFolioViaje.IsEnabled = true;
                        requiereViaje = true;
                    }
                    else
                    {
                        ctrFolioViaje.limpiar();
                        ctrFolioViaje.IsEnabled = false;
                        requiereViaje = false;
                    }
                }
                else
                {
                    ctrFolioViaje.limpiar();
                    ctrFolioViaje.IsEnabled = false;
                    requiereViaje = false;
                }
            }
            else
            {
                ctrFolioViaje.limpiar();
                ctrFolioViaje.IsEnabled = false;
                requiereViaje = false;
            }
        }

        List<DetOrdenServicio> convertirActividadesPendientesEnDetOrdenServicio(enumTipoOrdServ enumTipo)
        {
            List<DetOrdenServicio> listaDetOrdenServicio = new List<DetOrdenServicio>();
            foreach (ActividadPendiente actPend in (stpActividadesPendientes.Children.Cast<StackPanel>().ToList().
                Select(s => s.Tag as ActividadPendiente)).ToList().FindAll(s => s.enumTipoOrdServ == enumTipo))
            {
                listaDetOrdenServicio.Add(new DetOrdenServicio
                {
                    idActividadPendiente = actPend.idActividadPendiente,
                    unidad = actPend.unidad,
                    enumTipoOrdServ = actPend.enumTipoOrdServ,
                    actividad = actPend.actividad,
                    servicio = actPend.servicio,
                    estatus = "PRE",
                    tipoOrdenServicio = null,
                    ordenTaller = new OrdenTaller
                    {
                        actividad = actPend.actividad,
                        estatus = "PRE",
                        servicio = actPend.servicio,
                        costoActividad = 0,
                        descripcionOS = actPend.actividad != null ? actPend.actividad.nombre : actPend.servicio.nombre,
                        descripcionA = actPend.actividad != null ? actPend.actividad.nombre : actPend.servicio.nombre,
                        clave_llanta = string.Empty
                    },
                    enumTipoOrden = actPend.enumTipoOrden,
                    enumTipoServicio = actPend.enumTipoServicio,
                    descripcion = actPend.actividad != null ? actPend.actividad.nombre : actPend.servicio.nombre,
                    llanta = null,
                    pocision = actPend.posicion

                });

            }
            return listaDetOrdenServicio;
        }

        private void btnCambiarImpreso_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.PrintDialog dialog = new System.Windows.Forms.PrintDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                new Core.Utils.Util().setRutasImpresora("VIAJES_ATLANTE", dialog.PrinterSettings.PrinterName);
                this.establecerImpresora();
            }
        }
        List<CargaCombustibleExtra> listaCargaComExtra = new List<CargaCombustibleExtra>();
        private void btnAgregarCombustibleExtra_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cbxUnidadesCombustibleExtra.SelectedItem == null)
                {
                    return;
                }
                if (cbxOpcionCombustible.SelectedItem == null)
                {
                    return;
                }
                if (ctrOperador.personal == null)
                {
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "SE REQUIERE ELEGIR AL CHOFER."));
                    return;
                }
                if (ctrAutoriza.personal == null)
                {
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "SE REQUIERE ELEGIR AL PERSONAL QUE AUTORIZA."));
                    return;
                }
                if (requiereViaje)
                {
                    if (ctrFolioViaje.Tag == null)
                    {
                        imprimir(new OperationResult(ResultTypes.warning, "ESTE TIPO DE ORDEN DE COMBUSTIBLE EXTRA REQUIERE UN VIAJE ACTIVO."));
                        return;
                    }
                }
                if (!listaCargaComExtra.Exists(s => s.unidadTransporte.clave == (cbxUnidadesCombustibleExtra.SelectedItem as UnidadTransporte).clave
                && s.tipoOrdenCombustible.idTipoOrdenCombustible == (this.cbxOpcionCombustible.SelectedItem as TipoOrdenCombustible).idTipoOrdenCombustible))
                {
                    CargaCombustibleExtra cargaCombustibleExtra = new CargaCombustibleExtra
                    {
                        idCargaCombustibleExtra = 0,
                        externo = false,
                        chofer = ctrOperador.personal,
                        despachador = null,
                        ltCombustible = ctrLitros.valor,
                        fecha = DateTime.Now,
                        userCrea = mainWindow.usuario.nombreUsuario,
                        fechaCombustible = null,
                        unidadTransporte = cbxUnidadesCombustibleExtra.SelectedItem as UnidadTransporte,
                        tipoOrdenCombustible = this.cbxOpcionCombustible.SelectedItem as TipoOrdenCombustible,
                        idEmpleadoAutoriza = ctrAutoriza.personal.clave,
                        numViaje = ctrFolioViaje.Tag == null ? null : (int?)(ctrFolioViaje.Tag as Viaje).NumGuiaId
                    };
                    listaCargaComExtra.Add(cargaCombustibleExtra);
                    agregarControlesCombustibleEXTRA(cargaCombustibleExtra);

                    ctrLitros.valor = 0;
                }

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private bool agregarControlesCombustibleEXTRA(CargaCombustibleExtra carga)
        {
            try
            {
                Cursor = Cursors.Wait;
                StackPanel stpActividad = new StackPanel { Tag = carga, Orientation = Orientation.Horizontal };

                Label lblUnidadTransporte = new Label
                {
                    Content = carga.unidadTransporte.clave,
                    Foreground = Brushes.SteelBlue,
                    Width = 100,
                    FontWeight = FontWeights.Medium,
                    VerticalContentAlignment = VerticalAlignment.Center
                };

                Label lblActividad = new Label
                {
                    Content = carga.tipoOrdenCombustible.nombreTipoOrdenCombustible,
                    Foreground = Brushes.SteelBlue,
                    Width = 280,
                    FontWeight = FontWeights.Medium,
                    VerticalContentAlignment = VerticalAlignment.Center
                };

                Label lblLitros = new Label
                {
                    Content = carga.ltCombustible.Value.ToString("N4"),
                    Foreground = Brushes.SteelBlue,
                    Width = 100,
                    FontWeight = FontWeights.Medium,
                    VerticalContentAlignment = VerticalAlignment.Center
                };

                Button btnQuitarCombustibleExtra = new Button
                {
                    Content = carga.idCargaCombustibleExtra == 0 ? "BORRAR" : "CANCELAR",
                    Width = 120,
                    Background = carga.idCargaCombustibleExtra == 0 ? Brushes.OrangeRed : Brushes.Red,
                    Foreground = Brushes.White,
                    Tag = stpActividad
                };
                btnQuitarCombustibleExtra.Click += BtnQuitarCombustibleExtra_Click; ; ;
                btnQuitarCombustibleExtra.Visibility = carga.fechaCombustible == null ? Visibility.Visible : Visibility.Hidden;
                stpActividad.Children.Add(lblUnidadTransporte);
                stpActividad.Children.Add(lblActividad);
                stpActividad.Children.Add(lblLitros);
                stpActividad.Children.Add(btnQuitarCombustibleExtra);

                stpActividadesCombustibleExtra.Children.Add(stpActividad);

                return true;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return false;
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BtnQuitarCombustibleExtra_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                StackPanel stp = btn.Tag as StackPanel;
                CargaCombustibleExtra carga = stp.Tag as CargaCombustibleExtra;

                if (btn.Content.ToString() == "CANCELAR")
                {
                    if (new cancelarOrden(carga.detOrden.ordenTaller, base.mainWindow.usuario.nombreUsuario).cancelarOrdenTrabajo_v2().Value)
                    {
                        buscarMasterOrdenServ((ctrFolio.Tag as MasterOrdServ).idPadreOrdSer);
                    }
                }
                else
                {
                    listaCargaComExtra.Remove(carga);
                    stpActividadesCombustibleExtra.Children.Remove(stp);
                }

            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            cbxChcPosLlantas.SelectedItems.Clear();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            foreach (var item in cbxChcPosLlantas.Items)
            {
                cbxChcPosLlantas.SelectedItems.Add(item);
            }
        }
        List<CabOrdenServicio> listaCabOrdenServicio = new List<CabOrdenServicio>();
        private void agregarControlesCabOrdenServ(CabOrdenServicio cabOrdenServicio)
        {
            try
            {
                Cursor = Cursors.Wait;
                listaCabOrdenServicio.Add(cabOrdenServicio);

                StackPanel stpControles = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(2), Tag = cabOrdenServicio };

                Label lblUnidad = new Label
                {
                    Content = cabOrdenServicio.unidad.clave,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalContentAlignment = HorizontalAlignment.Left,
                    Width = 60,
                    Foreground = Brushes.SteelBlue
                };

                Label lblNoOrden = new Label
                {
                    Content = cabOrdenServicio.idOrdenSer,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalContentAlignment = HorizontalAlignment.Left,
                    Width = 60,
                    Foreground = Brushes.SteelBlue
                };

                Label lblTipoOrden = new Label
                {
                    Content = cabOrdenServicio.enumTipoOrdServ.ToString(),
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalContentAlignment = HorizontalAlignment.Left,
                    Width = 95,
                    Foreground = Brushes.SteelBlue
                };

                Label lblTipoServicio = new Label
                {
                    Content = cabOrdenServicio.enumtipoServicio.ToString(),
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalContentAlignment = HorizontalAlignment.Left,
                    Width = 95,
                    Foreground = Brushes.SteelBlue
                };

                Button btnCancelarOrdenServ = new Button
                {
                    Content = "CANCELAR",
                    Margin = new Thickness(2),
                    Width = 70,
                    Background = Brushes.Red,
                    Foreground = Brushes.White,
                    Height = 22,
                    Tag = cabOrdenServicio,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center
                };
                btnCancelarOrdenServ.Click += BtnCancelarOrdenServ_Click;
                if (cabOrdenServicio.enumTipoOrdServ == enumTipoOrdServ.COMBUSTIBLE || cabOrdenServicio.enumTipoOrdServ == enumTipoOrdServ.COMBUSTIBLE_EXTRA)
                {
                    btnCancelarOrdenServ.Visibility = cabOrdenServicio.fechaTerminado == null ? Visibility.Visible : Visibility.Hidden;
                }
                else
                {
                    btnCancelarOrdenServ.Visibility = cabOrdenServicio.fechaRecepcion == null ? Visibility.Visible : Visibility.Hidden;
                }
                

                stpControles.Children.Add(lblUnidad);
                stpControles.Children.Add(lblNoOrden);
                stpControles.Children.Add(lblTipoOrden);
                stpControles.Children.Add(lblTipoServicio);
                stpControles.Children.Add(btnCancelarOrdenServ);

                stpControlesCabOrdenServ.Children.Add(stpControles);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BtnCancelarOrdenServ_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button button = sender as Button;
                CabOrdenServicio cabOrdenServicio = button.Tag as CabOrdenServicio;
                string str = new ventanaInputTexto("Motivo De Cancelacion", "Proporcionar el motivo de la cancelación").obtenerRespuesta();
                if (!string.IsNullOrEmpty(str))
                {
                    OperationResult resp = new MasterOrdServSvc().cancelarOrdenServicio_v2(new TipoOrdenServicio { idOrdenSer = cabOrdenServicio.idOrdenSer }, str, mainWindow.usuario.nombreUsuario);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        //limpiarControles();
                        buscarMasterOrdenServ((ctrFolio.Tag as MasterOrdServ).idPadreOrdSer);
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        void limpiarControles()
        {
            ctrTractor.unidadTransporte = null;
            ctrRemolque.unidadTransporte = null;
            ctrDolly.unidadTransporte = null;
            ctrRemolque2.unidadTransporte = null;
            stpActividadesPendientes.Children.Clear();
        }

        private void btnQuitarTC_Click(object sender, RoutedEventArgs e)
        {
            stpCargasCombustible.Children.Clear();
            ctrTractor.limpiar();
        }

        private void btnQuitarTV2_Click(object sender, RoutedEventArgs e)
        {
            ctrRemolque2.limpiar();
        }

        private void btnQuitarDL_Click(object sender, RoutedEventArgs e)
        {
            ctrDolly.limpiar();
        }

        private void btnQuitarTV1_Click(object sender, RoutedEventArgs e)
        {
            ctrRemolque.limpiar();
        }
        public void cargarInfoUltimoViaje()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new CartaPorteSvc().getDatosUltimoViajeByTC(ctrTractor.unidadTransporte.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    ctrRemolque.limpiar();
                    ctrDolly.limpiar();
                    ctrRemolque2.limpiar();
                    UltimoViaje ultimoViaje = resp.result as UltimoViaje;
                    var respRemolque = new UnidadTransporteSvc().getUnidadesALLorById(0, ultimoViaje.idRemolque);
                    if (respRemolque.typeResult == ResultTypes.success)
                    {
                        ctrRemolque.unidadTransporte = (respRemolque.result as List<UnidadTransporte>)[0];
                    }

                    if (!string.IsNullOrEmpty(ultimoViaje.idDolly))
                    {
                        var respDolly = new UnidadTransporteSvc().getUnidadesALLorById(0, ultimoViaje.idDolly);
                        if (respDolly.typeResult == ResultTypes.success)
                        {
                            ctrDolly.unidadTransporte = (respDolly.result as List<UnidadTransporte>)[0];
                        }
                    }

                    if (!string.IsNullOrEmpty(ultimoViaje.idRemolque2))
                    {
                        var respRemolque2 = new UnidadTransporteSvc().getUnidadesALLorById(0, ultimoViaje.idRemolque2);
                        if (respRemolque2.typeResult == ResultTypes.success)
                        {
                            ctrRemolque2.unidadTransporte = (respRemolque2.result as List<UnidadTransporte>)[0];
                        }
                    }

                    var respOperador = new OperadorSvc().getOperadoresALLorById(ultimoViaje.idOperador);
                    if (respOperador.typeResult == ResultTypes.success)
                    {
                        ctrOperador.personal = (respOperador.result as List<Personal>)[0];
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void btnQuitarOperador_Click(object sender, RoutedEventArgs e)
        {
            ctrOperador.limpiar();
        }

        private void BtnCambiarViaje_Click(object sender, RoutedEventArgs e)
        {
            ctrFolioViaje.limpiar();
            ctrFolioViaje.IsEnabled = requiereViaje;
        }
        private void TxtEntero_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (cbxUnidadesCombustibleExtra.SelectedItem == null) return;
                    UnidadTransporte unidad = cbxUnidadesCombustibleExtra.SelectedItem as UnidadTransporte;
                    if (ctrFolioViaje.valor != 0)
                    {
                        var resp = new CartaPorteSvc().getCartaPorteById(ctrFolioViaje.valor);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            Viaje viaje = (resp.result as List<Viaje>)[0];
                            if (viaje.EstatusGuia == "ACTIVO")
                            {
                                if (unidad.clave == viaje.tractor.clave)
                                {
                                    goto Label_mapViaje;
                                }
                                if (viaje.remolque != null)
                                {
                                    if (unidad.clave == viaje.remolque.clave)
                                    {
                                        goto Label_mapViaje;
                                    }
                                }
                                if (viaje.dolly != null)
                                {
                                    if (unidad.clave == viaje.dolly.clave)
                                    {
                                        goto Label_mapViaje;
                                    }
                                }
                                if (viaje.remolque2 != null)
                                {
                                    if (unidad.clave == viaje.remolque2.clave)
                                    {
                                        goto Label_mapViaje;
                                    }
                                }
                                imprimir(new OperationResult(ResultTypes.warning, "LA UNIDAD SELECCIONADA NO ESTA DENTRO DEL VIAJE"));
                                return;
                            }
                            else
                            {
                                ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "EL VIAJE NO ESTA EN ESTATUS ACTIVO"));
                                return;
                            }
                            Label_mapViaje:
                            ctrFolioViaje.Tag = viaje;
                            ctrFolioViaje.valor = viaje.NumGuiaId;
                            ctrFolioViaje.IsEnabled = false;
                            return;

                        }
                        else
                        {
                            ImprimirMensaje.imprimir(resp);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CbxUnidadesCombustibleExtra_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ctrFolioViaje.limpiar();
            ctrFolioViaje.IsEnabled = requiereViaje;
        }
    }
}
