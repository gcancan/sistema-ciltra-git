﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
namespace WpfClient.ProcesosOS
{
    /// <summary>
    /// Lógica de interacción para ProcesamientoOrdenServicio.xaml
    /// </summary>
    public partial class ProcesamientoOrdenServicio : ViewBase
    {
        public ProcesamientoOrdenServicio()
        {
            InitializeComponent();
        }
        bool ajustarFecha = false;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                ajustarFecha = new PrivilegioSvc().consultarPrivilegio("AJUSTAR_FECHA_PROCESAMINTO_ORD_SER", mainWindow.usuario.idUsuario).typeResult == ResultTypes.success;

                Cursor = Cursors.Wait;
                ctrFolioOrdenServicio.KeyDown += CtrFolioOrdenServicio_KeyDown;
                nuevo();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CtrFolioOrdenServicio_KeyDown(object sender, KeyEventArgs e)
        {
            if (Key.Enter == e.Key)
            {
                if (ctrFolioOrdenServicio.valor > 0)
                {
                    buscarOrdenServicio(ctrFolioOrdenServicio.valor);
                }
            }
        }
        public void buscarOrdenServicio(int folioOrden)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new SolicitudOrdenServicioSvc().getCapOrdenServicioByFolioOrden(folioOrden);
                if (resp.typeResult == ResultTypes.success)
                {
                    CabOrdenServicio cabOrdenServicio = resp.result as CabOrdenServicio;
                    if (cabOrdenServicio.enumTipoOrdServ == enumTipoOrdServ.COMBUSTIBLE || cabOrdenServicio.enumTipoOrdServ == enumTipoOrdServ.COMBUSTIBLE_EXTRA)
                    {
                        ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, string.Format("ESTE TIPO DE SOLUCITUD ES DE {0} Y NO PODRÁ SER PROCESADO", cabOrdenServicio.enumTipoOrdServ.ToString())));
                        nuevo();
                        return;
                    }
                    mapForm(cabOrdenServicio);
                    if (cabOrdenServicio.enumTipoOrdServ == enumTipoOrdServ.TALLER)
                    {
                        if (stpAtividadesOrdenServ.Children.Count == 0)// (cabOrdenServicio.enumTipoOrden == enumTipoOrden.PREVENTIVO)
                        {
                            if (cabOrdenServicio.listaActividades.Count() == 0)
                            {
                                foreach (var detalle in cabOrdenServicio.listaDetOrdenServicio)
                                {
                                    if (detalle.actividad != null || detalle.servicio != null)
                                    {
                                        agregarActividad(new ActividadOrdenServicio
                                        {
                                            idActividadPendiente = detalle.idActividadPendiente,
                                            idOrdenTrabajo = detalle.idOrdenTrabajo,
                                            actividad = detalle.actividad,
                                            servicio = detalle.servicio,
                                            usuario = mainWindow.usuario.nombreUsuario,
                                            activo = true,
                                            enumTipoOrdServ = cabOrdenServicio.enumTipoOrdServ,
                                            idOrdenServicio = cabOrdenServicio.idOrdenSer,
                                            idPadreOrdenServicio = cabOrdenServicio.idPadreOrdSer,
                                            idResponsable = ctrResponsable.personal == null ? null : (int?)ctrResponsable.personal.clave,
                                            idAuxiliar1 = ctrAuxiliar1.personal == null ? null : (int?)ctrAuxiliar1.personal.clave,
                                            idAuxiliar2 = ctrAuxiliar2.personal == null ? null : (int?)ctrAuxiliar2.personal.clave,
                                            posicion = detalle.pocision
                                        });
                                    }

                                }

                                if (stpAtividadesOrdenServ.Children.Count > 0)
                                {
                                    chcExpRecepcion.IsChecked = true;
                                    chcExpActividades.IsChecked = true;
                                    chcExpActividades.IsEnabled = false;
                                    //stpActividades.IsEnabled = false;
                                }
                            }

                        }
                    }

                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    mapForm(null);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private CabOrdenServicio _Oservicio = null;
        private void mapForm(CabOrdenServicio cabOrdenServicio)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (cabOrdenServicio != null)
                {
                    _Oservicio = cabOrdenServicio;
                    switch (cabOrdenServicio.enumTipoOrdServ)
                    {

                        case enumTipoOrdServ.TALLER:
                            ctrResponsable.loaded(eTipoBusquedaPersonal.MECANICOS, null);
                            ctrAuxiliar1.loaded(eTipoBusquedaPersonal.MECANICOS, null);
                            ctrAuxiliar2.loaded(eTipoBusquedaPersonal.MECANICOS, null);
                            break;
                        case enumTipoOrdServ.LLANTAS:
                            ctrResponsable.loaded(eTipoBusquedaPersonal.LLANTEROS, null);
                            ctrAuxiliar1.loaded(eTipoBusquedaPersonal.LLANTEROS, null);
                            ctrAuxiliar2.loaded(eTipoBusquedaPersonal.LLANTEROS, null);
                            break;
                        case enumTipoOrdServ.LAVADERO:
                            ctrResponsable.loaded(eTipoBusquedaPersonal.LAVADORES, null);
                            ctrAuxiliar1.loaded(eTipoBusquedaPersonal.LAVADORES, null);
                            ctrAuxiliar2.loaded(eTipoBusquedaPersonal.LAVADORES, null);
                            break;
                        case enumTipoOrdServ.RESGUARDO:
                            break;
                        default:
                            break;
                    }

                    ctrFolioOrdenServicio.valor = cabOrdenServicio.idOrdenSer;
                    ctrFolioOrdenServicio.Tag = cabOrdenServicio;
                    ctrFolioOrdenServicio.IsEnabled = false;
                    ctrUnidadTransporte.unidadTransporte = cabOrdenServicio.unidad;
                    txtTipoOrdenServicio.Text = cabOrdenServicio.enumTipoOrdServ.ToString();
                    txtTipoServicio.Text = cabOrdenServicio.enumtipoServicio.ToString();
                    lvlOrdenesTrabajo.ItemsSource = cabOrdenServicio.listaDetOrdenServicio;
                    txtObservaciones.Text = cabOrdenServicio.observaciones;

                    if (cabOrdenServicio.enumTipoOrdServ == enumTipoOrdServ.LAVADERO)
                    {
                        stpTipoServicio.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        stpTipoServicio.Visibility = Visibility.Visible;
                    }

                    if (cabOrdenServicio.fechaRecepcion.HasValue)
                    {
                        dtpFechaRecepcion.Value = cabOrdenServicio.fechaRecepcion;
                        chcExpRecepcion.IsChecked = true;
                        expanderRecepcion.IsExpanded = false;
                        var respPersonalEntrega = new OperadorSvc().getPersonalALLorById(cabOrdenServicio.idEmpleadoEntrega.Value);
                        if (respPersonalEntrega.typeResult == ResultTypes.success)
                        {
                            ctrPersonalEntrega.personal = (respPersonalEntrega.result as List<Personal>)[0];
                        }
                        stpRecepcion.IsEnabled = false;
                        chcExpRecepcion.IsEnabled = false;
                    }

                    if (cabOrdenServicio.fechaAsignado.HasValue)
                    {
                        dtpFechaAsignacion.Value = cabOrdenServicio.fechaAsignado;
                        chcExpAsignacion.IsChecked = true;
                        expanderAsignacion.IsExpanded = false;

                        if (cabOrdenServicio.listaDetOrdenServicio[0].responsable != null)
                        {
                            var respPersonalResp = new OperadorSvc().getPersonalALLorById(cabOrdenServicio.listaDetOrdenServicio[0].responsable.clave);
                            if (respPersonalResp.typeResult == ResultTypes.success)
                            {
                                ctrResponsable.personal = (respPersonalResp.result as List<Personal>)[0];
                            }
                        }


                        if (cabOrdenServicio.listaDetOrdenServicio[0].auxiliar1 != null)
                        {
                            var respPersonal = new OperadorSvc().getPersonalALLorById(cabOrdenServicio.listaDetOrdenServicio[0].auxiliar1.clave);
                            if (respPersonal.typeResult == ResultTypes.success)
                            {
                                ctrAuxiliar1.personal = (respPersonal.result as List<Personal>)[0];
                            }
                        }

                        if (cabOrdenServicio.listaDetOrdenServicio[0].auxiliar2 != null)
                        {
                            var respPersonal = new OperadorSvc().getPersonalALLorById(cabOrdenServicio.listaDetOrdenServicio[0].auxiliar2.clave);
                            if (respPersonal.typeResult == ResultTypes.success)
                            {
                                ctrAuxiliar2.personal = (respPersonal.result as List<Personal>)[0];
                            }
                        }
                        stpAsignacion.IsEnabled = false;
                        chcExpAsignacion.IsEnabled = false;
                    }
                    if (cabOrdenServicio.enumTipoOrdServ == enumTipoOrdServ.LLANTAS)
                    {
                        UnidadTransporte unidad = cabOrdenServicio.unidad;
                        var resp = new DiagramasLLantasSvc().getDiagramaByTipoUnidadTrans(unidad.tipoUnidad);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            DiagramaLLantas diagrama = resp.result as DiagramaLLantas;
                            for (int i = 1; i <= diagrama.numLLantas; i++)
                            {
                                cbxChcPosLlantas.Items.Add(i);
                            }
                        }
                        stpControlesPosicion.IsEnabled = true;
                    }
                    else
                    {
                        stpControlesPosicion.IsEnabled = false;
                        cbxChcPosLlantas.Items.Clear();
                    }

                    cargarActividades(cabOrdenServicio.enumTipoOrdServ);
                    stpAtividadesOrdenServ.Children.Clear();

                    foreach (var item in cabOrdenServicio.listaActividades)
                    {
                        agregarActividad(item);

                    }

                    if (cabOrdenServicio.listaActividades.Count > 0)
                    {
                        chcExpActividades.IsChecked = true;
                        chcExpActividades.IsEnabled = false;
                    }
                    else
                    {
                        chcExpActividades.IsChecked = false;
                        chcExpActividades.IsEnabled = true;
                    }

                    if (cabOrdenServicio.fechaTerminado.HasValue)
                    {
                        dtpFechaTerminacion.Value = cabOrdenServicio.fechaTerminado;
                        chcExpTerminacion.IsChecked = true;
                        chcExpTerminacion.IsEnabled = false;
                        expanderActividades.IsExpanded = false;
                        //txtFechaTerminacion.Text = cabOrdenServicio.fechaTerminado.Value.ToString("dd/MM/yy HH:mm");
                        dtpFechaTerminacion.Value = cabOrdenServicio.fechaTerminado.Value;
                        stpActividades.IsEnabled = false;
                        stpActividades.IsEnabled = false;
                        chcExpActividades.IsEnabled = false;
                        txtObservaciones.IsEnabled = false;
                        llenarListaChcActividades(cabOrdenServicio.listaActividades);
                        stpActividadCheck.IsEnabled = false;
                        stpTerminacion.IsEnabled = false;
                    }
                    else
                    {
                        expanderActividades.IsExpanded = false;
                        chcExpTerminacion.IsEnabled = true;
                        chcExpTerminacion.IsChecked = false;
                        //txtFechaTerminacion.Clear();
                        dtpFechaTerminacion.Value = null;
                        stpActividades.IsEnabled = true;
                        stpTerminacion.IsEnabled = true;
                        txtObservaciones.IsEnabled = true;
                    }

                    if (cabOrdenServicio.fechaEntregado.HasValue)
                    {
                        dtpFechaEntrega.Value = cabOrdenServicio.fechaEntregado;
                        expanderTerminacion.IsExpanded = false;
                        chcExpEntrega.IsChecked = true;
                        chcExpEntrega.IsEnabled = false;
                        stpEntrega.IsEnabled = false;

                        var respPersonalRecibe = new OperadorSvc().getPersonalALLorById(cabOrdenServicio.idEmpleadoRecibe.Value);
                        if (respPersonalRecibe.typeResult == ResultTypes.success)
                        {
                            ctrPersonalRecibe.personal = (respPersonalRecibe.result as List<Personal>)[0];
                        }
                        mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                    }
                    else
                    {
                        chcExpEntrega.IsChecked = false;
                        stpEntrega.IsEnabled = true;
                        chcExpEntrega.IsEnabled = true;
                    }
                }
                else
                {
                    _Oservicio = null;
                    ctrFolioOrdenServicio.valor = 0;
                    ctrFolioOrdenServicio.Tag = null;
                    ctrFolioOrdenServicio.IsEnabled = true;
                    ctrUnidadTransporte.limpiar();
                    txtTipoOrdenServicio.Clear();
                    txtTipoServicio.Clear();
                    lvlOrdenesTrabajo.ItemsSource = null;
                    txtObservaciones.Clear();
                    ctrPersonalEntrega.limpiar();
                    stpRecepcion.IsEnabled = true;
                    chcExpRecepcion.IsEnabled = true;

                    ctrResponsable.limpiar();
                    ctrAuxiliar1.limpiar();
                    ctrAuxiliar2.limpiar();
                    stpAsignacion.IsEnabled = true;
                    chcExpAsignacion.IsEnabled = true;
                    lista = new List<Actividad>();

                    cbxChcPosLlantas.Items.Clear();
                    stpAtividadesOrdenServ.Children.Clear();
                    mapCampoActividad(null);
                    stpControlesPosicion.IsEnabled = false;

                    chcExpActividades.IsChecked = false;
                    chcExpActividades.IsEnabled = true;
                    stpAtividadesOrdenServ.Children.Clear();

                    chcExpTerminacion.IsChecked = false;
                    chcExpTerminacion.IsEnabled = true;
                    stpActividades.IsEnabled = true;
                    //txtFechaTerminacion.Clear();
                    dtpFechaTerminacion.Value = null;

                    chcExpEntrega.IsChecked = false;
                    stpEntrega.IsEnabled = true;
                    chcExpEntrega.IsEnabled = true;
                    ctrPersonalRecibe.limpiar();
                    txtObservaciones.IsEnabled = true;
                    stpActividadCheck.IsEnabled = true;
                    stpTerminacion.IsEnabled = true;

                    dtpFechaRecepcion.Value = null;
                    dtpFechaAsignacion.Value = null;
                    dtpFechaTerminacion.Value = null;
                    dtpFechaEntrega.Value = null;
                    stpTipoServicio.Visibility = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        List<Actividad> lista = new List<Actividad>();
        private void cargarActividades(enumTipoOrdServ enumTipo)
        {
            try
            {
                lista = new List<Actividad>();
                Cursor = Cursors.Wait;
                //var resp = new OrdenesTrabajoSvc().getRelacionActividad((int)enumTipo);
                var resp = new ActividadSvc().getAllActividadesByTipoOrdenServ(enumTipo);
                if (resp.typeResult == ResultTypes.success)
                {
                    lista = (List<Actividad>)resp.result;
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    lista = new List<Actividad>();
                }
                else
                {
                    lista = new List<Actividad>();
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);

            chcExpRecepcion.IsChecked = true;
            chcExpAsignacion.IsChecked = true;
            chcExpActividades.IsChecked = true;
            chcExpTerminacion.IsChecked = true;
            chcExpEntrega.IsChecked = true;

            chcExpRecepcion.IsChecked = false;
            chcExpAsignacion.IsChecked = false;
            chcExpActividades.IsChecked = false;
            chcExpTerminacion.IsChecked = false;
            chcExpEntrega.IsChecked = false;

            ctrFolioOrdenServicio.txtEntero.Focus();
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                CheckBox chc = sender as CheckBox;
                CabOrdenServicio cab = null;

                if (ctrFolioOrdenServicio.Tag != null)
                {
                    cab = ctrFolioOrdenServicio.Tag as CabOrdenServicio;

                }
                switch (chc.Name)
                {

                    case "chcExpRecepcion":
                        dtpFechaRecepcion.IsEnabled = ajustarFecha;
                        if (chc.IsChecked.Value)
                        {
                            expanderRecepcion.IsExpanded = true;
                            stpRecepcion.IsEnabled = true;
                            dtpFechaRecepcion.Value = cab == null ? DateTime.Now : (cab.fechaRecepcion == null ? DateTime.Now : cab.fechaRecepcion);

                            if (cab != null)
                            {
                                MasterOrdServ master = null;
                                var respMaster = new SolicitudOrdenServicioSvc().getMasterOrdenServicioByFolio(cab.idPadreOrdSer);
                                if (respMaster.typeResult == ResultTypes.success)
                                {
                                    master = respMaster.result as MasterOrdServ;
                                    ctrPersonalEntrega.personal = master != null ? master.chofer : null;
                                }
                            }


                        }
                        else
                        {
                            expanderRecepcion.IsExpanded = false;
                            stpRecepcion.IsEnabled = false;
                            dtpFechaRecepcion.Value = cab == null ? null : cab.fechaRecepcion;
                            ctrPersonalEntrega.personal = null;
                        }
                        break;
                    case "chcExpAsignacion":
                        dtpFechaAsignacion.IsEnabled = ajustarFecha;
                        if (chc.IsChecked.Value)
                        {
                            expanderAsignacion.IsExpanded = true;
                            stpAsignacion.IsEnabled = true;
                            dtpFechaAsignacion.Value = cab == null ? DateTime.Now : (cab.fechaAsignado == null ? DateTime.Now : cab.fechaAsignado);
                        }
                        else
                        {
                            expanderAsignacion.IsExpanded = false;
                            stpAsignacion.IsEnabled = false;
                            ctrResponsable.limpiar();
                            ctrAuxiliar1.limpiar();
                            ctrAuxiliar2.limpiar();
                            dtpFechaAsignacion.Value = cab == null ? null : cab.fechaAsignado;
                        }
                        break;
                    case "chcExpActividades":
                        if (chc.IsChecked.Value)
                        {
                            expanderActividades.IsExpanded = true;
                            stpActividades.IsEnabled = true;

                        }
                        else
                        {
                            expanderActividades.IsExpanded = false;
                            stpActividades.IsEnabled = false;
                        }
                        break;
                    case "chcExpTerminacion":
                        dtpFechaTerminacion.IsEnabled = ajustarFecha;
                        if (chc.IsChecked.Value)
                        {
                            expanderTerminacion.IsExpanded = true;
                            stpTerminacion.IsEnabled = true;
                            cargarActividadesFinalizacion();
                            dtpFechaTerminacion.Value = cab == null ? DateTime.Now : (cab.fechaTerminado == null ? DateTime.Now : cab.fechaTerminado);
                        }
                        else
                        {
                            expanderTerminacion.IsExpanded = false;
                            stpTerminacion.IsEnabled = false;
                            stpActividadCheck.Children.Clear();
                            dtpFechaTerminacion.Value = cab == null ? null : cab.fechaTerminado;
                        }
                        break;
                    case "chcExpEntrega":
                        dtpFechaEntrega.IsEnabled = ajustarFecha;
                        if (chc.IsChecked.Value)
                        {
                            expanderEntrega.IsExpanded = true;
                            stpEntrega.IsEnabled = true;
                            dtpFechaEntrega.Value = cab == null ? DateTime.Now : (cab.fechaEntregado == null ? DateTime.Now : cab.fechaEntregado);
                            ctrPersonalRecibe.personal = ctrPersonalEntrega.personal;
                        }
                        else
                        {
                            expanderEntrega.IsExpanded = false;
                            stpEntrega.IsEnabled = false;
                            dtpFechaEntrega.Value = cab == null ? null : cab.fechaEntregado;
                            ctrPersonalRecibe.personal = null;
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void cargarActividadesFinalizacion()
        {
            try
            {
                List<ActividadOrdenServicio> listaActServ = new List<ActividadOrdenServicio>();
                List<StackPanel> listStackPanel = stpAtividadesOrdenServ.Children.Cast<StackPanel>().ToList();
                foreach (var stp in listStackPanel)
                {
                    ActividadOrdenServicio actividad = (stp.Tag as ActividadOrdenServicio);
                    actividad.enumActividadOrdenServ = enumActividadOrdenServ.NO;
                    listaActServ.Add(actividad);
                }
                llenarListaChcActividades(listaActServ);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void llenarListaChcActividades(List<ActividadOrdenServicio> listaActServ)
        {
            try
            {
                stpActividadCheck.Children.Clear();
                foreach (var actServ in listaActServ)
                {
                    if (actServ.idActividadOrdenServicio == 0) continue;

                    //actServ.enumActividadOrdenServ = enumActividadOrdenServ.SI;
                    Border border = new Border { BorderBrush = Brushes.SteelBlue, BorderThickness = new Thickness(0, 0, 0, 1), Margin = new Thickness(1), Tag = actServ };
                    StackPanel stpContend = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(1) };

                    TextBox txtActividad = new TextBox
                    {
                        FontSize = 11,
                        FontWeight = FontWeights.Bold,
                        CharacterCasing = CharacterCasing.Upper,
                        Foreground = Brushes.SteelBlue,
                        Text = actServ.actividad == null ? actServ.servicio.nombre : actServ.actividad.nombre,
                        TextWrapping = TextWrapping.Wrap,
                        Width = 228,
                        Background = null,
                        BorderBrush = null,
                        IsReadOnly = true
                    };

                    RadioButton rbnSi = new RadioButton
                    {
                        IsChecked = actServ.enumActividadOrdenServ == enumActividadOrdenServ.SI,
                        Width = 18,
                        Margin = new Thickness(22, 0, 0, 0),
                        VerticalAlignment = VerticalAlignment.Center,
                        Tag = border,
                        Name = "rbnSi"
                    };
                    rbnSi.Checked += Rbn_Checked;

                    //RadioButton rbnNo = new RadioButton
                    //{
                    //    IsChecked = actServ.enumActividadOrdenServ == enumActividadOrdenServ.NO,
                    //    Width = 18,
                    //    Margin = new Thickness(40, 0, 0, 0),
                    //    VerticalAlignment = VerticalAlignment.Center,
                    //    Tag = border,
                    //    Name = "rbnNo"
                    //};
                    //rbnNo.Checked += Rbn_Checked;

                    RadioButton rbnPend = new RadioButton
                    {
                        IsChecked = actServ.enumActividadOrdenServ == enumActividadOrdenServ.PENDIENTE,
                        Width = 18,
                        Margin = new Thickness(42, 0, 0, 0),
                        VerticalAlignment = VerticalAlignment.Center,
                        Tag = border,
                        Name = "rbnPend"
                    };
                    rbnPend.Checked += Rbn_Checked;

                    stpContend.Children.Add(txtActividad);
                    stpContend.Children.Add(rbnSi);
                    //stpContend.Children.Add(rbnNo);
                    stpContend.Children.Add(rbnPend);


                    border.Child = stpContend;
                    stpActividadCheck.Children.Add(border);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private void Rbn_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    RadioButton rbn = sender as RadioButton;
                    ActividadOrdenServicio actServ = (rbn.Tag as Border).Tag as ActividadOrdenServicio;
                    switch (rbn.Name)
                    {
                        case "rbnSi":
                            actServ.activo = true;
                            actServ.pendiente = false;
                            actServ.enumActividadOrdenServ = enumActividadOrdenServ.SI;
                            break;
                        case "rbnNo":
                            actServ.activo = false;
                            actServ.pendiente = false;
                            actServ.enumActividadOrdenServ = enumActividadOrdenServ.NO;
                            break;
                        case "rbnPend":
                            actServ.activo = false;
                            actServ.pendiente = true;
                            actServ.enumActividadOrdenServ = enumActividadOrdenServ.PENDIENTE;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private void btnCambiarPersonalRecibe_Click(object sender, RoutedEventArgs e)
        {
            ctrPersonalEntrega.limpiar();
        }
        private void btnCambiarPersonal_Click(object sender, RoutedEventArgs e)
        {
            //if (chcResponsable.IsChecked.Value)
            //    ctrResponsable.limpiar();
            //if (chcAuxiliar1.IsChecked.Value)
            //    ctrAuxiliar1.limpiar();
            //if (chcAuxiliar2.IsChecked.Value)
            //    ctrAuxiliar2.limpiar();
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;

                var val = validar();                
                if (val.typeResult != ResultTypes.success) return val;

                ctrFolioOrdenServicio.Tag = _Oservicio;
                CabOrdenServicio cabOrdenServicio = mapForm();

                OperationResult valFecha = validarFechas(cabOrdenServicio);
                if (valFecha.typeResult != ResultTypes.success) return valFecha;

                if (cabOrdenServicio.estatus == "TER")
                {
                    if (cabOrdenServicio.listaActividades.FindAll(s => s.enumActividadOrdenServ == enumActividadOrdenServ.NO).Count() > 0)
                    {
                        return new OperationResult(ResultTypes.warning, "NO SE MARCARON TODAS LAS VALIDACIONES DE ACTIVIDADES");
                    }

                    if ((cabOrdenServicio.listaActividades.FindAll(s => s.enumActividadOrdenServ == enumActividadOrdenServ.SI).Count() == 0) && string.IsNullOrEmpty(txtObservaciones.Text))
                    {
                        txtObservaciones.Focus();
                        return new OperationResult(ResultTypes.warning, "SI NO SE GUARDARAN ACTIVIDADES LAS OBSERVACIONES SON OBLIGATORIAS");
                    }

                    if (cabOrdenServicio.listaActividades.Count() != cabOrdenServicio.listaActividades.FindAll(s => s.enumActividadOrdenServ == enumActividadOrdenServ.SI).Count())
                    {
                        var lista = new pantallaVerificacionActividades(cabOrdenServicio.listaActividades, enumProcesos.ACTIVIDAD_PENDIENTE).getListaActServicio();
                        if (lista != null)
                        {
                            cabOrdenServicio.listaActividades = lista;
                        }
                        else
                        {
                            return new OperationResult(ResultTypes.warning, "NO SE PODRÁ CONTINUAR SIN LOS MOTIVOS.");
                        }
                    }
                }

                var resp = new SolicitudOrdenServicioSvc().actualizarCabOrdenServicio(ref cabOrdenServicio);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(cabOrdenServicio);
                    mainWindow.habilitar = true;
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
                }
                else
                {

                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private OperationResult validarFechas(CabOrdenServicio cabOrdenServicio)
        {
            try
            {
                if (chcExpAsignacion.IsChecked.Value)
                {
                    if (cabOrdenServicio.fechaAsignado < cabOrdenServicio.fechaRecepcion)
                    {
                        return new OperationResult(ResultTypes.warning, "LA FECHA DE ASIGNACIÓN NO PUEDE SER MENOR A LA FECHA DE RECEPCIÓN");
                    }                    
                }
                if (chcExpTerminacion.IsChecked.Value)
                {
                    if (cabOrdenServicio.fechaTerminado < cabOrdenServicio.fechaAsignado)
                    {
                        return new OperationResult(ResultTypes.warning, "LA FECHA DE TERMINACIÓN NO PUEDE SER MENOR A LA FECHA DE ASIGNACIÓN");
                    }
                }
                if (chcExpEntrega.IsChecked.Value)
                {
                    if (cabOrdenServicio.fechaEntregado < cabOrdenServicio.fechaTerminado)
                    {
                        return new OperationResult(ResultTypes.warning, "LA FECHA DE ENTREGA NO PUEDE SER MENOR A LA FECHA DE TERMINACIÓN");
                    }
                }
                return new OperationResult(ResultTypes.success, "");
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public OperationResult validar()
        {
            try
            {
                if (ctrFolioOrdenServicio.Tag == null) return new OperationResult(ResultTypes.warning, "SIN INFORMACIÓN PARA GUARDAR");

                if (chcExpRecepcion.IsChecked.Value)
                {
                    if (ctrPersonalEntrega.personal == null)
                    {
                        ctrPersonalEntrega.txtPersonal.Focus();
                        return new OperationResult(ResultTypes.warning, "NO SE PODRÁ RECEPCIONAR LA UNIDAD SI NO SE ESPECIFICA QUIEN LA ENTREGA");
                    }
                    if (ajustarFecha && dtpFechaRecepcion.Value == null)
                    {
                        return new OperationResult(ResultTypes.warning, "NO SE PROPORCIONO LA FECHA DE RECEPCIÓN");
                    }
                }
                else
                {
                    return new OperationResult(ResultTypes.warning, "SIN CAMBIOS PARA REALIZAR");
                }
                if (chcExpAsignacion.IsChecked.Value)
                {
                    if (ctrPersonalEntrega.personal == null)
                    {
                        ctrPersonalEntrega.txtPersonal.Focus();
                        return new OperationResult(ResultTypes.warning, "PARA LA ASIGNACIÓN DE LA ORDEN ANTES DEBE ESTAR RECEPCIONADA");
                    }
                    if (ctrResponsable.personal == null)
                    {
                        ctrResponsable.txtPersonal.Focus();
                        return new OperationResult(ResultTypes.warning, "PARA ASIGNAR LA ORDEN DE SERVICIO SE NECESITA EL PERSONAL RESPONSABLE");
                    }
                    if (ajustarFecha && dtpFechaAsignacion.Value == null)
                    {
                        return new OperationResult(ResultTypes.warning, "NO SE PROPORCIONO LA FECHA DE ASIGNACIÓN");
                    }                    
                }

                if (chcExpTerminacion.IsChecked.Value)
                {
                    if (ctrResponsable.personal == null)
                    {
                        return new OperationResult(ResultTypes.warning, "NO SE PODRÁ TERMINAR LA ORDEN SI NO ESTA ASIGNADA");
                    }
                    if (stpAtividadesOrdenServ.Children.Count == 0)
                    {
                        if (string.IsNullOrEmpty(txtObservaciones.Text.Trim()))
                        {
                            return new OperationResult(ResultTypes.warning, "PARA TERMINAR LA ORDEN DE SERVICIO SIN ACTIVIDADES, LAS OBSERVACIONES SON OBLIGATORIAS");
                        }
                    }
                    if (ajustarFecha && dtpFechaTerminacion.Value == null)
                    {
                        return new OperationResult(ResultTypes.warning, "NO SE PROPORCIONO LA FECHA DE TERMINACIÓN");
                    }
                }

                if (chcExpEntrega.IsChecked.Value)
                {
                    if (!chcExpTerminacion.IsChecked.Value)
                    {
                        return new OperationResult(ResultTypes.warning, "PARA LA ENTREGA DE LA UNIDAD, LA ORDEN DEBERÁ ESTAR TERMINADA");
                    }
                    if (ctrPersonalRecibe.personal == null)
                    {
                        ctrResponsable.txtPersonal.Focus();
                        return new OperationResult(ResultTypes.warning, "SE NECESITA ESPECIFICAR QUIEN RECIBE LA UNIDAD");
                    }
                    if (ajustarFecha && dtpFechaEntrega.Value == null)
                    {
                        return new OperationResult(ResultTypes.warning, "NO SE PROPORCIONO LA FECHA DE ENTREGA");
                    }
                }

                return new OperationResult(ResultTypes.success, "");
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        private CabOrdenServicio mapForm()
        {
            try
            {
                Usuario usuario = mainWindow.usuario;
                Cursor = Cursors.Wait;
                CabOrdenServicio cabOrdenServicio = new CabOrdenServicio();
                var original = ctrFolioOrdenServicio.Tag as CabOrdenServicio;

                cabOrdenServicio.enumTipoOrden = original.enumTipoOrden;
                cabOrdenServicio.enumTipoOrdServ = original.enumTipoOrdServ;
                cabOrdenServicio.enumtipoServicio = original.enumtipoServicio;
                cabOrdenServicio.cargaCombustibleExtra = original.cargaCombustibleExtra;
                cabOrdenServicio.cliente = original.cliente;
                cabOrdenServicio.empresa = original.empresa;
                cabOrdenServicio.estatus = original.estatus;
                cabOrdenServicio.externo = original.externo;
                cabOrdenServicio.fechaAsignado = original.fechaAsignado;
                cabOrdenServicio.fechaCancela = original.fechaCancela;
                cabOrdenServicio.fechaCaptura = original.fechaCaptura;
                cabOrdenServicio.fechaDiagnostico = original.fechaDiagnostico;
                cabOrdenServicio.fechaEntregado = original.fechaEntregado;
                cabOrdenServicio.fechaRecepcion = original.fechaRecepcion;
                cabOrdenServicio.fechaTerminado = original.fechaTerminado;
                cabOrdenServicio.idEmpleadoAutoriza = original.idEmpleadoAutoriza;
                cabOrdenServicio.idEmpleadoEntrega = original.idEmpleadoEntrega;
                cabOrdenServicio.idEmpleadoRecibe = original.idEmpleadoRecibe;
                cabOrdenServicio.idOrdenSer = original.idOrdenSer;
                cabOrdenServicio.idOrdenSerOrigen = original.idOrdenSerOrigen;
                cabOrdenServicio.idPadreOrdSer = original.idPadreOrdSer;
                cabOrdenServicio.km = original.km;
                cabOrdenServicio.listaActividades = original.listaActividades;
                cabOrdenServicio.listaDetOrdenServicio = original.listaDetOrdenServicio;
                cabOrdenServicio.listaIdCabOS = original.listaIdCabOS;
                cabOrdenServicio.notaFinal = original.notaFinal;
                cabOrdenServicio.unidad = original.unidad;
                cabOrdenServicio.usuarioAsigna = original.usuarioAsigna;
                cabOrdenServicio.usuarioCancela = original.usuarioCancela;
                cabOrdenServicio.usuarioEntrega = original.usuarioEntrega;
                cabOrdenServicio.usuarioRecepcion = original.usuarioRecepcion;
                cabOrdenServicio.usuarioTermina = original.usuarioTermina;
                cabOrdenServicio.zonaOperativa = original.zonaOperativa;
                

                cabOrdenServicio.observaciones = txtObservaciones.Text.Trim();

                if (chcExpRecepcion.IsChecked.Value)
                {
                    if (!cabOrdenServicio.fechaRecepcion.HasValue)
                    {
                        cabOrdenServicio.fechaRecepcion = ajustarFecha ? dtpFechaRecepcion.Value : DateTime.Now;
                        cabOrdenServicio.usuarioRecepcion = usuario.nombreUsuario;
                        cabOrdenServicio.idEmpleadoEntrega = ctrPersonalEntrega.personal.clave;
                        cabOrdenServicio.estatus = "REC";
                    }
                }

                if (chcExpAsignacion.IsChecked.Value)
                {
                    if (!cabOrdenServicio.fechaAsignado.HasValue)
                    {
                        cabOrdenServicio.fechaAsignado = ajustarFecha ? dtpFechaAsignacion.Value : DateTime.Now;
                        cabOrdenServicio.usuarioAsigna = usuario.nombreUsuario;
                        foreach (var detOrden in cabOrdenServicio.listaDetOrdenServicio)
                        {
                            detOrden.responsable = ctrResponsable.personal;
                            detOrden.auxiliar1 = ctrAuxiliar1.personal;
                            detOrden.auxiliar2 = ctrAuxiliar2.personal;
                        }
                        cabOrdenServicio.estatus = "ASIG";
                    }
                }

                if (chcExpTerminacion.IsChecked.Value)
                {
                    if (!cabOrdenServicio.fechaTerminado.HasValue)
                    {
                        cabOrdenServicio.fechaTerminado = ajustarFecha ? dtpFechaTerminacion.Value : DateTime.Now;
                        cabOrdenServicio.usuarioTermina = usuario.nombreUsuario;
                        cabOrdenServicio.estatus = "TER";
                    }
                }
                if (chcExpEntrega.IsChecked.Value)
                {
                    cabOrdenServicio.estatus = "ENTG";
                    cabOrdenServicio.fechaEntregado = ajustarFecha ? dtpFechaEntrega.Value : DateTime.Now;
                    cabOrdenServicio.idEmpleadoRecibe = ctrPersonalRecibe.personal.clave;
                    cabOrdenServicio.usuarioEntrega = usuario.nombreUsuario;
                }

                cabOrdenServicio.listaActividades = new List<ActividadOrdenServicio>();
                foreach (StackPanel stp in stpAtividadesOrdenServ.Children.Cast<StackPanel>().ToList())
                {
                    ActividadOrdenServicio actSer = stp.Tag as ActividadOrdenServicio;
                    actSer.idResponsable = ctrResponsable.personal == null ? null : (int?)ctrResponsable.personal.clave;
                    actSer.idAuxiliar1 = ctrAuxiliar1.personal == null ? null : (int?)ctrAuxiliar1.personal.clave;
                    actSer.idAuxiliar2 = ctrAuxiliar2.personal == null ? null : (int?)ctrAuxiliar2.personal.clave;
                    cabOrdenServicio.listaActividades.Add(actSer);
                }

                return cabOrdenServicio;

            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void txtBuscadorActividad_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (ctrFolioOrdenServicio.Tag == null) return;

                CabOrdenServicio cab = ctrFolioOrdenServicio.Tag as CabOrdenServicio;

                //if (ctrResponsable.personal == null)
                //{
                //    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "SE REQUIERE ESPECIFICAR EL PERSONAL RESPONSABLE."));
                //    return;
                //}

                Actividad act = null;
                var lvl = lista.FindAll(x => (x.nombre.IndexOf(txtBuscadorActividad.Text, StringComparison.OrdinalIgnoreCase) >= 0) ||
                                       (x.CCODIGOPRODUCTO.IndexOf(txtBuscadorActividad.Text, StringComparison.OrdinalIgnoreCase) >= 0));
                CabOrdenServicio cabOrdenServicio = ctrFolioOrdenServicio.Tag as CabOrdenServicio;
                if (lvl.Count == 1)
                {
                    mapCampoActividad(lvl[0]);
                    if (cab.enumTipoOrdServ != enumTipoOrdServ.LLANTAS)
                    {
                        agregarActividad(
                                new ActividadOrdenServicio
                                {
                                    idActividadOrdenServicio = 0,
                                    usuario = mainWindow.usuario.nombreUsuario,
                                    idPadreOrdenServicio = cabOrdenServicio.idPadreOrdSer,
                                    idOrdenServicio = cabOrdenServicio.idOrdenSer,
                                    enumTipoOrdServ = cabOrdenServicio.enumTipoOrdServ,
                                    actividad = lvl[0],
                                    servicio = null,
                                    idResponsable = ctrResponsable.personal == null ? null : (int?)ctrResponsable.personal.clave,
                                    idAuxiliar1 = ctrAuxiliar1.personal == null ? null : (int?)ctrAuxiliar1.personal.clave,
                                    idAuxiliar2 = ctrAuxiliar2.personal == null ? null : (int?)ctrAuxiliar2.personal.clave,
                                    activo = true,
                                    posicion = null
                                });
                    }
                    else
                    {
                        cbxChcPosLlantas.Focus();
                    }
                }
                else
                {
                    selectActividades buscador = new selectActividades(txtBuscadorActividad.Text.Trim());
                    act = buscador.getActividad(lista);
                    mapCampoActividad(act);
                    if (act != null)
                    {
                        if (cab.enumTipoOrdServ != enumTipoOrdServ.LLANTAS)
                        {
                            agregarActividad(
                                new ActividadOrdenServicio
                                {
                                    idActividadOrdenServicio = 0,
                                    usuario = mainWindow.usuario.nombreUsuario,
                                    idPadreOrdenServicio = cabOrdenServicio.idPadreOrdSer,
                                    idOrdenServicio = cabOrdenServicio.idOrdenSer,
                                    enumTipoOrdServ = cabOrdenServicio.enumTipoOrdServ,
                                    actividad = act,
                                    servicio = null,
                                    idResponsable = ctrResponsable.personal == null ? null : (int?)ctrResponsable.personal.clave,
                                    idAuxiliar1 = ctrAuxiliar1.personal == null ? null : (int?)ctrAuxiliar1.personal.clave,
                                    idAuxiliar2 = ctrAuxiliar2.personal == null ? null : (int?)ctrAuxiliar2.personal.clave,
                                    activo = true,
                                    posicion = null
                                });
                        }
                        else
                        {
                            cbxChcPosLlantas.Focus();
                        }
                    }
                }
            }
        }
        void mapCampoActividad(Actividad actividad)
        {
            if (actividad != null)
            {
                txtBuscadorActividad.Tag = actividad;
                txtBuscadorActividad.Text = actividad.nombre;
                txtBuscadorActividad.IsEnabled = false;
            }
            else
            {
                txtBuscadorActividad.Tag = null;
                txtBuscadorActividad.Clear();
                txtBuscadorActividad.IsEnabled = true;
            }
        }
        void agregarActividad(ActividadOrdenServicio actividadOrdenServicio)
        {
            try
            {
                StackPanel stpControl = new StackPanel { Orientation = Orientation.Horizontal, HorizontalAlignment = HorizontalAlignment.Left, Tag = actividadOrdenServicio };
                TextBox lblActividad = new TextBox
                {
                    Text = actividadOrdenServicio.descripcion,
                    FontWeight = FontWeights.Bold,
                    Foreground = Brushes.SteelBlue,
                    TextWrapping = TextWrapping.Wrap,
                    Width = 350,
                    Background = null,
                    IsReadOnly = true,
                    BorderBrush = null
                };
                stpControl.Children.Add(lblActividad);

                Label lblPosicion = new Label
                {
                    Content = !actividadOrdenServicio.posicion.HasValue || actividadOrdenServicio.posicion == 0 ? string.Empty : actividadOrdenServicio.posicion.ToString(),
                    FontWeight = FontWeights.Bold,
                    Foreground = Brushes.SteelBlue,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Width = 100
                };
                stpControl.Children.Add(lblPosicion);

                if (actividadOrdenServicio.servicio == null)
                {
                    Button btnBorrar = new Button
                    {
                        Content = actividadOrdenServicio.idActividadPendiente != 0 ? ("CANCELAR") : (actividadOrdenServicio.idActividadOrdenServicio != 0 ? "CANCELAR" : "BORRAR"),
                        VerticalAlignment = VerticalAlignment.Center,
                        HorizontalAlignment = HorizontalAlignment.Center,
                        Width = 120,
                        Foreground = Brushes.White,
                        Background = actividadOrdenServicio.idActividadPendiente != 0 ? (Brushes.Red) : (actividadOrdenServicio.idActividadOrdenServicio != 0 ? Brushes.Red : Brushes.DarkOrange),
                        Tag = stpControl,
                        DataContext = actividadOrdenServicio
                    };
                    btnBorrar.Click += BtnBorrar_Click;
                    stpControl.Children.Add(btnBorrar);
                }

                stpAtividadesOrdenServ.Children.Add(stpControl);
                mapCampoActividad(null);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void BtnBorrar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    Button button = sender as Button;
                    StackPanel stp = button.Tag as StackPanel;
                    ActividadOrdenServicio actividadOrden = button.DataContext as ActividadOrdenServicio;

                    if (actividadOrden.idActividadOrdenServicio != 0 || actividadOrden.idActividadPendiente != 0)
                    {
                        var respMotivo = new seleccionarMotivoCancelacion(enumProcesos.ORDENES_DE_TRABAJO).obtenerMotivo();
                        if (respMotivo.typeResult == ResultTypes.success)
                        {
                            actividadOrden.motivoCancelacionObj = respMotivo.result as MotivoCancelacion;
                            actividadOrden.motivoCancelacion = respMotivo.mensaje;
                            actividadOrden.usuarioCancela = mainWindow.usuario.nombreUsuario;
                            var resp = new SolicitudOrdenServicioSvc().cancelarActividadOrdenServicio(actividadOrden);
                            if (resp.typeResult == ResultTypes.success)
                            {
                                stpAtividadesOrdenServ.Children.Remove(stpAtividadesOrdenServ.Children.Cast<StackPanel>().ToList().Find(s => s == stp));
                            }
                            else
                            {
                                ImprimirMensaje.imprimir(resp);
                                return;
                            }
                        }
                        //string str = new ventanaInputTexto("Motivo De Cancelacion", "Proporcionar el motivo de la cancelación").obtenerRespuesta();

                        //if (!string.IsNullOrEmpty(str))
                        //{

                        //}
                    }
                    else
                    {
                        stpAtividadesOrdenServ.Children.Remove(stpAtividadesOrdenServ.Children.Cast<StackPanel>().ToList().Find(s => s == stp));
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void btnAddActividad_Click(object sender, RoutedEventArgs e)
        {
            if (txtBuscadorActividad.Tag == null) return;
            if (cbxChcPosLlantas.SelectedItems.Count == 0) return;

            CabOrdenServicio cabOrdenServicio = ctrFolioOrdenServicio.Tag as CabOrdenServicio;

            Actividad actividad = txtBuscadorActividad.Tag as Actividad;
            foreach (int pos in cbxChcPosLlantas.SelectedItems)
            {
                agregarActividad(
                    new ActividadOrdenServicio
                    {
                        idActividadOrdenServicio = 0,
                        usuario = mainWindow.usuario.nombreUsuario,
                        idPadreOrdenServicio = cabOrdenServicio.idPadreOrdSer,
                        idOrdenServicio = cabOrdenServicio.idOrdenSer,
                        enumTipoOrdServ = cabOrdenServicio.enumTipoOrdServ,
                        actividad = actividad,
                        servicio = null,
                        idResponsable = ctrResponsable.personal == null ? null : (int?)ctrResponsable.personal.clave,
                        idAuxiliar1 = ctrAuxiliar1.personal == null ? null : (int?)ctrAuxiliar1.personal.clave,
                        idAuxiliar2 = ctrAuxiliar2.personal == null ? null : (int?)ctrAuxiliar2.personal.clave,
                        activo = true,
                        posicion = pos
                    });
            }
        }

        private void btnPersonalRecibe_Click(object sender, RoutedEventArgs e)
        {
            ctrPersonalRecibe.limpiar();
        }

        private void BtnCambiarResponsable_Click(object sender, RoutedEventArgs e)
        {
            ctrResponsable.limpiar();
        }

        private void BtnCambiarA_Click(object sender, RoutedEventArgs e)
        {
            ctrAuxiliar1.limpiar();
        }

        private void BtnCambiarAuxiliar2_Click(object sender, RoutedEventArgs e)
        {
            ctrAuxiliar2.limpiar();
        }
    }
}
