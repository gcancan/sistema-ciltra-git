﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectCasetas.xaml
    /// </summary>
    public partial class selectCasetas : Window
    {
        Estado estado = null;
        public selectCasetas()
        {
            InitializeComponent();
            cargarEstados();
        }
        public selectCasetas(Estado estado)
        {
            InitializeComponent();
            this.estado = estado;
            cargarEstados();
        }
        public List<Caseta> getListaCasetas()
        {
            try
            {
                bool? resp = ShowDialog();
                if (resp.Value && (lvlSelect.Items.Count > 0))
                {
                    return lvlSelect.Items.Cast<Caseta>().ToList();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }
        private bool cargarEstados()
        {
            try
            {
                OperationResult resp = new EstadoSvc().getEstadosAllorById();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxEstados.ItemsSource = resp.result as List<Estado>;
                    int i = 0;
                    if (estado == null)
                    {
                        cbxEstados.SelectedIndex = 0;
                        chcTodos.IsChecked = true;
                    }
                    else
                    {
                        foreach (Estado item in cbxEstados.Items)
                        {
                            if (item.idEstado == estado.idEstado)
                            {
                                cbxEstados.SelectedIndex = i;
                                chcTodos.IsChecked = true;
                                break;
                            }
                            i++;
                        }
                    }                    
                    return true;
                }
                else
                {
                    imprimir(resp);
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            buscarByEstado();
        }
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            buscarByEstado();
        }
        public bool buscarByEstado()
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult resp = new CasetaSvc().getCasetasByIdEstado(chcTodos.IsChecked.Value ? 0 : (cbxEstados.SelectedItem as Estado).idEstado);
                if (resp.typeResult == ResultTypes.success)
                {
                    lvlCasetas.ItemsSource = resp.result as List<Caseta>;
                    CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlCasetas.ItemsSource);
                    PropertyGroupDescription groupDescription = new PropertyGroupDescription("estado.nombre");
                    view.GroupDescriptions.Add(groupDescription);
                    view.Filter = UserFilter;
                    return true;
                }
                else
                {
                    imprimir(resp);
                    return false;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return false;
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void txtFiltro_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (lvlCasetas.ItemsSource != null)
            {
                CollectionViewSource.GetDefaultView(lvlCasetas.ItemsSource).Refresh();
            }
        }
        private bool UserFilter(object item)
        {
            return (item as Caseta).via.IndexOf(txtFiltro.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                    (item as Caseta).nombreCaseta.IndexOf(txtFiltro.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }
        private void lvlCasetas_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
            {
                Caseta caseta = (sender as ListView).SelectedItem as Caseta;
                agregarLista(caseta);
            }
        }    
        
        private void agregarLista(Caseta caseta)
        {
            //var list = lvlSelect.Items.Cast<Caseta>().ToList();
            //if (list.Find(s => s.idCaseta == caseta.idCaseta) == null)
            //{
                lvlSelect.Items.Add(caseta);
            //}
        }
        private void chcTodos_Checked(object sender, RoutedEventArgs e)
        {
            cbxEstados.IsEnabled = false;
        }

        private void chcTodos_Unchecked(object sender, RoutedEventArgs e)
        {
            cbxEstados.IsEnabled = true;
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
