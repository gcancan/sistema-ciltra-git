﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoreFletera.Models;
using Core.Models;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para ConfirmarSincronizacionClientes.xaml
    /// </summary>
    public partial class ConfirmarSincronizacionClientes : Window
    {
        Empresa empresa = null; List<Cliente> listaCliente = null;
        public ConfirmarSincronizacionClientes(Empresa empresa, List<Cliente> listaCliente)
        {
            InitializeComponent();
            this.empresa = empresa;
            this.listaCliente = listaCliente;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            lblEmpresa.Content = empresa.nombre;
            lvlClientes.ItemsSource = listaCliente;
        }

        public bool? confirmarSincronizacion()
        {
            try
            {
                return ShowDialog();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return false;
            }
        }

        private void btnSi_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void btnNo_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
