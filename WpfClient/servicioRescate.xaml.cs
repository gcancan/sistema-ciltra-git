﻿using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for servicioRescate.xaml
    /// </summary>
    public partial class servicioRescate : ViewBase
    {
        public servicioRescate()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            txtUnidad.loaded(etipoUniadBusqueda.TODAS, null);
            txtChofer.loaded(eTipoBusquedaPersonal.OPERADORES, null);
            txtLlantera.loaded(eTipoBusquedaPersonal.LLANTEROS, null);
            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            mapForm(null);
        }

        private void txtLlanta_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DetalleRescateLlanta detalleResc = new DetalleRescateLlanta();
                var resp = new LlantaSvc().getLlantasEnAlmacen((int)EnumAlmacenLlantas.USADAS, ("%" + txtLlanta.Text.Trim() + "%"));
                if (resp.typeResult == ResultTypes.success)
                {                    
                    if (((List<Llanta>)resp.result).Count == 1)
                    {
                        detalleResc.llantaLleva = ((List<Llanta>)resp.result)[0];
                        agregarDetalleRescate(detalleResc);
                    }
                    else
                    {
                        var scl = new selectLlantas(txtLlanta.Text.Trim());
                        var ll = scl.getLlantasAlmacen((int)EnumAlmacenLlantas.USADAS);
                        if (ll != null)
                        {
                            detalleResc.llantaLleva = ll;
                            agregarDetalleRescate(detalleResc);
                        }
                    }
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    var scl = new selectLlantas("");
                    var ll = scl.getLlantasAlmacen();
                    if (ll != null)
                    {
                        detalleResc.llantaLleva = ll;
                        agregarDetalleRescate(detalleResc);
                    }
                }
            }
        }

        void agregarDetalleRescate(DetalleRescateLlanta detalle)
        {
            RadioButton rbn = new RadioButton { Margin = new Thickness(0), Tag = detalle, FontWeight = FontWeights.Medium};
            StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal };

            Label lblLlanta1 = new Label
            {
                Content = detalle.llantaLleva.clave,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                Width = 120,
                Margin = new Thickness(2)
            };
            stpContent.Children.Add(lblLlanta1);

            Label lblLlanta2 = new Label
            {
                Content = detalle.llantaTrae == null ? "" : detalle.llantaTrae.clave,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                Width = 120,
                Margin = new Thickness(2),
                Name = "lblLlanta2"
            };
            stpContent.Children.Add(lblLlanta2);

            Label lblPosicion = new Label
            {
                Content = detalle.posicion,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                Width = 60,
                Margin = new Thickness(2),
                Name = "lblPosicion"
            };
            stpContent.Children.Add(lblPosicion);

            controlDecimal ctrDecimal = new controlDecimal
            {
                IsEnabled = detalle.idDetalleRescateLlanta != 0,
                valor = detalle.profundidad
            };
            stpContent.Children.Add(ctrDecimal);

            controlMotivoRescate ctrMotivoRescate = new controlMotivoRescate
            {
                IsEnabled = detalle.idDetalleRescateLlanta != 0                
            };
            ctrMotivoRescate.cargarControl();
            ctrMotivoRescate.motivoRescateSelect = detalle.motivoRescate;
            stpContent.Children.Add(ctrMotivoRescate);
            
            Button btnQuitar = new Button
            {
                Tag = detalle,
                Content = "QUITAR",
                Foreground = Brushes.White,
                Background  = Brushes.Red,
                Width = 120,
                Margin = new Thickness(2) ,
                IsEnabled = detalle.idDetalleRescateLlanta == 0
            };
            btnQuitar.Click += BtnQuitar_Click;
            stpContent.Children.Add(btnQuitar);

            rbn.Content = stpContent;
            stpDetalles.Children.Add(rbn);
        }

        private void BtnQuitar_Click(object sender, RoutedEventArgs e)
        {
            DetalleRescateLlanta detResc = (DetalleRescateLlanta)((Button)sender).Tag;
            foreach (RadioButton rbn in stpDetalles.Children.Cast<RadioButton>().ToList())
            {
                DetalleRescateLlanta det = (DetalleRescateLlanta)rbn.Tag;
                if (det.llantaLleva.idLlanta == detResc.llantaLleva.idLlanta)
                {
                    stpDetalles.Children.Remove(rbn);
                }
            }
        }

        public override OperationResult guardar()
        {
            try
            {
                var respVal = validarForm();
                if (respVal.typeResult != ResultTypes.success)
                {
                    return respVal;
                }
                RescateLlantas rescate = mapForm();
                OperationResult resp = new RescateLlantasSvc().saveRescateLlantas(ref rescate);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(rescate);
                    if (rescate.estatus == "ACTIVO")
                    {
                        mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.editar| ToolbarCommands.cerrar);
                    }
                    else
                    {
                        mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                    }
                    
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        private void mapForm(RescateLlantas rescate)
        {
            if (rescate != null )
            {
                txtFolio.Text = rescate.idRescateLlanta.ToString();
                txtFolio.Tag = rescate;
                txtFolio.IsEnabled = false;
                txtUnidad.unidadTransporte = rescate.unidadTransporte;
                dtpFechaSolicitud.Value = rescate.fechaSolicitud;
                dtpFechaSolicitud.IsEnabled = false;
                dtpFechaSalida.Value = rescate.fechaSalida;
                dtpFechaSalida.IsEnabled = false;
                dtpFechaEntrada.Value = rescate.fechaEntrada;
                dtpFechaEntrada.IsEnabled = rescate.fechaEntrada == null;
                txtChofer.personal = rescate.chofer;
                txtLlantera.personal = rescate.Llantero;
                txtUbicacion.Text = rescate.unicacion;
                txtUbicacion.IsEnabled = false;
                txtEstatus.Text = rescate.estatus;
                stpDetalles.Children.Clear();
                foreach (var item in rescate.listaDetalles)
                {
                    agregarDetalleRescate(item);
                }
                stpBuscarLlanta.IsEnabled = false;

                var r = new DiagramasLLantasSvc().getDiagramaByTipoUnidadTrans(rescate.unidadTransporte.tipoUnidad);
                if (r.typeResult != ResultTypes.success)
                {
                    ImprimirMensaje.imprimir(r);
                }
                else
                {
                    cargarDiagrama((DiagramaLLantas)r.result, rescate.unidadTransporte);
                }
                if (rescate.estatus == "TERMINADO")
                {
                    mainWindow.scrollContainer.IsEnabled = false;
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                }
            }
            else
            {
                txtFolio.Clear();
                txtFolio.Tag = rescate;
                txtFolio.IsEnabled = true;
                txtUnidad.limpiar();
                dtpFechaSolicitud.Value = DateTime.Now;
                dtpFechaSolicitud.IsEnabled = true;
                dtpFechaSalida.Value = DateTime.Now;
                dtpFechaSalida.IsEnabled = true;
                dtpFechaEntrada.Value = null;
                dtpFechaEntrada.IsEnabled = false;
                txtChofer.limpiar();
                txtLlantera.limpiar();
                txtUbicacion.Clear();
                txtUbicacion.IsEnabled = true;
                txtEstatus.Text = "NUEVO";
                stpDetalles.Children.Clear();
                stpBuscarLlanta.IsEnabled = true;
                mainWindow.scrollContainer.IsEnabled = true;

                eje1.Children.Clear();
                eje2.Children.Clear();
                eje3.Children.Clear();
                eje4.Children.Clear();

                txtFolio.Focus();
            }

        }

        private void cargarDiagrama(DiagramaLLantas diagrama, UnidadTransporte unidadTransporte)
        {
            eje1.Children.Clear();
            eje2.Children.Clear();
            eje3.Children.Clear();
            eje4.Children.Clear();
            var listaStps = stpDiagrama.Children.Cast<StackPanel>().ToList();

            string path = System.IO.Directory.GetCurrentDirectory();
            int posicion = 0;
            for (int i = 1; i <= diagrama.numEjes; i++)
            {
                int llantas = diagrama.listaDetalles[i - 1].llantas;
                for (int j = 1; j <= llantas; j++)
                {
                    posicion++;
                    /**/
                    Image img = new Image();
                    img.Height = 40;
                    BitmapImage ima = new BitmapImage(new Uri(path + "\\Recursos\\BalanceNew.png"));
                    Label lblLLanta = new Label();
                    //chc.VerticalContentAlignment = VerticalAlignment.Bottom;
                    StackPanel stp = new StackPanel() { Orientation = Orientation.Vertical };
                    stp.Children.Add(new Label { Content = posicion, HorizontalContentAlignment = HorizontalAlignment.Center });
                    img.Source = ima;
                    stp.Children.Add(img);
                    Label lblTitulo = new Label() { FontWeight = FontWeights.Medium };
                    var respLLanta = new LlantaSvc().getLLantaByPosAndUnidadTrans(posicion, unidadTransporte);
                    if (respLLanta.typeResult == ResultTypes.success)
                    {
                        Llanta llanta = (Llanta)respLLanta.result;
                        lblTitulo.Content = llanta.clave;
                        lblLLanta.Tag = llanta;
                        StackPanel stpToolTip = new StackPanel { Orientation = Orientation.Vertical };
                        stpToolTip.Children.Add(new Label { Content = "INFORMACIÓN DE LA LLANTA", FontWeight = FontWeights.Bold, FontSize = 16 });

                        StackPanel stpLabels = new StackPanel();

                        StackPanel stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "UBICACIÓN:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.unidadTransporte.clave + " POS: " + llanta.posicion.ToString() });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "MARCA:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.marca.descripcion });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "TIPO LLANTA:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.tipoLlanta.nombre });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "DISEÑO:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.diseño });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "MEDIDA:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.medida });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "PROFUNDIDAD:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.profundidad });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "FECHA:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.fecha });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "RFID:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.rfid });
                        stpLabels.Children.Add(stpControl);

                        stpToolTip.Children.Add(stpLabels);
                        lblLLanta.ToolTip = new ToolTip { AllowDrop = true, Content = stpToolTip /*"LLANTA " + ((Llanta)respLLanta.result).clave*/ };
                        lblLLanta.MouseDoubleClick += LblLLanta_MouseDoubleClick;
                    }
                    else
                    {
                        //lblLLanta.ToolTip = new ToolTip { Content = "SIN ASIGNAR" };
                        //ContextMenu menu = new ContextMenu();
                        //MenuItem itemAgregar = new MenuItem { Header = "Asignar llanta" };
                        //menu.Items.Add(itemAgregar);
                        //lblLLanta.ContextMenu = menu;
                        lblTitulo.Content = "SIN ASIGNAR";

                    }
                    stp.Children.Add(lblTitulo);
                    lblLLanta.Content = stp;
                    listaStps[i - 1].Children.Add(lblLLanta);
                    /**/
                }
            }
        }

        private void LblLLanta_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Llanta llanta = (Llanta)((Label)sender).Tag;
            agregarLlantaADetalle(llanta);
        }

        private void agregarLlantaADetalle(Llanta llanta)
        {
            foreach (RadioButton item in stpDetalles.Children.Cast<RadioButton>().ToList())
            {
                if (item.IsChecked.Value)
                {
                    foreach (object obj in ((StackPanel)item.Content).Children.Cast<object>().ToList())
                    {
                        if (obj is Label)
                        {
                            Label lbl = (Label)obj;
                            lbl.Tag = llanta;
                            switch (lbl.Name)
                            {
                                case "lblLlanta2":
                                    lbl.Content = llanta.clave;                                    
                                    break;
                                case "lblPosicion":
                                    lbl.Content = llanta.posicion;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
        }

        private RescateLlantas mapForm()
        {
            try
            {
                RescateLlantas rescate = new RescateLlantas
                {
                    idRescateLlanta = txtFolio.Tag == null ? 0 : ((RescateLlantas)txtFolio.Tag).idRescateLlanta,
                    chofer = txtChofer.personal,
                    Llantero = txtLlantera.personal,
                    estatus = txtEstatus.Text == "NUEVO" ? "ACTIVO" : "TERMINADO",
                    fechaSolicitud = (DateTime)dtpFechaSolicitud.Value,
                    fechaSalida  = (DateTime)dtpFechaSalida.Value,
                    fechaEntrada = txtEstatus.Text == "NUEVO" ? null : dtpFechaEntrada.Value,
                    unicacion = txtUbicacion.Text.Trim(),
                    unidadTransporte = txtUnidad.unidadTransporte,
                    usuario = mainWindow.inicio._usuario.nombreUsuario
                };
                rescate.listaDetalles = new List<DetalleRescateLlanta>();
                rescate.listaDetalles = getListaDetalles();
                return rescate;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }

        public OperationResult validarForm()
        {
            try
            {
                OperationResult result = new OperationResult { valor = 0, mensaje = "PARA CONTINUAR REALICE LO SIGUIENTE:" + Environment.NewLine };
                if (txtUnidad.unidadTransporte == null)
                {
                    result.valor = 2;
                    result.mensaje += " * Seleccionar una Unidad De Transporte." + Environment.NewLine;
                }

                if (txtChofer.personal == null)
                {
                    result.valor = 2;
                    result.mensaje += " * Seleccionar un Llantero." + Environment.NewLine;
                }
                if (txtEstatus.Text == "NUEVO")
                {
                    if (dtpFechaSalida.Value == null)
                    {
                        result.valor = 2;
                        result.mensaje += " * Seleccionar la fecha de Salida." + Environment.NewLine;
                    }
                }
                if (txtEstatus.Text == "ACTIVO")
                {
                    if (dtpFechaEntrada.Value == null)
                    {
                        result.valor = 2;
                        result.mensaje += " * Selecionar la fecha de Entrada." + Environment.NewLine;
                    }
                }

                if (string.IsNullOrEmpty(txtUbicacion.Text.Trim()))
                {
                    result.valor = 2;
                    result.mensaje += " * Proporcionar una ubicación." + Environment.NewLine;
                }
                var lista = getListaDetalles();
                if (lista.Count == 0)
                {
                    result.valor = 2;
                    result.mensaje += " * Elegir al menos una llanta para llevar." + Environment.NewLine;
                }
                if (txtEstatus.Text == "ACTIVO")
                {
                    foreach (var item in lista)
                    {
                        if (item.llantaTrae != null)
                        {
                            if (item.profundidad <= 0)
                            {
                                result.valor = 2;
                                result.mensaje += string.Format( " * La profundidad de la llanta {0} no puede ser menor o igual a 0.", item.llantaTrae.clave) + Environment.NewLine;
                            }
                            if (item.motivoRescate == null)
                            {
                                result.valor = 2;
                                result.mensaje += string.Format( " * No se selecciono el motivo del rescate de la Llanta {0}.", item.llantaTrae.clave) + Environment.NewLine;
                            }
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        private List<DetalleRescateLlanta> getListaDetalles()
        {
            List<DetalleRescateLlanta> lista = new List<DetalleRescateLlanta>();
            if (txtEstatus.Text == "NUEVO")
            {
                foreach (RadioButton item in stpDetalles.Children.Cast<RadioButton>().ToList())
                {
                    lista.Add((DetalleRescateLlanta)item.Tag);
                }
            }
            else
            {
                foreach (RadioButton item in stpDetalles.Children.Cast<RadioButton>().ToList())
                {
                    DetalleRescateLlanta det = (DetalleRescateLlanta)item.Tag;
                    StackPanel stpContent = (StackPanel)item.Content;
                    foreach (object obj in stpContent.Children.Cast<object>().ToList())
                    {
                        if (obj is Label)
                        {
                            Label lbl = (Label)obj;
                            if ((Llanta)lbl.Tag != null)
                            {
                                Llanta llanta = (Llanta)lbl.Tag;
                                switch (lbl.Name)
                                {
                                    case "lblLlanta2":
                                        det.llantaTrae = llanta;
                                        break;
                                    case "lblPosicion":
                                        det.posicion = llanta.posicion;
                                        break;
                                    default:
                                        break;
                                }
                            }
                            
                        }
                        if (obj is controlDecimal)
                        {
                            det.profundidad = ((controlDecimal)obj).valor;
                        }
                        if (obj is controlCondicionLlantas)
                        {
                            det.condicion = ((controlCondicionLlantas)obj).condicionSelect;
                        }
                        if (obj is controlMotivoRescate)
                        {
                            det.motivoRescate = ((controlMotivoRescate)obj).motivoRescateSelect;
                        }
                    }

                    lista.Add(det);
                }
            }
            return lista;
        }

        private void txtFolio_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrEmpty(txtFolio.Text.Trim()))
                {
                    int i;
                    if (int.TryParse(txtFolio.Text.Trim(), out i))
                    {
                        buscarRescateById(i);
                    }
                }
            }
        }

        private void buscarRescateById(int i)
        {
            OperationResult resp = new RescateLlantasSvc().getRescateByid(i);
            if (resp.typeResult == ResultTypes.success)
            {
                mapForm((RescateLlantas)resp.result);
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
            }
        }
    }
}
