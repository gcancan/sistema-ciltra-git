﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editTramos.xaml
    /// </summary>
    public partial class editTramos : ViewBase
    {
        public editTramos()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                cargarOrigenes();
                cargarDestinos();
                cargarTramos();
                nuevo();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void cargarTramos()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new TramoSvc().getAllTramos();
                if (resp.typeResult == ResultTypes.success)
                {

                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
        }
        List<Zona> listZonas = new List<Zona>();
        private void cargarDestinos()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new ZonasSvc().getDestinosAtlante(mainWindow.empresa.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    listZonas = resp.result as List<Zona>;
                    cbxDestino.ItemsSource = listZonas;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        List<Origen> listOrigenes = new List<Origen>();
        void cargarOrigenes()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new OrigenSvc().getOrigenesAll();
                if (resp.typeResult == ResultTypes.success)
                {
                    listOrigenes = resp.result as List<Origen>;
                    cbxOrigen.ItemsSource = listOrigenes;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                Tramo tramo = mapForm();
                if (tramo != null)
                {
                    var resp = new TramoSvc().saveTramo(ref tramo);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mapForm(tramo);
                        mainWindow.habilitar = true;
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult(ResultTypes.warning, "Ocurrio un error al leer la información de la pantalla");
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapForm(Tramo tramo)
        {
            if (tramo != null)
            {
                txtCve.Tag = tramo;
                selectDestino(tramo.destino);
                selectOrigen(tramo.origen);
                txtKm.valor = tramo.km;
            }
            else
            {
                txtCve.Tag = null;
                cbxDestino.SelectedItem = 0;
                cbxOrigen.SelectedItem = 0;
                txtKm.valor = 0;
            }
        }
        private void selectOrigen(Origen origen)
        {
            int i = 0;
            foreach (Origen item in cbxOrigen.Items)
            {
                if (item.idOrigen == origen.idOrigen)
                {
                    cbxOrigen.SelectedIndex = i;
                    cbxOrigen.IsEnabled = false;
                    break;
                }
                i++;
            }
        }
        private void selectDestino(Zona zona)
        {
            int i = 0;
            foreach (Zona item in cbxDestino.Items)
            {
                if (item.clave == zona.clave)
                {
                    cbxDestino.SelectedIndex = i;
                    cbxDestino.IsEnabled = false;
                    break;
                }
                i++;
            }
        }

        private Tramo mapForm()
        {
            try
            {
                return new Tramo
                {
                    idTramo = txtCve.Tag == null ? 0 : (txtCve.Tag as Tramo).idTramo,
                    destino = cbxDestino.SelectedItem as Zona,
                    origen = cbxOrigen.SelectedItem as Origen,
                    km = txtKm.valor
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
