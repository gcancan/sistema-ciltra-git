﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para DespachoDiesel.xaml
    /// </summary>
    public partial class DespachoDiesel : ViewBase
    {
        public DespachoDiesel()
        {
            InitializeComponent();
        }
        Usuario usuario = null;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;

                OperationResult result = new CargaCombustibleSvc().getProveedor(0);
                if (result.typeResult == ResultTypes.success)
                {
                    this.txtProveedorCarga.ItemsSource = (List<ProveedorCombustible>)result.result;
                }
                else
                {
                    this.IsEnabled = false;
                    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    return;
                }
                dtpFechaCombustible.Value = DateTime.Now;
                usuario = mainWindow.usuario;
                ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
                ctrEmpresa.loaded(usuario.empresa, new PrivilegioSvc().consultarPrivilegio("isAdmin", base.mainWindow.usuario.idUsuario).typeResult == ResultTypes.success);

                ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
                ctrZonaOperativa.cargarControl(usuario.zonaOperativa, usuario.idUsuario);
                //ctrKmInicial.valor = 10000;
                ctrTractor.DataContextChanged += CtrTractor_DataContextChanged;

                txtNombreImpresora.Text = new Core.Utils.Util().getRutasImpresora("VIAJES_ATLANTE");
                //this.establecerImpresora();
                //chcImprimir.IsChecked = true;
                nuevo();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        internal class Existencias
        {
            public string empresa { get; set; }
            public decimal existencia { get; set; }
        }
        DateTime? fechaMinima = null;
        private void buscarExistencias()
        {
            try
            {
                if (this.txtClave.Tag != null)
                {
                    NivelesTanque tag = this.txtClave.Tag as NivelesTanque;
                    base.Cursor = Cursors.Wait;
                    OperationResult resp = new CargaCombustibleSvc().getExistenciasDieselActual(tag.tanqueCombustible.idTanqueCombustible);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        Dictionary<string, decimal> result = resp.result as Dictionary<string, decimal>;
                        List<Existencias> list = new List<Existencias>();
                        foreach (KeyValuePair<string, decimal> pair in result)
                        {
                            Existencias item = new Existencias
                            {
                                empresa = pair.Key,
                                existencia = pair.Value
                            };
                            list.Add(item);
                        }
                        this.lvlExistencias.ItemsSource = list;
                        lblSumaExistencias.Content = list.Sum(s => s.existencia).ToString("N4");
                    }
                    else
                    {
                        this.lvlExistencias.ItemsSource = null;
                        ImprimirMensaje.imprimir(resp);
                        lblSumaExistencias.Content = decimal.Zero.ToString("N4");
                    }
                }
                else
                {
                    this.lvlExistencias.ItemsSource = null;
                    lblSumaExistencias.Content = decimal.Zero.ToString("N4");
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        private bool establecerImpresora()
        {
            try
            {
                string str = new Core.Utils.Util().getRutasImpresora("VIAJES_ATLANTE");
                if (string.IsNullOrEmpty(str))
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "Se requiere seleccionar la impresora"
                    };
                    //ImprimirMensaje.imprimir(resp);
                    System.Windows.Forms.PrintDialog dialog = new System.Windows.Forms.PrintDialog();
                    if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        new Core.Utils.Util().setRutasImpresora("VIAJES_ATLANTE", dialog.PrinterSettings.PrinterName);
                        return this.establecerImpresora();
                    }
                    return false;
                }
                this.txtNombreImpresora.Text = str;
                return true;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return false;
            }
        }
        private void CtrTractor_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                if (ctrTractor.unidadTransporte != null)
                {
                    var resp = new RegistroKilometrajeSvc().getUltimoKM(ctrTractor.unidadTransporte.clave, dtpFechaCombustible.Value.Value);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        ctrKmInicial.valor = Convert.ToDecimal(resp.result);
                    }
                    else
                    {
                        ctrKmInicial.valor = 0;
                    }
                    cargarInfoUltimoViaje();
                }
                else
                {
                    ctrKmInicial.valor = 0;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public void cargarInfoUltimoViaje()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new CartaPorteSvc().getDatosUltimoViajeByTC(ctrTractor.unidadTransporte.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    ctrRemolque1.limpiar();
                    ctrDolly.limpiar();
                    ctrRemolque2.limpiar();
                    UltimoViaje ultimoViaje = resp.result as UltimoViaje;
                    var respRemolque = new UnidadTransporteSvc().getUnidadesALLorById(0, ultimoViaje.idRemolque);
                    if (respRemolque.typeResult == ResultTypes.success)
                    {
                        ctrRemolque1.unidadTransporte = (respRemolque.result as List<UnidadTransporte>)[0];
                    }

                    if (!string.IsNullOrEmpty(ultimoViaje.idDolly))
                    {
                        var respDolly = new UnidadTransporteSvc().getUnidadesALLorById(0, ultimoViaje.idDolly);
                        if (respDolly.typeResult == ResultTypes.success)
                        {
                            ctrDolly.unidadTransporte = (respDolly.result as List<UnidadTransporte>)[0];
                        }
                    }

                    if (!string.IsNullOrEmpty(ultimoViaje.idRemolque2))
                    {
                        var respRemolque2 = new UnidadTransporteSvc().getUnidadesALLorById(0, ultimoViaje.idRemolque2);
                        if (respRemolque2.typeResult == ResultTypes.success)
                        {
                            ctrRemolque2.unidadTransporte = (respRemolque2.result as List<UnidadTransporte>)[0];
                        }
                    }

                    var respOperador = new OperadorSvc().getOperadoresALLorById(ultimoViaje.idOperador);
                    if (respOperador.typeResult == ResultTypes.success)
                    {
                        ctrChofer.personal = (respOperador.result as List<Personal>)[0];
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public override void nuevo()
        {
            base.nuevo();
            ctrTractor.limpiar();
            ctrRemolque1.limpiar();
            ctrDolly.limpiar();
            ctrRemolque2.limpiar();
            ctrChofer.limpiar();
            ctrDespachadores.limpiar();
            chcCargaExterna.IsChecked = false;
            ctrKmFinal.valor = 0;
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            txtTiempoTotal.Text = "000:00:00";
            txtTiempoPTO.Text = "00:00:00";
            txtTiempoEst.Text = "00:00:00";

            ctrLtsUtilizados.valor = 0;
            ctrLtsPTO.valor = 0;
            ctrLtsEST.valor = 0;

            ctrLtsCargados.valor = 0;

            ctrSello1.valor = 0;
            ctrSello2.valor = 0;
            ctrSelloReserva.valor = 0;

            ctrDespachadores.personal = usuario.personal;

            txtTiketCarga.Clear();
            txtProveedorCarga.SelectedItem = null;
            txtPrecioCarga.valor = 0;
            txtFolioImpresoCarga.Clear();
            txtObservaciones.Clear();
            ctrRecorridos.valor = 0;
            dtpFechaCombustible.Value = DateTime.Now;

            chcConECM.IsChecked = true;
            chcConECM.IsChecked = false;
            chcHubodometro.IsChecked = true;

        }
        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                Empresa empresa = ctrEmpresa.empresaSelected;
                ctrTractor.loaded(etipoUniadBusqueda.TRACTOR, empresa);
                ctrRemolque1.loaded(etipoUniadBusqueda.TOLVA, empresa);
                ctrDolly.loaded(etipoUniadBusqueda.DOLLY, empresa);
                ctrRemolque2.loaded(etipoUniadBusqueda.TOLVA, empresa);
                ctrChofer.loaded(eTipoBusquedaPersonal.OPERADORES, empresa);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (this.ctrZonaOperativa.zonaOperativa != null)
                {
                    this.getNivelesByZonaOperativa(this.ctrZonaOperativa.zonaOperativa);
                    buscarExistencias();
                    Empresa empresa = ctrEmpresa.empresaSelected;
                    ZonaOperativa zona = ctrZonaOperativa.zonaOperativa;

                    ctrTractor.loaded(etipoUniadBusqueda.TRACTOR, empresa, zona);
                    ctrRemolque1.loaded(etipoUniadBusqueda.TOLVA, empresa, zona);
                    ctrDolly.loaded(etipoUniadBusqueda.DOLLY, empresa, zona);
                    ctrRemolque2.loaded(etipoUniadBusqueda.TOLVA, empresa, zona);

                    cargarOperaciones();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void cargarOperaciones()
        {
            try
            {
                Cursor = Cursors.Arrow;
                Empresa empresa = ctrEmpresa.empresaSelected;
                var resp = new ClienteSvc().getClientesByZonasOperativas(
                    new List<string> { ctrZonaOperativa.zonaOperativa.idZonaOperativa.ToString() }, empresa.clave);

                if (resp.typeResult == ResultTypes.success)
                {
                    cbxCliente.ItemsSource = (resp.result as List<OperacionFletera>).Select(s => s.cliente).ToList<Cliente>();
                    cbxCliente.SelectedItem = cbxCliente.ItemsSource.Cast<Cliente>().ToList().Find(s => s.clave == mainWindow.usuario.cliente.clave);
                }
                else
                {
                    cbxCliente.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void CbxCliente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (this.cbxCliente.SelectedItem != null)
                {
                    this.getNivelesByZonaOperativa(this.ctrZonaOperativa.zonaOperativa);
                    Empresa empresa = ctrEmpresa.empresaSelected;
                    ZonaOperativa zona = ctrZonaOperativa.zonaOperativa;
                    Cliente cliente = cbxCliente.SelectedItem as Cliente;
                    ctrTractor.loaded(etipoUniadBusqueda.TRACTOR, empresa, zona, cliente);
                    ctrRemolque1.loaded(etipoUniadBusqueda.TOLVA, empresa, zona, cliente);
                    ctrDolly.loaded(etipoUniadBusqueda.DOLLY, empresa, zona, cliente);
                    ctrRemolque2.loaded(etipoUniadBusqueda.TOLVA, empresa, zona, cliente);

                    //cargarOperaciones();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void getNivelesByZonaOperativa(ZonaOperativa zonaOperativa)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult resp = new NivelesTanqueSvc().getNivelTanqueByZonaOperativa(zonaOperativa);
                NivelesTanque nivelesTanque = resp.result as NivelesTanque;
                if (resp.typeResult == ResultTypes.success)
                {
                    if (nivelesTanque.fechaFinal.HasValue)
                    {
                        ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "NO ESTAN ABIERTOS LOS NIVELES", null));
                        base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    }
                    else
                    {
                        this.mapNivelTanque(nivelesTanque);
                        base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
                    }
                }
                else
                {
                    this.mapNivelTanque(null);
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        private void mapNivelTanque(NivelesTanque nivelesTanque)
        {
            fechaMinima = null;
            if (nivelesTanque != null)
            {
                this.txtClave.Tag = nivelesTanque;
                this.txtClave.valor = nivelesTanque.idNivel;
                txtTanque.Text = nivelesTanque.tanqueCombustible.nombre;
                this.txtFechaInicio.Text = nivelesTanque.fechaInicio.ToString("dd/MM/yyyy HH:mm:ss");
                buscarFechaInicio(nivelesTanque.tanqueCombustible.idTanqueCombustible);
            }
            else
            {
                this.txtClave.Tag = null;
                this.txtClave.valor = 0;
                txtTanque.Clear();
                this.txtFechaInicio.Clear();
            }
        }
        void buscarFechaInicio(int idTanque)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new SuministroCombustibleSvc().getNivelesIniciales(idTanque);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<NivelInicial> lista = resp.result as List<NivelInicial>;
                    fechaMinima = lista[0].fecha;
                }
                else
                {
                    fechaMinima = null;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public override OperationResult guardar()
        {
            try
            {
                if (chcImprimir.IsChecked.Value)
                {
                    if (!establecerImpresora())
                    {
                        return new OperationResult(ResultTypes.warning, "SE REQUIERE ESPECIFICAR LA IMPRESORA");
                    }
                }

                decimal existencia = Convert.ToDecimal(lblSumaExistencias.Content);
                if (ctrLtsCargados.valor > existencia)
                {
                    return new OperationResult(ResultTypes.warning, "SALDO DE INVENTARIO INSUFICIENTE");
                }
                if (ctrLtsCargados.valor <= 0)
                {
                    return new OperationResult(ResultTypes.warning, "LA SALIDA DE COMBUSTIBLE TIENE QUE SER MAYOR A 0");
                }

                var val = validar();
                if (val.typeResult != ResultTypes.success)
                {
                    return val;
                }

                var masterOrdenServ = mapForm();

                if (fechaMinima != null)
                {
                    if (masterOrdenServ.cargaCombustible.fechaCombustible < fechaMinima)
                    {
                        return new OperationResult(ResultTypes.warning, string.Format( "NO SE PUEDEN REALIZAR CARGAS CON FECHA MENOR AL ULTIMO CIERRE DE PROCESO DE COMBUSTIBLE. ULTIMO CIERRE {0}", fechaMinima.Value.ToString("dd/MM/yy HH:mm")));
                    }
                }

                List<CargaCombustibleExtra> listaCargaComExtra = new List<CargaCombustibleExtra>();
                var resp = new SolicitudOrdenServicioSvc().saveSolicitudOrdenServicio(ref masterOrdenServ, ref listaCargaComExtra);
                if (resp.typeResult == ResultTypes.success)
                {
                    ctrRecorridos.valor = masterOrdenServ.cargaCombustible.kmRecorridos;
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                    CargaCombustible carga = masterOrdenServ.cargaCombustible;
                    carga.masterOrdServ = masterOrdenServ;
                    buscarExistencias();
                    if (chcImprimir.IsChecked.Value)
                        new ReportView().createValeGasolinaCoatza(carga, txtNombreImpresora.Text.Trim());
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public OperationResult validar()
        {
            if (ctrTractor.unidadTransporte == null)
            {
                return new OperationResult(ResultTypes.warning, "SE REQUIERE ESPECIFICAR EL TRACTOR");
            }
            if (ctrChofer.personal == null)
            {
                return new OperationResult(ResultTypes.warning, "SE REQUIERE ESPECIFICAR EL TRACTOR");
            }
            if (ctrDespachadores.personal == null)
            {
                return new OperationResult(ResultTypes.warning, "SE REQUIERE ESPECIFICAR EL TRACTOR");
            }
            if (chcHubodometro.IsChecked.Value)
            {
                if (ctrKmFinal.valor < ctrKmInicial.valor)
                {
                    return new OperationResult(ResultTypes.warning, "EL KM FINAL NO PUEDE SER MENOR AL KM INICIAL");
                }
            }
            
            if (ctrLtsCargados.valor == 0)
            {
                return new OperationResult(ResultTypes.warning, "SE REQUIERE LOS LITROS CARGADOS");
            }

            return new OperationResult(ResultTypes.success, "");
        }
        private void consultaUltimoKm()
        {
            if ((this.ctrTractor.unidadTransporte != null) && this.dtpFechaCombustible.Value.HasValue)
            {
                this.ctrKmInicial.valor = this.ultimoKm;
            }
        }
        private decimal ultimoKm
        {
            get
            {
                if (this.ctrTractor.unidadTransporte == null)
                {
                    this.ctrKmInicial.IsEnabled = true;
                    return decimal.Zero;
                }
                OperationResult result = new RegistroKilometrajeSvc().getUltimoKM(this.ctrTractor.unidadTransporte.clave, this.dtpFechaCombustible.Value.Value);
                if (result.typeResult == ResultTypes.success)
                {
                    this.ctrKmInicial.IsEnabled = false;
                    return (decimal)result.result;
                }
                this.ctrKmInicial.IsEnabled = true;
                return decimal.Zero;
            }
        }
        MasterOrdServ mapForm()
        {
            try
            {
                MasterOrdServ masterOrdServ = new MasterOrdServ
                {
                    idPadreOrdSer = 0,
                    tractor = ctrTractor.unidadTransporte,
                    tolva1 = ctrRemolque1.unidadTransporte == null ? null : ctrRemolque1.unidadTransporte,
                    dolly = ctrDolly.unidadTransporte == null ? null : ctrDolly.unidadTransporte,
                    tolva2 = ctrRemolque2.unidadTransporte == null ? null : ctrRemolque2.unidadTransporte,
                    chofer = ctrChofer.personal,
                    fechaCreacion = DateTime.Now,
                    userCrea = mainWindow.usuario.nombreUsuario,
                    fechaModificacion = null,
                    userModifica = string.Empty,
                    fechaCancela = null,
                    userCancela = string.Empty,
                    estatus = "TER",
                    motivoCancela = string.Empty,
                    cliente = cbxCliente.SelectedItem as Cliente,
                    empresa = ctrEmpresa.empresaSelected
                };

                masterOrdServ.listaCabOrdenServicio = new List<CabOrdenServicio>();
                List<CabOrdenServicio> listaCabOrdenServicio = new List<CabOrdenServicio>();
                List<DetOrdenServicio> listaDetOrdenServicio = new List<DetOrdenServicio>();
                listaDetOrdenServicio.Add(new DetOrdenServicio
                {
                    enumTipoOrden = enumTipoOrden.PREVENTIVO,
                    enumTipoOrdServ = enumTipoOrdServ.COMBUSTIBLE,
                    enumTipoServicio = enumTipoServicio.RECARGA,
                    unidad = ctrTractor.unidadTransporte,
                    servicio = new Servicio
                    {
                        idServicio = 3,
                        nombre = "CARGA DE COMBUSTIBLE",
                        descipcion = "CARGA DE COMBUSTIBLE"
                    },
                    actividad = null,
                    estatus = "TER",
                    ordenTaller = new OrdenTaller
                    {
                        servicio = new Servicio
                        {
                            idServicio = 3,
                            nombre = "CARGA DE COMBUSTIBLE",
                            descipcion = "CARGA DE COMBUSTIBLE"
                        },
                        actividad = null,
                        estatus = "TER",
                        descripcionA = "CARGA DE COMBUSTIBLE",
                        descripcionOS = "CARGA DE COMBUSTIBLE",
                        personal1 = ctrDespachadores.personal
                    }

                });
                DateTime fecha = dtpFechaCombustible.Value.Value;
                Usuario usuario = mainWindow.usuario;
                listaCabOrdenServicio = (from det in listaDetOrdenServicio
                                         group det by new { det.enumTipoOrden, det.enumTipoOrdServ, det.enumTipoServicio, det.unidad } into grupoDetalle
                                         select new CabOrdenServicio
                                         {
                                             idOrdenSer = 0,
                                             empresa = ctrEmpresa.empresaSelected,
                                             estatus = "TER",
                                             externo = false,
                                             fechaAsignado = null,
                                             fechaCancela = null,
                                             fechaCaptura = fecha,
                                             fechaDiagnostico = fecha,
                                             fechaEntregado = fecha,
                                             fechaRecepcion = fecha,
                                             fechaTerminado = fecha,
                                             idEmpleadoAutoriza = ctrDespachadores.personal.clave,
                                             idEmpleadoEntrega = ctrChofer.personal.clave,
                                             idEmpleadoRecibe = ctrDespachadores.personal.clave,
                                             km = "0",
                                             notaFinal = string.Empty,
                                             enumTipoOrden = (from s in grupoDetalle select s.enumTipoOrden).First(),
                                             enumtipoServicio = (from s in grupoDetalle select s.enumTipoServicio).First(),
                                             enumTipoOrdServ = (from s in grupoDetalle select s.enumTipoOrdServ).First(),
                                             unidad = (from s in grupoDetalle select s.unidad).First(),
                                             usuarioAsigna = usuario.nombreUsuario,
                                             usuarioCancela = string.Empty,
                                             usuarioEntrega = usuario.nombreUsuario,
                                             usuarioRecepcion = usuario.nombreUsuario,
                                             usuarioTermina = usuario.nombreUsuario,
                                             listaDetOrdenServicio = grupoDetalle.ToList<DetOrdenServicio>(),
                                             cliente = cbxCliente.SelectedItem as Cliente
                                         }).ToList<CabOrdenServicio>();
                masterOrdServ.listaCabOrdenServicio = listaCabOrdenServicio;

                CargaCombustible cargaCombustible = new CargaCombustible
                {
                    despachador = ctrDespachadores.personal,
                    ecm = chcConECM.IsChecked.Value,
                    fechaCombustible = fecha,
                    fechaEntrada = null,
                    folioImpreso = txtFolioImpresoCarga.Text.Trim(),
                    idCargaCombustible = 0,
                    idOrdenServ = 0,
                    isExterno = chcCargaExterna.IsChecked.Value,
                    idSalidaEntrada = 0,
                    folioImpresoExtra = string.Empty,
                    kmDespachador = ctrKmFinal.valor,
                    kmFinal = ctrKmFinal.valor,
                    kmInicial = ctrKmInicial.valor,
                    kmRecorridos =   ctrRecorridos.valor == 0 ? (!chcHubodometro.IsChecked.Value ? 0 : (ctrKmFinal.valor - ctrKmInicial.valor)) : ctrRecorridos.valor,
                    ltCombustible = ctrLtsCargados.valor,
                    ltCombustibleUtilizado = ctrLtsUtilizados.valor,
                    ltCombustibleEST = ctrLtsEST.valor,
                    ltCombustiblePTO = ctrLtsPTO.valor,
                    ltsExtra = 0,
                    observacioses = string.Empty,
                    precioCombustible = chcCargaExterna.IsChecked.Value ? txtPrecioCarga.valor : 0,
                    proveedor = txtProveedorCarga.SelectedItem == null ? null : (txtProveedorCarga.SelectedItem as ProveedorCombustible),
                    sello1 = ctrSello1.valor,
                    sello2 = ctrSello2.valor,
                    selloReserva = ctrSelloReserva.valor,
                    usuarioCrea = usuario.nombreUsuario,
                    tiempoTotal = txtTiempoTotal.Text,
                    tiempoPTO = txtTiempoPTO.Text,
                    tiempoEST = txtTiempoEst.Text,
                    tanqueCombustible = (txtClave.Tag as NivelesTanque).tanqueCombustible,
                    zonaOperativa = ctrZonaOperativa.zonaOperativa,
                    unidadTransporte = ctrTractor.unidadTransporte,
                    idCliente = (cbxCliente.SelectedItem as Cliente).clave,
                    isHubodometo = chcHubodometro.IsChecked.Value
                };
                masterOrdServ.cargaCombustible = cargaCombustible;
                return masterOrdServ;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void txtProveedorCarga_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.txtProveedorCarga.SelectedItem != null)
            {
                this.txtPrecioCarga.valor = ((ProveedorCombustible)this.txtProveedorCarga.SelectedItem).precio;
            }
            else
            {
                this.txtPrecioCarga.valor = decimal.Zero;
            }
        }

        private void chcCargaExterna_Checked(object sender, RoutedEventArgs e)
        {
            if (chcCargaExterna.IsChecked.Value)
            {
                stpCargaExterna.IsEnabled = true;
            }
            else
            {
                stpCargaExterna.IsEnabled = false;
                txtProveedorCarga.SelectedItem = null;
                txtPrecioCarga.valor = 0;
                txtFolioImpresoCarga.Clear();
                txtObservaciones.Clear();
            }
        }

        private void btnUltimoKm_Click(object sender, RoutedEventArgs e)
        {
            consultaUltimoKm();
        }
        private void btnQuitarTractor_Click(object sender, RoutedEventArgs e)
        {
            ctrTractor.limpiar();
        }
        private void btnQuitarRemolque1_Click(object sender, RoutedEventArgs e)
        {
            ctrRemolque1.limpiar();
        }
        private void btnQuitarDolly_Click(object sender, RoutedEventArgs e)
        {
            ctrDolly.limpiar();
        }
        private void btnQuitarRemolque2_Click(object sender, RoutedEventArgs e)
        {
            ctrRemolque2.limpiar();
        }
        private void btnQuitarChofer_Click(object sender, RoutedEventArgs e)
        {
            ctrChofer.limpiar();
        }
        private void btnQuitarDespachador_Click(object sender, RoutedEventArgs e)
        {
            ctrDespachadores.limpiar();
        }

        private void BtnCambiarImpreso_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.PrintDialog dialog = new System.Windows.Forms.PrintDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                new Core.Utils.Util().setRutasImpresora("VIAJES_ATLANTE", dialog.PrinterSettings.PrinterName);
                this.establecerImpresora();
            }
        }

        private void ChcConECM_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    CheckBox chc = sender as CheckBox;
                    if (!chc.IsChecked.Value)
                    {
                        ctrRecorridos.valor = 0;
                        ctrRecorridos.IsEnabled = false;
                        stpCombustible.IsEnabled = false;
                        ctrLtsUtilizados.valor = 0;
                        ctrLtsPTO.valor = 0;
                        ctrLtsEST.valor = 0;
                        stpTiempos.IsEnabled = false;
                        txtTiempoTotal.Text = "000:00:00";
                        txtTiempoPTO.Text = "00:00:00";
                        txtTiempoEst.Text = "00:00:00";
                    }
                    else
                    {
                        ctrRecorridos.IsEnabled = true;
                        stpCombustible.IsEnabled = true;
                        stpTiempos.IsEnabled = true;                        
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private void ChcHubodometro_Checked(object sender, RoutedEventArgs e)
        {
            this.ctrKmFinal.IsEnabled = true;
        }

        private void ChcHubodometro_Unchecked(object sender, RoutedEventArgs e)
        {
            this.ctrKmFinal.IsEnabled = false;
            this.ctrKmFinal.valor = 0;
        }
    }
}
