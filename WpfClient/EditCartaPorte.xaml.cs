﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
//using System.Windows.Forms;
using Core.Interfaces;
using Core.Models;
using time = System.Timers;
using Core.Utils;
using f = System.Windows.Forms;
using System.Diagnostics;
using System.Text.RegularExpressions;
using ap = System.Windows.Forms;
namespace WpfClient
{

    /// <summary>
    /// Interaction logic for EditCartaPorte.xaml
    /// </summary>
    public partial class EditCartaPorte : ViewBase
    {
        string archivo = ap.Application.StartupPath + @"\config.ini";
        private string DispString;
        Puerto pRFID = new Puerto();
        Puerto pRFID_Secundario = new Puerto();
        time.Timer timer = new time.Timer { Interval = 1000 };
        int contador = 0;
        int claveEmpresa = 0;
        List<string> lista = new List<string>();
        List<string> list1 = new List<string>();
        List<string> listaG = new List<string>();
        bool correoCSI = false;
        crearDocumento doc = null;
        public EditCartaPorte()
        {
            InitializeComponent();
        }
        List<Zona> _listZonas = new List<Zona>();

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            //bool respB = false;
            //doc = new crearDocumento(ref respB);
            //if (!respB)
            //{
            //    controles.IsEnabled = false;
            //    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
            //    return;
            //}
            #region lector
            if (new Util().isActivoAntena())
            {
                pRFID_Secundario = new Puerto();
                var res = pRFID_Secundario.abrirPuerto();
                if (!(bool)res["isOpen"])
                {
                    if (MessageBox.Show(string.Format(res["mensaje"].ToString()
                        + Environment.NewLine +
                        "¿Desea continuar?"), "Aviso", MessageBoxButton.YesNo, MessageBoxImage.Information) == MessageBoxResult.No)
                    {
                        pRFID_Secundario.RFID.Close();
                        controles.IsEnabled = false;
                        mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                        return;
                    }
                }
                pRFID_Secundario.RFID.DataReceived += RFID_DataReceived;
            }
            #endregion
            claveEmpresa = mainWindow.inicio._usuario.personal.empresa.clave;

            //Dispatcher.Invoke((Action)(() =>
            //{
            var resp = new UnidadTransporteSvc().getUnidadesALLorById(claveEmpresa);
            if (resp.typeResult != ResultTypes.success)
            {
                imprimeMensaje(resp);
                nuevo();
                return;
            }
            List<UnidadTransporte> list = (List<UnidadTransporte>)resp.result;

            resp = new OperadorSvc().getOperadoresALLorById();
            if (resp.typeResult != ResultTypes.success)
            {
                imprimeMensaje(resp);
                nuevo();
                return;
            }
            List<Personal> listOperadores = (List<Personal>)resp.result;

            resp = new ClienteSvc().getClientesALLorById(mainWindow.inicio._usuario.personal.empresa.clave, 0, true);
            if (resp.typeResult != ResultTypes.success)
            {
                imprimeMensaje(resp);
                nuevo();
                return;
            }
            List<Cliente> listCliente = (List<Cliente>)resp.result;

            mapComboClientes(listCliente);
            mapCombos(list);
            mapCombosOperador(listOperadores);

            //txtFecha.Text = DateTime.Now.ToString("dd MMMM/yyyy");
            txtFecha2.Value = DateTime.Now;

            int i = 0;
            foreach (Cliente item in cbxClientes.Items)
            {
                if (item.clave == mainWindow.inicio._usuario.cliente.clave)
                {
                    cbxClientes.SelectedIndex = i;
                    //cbxClientes.IsEnabled = false;
                    break;
                }
                i++;
            }
            //}));


            nuevo();
            chxTipoMedidaKilogramos.IsChecked = true;

            var respPrivilegio = new PrivilegioSvc().consultarPrivilegio("APLICAR_FECHA_VIAJE", mainWindow.inicio._usuario.idUsuario);
            if (respPrivilegio.typeResult == ResultTypes.success)
            {
                txtFecha2.IsEnabled = true;
            }
            else
            {
                txtFecha2.IsEnabled = false;
            }

            chcImprimir.IsChecked = new Util().isActivoImpresion();
            chcEnviarCorreo.IsChecked = new Util().isActivoCorreoCSI();
            chcTablero.IsChecked = new Util().isEventoTableroControlUnidades();
        }

        private void imprimeMensaje(OperationResult resp)
        {
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    break;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    break;
                case ResultTypes.recordNotFound:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    break;
                default:
                    break;
            }
        }

        private void RFID_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                timer.Stop();
                pRFID_Secundario.RFID.Close();
                iniciar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void iniciar()
        {
            try
            {
                pRFID = new Puerto();
                timer = new time.Timer { Interval = 1000 };
                timer.Elapsed += Timer_Elapsed;
                Dispatcher.Invoke((Action)(() =>
                {
                    Dictionary<string, object> res = pRFID.abrirPuerto();
                    //pRFID.RFID.Open();
                    contador = new Util().getTiempoEspera();
                    if ((bool)res["isOpen"])
                    {
                        listaG.Clear();
                        //lvl.Items.Clear();
                        list1.Clear();
                        pRFID.RFID.DataReceived += RFID_DataReceived1;

                        timer.Start();
                    }
                    else
                    {
                        MessageBox.Show(res["mensaje"].ToString());
                        //  gripPrincipal.IsEnabled = false;
                        pRFID.RFID.Close();
                    }
                }));
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        private void RFID_DataReceived1(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                contador = new Util().getTiempoEspera();
                DispString = pRFID.RFID.ReadLine();
                guardarLista(DispString);
            }
            catch (Exception)
            {
                return;
            }
        }

        private void guardarLista(string txt)
        {
            txt = txt.Replace("?", " ");
            txt = txt.Replace(")", " ");
            txt = txt.Replace("&", " ");
            txt = txt.Replace("&", " ");
            txt = txt.Replace("*", " ");
            txt = txt.Replace(" ", "");
            txt = txt.Replace("\r", "");
            txt = txt.Replace(@"\u", "");
            txt = txt.Replace(@"\", "");
            txt = txt.Trim();
            txt = Regex.Replace(txt, @"[^\w\.@-]", "",
                                RegexOptions.None, TimeSpan.FromSeconds(1.5));
            if (txt.Length >= 24)
            {
                DispString = txt.Replace("\r", " ").Trim();
                if (list1.Count <= 0)
                {
                    Dispatcher.Invoke((Action)(() =>
                    {
                        list1.Add(DispString);
                        listaG.Add(DispString);
                        //lvl.Items.Add(DispString);
                        buscarRFID(DispString);
                        //notifyIcon.ShowBalloonTip(10, "Lecturas", string.Format("Se detecto ID: {0}", DispString), form.ToolTipIcon.Info);
                    }));

                }
                else
                {
                    //if (!list1.Exists(x => x == DispString))
                    //{

                    Dispatcher.Invoke((Action)(() =>
                    {
                        list1.Add(DispString);
                        listaG.Add(DispString);
                        //lvl.Items.Add(DispString);
                        buscarRFID(DispString);
                        // notifyIcon.ShowBalloonTip(10, "Lecturas", string.Format("Se detecto ID: {0}", DispString), form.ToolTipIcon.Info);
                    }));
                    //}
                }
            }
        }

        private void Timer_Elapsed(object sender, time.ElapsedEventArgs e)
        {
            //contador--;
            //if (contador <= 0)
            //{
            //    //timer.Stop();
            //    //MessageBox.Show("Se acabo el tiempo");
            //    pRFID.RFID.Close();
            //    pRFID_Secundario = new Puerto();
            //    pRFID_Secundario.abrirPuerto();
            //    pRFID_Secundario.RFID.DataReceived += RFID_DataReceived;
            //    //buscarRFID();
            //    //timer.Stop();
            //}
        }

        private void buscarRFID(string RFID)
        {
            Dispatcher.Invoke((Action)(() =>
            {
                try
                {
                    int i = 0;
                    foreach (UnidadTransporte tractor in cbxCamion.Items)
                    {
                        if (tractor.rfid != "")
                        {
                            if (tractor.rfid.Trim() == RFID)
                            {
                                cbxCamion.SelectedIndex = i;
                                cbxCamion.IsEnabled = false;
                                mapCamposTractor((UnidadTransporte)cbxCamion.SelectedItem, ((UnidadTransporte)cbxCamion.SelectedItem).clave);
                                break;
                            }
                        }
                        i++;
                    }

                    i = 0;
                    foreach (UnidadTransporte tolva1 in cbxTolva1.Items)
                    {

                        if (cbxTolva1.SelectedItem != null)
                        {
                            break;
                        }
                        if (tolva1.rfid != "")
                        {
                            if (tolva1.rfid.Trim() == RFID)
                            {
                                cbxTolva1.SelectedIndex = i;
                                cbxTolva1.IsEnabled = false;
                                mapCamposTolva1((UnidadTransporte)cbxTolva1.SelectedItem, ((UnidadTransporte)cbxTolva1.SelectedItem).clave);
                                break;
                            }
                        }

                        i++;
                    }

                    i = 0;
                    foreach (UnidadTransporte dolly in cbxDolly.Items)
                    {

                        if (dolly.rfid != "")
                        {
                            if (dolly.rfid.Trim() == RFID)
                            {
                                cbxDolly.SelectedIndex = i;
                                cbxDolly.IsEnabled = false;
                                mapCamposDolly((UnidadTransporte)cbxDolly.SelectedItem, ((UnidadTransporte)cbxDolly.SelectedItem).clave);
                                break;
                            }
                        }


                        i++;
                    }
                    i = 0;
                    foreach (UnidadTransporte tolva2 in cbxTolva2.Items)
                    {
                        if (tolva2.rfid != "")
                        {
                            if (tolva2.rfid.Trim() == RFID)
                            {
                                if (((UnidadTransporte)cbxTolva1.SelectedItem).rfid != tolva2.rfid)
                                {
                                    cbxTolva2.SelectedIndex = i;
                                    cbxTolva2.IsEnabled = false;
                                    mapCampostolva2((UnidadTransporte)cbxTolva2.SelectedItem, ((UnidadTransporte)cbxTolva2.SelectedItem).clave);
                                    break;
                                }
                            }
                        }

                        i++;
                    }
                    i = 0;

                    foreach (Personal operador in cbxChofer.Items)
                    {
                        if (operador.rfid.Trim() == RFID)
                        {
                            cbxChofer.SelectedIndex = i;
                            cbxChofer.IsEnabled = false;
                            mapChofer((Personal)cbxChofer.SelectedItem);
                            break;
                        }
                        i++;
                    }
                    i = 0;

                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }

                //lvl.Items.Clear();
                //listaG.Clear();
                //contador = new Util().getTiempoEspera();
                //timer.Start();
                //pRFID_Secundario.RFID.Open();


            }));
        }

        //private void buscarRFID()
        //{
        //    var x = listaG;
        //    Dispatcher.Invoke((Action)(() =>
        //    {
        //        try
        //        {
        //            foreach (string rfid in listaG)
        //            {
        //                int i = 0;
        //                foreach (UnidadTransporte tractor in cbxCamion.Items)
        //                {
        //                    if (tractor.rfid == rfid)
        //                    {
        //                        cbxCamion.SelectedIndex = i;
        //                        cbxCamion.IsEnabled = false;
        //                        break;
        //                    }
        //                    i++;
        //                }

        //                i = 0;
        //                foreach (UnidadTransporte tolva1 in cbxTolva1.Items)
        //                {
        //                    if (cbxTolva1.SelectedItem != null)
        //                    {
        //                        break;
        //                    }
        //                    if (tolva1.rfid == rfid)
        //                    {
        //                        cbxTolva1.SelectedIndex = i;
        //                        cbxTolva1.IsEnabled = false;
        //                        break;
        //                    }
        //                    i++;
        //                }

        //                i = 0;
        //                foreach (UnidadTransporte dolly in cbxDolly.Items)
        //                {


        //                    if (dolly.rfid == rfid)
        //                    {
        //                        cbxDolly.SelectedIndex = i;
        //                        cbxDolly.IsEnabled = false;
        //                        break;
        //                    }

        //                    i++;
        //                }
        //                i = 0;
        //                foreach (UnidadTransporte tolva2 in cbxTolva2.Items)
        //                {
        //                    if (tolva2.rfid == rfid)
        //                    {
        //                        if (((UnidadTransporte)cbxTolva1.SelectedItem).rfid != rfid)
        //                        {
        //                            cbxTolva2.SelectedIndex = i;
        //                            cbxTolva2.IsEnabled = false;
        //                            break;
        //                        }
        //                    }
        //                    i++;
        //                }
        //                i = 0;

        //                foreach (Personal operador in cbxChofer.Items)
        //                {
        //                    if (operador.rfid == rfid)
        //                    {
        //                        cbxChofer.SelectedIndex = i;
        //                        cbxChofer.IsEnabled = false;
        //                        break;
        //                    }
        //                    i++;
        //                }
        //                i = 0;
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            MessageBox.Show(e.Message);
        //        }

        //        //lvl.Items.Clear();
        //        listaG.Clear();
        //        contador = new Util().getTiempoEspera();
        //        timer.Start();
        //        //pRFID_Secundario.RFID.Open();
        //    }));
        //}

        private void mapComboSucursal(List<Sucursal> listSuc)
        {
            foreach (Sucursal item in listSuc)
            {
                cbxSucursal.Items.Add(item);
            }
        }

        private void mapComboClientes(List<Cliente> listCliente)
        {
            foreach (Cliente item in listCliente)
            {
                cbxClientes.Items.Add(item);
            }
        }

        private void mapCombosOperador(List<Personal> listOperadores)
        {
            foreach (Personal item in listOperadores)
            {
                cbxChofer.Items.Add(item);
            }
        }

        private void mapCombos(List<UnidadTransporte> list)
        {
            foreach (UnidadTransporte item in list)
            {
                if (item.tipoUnidad.clasificacion.ToUpper() == "DOLLY")
                {
                    cbxDolly.Items.Add(item);
                }
                else if (item.tipoUnidad.clasificacion.ToUpper() == "TRACTOR")
                {
                    cbxCamion.Items.Add(item);
                }
                if (item.tipoUnidad.clasificacion.ToUpper() == "TOLVA")
                {
                    cbxTolva1.Items.Add(item);
                    cbxTolva2.Items.Add(item);
                }
            }
        }

        private void btnNewDetalle_Click(object sender, RoutedEventArgs e)
        {
            if (cbxClientes.SelectedItem != null && txtTolva1.Tag != null/*cbxClientes.SelectedItem != null && cbxTolva1.SelectedItem != null*/)
            {
                //CartaPorte det = new CartaPorte
                //{
                //    listProducto = ((Cliente)txtFraccion.Tag).fraccion.listProductos,
                //    listZonas = _listZonas,
                //    isFull = validarIsFull(),
                //    PorcIVA = ((Cliente)txtFraccion.Tag).impuesto.valor,
                //    PorcRetencion = ((Cliente)txtFraccion.Tag).impuestoFlete.valor,
                //    tipoPrecio = establecerPrecio()
                //};
                //ListViewItem lvl = new ListViewItem();
                //lvl.Content = det;
                //lvlProductos.Items.Add(lvl);
                mapDetalles();
            }

        }
        List<Producto> listaProductos = new List<Producto>();
        private void cbxClientes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender != null)
            {
                if (cbxClientes.SelectedItem != null)
                {
                    Cliente cliente = (Cliente)cbxClientes.SelectedItem;
                    var respZonas = new ZonasSvc().getZonasALLorById(0, ((Cliente)cbxClientes.SelectedItem).clave.ToString());
                    _listZonas = (List<Zona>)respZonas.result;

                    var resp = new ProductoSvc().getALLProductosByIdORidFraccion(cliente.clave);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listaProductos = (List<Producto>)resp.result;
                    }
                    else
                    {
                        listaProductos = new List<Producto>();
                    }

                    lvlProductos.Items.Clear();

                    //txtFraccion.Tag = cliente;
                    //txtFraccion.Text = cliente.fraccion.descripcion;
                    txtRetencion.Text = cliente.impuesto.valor.ToString("0.00");
                    txtFlete.Text = cliente.impuestoFlete.valor.ToString("0.00");
                }
            }
        }
        Viaje _viaje = new Viaje();
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                txtBusquedaRemision.Focus();
                OperationResult val = validar();
                if (val.typeResult != ResultTypes.success)
                {
                    return val;
                }
                Viaje cartaPorte = mapForm();

                //List<CartaPorte> listaAgrupada = agruparCartaPortes(cartaPorte.listDetalles);
                if (cartaPorte != null)
                {
                    if (!chxTimbrar.IsChecked.Value)
                    {
                        var ruta = new Util().getRutaXML();
                        if (string.IsNullOrEmpty(ruta))
                        {
                            return new OperationResult { valor = 2, mensaje = "Para proceder se requiere la ruta del archivo XML" };
                        }
                        //var respCliente = new SDKconsultasSvc().buscarDatosCliente(cartaPorte.cliente.clave);
                        //if (respCliente.typeResult != ResultTypes.success)
                        //{
                        //    return respCliente;
                        //}

                        ComercialCliente cli = new ComercialCliente
                        {
                            nombre = ((Cliente)cbxClientes.SelectedItem).nombre,
                            calle = ((Cliente)cbxClientes.SelectedItem).domicilio,
                            rfc = ((Cliente)cbxClientes.SelectedItem).rfc,
                            colonia = ((Cliente)cbxClientes.SelectedItem).colonia,
                            estado = ((Cliente)cbxClientes.SelectedItem).estado,
                            municipio = ((Cliente)cbxClientes.SelectedItem).municipio,
                            pais = ((Cliente)cbxClientes.SelectedItem).pais,
                            telefono = ((Cliente)cbxClientes.SelectedItem).telefono,
                            cp = ((Cliente)cbxClientes.SelectedItem).cp
                        };

                        ComercialEmpresa emp = new ComercialEmpresa
                        {
                            nombre = ((Empresa)mainWindow.inicio._usuario.personal.empresa).nombre,
                            calle = (mainWindow.inicio._usuario.personal.empresa).direccion,
                            rfc = (mainWindow.inicio._usuario.personal.empresa).rfc,
                            colonia = (mainWindow.inicio._usuario.personal.empresa).colonia,
                            estado = (mainWindow.inicio._usuario.personal.empresa).estado,
                            municipio = (mainWindow.inicio._usuario.personal.empresa).municipio,
                            pais = (mainWindow.inicio._usuario.personal.empresa).pais,
                            telefono = (mainWindow.inicio._usuario.personal.empresa).telefono,
                            cp = (mainWindow.inicio._usuario.personal.empresa).cp
                        };
                        //var respEmpresa = new SDKconsultasSvc().buscarDatosEmpresa(3);
                        //if (respEmpresa.typeResult != ResultTypes.success)
                        //{
                        //    return respEmpresa;
                        //}


                        var datosFactura = ObtenerDatosFactura.obtenerDatos(ruta);
                        if (datosFactura == null)
                        {
                            return new OperationResult { valor = 1, mensaje = "Ocurrio un error al intentar obtener datos del XML" };
                        }
                        int ordenServicio = 0;
                        OperationResult resp = new CartaPorteSvc().saveCartaPorte_V2(ref cartaPorte, ref ordenServicio);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            mapForm(cartaPorte);
                            envioCorreo(cartaPorte);

                            if (chcImprimir.IsChecked.Value)//(new Util().isActivoImpresion())
                            {
                                var listaAgrupada = agruparByZona(cartaPorte.listDetalles);
                                //imprimirReportes(cli, emp, datosFactura, listaAgrupada);
                                foreach (var item in listaAgrupada)
                                {
                                    imprimirReportes(cli, emp, datosFactura, item, cartaPorte);
                                }
                            }
                            if (chcTablero.IsChecked.Value)
                            {
                                ImprimirMensaje.imprimir(EstadoControlUnidadesTransporte.iniciarEstado(cartaPorte));
                            }
                            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                            return resp;
                        }
                        else
                        {
                            return resp;
                        }
                    }
                    else
                    {
                        //var respB = false;
                        //crearDocumento doc = new crearDocumento(ref respB);
                        //if (respB)
                        //{
                        var listDoc = doc.llenarEstructuraDocumento(ref cartaPorte);
                        //doc.cerrar();
                        if (listDoc != null)
                        {
                            int ordenServicio = 0;
                            OperationResult resp = new CartaPorteSvc().saveCartaPorte(cartaPorte, ref ordenServicio);
                            if (resp.typeResult == ResultTypes.success)
                            {
                                mapForm((Viaje)resp.result);
                                abrirDocs(listDoc);
                                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                                return resp;
                            }
                            else
                            {
                                return resp;
                            }
                        }
                        else
                        {
                            return new OperationResult { valor = 1, mensaje = "Ocurrio un error" };
                        }
                        //}
                        //else
                        //{
                        //    return new OperationResult { valor = 1, result = null, mensaje = "Error al realizar la operación" };
                        //}
                    }
                }
                else
                {
                    return new OperationResult { valor = 1, result = null, mensaje = "Error al contruir el objeto" };
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, result = null, mensaje = ex.Message };
            }
            finally
            { Cursor = Cursors.Arrow; }

        }

        private List<CartaPorte> agrupar(List<CartaPorte> listDetalles)
        {
            var grupo = new AgruparCartaPortes(listDetalles);
            return grupo._listaAgrupada;
        }

        private List<List<CartaPorte>> agruparByZona(List<CartaPorte> listDetalles)
        {
            var grupo = new AgruparCartaPortes();
            grupo.agruparByZona(listDetalles);
            return grupo.superLista;
        }

        private void envioCorreo(Viaje viaje)
        {
            EnvioCorreo envioCorreo = new EnvioCorreo();
            if (chcEnviarCorreo.IsChecked.Value)// (new Util().isActivoCorreoGranjas())
            {
                var mensaje = envioCorreo.envioCorreoDeCartaPorte(viaje);
            }
            if (new Util().isActivoCorreoCSI())
            {
                var mensaje2 = envioCorreo.envioCorreoCSI(viaje);
            }
        }

        private void imprimirReportes(ComercialCliente respCliente, ComercialEmpresa respEmpresa, DatosFacturaXML datosFactura, List<CartaPorte> listaAgrupada, Viaje viaje)
        {
            try
            {
                Cursor = Cursors.Wait;
                ReportView reporte = new ReportView(mainWindow);
                bool resp = reporte.cartaPorteFalso(respCliente, respEmpresa, datosFactura, listaAgrupada, viaje);
                if (resp)
                {
                    //reporte.Show();
                    //MessageBox.Show("Imprimiendo carta porte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Ocurrio un error al intentar imprimir la Carta Porte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private List<CartaPorte> agruparCartaPortes(List<CartaPorte> listDetalles)
        {
            try
            {
                List<CartaPorte> listaNueva = new List<CartaPorte>();
                List<CartaPorte> lista1 = new List<CartaPorte>();// listDetalles;
                List<CartaPorte> lista2 = new List<CartaPorte>();// listDetalles;
                foreach (var item in listDetalles)
                {
                    lista1.Add(item);
                    lista2.Add(item);
                }
                foreach (var item in lista1)
                {
                    lista2.Remove(lista2[0]);
                    if (lista2.Count <= 0)
                    {
                        continue;
                    }
                    foreach (var item2 in lista2)
                    {
                        if ((item.producto.clave == item2.producto.clave) && (item.zonaSelect.clave == item2.zonaSelect.clave))
                        {
                            item.volumenDescarga += item2.volumenDescarga;
                        }
                    }
                    listaNueva.Add(item);

                }

                return listaNueva;
            }
            catch (Exception e)
            {

                throw;
            }
            throw new NotImplementedException();
        }

        private void abrirDocs(List<string> listDoc)
        {
            try
            {
                foreach (var item in listDoc)
                {
                    //System.Windows.Forms.FileDialog fil;
                    //MessageBox.Show(item);
                    System.Diagnostics.Process.Start(item);
                    //MessageBox.Show(item);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private OperationResult validar()
        {
            bool resp = true;
            List<String> error = new List<string>();
            if (cbxClientes.SelectedItem == null)
            {
                resp = false;
                error.Add("Elegir un Cliente." + Environment.NewLine);
            }
            if (txtTractor.Tag == null/*cbxCamion.SelectedItem == null*/)
            {
                resp = false;
                error.Add("Elegir el tractor." + Environment.NewLine);
            }

            if (txtTolva1.Tag == null/*cbxTolva1.SelectedItem == null*/)
            {
                resp = false;
                error.Add("Elegir el remolque 1." + Environment.NewLine);
            }

            if (txtChofer.Tag == null)
            {
                resp = false;
                error.Add("Elegir el chofer." + Environment.NewLine);
            }

            //if (!string.IsNullOrEmpty(txtKM.Text.Trim()))
            //{
            //    decimal d = 0m;
            //    if (!decimal.TryParse(txtKM.Text, out d))
            //    {
            //        resp = false;
            //        error.Add("El valor del km no es valido." + Environment.NewLine);
            //    }
            //    else
            //    {
            //        var val = new RegistroKilometrajeSvc().getUltimoRegistroValido(((UnidadTransporte)txtTractor.Tag).clave);
            //        if (val.typeResult == ResultTypes.success)
            //        {
            //            if ((Convert.ToDecimal(txtKM.Text.Trim()) - ((RegistroKilometraje)val.result).kmFinal) >= 10000)
            //            {
            //                resp = false;
            //                error.Add("El kilometraje Total no es Valido, ecxede el valor maximo permitido");

            //            }
            //            if (((RegistroKilometraje)val.result).kmFinal > Convert.ToDecimal(txtKM.Text.Trim()))
            //            {
            //                resp = false;
            //                error.Add("El kilometraje Total no es Valido por que es menor al ultimo KM");
            //            }
            //        }
            //    }
            //}
            //else
            //{
            //    resp = false;
            //    error.Add("Proporcionar el kilometraje." + Environment.NewLine);
            //}

            if (txtDolly.Tag != null/*cbxDolly.SelectedItem != null*/)
            {
                if (txtTolva2.Tag == null/*cbxTolva2.SelectedItem == null*/)
                {
                    resp = false;
                    error.Add("Seleccionar el remolque 2." + Environment.NewLine);
                }
            }

            if (txtTolva2.Tag != null/*cbxTolva2.SelectedItem != null*/)
            {
                if (txtDolly.Tag == null/*cbxDolly.SelectedItem == null*/)
                {
                    resp = false;
                    error.Add("Seleccionar Dolly." + Environment.NewLine);
                }
            }

            //if (Convert.ToDecimal(txtCargaTotal.Text) < Convert.ToDecimal(txtSumValProgramados.Text))
            //{
            //    resp = false;
            //    error.Add("No se puede rebazar la Carga Maxima." + Environment.NewLine);
            //}

            if (validarLista())
            {
                resp = false;
                error.Add("Existen errores en la lista, o faltan proporcionar valores." + Environment.NewLine);
            }

            string cadena = "Para continuar se requiere corregir lo siguiente:" + Environment.NewLine;
            foreach (var item in error)
            {
                cadena += item;
            }
            return new OperationResult { valor = resp ? 0 : 2, result = resp, mensaje = cadena };
        }

        private bool validarLista()
        {
            if (lvlProductos.Items.Count <= 0)
            {
                return true;
            }
            foreach (ListViewItem item in lvlProductos.Items)
            {
                CartaPorte det = (CartaPorte)item.Content;
                if (string.IsNullOrEmpty(det.remision) ||
                    //det.importeReal <= 0 ||
                    det.volumenDescarga <= 0 ||
                    det.volumenCarga <= 0 ||
                    det.zonaSelect == null ||
                    det.producto == null)
                {
                    return true;
                }
                //if (det.zonaSelect.costoFull == 0 || det.zonaSelect.costoSencillo == 0 || det.zonaSelect.costoSencillo35 == 0)
                //{
                //    MessageBox.Show("A la zona " + det.zonaSelect.descripcion + " le faltan asignar algunos precios");
                //    return true;
                //}
            }
            return false;
        }

        private void mapForm(Viaje result)
        {
            if (result != null)
            {
                _viaje = result;
                txtFolio.Tag = result;
                txtFolio.Text = result.NumGuiaId.ToString();
                //txtFecha.Text = result.FechaHoraViaje.ToString("dd /MMMM/yyyy hh:mm:ss tt");
                txtFecha2.Value = result.FechaHoraViaje;
                txtNumViaje.Text = result.NumViaje.ToString();
                //txtKM.Text = result.km.ToString();
            }
            else
            {
                _viaje = null;
                txtFolio.Tag = null;
                txtFolio.Clear();
                //txtFecha.Text = DateTime.Now.ToString("dd/MMMM/yyyy");
                txtFecha2.Value = DateTime.Now;
                cbxChofer.SelectedItem = null;
                cbxChofer.IsEnabled = true;

                cbxCamion.SelectedItem = null;
                cbxCamion.IsEnabled = true;

                cbxTolva1.SelectedItem = null;
                cbxTolva1.IsEnabled = true;

                cbxTolva2.SelectedItem = null;
                cbxTolva2.IsEnabled = true;

                cbxDolly.SelectedItem = null;
                cbxDolly.IsEnabled = true;

                //cbxClientes.SelectedItem = null;
                //cbxClientes.IsEnabled = true;

                //txtFraccion.Clear();
                //txtFraccion.Tag = null;

                lvlProductos.Items.Clear();

                txtNumViaje.Clear();

                //lvl.Items.Clear();
                list1.Clear();
                listaG.Clear();


                mapCamposTractor(null);
                mapCamposTolva1(null);
                mapCampostolva2(null);
                mapCamposDolly(null);
                mapChofer(null);
                mapChoferCapacitacion(null);
                txtCargaTotal.Text = sumarTotalCargas().ToString("0.00");
                //txtKM.Clear();
                sumarVolumenCargas();
            }
        }

        private Viaje mapForm()
        {
            try
            {
                Viaje cartaPorte = new Viaje
                {
                    NumGuiaId = txtFolio.Tag == null ? 0 : ((Viaje)txtFolio.Tag).NumGuiaId,
                    EstatusGuia = "ACTIVO",
                    NumViaje = txtNumViaje.Text == "" ? 0 : Convert.ToInt32(txtNumViaje.Text.Trim()),
                    cliente = ((Cliente)cbxClientes.SelectedItem),
                    operador = ((Personal)txtChofer.Tag),
                    operadorCapacitacion = ((Personal)txtChoferCapacitacion.Tag),
                    IdEmpresa = mainWindow.inicio._usuario.personal.empresa.clave,
                    tractor = (UnidadTransporte)txtTractor.Tag/*cbxCamion.SelectedItem*/,
                    remolque = (UnidadTransporte)txtTolva1.Tag /*cbxTolva1.SelectedItem*/,
                    dolly = txtDolly.Tag == null ? null : (UnidadTransporte)txtDolly.Tag,
                    remolque2 = txtTolva2.Tag == null ? null : (UnidadTransporte)txtTolva2.Tag,
                    SerieGuia = "B",
                    FechaHoraViaje = (DateTime)txtFecha2.Value, // Convert.ToDateTime(txtFecha.Text),
                    UserViaje = mainWindow.inicio._usuario.idUsuario,
                    tipoViaje = validarIsFull() ? 'F' : 'S',
                    km = 0 //.ToDecimal(txtKM.Text)
                };
                cartaPorte.listDetalles = new List<CartaPorte>();

                foreach (ListViewItem item in lvlProductos.Items)
                {
                    cartaPorte.listDetalles.Add((CartaPorte)item.Content);
                }

                return cartaPorte;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private void btnCalcular_Click(object sender, RoutedEventArgs e)
        {
            List<CartaPorte> listDet = new List<CartaPorte>();
            foreach (ListViewItem item in lvlProductos.Items)
            {
                listDet.Add((CartaPorte)item.Content);
            }

            lvlProductos.Items.Clear();

            foreach (var item in listDet)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvlProductos.Items.Add(lvl);
            }
        }
        public override void cerrar()
        {
            if (pRFID_Secundario.RFID != null)
            {
                pRFID_Secundario.RFID.Close();
            }
            if (pRFID.RFID != null)
            {
                pRFID.RFID.Close();
            }
            base.cerrar();
        }

        public override void nuevo()
        {
            mapForm(null);
            establecerRutaXML();
            base.nuevo();

            limpiarDetalles();
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
            txtTractor.Focus();
            txtTractor.Focus();
        }

        private void btnAddRemision_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cbxClientes.SelectedItem != null && txtTolva1.Tag != null)
                {
                    var select = new selectTareaProgramada(((Cliente)cbxClientes.SelectedItem).clave);
                    var resp = select.selectTareas();
                    if (resp != null)
                    {
                        addLista(resp);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void funcionBuscarTareas()
        {
            var select = new selectTareaProgramada(((Cliente)cbxClientes.SelectedItem).clave, txtBusquedaRemision.Text.Trim());
            var resp = select.selectTareas();
            if (resp != null)
            {
                addLista(resp);
            }
        }
        int _idTareaProgramada = 0;
        private void addLista(List<TareaProgramada> resp)
        {
            string lvlExisten = "Ya se agregaron las siguientes remisiones:" + Environment.NewLine;
            string lvlfechas = "Las siguientes remisiones no corresponden al dia de hoy:" + Environment.NewLine;
            bool errorExiste = false;
            bool errorfecha = false;
            string cadena = "";
            foreach (var item in resp)
            {
                if (existeLista(item))
                {
                    lvlExisten += (item.remision.ToString() + Environment.NewLine);
                    errorExiste = true;
                    continue;
                }
                txtRemision.Text = item.remision.ToString();
                selectProducto(((Cliente)txtFraccion.Tag).fraccion.listProductos, item.DescripcionProducto);
                selectZona(_listZonas, item.destino);
                txtVolProgramado.Text = chxTipoMedidaKilogramos.IsChecked.Value ? (item.valorProgramado * 1000).ToString() : (item.valorProgramado * 1000).ToString();
                _idTareaProgramada = item.idTareaProgramada;
                txtVolDescarga.Focus();
                //ListViewItem lvl = new ListViewItem();
                //var cp = new CartaPorte
                //{
                //    idTarea = item.idTareaProgramada,
                //    remision = item.remision,
                //    volumenCarga = item.valorProgramado,
                //    listProducto = ((Cliente)txtFraccion.Tag).fraccion.listProductos,
                //    producto = selectProducto(((Cliente)txtFraccion.Tag).fraccion.listProductos, item.DescripcionProducto),
                //    listZonas = _listZonas,
                //    zonaSelect = selectZona(_listZonas, item.destino),

                //    PorcIVA = ((Cliente)txtFraccion.Tag).impuesto.valor,
                //    PorcRetencion = ((Cliente)txtFraccion.Tag).impuestoFlete.valor,
                //    tipoPrecio = establecerPrecio()
                //};

                //if (cp.producto == null || cp.zonaSelect == null)
                //{
                //    if (cp.producto == null)
                //    {
                //        cadena += "No se ha elegido un producto" + Environment.NewLine;
                //        MessageBox.Show(cadena, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                //        return;
                //    }
                //    if (cp.zonaSelect == null)
                //    {
                //        cadena += "No se ha elegido un destino" + Environment.NewLine;
                //        MessageBox.Show(cadena, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                //        return;
                //    }
                //}
                //else
                //{
                //    lvl.Content = cp;
                //    lvlProductos.Items.Add(lvl);
                //}


            }

            if (errorExiste || errorfecha)
            {
                if (errorExiste)
                {
                    cadena += lvlExisten;
                }
                if (errorfecha)
                {
                    cadena += lvlfechas;
                }
                MessageBox.Show(cadena, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private bool validarIsFull()
        {
            UnidadTransporte x = (UnidadTransporte)txtTolva1.Tag;//cbxTolva1.SelectedItem;
            if (txtDolly.Tag != null || txtTolva2.Tag != null/*cbxDolly.SelectedItem != null || cbxTolva2.SelectedItem != null*/)
            {
                return true;
            }
            if (x.tipoUnidad.descripcion == "TOLVA 35 TON")
            {
                return false;
            }

            return false;
        }

        private TipoPrecio establecerPrecio()
        {
            UnidadTransporte x = (UnidadTransporte)txtTolva1.Tag;//cbxTolva1.SelectedItem;
            int idCliente = ((Cliente)cbxClientes.SelectedItem).clave;

            if (idCliente == 3 || idCliente == 5)
            {
                if (txtDolly.Tag != null || txtTolva2.Tag != null)
                {
                    return TipoPrecio.FULL;
                }
                else if (x.tipoUnidad.descripcion == "TOLVA 35 TON")
                {
                    return TipoPrecio.SENCILLO35;
                }
                else
                {
                    return TipoPrecio.SENCILLO;
                }
            }
            else
            {
                if (txtDolly.Tag != null || txtTolva2.Tag != null)
                {
                    return TipoPrecio.FULL;
                }

                else
                {
                    return TipoPrecio.SENCILLO;
                }
            }
        }


        private bool existeLista(TareaProgramada item)
        {
            foreach (ListViewItem pro in lvlProductos.Items)
            {
                if (item.idTareaProgramada == ((CartaPorte)pro.Content).idTarea)
                {
                    return true;
                }
            }
            return false;
        }

        private /*Zona*/ void selectZona(List<Zona> _listZonas, string destino)
        {
            //foreach (var item in _listZonas)
            //{
            //    if (item.descripcion == destino)
            //    {
            //        return item;
            //    }
            //}
            List<Zona> listZonas = new List<Zona>();
            var mySearch = _listZonas.FindAll(S => S.descripcion.IndexOf(destino, StringComparison.OrdinalIgnoreCase) >= 0);
            foreach (var item in mySearch)
            {
                listZonas.Add(item);
            }
            selectDestinos buscador = new selectDestinos(destino);
            switch (listZonas.Count)
            {
                //case 0:
                //    var resp = buscador.buscarZona(_listZonas);
                //    mapDestinos(resp);
                //    break;
                case 1:
                    mapDestinos(listZonas[0]);
                    break;
                default:
                    var resp = buscador.buscarZona(_listZonas);

                    mapDestinos(resp);
                    break;
            }
        }

        private /*Producto*/ void selectProducto(List<Producto> listProductos, string producto)
        {
            //foreach (var item in listProductos)
            //{
            //    if (item.descripcion == producto.ToString())
            //    {
            //        return item;
            //    }
            //}
            List<Producto> listProducto = new List<Producto>();
            var mySearch = ((Cliente)txtFraccion.Tag).fraccion.listProductos.FindAll(S => S.descripcion.IndexOf(producto, StringComparison.OrdinalIgnoreCase) >= 0);
            foreach (var item in mySearch)
            {
                listProducto.Add(item);
            }
            selectProducto buscador = new selectProducto(producto);
            switch (listProducto.Count)
            {
                //case 0:
                //    var resp = buscador.buscarZona(_listZonas);
                //    mapDestinos(resp);
                //    break;
                case 1:
                    mapProductos(listProducto[0]);
                    break;
                default:
                    var resp = buscador.buscarProductos(((Cliente)txtFraccion.Tag).fraccion.listProductos);
                    mapProductos(resp);
                    break;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ReportView report = new ReportView();
            report.crearArchivo(mapForm());
        }

        private void btnQuitar_Click(object sender, RoutedEventArgs e)
        {
            var list = lvlProductos.SelectedItem;

            lvlProductos.Items.Remove(list);
            sumarVolumenCargas();
        }

        private void cbxTolva1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            //decimal val = Convert.ToDecimal(txtCargaTotal.Text);
            //txtCargaTotal.Text = sumarTotalCargas().ToString("0.00");

        }

        private void cbxTolva2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            //decimal val = Convert.ToDecimal(txtCargaTotal.Text);
            txtCargaTotal.Text = sumarTotalCargas().ToString("0.00");

        }

        internal decimal sumarTotalCargas()
        {
            decimal valor1 = 0.00m;
            decimal valor2 = 0.00m;
            if (txtTolva1.Tag != null)
            {
                //decimal val = Convert.ToDecimal(txtCargaTotal.Text);
                valor1 = (((UnidadTransporte)txtTolva1.Tag).tipoUnidad.TonelajeMax);
            }
            else
            {
                valor1 = 0.00m;
            }

            if (txtTolva2.Tag != null)
            {
                //decimal val = Convert.ToDecimal(txtCargaTotal.Text);
                valor2 = (((UnidadTransporte)txtTolva2.Tag).tipoUnidad.TonelajeMax);
            }
            else
            {
                valor2 = 0.00m;
            }

            return valor1 + valor2;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            sumarVolumenCargas();
        }

        internal void sumarVolumenCargas()
        {
            decimal suma = 0.00m;
            decimal sumaImportes = 0.00m;
            decimal sumaIva = 0.00m;
            decimal sumaRetencion = 0.00m;
            foreach (ListViewItem item in lvlProductos.Items)
            {
                CartaPorte det = (CartaPorte)item.Content;
                suma += det.volumenDescarga;
                sumaImportes += det.importeReal;
                sumaIva += det.ImporIVA;
                sumaRetencion = det.ImpRetencion;
            }

            txtSumValProgramados.Text = suma.ToString("0.00");
            txtTotalImporte.Text = sumaImportes.ToString("0.00");
            txtTotalRetencion.Text = sumaIva.ToString("0.00");
            txtTotalFlete.Text = sumaRetencion.ToString("0.00");
            txtSumaTotal.Text = ((sumaIva + sumaImportes) - sumaRetencion).ToString("0.00");
        }

        private void txtBusquedaRemision_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                int i = 0;
                if (int.TryParse(txtBusquedaRemision.Text.Trim(), out i))
                {
                    buscarRemision();
                }
                else
                {
                    MessageBox.Show("Se requiere una remisión valida", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }

        }

        private void buscarRemision()
        {
            var resp = new TareasProgramadasSvc().getTareasProgramadas(((Cliente)cbxClientes.SelectedItem).clave, Convert.ToInt32(txtBusquedaRemision.Text.Trim()));
            if (resp.typeResult == ResultTypes.error)
            {
                MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (resp.typeResult == ResultTypes.warning)
            {
                MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);

            }
            else if (resp.typeResult == ResultTypes.recordNotFound)
            {
                //MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                funcionBuscarTareas();
            }
            else if (resp.typeResult == ResultTypes.success)
            {
                if (((List<TareaProgramada>)resp.result).Count == 1)
                {
                    addLista((List<TareaProgramada>)resp.result);
                }
                else
                {
                    funcionBuscarTareas();
                }
            }

            txtBusquedaRemision.Clear();
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            sumarVolumenCargas();
        }

        private void addNewZona_Click(object sender, RoutedEventArgs e)
        {
            addNewDestino destino = new addNewDestino((Cliente)cbxClientes.SelectedItem);
            var a = destino.addNewZona();
            if (a != null)
            {
                _listZonas.Add(a);

                foreach (ListViewItem item in lvlProductos.Items)
                {
                    ((CartaPorte)item.Content).listZonas = _listZonas;
                }
            }

        }

        private void btnCargaArchivoXML_Click(object sender, RoutedEventArgs e)
        {
            f.OpenFileDialog dialog = new f.OpenFileDialog();
            //dialog
            dialog.Filter = "|*.XML";
            dialog.Title = "Elige el archivo XML para subir";
            if (dialog.ShowDialog() == f.DialogResult.OK)
            {
                var RUTA = new Util().setRutaXML(dialog.FileName);
                txtRutaXML.Text = RUTA;
            }
        }

        private void establecerRutaXML()
        {
            var RUTA = new Util().getRutaXML();
            if (System.IO.File.Exists(RUTA))
            {
                txtRutaXML.Text = RUTA;
            }
            else
            {
                txtRutaXML.Text = new Util().setRutaXML("");
            }
        }
        private void completarClave(ref string clave)
        {
            if (clave.Length < 3)
            {
                for (int i = 0; i <= (3 - clave.Length); i++)
                {
                    clave = "0" + clave;
                }
            }
        }

        private List<UnidadTransporte> buscarUnidades(string clave)
        {
            OperationResult resp = new UnidadTransporteSvc().getUnidadesALLorById(claveEmpresa, clave);
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    return (List<UnidadTransporte>)resp.result;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return null;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return null;
                case ResultTypes.recordNotFound:
                    return new List<UnidadTransporte>();

                default:
                    return null;
            }
        }

        private void txtTractor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string clave = txtTractor.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                completarClave(ref clave);

                List<UnidadTransporte> ListUnidadT = buscarUnidades("TC" + clave);
                if (ListUnidadT.Count == 1)
                {
                    var tc = validarTractor(ListUnidadT[0]);
                    if (tc)
                    {
                        mapCamposTractor(ListUnidadT[0], ListUnidadT[0].clave);
                    }
                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("TC", claveEmpresa, clave);
                    var s = buscardor.buscarUnidades();
                    mapCamposTractor(s, s == null ? "" : s.clave);
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    mapCamposTractor(s, s == null ? "" : s.clave);
                }
            }
        }

        private bool validarTractor(UnidadTransporte s)
        {
            if (s.tipoUnidad.clasificacion.ToUpper() == "TRACTOR")
            {
                return true;
            }
            return false;
        }

        private void mapCamposTractor(UnidadTransporte unidadTransporte, string clave = "")
        {
            if (unidadTransporte != null)
            {
                txtTractor.Text = unidadTransporte.clave;
                txtTractor.Tag = unidadTransporte;
                txtTractor.IsEnabled = false;
                txtTolva1.Focus();
            }
            else
            {
                txtTractor.Text = "";
                txtTractor.Tag = null;
                txtTractor.IsEnabled = true;
            }
        }

        private void txtTolva1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string clave = txtTolva1.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                completarClave(ref clave);

                List<UnidadTransporte> ListUnidadT = buscarUnidades("TV" + clave);
                if (ListUnidadT.Count == 1)
                {
                    bool tv = validarTolva1(ListUnidadT[0]);
                    if (tv)
                    {
                        mapCamposTolva1(ListUnidadT[0], ListUnidadT[0].clave);
                    }
                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("TV", claveEmpresa, clave);
                    var s = buscardor.buscarUnidades();
                    mapCamposTolva1(s, s == null ? "" : s.clave);
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    mapCamposTolva1(s, s == null ? "" : s.clave);
                }
            }
        }
        private bool validarTolva1(UnidadTransporte s)
        {
            if (s.tipoUnidad.clasificacion.ToUpper() == "TOLVA")
            {
                return true;
            }
            return false;
        }

        private void mapCamposTolva1(UnidadTransporte unidadTransporte, string clave = "")
        {
            if (unidadTransporte != null)
            {
                txtTolva1.Text = clave;
                txtTolva1.Tag = unidadTransporte;
                txtTolva1.IsEnabled = false;
                txtDolly.Focus();
                txtCargaTotal.Text = sumarTotalCargas().ToString("0.00");
            }
            else
            {
                txtTolva1.Text = "";
                txtTolva1.Tag = null;
                txtTolva1.IsEnabled = true;
            }
        }

        private void txtDolly_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string clave = txtDolly.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    txtChofer.Focus();
                    return;
                }
                completarClave(ref clave);

                List<UnidadTransporte> ListUnidadT = buscarUnidades("DL" + clave);
                if (ListUnidadT.Count == 1)
                {
                    bool DL = validarDolly(ListUnidadT[0]);
                    if (DL)
                    {
                        mapCamposDolly(ListUnidadT[0], ListUnidadT[0].clave);
                    }
                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("DL", claveEmpresa, clave);
                    var s = buscardor.buscarUnidades();
                    mapCamposDolly(s, s == null ? "" : s.clave);
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    mapCamposDolly(s, s == null ? "" : s.clave);
                }
            }
        }

        private bool validarDolly(UnidadTransporte s)
        {
            if (s.tipoUnidad.clasificacion.ToUpper() == "DOLLY")
            {
                return true;
            }
            return false;
        }

        private void mapCamposDolly(UnidadTransporte unidadTransporte, string clave = "")
        {
            if (unidadTransporte != null)
            {
                txtDolly.Text = clave;
                txtDolly.Tag = unidadTransporte;
                txtDolly.IsEnabled = false;
                txtTolva2.Focus();
            }
            else
            {
                txtDolly.Text = "";
                txtDolly.Tag = null;
                txtDolly.IsEnabled = true;
            }
        }

        private void txtTolva2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string clave = txtTolva2.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    txtChofer.Focus();
                    return;
                }
                completarClave(ref clave);

                List<UnidadTransporte> ListUnidadT = buscarUnidades("TV" + clave);
                if (ListUnidadT.Count == 1)
                {
                    bool tv = validarTolva2(ListUnidadT[0]);
                    if (tv)
                    {
                        mapCampostolva2(ListUnidadT[0], ListUnidadT[0].clave);
                    }
                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("TV", claveEmpresa, clave);
                    var s = buscardor.buscarUnidades();
                    mapCampostolva2(s, s == null ? "" : s.clave);
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    mapCampostolva2(s, s == null ? "" : s.clave);
                }
            }
        }
        private bool validarTolva2(UnidadTransporte s)
        {
            if (s.tipoUnidad.clasificacion.ToUpper() == "TOLVA")
            {
                if (s.clave == txtTolva1.Text)
                {
                    MessageBox.Show("Se esta duplicando el valor para el remolque 1");
                    return false;
                }
                return true;
            }
            return false;
        }

        private void mapCampostolva2(UnidadTransporte unidadTransporte, string clave = "")
        {
            if (unidadTransporte != null)
            {
                txtTolva2.Text = clave;
                txtTolva2.Tag = unidadTransporte;
                txtTolva2.IsEnabled = false;
                txtChofer.Focus();
                txtCargaTotal.Text = sumarTotalCargas().ToString("0.00");
            }
            else
            {
                txtTolva2.Text = "";
                txtTolva2.Tag = null;
                txtTolva2.IsEnabled = true;
            }
        }

        private void txtChofer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string clave = txtChofer.Text.Trim();
                //completarClave(ref clave);
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                List<Personal> listPersonal = buscarPersonal(clave);
                if (listPersonal.Count == 1)
                {
                    mapChofer(listPersonal[0]);
                }
                else if (listPersonal.Count == 0)
                {
                    selectPersonal buscardor = new selectPersonal(clave, claveEmpresa);
                    var per = buscardor.buscarPersonal();
                    mapChofer(per);
                }
                else
                {
                    selectPersonal buscardor = new selectPersonal(clave, claveEmpresa);
                    var per = buscardor.buscarPersonal();
                    mapChofer(per);
                }
            }
        }

        private void mapChofer(Personal personal)
        {
            if (personal != null)
            {
                txtChofer.Text = personal.nombre;
                txtChofer.Tag = personal;
                txtChofer.IsEnabled = false;
                txtChoferCapacitacion.Focus();
                //txtKM.Focus();
            }
            else
            {
                txtChofer.Text = "";
                txtChofer.Tag = null;
                txtChofer.IsEnabled = true;
            }
        }

        private void mapChoferCapacitacion(Personal personal)
        {
            if (personal != null)
            {
                txtChoferCapacitacion.Text = personal.nombre;
                txtChoferCapacitacion.Tag = personal;
                txtChofer.IsEnabled = false;
                txtRemision.Focus();
                //txtKM.Focus();
            }
            else
            {
                txtChoferCapacitacion.Text = "";
                txtChoferCapacitacion.Tag = null;
                txtChoferCapacitacion.IsEnabled = true;
            }
        }

        private List<Personal> buscarPersonal(string nombre)
        {
            OperationResult resp = new OperadorSvc().getOperadoresByNombre(nombre);
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    return (List<Personal>)resp.result;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return null;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return null;
                case ResultTypes.recordNotFound:
                    return new List<Personal>();

                default:
                    return null;
            }
        }

        private void txtDestino_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                List<Zona> listZonas = new List<Zona>();
                var mySearch = _listZonas.FindAll(S => S.descripcion.IndexOf(txtDestino.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (var item in mySearch)
                {
                    listZonas.Add(item);
                }

                selectDestinos buscador = new selectDestinos(txtDestino.Text);
                switch (listZonas.Count)
                {
                    //case 0:
                    //    var resp = buscador.buscarZona(_listZonas);
                    //    mapDestinos(resp);
                    //    break;
                    case 1:
                        mapDestinos(listZonas[0]);
                        break;
                    default:
                        var resp = buscador.buscarZona(_listZonas);
                        mapDestinos(resp);
                        break;
                }
            }
        }

        void limpiarDetalles()
        {
            mapDestinos(null);
            mapProductos(null);
            txtRemision.Clear();
            txtVolProgramado.Clear();
            txtVolDescarga.Clear();
        }

        private void mapDestinos(Zona zona)
        {
            if (zona != null)
            {
                txtDestino.Tag = zona;
                txtDestino.Text = zona.descripcion;
                txtDestino.IsEnabled = false;
                txtVolProgramado.Focus();
            }
            else
            {
                txtDestino.Tag = null;
                txtDestino.Clear();
                txtDestino.IsEnabled = true;
            }
        }

        private void txtProducto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                List<Producto> listProducto = new List<Producto>();
                var mySearch = this.listaProductos.FindAll(S => S.descripcion.IndexOf(txtProducto.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (var item in mySearch)
                {
                    listProducto.Add(item);
                }
                selectProducto buscador = new selectProducto(txtProducto.Text);
                switch (listProducto.Count)
                {
                    //case 0:
                    //    var resp = buscador.buscarZona(_listZonas);
                    //    mapDestinos(resp);
                    //    break;
                    case 1:
                        mapProductos(listProducto[0]);
                        break;
                    default:
                        var resp = buscador.buscarProductos(this.listaProductos);
                        mapProductos(resp);
                        break;
                }
            }
        }

        private void mapProductos(Producto producto)
        {
            if (producto != null)
            {
                txtProducto.Tag = producto;
                txtProducto.Text = producto.descripcion;
                txtProducto.IsEnabled = false;
                txtDestino.Focus();
            }
            else
            {
                txtProducto.Tag = null;
                txtProducto.Clear();
                txtProducto.IsEnabled = true;
            }
        }

        private void txtRemision_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && !string.IsNullOrEmpty(txtRemision.Text.Trim()))
            {
                int i = 0;
                if (int.TryParse(txtRemision.Text.Trim(), out i))
                {
                    var resp = new TareasProgramadasSvc().getTareasProgramadas(((Cliente)cbxClientes.SelectedItem).clave, i);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        if (((List<TareaProgramada>)resp.result).Count == 1)
                        {
                            addLista((List<TareaProgramada>)resp.result);
                        }
                    }
                    else
                    {
                        txtProducto.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("REMISION INVALIDA");
                }
            }
        }

        private void txtVolProgramado_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && !string.IsNullOrEmpty(txtRemision.Text.Trim()))
            {
                txtVolDescarga.Focus();
            }
        }

        private void txtVolDescarga_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && !string.IsNullOrEmpty(txtRemision.Text.Trim()))
            {
                mapDetalles();
            }
        }

        public void mapDetalles()
        {
            if (cbxClientes.SelectedItem != null && txtTolva1.Tag != null/*cbxClientes.SelectedItem != null && cbxTolva1.SelectedItem != null*/)
            {
                if (!validarDetalles())
                {
                    return;
                }
                CartaPorte det = new CartaPorte
                {
                    //listProducto = ((Cliente)txtFraccion.Tag).fraccion.listProductos,
                    producto = (Producto)txtProducto.Tag,
                    //listZonas = _listZonas,
                    zonaSelect = (Zona)txtDestino.Tag,
                    PorcIVA = ((Cliente)cbxClientes.SelectedItem).impuesto.valor,
                    PorcRetencion = ((Cliente)cbxClientes.SelectedItem).impuestoFlete.valor,
                    tipoPrecio = EstablecerPrecios.establecerPrecio(
                                                                        (UnidadTransporte)txtTolva1.Tag,
                                                                        (UnidadTransporte)txtDolly.Tag,
                                                                        (UnidadTransporte)txtTolva2.Tag, mainWindow.empresa,
                                                                        (Cliente)cbxClientes.SelectedItem
                                                                     ),
                    tipoPrecioChofer = EstablecerPrecios.establecerPrecioChofer(
                                                                                    (UnidadTransporte)txtTolva1.Tag,
                                                                                    (UnidadTransporte)txtDolly.Tag,
                                                                                    (UnidadTransporte)txtTolva2.Tag, mainWindow.empresa
                                                                                ),
                    remision = txtRemision.Text.Trim(),
                    volumenCarga = chxTipoMedidaKilogramos.IsChecked.Value ?
                        Convert.ToDecimal(txtVolProgramado.Text.Trim()) / 1000 :
                        Convert.ToDecimal(txtVolProgramado.Text.Trim()),
                    volumenDescarga = chxTipoMedidaKilogramos.IsChecked.Value ?
                        Convert.ToDecimal(txtVolDescarga.Text.Trim()) / 1000 :
                        Convert.ToDecimal(txtVolDescarga.Text.Trim()),
                    idTarea = _idTareaProgramada
                };
                ListViewItem lvl = new ListViewItem();
                lvl.Content = det;
                lvlProductos.Items.Add(lvl);
                limpiarDetalles();
                _idTareaProgramada = 0;
                sumarVolumenCargas();
                txtRemision.Focus();
            }

        }

        private bool validarDetalles()
        {
            int val = 0;
            if (!int.TryParse(txtRemision.Text.Trim(), out val))
            {
                txtRemision.Clear();
                txtRemision.Focus();
                return false;
            }

            if (txtProducto.Tag == null)
            {
                txtProducto.Clear();
                txtProducto.Focus();
                return false;
            }

            if (txtDestino.Tag == null)
            {
                txtDestino.Clear();
                txtDestino.Focus();
                return false;
            }

            decimal valDec = 0m;

            if (!decimal.TryParse(txtVolProgramado.Text.Trim(), out valDec))
            {
                txtVolProgramado.Clear();
                txtVolProgramado.Focus();
                return false;
            }
            if (!decimal.TryParse(txtVolDescarga.Text.Trim(), out valDec))
            {
                txtVolDescarga.Clear();
                txtVolDescarga.Focus();

                return false;
            }
            else
            {
                if (chxTipoMedidaKilogramos.IsChecked.Value)
                {
                    if (Convert.ToDecimal(txtVolDescarga.Text.Trim()) < 100)
                    {
                        MessageBox.Show("Esta marcada la opcion de captura en kilogramos. Revisa tu valor capturado", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                        txtVolDescarga.Focus();
                        return false;
                    }
                }
                else
                {
                    if (Convert.ToDecimal(txtVolDescarga.Text.Trim()) > 40)
                    {
                        MessageBox.Show("Esta marcada la opcion de captura en Toneladas. Revisa tu valor capturado", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                        txtVolDescarga.Focus();
                        return false;
                    }
                }
            }

            return true;
        }

        private void chxTipoMedidaKilogramos_Checked(object sender, RoutedEventArgs e)
        {
            lblVolPro.Content = "V. Prog (Kgs.)";
            lblVolDes.Content = "V. Des (Kgs.)";
        }

        private void chxTipoMedidaToneladas_Checked(object sender, RoutedEventArgs e)
        {
            lblVolPro.Content = "V. Prog (Tns.)";
            lblVolDes.Content = "V. Des (Tns.)";
        }

        private void txtDolly_KeyDown_1(object sender, KeyEventArgs e)
        {

        }

        private void txtDolly_KeyDown_2(object sender, KeyEventArgs e)
        {

        }

        private void txtKM_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                //if (!string.IsNullOrEmpty(txtKM.Text.Trim()))
                //{
                //    txtRemision.Focus();
                //}
            }
        }

        private void chcImprimir_Checked(object sender, RoutedEventArgs e)
        {
            new Util().Write("IMPRIMIR", "ACTIVO", "true", archivo);
        }

        private void chcImprimir_Unchecked(object sender, RoutedEventArgs e)
        {
            new Util().Write("IMPRIMIR", "ACTIVO", "false", archivo);
        }

        private void chcEnviarCorreo_Checked(object sender, RoutedEventArgs e)
        {
            new Util().Write("PERMISOS_CORREO_CSI", "CSI", "true", archivo);
        }

        private void chcEnviarCorreo_Unchecked(object sender, RoutedEventArgs e)
        {
            new Util().Write("PERMISOS_CORREO_CSI", "CSI", "false", archivo);
        }

        private void txtChoferCapacitacion_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string clave = txtChoferCapacitacion.Text.Trim();
                //completarClave(ref clave);
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                List<Personal> listPersonal = buscarPersonal(clave);
                if (listPersonal.Count == 1)
                {
                    mapChoferCapacitacion(listPersonal[0]);
                }
                else if (listPersonal.Count == 0)
                {
                    selectPersonal buscardor = new selectPersonal(clave, claveEmpresa);
                    var per = buscardor.buscarPersonal();
                    mapChoferCapacitacion(per);
                }
                else
                {
                    selectPersonal buscardor = new selectPersonal(clave, claveEmpresa);
                    var per = buscardor.buscarPersonal();
                    mapChoferCapacitacion(per);
                }
            }
        }

        private void chcTablero_Checked(object sender, RoutedEventArgs e)
        {
            new Util().Write("ACTIVAR_EVENTO_TABLERO", "ACTIVO", "true", archivo);
        }

        private void chcTablero_Unchecked(object sender, RoutedEventArgs e)
        {
            new Util().Write("ACTIVAR_EVENTO_TABLERO", "ACTIVO", "false", archivo);
        }
    }
}
