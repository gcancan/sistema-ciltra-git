﻿using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para catCondicionLlanta.xaml
    /// </summary>
    public partial class catCondicionLlanta : ViewBase
    {
        public catCondicionLlanta()
        {
            InitializeComponent();
        }
        public override OperationResult guardar()
        {
            try
            {
                if (string.IsNullOrEmpty(this.txtClave.Text.Trim()))
                {
                    return new OperationResult(ResultTypes.success, "SE NECESITA PROPORCIONAR LA CLAVE", null);
                }
                if (string.IsNullOrEmpty(this.txtDescripcion.Text.Trim()))
                {
                    return new OperationResult(ResultTypes.success, "SE NECESITA PROPORCIONAR EL NOMBRE", null);
                }
                CondicionLlanta condicionLlanta = this.mapForm();
                OperationResult result = new CondicionSvc().saveCondicionLlantas(ref condicionLlanta);
                if (result.typeResult == ResultTypes.success)
                {
                    this.mapForm(condicionLlanta);
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }
        private CondicionLlanta mapForm()
        {
            try
            {
                return new CondicionLlanta
                {
                    idCondicion = (this.ctrClave.Tag == null) ? 0 : (this.ctrClave.Tag as CondicionLlanta).idCondicion,
                    clave = this.txtClave.Text.Trim(),
                    descripcion = this.txtDescripcion.Text.Trim()
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void mapForm(CondicionLlanta condicionLlanta)
        {
            if (condicionLlanta != null)
            {
                this.ctrClave.Tag = condicionLlanta;
                this.ctrClave.valor = condicionLlanta.idCondicion;
                this.txtClave.Text = condicionLlanta.clave;
                this.txtDescripcion.Text = condicionLlanta.descripcion;
            }
            else
            {
                this.ctrClave.Tag = null;
                this.ctrClave.limpiar();
                this.txtClave.Clear();
                this.txtDescripcion.Clear();
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            this.mapForm(null);
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.nuevo();
        }
    }

}
