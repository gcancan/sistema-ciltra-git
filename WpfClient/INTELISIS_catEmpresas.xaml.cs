﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using CoreINTELISIS.INTERFACES;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using Core.Interfaces;
using CoreINTELISIS.INTERFACES;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para INTELISIS_catEmpresas.xaml
    /// </summary>
    public partial class INTELISIS_catEmpresas : UserControl
    {
        SincronizadorINTELISIS pantalla = null;
        public INTELISIS_catEmpresas(SincronizadorINTELISIS pantalla)
        {
            InitializeComponent();
            this.pantalla = pantalla;
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            pantalla.stpContenedor.Children.Clear();
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lvlEmpresas.ItemsSource = null;
                lblContador.Content = 0;
                lblActualizados.Content = 0;
                lblNuevos.Content = 0;
                Cursor = Cursors.Wait;
                var resp = new EmpresaINTELISISSvc().getAllEmpresasIntelisis();
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Empresa> listaEmpresa = resp.result as List<Empresa>;
                    lvlEmpresas.ItemsSource = listaEmpresa;
                    lblContador.Content = listaEmpresa.Count;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                lblActualizados.Content = 0;
                lblNuevos.Content = 0;
                if (lvlEmpresas.ItemsSource != null)
                {
                    List<Empresa> listaEmpresa = lvlEmpresas.ItemsSource as List<Empresa>;
                    var resp = new EmpresaSvc().sincronizarEmpresas(listaEmpresa);
                    lblActualizados.Content = resp.noActualizados;
                    lblNuevos.Content = resp.noNuevos;
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
