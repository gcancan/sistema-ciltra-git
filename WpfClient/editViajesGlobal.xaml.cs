﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using ap = System.Windows.Forms;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editViajesGlobal.xaml
    /// </summary>
    public partial class editViajesGlobal : ViewBase
    {
        public editViajesGlobal()
        {
            InitializeComponent();
        }
        Usuario usuario = null;
        bool fechaHabilitada = false;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.usuario = mainWindow.usuario;

            ctrOperador.DataContextChanged += CtrOperador_DataContextChanged;
            ctrTC.DataContextChanged += CtrTC_DataContextChanged;
            ctrRM1.DataContextChanged += CtrRemolques1_DataContextChanged;
            ctrDL.DataContextChanged += CtrDL_DataContextChanged;
            ctrRM2.DataContextChanged += CtrRemolques2_DataContextChanged;

            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresa.loaded(usuario);
            ctrZonaOperativa.cargarControl(usuario);

            fechaHabilitada = new PrivilegioSvc().consultarPrivilegio("APLICAR_FECHA_VIAJE", usuario.idUsuario).typeResult == ResultTypes.success;
            dtpFechaInicio.IsEnabled = fechaHabilitada;

            habilitarPrecioRemision = new PrivilegioSvc().consultarPrivilegio("HABILITAR_PRECIO_REMISIONES", usuario.idUsuario).typeResult == ResultTypes.success;

            nuevo();
        }

        private void CtrOperador_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (ctrOperador.personal != null)
            {
                ctrTC.txtUnidadTrans.Focus();
            }
        }

        bool habilitarPrecioRemision = false;
        private void CtrTC_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            cargarCostoCasetas();
            if (ctrTC.unidadTransporte != null)
            {
                ctrRM1.txtUnidadTrans.Focus();
            }
        }

        private void CtrDL_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            cargarCostoCasetas(); 
            if (ctrDL.unidadTransporte != null)
            {
                ctrRM2.txtUnidadTrans.Focus();
            }
        }

        private void CtrRemolques1_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            stpPrimeraRemision.Children.Clear();
            stpRemisiones.Children.Clear();
            
            if (ctrRM1.unidadTransporte == null) return;

            if (ctrRM1.unidadTransporte != null)
            {
                ctrDL.txtUnidadTrans.Focus();
            }

            if (!isMapForm)
            {
                Zona zona = txtRuta.Tag as Zona;
                var tipoPrecio = EstablecerPrecios.establecerPrecio(ctrRM1.unidadTransporte, ctrDL.unidadTransporte, ctrRM2.unidadTransporte, ctrEmpresa.empresaSelected);
                controlClienteRemisiones ctrCliente = new controlClienteRemisiones(usuario, ctrEmpresa.empresaSelected, ctrZonaOperativa.zonaOperativa, zona, tipoPrecio, habilitarPrecioRemision, null, true);
                ctrCliente.ctrPrecioFlete.txtDecimal.TextChanged += TxtDecimal_TextChanged1;
                stpPrimeraRemision.Children.Add(ctrCliente);
                cargarPagos();
                cargarCostoCasetas();
                cargarAnticipos();
            }
            else
            {
               
            }
        }
        private void CtrRemolques2_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            stpPrimeraRemision.Children.Clear();
            stpRemisiones.Children.Clear();
            
            if (ctrRM1.unidadTransporte == null) return;

            if (ctrRM2.unidadTransporte != null)
            {
                dtpFechaInicio.Focus();
            }

            if (!isMapForm)
            {
                Zona zona = txtRuta.Tag as Zona;
                var tipoPrecio = EstablecerPrecios.establecerPrecio(ctrRM1.unidadTransporte, ctrDL.unidadTransporte, ctrRM2.unidadTransporte, ctrEmpresa.empresaSelected);
                controlClienteRemisiones ctrCliente = new controlClienteRemisiones(usuario, ctrEmpresa.empresaSelected, ctrZonaOperativa.zonaOperativa, zona, tipoPrecio, habilitarPrecioRemision, null, true);
                ctrCliente.ctrPrecioFlete.txtDecimal.TextChanged += TxtDecimal_TextChanged1;
                stpPrimeraRemision.Children.Add(ctrCliente);
                cargarPagos();
                cargarCostoCasetas();
                cargarAnticipos();
            }
        }

        Empresa empresa = null;
        private List<Zona> _listZonas;
        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            empresa = ctrEmpresa.empresaSelected;
            stpPrimeraRemision.Children.Clear();

            mapDestinos(null);

            this.ctrTC.loaded(etipoUniadBusqueda.TRACTOR, empresa);
            this.ctrRM1.loaded(etipoUniadBusqueda.TOLVA, empresa);
            this.ctrDL.loaded(etipoUniadBusqueda.DOLLY, empresa);
            this.ctrRM2.loaded(etipoUniadBusqueda.TOLVA, empresa);

            ctrOperador.loaded(eTipoBusquedaPersonal.OPERADORES, empresa);

            OperationResult resp = new ZonasSvc().getZonasByEmpresaCliente(empresa.clave, 0);
            if (resp.typeResult == ResultTypes.success)
            {
                this._listZonas = (List<Zona>)resp.result;
            }
            else
            {
                this._listZonas = new List<Zona>();
                ImprimirMensaje.imprimir(resp);
            }
        }
        public override void nuevo()
        {
            base.nuevo();
            stpPrimeraRemision.Children.Clear();
            mapDestinos(null);

            ctrFolio.txtEntero.HorizontalContentAlignment = HorizontalAlignment.Center;
            ctrFolio.txtEntero.Foreground = Brushes.Red;
            ctrFolio.txtEntero.FontSize = 14;
            ctrFolio.txtEntero.VerticalContentAlignment = VerticalAlignment.Center;

            this.ctrTC.loaded(etipoUniadBusqueda.TRACTOR, empresa);
            this.ctrRM1.loaded(etipoUniadBusqueda.TOLVA, empresa);
            this.ctrDL.loaded(etipoUniadBusqueda.DOLLY, empresa);
            this.ctrRM2.loaded(etipoUniadBusqueda.TOLVA, empresa);

            ctrOperador.loaded(eTipoBusquedaPersonal.OPERADORES, empresa);

            mapForm(null);

            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);

            ctrSumaViajes.valor = 0;

        }
        private void BtnNuevoCliente_Click(object sender, RoutedEventArgs e)
        {
            if (txtRuta.Tag != null)
            {
                if (ctrRM1.unidadTransporte == null) return;

                Zona zona = txtRuta.Tag as Zona;
                var tipoPrecio = EstablecerPrecios.establecerPrecio(ctrRM1.unidadTransporte, ctrDL.unidadTransporte, ctrRM2.unidadTransporte, ctrEmpresa.empresaSelected);
                controlClienteRemisiones ctrCliente = new controlClienteRemisiones(usuario, ctrEmpresa.empresaSelected, ctrZonaOperativa.zonaOperativa, zona, tipoPrecio, habilitarPrecioRemision);
                ctrCliente.ctrPrecioFlete.txtDecimal.TextChanged += TxtDecimal_TextChanged1;
                ctrCliente.btnQuitar.Tag = ctrCliente;
                ctrCliente.btnQuitar.Click += BtnQuitar_Click;
                stpRemisiones.Children.Add(ctrCliente);
            }
        }

        private void BtnQuitar_Click(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                Button btnQuitar = sender as Button;
                controlClienteRemisiones ctr = btnQuitar.Tag as controlClienteRemisiones;

                 var list = ctr.getListaCartaPortes();

                List<int> listaConsecutivos = list.Where(s => s.Consecutivo > 0).Select(s => s.Consecutivo).ToList();
                if (listaConsecutivos.Count > 0)
                {
                    var resp = new ControlBajarCartaPortes().darBajaRemisiones(listaConsecutivos, usuario);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        stpRemisiones.Children.Remove(ctr);
                    }
                }
                else
                {
                    stpRemisiones.Children.Remove(ctr);
                }

                
            }
        }

        private void cargarPagos()
        {
            if (ctrRM1.unidadTransporte == null)
            {
                ctrPagoOperador.valor = 0;
                ctrPagoCasetas.valor = 0;
            }
            else
            {
                var tipoPagoOperador = EstablecerPrecios.establecerPrecioChofer(ctrRM1.unidadTransporte, ctrDL.unidadTransporte, ctrRM2.unidadTransporte, ctrEmpresa.empresaSelected);
                Zona zona = txtRuta.Tag as Zona;
                switch (tipoPagoOperador)
                {
                    case TipoPrecio.SENCILLO:
                        ctrPagoOperador.valor = zona.sencillo_24;
                        break;
                    case TipoPrecio.FULL:
                        ctrPagoOperador.valor = zona.full;
                        break;
                    default:
                        ctrPagoOperador.valor = 0;
                        break;
                }
            }
        }
        public void cargarCostoCasetas()
        {
            var tipo = EstablecerPrecios.establecerCostoCasetas(ctrTC.unidadTransporte, ctrRM1.unidadTransporte, ctrDL.unidadTransporte, ctrRM2.unidadTransporte);
            Zona zona = txtRuta.Tag as Zona;
            if (zona == null) return;
            switch (tipo)
            {
                case TipoPrecioCasetas.TRES_EJES:
                    ctrPagoCasetas.valor = zona.listaCasetas.Sum(s => s.costo3Ejes);
                    break;
                case TipoPrecioCasetas.CINCO_EJES:
                    ctrPagoCasetas.valor = zona.listaCasetas.Sum(s => s.costo5Ejes);
                    break;
                case TipoPrecioCasetas.SIES_EJES:
                    ctrPagoCasetas.valor = zona.listaCasetas.Sum(s => s.costo6Ejes);
                    break;
                case TipoPrecioCasetas.NUEVE_EJES:
                    ctrPagoCasetas.valor = zona.listaCasetas.Sum(s => s.costo9Ejes);
                    break;
                case TipoPrecioCasetas.CERO_EJES:
                    ctrPagoCasetas.valor = 0;
                    break;
            }
        }
        private void cargarAnticipos()
        {
            stpAnticipos.Children.Clear();

            stpNewAnticipos.Children.Clear();
            calcularAnticipos();

            List<DetalleAnticipoMaster> listaDet = new List<DetalleAnticipoMaster>();

            if (ctrRM1.unidadTransporte == null) return;

            TipoPrecioCasetas tipo = EstablecerPrecios.establecerPrecioAnticipo(ctrRM1.unidadTransporte, ctrRM2.unidadTransporte);
            Zona zona = txtRuta.Tag as Zona;

            if (zona == null) return;
            decimal costo = 0;

            foreach (var anticipo in zona.listaAnticipo)
            {
                switch (tipo)
                {
                    case TipoPrecioCasetas.DOS_EJES:
                        costo = anticipo.sencillo2Ejes;
                        break;
                    case TipoPrecioCasetas.TRES_EJES:
                        costo = anticipo.sencillo3Ejes;
                        break;
                    case TipoPrecioCasetas.FULL:
                        costo = anticipo.full;
                        break;
                    default:
                        break;
                }

                DetalleAnticipoMaster detalle = new DetalleAnticipoMaster
                {
                    idDetalleAnticipoMaster = 0,
                    idAnticipoMaster = 0,
                    importe = costo,
                    tipoAnticipo = anticipo.tipoAnticipo
                };
                listaDet.Add(detalle);
                continue;

                StackPanel stpContent = new StackPanel { Tag = anticipo, Orientation = Orientation.Horizontal };
                Label lblAnticipo = new Label
                {
                    Content = anticipo.tipoAnticipo.anticipo,
                    FontWeight = FontWeights.Medium,
                    VerticalAlignment = VerticalAlignment.Center,
                    Width = 140,
                    HorizontalContentAlignment = HorizontalAlignment.Right,
                    Foreground = Brushes.SteelBlue
                };

                controlDecimal ctr = new controlDecimal { Tag = anticipo, FontWeight = FontWeights.Medium, soloLectura = !anticipo.tipoAnticipo.obs };
                ctr.txtDecimal.Tag = ctr;
                ctr.txtDecimal.TextChanged += TxtDecimal_TextChanged;
                ctr.valor = costo;

                stpContent.Children.Add(lblAnticipo);
                stpContent.Children.Add(ctr);

                if (anticipo.tipoAnticipo.obs)
                {
                    TextBox txtObs = new TextBox
                    {
                        FontSize = 10,
                        FontWeight = FontWeights.Medium,
                        Tag = anticipo,
                        Width = 280,
                        Margin = new Thickness(2),
                        MaxLength = 80,
                        CharacterCasing = CharacterCasing.Upper,
                        AcceptsReturn = true,
                        TextWrapping = TextWrapping.Wrap
                    };
                    txtObs.TextChanged += TxtObs_TextChanged;
                    txtObs.Text = anticipo.observaciones;

                    stpContent.Children.Add(txtObs);
                }

                stpAnticipos.Children.Add(stpContent);
            }

            controlAddAnticipos ctrAnt = new controlAddAnticipos(new AnticipoMaster
            {
                idAnticipoMaster = 0,
                usuario = mainWindow.usuario.nombreUsuario,
                fechaPago = DateTime.Now,
                folioViaje = 0,
                listaDetalles = listaDet
            });
            ctrAnt.ctrSubTotal.txtDecimal.TextChanged += TxtDecimal_TextChanged2;
            stpNewAnticipos.Children.Add(ctrAnt);

            calcularAnticipos();
        }

        private void TxtDecimal_TextChanged2(object sender, TextChangedEventArgs e)
        {
            calcularAnticipos();
        }

        private void cargarAnticipos(List<Anticipo> listaAnticipos)
        {
            try
            {
                stpAnticipos.Children.Clear();

                foreach (var anticipo in listaAnticipos)
                {
                    StackPanel stpContent = new StackPanel { Tag = anticipo, Orientation = Orientation.Horizontal };
                    Label lblAnticipo = new Label
                    {
                        Content = anticipo.tipoAnticipo.anticipo,
                        FontWeight = FontWeights.Medium,
                        VerticalAlignment = VerticalAlignment.Center,
                        Width = 140,
                        HorizontalContentAlignment = HorizontalAlignment.Right,
                        Foreground = Brushes.SteelBlue
                    };

                    controlDecimal ctr = new controlDecimal { Tag = anticipo, FontWeight = FontWeights.Medium, soloLectura = !anticipo.tipoAnticipo.obs };
                    ctr.txtDecimal.Tag = ctr;
                    ctr.txtDecimal.TextChanged += TxtDecimal_TextChanged;
                    ctr.valor = anticipo.costo;

                    stpContent.Children.Add(lblAnticipo);
                    stpContent.Children.Add(ctr);

                    if (anticipo.tipoAnticipo.obs)
                    {
                        TextBox txtObs = new TextBox
                        {
                            FontSize = 10,
                            FontWeight = FontWeights.Medium,
                            Tag = anticipo,
                            Width = 280,
                            Margin = new Thickness(2),
                            MaxLength = 80,
                            CharacterCasing = CharacterCasing.Upper,
                            AcceptsReturn = true,
                            TextWrapping = TextWrapping.Wrap
                        };
                        txtObs.TextChanged += TxtObs_TextChanged;
                        txtObs.Text = anticipo.observaciones;

                        stpContent.Children.Add(txtObs);
                    }

                    stpAnticipos.Children.Add(stpContent);
                }
                calcularAnticipos();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        private void TxtObs_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender != null)
            {
                TextBox txt = sender as TextBox;
                Anticipo anticipo = txt.Tag as Anticipo;
                anticipo.observaciones = txt.Text;
            }
        }
        private void TxtDecimal_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender != null)
            {
                TextBox txt = sender as TextBox;
                controlDecimal ctr = txt.Tag as controlDecimal;
                Anticipo anticipo = ctr.Tag as Anticipo;
                anticipo.costo = ctr.valor;

                calcularAnticipos();
            }
        }
        private void calcularAnticipos()
        {
            List<Anticipo> lista = new List<Anticipo>();

            if (stpAnticipos.Children == null)
            {
                ctrTotalAnticipos.valor = 0;
            }
            else
            {
                lista = stpAnticipos.Children.Cast<StackPanel>().ToList().Select(s => s.Tag as Anticipo).ToList();
                ctrTotalAnticipos.valor = lista.Sum(s => s.costo);
            }
            decimal sum = 0m;
            foreach (var item in stpNewAnticipos.Children.Cast<controlAddAnticipos>().ToList())
            {
                sum += item.subTotal;
                var s = item.ctrSubTotal.valor;
            }
            ctrTotalAnticipos.valor = decimal.Round(sum, 2);

        }
        private void TxtRuta_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                List<Zona> list = new List<Zona>();
                List<Zona> list2 = this._listZonas.FindAll(S => S.descripcion.IndexOf(this.txtRuta.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (Zona zona in list2)
                {
                    list.Add(zona);
                }
                selectRutasGlobal destinos = new selectRutasGlobal(this.txtRuta.Text);
                if (list.Count == 1)
                {
                    this.mapDestinos(list[0]);
                }
                else
                {
                    Zona zona2 = destinos.buscarZona(this._listZonas);
                    this.mapDestinos(zona2);
                }
            }
        }
        private void mapDestinos(Zona zona)
        {
            stpRemisiones.Children.Clear();
            stpPrimeraRemision.Children.Clear();
            cargarAnticipos();
            if (zona != null)
            {
                var resp = new ZonasSvc().getAnticipos(zona.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    zona.listaAnticipo = resp.result as List<Anticipo>;
                }
                else
                {
                    zona.listaAnticipo = new List<Anticipo>();
                }

                var respCaseta = new ZonasSvc().getCasetasByDestino(zona.clave);
                if (respCaseta.typeResult == ResultTypes.success)
                {
                    zona.listaCasetas = respCaseta.result as List<Caseta>;
                }
                else
                {
                    zona.listaCasetas = new List<Caseta>();
                }

                txtRuta.Tag = zona;
                txtRuta.Text = zona.origenPegasoDescripcion;// zona.descCliente;
                txtRuta.IsReadOnly = true;
                stpUnidades.IsEnabled = true;
                //stpPrimeraRemision.Children.Add(new controlClienteRemisiones(ctrEmpresa.empresaSelected, ctrZonaOperativa.zonaOperativa, zona));
                ctrOperador.txtPersonal.Focus();
            }
            else
            {
                txtRuta.Tag = null;
                txtRuta.Clear();
                txtRuta.IsReadOnly = false;
                stpUnidades.IsEnabled = false;
            }
        }
        private void BtnLimpiarTC_Click(object sender, RoutedEventArgs e)
        {
            ctrTC.unidadTransporte = null;
        }
        private void BtnLimpiarRM1_Click(object sender, RoutedEventArgs e)
        {
            ctrRM1.unidadTransporte = null;
        }
        private void BtnLimpiarRDL_Click(object sender, RoutedEventArgs e)
        {
            ctrDL.unidadTransporte = null;
        }
        private void BtnLimpiarRM2_Click(object sender, RoutedEventArgs e)
        {
            ctrRM2.unidadTransporte = null;
        }
        bool finalizar = false;
        public override OperationResult guardar()
        {
            try
            {
                var validacion = validar();
                if (validacion.typeResult != ResultTypes.success) return validacion;

                Viaje viaje = mapForm();

                if (finalizar)
                {
                    var valFin = validarFinalizado();

                    if (valFin.typeResult != ResultTypes.success) return valFin;

                    viaje.FechaSalida = dtpFechaSalida.Value;
                    viaje.FechaHoraFin = dtpFechaFin.Value;
                    viaje.UserFinViaje = usuario.idUsuario;
                    viaje.EstatusGuia = "FINALIZADO";
                }

                int id = 0;
                var resp = new CartaPorteSvc().saveCartaPorte_V2(ref viaje, ref id);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(viaje);
                    if (chcImprimirCP.IsChecked.Value)
                    {
                        imprimirCartaPortes(viaje);
                    }
                    if (chcAnticipo.IsChecked.Value) new ReportView().imprimirComprobanteAnticipo(viaje, usuario, nombreImpresora);

                    if (chcCorreoCsi.IsChecked.Value) enviarCorreo(viaje);
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                finalizar = false;
            }
        }

        private void enviarCorreo(Viaje viaje)
        {
            new EnvioCorreo().envioCorreoCSI(viaje);
        }

        private string _nombreImpresora { get; set; }
        private string nombreImpresora
        {
            get
            {
                if (string.IsNullOrEmpty(_nombreImpresora))
                {
                    ap.PrintDialog printDialog1 = new ap.PrintDialog();
                    printDialog1.PrinterSettings.Copies = 1;
                    ap.DialogResult result = printDialog1.ShowDialog();
                    var ss = printDialog1.PrinterSettings.PrinterName;
                    _nombreImpresora = ss;
                    return nombreImpresora;
                }
                else
                {
                    return _nombreImpresora;
                }
            }
            set
            {
                _nombreImpresora = value;
            }
        }
        private void imprimirCartaPortes(Viaje viaje)
        {
            try
            {
                string str = new Core.Utils.Util().getRutaXML();
                if (string.IsNullOrEmpty(str))
                {
                    ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "Para proceder se requiere la ruta del archivo XML" });
                }

                List<GrupoCliente> listaClientes = (from v in viaje.listDetalles
                                                    group v by v.cliente.clave into grupo
                                                    select new GrupoCliente
                                                    {
                                                        cliente = grupo.Select(s => s.cliente).First(),
                                                        listaCp = grupo.ToList()
                                                    }).ToList();
                DatosFacturaXML datosFactura = ObtenerDatosFactura.obtenerDatos(str);
                if (datosFactura == null)
                {
                    ImprimirMensaje.imprimir(new OperationResult { valor = 1, mensaje = "Ocurrio un error al intentar obtener datos del XML" });
                }
                foreach (var clienteGp in listaClientes)
                {
                    Cliente cliente = clienteGp.cliente;
                    Empresa empresa = ctrEmpresa.empresaSelected;

                    ComercialCliente respCliente = new ComercialCliente
                    {
                        nombre = cliente.nombre,
                        calle = cliente.domicilio,
                        rfc = cliente.rfc,
                        colonia = cliente.colonia,
                        estado = cliente.estado,
                        municipio = cliente.municipio,
                        pais = cliente.pais,
                        telefono = cliente.telefono,
                        cp = cliente.cp
                    };
                    ComercialEmpresa respEmpresa = new ComercialEmpresa
                    {
                        nombre = empresa.nombre,
                        calle = empresa.direccion,
                        rfc = empresa.rfc,
                        colonia = empresa.colonia,
                        estado = empresa.estado,
                        municipio = empresa.municipio,
                        pais = empresa.pais,
                        telefono = empresa.telefono,
                        cp = empresa.cp
                    };

                    new ReportView().imprimirCartaPorteGlobal(respCliente, respEmpresa, datosFactura, clienteGp.listaCp, viaje, nombreImpresora);
                }


            }
            catch (Exception ex)
            {

            }
        }

        bool isMapForm = false;
        private OperationResult validar()
        {
            try
            {
                OperationResult validacion = new OperationResult(ResultTypes.success, ("PARA CONTINUAR SE REQUIERE" + Environment.NewLine));
                if (ctrEmpresa.empresaSelected == null)
                {
                    validacion.valor = 2;
                    validacion.mensaje += " * Elegir Una Empresa." + Environment.NewLine;
                }
                if (ctrZonaOperativa.zonaOperativa == null)
                {
                    validacion.valor = 2;
                    validacion.mensaje += " * Elegir Una Zona Operativa." + Environment.NewLine;
                }
                if (txtRuta.Tag == null)
                {
                    validacion.valor = 2;
                    validacion.mensaje += " * Proporcionar La Ruta." + Environment.NewLine;
                }
                if (ctrOperador.personal == null)
                {
                    validacion.valor = 2;
                    validacion.mensaje += " * Proporcionar Al Operador." + Environment.NewLine;
                }
                if (ctrTC.unidadTransporte == null)
                {
                    validacion.valor = 2;
                    validacion.mensaje += " * Proporcionar El Tracto" + Environment.NewLine;
                }
                if (ctrRM1.unidadTransporte == null)
                {
                    validacion.valor = 2;
                    validacion.mensaje += " * Proporcionar El Remolque 1" + Environment.NewLine;
                }
                if (ctrRM2.unidadTransporte != null)
                {
                    if (ctrDL.unidadTransporte == null)
                    {
                        validacion.valor = 2;
                        validacion.mensaje += " * Proporcionar El Dolly" + Environment.NewLine;
                    }
                }
                if (ctrDL.unidadTransporte != null)
                {
                    if (ctrRM2.unidadTransporte == null)
                    {
                        validacion.valor = 2;
                        validacion.mensaje += " * Proporcionar El Remolque 2" + Environment.NewLine;
                    }
                }
                if (dtpFechaInicio.Value == null)
                {
                    validacion.valor = 2;
                    validacion.mensaje += " * Proporcionar La Fecha de Captura" + Environment.NewLine;
                }
                if (dtpFechaPrograma.Value == null)
                {
                    validacion.valor = 2;
                    validacion.mensaje += " * Proporcionar La Fecha de Programa" + Environment.NewLine;
                }

                foreach (var anticipo in obtenerListaAnticipos())
                {
                    if (anticipo.tipoAnticipo.obs)
                    {
                        if (string.IsNullOrEmpty(anticipo.observaciones))
                        {
                            validacion.valor = 2;
                            validacion.mensaje += string.Format(" * El Anticipo {0} Requiere Observaciones.", anticipo.tipoAnticipo.anticipo) + Environment.NewLine;
                        }
                    }
                }

                var listaCp = getListasCartaPortes();
                if (listaCp.Count == 0)
                {
                    validacion.valor = 2;
                    validacion.mensaje += " * Proporcionar Al Menos Una Remisión." + Environment.NewLine;
                }

                foreach (CartaPorte cp in listaCp)
                {
                    if (cp.producto == null)
                    {
                        validacion.valor = 2;
                        validacion.mensaje += " * Totas Las Remisiones Requieren Producto." + Environment.NewLine;
                    }
                    //if (cp.volumenDescarga == 0)
                    //{
                    //    validacion.valor = 2;
                    //    validacion.mensaje += " * Totas Las Remisiones Requieren Cantidad." + Environment.NewLine;
                    //}
                }

                return validacion;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        private OperationResult validarFinalizado()
        {
            try
            {
                OperationResult validacion = new OperationResult(ResultTypes.success, ("PARA FINALIZAR EL VIAJE SE REQUIERE" + Environment.NewLine));

                if (dtpFechaSalida.Value == null)
                {
                    validacion.valor = 2;
                    validacion.mensaje += " * Proporcionar La Fecha De Salida." + Environment.NewLine;
                }
                if (dtpFechaFin.Value == null)
                {
                    validacion.valor = 2;
                    validacion.mensaje += " * Proporcionar La Fecha Final." + Environment.NewLine;
                }

                var listaCp = getListasCartaPortes();
                if (listaCp.Count == 0)
                {
                    validacion.valor = 2;
                    validacion.mensaje += " * Proporcionar Al Menos Una Remisión." + Environment.NewLine;
                }

                foreach (CartaPorte cp in listaCp)
                {
                    if (cp.producto == null)
                    {
                        validacion.valor = 2;
                        validacion.mensaje += " * Totas Las Remisiones Requieren Producto." + Environment.NewLine;
                    }
                    //if (cp.volumenDescarga == 0)
                    //{
                    //    validacion.valor = 2;
                    //    validacion.mensaje += " * Totas Las Remisiones Requieren Cantidad." + Environment.NewLine;
                    //}
                    //if (string.IsNullOrEmpty(cp.remision))
                    //{
                    //    validacion.valor = 2;
                    //    validacion.mensaje += " * Especificar Totas Las Remisiones." + Environment.NewLine;
                    //}
                }

                return validacion;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public void mapForm(Viaje viaje)
        {
            try
            {
                if (viaje != null)
                {
                    isMapForm = true;
                    ctrFolio.valor = viaje.NumGuiaId;
                    ctrFolio.Tag = viaje;
                    ctrEmpresa.seleccionarEmpresa(new Empresa { clave = viaje.IdEmpresa });
                    ctrZonaOperativa.zonaOperativa = viaje.zonaOperativa;
                    mapDestinos(viaje.listDetalles[0].zonaSelect);

                    ctrOperador.personal = viaje.operador;
                    ctrTC.unidadTransporte = viaje.tractor;
                    ctrRM1.unidadTransporte = viaje.remolque;
                    ctrDL.unidadTransporte = viaje.dolly;
                    ctrRM2.unidadTransporte = viaje.remolque2;
                    dtpFechaInicio.Value = viaje.FechaHoraViaje;
                    dtpFechaPrograma.Value = viaje.FechaPrograma;
                    dtpFechaSalida.Value = viaje.FechaSalida;
                    dtpFechaFin.Value = viaje.FechaHoraFin;
                    txtEstatus.Text = viaje.EstatusGuia;

                    ctrPagoOperador.valor = viaje.pagoOperadores;
                    ctrPagoCasetas.valor = viaje.pagoCasetas;

                    //cargarAnticipos(viaje.listaAnticipos);

                    stpNewAnticipos.Children.Clear();
                    var respAnt = new CartaPorteSvc().getAnticipoMasterByFolioViaje(viaje.NumGuiaId);
                    if (respAnt.typeResult == ResultTypes.success)
                    {
                        foreach (var ant in respAnt.result as List<AnticipoMaster>)
                        {
                            controlAddAnticipos control = new controlAddAnticipos(ant);
                            control.ctrSubTotal.txtDecimal.TextChanged += TxtDecimal_TextChanged4;
                            stpNewAnticipos.Children.Add(control);
                        }
                    }

                    List<GrupoCliente> listaClientes = (from v in viaje.listDetalles
                                                        group v by v.cliente.clave into grupo
                                                        select new GrupoCliente
                                                        {
                                                            cliente = grupo.Select(s => s.cliente).First(),
                                                            listaCp = grupo.ToList()
                                                        }).ToList();
                    int i = 0;
                    Zona zona = viaje.listDetalles[0].zonaSelect;
                    var tipoPrecio = EstablecerPrecios.establecerPrecio(ctrRM1.unidadTransporte, ctrDL.unidadTransporte, ctrRM2.unidadTransporte, ctrEmpresa.empresaSelected);
                    foreach (var cliente in listaClientes)
                    {
                        if (i == 0)
                        {
                            controlClienteRemisiones ctrCliente = new controlClienteRemisiones(usuario, ctrEmpresa.empresaSelected, viaje.zonaOperativa, zona,
                                tipoPrecio, habilitarPrecioRemision, cliente.listaCp, true);
                            ctrCliente.ctrPrecioFlete.txtDecimal.TextChanged += TxtDecimal_TextChanged1;
                            stpPrimeraRemision.Children.Add(ctrCliente);
                        }
                        else
                        {
                            controlClienteRemisiones ctrCliente = new controlClienteRemisiones(usuario, ctrEmpresa.empresaSelected, ctrZonaOperativa.zonaOperativa, zona, 
                                tipoPrecio, habilitarPrecioRemision, cliente.listaCp);
                            ctrCliente.ctrPrecioFlete.txtDecimal.TextChanged += TxtDecimal_TextChanged1;
                            ctrCliente.btnQuitar.Tag = ctrCliente;
                            ctrCliente.btnQuitar.Click += BtnQuitar_Click;
                            stpRemisiones.Children.Add(ctrCliente);
                        }
                        i++;
                    }

                    stpHeader.IsEnabled = false;
                    stpFolio.IsEnabled = false;
                    stpUnidades.IsEnabled = false;
                    if (viaje.EstatusGuia == "ACTIVO")
                    {
                        mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo | ToolbarCommands.editar | ToolbarCommands.eliminar);
                    }
                    else
                    {
                        mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                    }

                    this.IsEnabled = false;
                    btnFinalizar.IsEnabled = true;
                    stpFechasFinales.IsEnabled = true;
                }
                else
                {
                    isMapForm = false;
                    ctrFolio.valor = 0;
                    ctrFolio.Tag = null;
                    ctrPagoCasetas.valor = 0;
                    ctrPagoOperador.valor = 0;

                    ctrOperador.personal = null;
                    ctrTC.unidadTransporte = null;
                    ctrRM1.unidadTransporte = null;
                    ctrDL.unidadTransporte = null;
                    ctrRM2.unidadTransporte = null;
                    stpFechasFinales.IsEnabled = false;
                    dtpFechaInicio.Value = DateTime.Now;
                    dtpFechaPrograma.Value = DateTime.Now;
                    dtpFechaSalida.Value = null;
                    dtpFechaFin.Value = null;
                    txtEstatus.Text = string.Empty;
                    //cargarAnticipos();

                    stpHeader.IsEnabled = true;
                    stpFolio.IsEnabled = true;
                    stpUnidades.IsEnabled = false;
                    btnFinalizar.IsEnabled = false;
                    stpFechasFinales.IsEnabled = false;
                    stpNewAnticipos.Children.Clear();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                mapForm(null);
            }
        }

        private void TxtDecimal_TextChanged4(object sender, TextChangedEventArgs e)
        {
            calcularAnticipos();
        }

        private void TxtDecimal_TextChanged1(object sender, TextChangedEventArgs e)
        {
            try
            {
                decimal sumaFlete = 0m;
                foreach (controlClienteRemisiones ctr in stpPrimeraRemision.Children.Cast<controlClienteRemisiones>().ToList())
                {
                    sumaFlete += ctr.ctrPrecioFlete.valor;
                }
                foreach (controlClienteRemisiones ctr in stpRemisiones.Children.Cast<controlClienteRemisiones>().ToList())
                {
                    sumaFlete += ctr.ctrPrecioFlete.valor;
                }

                ctrSumaViajes.valor = sumaFlete;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        class GrupoCliente
        {
            internal Cliente cliente { get; set; }
            internal List<CartaPorte> listaCp { get; set; }
        }
        private Viaje mapForm()
        {
            try
            {
                Viaje viaje = new Viaje
                {
                    NumGuiaId = ctrFolio.Tag == null ? 0 : (ctrFolio.Tag as Viaje).NumGuiaId,
                    IdEmpresa = ctrEmpresa.empresaSelected.clave,
                    SerieGuia = string.Empty,
                    zonaOperativa = ctrZonaOperativa.zonaOperativa,
                    cliente = null,
                    EstatusGuia = "ACTIVO",
                    operador = ctrOperador.personal,
                    tractor = ctrTC.unidadTransporte,
                    remolque = ctrRM1.unidadTransporte,
                    dolly = ctrDL.unidadTransporte,
                    remolque2 = ctrRM2.unidadTransporte,
                    FechaHoraViaje = fechaHabilitada ? dtpFechaInicio.Value.Value : DateTime.Now,
                    FechaPrograma = dtpFechaPrograma.Value.Value,
                    FechaSalida = dtpFechaSalida.Value,
                    FechaHoraFin = dtpFechaFin.Value,
                    pagoOperadores = ctrPagoOperador.valor,
                    pagoCasetas = ctrPagoCasetas.valor,
                    UserViaje = usuario.idUsuario,
                    tipoViaje = ctrRM2.unidadTransporte == null ? 'S' : 'F',
                    km = (txtRuta.Tag as Zona).km,
                    cobroXKm = false,
                    ayudante = null,
                    taller = false,
                    tallerExterno = false,
                    noViaje = 0,
                    editoNoViaje = false
                };

                viaje.listDetalles = new List<CartaPorte>();
                viaje.listDetalles = getListasCartaPortes();

                viaje.listaAnticipos = obtenerListaAnticipos();

                viaje.listaAnticiposMaster = new List<AnticipoMaster>();
                viaje.listaAnticiposMaster = getListaAnticiposMaster();

                return viaje;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private List<AnticipoMaster> getListaAnticiposMaster()
        {
            
            try
            {
                List<AnticipoMaster> lista = new List<AnticipoMaster>();
                foreach (var item in stpNewAnticipos.Children.Cast<controlAddAnticipos>().ToList())
                {
                    var ii = item.ctrFechaSolicitud.Value;
                    AnticipoMaster anticipo = item.ctrFolio.Tag as AnticipoMaster;
                    anticipo.fechaPago = ii.Value;
                    lista.Add(anticipo);
                }
                return lista;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }

        private List<CartaPorte> getListasCartaPortes()
        {
            try
            {
                var listDetalles = new List<CartaPorte>();
                foreach (controlClienteRemisiones ctr in stpPrimeraRemision.Children.Cast<controlClienteRemisiones>().ToList())
                {
                    listDetalles.AddRange(ctr.getListaCartaPortes());
                }
                foreach (controlClienteRemisiones ctr in stpRemisiones.Children.Cast<controlClienteRemisiones>().ToList())
                {
                    listDetalles.AddRange(ctr.getListaCartaPortes());
                }
                return listDetalles;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return new List<CartaPorte>();
            }
        }
        List<Anticipo> obtenerListaAnticipos()
        {
            try
            {
                List<Anticipo> lista = new List<Anticipo>();
                lista = stpAnticipos.Children.Cast<StackPanel>().ToList().Select(s => s.Tag as Anticipo).ToList();
                return lista;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return new List<Anticipo>();
            }
        }
        private void CtrFolio_KeyDown(object sender, KeyEventArgs e)
        {
            if (sender != null)
            {
                if (e.Key == Key.Enter)
                {
                    if (ctrFolio.valor > 0)
                    {
                        buscarViajeById(ctrFolio.valor);
                    }
                }
            }
        }

        private void buscarViajeById(int valor)
        {
            var resp = new CartaPorteSvc().getCartaPorteById(valor);
            if (resp.typeResult == ResultTypes.success)
            {
                Viaje viaje = (resp.result as List<Viaje>)[0];                
                mapForm(viaje);
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
            }
        }

        private void BtnFinalizar_Click(object sender, RoutedEventArgs e)
        {
            finalizar = true;
            ImprimirMensaje.imprimir(guardar());
        }
        public override OperationResult eliminar()
        {
            try
            {

                Viaje viaje = ctrFolio.Tag as Viaje;

                viaje.FechaHoraFin = DateTime.Now;
                viaje.UserFinViaje = usuario.idUsuario;
                viaje.EstatusGuia = "CANCELADO";

                int id = 0;
                var resp = new CartaPorteSvc().saveCartaPorte_V2(ref viaje, ref id);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(viaje);
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                finalizar = false;
            }
        }

        private void BtnLimpiarOperador_Click(object sender, RoutedEventArgs e)
        {
            ctrOperador.limpiar();
        }

        private void btnAddAnticipo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ctrFolio.Tag == null) return;

                AnticipoMaster anticipo = new AnticipoMaster
                {
                    folioViaje = (ctrFolio.Tag as Viaje).NumGuiaId,
                    idAnticipoMaster = 0,
                    usuario = mainWindow.usuario.nombreUsuario,
                    fechaPago = DateTime.Now,
                    listaDetalles = new List<DetalleAnticipoMaster> 
                    { 
                        new DetalleAnticipoMaster 
                        {
                            idAnticipoMaster = 0,
                            idDetalleAnticipoMaster = 0,
                            importe = 0,
                            tipoAnticipo = new TipoAnticipo
                            {
                                idTipoAnticipo = 3,
                                anticipo = "Gastos Operativos",
                                activo = true,
                                obs = true
                            }
                        } 
                    }
                };
                controlAddAnticipos ctrAnt = new controlAddAnticipos(anticipo);
                ctrAnt.ctrSubTotal.txtDecimal.TextChanged += TxtDecimal_TextChanged3;
                stpNewAnticipos.Children.Add(ctrAnt);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void TxtDecimal_TextChanged3(object sender, TextChangedEventArgs e)
        {
            calcularAnticipos();
        }
    }
}
