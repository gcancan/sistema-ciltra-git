﻿using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para controlResultadoViajes.xaml
    /// </summary>
    public partial class controlResultadoViajes : UserControl
    {
        public controlResultadoViajes()
        {
            InitializeComponent();
        }
        public void llenarLista(List<ReporteCartaPorteViajes> listaReporteCartaPorte)
        {
            lvlViajes.Items.Clear();
            DataContext = new List<ReporteCartaPorteViajes>();
            //List<ListViewItem> listLvl = new List<ListViewItem>();
            foreach (var item in listaReporteCartaPorte)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvlViajes.Items.Add(lvl);
            }
            DataContext = listaViajesCheck;
            //lvlViajes.ItemsSource = listaReporteCartaPorte;
        }
        List<ReporteCartaPorteViajes> listaViajesCheck
        {
            get
            {
                List<ReporteCartaPorteViajes> lista = new List<ReporteCartaPorteViajes>();
                foreach (ListViewItem item in lvlViajes.Items)
                {
                    if ((item.Content as ReporteCartaPorteViajes).isSeleccionado)
                    {
                        lista.Add(item.Content as ReporteCartaPorteViajes);
                    }                    
                }
                return lista;
            }
        }
        private void ChcSeleccionado_Checked(object sender, RoutedEventArgs e) => DataContext = listaViajesCheck;        
        public void desMarcarTodos()
        {
            try
            {
                Cursor = Cursors.Wait;
                foreach (var item in lvlViajes.Items.Cast<ListViewItem>().ToList())
                {
                    var s = item.Content as ReporteCartaPorteViajes;
                    s.isSeleccionado = false;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public void marcarTodos()
        {
            try
            {
                Cursor = Cursors.Wait;
                foreach (var item in lvlViajes.Items.Cast<ListViewItem>().ToList())
                {
                    var s = item.Content as ReporteCartaPorteViajes;
                    s.isSeleccionado = true;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }       
    }
}