﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using CoreFletera.Interfaces;
    using CoreFletera.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Markup;

    public partial class editProductos : WpfClient.ViewBase
    {
        public editProductos()
        {
            this.InitializeComponent();
        }
        private void btnImprimir_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                ReportView view = new ReportView();
                if (view.imprimirCatalogoProductos((Cliente)this.cbxCliente.SelectedItem))
                {
                    view.Show();
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        public override void buscar()
        {
            if (this.cbxCliente.SelectedItem != null)
            {
                Cliente cliente = cbxCliente.SelectedItem as Cliente;
                Producto producto = new selectProducto().buscarProductosByEmpresaCliente(ctrEmpresa.empresaSelected.clave, cliente.clave);
                if (producto != null)
                {
                    this.mapForm(producto);
                    base.buscar();
                }
                else
                {
                    base.nuevo();
                }
            }
            else
            {
                MessageBox.Show("Antes de realizar la b\x00fasqueda, elija un cliente.", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void cbxCliente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.cbxCliente.SelectedItem == null)
            {
                this.btnImprimir.IsEnabled = false;
            }
            else
            {
                this.btnImprimir.IsEnabled = true;
            }
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.ctrEmpresa.empresaSelected != null)
            {
                OperationResult result = new ClienteSvc().getClientesALLorById(this.ctrEmpresa.empresaSelected.clave, 0, true);
                if (result.typeResult == ResultTypes.success)
                {
                    this.cbxCliente.ItemsSource = result.result as List<Cliente>;
                }
            }
        }

        public override OperationResult eliminar()
        {
            Producto producto = this.mapForm();
            if (producto != null)
            {
                OperationResult result = new ProductoSvc().deleteProductos(producto.clave);
                if (result.typeResult == ResultTypes.success)
                {
                    base.eliminar();
                    this.mapForm(null);
                }
                return result;
            }
            return new OperationResult
            {
                mensaje = "Ocurrio un error",
                result = null,
                valor = 1,
                identity = 0
            };
        }

        public override OperationResult guardar()
        {
            Producto producto = this.mapForm();
            if (producto != null)
            {
                OperationResult result = new ProductoSvc().saveProducto(ref producto);
                if (result.typeResult == ResultTypes.success)
                {
                    base.guardar();
                    this.mapForm(producto);
                }
                return result;
            }
            return new OperationResult
            {
                mensaje = "Ocurrio un error",
                result = null,
                valor = 1,
                identity = 0
            };
        }
        private Producto mapForm()
        {
            try
            {
                return new Producto
                {
                    clave = (this.txtClave.Tag == null) ? 0 : ((Producto)this.txtClave.Tag).clave,
                    clave_fraccion = (this.ctrEmpresa.empresaSelected.clave == 2) ? 6 : ((Cliente)this.cbxCliente.SelectedItem).fraccion.clave,
                    clave_empresa = this.ctrEmpresa.empresaSelected.clave,
                    clave_externo = this.txtClaveExtra.Text,
                    descripcion = this.txtDescripcion.Text,
                    embalaje = "GRANEL",
                    clave_unidad_de_medida = (cbxUnidadMedida.SelectedItem as UnidadMedida).medida, // this.cbxUnidadMedida.SelectedItem.ToString(),
                    clasificacion = (this.cbxClasificaciones.SelectedItem == null) ? null : (this.cbxClasificaciones.SelectedItem as ClasificacionProductos)
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void mapForm(Producto producto)
        {
            if (producto != null)
            {
                this.txtClave.Tag = producto;
                this.txtClave.Text = producto.clave.ToString();
                this.txtClaveExtra.Text = producto.clave_externo;
                this.txtDescripcion.Text = producto.descripcion;
                if (producto.clasificacion == null)
                {
                    this.cbxClasificaciones.SelectedItem = null;
                }
                else
                {
                    this.cbxClasificaciones.SelectedItem = this.cbxClasificaciones.ItemsSource.Cast<ClasificacionProductos>().ToList<ClasificacionProductos>().Find(s => s.idClasificacionProducto == producto.clasificacion.idClasificacionProducto);
                }
                cbxUnidadMedida.SelectedItem = cbxUnidadMedida.ItemsSource.Cast<UnidadMedida>().ToList().Find(s => s.medida == producto.clave_unidad_de_medida);
            }
            else
            {
                this.txtClave.Tag = null;
                this.txtClave.Clear();
                this.txtClaveExtra.Clear();
                this.txtDescripcion.Clear();
                this.cbxCliente.SelectedItem = null;
                this.cbxClasificaciones.SelectedItem = null;
                cbxUnidadMedida.SelectedIndex = 0;
            }
        }

        private void mostrarMesaje(OperationResult resp)
        {
            switch (resp.typeResult)
            {
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
                    base.mainWindow.scrollContainer.IsEnabled = false;
                    break;

                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    base.mainWindow.scrollContainer.IsEnabled = false;
                    break;

                case ResultTypes.recordNotFound:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    base.mainWindow.scrollContainer.IsEnabled = false;
                    break;
            }
        }

        public override void nuevo()
        {
            this.mapForm(null);
            base.nuevo();
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.ctrEmpresa.cbxEmpresa.SelectionChanged += new SelectionChangedEventHandler(this.CbxEmpresa_SelectionChanged);
            if (new PrivilegioSvc().consultarPrivilegio("ISADMIN", base.mainWindow.inicio._usuario.idUsuario).typeResult == ResultTypes.success)
            {
                this.ctrEmpresa.loaded(base.mainWindow.empresa, true);
            }
            else
            {
                this.ctrEmpresa.loaded(base.mainWindow.empresa, false);
            }

            var resp = new ProductoSvc().getAllUnidadesMedida();
            if (resp.typeResult == ResultTypes.success)
            {
                cbxUnidadMedida.ItemsSource = resp.result as List<UnidadMedida>;
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                return;
            }

            //this.cbxUnidadMedida.Items.Add("TNS");
            //this.cbxUnidadMedida.Items.Add("M3");
            //this.cbxUnidadMedida.Items.Add("PZA");
            //this.cbxUnidadMedida.Items.Add("TARIMA");
            this.cbxUnidadMedida.SelectedIndex = 0;
            OperationResult result2 = new ClasificacionProductosSvc().getAllClasificacionProductos();
            if (result2.typeResult == ResultTypes.success)
            {
                this.cbxClasificaciones.ItemsSource = result2.result as List<ClasificacionProductos>;
            }
            this.nuevo();
        }
    }
}
