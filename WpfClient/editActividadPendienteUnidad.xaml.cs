﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editActividadPendienteUnidad.xaml
    /// </summary>
    public partial class editActividadPendienteUnidad : ViewBase
    {
        public editActividadPendienteUnidad()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ctrUnidadTransporte.DataContextChanged += CtrUnidadTransporte_DataContextChanged;

            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresa.loaded(mainWindow.empresa);


            //ctrEmpresa.lbl.Width = 140;
            //ctrEmpresa.cbxEmpresa.Width = 180;
            cbxTipoOrden.ItemsSource = Enum.GetValues(typeof(enumTipoOrden));
            nuevo();
            //cbxTipoOrdenServicio.ItemsSource = Enum.GetValues(typeof(enumTipoOrdServ));
        }
        public override void nuevo()
        {
            base.nuevo();
            ctrUnidadTransporte.limpiar();
            cbxTipoOrden.SelectedItem = null;

            cbxActividades.ItemsSource = null;
            cbxServicio.ItemsSource = null;

            cbxServicio.IsEnabled = false;
            cbxActividades.IsEnabled = false;
            stpPosiciones.IsEnabled = false;
            cbxChcPosLlantas.SelectedItems.Clear();
            chcTodosPosiciones.IsChecked = false;

            stpActividadesServicios.Children.Clear();
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
        }
        private void CtrUnidadTransporte_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (ctrUnidadTransporte.DataContext != null)
            {
                if (ctrUnidadTransporte.unidadTransporte != null)
                {
                    cargarPosicionesLlantas(ctrUnidadTransporte.unidadTransporte);
                }
                else
                {
                    cbxChcPosLlantas.Items.Clear();
                }
            }
            else
            {
                cbxChcPosLlantas.Items.Clear();
            }
        }

        private void cargarPosicionesLlantas(UnidadTransporte unidadTransporte)
        {
            try
            {
                cbxChcPosLlantas.Items.Clear();
                chcTodosPosiciones.IsChecked = false;
                cbxChcPosLlantas.Items.Clear();

                var resp = new DiagramasLLantasSvc().getDiagramaByTipoUnidadTrans(unidadTransporte.tipoUnidad);
                if (resp.typeResult == ResultTypes.success)
                {
                    DiagramaLLantas diagrama = resp.result as DiagramaLLantas;
                    for (int i = 1; i <= diagrama.numLLantas; i++)
                    {
                        cbxChcPosLlantas.Items.Add(i);
                    }
                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ctrUnidadTransporte.loaded(etipoUniadBusqueda.TODAS, ctrEmpresa.empresaSelected);
        }

        private void cbxTipoOrden_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender != null)
            {
                cbxTipoOrdenServicio.Items.Clear();
                if (cbxTipoOrden.SelectedItem != null)
                {
                    enumTipoOrden enumTipo = (enumTipoOrden)(sender as ComboBox).SelectedItem;
                    switch (enumTipo)
                    {
                        case enumTipoOrden.PREVENTIVO:
                            cbxTipoOrdenServicio.Items.Add(enumTipoOrdServ.LAVADERO);
                            cbxTipoOrdenServicio.Items.Add(enumTipoOrdServ.TALLER);
                            break;
                        case enumTipoOrden.CORRECTIVO:
                            cbxTipoOrdenServicio.Items.Add(enumTipoOrdServ.LLANTAS);
                            cbxTipoOrdenServicio.Items.Add(enumTipoOrdServ.TALLER);
                            break;
                        default:
                            break;
                    }
                }

            }
        }

        private void cbxTipoOrdenServicio_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                cbxActividades.ItemsSource = null;
                cbxServicio.ItemsSource = null;

                cbxServicio.IsEnabled = false;
                cbxActividades.IsEnabled = false;
                stpPosiciones.IsEnabled = false;
                cbxChcPosLlantas.SelectedItems.Clear();
                chcTodosPosiciones.IsChecked = false;

                if (sender != null)
                {

                    if (cbxTipoOrdenServicio.SelectedItem != null)
                    {
                        enumTipoOrdServ enumTipo = (enumTipoOrdServ)(sender as ComboBox).SelectedItem;
                        switch (enumTipo)
                        {
                            case enumTipoOrdServ.COMBUSTIBLE:
                                break;
                            case enumTipoOrdServ.TALLER:
                                {
                                    enumTipoOrden tipoOrden = (enumTipoOrden)cbxTipoOrden.SelectedItem;
                                    if (tipoOrden == enumTipoOrden.CORRECTIVO)
                                    {
                                        cargarActividades(enumTipo);
                                        cbxActividades.IsEnabled = true;
                                    }
                                    else if (tipoOrden == enumTipoOrden.PREVENTIVO)
                                    {
                                        cargarServicios(enumTipo);
                                        cbxServicio.IsEnabled = true;
                                    }
                                }
                                break;
                            case enumTipoOrdServ.LLANTAS:
                                {
                                    cargarActividades(enumTipo);
                                    stpPosiciones.IsEnabled = true;
                                    cbxActividades.IsEnabled = true;
                                }                                
                                break;
                            case enumTipoOrdServ.LAVADERO:
                                cbxActividades.IsEnabled = true;
                                cargarActividades(enumTipo);
                                break;
                            case enumTipoOrdServ.RESGUARDO:
                                break;
                            default:
                                break;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }

        }
        void cargarActividades(enumTipoOrdServ tipoOrdenServ)
        {
            try
            {
                cbxActividades.ItemsSource = null;
                var resp = new OrdenesTrabajoSvc().getRelacionActividad((int)tipoOrdenServ);
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxActividades.ItemsSource = (List<Actividad>)resp.result;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        void cargarServicios(enumTipoOrdServ tipoOrdenServ)
        {
            try
            {
                var resp = new OrdenesTrabajoSvc().getRelacionServicios((int)tipoOrdenServ);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Servicio> lista = (List<Servicio>)resp.result;
                    cbxServicio.ItemsSource = lista;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void btnAddActividadTALLER_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (cbxTipoOrdenServicio.SelectedItem != null)
                {
                    if (ctrUnidadTransporte.unidadTransporte == null) return;
                    List<ActividadPendiente> listActSer = new List<ActividadPendiente>();
                    enumTipoOrdServ enumTipo = (enumTipoOrdServ)cbxTipoOrdenServicio.SelectedItem;

                    if (enumTipo == enumTipoOrdServ.LLANTAS)
                    {
                        foreach (int posicion in cbxChcPosLlantas.SelectedItems)
                        {
                            crearActividadPendiente(enumTipo, posicion);
                        }
                    }
                    else
                    {
                        crearActividadPendiente(enumTipo);
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void crearActividadPendiente(enumTipoOrdServ enumTipo, int posicion = 0)
        {
            try
            {
                ActividadPendiente actividad = new ActividadPendiente
                {
                    idActividadPendiente = 0,
                    unidad = ctrUnidadTransporte.unidadTransporte,
                    idOrdenServicio = 0,
                    enumTipoOrden = (enumTipoOrden)cbxTipoOrden.SelectedItem,
                    enumTipoOrdServ = enumTipo,
                    actividad = cbxActividades.SelectedItem == null ? null : cbxActividades.SelectedItem as Actividad,
                    servicio = cbxServicio.SelectedItem == null ? null : cbxServicio.SelectedItem as Servicio,
                    posicion = posicion,
                    usuario = mainWindow.usuario.nombreUsuario,
                    estatus = true
                };
                crearControlesActividadPendiente(actividad);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void crearControlesActividadPendiente(ActividadPendiente actPendiente)
        {
            try
            {
                StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal , Width = 740, Tag = actPendiente};

                Label lblUnidad = new Label
                {
                    Content = actPendiente.unidad.clave,
                    FontWeight = FontWeights.Medium,
                    Width = 80,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Margin = new Thickness(2),
                    Foreground = Brushes.SteelBlue
                };

                TextBox txtAct = new TextBox
                {
                    Text = actPendiente.actividad == null ? actPendiente.servicio.nombre : actPendiente.actividad.nombre,
                    IsReadOnly = true,
                    Width = 450,
                    Margin = new Thickness(2),
                    TextWrapping = TextWrapping.Wrap,
                    Foreground = Brushes.SteelBlue,
                    BorderBrush = null,
                    Background = null,
                    FontWeight = FontWeights.Medium,
                    FontSize = 10.5,
                    VerticalAlignment = VerticalAlignment.Center
                };

                Label lblPosicion = new Label
                {
                    Content = actPendiente.posicion == 0 ? string.Empty : actPendiente.posicion.ToString(),
                    FontWeight = FontWeights.Medium,
                    Width = 80,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Margin = new Thickness(2),
                    Foreground = Brushes.SteelBlue
                };

                Button btnBorrar = new Button
                {
                    Content = "BORRAR",
                    Background = Brushes.Orange,
                    Foreground = Brushes.White,
                    Width = 100,
                    Margin = new Thickness(2),
                    HorizontalAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Tag = stpContent
                };
                btnBorrar.Click += BtnBorrar_Click;

                stpContent.Children.Add(lblUnidad);
                stpContent.Children.Add(txtAct);
                stpContent.Children.Add(lblPosicion);
                stpContent.Children.Add(btnBorrar);
                stpActividadesServicios.Children.Add(stpContent);

            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void BtnBorrar_Click(object sender, RoutedEventArgs e)
        {
            StackPanel stpContet = (sender as Button).Tag as StackPanel;
            stpActividadesServicios.Children.Remove(stpContet);
        }

        private void chcTodosPosiciones_Checked(object sender, RoutedEventArgs e)
        {
            foreach (var item in cbxChcPosLlantas.Items)
            {
                cbxChcPosLlantas.SelectedItems.Add(item);
            }
        }

        private void chcTodosPosiciones_Unchecked(object sender, RoutedEventArgs e)
        {
            cbxChcPosLlantas.SelectedItems.Clear();
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;

                List<ActividadPendiente> listaActPend = new List<ActividadPendiente>();
                foreach (StackPanel stpContent in stpActividadesServicios.Children.Cast<StackPanel>().ToList())
                {
                    listaActPend.Add(stpContent.Tag as ActividadPendiente);
                }

                if (listaActPend.Count > 0)
                {
                    var resp = new SolicitudOrdenServicioSvc().saveActividadesPendientes(ref listaActPend);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        nuevo();
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult(ResultTypes.warning, "SIN ACTIVIDADES PENDIENTES PARA GUARDAR");
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ctrUnidadTransporte.limpiar();
        }
    }
}
