﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para controlResumenViajes.xaml
    /// </summary>
    public partial class controlResumenViajes : UserControl
    {
        public controlResumenViajes()
        {
            InitializeComponent();
        }
        List<ReporteCartaPorteViajes>  listaViajes = null;
        public controlResumenViajes(List<ReporteCartaPorteViajes> listaViajes)
        {
            InitializeComponent();
            this.listaViajes = listaViajes;
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            cargarControles(this.listaViajes, diasEvaluados);
        }
        int diasEvaluados = 0;
        public void cargarControles(List<ReporteCartaPorteViajes> listaViajes = null, int diasEvaluados = 0)
        {
            this.diasEvaluados = diasEvaluados;
            if (listaViajes != null)
            {
                txtNoViajes.Text = listaViajes.Count().ToString("N0");

                //decimal sumaKm = 0;
                //decimal sumaToneladas = 0;
                //decimal sumaSubtotal = 0;
                //decimal sumaIVA = 0;
                //decimal sumaRetencion = 0;

                //foreach (var viaje in listaViajes)
                //{
                //    sumaKm += viaje.km.Max(s => s.km);
                //    sumaToneladas += viaje.listDetalles.Sum(s => s.vDescarga);
                //    sumaSubtotal += viaje.listDetalles.Sum(s => s.importeReal);
                //    sumaIVA += viaje.listDetalles.Sum(s => s.ImporIVA);
                //    sumaRetencion += viaje.listDetalles.Sum(s => s.ImpRetencion);
                //}
                txtDias.Text = diasEvaluados.ToString("N0");
                txtKm.Text = listaViajes.Sum(s => s.km).ToString("N0"); // sumaKm.ToString("N");
                txtToneladas.Text = listaViajes.Sum(s => s.toneladas).ToString("N4"); // sumaToneladas.ToString("N4");
                txtSubTotal.Text = listaViajes.Sum(s=> s.subTotal).ToString("C4") ;// sumaSubtotal.ToString("C4");
                txtIva.Text = listaViajes.Sum(s=> s.impIva).ToString("C4"); //sumaIVA.ToString("C4");
                txtRetencion.Text = listaViajes.Sum(s => s.impRetencion).ToString("C4");// sumaRetencion.ToString("C4");
                txtTotal.Text = listaViajes.Sum(s => s.importe).ToString("C4"); // (sumaSubtotal + (sumaIVA - sumaRetencion)).ToString("C4");
            }
            else
            {
                txtNoViajes.Text = "0";                
                txtKm.Text = "0";
                txtToneladas.Text = "0";
                txtSubTotal.Text = "$ 0";
                txtIva.Text = "$ 0";
                txtRetencion.Text = "$ 0";
                txtTotal.Text = "$ 0";
            }
        }        
    }
}
