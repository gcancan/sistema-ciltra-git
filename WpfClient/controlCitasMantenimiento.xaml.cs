﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using Core.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para controlCitasMantenimiento.xaml
    /// </summary>
    public partial class controlCitasMantenimiento : UserControl
    {        
        private MainWindow mainWindow = null;
        public controlCitasMantenimiento(MainWindow mainWindow, SemaforoMtto semaforo)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
            this.semaforo = semaforo;
        }
        public controlCitasMantenimiento()
        {
            InitializeComponent();
        }
        private SemaforoMtto semaforo = null;
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            dtFecha.Value = DateTime.Now;

            var resp = new ServiciosSvc().getAllServicios();
            if (resp.typeResult == ResultTypes.success)
            {
                var lista = resp.result as List<Servicio>;
                lista = lista.FindAll(s => s.idServicio == 1 || s.idServicio == 2 || s.idServicio == 13 || s.idServicio == 23 || s.idServicio == 24).ToList();
                cbxServicio.ItemsSource = lista;
            }

            if (this.semaforo != null)
            {
                ctrUnidades.unidadTransporte = new UnidadTransporte { clave = semaforo.claveUnidad };
                stpUnidad.IsEnabled = false;

                dtFecha.Value = semaforo.fechaProxMtto;
                cbxServicio.SelectedItem = cbxServicio.ItemsSource.Cast<Servicio>().ToList().Find(s => s.idServicio == semaforo.proximoServicio.idServicio);
            }
            else
            {
                ctrUnidades.loaded(etipoUniadBusqueda.TRACTOR, null);
                btnGuardar.Visibility = Visibility.Hidden;
            }
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void BtnGuardar_Click(object sender, RoutedEventArgs e)
        {
            var resp = guardarForm();
            ImprimirMensaje.imprimir(resp);
        }
        public OperationResult guardarForm()
        {
            try
            {
                Cursor = Cursors.Wait;
                var val = validar();
                if (val.typeResult != ResultTypes.success) return val;

                CitaMantenimiento citaMantenimiento = mapForm();
                if (citaMantenimiento != null)
                {
                    var resp = new CitaMantenimientoSvc().saveCitaMantenimiento(ref citaMantenimiento);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mapForm(citaMantenimiento);
                        this.DataContext = citaMantenimiento;
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult(ResultTypes.error, "Ocurrio un error al leer la información de la pantalla");
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public OperationResult eliminarForm()
        {
            try
            {
                Cursor = Cursors.Wait;                

                CitaMantenimiento citaMantenimiento = (ctrClave.Tag as CitaMantenimiento);
                if (citaMantenimiento != null)
                {
                    citaMantenimiento.activo = false;
                    var resp = new CitaMantenimientoSvc().saveCitaMantenimiento(ref citaMantenimiento);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mapForm(null);
                        this.DataContext = null;
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult(ResultTypes.error, "Ocurrio un error al leer la información de la pantalla");
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapForm(CitaMantenimiento citaMantenimiento)
        {
            try
            {
                if (citaMantenimiento != null)
                {
                    ctrClave.valor = citaMantenimiento.idCitaMantenimiento;
                    ctrClave.Tag = citaMantenimiento;
                    dtFecha.Value = citaMantenimiento.fechaCita;
                    ctrUnidades.unidadTransporte = citaMantenimiento.unidad;
                    stpUnidad.IsEnabled = false;
                    cbxServicio.SelectedItem = cbxServicio.ItemsSource.Cast<Servicio>().ToList().Find(s=> s.idServicio == citaMantenimiento.servicio.idServicio);
                }
                else
                {
                    ctrClave.valor = 0;
                    ctrClave.Tag = null;
                    dtFecha.Value = DateTime.Now;
                    ctrUnidades.unidadTransporte = null;
                    stpUnidad.IsEnabled = true;
                    cbxServicio.SelectedItem = null;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private OperationResult validar()
        {
            try
            {
                OperationResult resp = new OperationResult(ResultTypes.success, "PARA CONTINUAR:" + Environment.NewLine);
                if (dtFecha.Value == null)
                {
                    resp.valor = 2;
                    resp.mensaje += "  * Seleccionar una fecha." + Environment.NewLine;
                }
                if(ctrUnidades.unidadTransporte == null)
                {
                    resp.valor = 2;
                    resp.mensaje += "  * Seleccionar una unidad de transporte." + Environment.NewLine;
                }
                if (cbxServicio.SelectedItem == null)
                {
                    resp.valor = 2;
                    resp.mensaje += "  * Seleccionar un servicio." + Environment.NewLine;
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        private CitaMantenimiento mapForm()
        {
            try
            {
                CitaMantenimiento citaMantenimiento = new CitaMantenimiento
                {
                    idCitaMantenimiento = ctrClave.Tag == null ? 0 : (ctrClave.Tag as CitaMantenimiento).idCitaMantenimiento,
                     fechaCaptura = DateTime.Now,
                     fechaCita = dtFecha.Value.Value,
                     unidad = ctrUnidades.unidadTransporte,
                     usuario = mainWindow.usuario.nombreUsuario,
                     servicio = cbxServicio.SelectedItem as Servicio
                };
                return citaMantenimiento;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ctrUnidades.limpiar();
        }
        public void nuevo()
        {
            mapForm(null);
        }
        public CitaMantenimiento buscarForm()
        {
            CitaMantenimiento citaMantenimiento = new selectCitasMtto().buscarCitaMantenimiento();
            mapForm(citaMantenimiento);
            return citaMantenimiento;
        }
    }
}
