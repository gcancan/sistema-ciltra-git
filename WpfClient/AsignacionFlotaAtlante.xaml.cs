﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para AsignacionFlotaAtlante.xaml
    /// </summary>
    public partial class AsignacionFlotaAtlante : ViewBase
    {
        public AsignacionFlotaAtlante()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresa.loaded(new Empresa { clave = 2 });
            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
        }
        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            empresa = ctrEmpresa.empresaSelected;
            
            cargarZonasOperativas();
            cargarUnidades();
        }
        StackPanel _stp = null;
        private void cbxZonasOperativas_ItemSelectionChanged(object sender, Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                stpOperaciones.Children.Clear();
                foreach (ZonaOperativa item in cbxZonasOperativas.SelectedItems)
                {
                    OperacionFletera operacionFletera = new OperacionFletera
                    {
                        cliente = null,
                        empresa = ctrEmpresa.empresaSelected,
                        zonaOperativa = item
                    };
                    GroupBox combo = new GroupBox
                    {
                        Tag = operacionFletera,
                        Header = operacionFletera.nombre,
                        Height = 470,
                        Width = 360,
                        VerticalAlignment = VerticalAlignment.Top,
                        HorizontalAlignment = HorizontalAlignment.Left,
                        Margin = new Thickness(0, 0, 2, 0)
                    };
                    StackPanel stpContent = new StackPanel();

                    ScrollViewer scroll = new ScrollViewer() { Height = 400 };
                    StackPanel stpControles = new StackPanel();
                    scroll.Content = stpControles;

                    var resp = new FlotaSvc().getFlota(operacionFletera);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        foreach (Flota flota in resp.result as List<Flota>)
                        {
                            _stp = stpControles;
                            stpControles.Children.Add(agregarRbnFlota(flota));
                        }
                    }
                    else if (resp.typeResult == ResultTypes.error)
                    {
                        imprimir(resp);
                    }

                    StackPanel stpBotones = new StackPanel { Orientation = Orientation.Horizontal };
                    Button btnAsignar = new Button
                    {
                        Name = "btnAsignar",
                        Content = "ASIGNAR",
                        Width = 120,
                        Background = Brushes.Green,
                        Foreground = Brushes.White,
                        Margin = new Thickness(2),
                        Tag = stpControles,
                        DataContext = operacionFletera
                    };
                    btnAsignar.Click += BtnAsignar_Click;
                    Button btnAgregar = new Button
                    {
                        Name = "btnAsignar",
                        Content = "AGREGAR",
                        Width = 120,
                        Background = Brushes.SteelBlue,
                        Foreground = Brushes.White,
                        Margin = new Thickness(2),
                        Tag = stpControles,
                        DataContext = operacionFletera
                    };
                    btnAgregar.Click += BtnAgregar_Click;
                    stpBotones.Children.Add(btnAsignar);
                    stpBotones.Children.Add(btnAgregar);

                    stpContent.Children.Add(scroll);
                    stpContent.Children.Add(stpBotones);
                    combo.Content = stpContent;

                    stpOperaciones.Children.Add(combo);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void BtnAgregar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                if (btn != null)
                {
                    StackPanel stpControles = btn.Tag as StackPanel;
                    OperacionFletera operacionFletera = btn.DataContext as OperacionFletera;
                    Flota flota = new Flota().mapFlota(operacionFletera);
                    var resp = new FlotaSvc().saveFlota(ref flota);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        _stp = stpControles;
                        RadioButton rbn = agregarRbnFlota(flota);
                        stpControles.Children.Add(rbn);
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private RadioButton agregarRbnFlota(Flota flota)
        {
            try
            {
                UnidadTransporte tractor = flota.tractor ?? flota.tractor;
                UnidadTransporte remolque1 = flota.remolque1 ?? flota.remolque1;
                UnidadTransporte dolly = flota.dolly ?? flota.dolly;
                UnidadTransporte remolque2 = flota.remolque2 ?? flota.remolque2;
                RadioButton rbnControlFlota = new RadioButton
                {
                    FontSize = 11,
                    Width = 400,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    VerticalAlignment = VerticalAlignment.Center,
                    Tag = flota,
                    DataContext = _stp
                };

                StackPanel stpContenedor = new StackPanel { Orientation = Orientation.Horizontal, Name = "stpContenedor" };

                #region stpTractor
                StackPanel stpTractor = new StackPanel() { Tag = flota.tractor, Orientation = Orientation.Horizontal, Name = "stpTractor" };
                Label lblTractor = new Label()
                {
                    Content = tractor == null ? string.Empty : tractor.clave,
                    Padding = new Thickness(-4),
                    Foreground = Brushes.SteelBlue,
                    FontWeight = FontWeights.Bold,
                    Width = 45,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    BorderThickness = new Thickness(0, 0, 0, 1.5),
                    BorderBrush = Brushes.Black
                };
                Button btnQuitar = new Button
                {
                    Content = "X",
                    Width = 25,
                    Height = 30,
                    //FontSize = 8,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    Padding = new Thickness(8),
                    Foreground = Brushes.Black,
                    Background = Brushes.Gold,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Visibility = tractor == null ? Visibility.Hidden : Visibility.Visible,
                    Tag = stpTractor,
                    DataContext = flota
                };
                btnQuitar.Click += BtnQuitar_Click;
                stpTractor.Children.Add(lblTractor);
                stpTractor.Children.Add(btnQuitar);
                #endregion

                #region remolque1
                StackPanel stpRemolque1 = new StackPanel() { Tag = remolque1, Orientation = Orientation.Horizontal, Name = "stpRemolque1" };
                Label lblRemolque1 = new Label()
                {
                    Content = remolque1 == null ? string.Empty : remolque1.clave,
                    Padding = new Thickness(-4),
                    Foreground = Brushes.SteelBlue,
                    FontWeight = FontWeights.Bold,
                    Width = 45,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    BorderThickness = new Thickness(0, 0, 0, 1.5),
                    BorderBrush = Brushes.Black
                };
                Button btnQuitarRemolque1 = new Button
                {
                    Content = "X",
                    Width = 25,
                    Height = 30,
                    //FontSize = 8,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    Padding = new Thickness(8),
                    Foreground = Brushes.Black,
                    Background = Brushes.Gold,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Visibility = remolque1 == null ? Visibility.Hidden : Visibility.Visible,
                    Tag = stpRemolque1,
                    DataContext = flota
                };
                btnQuitarRemolque1.Click += BtnQuitarRemolque1_Click;
                stpRemolque1.Children.Add(lblRemolque1);
                stpRemolque1.Children.Add(btnQuitarRemolque1);
                #endregion

                #region Dooly
                StackPanel stpDolly = new StackPanel() { Tag = dolly, Orientation = Orientation.Horizontal, Name = "stpDolly" };
                Label lblDolly = new Label()
                {
                    Content = dolly == null ? string.Empty : dolly.clave,
                    Padding = new Thickness(-4),
                    Foreground = Brushes.SteelBlue,
                    FontWeight = FontWeights.Bold,
                    Width = 45,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    BorderThickness = new Thickness(0, 0, 0, 1.5),
                    BorderBrush = Brushes.Black
                };
                Button btnQuitarDolly = new Button
                {
                    Content = "X",
                    Width = 25,
                    Height = 30,
                    //FontSize = 8,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    Padding = new Thickness(8),
                    Foreground = Brushes.Black,
                    Background = Brushes.Gold,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Visibility = dolly == null ? Visibility.Hidden : Visibility.Visible,
                    Tag = stpDolly,
                    DataContext = flota
                };
                btnQuitarDolly.Click += BtnQuitarDolly_Click;
                stpDolly.Children.Add(lblDolly);
                stpDolly.Children.Add(btnQuitarDolly);
                #endregion

                #region Remolque2
                StackPanel stpRemolque2 = new StackPanel() { Tag = remolque2, Orientation = Orientation.Horizontal, Name = "stpRemolque2" };
                Label lblRemolque2 = new Label()
                {
                    Content = remolque2 == null ? string.Empty : remolque2.clave,
                    Padding = new Thickness(-4),
                    Foreground = Brushes.SteelBlue,
                    FontWeight = FontWeights.Bold,
                    Width = 45,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    BorderThickness = new Thickness(0, 0, 0, 1.5),
                    BorderBrush = Brushes.Black
                };
                Button btnQuitarRemolque2 = new Button
                {
                    Content = "X",
                    Width = 25,
                    Height = 30,
                    //FontSize = 8,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    Padding = new Thickness(8),
                    Foreground = Brushes.Black,
                    Background = Brushes.Gold,
                    Visibility = remolque2 == null ? Visibility.Hidden : Visibility.Visible,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Tag = stpRemolque2,
                    DataContext = flota
                };
                btnQuitarRemolque2.Click += BtnQuitarRemolque2_Click;
                stpRemolque2.Children.Add(lblRemolque2);
                stpRemolque2.Children.Add(btnQuitarRemolque2);
                #endregion

                //Visibility visible = Visibility.Visible;
                //if (tractor == null && remolque1 == null && dolly == null && remolque2 == null)
                //{
                //    visible = Visibility.Hidden;
                //}
                Button btnQuitarTodo = new Button { Content = "x", Foreground = Brushes.White, Background = Brushes.Red, Width = 25, Tag = rbnControlFlota };
                btnQuitarTodo.Click += BtnQuitarTodo_Click;

                stpContenedor.Children.Add(stpTractor);
                stpContenedor.Children.Add(stpRemolque1);
                stpContenedor.Children.Add(stpDolly);
                stpContenedor.Children.Add(stpRemolque2);
                stpContenedor.Children.Add(btnQuitarTodo);

                rbnControlFlota.Content = stpContenedor;

                return rbnControlFlota;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private void BtnQuitarTodo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                Button btn = sender as Button;
                RadioButton rbn = btn.Tag as RadioButton;
                StackPanel stp = rbn.DataContext as StackPanel;
                Flota flota = rbn.Tag as Flota;
                var resp = new FlotaSvc().eliminarFlota(flota);
                if (resp.typeResult == ResultTypes.success)
                {
                    stp.Children.Remove(rbn);
                    cargarUnidades();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void BtnQuitarRemolque1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                Button btn = sender as Button;
                Flota flota = btn.DataContext as Flota;
                StackPanel stpUnidad = btn.Tag as StackPanel;
                UnidadTransporte remolque1 = stpUnidad.Tag as UnidadTransporte;

                CheckBox check = new CheckBox
                {
                    Tag = remolque1,
                    Content = remolque1.clave,
                    FontWeight = FontWeights.Bold,
                    Foreground = Brushes.SteelBlue
                };
                check.Checked += Check_Checked;
                check.Unchecked += Check_Unchecked;
                stpRemolques.Children.Add(check);

                flota.remolque1 = null;
                var resp = new FlotaSvc().saveFlota(ref flota);
                //stpRemolques.Children.Add(check);
                quitarControlUnidad(stpUnidad);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void BtnQuitarDolly_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                Button btn = sender as Button;
                Flota flota = btn.DataContext as Flota;
                StackPanel stpUnidad = btn.Tag as StackPanel;
                UnidadTransporte tractor = stpUnidad.Tag as UnidadTransporte;
                RadioButton radioButton = new RadioButton
                {
                    Name = "radioButtonDolly",
                    Tag = tractor,
                    Content = tractor.clave,
                    FontWeight = FontWeights.Bold,
                    Foreground = Brushes.SteelBlue
                };
                radioButton.Checked += RadioButton_Checked;
                flota.dolly = null;
                var resp = new FlotaSvc().saveFlota(ref flota);
                stpDolly.Children.Add(radioButton);
                quitarControlUnidad(stpUnidad);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void BtnQuitarRemolque2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                Button btn = sender as Button;
                Flota flota = btn.DataContext as Flota;
                StackPanel stpUnidad = btn.Tag as StackPanel;
                UnidadTransporte remolque1 = stpUnidad.Tag as UnidadTransporte;

                CheckBox check = new CheckBox
                {
                    Tag = remolque1,
                    Content = remolque1.clave,
                    FontWeight = FontWeights.Bold,
                    Foreground = Brushes.SteelBlue
                };
                check.Checked += Check_Checked;
                check.Unchecked += Check_Unchecked;
                //stpRemolques.Children.Add(check);

                flota.remolque2 = null;
                var resp = new FlotaSvc().saveFlota(ref flota);
                stpRemolques.Children.Add(check);
                quitarControlUnidad(stpUnidad);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void BtnQuitar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                Button btn = sender as Button;
                Flota flota = btn.DataContext as Flota;
                StackPanel stpUnidad = btn.Tag as StackPanel;
                UnidadTransporte tractor = stpUnidad.Tag as UnidadTransporte;
                RadioButton radioButton = new RadioButton
                {
                    Name = "radioButtonTractor",
                    Tag = tractor,
                    Content = tractor.clave,
                    FontWeight = FontWeights.Bold,
                    Foreground = Brushes.SteelBlue
                };
                radioButton.Checked += RadioButton_Checked;
                flota.tractor = null;
                var resp = new FlotaSvc().saveFlota(ref flota);
                stpTractores.Children.Add(radioButton);
                quitarControlUnidad(stpUnidad);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }

            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void BtnAsignar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                Button btn = sender as Button;
                if (btn != null)
                {
                    StackPanel stpControles = btn.Tag as StackPanel;
                    OperacionFletera operacionFletera = btn.DataContext as OperacionFletera;
                    asignarUnidadesFlota(stpControles, operacionFletera);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        void quitarControlUnidad(StackPanel stpUnidad)
        {
            stpUnidad.Tag = null;
            foreach (Control item in stpUnidad.Children.Cast<Control>().ToList())
            {
                if (item is Label)
                {
                    Label lbl = item as Label;
                    lbl.Tag = null;
                    lbl.Content = string.Empty;
                }
                if (item is Button)
                {
                    Button btn = item as Button;
                    btn.Visibility = Visibility.Hidden;
                }
            }
        }
        private void asignarUnidadesFlota(StackPanel stpControles, OperacionFletera operacionFletera)
        {
            try
            {
                Cursor = Cursors.Wait;
                RadioButton rbnSelect = null;
                foreach (RadioButton rbn in stpControles.Children.Cast<RadioButton>().ToList())
                {
                    if (rbn.IsChecked.Value)
                    {
                        rbnSelect = rbn;
                        break;
                    }
                }
                if (rbnSelect == null)
                {
                    return;
                }
                Flota flota = rbnSelect.Tag as Flota;
                StackPanel _stpTractor = null;
                StackPanel _stpRemolque1 = null;
                StackPanel _stpDolly = null;
                StackPanel _stpRemolque2 = null;
                if (rbnSelect != null)
                {
                    foreach (object ctr in (rbnSelect.Content as StackPanel).Children.Cast<object>().ToList())
                    {
                        if (ctr is StackPanel)
                        {
                            StackPanel strCtr = ctr as StackPanel;
                            switch (strCtr.Name)
                            {
                                case "stpTractor":
                                    _stpTractor = strCtr;
                                    break;
                                case "stpRemolque1":
                                    _stpRemolque1 = strCtr;
                                    break;
                                case "stpDolly":
                                    _stpDolly = strCtr;
                                    break;
                                case "stpRemolque2":
                                    _stpRemolque2 = strCtr;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    if (rbnTractorSelect != null)
                    {
                        if (_stpTractor.Tag == null)
                        {
                            mapearControlUnidad(_stpTractor, tractorSelect);
                            stpTractores.Children.Remove(rbnTractorSelect);
                            flota.tractor = tractorSelect;
                            rbnTractorSelect = null;
                            tractorSelect = null;
                        }
                    }
                    if (rbnDollySelect != null)
                    {
                        if (_stpDolly.Tag == null)
                        {
                            mapearControlUnidad(_stpDolly, DollySelect);
                            stpDolly.Children.Remove(rbnDollySelect);
                            flota.dolly = DollySelect;
                            rbnDollySelect = null;
                            DollySelect = null;
                        }
                    }

                    if (listaCheckRemolques.Count == 1)
                    {
                        if (_stpRemolque1.Tag == null)
                        {
                            mapearControlUnidad(_stpRemolque1, listaRemolques[0]);
                            stpRemolques.Children.Remove(listaCheckRemolques[0]);
                            flota.remolque1 = listaRemolques[0];
                            listaRemolques = new List<UnidadTransporte>();
                            listaCheckRemolques = new List<CheckBox>();
                        }
                        else if (_stpRemolque2.Tag == null)
                        {
                            mapearControlUnidad(_stpRemolque2, listaRemolques[0]);
                            stpRemolques.Children.Remove(listaCheckRemolques[0]);
                            flota.remolque2 = listaRemolques[0];
                            listaRemolques = new List<UnidadTransporte>();
                            listaCheckRemolques = new List<CheckBox>();
                        }
                    }

                    if (listaCheckRemolques.Count == 2)
                    {
                        if (_stpRemolque1.Tag == null)
                        {
                            mapearControlUnidad(_stpRemolque1, listaRemolques[0]);
                            stpRemolques.Children.Remove(listaCheckRemolques[0]);
                            flota.remolque1 = listaRemolques[0];
                        }
                        if (_stpRemolque2.Tag == null)
                        {
                            mapearControlUnidad(_stpRemolque2, listaRemolques[1]);
                            stpRemolques.Children.Remove(listaCheckRemolques[1]);
                            flota.remolque2 = listaRemolques[1];

                        }
                        listaRemolques = new List<UnidadTransporte>();
                        listaCheckRemolques = new List<CheckBox>();
                        foreach (var item in stpRemolques.Children.Cast<CheckBox>().ToList())
                        {
                            item.IsChecked = false;
                        }
                    }

                    var resp = new FlotaSvc().saveFlota(ref flota);

                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        void mapearControlUnidad(StackPanel stpUnidad, UnidadTransporte unidadTransporte)
        {
            stpUnidad.Tag = unidadTransporte;
            foreach (Control item in stpUnidad.Children.Cast<Control>().ToList())
            {
                if (item is Label)
                {
                    Label lbl = item as Label;
                    lbl.Tag = unidadTransporte;
                    lbl.Content = unidadTransporte.clave;
                }
                if (item is Button)
                {
                    Button btn = item as Button;
                    btn.Visibility = Visibility.Visible;
                }
            }
        }
        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                CheckBox chc = sender as CheckBox;
                switch (chc.Name)
                {
                    case "chcTodosZonaOperativa":
                        cbxZonasOperativas.SelectedItems.Clear();
                        if (chc.IsChecked.Value)
                        {
                            foreach (var item in cbxZonasOperativas.Items)
                            {
                                cbxZonasOperativas.SelectedItems.Add(item);
                            }
                        }
                        break;
                    //case "chcTodosOperacion":
                    //    cbxOperacion.SelectedItems.Clear();
                    //    if (chc.IsChecked.Value)
                    //    {
                    //        foreach (var item in cbxOperacion.Items)
                    //        {
                    //            cbxOperacion.SelectedItems.Add(item);
                    //        }
                    //    }
                    //    break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        Empresa empresa = null;
        List<ZonaOperativa> listaZonaOpertiva = new List<ZonaOperativa>();
        void cargarZonasOperativas()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new ZonaOperativaSvc().getAllZonasOperativas();
                if (resp.typeResult == ResultTypes.success)
                {
                    listaZonaOpertiva = resp.result as List<ZonaOperativa>;
                    cbxZonasOperativas.ItemsSource = listaZonaOpertiva;
                }
                else
                {
                    cbxZonasOperativas.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void cargarUnidades()
        {
            try
            {
                stpTractores.Children.Clear();
                stpDolly.Children.Clear();
                stpRemolques.Children.Clear();
                Cursor = Cursors.Wait;
                var resp = new UnidadTransporteSvc().getTractoresSinFlota(empresa.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<UnidadTransporte> listaTractores = resp.result as List<UnidadTransporte>;
                    stpTractores.Children.Clear();
                    foreach (var item in listaTractores)
                    {
                        RadioButton radioButton = new RadioButton
                        {
                            Name = "radioButtonTractor",
                            Tag = item,
                            Content = item.clave,
                            FontWeight = FontWeights.Bold,
                            Foreground = Brushes.SteelBlue
                        };
                        radioButton.Checked += RadioButton_Checked;
                        stpTractores.Children.Add(radioButton);
                    }
                }
                resp = new UnidadTransporteSvc().getDollysSinFlota(empresa.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<UnidadTransporte> ListaDolly = resp.result as List<UnidadTransporte>;
                    stpDolly.Children.Clear();
                    foreach (var item in ListaDolly)
                    {
                        RadioButton radioButton = new RadioButton
                        {
                            Name = "radioButtonDolly",
                            Tag = item,
                            Content = item.clave,
                            FontWeight = FontWeights.Bold,
                            Foreground = Brushes.SteelBlue
                        };
                        radioButton.Checked += RadioButton_Checked;
                        stpDolly.Children.Add(radioButton);
                    }
                }
                resp = new UnidadTransporteSvc().getTolvasSinFlota(empresa.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<UnidadTransporte> listaRemolques = resp.result as List<UnidadTransporte>;
                    stpRemolques.Children.Clear();
                    foreach (var item in listaRemolques)
                    {
                        CheckBox check = new CheckBox
                        {
                            Tag = item,
                            Content = item.clave,
                            FontWeight = FontWeights.Bold,
                            Foreground = Brushes.SteelBlue
                        };
                        check.Checked += Check_Checked;
                        check.Unchecked += Check_Unchecked;
                        stpRemolques.Children.Add(check);
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                RadioButton rbn = sender as RadioButton;
                if (rbn != null)
                {
                    switch (rbn.Name)
                    {
                        case "radioButtonTractor":
                            rbnTractorSelect = rbn;
                            tractorSelect = rbn.Tag as UnidadTransporte;
                            break;
                        case "radioButtonDolly":
                            rbnDollySelect = rbn;
                            DollySelect = rbn.Tag as UnidadTransporte;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        List<UnidadTransporte> listaRemolques = new List<UnidadTransporte>();
        List<CheckBox> listaCheckRemolques = new List<CheckBox>();
        private void Check_Unchecked(object sender, RoutedEventArgs e)
        {
            CheckBox chc = sender as CheckBox;
            listaCheckRemolques.Remove(chc);
            listaRemolques.Remove(chc.Tag as UnidadTransporte);
        }

        private void Check_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox chc = sender as CheckBox;
            if (listaRemolques.Count < 2)
            {
                listaCheckRemolques.Add(chc);
                listaRemolques.Add(chc.Tag as UnidadTransporte);
            }
            else
            {
                chc.IsChecked = false;
            }
        }
        UnidadTransporte tractorSelect = null;
        RadioButton rbnTractorSelect = null;
        UnidadTransporte DollySelect = null;
        RadioButton rbnDollySelect = null;
    }
}
