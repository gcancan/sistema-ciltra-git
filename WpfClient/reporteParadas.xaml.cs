﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using CoreFletera.Interfaces;
    using CoreFletera.Models;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Runtime.CompilerServices;
    using System.Text;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Markup;
    using Xceed.Wpf.Toolkit;
    using Xceed.Wpf.Toolkit.Primitives;

    public partial class reporteParadas : WpfClient.ViewBase
    {
        private UsuarioCSI usuarioCSI = null;
        private List<ParadaEquipo> listaParadas = new List<ParadaEquipo>();
        private bool siFiltrar = false;


        public reporteParadas()
        {
            this.InitializeComponent();
        }
        private void btnConsultar_Click(object sender, RoutedEventArgs e)
        {
            metodo2();
        }
        public class PnaHeader
        {
            public ParametrosNew PNA { get; set; }
        }
        private void metodo2()
        {
            string al = string.Empty;
            try
            {
                if (this.cbxCuenta.SelectedItem != null)
                {
                    this.usuarioCSI = this.cbxCuenta.SelectedItem as UsuarioCSI;
                    base.Cursor = Cursors.Wait;
                    PnaHeader PNAParametros = new PnaHeader();
                    ParametrosNew parametros = new ParametrosNew
                    {
                        sUsuario = this.usuarioCSI.nombreUsuario,
                        sContrasenia = this.usuarioCSI.contraseña,
                        sFechaInicial = ctrFechas.fechaInicial.ToString("dd/MM/yyyy"),
                        sFechaFinal = ctrFechas.fechaFinal.ToString("dd/MM/yyyy"),
                        sHoraInicial = ctrFechas.fechaInicial.ToString("HH:mm"),
                        sHoraFinal = ctrFechas.fechaFinal.ToString("HH:mm")
                    };
                    PNAParametros.PNA = parametros;
                    string s = JsonConvert.SerializeObject(PNAParametros, Formatting.Indented);

                    byte[] bytes = Encoding.UTF8.GetBytes(s);
                    WebRequest request = WebRequest.Create("https://swservicios.mastrack.com.mx/Vehicles/USVehicles/");
                    request.Method = "POST";
                    request.ContentType = "application/json;";
                    request.ContentLength = bytes.Length;
                    request.GetRequestStream().Write(bytes, 0, bytes.Length);
                    request.Timeout = 9000000;
                    HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                    JObject obj2 = JObject.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd());

                    JToken tokenPNA = obj2.SelectToken("PNA");
                    //JToken token2 = tokenPNA.SelectToken("Respuesta");
                    PNA PNA = new PNA
                    {
                        Valido = tokenPNA.Value<bool>("Valido"),
                        Mensaje = tokenPNA.Value<string>("Mensaje"),
                        CodEstado = tokenPNA.Value<int>("CodEstado"),
                        Vehiculos = new List<Vehiculos>()
                    };

                    if (PNA.Valido)
                    {
                        List<Equipos> list = new List<Equipos>();
                        JArray arrayVehiculos = (JArray)tokenPNA.SelectToken("Vehiculos");
                        foreach (JToken tokenVehiculos in arrayVehiculos)
                        {
                            al = (string)tokenVehiculos["Alias"];
                            if (al == "TC373")
                            {

                            }
                            Vehiculos vehiculos = new Vehiculos()
                            {
                                Alias = (string)tokenVehiculos["Alias"],
                                FechaInicial = string.IsNullOrEmpty(tokenVehiculos["FechaInicial"].ToString()) ? null : (DateTime?)Convert.ToDateTime(tokenVehiculos["FechaInicial"].ToString()),
                                FechaFinal = string.IsNullOrEmpty(tokenVehiculos["FechaFinal"].ToString()) ? null : (DateTime?)Convert.ToDateTime(tokenVehiculos["FechaFinal"].ToString()),
                                DuracionTotalPNA = string.IsNullOrEmpty(tokenVehiculos["DuracionTotalPNA"].ToString()) ? new TimeSpan(0,0,0) : (TimeSpan)tokenVehiculos["DuracionTotalPNA"],
                                Detalles = new List<Detalles>()
                            };
                            if (tokenVehiculos.SelectToken("Detalles").Type == JTokenType.Null) continue;

                            JArray arrayDetalles = (JArray)tokenVehiculos.SelectToken("Detalles");
                            foreach (var tokenDetalles in arrayDetalles)
                            {
                                Detalles detalles = new Detalles
                                {
                                    DuracionPNA = (TimeSpan)tokenDetalles["DuracionPNA"],
                                    FechaInicial = Convert.ToDateTime(tokenDetalles["FechaInicial"].ToString()),
                                    FechaFinal = Convert.ToDateTime(tokenDetalles["FechaFinal"].ToString()),
                                    Latitud = (decimal)tokenDetalles["Latitud"],//latitud
                                    //LatitudFinal = (decimal)tokenDetalles["LatitudFinal"],//longitud
                                    Longitud = (decimal)tokenDetalles["Longitud"], // se van
                                    //LongitudFinal = (decimal)tokenDetalles["LongitudFinal"], // se van
                                    CategoriaGeocerca = (string)tokenDetalles["CategoriaGeocerca"],
                                    GeoCerca = (string)tokenDetalles["Geocerca"],
                                    Estado = (string)tokenDetalles["Estado"]
                                };
                                if (!string.IsNullOrEmpty(detalles.GeoCerca))
                                {

                                }
                                vehiculos.Detalles.Add(detalles);
                            }
                            PNA.Vehiculos.Add(vehiculos);
                        }
                        llenarLista(PNA);
                    }
                    else
                    {
                        OperationResult resp = new OperationResult
                        {
                            mensaje = PNA.Mensaje,
                            valor = 1
                        };
                        ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        private void metodo1()
        {
            try
            {
                if (this.cbxCuenta.SelectedItem != null)
                {
                    this.usuarioCSI = this.cbxCuenta.SelectedItem as UsuarioCSI;
                    base.Cursor = Cursors.Wait;
                    Parametros parametros = new Parametros
                    {
                        sUsuario = this.usuarioCSI.nombreUsuario,
                        sPassword = this.usuarioCSI.contraseña,
                        sSerie = "Todas",
                        dtFechaInicial = this.ctrFechas.fechaInicial.ToString("MM-dd-yyyy HH:mm:ss"),
                        dtFechaFinal = this.ctrFechas.fechaFinal.ToString("MM-dd-yyyy HH:mm:ss"),
                        sGrupo = "",
                        sClasificacion = "",
                        sCategoria = "",
                        bGeocerca = true,
                        iTiempoParada = 5,
                        iNumPagina = 1
                    };
                    string s = JsonConvert.SerializeObject(parametros, Formatting.Indented);

                    byte[] bytes = Encoding.UTF8.GetBytes(s);
                    WebRequest request = WebRequest.Create("http://swservicios.mastrack.com.mx/paradas/paradas");
                    request.Method = "POST";
                    request.ContentType = "application/json;";
                    request.ContentLength = bytes.Length;
                    request.GetRequestStream().Write(bytes, 0, bytes.Length);
                    HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                    JObject obj2 = JObject.Parse(new StreamReader(response.GetResponseStream()).ReadToEnd());
                    Paradas paradas = new Paradas();
                    JToken token = obj2.SelectToken("Paradas");
                    JToken token2 = token.SelectToken("Respuesta");
                    Respuesta respuesta = new Respuesta
                    {
                        code = token2.Value<int>("code"),
                        estatus = token2.Value<string>("estatus"),
                        pag = token2.Value<int>("pag"),
                        totPag = token2.Value<int>("totPag")
                    };
                    paradas.Respuesta = respuesta;
                    if (paradas.Respuesta.estatus == "OK")
                    {
                        List<Equipos> list = new List<Equipos>();
                        JArray array = (JArray)token.SelectToken("Equipos");
                        foreach (JToken token3 in array)
                        {
                            Equipos item = new Equipos
                            {
                                alias = (string)token3["alias"],
                                serie = (string)token3["serie"],
                                paradas = new List<ParadaEquipo>()
                            };
                            JArray array2 = (JArray)token3.SelectToken("paradas");
                            foreach (JToken token4 in array2)
                            {
                                ParadaEquipo equipo = new ParadaEquipo
                                {
                                    alias = item.alias,
                                    aliasSerie = $"{item.alias} - {item.serie}",
                                    estatus = (string)token4["estatus"],
                                    coordenadas = (string)token4["coordenadas"],
                                    fechaFinal = (DateTime)token4["fechaFinal"],
                                    fechaInicial = (DateTime)token4["fechaInicial"],
                                    geocerca = (string)token4["geocerca"]
                                };
                                OperationResult result = new CartaPorteSvc().getViajeByEventoCSI(equipo.fechaFinal, equipo.alias);
                                if (result.typeResult == ResultTypes.success)
                                {
                                    equipo.folioViaje = new int?((int)result.result);
                                }
                                item.paradas.Add(equipo);
                            }
                            list.Add(item);
                        }
                        paradas.Equipos = list;
                        this.llenarLista(paradas);
                    }
                    else
                    {
                        OperationResult resp = new OperationResult
                        {
                            mensaje = paradas.Respuesta.estatus,
                            valor = 2
                        };
                        ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void btnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                List<ParadaEquipo> listaParadas = this.lvlParadas.ItemsSource.Cast<ParadaEquipo>().ToList<ParadaEquipo>();
                new ReportView().generarReporteParadas(listaParadas);
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void cbx_ItemSelectionChanged(object sender, ItemSelectionChangedEventArgs e)
        {
            if (this.siFiltrar)
            {
                this.filtrar();
            }
        }

        private void chc_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckBox box;
                base.Cursor = Cursors.Wait;
                if (sender != null)
                {
                    box = sender as CheckBox;
                    switch (box.Name)
                    {
                        case "chcTodosTractores":
                            if (box.IsChecked.Value)
                            {
                                this.cbxTractores.IsEnabled = true;
                                foreach (object obj2 in this.cbxTractores.ItemsSource)
                                {
                                    this.cbxTractores.SelectedItems.Add(obj2);
                                }
                                this.cbxTractores.IsEnabled = false;
                            }
                            else
                            {
                                this.cbxTractores.IsEnabled = true;
                                this.cbxTractores.SelectedItems.Clear();
                            }
                            break;

                        case "chcTodosGeoCercas":
                            if (box.IsChecked.Value)
                            {
                                this.cbxGeoCercas.IsEnabled = true;
                                foreach (object obj3 in this.cbxGeoCercas.ItemsSource)
                                {
                                    this.cbxGeoCercas.SelectedItems.Add(obj3);
                                }
                                this.cbxGeoCercas.IsEnabled = false;
                            }
                            else
                            {
                                this.cbxGeoCercas.IsEnabled = true;
                                this.cbxGeoCercas.SelectedItems.Clear();
                            }
                            break;

                        case "chcTodosEstatus":
                            goto Label_01B2;
                    }
                }
                return;
            Label_01B2:
                if (box.IsChecked.Value)
                {
                    this.cbxEstatus.IsEnabled = true;
                    foreach (object obj4 in this.cbxEstatus.ItemsSource)
                    {
                        this.cbxEstatus.SelectedItems.Add(obj4);
                    }
                    this.cbxEstatus.IsEnabled = false;
                    this.cbxEstatus.IsEnabled = false;
                    this.chcTodosGeoCercas.IsEnabled = true;
                    this.cbxGeoCercas.IsEnabled = true;
                }
                else
                {
                    this.chcTodosGeoCercas.IsEnabled = false;
                    this.chcTodosGeoCercas.IsChecked = false;
                    this.cbxEstatus.IsEnabled = true;
                    this.cbxEstatus.IsEnabled = true;
                    this.cbxEstatus.SelectedItems.Clear();
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void filtrar()
        {
            try
            {
                this.lvlParadas.ItemsSource = null;
                if (this.listaParadas.Count != 0)
                {
                    base.Cursor = Cursors.Wait;
                    List<string> listaEstatus = (from s in this.cbxEstatus.SelectedItems.Cast<EstatusParadas>().ToList<EstatusParadas>() select s.estatus).ToList<string>();
                    if (listaEstatus.Count == 1)
                    {
                        if (listaEstatus[0] == "PNA")
                        {
                            this.cbxGeoCercas.IsEnabled = false;
                            this.chcTodosGeoCercas.IsEnabled = false;
                            this.cbxGeoCercas.SelectedItems.Clear();
                        }
                        else
                        {
                            this.cbxGeoCercas.IsEnabled = true;
                            this.chcTodosGeoCercas.IsEnabled = true;
                        }
                    }
                    else
                    {
                        this.cbxGeoCercas.IsEnabled = true;
                        this.chcTodosGeoCercas.IsEnabled = true;
                    }
                    List<ParadaEquipo> list = new List<ParadaEquipo>();
                    List<string> listaUnidades = (from s in this.cbxTractores.SelectedItems.Cast<UnidadTransporte>().ToList<UnidadTransporte>() select s.clave).ToList<string>();
                    List<string> listaGeoCercas = (from s in this.cbxGeoCercas.SelectedItems.Cast<GeoCerca>().ToList<GeoCerca>() select s.nombre).ToList<string>();
                    if (((listaUnidades.Count > 0) && (listaGeoCercas.Count > 0)) && (listaEstatus.Count > 0))
                    {
                        list = (from x in this.listaParadas
                                where ((listaUnidades.Contains(x.alias) && listaGeoCercas.Contains(x.geocerca)) && ((x.duracion.TotalSeconds >= this.timeInicio.TotalSeconds) && (x.duracion.TotalSeconds <= this.timeFin.TotalSeconds))) && listaEstatus.Contains(x.estatus)
                                select x).ToList<ParadaEquipo>();
                    }
                    else if ((listaUnidades.Count > 0) && (listaEstatus.Count > 0))
                    {
                        list = (from x in this.listaParadas
                                where ((listaUnidades.Contains(x.alias) && (x.duracion.TotalSeconds >= this.timeInicio.TotalSeconds)) && (x.duracion.TotalSeconds <= this.timeFin.TotalSeconds)) && listaEstatus.Contains(x.estatus)
                                select x).ToList<ParadaEquipo>();
                    }
                    else if (listaUnidades.Count > 0)
                    {
                        list = (from x in this.listaParadas
                                where (listaUnidades.Contains(x.alias) && (x.duracion.TotalSeconds >= this.timeInicio.TotalSeconds)) && (x.duracion.TotalSeconds <= this.timeFin.TotalSeconds)
                                select x).ToList<ParadaEquipo>();
                    }
                    else if ((listaGeoCercas.Count > 0) && (listaEstatus.Count > 0))
                    {
                        list = (from x in this.listaParadas
                                where (listaGeoCercas.Contains(x.geocerca) && (x.duracion.TotalSeconds >= this.timeInicio.TotalSeconds)) && (x.duracion.TotalSeconds <= this.timeFin.TotalSeconds)
                                select x).ToList<ParadaEquipo>();
                    }
                    else if (listaGeoCercas.Count > 0)
                    {
                        list = (from x in this.listaParadas
                                where ((listaGeoCercas.Contains(x.geocerca) && (x.duracion.TotalSeconds >= this.timeInicio.TotalSeconds)) && (x.duracion.TotalSeconds <= this.timeFin.TotalSeconds)) && listaEstatus.Contains(x.estatus)
                                select x).ToList<ParadaEquipo>();
                    }
                    else if (listaEstatus.Count > 0)
                    {
                        list = (from x in this.listaParadas
                                where listaEstatus.Contains(x.estatus)
                                select x).ToList<ParadaEquipo>();
                    }
                    else
                    {
                        list = (from x in this.listaParadas
                                where (x.duracion.TotalSeconds >= this.timeInicio.TotalSeconds) && (x.duracion.TotalSeconds <= this.timeFin.TotalSeconds)
                                select x).ToList<ParadaEquipo>();
                    }
                    this.lvlParadas.ItemsSource = list;
                    CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlParadas.ItemsSource);
                    PropertyGroupDescription item = new PropertyGroupDescription("aliasSerie");
                    defaultView.GroupDescriptions.Add(item);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        //private UsuarioCSI usuarioCSI = null;
        //private List<ParadaEquipo> listaParadas = new List<ParadaEquipo>();
        //private bool siFiltrar = false;
        private TimeSpan timeInicio = new TimeSpan(0, 0, 0);
        private TimeSpan timeFin = new TimeSpan(0, 0, 0);
        public override OperationResult guardar()
        {
            OperationResult result2;
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult result = new ParadasEquiposSvc().saveParadasEquipo(ref this.listaParadas, this.usuarioCSI);
                base.mainWindow.habilitar = true;
                result2 = result;
            }
            catch (Exception exception)
            {
                result2 = new OperationResult(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return result2;
        }
        private void llenarLista(Paradas paradas)
        {
            try
            {
                this.siFiltrar = false;
                this.chcTodosTractores.IsChecked = false;
                this.chcTodosGeoCercas.IsChecked = false;
                this.lvlParadas.ItemsSource = null;
                this.listaParadas = new List<ParadaEquipo>();
                foreach (Equipos equipos in paradas.Equipos)
                {
                    foreach (ParadaEquipo equipo in equipos.paradas)
                    {
                        this.listaParadas.Add(equipo);
                    }
                }
                this.lvlParadas.ItemsSource = this.listaParadas;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlParadas.ItemsSource);
                PropertyGroupDescription item = new PropertyGroupDescription("aliasSerie");
                defaultView.GroupDescriptions.Add(item);
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                this.siFiltrar = true;
                base.Cursor = Cursors.Arrow;
            }
        }
        private void llenarLista(PNA pna)
        {
            try
            {
                this.siFiltrar = false;
                this.chcTodosTractores.IsChecked = false;
                this.chcTodosGeoCercas.IsChecked = false;
                this.lvlParadas.ItemsSource = null;
                this.listaParadas = new List<ParadaEquipo>();
                int id = 0;
                foreach (Vehiculos vehiculo in pna.Vehiculos.OrderBy(s => s.Alias).ToList())
                {
                    foreach (Detalles detalle in vehiculo.Detalles)
                    {
                        id++;
                        ParadaEquipo paradas = new ParadaEquipo
                        {
                            idParadaEquipo = id,
                            alias = vehiculo.Alias,
                            aliasSerie = vehiculo.Alias,
                            coordenadas = string.Format("{0},{1}", detalle.Latitud.ToString(), detalle.Longitud.ToString()),
                            coordenadasFinales = string.Format("{0},{1}", detalle.Latitud.ToString(), detalle.Longitud.ToString()),
                            estatus = "PNA",
                            fechaInicial = detalle.FechaInicial,
                            fechaFinal = detalle.FechaFinal,
                            geocerca = detalle.GeoCerca,
                            CategoriaGeocerca = detalle.CategoriaGeocerca.ToUpper(),
                            Estado = detalle.Estado.ToUpper()
                        };
                        this.listaParadas.Add(paradas);
                    }
                }

                //var resp = new CartaPorteSvc().getViajeByEventoCSI(ref listaParadas);
                //if (resp.typeResult != ResultTypes.success)
                //{
                //    ImprimirMensaje.imprimir(resp);
                //}

                this.lvlParadas.ItemsSource = this.listaParadas;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlParadas.ItemsSource);
                PropertyGroupDescription item = new PropertyGroupDescription("aliasSerie");
                defaultView.GroupDescriptions.Add(item);
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                this.siFiltrar = true;
                base.Cursor = Cursors.Arrow;
            }
        }

        private void lvlParadas_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.lvlParadas.SelectedItem != null)
            {
                bool? nullable = new GeoLocalizacionCiltra(this.lvlParadas.SelectedItem as ParadaEquipo).ShowDialog();
            }
        }
        private void txt_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender != null)
            {
                MaskedTextBox box = sender as MaskedTextBox;
                switch (box.Name)
                {
                    case "txtTiempoMinimo":
                        {
                            char[] separator = new char[] { ':' };
                            string[] strArray = this.txtTiempoMinimo.Text.Split(separator);
                            this.timeInicio = new TimeSpan((strArray[0] == "__") ? 0 : Convert.ToInt32(strArray[0].Trim(new char[] { '_' })), (strArray[1] == "__") ? 0 : Convert.ToInt32(strArray[1].Trim(new char[] { '_' })), (strArray[2] == "__") ? 0 : Convert.ToInt32(strArray[2].Trim(new char[] { '_' })));
                            break;
                        }
                    case "txtTiempoMaximo":
                        {
                            char[] separator = new char[] { ':' };
                            string[] strArray2 = this.txtTiempoMaximo.Text.Split(separator);
                            this.timeFin = new TimeSpan((strArray2[0] == "__") ? 0 : Convert.ToInt32(strArray2[0].Trim(new char[] { '_' })), (strArray2[1] == "__") ? 0 : Convert.ToInt32(strArray2[1].Trim(new char[] { '_' })), (strArray2[2] == "__") ? 0 : Convert.ToInt32(strArray2[2].Trim(new char[] { '_' })));
                            break;
                        }
                }
            }
            if (this.siFiltrar)
            {
                this.filtrar();
            }
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar);
                base.Cursor = Cursors.Wait;
                OperationResult resp = new UsuarioCSISvc().getAllUsuariosCSI();
                if (resp.typeResult == ResultTypes.success)
                {
                    this.cbxCuenta.ItemsSource = resp.result as List<UsuarioCSI>;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    base.IsEnabled = false;
                    return;
                }
                OperationResult result2 = new UnidadTransporteSvc().getTractores(1, "%");
                if (result2.typeResult == ResultTypes.success)
                {
                    this.cbxTractores.ItemsSource = result2.result as List<UnidadTransporte>;
                }
                else
                {
                    ImprimirMensaje.imprimir(result2);
                    base.IsEnabled = false;
                    return;
                }
                OperationResult result3 = new GeoCercaSvc().getGeoCerca();
                if (result3.typeResult == ResultTypes.success)
                {
                    this.cbxGeoCercas.ItemsSource = result3.result as List<GeoCerca>;
                }
                else
                {
                    ImprimirMensaje.imprimir(result3);
                    base.IsEnabled = false;
                    return;
                }
                List<EstatusParadas> list = new List<EstatusParadas>();
                EstatusParadas item = new EstatusParadas
                {
                    estatus = "TE"
                };
                list.Add(item);
                EstatusParadas paradas2 = new EstatusParadas
                {
                    estatus = "PNA"
                };
                list.Add(paradas2);
                this.cbxEstatus.ItemsSource = list;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        public class EstatusParadas
        {
            public string estatus { get; set; }
        }

        public class Parametros
        {
            public string sUsuario { get; set; }
            public string sPassword { get; set; }
            public string sSerie { get; set; }
            public string dtFechaInicial { get; set; }
            public string dtFechaFinal { get; set; }
            public string sGrupo { get; set; }
            public string sClasificacion { get; set; }
            public string sCategoria { get; set; }
            public bool bGeocerca { get; set; }
            public int iTiempoParada { get; set; }
            public int iNumPagina { get; set; }
        }
        public class ParametrosNew
        {
            public string sUsuario { get; set; }
            public string sContrasenia { get; set; }
            public string bResponseFormat => "json";
            public string sValorUnidad =>  "tractor";//string.Empty;
            public string sTipoConsultaUnidad => "categoria";// "todos";
            public string sCategoriaEvaluar => "parada autorizada;poblado;punto de interes;zona de robo;ZONA DE VIGILANCIA;zona sin cobertura";
                //"zona de robo;parada autorizada;punto de interes;poblado;punto de interés;zona sin cobertura";
            public string sCategoriaOrigen => "planta";
            public string sCategoriaDestino => "granja";
            public string sCategoriaDescartar => "basculas;base;camino;carga/origen;caseta;caseta-peaje;corralon;curva peligrosa;descarga/destino;domicilio operador;entrada a granja;fitosanitaria;gasolinera externa;lavadero;maniobras;punto de descanso;resguardo de unidad;taller externo";
                
                //"ruta"; // "caminoss;entrada;camino;entrada a granja;base;maniobras;basculas;taller externo;puntos de quiebre;caseta;caseta-peaje;corralón;curva peligrosa;desinfección;fitosanitaria;lavadero;lavadero externo";
            //public string sCategoriaPuntoInteres => string.Empty;//  "punto de interes;poblado;punto de interés";
            //public string sCategoriaZonaRobo => string.Empty; // "zona de robo";
            public int iDuracionMinimaPNA => 1;
            public string sFechaInicial { get; set; }
            public string sFechaFinal { get; set; }
            public string sHoraInicial { get; set; }
            public string sHoraFinal { get; set; }
        }
    }
}