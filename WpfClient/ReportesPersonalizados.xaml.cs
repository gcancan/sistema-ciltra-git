﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Excel = Microsoft.Office.Interop.Excel;
//using System.Windows;
using ap = System.Windows.Forms;
using Core.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for ReportesPersonalizados.xaml
    /// </summary>
    public partial class ReportesPersonalizados : Window
    {
        List<Viaje> listCartaPorte = new List<Viaje>();
        Usuario usuario = new Usuario();
        private DateTime fechaInicio;
        private DateTime fechaFin;
        private Cliente cliente;
        private bool mostrarImportes = false;
        public ReportesPersonalizados()
        {
            InitializeComponent();
        }

        public ReportesPersonalizados(List<Viaje> listCartaPorte, Usuario usuario)
        {
            InitializeComponent();
            this.listCartaPorte = listCartaPorte;
            this.usuario = usuario;
        }

        public ReportesPersonalizados(List<Viaje> listCartaPorte, Cliente cliente, Usuario usuario, DateTime selectedDate1, DateTime selectedDate2, bool mostrarImportes = false) : this(listCartaPorte, usuario)
        {
            InitializeComponent();
            this.listCartaPorte = listCartaPorte;
            this.usuario = usuario;
            this.fechaInicio = selectedDate1;
            this.fechaFin = selectedDate2;
            this.cliente = cliente;
            this.mostrarImportes = mostrarImportes;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            OperationResult resp = new PerfilReporteSvc().getPerfilesReportes(usuario.idUsuario, enumPerfilRporte.COBRANZA);
            if (resp.typeResult == ResultTypes.error)
            {
                MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (resp.typeResult == ResultTypes.warning)
            {
                MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (resp.typeResult == ResultTypes.recordNotFound)
            {
                //MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (resp.typeResult == ResultTypes.success)
            {
                //MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                mapComboPerfiles((List<PerfilReporte>)resp.result);
            }

            chxPrecio.Visibility = mostrarImportes ? Visibility.Visible : Visibility.Collapsed;
            chxImporte.Visibility = mostrarImportes ? Visibility.Visible : Visibility.Collapsed;
            chxIvas.Visibility = mostrarImportes ? Visibility.Visible : Visibility.Collapsed;
            chxRetencion.Visibility = mostrarImportes ? Visibility.Visible : Visibility.Collapsed;
            chxTotal.Visibility = mostrarImportes ? Visibility.Visible : Visibility.Collapsed;
        }

        private void mapComboPerfiles(List<PerfilReporte> list)
        {
            foreach (PerfilReporte item in list)
            {
                cbxPerfiles.Items.Add(item);
            }
        }

        public void generar()
        {
            try
            {
                Cursor = Cursors.Wait;                

                if (generarReportePersonalizado())
                {
                    MessageBox.Show("Se creo correctamente el reporte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Ocurrio un error al intentar crear el reporte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        public bool generarReportePersonalizado()
        {
            try
            {
                ReportView reporte = new ReportView();
                bool resp = reporte.documentoReporte(listCartaPorte,
                    chxFecha.IsChecked.Value, chxHora.IsChecked.Value, chxOrden.IsChecked.Value, chxOperador.IsChecked.Value, chxTC.IsChecked.Value,
                    chxTolva1.IsChecked.Value, chxTolvar2.IsChecked.Value, chxDestino.IsChecked.Value, chxKM.IsChecked.Value, chxProducto.IsChecked.Value, chxToneladas.IsChecked.Value,
                    chxPrecio.IsChecked.Value, chxImporte.IsChecked.Value, chxIvas.IsChecked.Value, chxRetencion.IsChecked.Value, chxTotal.IsChecked.Value, this.usuario, fechaInicio, fechaFin, cliente);
                if (resp)
                {
                    //reporte.Show();
                    //reporte.Owner = this;
                    return resp;
                }
                return false;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }

        private void Generar_Click(object sender, RoutedEventArgs e)
        {
            generar();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNamePerfil.Text.Trim()))
            {
                PerfilReporte perfilReporte = new PerfilReporte();
                perfilReporte.nombrePerfil = txtNamePerfil.Text.Trim();
                perfilReporte.usuraio = usuario;
                perfilReporte.tipoReporte = enumPerfilRporte.COBRANZA;
                perfilReporte.listDetalles = mapListaDetalles();

                if (perfilReporte.listDetalles.Count == 0)
                {
                    MessageBox.Show("Se requiere al menos una opción", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    OperationResult resp = new PerfilReporteSvc().savePerfilReporte(ref perfilReporte);
                    if (resp.typeResult == ResultTypes.error)
                    {
                        MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else if (resp.typeResult == ResultTypes.warning)
                    {
                        MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else if (resp.typeResult == ResultTypes.success)
                    {
                        MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                        cbxPerfiles.Items.Add((PerfilReporte)resp.result);
                    }
                }
            }
            else
            {
                MessageBox.Show("Se requiere del nombre del perfil", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private List<DetallesPerfilReporte> mapListaDetalles()
        {
            List<DetallesPerfilReporte> lista = new List<DetallesPerfilReporte>();

            if (chxFecha.IsChecked.Value)
            {
                lista.Add(new DetallesPerfilReporte
                {
                    nombreDetalle = "FECHA"
                });
            }

            if (chxHora.IsChecked.Value)
            {
                lista.Add(new DetallesPerfilReporte
                {
                    nombreDetalle = "HORA"
                });
            }

            if (chxOrden.IsChecked.Value)
            {
                lista.Add(new DetallesPerfilReporte
                {
                    nombreDetalle = "ORDEN"
                });
            }

            if (chxTC.IsChecked.Value)
            {
                lista.Add(new DetallesPerfilReporte
                {
                    nombreDetalle = "TC"
                });
            }

            if (chxTolva1.IsChecked.Value)
            {
                lista.Add(new DetallesPerfilReporte
                {
                    nombreDetalle = "TOLVA1"
                });
            }

            if (chxTolvar2.IsChecked.Value)
            {
                lista.Add(new DetallesPerfilReporte
                {
                    nombreDetalle = "TOLVA2"
                });
            }

            if (chxOperador.IsChecked.Value)
            {
                lista.Add(new DetallesPerfilReporte
                {
                    nombreDetalle = "OPERADOR"
                });
            }

            if (chxDestino.IsChecked.Value)
            {
                lista.Add(new DetallesPerfilReporte
                {
                    nombreDetalle = "DESTINO"
                });
            }

            if (chxKM.IsChecked.Value)
            {
                lista.Add(new DetallesPerfilReporte
                {
                    nombreDetalle = "KM"
                });
            }
            if (chxProducto.IsChecked.Value)
            {
                lista.Add(new DetallesPerfilReporte
                {
                    nombreDetalle = "PRODUCTO"
                });
            }

            if (chxToneladas.IsChecked.Value)
            {
                lista.Add(new DetallesPerfilReporte
                {
                    nombreDetalle = "TONELADAS"
                });
            }

            if (chxPrecio.IsChecked.Value)
            {
                lista.Add(new DetallesPerfilReporte
                {
                    nombreDetalle = "PRECIO"
                });
            }

            if (chxImporte.IsChecked.Value)
            {
                lista.Add(new DetallesPerfilReporte
                {
                    nombreDetalle = "IMPORTE"
                });
            }

            if (chxIvas.IsChecked.Value)
            {
                lista.Add(new DetallesPerfilReporte
                {
                    nombreDetalle = "IVA"
                });
            }

            if (chxRetencion.IsChecked.Value)
            {
                lista.Add(new DetallesPerfilReporte
                {
                    nombreDetalle = "RETENCION"
                });
            }

            if (chxTotal.IsChecked.Value)
            {
                lista.Add(new DetallesPerfilReporte
                {
                    nombreDetalle = "TOTAL"
                });
            }
            return lista;
        }

        private void cbxPerfiles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxPerfiles.SelectedItem != null)
            {
                PerfilReporte perfilReporte = (PerfilReporte)cbxPerfiles.SelectedItem;
                mapPantalla(perfilReporte);
            }
        }

        private void mapPantalla(PerfilReporte perfilReporte)
        {

            chxFecha.IsChecked = false;
            chxHora.IsChecked = false;
            chxOrden.IsChecked = false;
            chxTC.IsChecked = false;
            chxTolva1.IsChecked = false;
            chxTolvar2.IsChecked = false;
            chxOperador.IsChecked = false;
            chxDestino.IsChecked = false;
            chxKM.IsChecked = false;
            chxProducto.IsChecked = false;
            chxToneladas.IsChecked = false;
            chxPrecio.IsChecked = false;
            chxImporte.IsChecked = false;
            chxIvas.IsChecked = false;
            chxRetencion.IsChecked = false;
            chxTotal.IsChecked = false;

            foreach (var item in perfilReporte.listDetalles)
            {
                switch (item.nombreDetalle)
                {
                    case "FECHA":
                        chxFecha.IsChecked = true;
                        continue;
                    case "HORA":
                        chxHora.IsChecked = true;
                        continue;
                    case "ORDEN":
                        chxOrden.IsChecked = true;
                        continue;
                    case "TC":
                        chxTC.IsChecked = true;
                        continue;
                    case "TOLVA1":
                        chxTolva1.IsChecked = true;
                        continue;
                    case "TOLVA2":
                        chxTolvar2.IsChecked = true;
                        continue;
                    case "OPERADOR":
                        chxOperador.IsChecked = true;
                        continue;
                    case "DESTINO":
                        chxDestino.IsChecked = true;
                        continue;
                    case "KM":
                        chxKM.IsChecked = true;
                        continue;
                    case "PRODUCTO":
                        chxProducto.IsChecked = true;
                        continue;
                    case "TONELADAS":
                        chxToneladas.IsChecked = true;
                        continue;
                    case "PRECIO":
                        chxPrecio.IsChecked = true;
                        continue;
                    case "IMPORTE":
                        chxImporte.IsChecked = true;
                        continue;
                    case "IVA":
                        chxIvas.IsChecked = true;
                        continue;
                    case "RETENCION":
                        chxRetencion.IsChecked = true;
                        continue;
                    case "TOTAL":
                        chxTotal.IsChecked = true;
                        break;                   
                }
            }
        }

        
    }
}
