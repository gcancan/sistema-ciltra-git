﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for selectZonas.xaml
    /// </summary>
    public partial class selectPrivilegios : Window
    {
        public string parametro = "";
        public selectPrivilegios()
        {
            InitializeComponent();
        }
        List<Privilegio> listaPrivilegiosUsuario = new List<Privilegio>();
        public selectPrivilegios(List<Privilegio> listaPrivilegiosUsuario)
        {
            InitializeComponent();
            this.listaPrivilegiosUsuario = listaPrivilegiosUsuario;
        }
        public selectPrivilegios(string parametro)
        {
            InitializeComponent();
            this.parametro = parametro;
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            //txtFind.Text = parametro;
            findAllPrivilegios();
            buscarTodosMenus();
            buscarMenusAplicacion();
        }
        List<MenusAplicacion> menus = new List<MenusAplicacion>();
        private void buscarTodosMenus()
        {
            try
            {
                var res = new MenusAplicacionSvc().getAllMenus();
                if (res.typeResult == ResultTypes.success)
                {
                    menus = res.result as List<MenusAplicacion>;
                    foreach (MenusAplicacion ma in menus)
                    {
                        if (ma.padre == null)
                        {
                            MenuItem mi = new MenuItem();
                            StackPanel stp = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0) };
                            CheckBox chc = new CheckBox { Content = ma.nombre, IsChecked = false, Tag = ma, FontSize = 10 };
                            chc.Checked += Chc_Checked;
                            chc.Unchecked += Chc_Unchecked;
                            chc.IsChecked = listaPrivilegiosUsuario.Exists(s => s.clave == ma.privilegio.clave);
                            stp.Children.Add(chc);
                            stp.Children.Add(new Label { Content = "       " });
                            mi.Header = stp;
                            mi.Name = ma.clave;
                            mi.Tag = ma;
                            generateMenuRaiz(mi, menus);
                            mainMenu.Items.Add(mi);

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void Chc_Unchecked(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                CheckBox chc = sender as CheckBox;
                Privilegio privilegio = ((MenusAplicacion)chc.Tag).privilegio;
                listaPrivilegios.Remove(privilegio);
            }
        }

        private void Chc_Checked(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                CheckBox chc = sender as CheckBox;
                Privilegio privilegio = ((MenusAplicacion)chc.Tag).privilegio;
                listaPrivilegios.Add(privilegio);
            }
        }

        private void generateMenuRaiz(MenuItem raiz, List<MenusAplicacion> list)
        {
            foreach (MenusAplicacion ma in list)
            {
                if (ma.idPadre == ((MenusAplicacion)raiz.Tag).idMenuAplicacion)
                {
                    MenuItem mi = new MenuItem();
                    mi.Name = ma.clave;
                    CheckBox chc = new CheckBox { Content = ma.nombre, IsChecked = false, Tag = ma, FontSize = 10 };
                    chc.Checked += Chc_Checked;
                    chc.Unchecked += Chc_Unchecked;
                    chc.IsChecked = listaPrivilegiosUsuario.Exists(s => s.clave == ma.privilegio.clave);
                    mi.Header = chc;
                    mi.Tag = ma;

                    generateMenuRaiz(mi, list);
                    //if (mi.Items.Count == 0)
                    raiz.Items.Add(mi);
                }
            }
        }

        public List<Privilegio> findPrivilegios()
        {

            bool? dResult = ShowDialog();
            //if (dResult.Value && lvlPrivilegio != null && lvlPrivilegio.SelectedItems.Count > 0)
            if (dResult.Value && listaPrivilegios.Count > 0)
            {
                return listaPrivilegios;
                //List<Privilegio> listP = new List<Privilegio>();
                //foreach (ListViewItem item in lvlPrivilegio.SelectedItems)
                //{
                //    listP.Add((Privilegio)item.Content);
                //}
                //return listP;
            }
            else
            {
                return null;
            }
        }

        public void buscarMenusAplicacion()
        {
            //var resp = new MenusAplicacionSvc().getAllMenus();
            //if (resp.typeResult == ResultTypes.success)
            //{
            //    mapTapControl((List<MenusAplicacion>)resp.result);
            //}
        }

        //private void mapTapControl(List<MenusAplicacion> result)
        //{
        //    foreach (var item in result)
        //    {
        //        if (item.idPadre == null)
        //        {
        //            TabItem tabItem = new TabItem();
        //            tabItem.Tag = item;
        //            tabItem.Header = item.nombre;
        //            ScrollViewer scroll = new ScrollViewer() { VerticalScrollBarVisibility = ScrollBarVisibility.Auto };
        //            StackPanel stp = new StackPanel { Orientation = Orientation.Vertical };
        //            CheckBox chc = new CheckBox { Tag = item, Content = new Label { Content = item.nombre, FontSize = 16 }, Margin = new Thickness(1), FontWeight = FontWeights.Bold };
        //            stp.Children.Add(chc);
        //            scroll.Content = stp;

        //            tabItem.Content = scroll;
        //            tapMenus.Items.Add(tabItem);
        //        }
        //    }
        //    foreach (var item in result)
        //    {
        //        if (item.idPadre != null)
        //        {
        //            añadirSubMenu(item);
        //        }
        //    }
        //}

        //private void añadirSubMenu(MenusAplicacion subMenu)
        //{
        //    var listaMenus = tapMenus.Items;
        //    foreach (TabItem item in listaMenus)
        //    {
        //        MenusAplicacion menu = (MenusAplicacion)item.Tag;
        //        if (menu.idMenuAplicacion == subMenu.idPadre)
        //        {
        //            CheckBox chc = new CheckBox { Tag = subMenu, Content = new Label { Content = subMenu.nombre, FontSize = 16 }, Margin = new Thickness(6) };
        //            ScrollViewer scroll = (ScrollViewer)item.Content;
        //            StackPanel stp = (StackPanel)scroll.Content;
        //            stp.Children.Add(chc);
        //            return;
        //        }
        //    }
        //}

        private void mapList(List<Privilegio> list)
        {
            foreach (var item in list)
            {
                CheckBox chc = new CheckBox
                {
                    Tag = item,
                    Content = new Label
                    {
                        Content = item.descripcion.ToUpper(),
                        FontSize = 14
                    },
                    Margin = new Thickness(2),
                    IsChecked = listaPrivilegiosUsuario.Exists(s => s.clave == item.clave)
                };
                stpEspeciales.Children.Add(chc);
            }
            return;
            //lvlPrivilegio.Items.Clear();
            //List<ListViewItem> lvlList = new List<ListViewItem>();
            //foreach (Privilegio item in list)
            //{
            //    ListViewItem lvl = new ListViewItem();
            //    lvl.Content = item;
            //    lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
            //    lvlList.Add(lvl);
            //    //lvlUnidades.Items.Add(lvl);
            //}
            //lvlPrivilegio.ItemsSource = lvlList;
            //CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlPrivilegio.ItemsSource);
            //view.Filter = UserFilter;
        }

        private bool UserFilter(object item)
        {
            //if (String.IsNullOrEmpty(txtFind.Text))
            //    return true;
            //else
            //    //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
            //    return ((Privilegio)(item as ListViewItem).Content).clave.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
            //            ((Privilegio)(item as ListViewItem).Content).descripcion.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
            //            ((Privilegio)(item as ListViewItem).Content).idPrivilegio.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;
            return false;
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            //CollectionViewSource.GetDefaultView(lvlPrivilegio.ItemsSource).Refresh();
        }

        private void findAllPrivilegios()
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult resp = new PrivilegioSvc().getAllPrivilegiosEspeiales();
                if (resp.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.success)
                {
                    //lvlList = ((List<Personal>)resp.result);
                    mapList(((List<Privilegio>)resp.result));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format(ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                controles.IsEnabled = false;
            }
            finally { Cursor = Cursors.Arrow; }

        }
        List<Privilegio> listaPrivilegios = new List<Privilegio>();

        private void leerElementosMenu(List<MenuItem> listaMenus)
        {
            foreach (MenuItem item in listaMenus)
            {
                CheckBox chc = (CheckBox)item.Header;
                if (chc.IsChecked.Value)
                {
                    Privilegio privilegio = ((MenusAplicacion)chc.Tag).privilegio;
                    listaPrivilegios.Add(privilegio);
                }
                if (item.Items.Count > 0)
                {
                    List<MenuItem> listaMenusNew = item.Items.Cast<MenuItem>().ToList();
                    leerElementosMenu(listaMenusNew);
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //listaPrivilegios = new List<Privilegio>();

            //List<MenuItem> listaMenus =mainMenu.Items.Cast<MenuItem>().ToList();
            //leerElementosMenu(listaMenus);
            //var s = listaPrivilegios;

            //var listaMenus = tapMenus.Items;
            //foreach (TabItem item in listaMenus)
            //{

            //    ScrollViewer scroll = (ScrollViewer)item.Content;
            //    StackPanel stp = (StackPanel)scroll.Content;
            //    List<CheckBox> listaChec = stp.Children.Cast<CheckBox>().ToList();
            //    foreach (CheckBox chc in listaChec)
            //    {
            //        if (chc.IsChecked.Value)
            //        {
            //            MenusAplicacion menu = (MenusAplicacion)chc.Tag;
            //            listaPrivilegios.Add(menu.privilegio);
            //        }
            //    }
            //}

            List<CheckBox> listaEspecial = stpEspeciales.Children.Cast<CheckBox>().ToList();
            foreach (CheckBox item in listaEspecial)
            {
                if (item.IsChecked.Value)
                {
                    listaPrivilegios.Add((Privilegio)item.Tag);
                }
            }
            DialogResult = true;
        }
    }
}
