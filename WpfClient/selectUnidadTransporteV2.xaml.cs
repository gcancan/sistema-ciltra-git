﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectUnidadTransporteV2.xaml
    /// </summary>
    public partial class selectUnidadTransporteV2 : Window
    {
        public selectUnidadTransporteV2()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtBuscador.Text = this.parametro;
        }
        string parametro = string.Empty;
        public UnidadTransporte getUnidadAll(string parametro = "")
        {
            try
            {
                this.parametro = parametro;
                buscarTodasLasUnidades();
                bool? result = ShowDialog();
                if (result.Value && lvlUnidades.SelectedItem != null)
                {
                    return (lvlUnidades.SelectedItem as ListViewItem).Content as UnidadTransporte;
                }
                return null;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }
        private void buscarTodasLasUnidades()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new UnidadTransporteSvc().getAllUnidadesTransV2();
                if (resp.typeResult == ResultTypes.success)
                {
                    mapListaUnidades(resp.result as List<UnidadTransporte>);
                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void mapListaUnidades(List<UnidadTransporte> listaUnidades)
        {
            try
            {
                lvlUnidades.ItemsSource = null;
                List<ListViewItem> list = new List<ListViewItem>();
                foreach (UnidadTransporte col in listaUnidades)
                {
                    ListViewItem item = new ListViewItem
                    {
                        Content = col
                    };
                    item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                    list.Add(item);
                }
                this.lvlUnidades.ItemsSource = list;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlUnidades.ItemsSource);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtBuscador.Text) ||
            ((((UnidadTransporte)(item as ListViewItem).Content).clave.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(this.lvlUnidades.ItemsSource).Refresh();
        }
    }
}
