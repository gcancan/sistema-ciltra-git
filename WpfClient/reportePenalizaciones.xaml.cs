﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using Core.Interfaces;
using Microsoft.VisualBasic;
using Microsoft.Win32;
using Core.Utils;
using ap = System.Windows.Forms;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for cerrarViaje.xaml
    /// </summary>
    public partial class reportePenalizaciones : ViewBase
    {
        public reportePenalizaciones()
        {
            InitializeComponent();
        }

        //public Declaraciones SDK;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            //mainWindow.Title = "Reporte de Penalizaciones.";
            OperationResult resp = new ClienteSvc().getClientesALLorById(mainWindow.empresa.clave);
            if (resp.typeResult == ResultTypes.success)
            {
                cbxClientes.ItemsSource = (List<Cliente>)resp.result;                
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
                return;
            }
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
            dtpFechaInicio.SelectedDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            dtpFechaFin.SelectedDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            
        }

        internal void buscarPenalizaciones()
        {
            try
            {
                Cursor = Cursors.Wait;
                if (cbxClientes.SelectedValue == null)
                {
                    ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "Se necesita elegir un Cliente" });
                    return;
                }
                sumaImportes = 0; ;
                recalcularImportes();
                lvlPenalizaciones.Items.Clear();
                OperationResult resp = new PenalizacionSvc().getPenalizacionesByFechas(dtpFechaInicio.SelectedDate.Value, dtpFechaFin.SelectedDate.Value.AddDays(1), ((Cliente)cbxClientes.SelectedItem).clave);

                if (resp.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else if (resp.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else if (resp.typeResult == ResultTypes.success)
                {
                    mapLista((List<Penalizacion>)resp.result);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            } 
            finally
            {
                Cursor = Cursors.Arrow;
            }                        
        }

        private decimal sumaImportes = 0m;
        private void mapLista(List<Penalizacion> result)
        {            
            foreach (Penalizacion item in result)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                sumaImportes += item.importe;
                recalcularImportes();
                lvlPenalizaciones.Items.Add(lvl);
            }
        }

        private void recalcularImportes()
        {
            lblSumaImportes.Content = sumaImportes.ToString("C4");
            decimal retencion = sumaImportes * (((Cliente)cbxClientes.SelectedItem).impuestoFlete.valor / 100);
            lblRetencion.Content = retencion.ToString("C4");
            decimal IVA = sumaImportes * (((Cliente)cbxClientes.SelectedItem).impuesto.valor / 100);
            lblIVA.Content = IVA.ToString("C4");
            lblTOTAL.Content = (sumaImportes + (IVA - retencion)).ToString("C4");
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            buscarPenalizaciones();
        }

        private void btnGenerarReporte_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lvlPenalizaciones.Items.Count <= 0)
                {
                    MessageBox.Show("Se requieren resultados para proceder", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                Cursor = Cursors.Wait;

                ReportView reporte = new ReportView();
                bool resp = reporte.documentoReportePenalizaciones
                    (getListPenalizaciones(),
                    (DateTime)dtpFechaInicio.SelectedDate, 
                    (DateTime)dtpFechaFin.SelectedDate,
                    (Cliente)cbxClientes.SelectedItem);
                if (resp)
                {
                    reporte.Show();
                    MessageBox.Show("Se creo correctamente el reporte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Ocurrio un error al intentar crear el reporte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception)
            {
                
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private List<Penalizacion> getListPenalizaciones()
        {
            List<Penalizacion> list = new List<Penalizacion>();
            foreach (ListViewItem item in lvlPenalizaciones.Items)
            {
                list.Add((Penalizacion)item.Content);
            }
            return list;
        }
    }
}
