﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectLiquidaciones.xaml
    /// </summary>
    public partial class selectLiquidaciones : Window
    {
        public selectLiquidaciones()
        {
            InitializeComponent();
        }
        public Liquidacion obtenerLiquidacion()
        {
            try
            {
                getAllLiquidaciones();
                bool? resul = ShowDialog();
                if (resul.Value && lvlLiquidacion.SelectedItem != null)
                {
                    return (lvlLiquidacion.SelectedItem as ListViewItem).Content as Liquidacion;
                }
                return null;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }
        public Liquidacion obtenerLiquidacion(int idDepartamento)
        {
            try
            {
                getAllLiquidaciones(idDepartamento);
                bool? resul = ShowDialog();
                if (resul.Value && lvlLiquidacion.SelectedItem != null)
                {
                    return (lvlLiquidacion.SelectedItem as ListViewItem).Content as Liquidacion;
                }
                return null;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }
        void getAllLiquidaciones()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new LiquidacionSvc().getAllLiquidaciones();
                if (resp.typeResult == ResultTypes.success)
                {
                    llenarLista(resp.result as List<Liquidacion>);
                }
                else if(resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception  ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        void getAllLiquidaciones(int idDepartamento)
        {
            try
            {
                Cursor = Cursors.Wait;
                
                var resp = new LiquidacionSvc().getAllLiquidacionesByIdDetpto(idDepartamento);
                if (resp.typeResult == ResultTypes.success)
                {
                    llenarLista(resp.result as List<Liquidacion>);
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void llenarLista(List<Liquidacion> listaLiquidacion)
        {
            try
            {
                lvlLiquidacion.ItemsSource = null;
                List<ListViewItem> list = new List<ListViewItem>();
                foreach (Liquidacion liquidacion in listaLiquidacion)
                {
                    ListViewItem item = new ListViewItem
                    {
                        Content = liquidacion
                    };
                    item.ToolTip = liquidacion.strDeptosEnter;
                    item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                    list.Add(item);
                }
                this.lvlLiquidacion.ItemsSource = list;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlLiquidacion.ItemsSource);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtBuscador.Text) ||
            ((((CargaCombustibleExtra)(item as ListViewItem).Content).idCargaCombustibleExtra.ToString().IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (lvlLiquidacion.ItemsSource == null) return;
            CollectionViewSource.GetDefaultView(this.lvlLiquidacion.ItemsSource).Refresh();
        }
    }
}
