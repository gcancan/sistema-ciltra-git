﻿namespace WpfClient
{
    using Core.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Markup;

    public partial class selectOrigenes : Window
    {
        private string parametro;       
        public selectOrigenes()
        {
            this.InitializeComponent();
        }

        public selectOrigenes(string parametro)
        {
            this.InitializeComponent();
            this.parametro = parametro;
        }

        public Origen buscarOrigenes(List<Origen> listOrigen)
        {
            this.mapOrigen(listOrigen);
            if ((base.ShowDialog().Value && (this.lvlOrigenes != null)) && (this.lvlOrigenes.SelectedItem != null))
            {
                return (Origen)((ListViewItem)this.lvlOrigenes.SelectedItem).Content;
            }
            return null;
        }
        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            base.DialogResult = true;
        }

        private void lvlOrigenes_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                base.DialogResult = true;
            }
        }

        private void mapOrigen(List<Origen> listOrigen)
        {
            this.lvlOrigenes.Items.Clear();
            List<ListViewItem> list = new List<ListViewItem>();
            foreach (Origen origen in listOrigen)
            {
                ListViewItem item = new ListViewItem
                {
                    Content = origen
                };
                item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                list.Add(item);
            }
            this.lvlOrigenes.ItemsSource = list;
            CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlOrigenes.ItemsSource);
            defaultView.Filter = new Predicate<object>(this.UserFilter);
        }
        private void txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                this.lvlOrigenes.Focus();
            }
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(this.lvlOrigenes.ItemsSource).Refresh();
        }

        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtFind.Text) ||
            ((((Origen)(item as ListViewItem).Content).nombreOrigen.ToString().IndexOf(this.txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0) || 
            (((Origen)(item as ListViewItem).Content).clasificacion.IndexOf(this.txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                base.DialogResult = false;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.txtFind.Text = this.parametro;
            this.lvlOrigenes.Focus();
        }
    }
}
