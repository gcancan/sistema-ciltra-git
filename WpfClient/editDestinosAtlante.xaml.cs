﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using CoreFletera.Interfaces;
    using CoreFletera.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Markup;

    public partial class editDestinosAtlante : WpfClient.ViewBase
    {        
        public editDestinosAtlante()
        {
            this.InitializeComponent();
        }
        private bool cargarBases()
        {
            bool flag2;
            try
            {
                base.Cursor = Cursors.Arrow;
                OperationResult resp = new BaseSvc().getAllBase();
                if (resp.typeResult == ResultTypes.success)
                {
                    this.cbxBase.ItemsSource = resp.result as List<Base>;
                    return true;
                }
                if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                    return false;
                }
                flag2 = false;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                flag2 = false;
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return flag2;
        }

        private void cargarDestinos()
        {
            try
            {
                OperationResult resp = new ZonasSvc().getDestinosAtlante(this.ctrEmpresa.empresaSelected.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    this.lvlDestinos.ItemsSource = resp.result as List<Zona>;
                    CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlDestinos.ItemsSource);
                    PropertyGroupDescription item = new PropertyGroupDescription("estado.nombre");
                    defaultView.GroupDescriptions.Add(item);
                    defaultView.Filter = new Predicate<object>(this.UserFilter);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.cargarDestinos();
        }

        public override OperationResult guardar()
        {
            OperationResult result3;
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult result = this.validar();
                if (result.typeResult > ResultTypes.success)
                {
                    return result;
                }
                Zona zona = this.mapForm();
                OperationResult result2 = new ZonasSvc().saveDestino_v2(ref zona);
                if (result2.typeResult == ResultTypes.success)
                {
                    if (zona.activo)
                    {
                        this.mapForm(zona);
                    }
                    else
                    {
                        this.mapForm(null);
                    }
                    base.mainWindow.habilitar = true;
                    this.cargarDestinos();
                }
                result3 = result2;
            }
            catch (Exception exception)
            {
                result3 = new OperationResult(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return result3;
        }
        private void lvlDestinos_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
            {
                Zona selectedItem = (sender as ListView).SelectedItem as Zona;
                if (selectedItem != null)
                {
                    this.mapForm(selectedItem);
                }
            }
        }
        private void mapEstados()
        {
            try
            {
                OperationResult resp = new EstadoSvc().getEstadosAllorById(0);
                if (resp.typeResult == ResultTypes.success)
                {
                    foreach (Estado estado in (List<Estado>)resp.result)
                    {
                        this.cbxEstados.Items.Add(estado);
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }
        private Zona mapForm()
        {
            try
            {
                return new Zona
                {
                    clave = (this.txtClave.Tag == null) ? 0 : (this.txtClave.Tag as Zona).clave,
                    clave_empresa = this.ctrEmpresa.empresaSelected.clave,
                    baseDestino = (this.cbxBase.SelectedItem == null) ? null : (this.cbxBase.SelectedItem as Base),
                    descripcion = this.txtDescripcionDestino.Text,
                    estado = this.cbxEstados.SelectedItem as Estado,
                    activo = this.chcActivo.IsChecked.Value,
                    correo = this.txtCorreo.Text,
                    km = this.txtkm.valor
                };
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return null;
            }
        }
        private void mapForm(Zona zona)
        {
            if (zona != null)
            {
                this.txtClave.Tag = zona;
                this.txtClave.valor = zona.clave;
                this.txtDescripcionDestino.Text = zona.descripcion;
                this.txtCorreo.Text = zona.correo;
                this.selectEstado(zona.estado);
                this.txtkm.valor = zona.km;
                this.chcActivo.IsChecked = new bool?(zona.activo);
                if (zona.baseDestino != null)
                {
                    cbxBase.SelectedItem = (cbxBase.ItemsSource as List<Base>).Find(s => s.idBase == zona.baseDestino.idBase);
                }
                else
                {
                    cbxBase.SelectedItem = null;
                }                
            }
            else
            {
                this.txtClave.Tag = null;
                this.txtClave.valor = 0;
                this.txtDescripcionDestino.Clear();
                this.txtCorreo.Clear();
                this.cbxEstados.SelectedItem = null;
                this.txtkm.valor = decimal.Zero;
                this.chcActivo.IsChecked = true;
            }
        }
        public override void nuevo()
        {
            base.nuevo();
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
            this.mapForm(null);
        }
        private void selectEstado(Estado estado)
        {
            int num = 0;
            foreach (Estado estado2 in (IEnumerable)this.cbxEstados.Items)
            {
                if (estado2.idEstado == estado.idEstado)
                {
                    this.cbxEstados.SelectedIndex = num;
                    break;
                }
                num++;
            }
        }
        private void txtFiltro_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.lvlDestinos.ItemsSource != null)
            {
                CollectionViewSource.GetDefaultView(this.lvlDestinos.ItemsSource).Refresh();
            }
        }

        private bool UserFilter(object item) =>
            ((item as Zona).descripcion.IndexOf(this.txtDescripcionDestino.Text, StringComparison.OrdinalIgnoreCase) >= 0);

        private OperationResult validar()
        {
            try
            {
                OperationResult result = new OperationResult
                {
                    valor = 0,
                    mensaje = "Para Continuar Se Requiere" + Environment.NewLine
                };
                if (string.IsNullOrEmpty(this.txtDescripcionDestino.Text.Trim()))
                {
                    result.mensaje = result.mensaje + " * Proporcionar El Nombre Del Destino" + Environment.NewLine;
                    result.valor = 2;
                }
                if (this.cbxEstados.SelectedItem == null)
                {
                    result.mensaje = result.mensaje + " * Seleccionar Un Estado" + Environment.NewLine;
                    result.valor = 2;
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                this.nuevo();
                this.mapEstados();
                this.cargarBases();
                bool habilitado = new PrivilegioSvc().consultarPrivilegio("isAdmin", base.mainWindow.inicio._usuario.idUsuario).typeResult == ResultTypes.success;
                this.ctrEmpresa.cbxEmpresa.SelectionChanged += new SelectionChangedEventHandler(this.CbxEmpresa_SelectionChanged);
                this.ctrEmpresa.loaded(base.mainWindow.empresa, habilitado);
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
    }
}
