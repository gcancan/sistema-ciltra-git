﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for selectProducto.xaml
    /// </summary>
    public partial class selectProducto : Window
    {
        string parametro = "";
        public selectProducto()
        {
            InitializeComponent();
        }
        public selectProducto(string parametro)
        {
            InitializeComponent();
            this.parametro = parametro;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtFind.Text = parametro;
            lvlProductos.Focus();
        }

        public Producto buscarProductos(List<Producto> listProductos)
        {
            mapProducto(listProductos);
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlProductos != null && lvlProductos.SelectedItem != null)
            {
                return (Producto)((ListViewItem)lvlProductos.SelectedItem).Content;
            }
            return null;
        }

        public Producto buscarProductos(int claveFraccion)
        {
            var resp = new ProductoSvc().getALLProductosByIdORidFraccion(claveFraccion);
            if (resp.typeResult == ResultTypes.success)
            {
                mapProducto((List<Producto>)resp.result);
            }
            else
            {
                MessageBox.Show("Ocurrio un error al ralizar la busqueda");
                return null;
            }
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlProductos != null && lvlProductos.SelectedItem != null)
            {
                return (Producto)((ListViewItem)lvlProductos.SelectedItem).Content;
            }
            return null;
        }

        public Producto buscarProductosByEmpresaCliente(int idEmpresa, int idFraccion)
        {
            var resp = new ProductoSvc().getALLProductosByidFraccionEmpresa(idEmpresa, idFraccion);
            if (resp.typeResult == ResultTypes.success)
            {
                mapProducto((List<Producto>)resp.result);
            }
            else
            {
                MessageBox.Show("Ocurrio un error al ralizar la busqueda");
                return null;
            }
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlProductos != null && lvlProductos.SelectedItem != null)
            {
                return (Producto)((ListViewItem)lvlProductos.SelectedItem).Content;
            }
            return null;
        }

        private void mapProducto(List<Producto> mapProducto)
        {
            lvlProductos.Items.Clear();
            List<ListViewItem> lvlList = new List<ListViewItem>();
            foreach (Producto item in mapProducto)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick; ;
                lvlList.Add(lvl);
                //lvlUnidades.Items.Add(lvl);
            }
            lvlProductos.ItemsSource = lvlList;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlProductos.ItemsSource);
            view.Filter = UserFilter;

        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((Producto)(item as ListViewItem).Content).clave.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Producto)(item as ListViewItem).Content).descripcion.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlProductos.ItemsSource).Refresh();
        }

        private void lvlProductos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                DialogResult = false;
            }
        }

        private void txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                lvlProductos.Focus();
            }
        }
    }
}
