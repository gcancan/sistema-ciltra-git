﻿using Core.Interfaces;
using Core.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para catMarcasLlantas.xaml
    /// </summary>
    public partial class catMarcasLlantas : ViewBase
    {
        public catMarcasLlantas()
        {
            InitializeComponent();
        }
        private bool cargarMarcas()
        {
            try
            {
                this.lvlMarcas.Items.Clear();
                OperationResult resp = new MarcasSvc().getMarcasALLorById(0);
                if (resp.typeResult == ResultTypes.success)
                {
                    foreach (Marca marca in resp.result as List<Marca>)
                    {
                        ListViewItem newItem = new ListViewItem
                        {
                            Content = marca
                        };
                        newItem.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                        newItem.Selected += new RoutedEventHandler(this.Lvl_Selected);
                        this.lvlMarcas.Items.Add(newItem);
                    }
                    return true;
                }
                ImprimirMensaje.imprimir(resp);
                return false;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return false;
            }
        }

        public override OperationResult guardar()
        {
            try
            {
                if (string.IsNullOrEmpty(this.txtDescripcion.Text.Trim()))
                {
                    base.mainWindow.habilitar = true;
                    return new OperationResult(ResultTypes.success, "SE NECESITA PROPORCIONAR EL NOMBRE", null);
                }
                Marca marca = this.mapForm();
                OperationResult result = new MarcasSvc().saveMarcaLlantas(ref marca);
                if (result.typeResult == ResultTypes.success)
                {
                    this.mapForm(null);
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
                    base.mainWindow.habilitar = true;
                    this.cargarMarcas();
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }
        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.lvlMarcas.SelectedItem != null)
            {
                this.mapForm((this.lvlMarcas.SelectedItem as ListViewItem).Content as Marca);
            }
        }
        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                ListViewItem item = sender as ListViewItem;
                if (item != null)
                {
                    this.mapForm(item.Content as Marca);
                }
            }
        }
        private Marca mapForm()
        {
            try
            {
                return new Marca
                {
                    clave = (this.ctrClave.Tag == null) ? 0 : (this.ctrClave.Tag as Marca).clave,
                    descripcion = this.txtDescripcion.Text.Trim(),
                    llantas = this.chcLlantas.IsChecked.Value,
                    uniTrans = this.chcUnidadTrans.IsChecked.Value,
                    activo = this.chcActivo.IsChecked.Value
                };
            }
            catch (Exception)
            {
                return null;
            }
        }
        private void mapForm(Marca marca)
        {
            try
            {
                if (marca != null)
                {
                    this.ctrClave.Tag = marca;
                    this.ctrClave.valor = marca.clave;
                    this.txtDescripcion.Text = marca.descripcion;
                    this.chcLlantas.IsChecked = new bool?(marca.llantas);
                    this.chcUnidadTrans.IsChecked = new bool?(marca.uniTrans);
                    this.chcActivo.IsChecked = new bool?(marca.activo);
                }
                else
                {
                    this.ctrClave.Tag = null;
                    this.ctrClave.limpiar();
                    this.txtDescripcion.Clear();
                    this.chcLlantas.IsChecked = false;
                    this.chcUnidadTrans.IsChecked = false;
                    this.chcActivo.IsChecked = true;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }
        public override void nuevo()
        {
            base.nuevo();
            this.mapForm(null);
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.cargarMarcas())
            {
                this.nuevo();
            }
            else
            {
                base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                base.IsEnabled = false;
            }
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult resp = new MarcasSvc().getMarcasALLorById(0);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Marca> lista = resp.result as List<Marca>;
                    new ReportView().exportarListaMarcas(lista);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
