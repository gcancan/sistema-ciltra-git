﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for editRevitalizaciones.xaml
    /// </summary>
    public partial class editRevitalizaciones : ViewBase
    {
        public editRevitalizaciones()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            mapProveedores();
            ctrEmpresa.loaded(null, true);
            nuevo();
        }

        public override void nuevo()
        {
            base.nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            mapForm(null);

        }
         void mapForm(Revitalizaciones revitalizacion)
        {
            if (revitalizacion != null)
            {
                txtFolio.Text = revitalizacion.idRevitalizacion.ToString();
                txtFolio.IsEnabled = false;
                txtFolio.Tag = revitalizacion;
                txtEstatus.Text = revitalizacion.estatus;
                int i = 0;
                foreach (Proveedor item in cbxProveedor.Items)
                {
                    if (item.idProveedor == revitalizacion.proveedor.idProveedor)
                    {
                        cbxProveedor.SelectedIndex = i;
                        break;
                    }
                    i++;
                }
                cbxProveedor.IsEnabled = false;
                dtpFechaSalida.Value = revitalizacion.fechaSalida;
                dtpFechaSalida.IsEnabled = false;
                dtpFechaEntrada.Value = revitalizacion.fechaEntrada;
                dtpFechaEntrada.IsEnabled = true;
                gBoxDetalles.IsEnabled = false;
                stpLlantas.Children.Clear();
                llenarListaLlantas(revitalizacion.listaDetalles);
                if (revitalizacion.estatus == "ACTIVO")
                {
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.editar | ToolbarCommands.cerrar);
                }
                else
                {
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                }
                mainWindow.scrollContainer.IsEnabled = false;
            }
            else
            {
                txtFolio.Clear();
                txtFolio.IsEnabled = true;
                txtFolio.Tag = null;
                txtEstatus.Text = "NUEVO";
                cbxProveedor.SelectedItem = null;
                cbxProveedor.IsEnabled = true;
                dtpFechaSalida.Value = DateTime.Now;
                dtpFechaSalida.IsEnabled = true;
                dtpFechaEntrada.Value = null;
                dtpFechaEntrada.IsEnabled = false;
                gBoxDetalles.IsEnabled = true;
                stpLlantas.Children.Clear();
            }
            stpDetalles.Children.Clear();
        }

        public override void editar()
        {
            base.editar();
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
        }

        private void mapProveedores()
        {
            try
            {
                OperationResult resp = new ProveedorSvc().getAllProveedores();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxProveedor.ItemsSource = (List<Proveedor>)resp.result;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void txtFolio_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrEmpty(txtFolio.Text.Trim()))
                {
                    int i = 0;
                    if (int.TryParse(txtFolio.Text.Trim(), out i))
                    {
                        buscarRevitalizacion(i);
                    }
                }
            }
        }

        private void buscarRevitalizacion(int i)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new RevitalizacionesSvc().getRevitalizacionById(i);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm((Revitalizaciones)resp.result);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new LlantaSvc().getLlantasParaRevitalizar(ctrEmpresa.empresaSelected.clave, txtProfundidadInicial.valor, txtProfundidadFinal.valor);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapListaLlantas((List<Llanta>)resp.result);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapListaLlantas(List<Llanta> result)
        {
            stpDetalles.Children.Clear();
            foreach (Llanta llanta in result)
            {
                CheckBox chc = new CheckBox { Tag = llanta, VerticalContentAlignment = VerticalAlignment.Center, Height = 24, FontSize = 11 };
                StackPanel stpContent = new StackPanel() { Orientation = Orientation.Horizontal };
                Label lblClave = new Label { HorizontalContentAlignment = HorizontalAlignment.Center, Content = llanta.clave, Width = 90, FontWeight = FontWeights.Medium };
                stpContent.Children.Add(lblClave);

                Label lblMarcar = new Label { HorizontalContentAlignment = HorizontalAlignment.Center, Content = llanta.marca.descripcion, Width = 150, FontWeight = FontWeights.Medium };
                stpContent.Children.Add(lblMarcar);

                Label lblDiseño = new Label { HorizontalContentAlignment = HorizontalAlignment.Center, Content = llanta.diseño, Width = 60, FontWeight = FontWeights.Medium };
                stpContent.Children.Add(lblDiseño);

                Label lblProfundidad = new Label { HorizontalContentAlignment = HorizontalAlignment.Center, Content = llanta.profundidadActual, Width = 60, FontWeight = FontWeights.Medium };
                stpContent.Children.Add(lblProfundidad);

                chc.Content = stpContent;
                stpDetalles.Children.Add(chc);
            }
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            List<DetalleRevitalizacion> list = new List<DetalleRevitalizacion>();
            foreach (CheckBox chc in stpDetalles.Children.Cast<CheckBox>().ToList())
            {
                if (chc.IsChecked.Value)
                {
                    DetalleRevitalizacion detalle = new DetalleRevitalizacion
                    {
                        llanta = (Llanta)chc.Tag,
                        profundidadAct = ((Llanta)chc.Tag).profundidadActual,
                        profundidadNueva = 0,
                        diseño = null
                    };
                    list.Add(detalle);
                }
                
            }
            llenarListaLlantas(list);
        }

        private void llenarListaLlantas(List<DetalleRevitalizacion> list)
        {
            foreach (DetalleRevitalizacion item in list)
            {
                int? i = 0;
                if (!buscarRegistro(item.llanta, out i))
                {
                    StackPanel stpContent = new StackPanel() { Orientation = Orientation.Horizontal };
                    Label lblClave = new Label { HorizontalContentAlignment = HorizontalAlignment.Center, Content = item.llanta.clave, Width = 90, FontWeight = FontWeights.Medium };
                    stpContent.Children.Add(lblClave);

                    Label lblDiseñoAct = new Label { HorizontalContentAlignment = HorizontalAlignment.Center, Content = item.llanta.diseño, Width = 80, FontWeight = FontWeights.Medium };
                    stpContent.Children.Add(lblDiseñoAct);

                    Label lblProfundidadAct = new Label { HorizontalContentAlignment = HorizontalAlignment.Center, Content = item.profundidadAct, Width = 80, FontWeight = FontWeights.Medium };
                    stpContent.Children.Add(lblProfundidadAct);

                    controlDiseños ctrDiseños = new controlDiseños() { Width = 90 };
                    ctrDiseños.cargarControl();
                    ctrDiseños.diseñoSelect = item.diseño;
                    stpContent.Children.Add(ctrDiseños);

                    controlDecimal ctrProfundidad = new controlDecimal() { Width = 90, valor = item.profundidadNueva, IsEnabled = item.idDetalleRevitalizacion != 0 };
                    stpContent.Children.Add(ctrProfundidad);

                    if (item.idDetalleRevitalizacion == 0)
                    {
                        Button btnQuitar = new Button() { Content = "Quitar", Tag = item, Width = 120, Foreground = Brushes.White, Background = Brushes.Red };
                        btnQuitar.Click += BtnQuitar_Click;
                        stpContent.Children.Add(btnQuitar);
                    }                    

                    //if (item.idDetalleRevitalizacion == 0)
                    //{
                        CheckBox lblLlanta = new CheckBox { Tag = item, Content = stpContent, VerticalContentAlignment = VerticalAlignment.Center, FontSize = 11 };
                        stpLlantas.Children.Add(lblLlanta);
                    //}
                    //Label lblProfundidad = new Label { HorizontalContentAlignment = HorizontalAlignment.Center, Content = llanta.profundidadActual, Width = 60, FontWeight = FontWeights.Medium };
                    //stpContent.Children.Add(lblProfundidad);
                }

            }
        }

        private void BtnQuitar_Click(object sender, RoutedEventArgs e)
        {
            DetalleRevitalizacion detalle = (DetalleRevitalizacion)((Button)sender).Tag;
            int? i = null;
            if (buscarRegistro(detalle.llanta, out i))
            {
                stpLlantas.Children.Remove(stpLlantas.Children[(int)i]);
            }
        }

        public bool buscarRegistro(Llanta llanta, out int ? i)
        {
            int contador = 0;
            foreach (CheckBox item in stpLlantas.Children.Cast<CheckBox>().ToList())
            {
                if (llanta.idLlanta == ((DetalleRevitalizacion)item.Tag).llanta.idLlanta)
                {
                    i = contador;
                    return true;
                }
                contador++;
            }
            i = null;
            return false;
        }

        public override OperationResult guardar()
        {
            try
            {
                Revitalizaciones revitalizacion = mapForm();
                OperationResult resp = new RevitalizacionesSvc().saveRevitalizacion(ref revitalizacion);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(revitalizacion);                    
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        private Revitalizaciones mapForm()
        {
            try
            {
                Revitalizaciones revitalizacion = new Revitalizaciones
                {
                    idRevitalizacion = txtFolio.Tag == null ? 0 : ((Revitalizaciones)((txtFolio).Tag)).idRevitalizacion,
                    estatus = txtEstatus.Text == "NUEVO" ? "ACTIVO" : "FINALIZADO",
                    fechaSalida = (DateTime)dtpFechaSalida.Value,
                    fechaEntrada = txtEstatus.Text == "NUEVO" ? null : dtpFechaEntrada.Value,
                    proveedor = (Proveedor)cbxProveedor.SelectedItem,
                    usuario = mainWindow.inicio._usuario.nombreUsuario
                };
                revitalizacion.listaDetalles = new List<DetalleRevitalizacion>();
                revitalizacion.listaDetalles = getListaDetalles();

                return revitalizacion;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private List<DetalleRevitalizacion> getListaDetalles()
        {
            List<DetalleRevitalizacion> lista = new List<DetalleRevitalizacion>();
            if (txtEstatus.Text == "NUEVO")
            {
                foreach (CheckBox chc in stpLlantas.Children.Cast<CheckBox>().ToList())
                {
                    DetalleRevitalizacion detalle = ((DetalleRevitalizacion)chc.Tag);
                    foreach (object obj in ((StackPanel)chc.Content).Children.Cast<object>().ToList())
                    {
                        if (obj is controlDiseños)
                        {
                            detalle.diseño = ((controlDiseños)obj).diseñoSelect;
                        }
                    }
                    lista.Add(detalle);
                }
            }
            else if (txtEstatus.Text == "ACTIVO")
            {
                foreach (CheckBox chc in stpLlantas.Children.Cast<CheckBox>().ToList())
                {
                    DetalleRevitalizacion detalle = ((DetalleRevitalizacion)chc.Tag);
                    foreach (object obj in ((StackPanel)chc.Content).Children.Cast<object>().ToList())
                    {
                        if (obj is controlDiseños)
                        {
                            detalle.diseño = ((controlDiseños)obj).diseñoSelect;
                        }
                        if (obj is controlDecimal)
                        {
                            detalle.profundidadNueva = ((controlDecimal)obj).valor;
                        }
                    }
                    lista.Add(detalle);
                }
            }

            return lista;
        }
    }
}
