﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for agregarOrdenLlantera.xaml
    /// </summary>
    public partial class agregarOrdenLlantera : Window
    {
        private TipoOrdenServicio tipoOrden;

        public agregarOrdenLlantera()
        {
            InitializeComponent();
        }
        private bool isLlantera = true;
        private bool personal = false;
        List<OrdenTaller> listaOrdenes;
        OrdenTaller ordenTaller = null;
        public agregarOrdenLlantera(TipoOrdenServicio tipoOrden, bool isLlantera = true, List<OrdenTaller> listaOrdenes = null, bool soloActividad = false, bool personal = false)
        {
            this.personal = personal;
            this.soloActividad = soloActividad;
            this.listaOrdenes = listaOrdenes;
            this.isLlantera = isLlantera;
            this.tipoOrden = tipoOrden;
            InitializeComponent();
        }
        bool soloActividad = false;
        public agregarOrdenLlantera(OrdenTaller ordenTaller, TipoOrdenServicio tipoOrden, bool isLlantera = true, List<OrdenTaller> listaOrdenes = null, bool soloActividad = false)
        {
            this.ordenTaller = ordenTaller;
            this.listaOrdenes = listaOrdenes;
            this.isLlantera = isLlantera;
            this.tipoOrden = tipoOrden;
            this.soloActividad = soloActividad;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;

                cargarTiposOrden();
                cargarServicios();
                cargarActividades();
                cargarDivision();
                chcServicio.IsChecked = true;
                txtUnidadTrans.Text = tipoOrden.unidadTrans.clave + "-" + tipoOrden.unidadTrans.tipoUnidad.descripcion;
                txtTipoOrden.Text = tipoOrden.TipoServicio;

                if (isLlantera)
                {
                    var r = new DiagramasLLantasSvc().getDiagramaByTipoUnidadTrans(tipoOrden.unidadTrans.tipoUnidad);
                    if (r.typeResult != ResultTypes.success)
                    {
                        ImprimirMensaje.imprimir(r);
                    }
                    else
                    {
                        cargarDiagrama((DiagramaLLantas)r.result, tipoOrden);
                        int idex = 0;
                        if (ordenTaller != null)
                        {
                            foreach (Llanta llanta in cbxLlanta.Items)
                            {
                                if (llanta.clave == ordenTaller.clave_llanta)
                                {
                                    cbxLlanta.SelectedIndex = idex;
                                    break;
                                }
                                idex++;
                            }
                        }                        
                    }
                }
                else
                {
                    gBoxDiagrama.Visibility = Visibility.Collapsed;
                }

                int i = 0;
                foreach (TipoOrden item in cbxTipoOrden.Items)
                {
                    if (this.tipoOrden.tipoOrden.idTipoOrden == item.idTipoOrden)
                    {
                        cbxTipoOrden.SelectedIndex = i;
                        cbxTipoOrden.IsEnabled = false;
                        break;
                    }
                    i++;
                }

                if (ordenTaller != null)
                {
                    if (ordenTaller.actividad != null)
                    {
                        mapActividad(ordenTaller.actividad);
                        seleccionarDivicion(ordenTaller.actividad.idDivicion);
                        //int ind = 0;
                        //foreach (Actividad act in cbxActividad.Items)
                        //{
                        //    chcActividad.IsChecked = true;
                        //    if (act.idActividad == ordenTaller.actividad.idActividad)
                        //    {
                        //        cbxActividad.SelectedIndex = ind;
                        //        break;
                        //    }
                        //    ind++;
                        //}
                    }
                    else if (!string.IsNullOrEmpty(ordenTaller.descripcionOS))
                    {
                        chcDescripcion.IsChecked = true;
                        txtDescripcion.Text = ordenTaller.descripcionOS;
                    }
                    lvlOrdenes.Visibility = Visibility.Collapsed;
                    btnAgregar.Visibility = Visibility.Collapsed;
                }
                if (soloActividad)
                {
                    chcDescripcion.Visibility = Visibility.Collapsed;
                    txtDescripcion.Visibility = Visibility.Collapsed;
                    chcActividad.IsChecked = true;
                }

                if (personal)
                {
                    gBoxPersonal.Visibility = Visibility.Visible;
                }

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        void cargarDiagrama(DiagramaLLantas diagrama, TipoOrdenServicio orden)
        {
            eje1.Children.Clear();
            eje2.Children.Clear();
            eje3.Children.Clear();
            eje4.Children.Clear();
            var listaStps = stpDiagrama.Children.Cast<StackPanel>().ToList();

            string path = System.IO.Directory.GetCurrentDirectory();
            int posicion = 0;
            for (int i = 1; i <= diagrama.numEjes; i++)
            {
                int llantas = diagrama.listaDetalles[i - 1].llantas;
                for (int j = 1; j <= llantas; j++)
                {

                    posicion++;
                    /**/
                    Image img = new Image();
                    img.Height = 40;
                    BitmapImage ima = new BitmapImage(new Uri(path + "\\Recursos\\BalanceNew.png"));
                    Label lblLLanta = new Label();
                    //chc.VerticalContentAlignment = VerticalAlignment.Bottom;
                    StackPanel stp = new StackPanel() { Orientation = Orientation.Vertical };
                    stp.Children.Add(new Label { Content = posicion, HorizontalContentAlignment = HorizontalAlignment.Center });
                    img.Source = ima;
                    stp.Children.Add(img);
                    Label lblTitulo = new Label() { FontWeight = FontWeights.Medium };
                    var respLLanta = new LlantaSvc().getLLantaByPosAndUnidadTrans(posicion, orden.unidadTrans);
                    if (respLLanta.typeResult == ResultTypes.success)
                    {
                        Llanta llanta = (Llanta)respLLanta.result;
                        lblTitulo.Content = llanta.clave;
                        lblLLanta.Tag = llanta;
                        cbxLlanta.Items.Add(llanta);
                        StackPanel stpToolTip = new StackPanel { Orientation = Orientation.Vertical };
                        stpToolTip.Children.Add(new Label { Content = "INFORMACIÓN DE LA LLANTA", FontWeight = FontWeights.Bold, FontSize = 16 });

                        StackPanel stpLabels = new StackPanel();

                        StackPanel stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "UBICACIÓN:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.unidadTransporte.clave + " POS: " + llanta.posicion.ToString() });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "MARCA:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.marca.descripcion });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "TIPO LLANTA:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.tipoLlanta.nombre });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "DISEÑO:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.diseño });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "MEDIDA:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.medida });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "PROFUNDIDAD:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.profundidad });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "FECHA:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.fecha });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "RFID:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.rfid });
                        stpLabels.Children.Add(stpControl);

                        stpToolTip.Children.Add(stpLabels);
                        lblLLanta.ToolTip = new ToolTip { AllowDrop = true, Content = stpToolTip /*"LLANTA " + ((Llanta)respLLanta.result).clave*/ };

                    }
                    else
                    {
                        //lblLLanta.ToolTip = new ToolTip { Content = "SIN ASIGNAR" };
                        //ContextMenu menu = new ContextMenu();
                        //MenuItem itemAgregar = new MenuItem { Header = "Asignar llanta" };
                        //menu.Items.Add(itemAgregar);
                        //lblLLanta.ContextMenu = menu;
                        lblTitulo.Content = "SIN ASIGNAR";

                    }
                    stp.Children.Add(lblTitulo);
                    lblLLanta.Content = stp;
                    lblLLanta.MouseDoubleClick += LblLLanta_MouseDoubleClick;
                    listaStps[i - 1].Children.Add(lblLLanta);
                    /**/
                }
            }
        }

        private void LblLLanta_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Label lbl = (Label)sender;
            if (lbl.Tag != null)
            {
                Llanta llanta = (Llanta)lbl.Tag;
                int i = 0;
                foreach (Llanta item in cbxLlanta.Items)
                {
                    if (item.clave == llanta.clave)
                    {
                        cbxLlanta.SelectedIndex = i;
                        return;
                    }
                    i++;
                }
            }
        }

        private void cargarDivision()
        {
            var resp = new OrdenesTrabajoSvc().getDivisiones();
            if (resp.typeResult == ResultTypes.success)
            {
                List<Division> lista = (List<Division>)resp.result;
                cbxDivision.ItemsSource = lista;
                cbxDivisionNuevo.ItemsSource = lista;
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
            }
        }
        List<Actividad> lista = new List<Actividad>();
        private void cargarActividades()
        {
            var resp = new OrdenesTrabajoSvc().getRelacionActividad(this.tipoOrden.idTipoOrdenServicio);
            if (resp.typeResult == ResultTypes.success)
            {
                lista = (List<Actividad>)resp.result;
                //cbxActividad.ItemsSource = lista;
                //cbxActividadNuevo.ItemsSource = lista;
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
            }
        }

        private void cargarTiposOrden()
        {
            var resp = new OrdenesTrabajoSvc().getTipoDeOrden(this.tipoOrden.idTipoOrdenServicio);
            if (resp.typeResult == ResultTypes.success)
            {
                List<TipoOrden> lista = (List<TipoOrden>)resp.result;
                cbxTipoOrden.ItemsSource = lista;
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
            }
        }

        private void cargarServicios()
        {
            var resp = new OrdenesTrabajoSvc().getRelacionServicios(this.tipoOrden.idTipoOrdenServicio);
            if (resp.typeResult == ResultTypes.success)
            {
                List<Servicio> lista = (List<Servicio>)resp.result;
                cbxServicio.ItemsSource = lista;
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
            }
        }

        private void cbxServicio_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (((ComboBox)sender).SelectedItem != null)
            {
                Servicio servicio = ((Servicio)((ComboBox)sender).SelectedItem);
                txtDescripcionServicio.Text = servicio.descipcion;
                seleccionarDivicion(servicio.idDivicion);
            }
            else
            {
                txtDescripcionServicio.Clear();
            }

        }

        private void seleccionarDivicion(int idDivicion)
        {
            int i = 0;
            foreach (Division item in cbxDivision.Items)
            {
                if (item.idDivision == idDivicion)
                {
                    cbxDivision.SelectedIndex = i;
                    cbxDivision.IsEnabled = false;
                    return;
                }
                i++;
            }
            cbxDivision.SelectedIndex = 0;
            cbxDivision.IsEnabled = true;
        }
        private void seleccionarDivicionNuevo(int idDivicion)
        {
            int i = 0;
            foreach (Division item in cbxDivisionNuevo.Items)
            {
                if (item.idDivision == idDivicion)
                {
                    cbxDivisionNuevo.SelectedIndex = i;
                    cbxDivisionNuevo.IsEnabled = false;
                    return;
                }
                i++;
            }
            cbxDivisionNuevo.SelectedIndex = 0;
            cbxDivisionNuevo.IsEnabled = true;
        }

        private void Tipo_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton rbn = (RadioButton)sender;
            switch (rbn.Name)
            {
                case "chcServicio":
                    cbxServicio.IsEnabled = true;
                    ///////////////////////////////
                    txtDescripcion.Clear();
                    txtDescripcion.IsEnabled = false;
                    //cbxActividad.SelectedItem = null;
                    mapActividad(null);
                    cbxActividad.IsEnabled = false;
                    //cbxServicio.IsDropDownOpen = true;
                    stpAgregarNuevos.Visibility = Visibility.Collapsed;
                    break;
                case "chcDescripcion":
                    txtDescripcion.IsEnabled = true;
                    //////////////////////////////
                    cbxServicio.IsEnabled = false;
                    cbxServicio.SelectedItem = null;
                    //cbxActividad.SelectedItem = null;
                    mapActividad(null);
                    cbxActividad.IsEnabled = false;
                    if (ordenTaller != null)
                    {
                        txtDescripcion.Text = ordenTaller.descripcionOS;
                        stpAgregarNuevos.Visibility = Visibility.Visible;
                    }
                    cbxDivision.SelectedIndex = 0;
                    break;
                case "chcActividad":
                    cbxActividad.IsEnabled = true;
                    /////////////////////////////
                    txtDescripcion.Clear();
                    txtDescripcion.IsEnabled = false;
                    cbxServicio.IsEnabled = false;
                    cbxServicio.SelectedItem = null;
                    stpAgregarNuevos.Visibility = Visibility.Collapsed;
                    if (ordenTaller != null)
                    {
                        if (ordenTaller.actividad != null)
                        {
                            mapActividad(ordenTaller.actividad);
                            //int ind = 0;
                            //foreach (Actividad act in cbxActividad.Items)
                            //{
                            //    chcActividad.IsChecked = true;
                            //    if (act.idActividad == ordenTaller.actividad.idActividad)
                            //    {
                            //        cbxActividad.SelectedIndex = ind;
                            //        break;
                            //    }
                            //    ind++;
                            //}
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            agregarNuevo();
            //cbxTipoOrden.SelectedItem = null;
            //cbxTipoOrden.Focus();
            //cbxTipoOrden.IsDropDownOpen = true;                 
        }

        private void agregarNuevo()
        {
            DetOrdenServicio detOrdenServicio = new DetOrdenServicio();
            if (personal)
            {
                if (txtResponsable.personal == null)
                {
                    ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "Se requiere asignar al personal para esta actividad" });
                    return;
                }
                else
                {
                    detOrdenServicio.responsable = txtResponsable.personal;
                    detOrdenServicio.auxiliar1 = txtAux1.personal;
                    detOrdenServicio.auxiliar2 = txtAux2.personal;
                }
            }
            //if (cbxTipoOrden.SelectedItem == null)
            //{
            //    ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "NO SE SELECCIONO UN TIPO SERVICIO" });
            //    return;
            //}
            //detOrdenServicio.tipoOrden = (TipoOrden)cbxTipoOrden.SelectedItem;
            if (chcServicio.IsChecked.Value)
            {
                if (cbxServicio.SelectedItem != null)
                {
                    detOrdenServicio.servicio = (Servicio)cbxServicio.SelectedItem;
                }
                else
                {
                    ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "NO SE SELECCIONO UN SERVICIO" });
                    return;
                }
            }
            else if (chcDescripcion.IsChecked.Value)
            {
                if (!string.IsNullOrEmpty(txtDescripcion.Text.Trim()))
                {
                    detOrdenServicio.descripcion = txtDescripcion.Text.Trim();
                }
                else
                {
                    ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "LA DESCRIPCIÓN NO PUEDE ESTAR VACIA" });
                    return;
                }
            }
            else if (chcActividad.IsChecked.Value)
            {
                if (cbxActividad.Tag != null)
                {
                    detOrdenServicio.actividad = (Actividad)cbxActividad.Tag;
                }
                else
                {
                    ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "NO SE SELECCIONO UNA ACTIVIDAD" });
                    return;
                }
            }

            if (cbxDivision.SelectedItem == null)
            {
                ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "NO SE SELECCIONO LA DIVISIÓN" });
                return;
            }
            if (isLlantera)
            {
                if (cbxLlanta.SelectedItem != null)
                {
                    detOrdenServicio.llanta = (Llanta)cbxLlanta.SelectedItem;
                }
                else
                {
                    ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "NO SE SELECCIONO LA LLANTA" });
                    return;
                }
            }

            detOrdenServicio.division = (Division)cbxDivision.SelectedItem;
            detOrdenServicio.tipoOrdenServicio = tipoOrden;
            detOrdenServicio.idOrdenTrabajo = ordenTaller == null ? 0 : ordenTaller.idOrdenTrabajo;
            //if (tipoOrden.estatus == "DIAG" || tipoOrden.estatus == "ASIG")
            //{
            //    if (detOrdenServicio.idOrdenTrabajo == 0)
            //    {
            //        detOrdenServicio.estatus = "REC";
            //    }               

            //}
            //if (detOrdenServicio.idOrdenTrabajo == 0)
            //{
            //    detOrdenServicio.estatus = "REC";
            //}
            //else
            //{
            detOrdenServicio.estatus = tipoOrden.estatus;
            //}

            if (detOrdenServicio.servicio != null)
            {
                if (detOrdenServicio.servicio.listaActividades.Count > 0)
                {
                    foreach (var item in detOrdenServicio.servicio.listaActividades)
                    {
                        DetOrdenServicio newDetOr = new DetOrdenServicio()
                        {
                            actividad = item,
                            servicio = detOrdenServicio.servicio,
                            descripcion = detOrdenServicio.descripcion,
                            division = detOrdenServicio.division,
                            llanta = detOrdenServicio.llanta,
                            tipoOrdenServicio = detOrdenServicio.tipoOrdenServicio
                        };
                        if (agregar(newDetOr))
                        {
                            ListViewItem lvl = new ListViewItem();
                            lvl.Content = newDetOr;
                            lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                            lvlOrdenes.Items.Add(lvl);
                        }
                    }
                }
                else
                {
                    if (agregar(detOrdenServicio))
                    {
                        ListViewItem lvl = new ListViewItem();
                        lvl.Content = detOrdenServicio;
                        lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                        lvlOrdenes.Items.Add(lvl);
                    }
                }
            }
            else
            {
                if (agregar(detOrdenServicio))
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = detOrdenServicio;
                    lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                    lvlOrdenes.Items.Add(lvl);
                }
            }
            mapActividad(null);
        }

        private bool agregar(DetOrdenServicio detOrdenServicio)
        {
            try
            {
                foreach (ListViewItem item in lvlOrdenes.Items)
                {
                    var detOrd = (DetOrdenServicio)item.Content;
                    if (isLlantera)
                    {
                        if (detOrdenServicio.ordenTrabajo == detOrd.ordenTrabajo && detOrdenServicio.llanta.clave == detOrd.llanta.clave)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (detOrdenServicio.ordenTrabajo == detOrd.ordenTrabajo)
                        {
                            return false;
                        }
                    }
                }

                if (listaOrdenes != null)
                {
                    foreach (var item in listaOrdenes)
                    {
                        if (item.actividad == null)
                        {
                            continue;
                        }
                        if (item.estatus.ToUpper() != "CAN")
                        {
                            if (detOrdenServicio.actividad != null)
                            {
                                if (detOrdenServicio.actividad.idActividad == item.actividad.idActividad)
                                {
                                    ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "Ya existe esta actividad para esta Orden de Servicio" });
                                    return false;
                                }
                            }

                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return false;
            }
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            lvlOrdenes.Items.Remove(lvlOrdenes.SelectedItem);
        }

        private void cbxLlanta_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox combo = (ComboBox)sender;
            Llanta llanta = (Llanta)combo.SelectedItem;
            txtPos.Text = llanta.posicion.ToString();
            seleccionarImagen(llanta);
        }
        private void seleccionarImagen(Llanta det)
        {
            List<StackPanel> listaStps = stpDiagrama.Children.Cast<StackPanel>().ToList();
            foreach (var stpLabel in listaStps)
            {
                List<Label> listaLabel = stpLabel.Children.Cast<Label>().ToList();
                foreach (var item in listaLabel)
                {
                    Llanta llanta = (Llanta)item.Tag;
                    if (llanta == null)
                    {
                        item.Background = null;
                        continue;
                    }
                    if (llanta.clave == det.clave)
                    {
                        item.Background = Brushes.SteelBlue;
                    }
                    else
                    {
                        item.Background = null;
                    }
                }
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;



                List<DetOrdenServicio> lista = getLista();

                if (lista != null)
                {
                    if (lista.Count > 0)
                    {
                        if (lvlOrdenesNuevos.Items.Count > 0)
                        {
                            Actividad actividad = (Actividad)((ListViewItem)lvlOrdenesNuevos.Items[0]).Content;
                            lista[0].actividad = actividad;
                            lista[0].servicio = null;
                            lista[0].division = actividad.division;
                            lista[0].listaActividades = new List<Actividad>();
                            int i = 0;
                            foreach (ListViewItem item in lvlOrdenesNuevos.Items)
                            {
                                if (i > 0)
                                {
                                    lista[0].listaActividades.Add(item.Content as Actividad);
                                }
                                i++;
                            }
                        }

                        var resp = new OrdenesTrabajoSvc().saveDetOrdenServ(lista);
                        ImprimirMensaje.imprimir(resp);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            DialogResult = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }

        }

        private List<DetOrdenServicio> getLista()
        {
            try
            {
                List<DetOrdenServicio> list = new List<DetOrdenServicio>();
                if (ordenTaller != null)
                {
                    agregarNuevo();
                    foreach (ListViewItem item in lvlOrdenes.Items)
                    {
                        list.Add((DetOrdenServicio)item.Content);
                    }
                }
                else
                {
                    foreach (ListViewItem item in lvlOrdenes.Items)
                    {
                        list.Add((DetOrdenServicio)item.Content);
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }

        private void cbxActividad_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (cbxActividad.SelectedItem != null)
            //{
            //    Actividad actividad = cbxActividad.SelectedItem as Actividad;
            //    seleccionarDivicion(actividad.idDivicion);
            //}
            //else
            //{
            //    cbxDivision.SelectedItem = 0;
            //    cbxDivision.IsEnabled = true;
            //}

        }
        private void cbxActividadNuevo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (cbxActividadNuevo.SelectedItem != null)
            //{
            //    Actividad actividad = cbxActividadNuevo.SelectedItem as Actividad;
            //    seleccionarDivicionNuevo(actividad.idDivicion);
            //}
            //else
            //{
            //    cbxDivisionNuevo.SelectedItem = 0;
            //    cbxDivisionNuevo.IsEnabled = true;
            //}

        }

        private void cbxTipoOrden_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cbx = sender as ComboBox;
            if (cbx.SelectedItem != null)
            {

                TipoOrden orden = cbx.SelectedItem as TipoOrden;
                if (orden.nombreTipoOrden.ToUpper() == "CORRECTIVO")
                {
                    chcServicio.Visibility = Visibility.Collapsed;
                    stpServicios.Visibility = Visibility.Collapsed;
                    chcDescripcion.IsChecked = true;
                }
                else
                {
                    chcServicio.IsEnabled = true;
                    chcServicio.IsChecked = true;

                    chcDescripcion.IsEnabled = false;

                    chcActividad.IsEnabled = false;
                }
                grigTipos.IsEnabled = true;
                stpAgregar.IsEnabled = true;
            }
            else
            {
                grigTipos.IsEnabled = false;
                stpAgregar.IsEnabled = false;
                cbxServicio.SelectedItem = null;
                //cbxActividad.SelectedItem = null;
                cbxDivision.SelectedItem = null;
                txtDescripcion.Clear();
            }

        }

        private void btnAgregarNuevo_Click(object sender, RoutedEventArgs e)
        {
            if (cbxActividadNuevo.Tag != null && cbxDivisionNuevo.SelectedItem != null)
            {
                Actividad actividad = cbxActividadNuevo.Tag as Actividad;
                actividad.division = cbxDivisionNuevo.SelectedItem as Division;

                if (agregarNuevo(actividad))
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = actividad;
                    lvlOrdenesNuevos.Items.Add(lvl);
                }

                mapActividadNuevo(null);
            }
        }

        private bool agregarNuevo(Actividad actividad)
        {
            var add = true;
            foreach (ListViewItem lvl in lvlOrdenesNuevos.Items)
            {
                Actividad act = lvl.Content as Actividad;
                if (act.idActividad == actividad.idActividad)
                {
                    return false;
                }
            }

            if (listaOrdenes != null)
            {
                foreach (var item in listaOrdenes)
                {
                    if (item.actividad == null)
                    {
                        continue;
                    }
                    if (item.estatus.ToUpper() != "CAN")
                    {
                        if (actividad.idActividad == item.actividad.idActividad)
                        {
                            ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "Ya existe esta actividad para está Orden de Servicio" });
                            return false;
                        }
                    }
                }
            }

            return add;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            txtResponsable.limpiar();
            txtAux1.limpiar();
            txtAux2.limpiar();
        }

        private void cbxActividad_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Actividad act = null;
                var lvl = lista.FindAll(x => (x.nombre.IndexOf(cbxActividad.Text, StringComparison.OrdinalIgnoreCase) >= 0) ||
                                       (x.CCODIGOPRODUCTO.IndexOf(cbxActividad.Text, StringComparison.OrdinalIgnoreCase) >= 0));

                if (lvl.Count == 1)
                {
                    mapActividad(lvl[0]);
                }
                else
                {
                    selectActividadCOM buscador = new selectActividadCOM(cbxActividad.Text.Trim());
                    act = buscador.getActividad(lista);
                    if (act != null)
                    {
                        mapActividad(act);
                    }
                }                
            }
            
        }

        private void mapActividad(Actividad act)
        {
            if (act != null)
            {
                cbxActividad.Tag = act;
                cbxActividad.Text = act.nombre;
                cbxActividad.IsEnabled = false;
                seleccionarDivicion(act.idDivicion);
            }
            else
            {
                cbxActividad.Tag = null;
                cbxActividad.Clear();
                cbxActividad.IsEnabled = true;
            }

        }
        private void mapActividadNuevo(Actividad act)
        {
            if (act != null)
            {
                cbxActividadNuevo.Tag = act;
                cbxActividadNuevo.Text = act.nombre;
                cbxActividadNuevo.IsEnabled = false;
                seleccionarDivicionNuevo(act.idDivicion);
            }
            else
            {
                cbxActividadNuevo.Tag = null;
                cbxActividadNuevo.Clear();
                cbxActividadNuevo.IsEnabled = true;
            }

        }

        private void btnCambiar_Click(object sender, RoutedEventArgs e)
        {
            mapActividad(null);
        }

        private void btnCambiarnUEVO_Click(object sender, RoutedEventArgs e)
        {
            mapActividadNuevo(null);
        }

        private void cbxActividadNuevo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Actividad act = null;
                var lvl = lista.FindAll(x => (x.nombre.IndexOf(cbxActividadNuevo.Text, StringComparison.OrdinalIgnoreCase) >= 0) ||
                                       (x.CCODIGOPRODUCTO.IndexOf(cbxActividadNuevo.Text, StringComparison.OrdinalIgnoreCase) >= 0));

                if (lvl.Count == 1)
                {
                    mapActividadNuevo(lvl[0]);
                }
                else
                {
                    selectActividadCOM buscador = new selectActividadCOM(cbxActividad.Text.Trim());
                    act = buscador.getActividad(lista);
                    if (act != null)
                    {
                        mapActividadNuevo(act);
                    }
                }

            }
        }
    }
}
