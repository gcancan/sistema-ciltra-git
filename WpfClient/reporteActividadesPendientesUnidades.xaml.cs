﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para reporteActividadesPendientesUnidades.xaml
    /// </summary>
    public partial class reporteActividadesPendientesUnidades : ViewBase
    {
        public reporteActividadesPendientesUnidades()
        {
            InitializeComponent();
        }
        controlMotivosCancelacion ctrMotivos;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresa.loaded(mainWindow.empresa, (new PrivilegioSvc().consultarPrivilegio("isAdmin", mainWindow.usuario.idUsuario).typeResult == ResultTypes.success));
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
            ctrMotivos = new controlMotivosCancelacion(enumProcesos.ACTIVIDAD_PENDIENTE);
            stpMotivos.Children.Add(ctrMotivos);
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                ctrUnidades.ItemsSource = null;
                if (ctrEmpresa.empresaSelected != null)
                {
                    var resp = new UnidadTransporteSvc().getUnidadesALLorById(ctrEmpresa.empresaSelected.clave);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        ctrUnidades.ItemsSource = resp.result as List<UnidadTransporte>;
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            buscar();
        }
        void buscar()
        {
            try
            {
                lvlActividadesPendientes.ItemsSource = null;
                Cursor = Cursors.Wait;

                if (ctrUnidades.SelectedItems.Count == 0) return;

                List<string> listaId = ctrUnidades.SelectedItems.Cast<UnidadTransporte>().ToList().Select(s => s.clave).ToList();

                //var resp = new SolicitudOrdenServicioSvc().getActividadesPendientes(ctrEmpresa.empresaSelected.clave, ctrFechas.fechaInicial, ctrFechas.fechaFinal, listaId);
                var resp = new SolicitudOrdenServicioSvc().getActividadesPendientes(listaId);

                if (resp.typeResult == ResultTypes.success)
                {
                    lvlActividadesPendientes.ItemsSource = resp.result as List<ActividadPendiente>;
                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        List<string> listaId = new List<string>();
        private void chcBaja_Checked(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                listaId.Add(((int)(sender as CheckBox).Tag).ToString());
            }
        }

        private void chcBaja_Unchecked(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                listaId.Remove(((int)(sender as CheckBox).Tag).ToString());
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ctrMotivos.cbxMotivos.SelectedItem == null)
                {
                    imprimir(new OperationResult(ResultTypes.warning, "SE REQUIERE MOTIVO DE CANCELACIÓN"));
                    return;
                }
                MotivoCancelacion motivo = ctrMotivos.motivoCancelacion;
                if (motivo.requiereObservacion && string.IsNullOrEmpty(ctrMotivos.obsMotivo))
                {
                    imprimir(new OperationResult(ResultTypes.warning, "SE REQUIERE LA OBSERVACIÓN"));
                    return;
                }

                if (listaId.Count == 0)
                {
                    return;
                }

                var resp = new SolicitudOrdenServicioSvc().bajaActividadesPendientes(listaId, mainWindow.usuario.nombreUsuario, ctrMotivos.obsMotivo, motivo.idMotivoCancelacion);
                if (resp.typeResult == ResultTypes.success)
                {
                    ctrMotivos.motivoCancelacion = null;
                    listaId = new List<string>();
                    buscar();
                }
                imprimir(resp);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void ChcTodos_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                CheckBox chc = sender as CheckBox;
                switch (chc.IsChecked)
                {
                    case true:
                        foreach (var item in ctrUnidades.Items)
                        {
                            ctrUnidades.SelectedItems.Add(item);
                        }                        
                        break;
                    case false:
                        ctrUnidades.SelectedItems.Clear();
                        break;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
