﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para catActividades.xaml
    /// </summary>
    public partial class catActividades : ViewBase
    {
        public catActividades()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);
        }
        void mapForm(Actividad actividad)
        {
            if (actividad != null)
            {
                ctrClave.Tag = actividad;
                ctrClave.valor = actividad.idActividad;
                chcActivo.IsChecked = actividad.activo;
                txtNombreActividad.Text = actividad.nombre;
                chcTaller.IsChecked = actividad.taller;
                chcLlantera.IsChecked = actividad.llantera;
                chcLavadero.IsChecked = actividad.lavadero;
            }
            else
            {
                ctrClave.Tag = null;
                ctrClave.valor = 0;
                chcActivo.IsChecked = true;
                txtNombreActividad.Clear();
                chcTaller.IsChecked = false;
                chcLlantera.IsChecked = false;
                chcLavadero.IsChecked = false;
            }
        }
        Actividad mapForm()
        {
            try
            {
                Actividad actividad = new Actividad
                {
                    idActividad = ctrClave.Tag == null ? 0 : (ctrClave.Tag as Actividad).idActividad,
                    nombre = txtNombreActividad.Text.Trim(),
                    taller = chcTaller.IsChecked.Value,
                    lavadero = chcLavadero.IsChecked.Value,
                    llantera = chcLlantera.IsChecked.Value,
                    listProductosCOM = new List<ProductosCOM>(),
                    activo = chcActivo.IsChecked.Value
                };
                return actividad;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult validacion = validar();
                if (validacion.typeResult != ResultTypes.success) return validacion;

                Actividad actividad = mapForm();
                if (actividad != null)
                {
                    var resp = new ActividadSvc().saveActividad(ref actividad);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mapForm(actividad);
                        base.guardar();
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult(ResultTypes.warning, "OCURRIO UN ERROR AL LEER LA INFORMACIÓN DE LA PANTALLA.");
                }

            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        public override OperationResult eliminar()
        {
            try
            {
                Cursor = Cursors.Wait;
               
                Actividad actividad = mapForm();
                actividad.activo = false;
                if (actividad != null)
                {
                    var resp = new ActividadSvc().saveActividad(ref actividad);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        nuevo();
                    }

                    return resp;
                }
                else
                {
                    return new OperationResult(ResultTypes.warning, "OCURRIO UN ERROR AL LEER LA INFORMACIÓN DE LA PANTALLA.");
                }

            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private OperationResult validar()
        {
            try
            {
                OperationResult operation = new OperationResult() { valor = 0, mensaje = "PARA CONTINUAR SE REQUIERE:" + Environment.NewLine };
                if (string.IsNullOrEmpty(txtNombreActividad.Text.Trim()))
                {
                    operation.valor = 2;
                    operation.mensaje += "  *   Proporcionar el nombre de la actividad" + Environment.NewLine;
                }
                //if (!chcLavadero.IsChecked.Value && !chcLlantera.IsChecked.Value && ! chcTaller.IsChecked.Value)
                //{
                //    operation.valor = 2;
                //    operation.mensaje += "  *   Elegir al menos un area de trabajo" + Environment.NewLine;
                //}
                return operation;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public override void buscar()
        {
            try
            {
                Actividad actividad = new selectActividades().getActividad();
                if (actividad != null)
                {
                    mapForm(actividad);
                    base.buscar();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new ActividadSvc().getAllActividades();
                if (resp.typeResult == ResultTypes.success)
                {
                    new ReportView().exportarListaActividades(resp.result as List<Actividad>);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
