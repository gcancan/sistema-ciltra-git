﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para ReporteMovimientoLlantas.xaml
    /// </summary>
    public partial class ReporteMovimientoLlantas : ViewBase
    {
        public ReporteMovimientoLlantas()
        {
            InitializeComponent();
        }
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.lvl.ItemsSource = null;
                base.Cursor = Cursors.Wait;
                OperationResult resp = new ReporteMovimientoLlantasSvc().getMovimientoLlantas(this.ctrFechas.fechaInicial, this.ctrFechas.fechaFinal, (from s in this.cbxLlantas.SelectedItems.Cast<Llanta>().ToList<Llanta>() select s.idLlanta.ToString()).ToList<string>());
                if (resp.typeResult == ResultTypes.success)
                {
                    PropertyGroupDescription description;
                    List<ReporteMovimientoLlanta> source = resp.result as List<ReporteMovimientoLlanta>;
                    this.txtNoLlantas.Text = (from s in source group s by s.idLlanta.ToString()).Count<IGrouping<string, ReporteMovimientoLlanta>>().ToString("N0");
                    this.txtNoMovimientos.Text = source.Count<ReporteMovimientoLlanta>().ToString("N0");
                    this.txtProfMin.Text = source.Min<ReporteMovimientoLlanta>(s => s.profundidad).ToString("N0");
                    this.txtProfMax.Text = source.Max<ReporteMovimientoLlanta>(s => s.profundidad).ToString("N0");
                    TimeSpan span = (TimeSpan)(this.ctrFechas.fechaFinal - this.ctrFechas.fechaInicial);
                    int days = span.Days;
                    this.txtDias.Text = (days == 1) ? "1" : (days + 1).ToString("N0");
                    this.lvl.ItemsSource = source;
                    CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvl.ItemsSource);
                    defaultView.GroupDescriptions.Clear();
                    if (this.rbnUnidad.IsChecked.Value)
                    {
                        description = new PropertyGroupDescription("folioLlanta");
                        defaultView.GroupDescriptions.Add(description);
                    }
                    else
                    {
                        description = new PropertyGroupDescription("tipoDocumento");
                        defaultView.GroupDescriptions.Add(description);
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void cargarLlantas()
        {
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult resp = new LlantaSvc().getLlantasByIdEmpresa((from s in this.cbxEmpresa.SelectedItems.Cast<Empresa>().ToList<Empresa>() select s.clave.ToString()).ToList<string>());
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Llanta> list = resp.result as List<Llanta>;
                    this.cbxLlantas.ItemsSource = list;
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void CbxEmpresa_ItemSelectionChanged(object sender, ItemSelectionChangedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                this.cbxLlantas.ItemsSource = null;
                this.chcTodosLlantas.IsChecked = false;
                this.cbxLlantas.IsEnabled = true;
                this.cbxLlantas.SelectedItems.Clear();
                this.cargarLlantas();
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void chc_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckBox box;
                base.Cursor = Cursors.Wait;
                if (sender != null)
                {                    
                    box = sender as CheckBox;

                    switch (box.Name)
                    {
                        case "chcTodosEmpresa":
                            if (this.cbxEmpresa.ItemsSource != null)
                            {
                                if (box.IsChecked.Value)
                                {
                                    //foreach (object obj2 in this.cbxEmpresa.ItemsSource)
                                    //{
                                    //    this.cbxEmpresa.SelectedItems.Add(obj2);
                                    //}
                                    cbxEmpresa.SelectAll();
                                    this.cbxEmpresa.IsEnabled = false;
                                }
                                else
                                {
                                    this.cbxEmpresa.SelectedItems.Clear();
                                    this.cbxEmpresa.IsEnabled = true;
                                }
                                this.cargarLlantas();
                            }
                            break;

                        case "chcTodosLlantas":
                            goto Label_0108;
                    }
                }
                return;
                Label_0108:
                if (this.cbxLlantas.ItemsSource != null)
                {
                    if (box.IsChecked.Value)
                    {
                        //foreach (object obj3 in this.cbxLlantas.ItemsSource)
                        //{
                        //    this.cbxLlantas.SelectedItems.Add(obj3);
                        //}
                        cbxLlantas.SelectAll();
                        this.cbxLlantas.IsEnabled = false;
                    }
                    else
                    {
                        this.cbxLlantas.SelectedItems.Clear();
                        this.cbxLlantas.IsEnabled = true;
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void rbn_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                CollectionView defaultView;
                PropertyGroupDescription description;
                base.Cursor = Cursors.Wait;
                if ((sender != null) && (this.lvl.ItemsSource != null))
                {
                    defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvl.ItemsSource);
                    defaultView.GroupDescriptions.Clear();
                    RadioButton button = sender as RadioButton;
                    switch (button.Name)
                    {
                        case "rbnUnidad":
                            description = new PropertyGroupDescription("folioLlanta");
                            defaultView.GroupDescriptions.Add(description);
                            break;

                        case "rbnMovimiento":
                            goto Label_009D;
                    }
                }
                return;
                Label_009D:
                description = new PropertyGroupDescription("tipoDocumento");
                defaultView.GroupDescriptions.Add(description);
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                this.cbxEmpresa.ItemSelectionChanged += new ItemSelectionChangedEventHandler(this.CbxEmpresa_ItemSelectionChanged);
                OperationResult result = new EmpresaSvc().getEmpresasALLorById(0);
                if (result.typeResult == ResultTypes.success)
                {
                    this.cbxEmpresa.ItemsSource = result.result as List<Empresa>;
                }
                this.rbnUnidad.IsChecked = true;
                base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
    }
}
