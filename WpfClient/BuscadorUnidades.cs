﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfClient
{
    static class BuscadorUnidades
    {
        public static  List<UnidadTransporte> buscarRemolques(int idEmpresa, string clave)
        {
            OperationResult resp = new UnidadTransporteSvc().buscarRemolques(idEmpresa, clave);
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    return (List<UnidadTransporte>)resp.result;
                case ResultTypes.error:
                    ImprimirMensaje.imprimir(resp);
                    return null;
                case ResultTypes.warning:
                    ImprimirMensaje.imprimir(resp);
                    return null;
                case ResultTypes.recordNotFound:
                    return new List<UnidadTransporte>();

                default:
                    return null;
            }
        }

        public static List<UnidadTransporte> buscarTractores(string clave, int idEmpresa = 0)
        {
            OperationResult resp = new UnidadTransporteSvc().getUnidadesALLorById(idEmpresa, clave);
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    return (List<UnidadTransporte>)resp.result;
                case ResultTypes.error:
                    ImprimirMensaje.imprimir(resp);
                    return null;
                case ResultTypes.warning:
                    ImprimirMensaje.imprimir(resp);
                    return null;
                case ResultTypes.recordNotFound:
                    return new List<UnidadTransporte>();

                default:
                    return null;
            }
        }
    }
}
