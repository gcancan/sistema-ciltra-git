﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using Core.Utils;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Forms;
    using System.Windows.Input;
    using System.Windows.Markup;
    using Xceed.Wpf.Toolkit;

    public partial class editCartaPorteAtlante : WpfClient.ViewBase
    {
        private int claveEmpresa = 2;
        private List<Origen> _listOrigen = new List<Origen>();
        private List<Producto> _lisProducto = new List<Producto>();
        private List<Zona> _listZonas = new List<Zona>();
        private string archivo = (System.Windows.Forms.Application.StartupPath + @"\config.ini");

        public editCartaPorteAtlante()
        {
            this.InitializeComponent();
        }
        private List<CartaPorte> agrupar(List<CartaPorte> listDetalles)
        {
            AgruparCartaPortes portes = new AgruparCartaPortes(listDetalles, false);
            return portes._listaAgrupada;
        }

        private void btnCambiarImpresora_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.PrintDialog dialog = new System.Windows.Forms.PrintDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                new Core.Utils.Util().setRutasImpresora("VIAJES_ATLANTE", dialog.PrinterSettings.PrinterName);
            }
        }

        private void btnNewDetalle_Click(object sender, RoutedEventArgs e)
        {
            if ((this.cbxClientes.SelectedItem != null) && (ctrRemolque1.unidadTransporte != null))// ((this.cbxClientes.SelectedItem != null) && (this.txtTolva1.Tag != null))
            {
                this.mapDetalles();
            }
        }

        private void buscarDestinos()
        {
            try
            {
                if (this.txtOrigen.Tag != null)
                {
                    OperationResult resp = new ZonasSvc().getDestinosByOrigenAndIdCliente(((Cliente)this.cbxClientes.SelectedItem).clave, ((Origen)this.txtOrigen.Tag).idOrigen);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        this._listZonas = (List<Zona>)resp.result;
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private List<Personal> buscarPersonal(string nombre)
        {
            OperationResult result = new OperadorSvc().getOperadoresByNombre(nombre, this.claveEmpresa);
            switch (result.typeResult)
            {
                case ResultTypes.success:
                    return (List<Personal>)result.result;

                case ResultTypes.error:
                    System.Windows.MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
                    return null;

                case ResultTypes.warning:
                    System.Windows.MessageBox.Show(result.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return null;

                case ResultTypes.recordNotFound:
                    return new List<Personal>();
            }
            return null;
        }

        private List<UnidadTransporte> buscarUnidades(string clave)
        {
            OperationResult result = new UnidadTransporteSvc().getUnidadesALLorById(this.claveEmpresa, clave);
            switch (result.typeResult)
            {
                case ResultTypes.success:
                    return (List<UnidadTransporte>)result.result;

                case ResultTypes.error:
                    System.Windows.MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
                    return null;

                case ResultTypes.warning:
                    System.Windows.MessageBox.Show(result.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return null;

                case ResultTypes.recordNotFound:
                    return new List<UnidadTransporte>();
            }
            return null;
        }

        private void buscarViaje(int numGuiaId)
        {
            try
            {
                OperationResult result = new CartaPorteSvc().getCartaPorteById(numGuiaId);
                if (result.typeResult == ResultTypes.success)
                {
                    this.mapForm(((List<Viaje>)result.result)[0]);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void buscarViajesActivos(UnidadTransporte unidadTransporte)
        {
            try
            {
                OperationResult result = new CartaPorteSvc().getViajesActivosByIdTractor(unidadTransporte.clave);
                if (result.typeResult == ResultTypes.success)
                {
                    new avisoViajesActivos().mostrarLista(result.result as List<Viaje>, unidadTransporte.clave, base.mainWindow.inicio._usuario.personal.nombre);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void cbxClientes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                base.Cursor = System.Windows.Input.Cursors.Wait;
                _listOrigen = new List<Origen>();

                if (sender != null)
                {
                    System.Windows.Controls.ComboBox combo = sender as System.Windows.Controls.ComboBox;
                    if (combo.SelectedItem == null) return;


                    Cliente clienteSelect = (Cliente)combo.SelectedItem;


                    OperationResult respOrigenes = new OrigenSvc().getOrigenesAll(clienteSelect.clave, 0);
                    if (respOrigenes.typeResult == ResultTypes.success)
                    {
                        this._listOrigen = (List<Origen>)respOrigenes.result;
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(respOrigenes);
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }

        private void chcBoxImprimir_Checked(object sender, RoutedEventArgs e)
        {
            new Core.Utils.Util().Write("IMPRIMIR", "ACTIVO", "true", this.archivo);
        }

        private void chcBoxImprimir_Unchecked(object sender, RoutedEventArgs e)
        {
            new Core.Utils.Util().Write("IMPRIMIR", "ACTIVO", "false", this.archivo);
        }

        private void chcBoxValeSalida_Checked(object sender, RoutedEventArgs e)
        {
            this.chcBoxOrdenGasolina.IsEnabled = true;
            this.chcBoxOrdenGasolina.IsChecked = true;
        }

        private void chcBoxValeSalida_Unchecked(object sender, RoutedEventArgs e)
        {
            this.chcBoxOrdenGasolina.IsEnabled = false;
            this.chcBoxOrdenGasolina.IsChecked = false;
        }

        private void completarClave(ref string clave)
        {
            if (clave.Length < 3)
            {
                for (int i = 0; i <= (3 - clave.Length); i++)
                {
                    clave = "0" + clave;
                }
            }
        }

        public static Viaje DeepClone<Viaje>(Viaje obj)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, obj);
                stream.Position = 0L;
                return (Viaje)formatter.Deserialize(stream);
            }
        }

        private void envioCorreo(Viaje viaje)
        {
            EnvioCorreo correo = new EnvioCorreo();
            if (new Core.Utils.Util().isActivoCorreoGranjas())
            {
                string str = correo.envioCorreoDeCartaPorte(viaje);
            }
            if (new Core.Utils.Util().isActivoCorreoCSI())
            {
                string str2 = correo.envioCorreoCSI(viaje);
            }
        }

        private bool establecerImpresora()
        {
            try
            {
                if (string.IsNullOrEmpty(new Core.Utils.Util().getRutasImpresora("VIAJES_ATLANTE")))
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "Se requiere seleccionar la impresora"
                    };
                    ImprimirMensaje.imprimir(resp);
                    System.Windows.Forms.PrintDialog dialog = new System.Windows.Forms.PrintDialog();
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        new Core.Utils.Util().setRutasImpresora("VIAJES_ATLANTE", dialog.PrinterSettings.PrinterName);
                        return this.establecerImpresora();
                    }
                    return false;
                }
                return true;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return false;
            }
        }

        private TipoPrecio establecerPrecio()
        {
            UnidadTransporte tag = ctrRemolque1.unidadTransporte; // (UnidadTransporte)this.txtTolva1.Tag;
            if (((Cliente)this.cbxClientes.SelectedItem).clave == 3)
            {
                if ((ctrDolly.unidadTransporte != null) || (ctrRemolque2.unidadTransporte != null))//((this.txtDolly.Tag != null) || (this.txtTolva2.Tag != null))
                {
                    return TipoPrecio.FULL;
                }
                return TipoPrecio.SENCILLO;
            }
            if ((ctrDolly.unidadTransporte != null) || (ctrRemolque2.unidadTransporte != null))//((this.txtDolly.Tag != null) || (this.txtTolva2.Tag != null))
            {
                return TipoPrecio.FULL;
            }
            return TipoPrecio.SENCILLO;
        }

        public override OperationResult guardar()
        {
            OperationResult result2;
            try
            {
                base.Cursor = System.Windows.Input.Cursors.Wait;
                OperationResult result = this.validar();
                if (result.typeResult > ResultTypes.success)
                {
                    return result;
                }
                Viaje viaje = this.mapForm();
                if (viaje != null)
                {
                    string str = new Core.Utils.Util().getRutaXML();
                    if (string.IsNullOrEmpty(str))
                    {
                        return new OperationResult
                        {
                            valor = 2,
                            mensaje = "Para proceder se requiere la ruta del archivo XML"
                        };
                    }
                    ComercialCliente cli = new ComercialCliente
                    {
                        nombre = ((Cliente)this.cbxClientes.SelectedItem).nombre,
                        calle = ((Cliente)this.cbxClientes.SelectedItem).domicilio,
                        rfc = ((Cliente)this.cbxClientes.SelectedItem).rfc,
                        colonia = ((Cliente)this.cbxClientes.SelectedItem).colonia,
                        estado = ((Cliente)this.cbxClientes.SelectedItem).estado,
                        municipio = ((Cliente)this.cbxClientes.SelectedItem).municipio,
                        pais = ((Cliente)this.cbxClientes.SelectedItem).pais,
                        telefono = ((Cliente)this.cbxClientes.SelectedItem).telefono,
                        cp = ((Cliente)this.cbxClientes.SelectedItem).cp
                    };
                    ComercialEmpresa emp = new ComercialEmpresa
                    {
                        nombre = base.mainWindow.inicio._usuario.personal.empresa.nombre,
                        calle = base.mainWindow.inicio._usuario.personal.empresa.direccion,
                        rfc = base.mainWindow.inicio._usuario.personal.empresa.rfc,
                        colonia = base.mainWindow.inicio._usuario.personal.empresa.colonia,
                        estado = base.mainWindow.inicio._usuario.personal.empresa.estado,
                        municipio = base.mainWindow.inicio._usuario.personal.empresa.municipio,
                        pais = base.mainWindow.inicio._usuario.personal.empresa.pais,
                        telefono = base.mainWindow.inicio._usuario.personal.empresa.telefono,
                        cp = base.mainWindow.inicio._usuario.personal.empresa.cp
                    };
                    DatosFacturaXML datosFactura = ObtenerDatosFactura.obtenerDatos(str);
                    if (datosFactura == null)
                    {
                        return new OperationResult
                        {
                            valor = 1,
                            mensaje = "Ocurrio un error al intentar obtener datos del XML"
                        };
                    }
                    int ordenTrabajo = 0;
                    OperationResult result3 = new CartaPorteSvc().saveCartaPorte_V2(ref viaje, ref ordenTrabajo, false, this.chcBoxValeSalida.IsChecked.Value, this.chcBoxOrdenGasolina.IsChecked.Value);
                    if (result3.typeResult == ResultTypes.success)
                    {
                        ProcesosViaje.procesosViaje(cli, emp, datosFactura, viaje, ordenTrabajo, this.chBoxViajeExterno.IsChecked.Value, this.chcVales.IsChecked.Value, base.mainWindow, this.numValesCarga.Value.Value, this.chcBoxValeSalida.IsChecked.Value);
                        this.buscarViaje(viaje.NumGuiaId);
                        base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                        txtFolio.Text = viaje.NumGuiaId.ToString();
                        return result3;
                    }
                    return result3;
                }
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = "ERROR AL CONSTRUIR LA INFORMACION"
                };
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
            return result2;
        }
        private void limpiarDetalles()
        {
            this.mapDestinos(null);
            this.mapProductos(null);
            this.mapOrigenes(null);
            this.txtRemision.Clear();
            this.txtCantidad.Clear();
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            this.lvlProductos.Items.Remove(this.lvlProductos.SelectedItem);
        }

        private void mapCamposDolly(UnidadTransporte unidadTransporte, string clave = "")
        {
            if (unidadTransporte != null)
            {
                //this.txtDolly.Text = clave;
                //this.txtDolly.Tag = unidadTransporte;
                //this.txtDolly.IsEnabled = false;
                ctrDolly.unidadTransporte = unidadTransporte;
                //this.txtTolva2.Focus();
                ctrRemolque2.txtUnidadTrans.Focus();
            }
            else
            {
                //this.txtDolly.Text = "";
                //this.txtDolly.Tag = null;
                //this.txtDolly.IsEnabled = true;
                ctrDolly.unidadTransporte = null;
            }
        }

        private void mapCamposTolva1(UnidadTransporte unidadTransporte, string clave = "")
        {
            if (unidadTransporte != null)
            {
                //this.txtTolva1.Text = clave;
                //this.txtTolva1.Tag = unidadTransporte;
                //this.txtTolva1.IsEnabled = false;
                ctrRemolque1.unidadTransporte = unidadTransporte;
                //this.txtDolly.Focus();
                ctrDolly.txtUnidadTrans.Focus();
                if (!unidadTransporte.tipoUnidad.descripcion.Contains("CAJA VOLTEO"))
                {
                    this.numValesCarga.Value = 0;
                    this.numValesCarga.IsEnabled = false;
                }
                else
                {
                    this.numValesCarga.IsEnabled = true;
                }
            }
            else
            {
                //this.txtTolva1.Text = "";
                //this.txtTolva1.Tag = null;
                //this.txtTolva1.IsEnabled = true;
                ctrRemolque1.unidadTransporte = null;
                this.numValesCarga.IsEnabled = true;
            }
        }

        private void mapCampostolva2(UnidadTransporte unidadTransporte, string clave = "")
        {
            if (unidadTransporte != null)
            {
                //this.txtTolva2.Text = clave;
                //this.txtTolva2.Tag = unidadTransporte;
                //this.txtTolva2.IsEnabled = false;
                ctrRemolque2.unidadTransporte = unidadTransporte;
                this.txtChofer.Focus();
            }
            else
            {
                //this.txtTolva2.Text = "";
                //this.txtTolva2.Tag = null;
                //this.txtTolva2.IsEnabled = true;
                ctrRemolque2.unidadTransporte = null;
            }
        }

        private void mapCamposTractor(UnidadTransporte unidadTransporte, string clave = "")
        {
            if (unidadTransporte != null)
            {
                //this.txtTractor.Text = unidadTransporte.clave;
                //this.txtTractor.Tag = unidadTransporte;
                //this.txtTractor.IsEnabled = false;
                ctrTractor.unidadTransporte = unidadTransporte;
                ctrRemolque1.txtUnidadTrans.Focus(); //this.txtTolva1.Focus();
                this.buscarViajesActivos(unidadTransporte);
            }
            else
            {
                //this.txtTractor.Text = "";
                //this.txtTractor.Tag = null;
                //this.txtTractor.IsEnabled = true;
                ctrTractor.unidadTransporte = null;
            }
        }

        private void mapChofer(Personal personal)
        {
            if (personal != null)
            {
                this.txtChofer.Text = personal.nombre;
                this.txtChofer.Tag = personal;
                this.txtChofer.IsEnabled = false;
                this.txtRemision.Focus();
            }
            else
            {
                this.txtChofer.Text = "";
                this.txtChofer.Tag = null;
                this.txtChofer.IsEnabled = true;
            }
        }

        private void mapChoferAuxiliar(Personal personal)
        {
            if (personal != null)
            {
                this.txtChoferAuxiliar.Text = personal.nombre;
                this.txtChoferAuxiliar.Tag = personal;
                this.txtChoferAuxiliar.IsEnabled = false;
                this.txtRemision.Focus();
            }
            else
            {
                this.txtChoferAuxiliar.Text = "";
                this.txtChoferAuxiliar.Tag = null;
                this.txtChoferAuxiliar.IsEnabled = true;
            }
        }

        private void mapComboClientes()
        {
            try
            {
                cbxClientes.Items.Clear();
                OperationResult resp = new ClienteSvc().getClientesALLorById(claveEmpresa, 0, true);
                if (resp.typeResult == ResultTypes.success)
                {
                    foreach (Cliente cliente in (List<Cliente>)resp.result)
                    {
                        this.cbxClientes.Items.Add(cliente);
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void mapDestinos(Zona zona)
        {
            if (zona != null)
            {
                this.txtDestino.Tag = zona;
                this.txtDestino.Text = zona.baseNombre;
                this.txtDestino.IsEnabled = false;
                this.txtProducto.Focus();

                Cliente selectedItem = cbxClientes.SelectedItem as Cliente;
                OperationResult resp = new FraccionSvc().getFraccionALLorById(selectedItem.clave, zona.clasificacion.idClasificacionProducto);
                if (resp.typeResult == ResultTypes.success)
                {
                    this._lisProducto = ((List<Fraccion>)resp.result)[0].listProductos;
                }
                else
                {
                    this._lisProducto = new List<Producto>();
                    ImprimirMensaje.imprimir(resp);
                }
            }
            else
            {
                this.txtDestino.Tag = null;
                this.txtDestino.Clear();
                this.txtDestino.IsEnabled = true;
            }
        }

        private void mapDetalles()
        {
            CartaPorte porte = new CartaPorte
            {
                producto = (Producto)this.txtProducto.Tag,
                zonaSelect = (Zona)this.txtDestino.Tag,
                origen = (Origen)this.txtOrigen.Tag,
                PorcIVA = ((Cliente)this.cbxClientes.SelectedItem).impuesto.valor,
                PorcRetencion = ((Cliente)this.cbxClientes.SelectedItem).impuestoFlete.valor,
                tipoPrecio = EstablecerPrecios.establecerPrecio(ctrRemolque1.unidadTransporte, ctrDolly.unidadTransporte, ctrRemolque2.unidadTransporte, ctrEmpresa.empresaSelected, null),
                tipoPrecioChofer = EstablecerPrecios.establecerPrecioChofer(ctrRemolque1.unidadTransporte, ctrDolly.unidadTransporte, ctrRemolque2.unidadTransporte, ctrEmpresa.empresaSelected),
                remision = this.txtRemision.Text.Trim(),
                volumenDescarga = Convert.ToDecimal(this.txtCantidad.Text.Trim()),
                viaje = ((Zona)this.txtDestino.Tag).viaje
            };
            System.Windows.Controls.ListViewItem newItem = new System.Windows.Controls.ListViewItem
            {
                Content = porte
            };
            newItem.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
            this.lvlProductos.Items.Add(newItem);
            this.limpiarDetalles();
            this.txtOrigen.Focus();
        }

        private Viaje mapForm()
        {
            try
            {
                Viaje viaje = new Viaje
                {
                    NumGuiaId = (this.txtFolio.Tag == null) ? 0 : ((Viaje)this.txtFolio.Tag).NumGuiaId,
                    EstatusGuia = this.chBoxViajeExterno.IsChecked.Value ? "EXTERNO" : "ACTIVO",
                    NumViaje = (this.txtFolio.Text == "") ? 0 : Convert.ToInt32(this.txtFolio.Text.Trim()),
                    cliente = (Cliente)this.cbxClientes.SelectedItem,
                    operador = (Personal)this.txtChofer.Tag,
                    operadorCapacitacion = (Personal)this.txtChoferAuxiliar.Tag,
                    IdEmpresa = ctrEmpresa.empresaSelected.clave,
                    tractor = ctrTractor.unidadTransporte, // (UnidadTransporte)this.txtTractor.Tag,
                    remolque = ctrRemolque1.unidadTransporte, //(UnidadTransporte)this.txtTolva1.Tag,
                    dolly = ctrDolly.unidadTransporte,// (this.txtDolly.Tag == null) ? null : ((UnidadTransporte)this.txtDolly.Tag),
                    remolque2 = ctrRemolque2.unidadTransporte,// (this.txtTolva2.Tag == null) ? null : ((UnidadTransporte)this.txtTolva2.Tag),
                    SerieGuia = "B",
                    FechaHoraViaje = this.txtFecha2.Value.Value,
                    UserViaje = base.mainWindow.inicio._usuario.idUsuario,
                    tipoViaje = this.validarIsFull() ? 'F' : 'S',
                    km = decimal.Zero,
                    isExterno = this.chBoxViajeExterno.IsChecked.Value,
                    zonaOperativa = ctrZonaOperativa.zonaOperativa
                };
                viaje.listDetalles = new List<CartaPorte>();
                foreach (System.Windows.Controls.ListViewItem item in (IEnumerable)this.lvlProductos.Items)
                {
                    viaje.listDetalles.Add((CartaPorte)item.Content);
                }
                return viaje;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void mapForm(Viaje viaje)
        {
            if (viaje != null)
            {
                this.txtFolio.Tag = viaje;
                this.txtFolio.Text = viaje.NumGuiaId.ToString();
                this.txtFecha2.Value = new DateTime?(viaje.FechaHoraViaje);
                this.lvlProductos.Items.Clear();
                foreach (CartaPorte porte in viaje.listDetalles)
                {
                    System.Windows.Controls.ListViewItem newItem = new System.Windows.Controls.ListViewItem
                    {
                        Content = porte
                    };
                    this.lvlProductos.Items.Add(newItem);
                }
                this.mapChofer(viaje.operador);
                this.mapChoferAuxiliar(viaje.operadorCapacitacion);
            }
            else
            {
                this.txtFolio.Tag = null;
                this.txtFolio.Clear();
                this.txtFecha2.Value = new DateTime?(DateTime.Now);
                this.lvlProductos.Items.Clear();
                this.mapCamposTractor(null, "");
                this.mapCamposTolva1(null, "");
                this.mapCampostolva2(null, "");
                this.mapCamposDolly(null, "");
                this.mapChofer(null);
                this.mapChoferAuxiliar(null);
            }
        }

        private void mapOrigenes(Origen origen)
        {
            if (origen != null)
            {
                this.txtOrigen.Tag = origen;
                this.txtOrigen.Text = origen.baseNombre;
                this.txtOrigen.IsEnabled = false;
                this.txtDestino.Focus();
                this.buscarDestinos();
            }
            else
            {
                this.txtOrigen.Tag = null;
                this.txtOrigen.Clear();
                this.txtOrigen.IsEnabled = true;
            }
        }

        private void mapProductos(Producto producto)
        {
            if (producto != null)
            {
                this.txtProducto.Tag = producto;
                this.txtProducto.Text = producto.descripcion;
                this.txtProducto.IsEnabled = false;
                this.txtCantidad.Focus();
                if (!string.IsNullOrEmpty(this.txtCantidad.Text))
                {
                    this.txtCantidad.Select(0, this.txtCantidad.Text.Length);
                }
                this.lblCantidad.Content = "Cantidad " + producto.clave_unidad_de_medida;
            }
            else
            {
                this.txtProducto.Tag = null;
                this.txtProducto.Clear();
                this.txtProducto.IsEnabled = true;
                this.lblCantidad.Content = "Cantidad";
            }
        }

        public override void nuevo()
        {
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
            this.mapForm(null);
            this.limpiarDetalles();
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
            //this.txtTractor.Focus();
            ctrTractor.txtUnidadTrans.Focus();
            this.chcBoxValeSalida.IsChecked = true;
            this.chcBoxOrdenGasolina.IsEnabled = true;
            this.chcVales.IsChecked = true;
        }
        private void txtCantidad_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                decimal result = 0M;
                if (decimal.TryParse(this.txtCantidad.Text.Trim(), out result) && ((this.cbxClientes.SelectedItem != null) && (ctrRemolque1.unidadTransporte != null)))
                {
                    this.mapDetalles();
                }
            }
        }

        private void txtChofer_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string str = this.txtChofer.Text.Trim();
                if (!string.IsNullOrEmpty(str))
                {
                    List<Personal> list = this.buscarPersonal(str);
                    if (list.Count == 1)
                    {
                        this.mapChofer(list[0]);
                    }
                    else if (list.Count == 0)
                    {
                        Personal personal = new selectPersonal(str, this.claveEmpresa).buscarPersonal();
                        this.mapChofer(personal);
                    }
                    else
                    {
                        Personal personal = new selectPersonal(str, this.claveEmpresa).buscarPersonal();
                        this.mapChofer(personal);
                    }
                }
            }
        }

        private void txtChoferAuxiliar_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string str = this.txtChoferAuxiliar.Text.Trim();
                if (!string.IsNullOrEmpty(str))
                {
                    List<Personal> list = this.buscarPersonal(str);
                    if (list.Count == 1)
                    {
                        this.mapChoferAuxiliar(list[0]);
                    }
                    else if (list.Count == 0)
                    {
                        Personal personal = new selectPersonal(str, this.claveEmpresa).buscarPersonal();
                        this.mapChoferAuxiliar(personal);
                    }
                    else
                    {
                        Personal personal = new selectPersonal(str, this.claveEmpresa).buscarPersonal();
                        this.mapChoferAuxiliar(personal);
                    }
                }
            }
        }

        private void txtDestino_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && (this._listZonas.Count > 0))
            {
                List<Zona> list = new List<Zona>();
                List<Zona> list2 = this._listZonas.FindAll(S => S.descripcion.IndexOf(this.txtDestino.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (Zona zona in list2)
                {
                    list.Add(zona);
                }
                selectDestinos destinos = new selectDestinos(this.txtDestino.Text);
                if (list.Count == 1)
                {
                    this.mapDestinos(list[0]);
                }
                else
                {
                    Zona zona2 = destinos.buscarZona(this._listZonas);
                    this.mapDestinos(zona2);
                }
            }
        }

        private void txtDolly_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string str = string.Empty; //this.txtDolly.Text.Trim();
                if (string.IsNullOrEmpty(str))
                {
                    this.txtChofer.Focus();
                }
                else
                {
                    this.completarClave(ref str);
                    List<UnidadTransporte> list = this.buscarUnidades("DL" + str);
                    if (list.Count == 1)
                    {
                        if (this.validarDolly(list[0]))
                        {
                            this.mapCamposDolly(list[0], list[0].clave);
                        }
                    }
                    else if (list.Count == 0)
                    {
                        UnidadTransporte unidadTransporte = new selectUnidadTransporte("DL", this.claveEmpresa, str).buscarUnidades();
                        this.mapCamposDolly(unidadTransporte, (unidadTransporte == null) ? "" : unidadTransporte.clave);
                    }
                    else
                    {
                        UnidadTransporte unidadTransporte = new selectUnidadTransporte(list).selectUnidadTransporteFind();
                        this.mapCamposDolly(unidadTransporte, (unidadTransporte == null) ? "" : unidadTransporte.clave);
                    }
                }
            }
        }

        private void txtOrigen_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                List<Origen> list = new List<Origen>();
                if (this._listOrigen.Count > 0)
                {
                    List<Origen> list2 = this._listOrigen.FindAll(S => S.baseNombre.IndexOf(this.txtOrigen.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                    foreach (Origen origen in list2)
                    {
                        list.Add(origen);
                    }
                    selectOrigenes origenes = new selectOrigenes(this.txtOrigen.Text);
                    if (list.Count == 1)
                    {
                        this.mapOrigenes(list[0]);
                    }
                    else
                    {
                        Origen origen2 = origenes.buscarOrigenes(this._listOrigen);
                        this.mapOrigenes(origen2);
                    }
                }
            }
        }

        private void txtProducto_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                List<Producto> list = new List<Producto>();
                if (this._lisProducto.Count > 0)
                {
                    foreach (Producto producto2 in this._lisProducto)
                    {
                        if (string.IsNullOrEmpty(producto2.descripcion))
                        {
                        }
                    }
                    if (!string.IsNullOrEmpty(this.txtProducto.Text))
                    {
                        List<Producto> list2 = this._lisProducto.FindAll(S => S.descripcion.IndexOf(this.txtProducto.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                        foreach (Producto producto3 in list2)
                        {
                            list.Add(producto3);
                        }
                    }
                    selectProducto producto = new selectProducto(this.txtProducto.Text);
                    if (list.Count == 1)
                    {
                        this.mapProductos(list[0]);
                    }
                    else
                    {
                        Producto producto4 = producto.buscarProductos(this._lisProducto);
                        this.mapProductos(producto4);
                    }
                }
            }
        }

        private void txtRemision_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.txtOrigen.Focus();
            }
        }

        private void txtTolva1_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string str = string.Empty; // this.txtTolva1.Text.Trim();
                if (!string.IsNullOrEmpty(str))
                {
                    this.completarClave(ref str);
                    List<UnidadTransporte> list = BuscadorUnidades.buscarRemolques(this.claveEmpresa, str);
                    if (list.Count == 1)
                    {
                        this.mapCamposTolva1(list[0], list[0].clave);
                    }
                    else if (list.Count == 0)
                    {
                        UnidadTransporte unidadTransporte = new selectUnidadTransporte("", this.claveEmpresa, "").buscarRemolques();
                        this.mapCamposTolva1(unidadTransporte, (unidadTransporte == null) ? "" : unidadTransporte.clave);
                    }
                    else
                    {
                        UnidadTransporte unidadTransporte = new selectUnidadTransporte(list).selectUnidadTransporteFind();
                        this.mapCamposTolva1(unidadTransporte, (unidadTransporte == null) ? "" : unidadTransporte.clave);
                    }
                }
            }
        }

        private void txtTolva2_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string str = string.Empty;//this.txtTolva2.Text.Trim();
                if (string.IsNullOrEmpty(str))
                {
                    this.txtChofer.Focus();
                }
                else
                {
                    this.completarClave(ref str);
                    List<UnidadTransporte> list = BuscadorUnidades.buscarRemolques(this.claveEmpresa, str);
                    if (list.Count == 1)
                    {
                        this.mapCampostolva2(list[0], list[0].clave);
                    }
                    else if (list.Count == 0)
                    {
                        UnidadTransporte unidadTransporte = new selectUnidadTransporte("", this.claveEmpresa, "").buscarRemolques();
                        this.mapCampostolva2(unidadTransporte, (unidadTransporte == null) ? "" : unidadTransporte.clave);
                    }
                    else
                    {
                        UnidadTransporte unidadTransporte = new selectUnidadTransporte(list).selectUnidadTransporteFind();
                        this.mapCampostolva2(unidadTransporte, (unidadTransporte == null) ? "" : unidadTransporte.clave);
                    }
                }
            }
        }

        private void txtTractor_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string str = string.Empty;//this.txtTractor.Text.Trim();
                if (!string.IsNullOrEmpty(str))
                {
                    this.completarClave(ref str);
                    List<UnidadTransporte> list = this.buscarUnidades("TC" + str);
                    if (list.Count == 1)
                    {
                        if (this.validarTractor(list[0]))
                        {
                            this.mapCamposTractor(list[0], list[0].clave);
                        }
                    }
                    else if (list.Count == 0)
                    {
                        UnidadTransporte unidadTransporte = new selectUnidadTransporte("TC", this.claveEmpresa, str).buscarUnidades();
                        this.mapCamposTractor(unidadTransporte, (unidadTransporte == null) ? "" : unidadTransporte.clave);
                    }
                    else
                    {
                        UnidadTransporte unidadTransporte = new selectUnidadTransporte(list).selectUnidadTransporteFind();
                        this.mapCamposTractor(unidadTransporte, (unidadTransporte == null) ? "" : unidadTransporte.clave);
                    }
                }
            }
        }

        private OperationResult validar()
        {
            bool flag = true;
            List<string> list = new List<string>();
            if (this.cbxClientes.SelectedItem == null)
            {
                flag = false;
                list.Add("Elegir un Cliente." + Environment.NewLine);
            }
            if (this.ctrTractor.unidadTransporte == null)//if (this.txtTractor.Tag == null)
            {
                flag = false;
                list.Add("Elegir el tractor." + Environment.NewLine);
            }
            if (ctrRemolque1.unidadTransporte == null)// (this.txtTolva1.Tag == null)
            {
                flag = false;
                list.Add("elegir el remolque 1." + Environment.NewLine);
            }
            if (this.txtChofer.Tag == null)
            {
                flag = false;
                list.Add("Elegir el chofer." + Environment.NewLine);
            }
            if ((ctrDolly.unidadTransporte != null) && (ctrRemolque2.unidadTransporte == null))//((this.txtDolly.Tag != null) && (this.txtTolva2.Tag == null))
            {
                flag = false;
                list.Add("Seleccionar el remolque 2." + Environment.NewLine);
            }
            if ((ctrRemolque2.unidadTransporte != null) && (ctrDolly.unidadTransporte == null))//((this.txtTolva2.Tag != null) && (this.txtDolly.Tag == null))
            {
                flag = false;
                list.Add("Seleccionar Dolly." + Environment.NewLine);
            }
            if (this.validarLista())
            {
                flag = false;
                list.Add("Existen errores en la lista, o faltan proporcionar valores." + Environment.NewLine);
            }
            string str = "Para continuar se requiere corregir lo siguiente:" + Environment.NewLine;
            foreach (string str2 in list)
            {
                str = str + str2;
            }
            return new OperationResult
            {
                valor = new int?(flag ? 0 : 2),
                result = flag,
                mensaje = str
            };
        }

        private bool validarDolly(UnidadTransporte s) =>
            (s.tipoUnidad.clasificacion.ToUpper() == "DOLLY");

        private bool validarIsFull()
        {
            UnidadTransporte tag = ctrRemolque1.unidadTransporte; // (UnidadTransporte)this.txtTolva1.Tag;
            return ((ctrDolly.unidadTransporte != null) || (ctrRemolque2.unidadTransporte != null));// ((this.txtDolly.Tag != null) || (this.txtTolva2.Tag != null));
        }

        private bool validarLista()
        {
            if (this.lvlProductos.Items.Count <= 0)
            {
                return true;
            }
            foreach (System.Windows.Controls.ListViewItem item in (IEnumerable)this.lvlProductos.Items)
            {
                CartaPorte content = (CartaPorte)item.Content;
                if ((content.zonaSelect == null) || (content.producto == null))
                {
                    return true;
                }
            }
            return false;
        }

        private bool validarTolva1(UnidadTransporte s) =>
            (s.tipoUnidad.clasificacion.ToUpper() == "TOLVA");

        private bool validarTolva2(UnidadTransporte s)
        {
            if (s.tipoUnidad.clasificacion.ToUpper() == "TOLVA")
            {
                if (s.clave == ctrRemolque1.unidadTransporte.clave /*this.txtTolva1.Text*/)
                {
                    System.Windows.MessageBox.Show("Se esta duplicando el valor para el remolque 1");
                    return false;
                }
                return true;
            }
            return false;
        }

        private bool validarTractor(UnidadTransporte unidadTransporte) =>
            (unidadTransporte.tipoUnidad.clasificacion.ToUpper() == "TRACTOR");
        Usuario usuario = null;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!this.establecerImpresora())
                {
                    base.mainWindow.scrollContainer.IsEnabled = false;
                }
                else
                {                    
                    this.usuario = mainWindow.usuario;

                    ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
                    ctrEmpresa.loaded(this.usuario.empresa, (new PrivilegioSvc().consultarPrivilegio("isAdmin", usuario.idUsuario)).typeResult == ResultTypes.success);

                    ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
                    ctrZonaOperativa.cargarControl(usuario.zonaOperativa, usuario.idUsuario);

                    base.Cursor = System.Windows.Input.Cursors.Wait;

                    this.nuevo();
                    if (new PrivilegioSvc().consultarPrivilegio("APLICAR_FECHA_VIAJE", usuario.idUsuario).typeResult == ResultTypes.success)
                    {
                        this.txtFecha2.IsEnabled = true;
                    }
                    else
                    {
                        this.txtFecha2.IsEnabled = false;
                    }
                    this.chcBoxImprimir.IsChecked = new bool?(new Core.Utils.Util().isActivoImpresion());
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }

        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ctrTractor.loaded(etipoUniadBusqueda.TRACTOR, ctrEmpresa.empresaSelected, ctrZonaOperativa.zonaOperativa);
            ctrRemolque1.loaded(etipoUniadBusqueda.TOLVA, ctrEmpresa.empresaSelected, ctrZonaOperativa.zonaOperativa);
            ctrDolly.loaded(etipoUniadBusqueda.DOLLY, ctrEmpresa.empresaSelected, ctrZonaOperativa.zonaOperativa);
            ctrRemolque2.loaded(etipoUniadBusqueda.TOLVA, ctrEmpresa.empresaSelected, ctrZonaOperativa.zonaOperativa);
            if (ctrZonaOperativa.zonaOperativa != null)
            {
                stpUnidades.IsEnabled = true;
            }
            else
            {
                stpUnidades.IsEnabled = false;
            }
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = System.Windows.Input.Cursors.Wait;
                this.claveEmpresa = ctrEmpresa.empresaSelected.clave;
                ctrZonaOperativa.zonaOperativa = null;
                this.mapComboClientes();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }
    }
}
