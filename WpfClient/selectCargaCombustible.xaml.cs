﻿using CoreFletera.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectCargaCombustible.xaml
    /// </summary>
    public partial class selectCargaCombustible : Window
    {
        Usuario usuario = null;
        public selectCargaCombustible(Usuario usuario)
        {
            InitializeComponent();
            this.usuario = usuario;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresa.loaded(usuario);
        }
        public ReporteCargasDiesel getCargaCombustible()
        {
            try
            {
                bool? resp = ShowDialog();
                if (resp.Value && lvlCargas.SelectedItem != null)
                {
                    return (lvlCargas.SelectedItem as ListViewItem).Content as ReporteCargasDiesel;
                }
                return null;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }
        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                lvlCargas.ItemsSource = null;
                Cursor = Cursors.Wait;
                var resp = new ReporteCargaCombustibleSvc().getReporteCombustibleByFiltros(ctrEmpresa.empresaSelected.clave, ctrFechas.fechaInicial, ctrFechas.fechaFinal,
                  new List<string>(), true, new List<string>(), true, new List<string>(), true, new List<string>(), true);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<ReporteCargasDiesel> lista = resp.result as List<ReporteCargasDiesel>;
                    llenarLista(lista);
                }
            }
            catch (Exception EX)
            {
                imprimir(EX);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lvlCargas.ItemsSource = null;
        }

        private void llenarLista(List<ReporteCargasDiesel> listaCargas)
        {
            try
            {
                lvlCargas.ItemsSource = null;
                List<ListViewItem> list = new List<ListViewItem>();
                foreach (ReporteCargasDiesel col in listaCargas)
                {
                    ListViewItem item = new ListViewItem
                    {
                        Content = col
                    };
                    item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                    list.Add(item);
                }
                this.lvlCargas.ItemsSource = list;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlCargas.ItemsSource);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private bool UserFilter(object item) =>
             (string.IsNullOrEmpty(this.txtBuscador.Text) ||
                ((((ReporteCargasDiesel)(item as ListViewItem).Content).unidad.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)) ||
                ((((ReporteCargasDiesel)(item as ListViewItem).Content).vale.ToString().IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0))||
                ((((ReporteCargasDiesel)(item as ListViewItem).Content).idOrdenServ.ToString().IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (lvlCargas.ItemsSource == null) return;

            CollectionViewSource.GetDefaultView(this.lvlCargas.ItemsSource).Refresh();
        }
    }
}
