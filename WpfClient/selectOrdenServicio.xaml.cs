﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for selectOrdenServicio.xaml
    /// </summary>
    public partial class selectOrdenServicio : Window
    {
        public selectOrdenServicio()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Opacity = .92;
            buscar();
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            //ctrFechas = new controlFechas();
            buscar(); 
        }
        int tipoOrden = 0;
        public TipoOrdenServicio getOrdenServicio(int tipoOrden = 0)
        {
            try
            {
                this.tipoOrden = tipoOrden;
                bool? dResult = ShowDialog();
                if (dResult.Value && lvlOrdenes != null && lvlOrdenes.SelectedItem != null)
                {
                    return (TipoOrdenServicio)((ListViewItem)lvlOrdenes.SelectedItem).Content;
                }
                return null;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }

        private void buscar()
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult resp = new OperacionSvc().getOrdenServByFechas(ctrFechas.fechaInicial, ctrFechas.fechaFinal);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapLista(resp.result as List<TipoOrdenServicio>);
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapLista(List<TipoOrdenServicio> list)
        {
            lvlOrdenes.ItemsSource = null;
            List<ListViewItem> listaLvl = new List<ListViewItem>();
            foreach (var item in list)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick; ; ;
                listaLvl.Add(lvl);
            }
            lvlOrdenes.ItemsSource = listaLvl;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlOrdenes.ItemsSource);
            view.Filter = UserFilter;
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((TipoOrdenServicio)(item as ListViewItem).Content).idOrdenSer.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((TipoOrdenServicio)(item as ListViewItem).Content).chofer.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((TipoOrdenServicio)(item as ListViewItem).Content).TipoServicio.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((TipoOrdenServicio)(item as ListViewItem).Content).estatus.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlOrdenes.ItemsSource).Refresh();
        }
    }
}
