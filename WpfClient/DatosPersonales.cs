﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfClient
{
    public static class DatosPersonales
    {
        public static List<string> getTiposSangre()
        {
            return new List<string>
            {
                "A+",
                "A-",
                "B+",
                "B-",
                "AB+",
                "AB-",
                "O+",
                "O-"
            };
        }
        public static List<string> getListaGeneros()
        {
            return new List<string>
            {
                "MASCULINO",
                "FEMENINO"
            };
        }
        public static List<string> getListaEstadoCivil()
        {
            return new List<string>
            {
                "CASADO(A)",
                "SOLTERO(A)",
                "UNIÓN LIBRE",
                "VIUDO(A)"
            };
        }

        public static List<string> getListaTallaCamisaHombres()
        {
            return new List<string>
            {
                "XXS (32)",
                "XS (34)",
                "S (36)",
                "M (38)",
                "L (40)",
                "XL (42)",
                "2XL (44)",
                "3XL (46)",
                "4XL (48)"
            };
        }
        public static List<string> getListaTallaCamisaMujer()
        {
            return new List<string>
            {
                "XXS (24)",
                "XS (26)",
                "S (28)",
                "M (30)",
                "L (32)",
                "XL (34)",
                "2XL (36)",
                "3XL (38)",
                "4XL (40)"
            };
        }
        /// <summary>
        /// METODO PARA OBTENER LISTADO DE LAS TALLAS DE ZAPATOS
        /// </summary>
        /// <returns>LISTA DE STRING</returns>
        public static List<string> getListaTallaZapatos()
        {
            return new List<string>
            {
                 "22.0",
                 "22.0",
                 "22.0",
                 "22.5",
                 "23.0",
                 "23.5",
                 "24.0",
                 "24.5",
                 "25.0",
                 "25.5",
                 "26.0",
                 "26.5",
                 "27.0",
                 "27.5",
                 "28.0",
                 "28.5",
                 "29.0",
                 "29.5",
                 "30.0",
                 "30.5",
                 "31.0",
                 "31.5",
                 "32.0"
            };
        }
        public static List<string> getListaParentezcos()
        {
            return new List<string>
            {
                "ESPOSO(A)",
                "PAREJA",
                "PADRE",
                "MADRE",
                "HERMANO(A)",
                "HIJO(A)",
                "AMIGO(A)",
                "PRIMO(A)",
                "TÍO(A)",
                "ABUELO(A)",
                "SOBRINO(A)"
            };
        }

        public static List<string> getListaMotivosBaja()
        {
            return new List<string>
            {
                "AUSENTISMO",
                "ABANDO DE TRABAJO",
                "VALORES",
                "OTRO"
            };
        }

        public static List<string> getListaLicenciaEstatal()
        {
            return new List<string>
            {
                "AUTOMOVILISTA",
                "CHÓFER",
                "MOTOCICLETA",
                "PERSONA CON DISCAPACIDAD AUTOMOVIL",
                "SERVICIO PÚBLICO DE CARGA",
                "TRANSPORTE PÚBLICO DE PASAJEROS"
            };
        }
        public static List<string> getListaLicenciaFederal()
        {
            return new List<string>
            {
                "TIPO A",
                "TIPO B",
                "TIPO C",
                "TIPO D",
                "TIPO E",
                "TIPO F"
            };
        }
        public static List<string> getListaBancos()
        {
            return new List<string>
            {
                "HSBC",
                "BANAMEX",
                "SANTANDER",
                "BANORTE",
                "BANREGIO",
                "SCOTIABANK",
                "INBURSA",
                "BANCOMER"

            };

        }


    }
}

