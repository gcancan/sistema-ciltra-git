﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for selectHojasCalculo.xaml
    /// </summary>
    public partial class selectHojasCalculo : Window
    {
        List<string> _hojas = new List<string>();
        public selectHojasCalculo()
        {
            InitializeComponent();
        }
        public selectHojasCalculo(List<string> hojas)
        {
            InitializeComponent();
            _hojas = hojas;
        }

        public string selectHoja()
        {
            bool? sResult = ShowDialog();
            if (sResult.Value == true && lvlHojas.SelectedItem != null)
            {
                return Convert.ToString(lvlHojas.SelectedItem);
            }
            return "";
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            foreach (var item in _hojas)
            {
                lvlHojas.Items.Add(item);
            }
        }
    }
}
