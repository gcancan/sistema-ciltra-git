﻿using Core.Interfaces;
using Core.Models;
using CoreINTELISIS.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para INTELISIS_catUnidadTransporte.xaml
    /// </summary>
    public partial class INTELISIS_catUnidadTransporte : UserControl
    {
        public INTELISIS_catUnidadTransporte(SincronizadorINTELISIS pantalla)
        {
            InitializeComponent();
            this.pantalla = pantalla;
        }
        SincronizadorINTELISIS pantalla = null;
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            pantalla.stpContenedor.Children.Clear();
        }
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lvlUnidadTransporte.ItemsSource = null;
                lblContador.Content = 0;
                lblActualizados.Content = 0;
                lblNuevos.Content = 0;
                Cursor = Cursors.Wait;
                var resp = new UnidadTransporteINTELISISSvc().getAllUnidadesTransporte();
                if (resp.typeResult == ResultTypes.success)
                {
                    List<UnidadTransporte> listaTipos = resp.result as List<UnidadTransporte>;
                    lvlUnidadTransporte.ItemsSource = listaTipos;
                    lblContador.Content = listaTipos.Count;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                lblActualizados.Content = 0;
                lblNuevos.Content = 0;
                if (lvlUnidadTransporte.ItemsSource != null)
                {
                    List<UnidadTransporte> listaUnidades = lvlUnidadTransporte.ItemsSource as List<UnidadTransporte>;
                    var resp = new UnidadTransporteSvc().sincronizarUnidadesTransporte(listaUnidades);
                    lblActualizados.Content = resp.noActualizados;
                    lblNuevos.Content = resp.noNuevos;
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

       
    }
}
