﻿namespace WpfClient
{
    using Core.Models;
    using System;
    using System.Runtime.InteropServices;

    public static class EstablecerPrecios
    {
        public static TipoPrecio establecerPrecio(UnidadTransporte remolque1, UnidadTransporte dolly, UnidadTransporte remolque2, Empresa empresa, Cliente cliente = null)
        {
            if (empresa.clave == 1)
            {
                if (cliente == null) return TipoPrecio.SENCILLO;
                if ((cliente.clave == 3) || (cliente.clave == 5))
                {
                    if ((dolly != null) || (remolque2 != null))
                    {
                        return TipoPrecio.FULL;
                    }
                    if (remolque1.tipoUnidad.descripcion == "TOLVA 35 TON")
                    {
                        return TipoPrecio.SENCILLO35;
                    }
                    return TipoPrecio.SENCILLO;
                }
                if (remolque2 != null)
                {
                    return TipoPrecio.FULL;
                }
                return TipoPrecio.SENCILLO;
            }
            if (remolque2 != null)
            {
                return TipoPrecio.FULL;
            }
            return TipoPrecio.SENCILLO;
        }
        public static TipoPrecio establecerPrecioChofer(UnidadTransporte remolque1, UnidadTransporte dolly, UnidadTransporte remolque2, Empresa empresa)
        {
            if (empresa.clave == 1)
            {
                if ((dolly != null) || (remolque2 != null))
                {
                    return TipoPrecio.FULL;
                }
                if (remolque1.tipoUnidad.descripcion == "TOLVA 35 TON")
                {
                    return TipoPrecio.SENCILLO35;
                }
                if (remolque1.tipoUnidad.TonelajeMax == 30M)
                {
                    return TipoPrecio.SENCILLO30;
                }
                if (remolque1.tipoUnidad.TonelajeMax == 24M)
                {
                    return TipoPrecio.SENCILLO24;
                }
                return TipoPrecio.SENCILLO;
            }
            if (remolque2 != null)
            {
                return TipoPrecio.FULL;
            }
            return TipoPrecio.SENCILLO;
        }
        public static TipoPrecioCasetas establecerCostoCasetas(UnidadTransporte tc, UnidadTransporte rm1, UnidadTransporte dl, UnidadTransporte rm2)
        {
            int noEjes = 0;
            noEjes += tc == null ? 0 : Convert.ToInt32(tc.tipoUnidad.NoEjes.Trim());
            noEjes += rm1 == null ? 0 : Convert.ToInt32(rm1.tipoUnidad.NoEjes.Trim());
            noEjes += dl == null ? 0 : Convert.ToInt32(dl.tipoUnidad.NoEjes.Trim());
            noEjes += rm2 == null ? 0 : Convert.ToInt32(rm2.tipoUnidad.NoEjes.Trim());

            return (TipoPrecioCasetas)noEjes;
        }        
        public static TipoPrecioCasetas establecerPrecioAnticipo(UnidadTransporte rm1, UnidadTransporte rm2)
        {
            int noEjes = 0;
          
            noEjes += rm1 == null ? 0 : Convert.ToInt32(rm1.tipoUnidad.NoEjes.Trim());

            if (rm2 != null) return TipoPrecioCasetas.FULL;

            noEjes += rm2 == null ? 0 : Convert.ToInt32(rm2.tipoUnidad.NoEjes.Trim());            

            return (TipoPrecioCasetas)noEjes;
        }
    }
}
