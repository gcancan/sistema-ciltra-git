﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para reporteCitasMatto.xaml
    /// </summary>
    public partial class reporteCitasMatto : ViewBase
    {
        public reporteCitasMatto()
        {
            InitializeComponent();
        }
        List<DateTime> listaFechas = new List<DateTime>();
        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                listaFechas = new List<DateTime>();
                TimeSpan tiempo = ctrFEchas.fechaFinal - ctrFEchas.fechaInicial;
                for (int i = 0; i < tiempo.TotalDays; i++)
                {
                    listaFechas.Add(ctrFEchas.fechaInicial.AddDays(i));
                }
                //llenarFechasHeader(listaFechas);
                buscarCitasMtto(listaFechas);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        List<ReporteAllCitasMtto> listaCitas = new List<ReporteAllCitasMtto>();
        List<string> listaId = new List<string>();
        void buscarCitasMtto(List<DateTime> listaFechas)
        {
            try
            {
                Cursor = Cursors.Wait;

                listaId = cbxUnidades.ItemsSource.Cast<UnidadTransporte>().ToList().Select(s => s.clave).ToList();
                
                var resp = new CitaMantenimientoSvc().getReporteCitasMatto(listaId, ctrFEchas.fechaInicial, ctrFEchas.fechaFinal);
                if (resp.typeResult == ResultTypes.success)
                {
                    listaCitas = resp.result as List<ReporteAllCitasMtto>;
                }
                else if(resp.typeResult != ResultTypes.recordNotFound)
                {
                    imprimir(resp);
                }

                //crearCalendario(listaId, listaFechas, listaCitas);
                ocultarMostrarCalendario();

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void ocultarMostrarCalendario()
        {
            List<DateTime> listaFechasNew = new List<DateTime>();    
            if (chcColumnas.IsChecked.Value)
            {
                bool vacio = true;
                foreach (DateTime fecha in listaFechas)
                {
                    if (listaCitas.Exists(s => s.fecha == fecha))
                        listaFechasNew.Add(fecha);
                }
            }
            else
            {
                listaFechasNew = listaFechas;
            }

            llenarFechasHeader(listaFechasNew);
            crearCalendario(listaId, listaFechasNew, listaCitas);

            if (chcFilas.IsChecked.Value)
            {
                List<StackPanel> listaFilasVacias = new List<StackPanel>();
                foreach (StackPanel itemStpContent in stpCalendario.Children.Cast<StackPanel>().ToList())
                {
                    bool vacio = true;
                    foreach (Label lblCalendario in itemStpContent.Children.Cast<Label>().ToList())
                    {
                        if (!string.IsNullOrEmpty(lblCalendario.Content.ToString()))
                        {
                            if (lblCalendario.Content.ToString().Contains("TC"))
                            {
                                continue;
                            }
                            vacio = false;
                            break;
                        }
                    }
                    if (vacio) listaFilasVacias.Add(itemStpContent);
                }
                foreach (var item in listaFilasVacias)
                {
                    stpCalendario.Children.Remove(item);
                }
            }
        }
        private void crearCalendario(List<string> listaId, List<DateTime> listaFechas, List<ReporteAllCitasMtto> listaCitas)
        {
            stpCalendario.Children.Clear();
            int contA = 0;
            int contAA = 0;
            int contB = 0;
            int contBA = 0;
            int contC = 0;
            int contP = 0;
            ctrA.valor = contA;
            ctrAA.valor = contAA;
            ctrB.valor = contB;
            ctrBA.valor = contBA;
            ctrC.valor = contC;
            ctrP.valor = contP;
            foreach (string unidad in listaId)
            {
                StackPanel stpContent = new StackPanel() { Orientation = Orientation.Horizontal };

                Label lblUnidad = new Label
                {
                    Content = unidad, FontWeight = FontWeights.Bold,
                    Width = 140, Height = 30, 
                    HorizontalContentAlignment = HorizontalAlignment.Center, VerticalContentAlignment = VerticalAlignment.Center
                };
                stpContent.Children.Add(lblUnidad);

                foreach (DateTime fechaItem in listaFechas)
                {
                    List<ReporteAllCitasMtto> listaFiltrada = listaCitas.FindAll(s => s.unidad == unidad && s.fecha == fechaItem).ToList();

                    List<ReporteAllCitasMtto> listaPrograma = listaFiltrada.FindAll(s => s.tipoReporteAllCitasMtto == TipoReporteAllCitasMtto.PROGRAMADO).ToList();
                    List<ReporteAllCitasMtto> listaServicio = listaFiltrada.FindAll(s => s.tipoReporteAllCitasMtto == TipoReporteAllCitasMtto.SERVICIO).ToList();

                    Label lblCalendario = new Label
                    {
                        Content = string.Empty, FontWeight = FontWeights.Bold,
                        Width = 140, BorderBrush = Brushes.SteelBlue,
                        BorderThickness = new Thickness(1), Height = 30,
                        HorizontalContentAlignment = HorizontalAlignment.Center, VerticalContentAlignment = VerticalAlignment.Center,
                        ToolTip = string.Format("{0}-{1}", unidad, getHeader(fechaItem))
                    };

                    if (listaPrograma.Count > 0 && listaServicio.Count > 0)
                    {
                        ReporteAllCitasMtto repAll = listaServicio.OrderByDescending(s => s.tipo).First();
                        lblCalendario.Background = Brushes.Green;
                        lblCalendario.Content = string.Format("P/{0}", repAll.tipo);
                        switch (repAll.tipo)
                        {
                            case "A":
                                contA++;
                                break;
                            case "AA":
                                contAA++;
                                break;
                            case "B":
                                contB++;
                                break;
                            case "BA":
                                contBA++;
                                break;
                            case "C":
                                contC++;
                                break;                            
                        }
                        contP++;
                    }
                    else if (listaPrograma.Count > 0)
                    {
                        lblCalendario.Background = Brushes.Orange;
                        lblCalendario.Content = string.Format("P");
                        contP++;
                    }
                    else if(listaServicio.Count > 0)
                    {
                        ReporteAllCitasMtto repAll = listaServicio.OrderByDescending(s => s.tipo).First();
                        lblCalendario.Background = Brushes.Green;
                        lblCalendario.Content = repAll.tipo;
                        switch (repAll.tipo)
                        {
                            case "A":
                                contA++;
                                break;
                            case "AA":
                                contAA++;
                                break;
                            case "B":
                                contB++;
                                break;
                            case "BA":
                                contBA++;
                                break;
                            case "C":
                                contC++;
                                break;
                        }
                    }

                    stpContent.Children.Add(lblCalendario);
                }

                stpCalendario.Children.Add(stpContent);
            }
            ctrA.valor = contA;
            ctrAA.valor = contAA;
            ctrB.valor = contB;
            ctrBA.valor = contBA;
            ctrC.valor = contC;
            ctrP.valor = contP;
        }

        private void llenarFechasHeader(List<DateTime> listaFechas)
        {
            stpHeader.Children.Clear();
            foreach (var fecha in listaFechas)
            {
                Label lbl = new Label()
                {
                    Width = 140,
                    Background = Brushes.SteelBlue,
                    //Foreground = Brushes.White,
                    FontWeight = (fecha == new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day) ? FontWeights.Bold : FontWeights.Normal )
                     
                };

                string header = getHeader(fecha);
                
                lbl.Content = header;
                stpHeader.Children.Add(lbl);
            }
        }
        string getHeader(DateTime fecha)
        {
            return (string.Format("{0} {1}-{2}", getDiaSemana(fecha.DayOfWeek), fecha.Day.ToString(), fecha.ToString("MMM"))).ToUpper();
        }
        private string getDiaSemana(DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Sunday:
                    return "Domingo";
                case DayOfWeek.Monday:
                    return "Lunes";
                case DayOfWeek.Tuesday:
                    return "Martes";
                case DayOfWeek.Wednesday:
                    return "Miércoles";
                case DayOfWeek.Thursday:
                    return "Jueves";
                case DayOfWeek.Friday:
                    return "Viernes";
                case DayOfWeek.Saturday:
                    return "Sábado";
                default:
                    return string.Empty;
            }
        }
        Usuario usuario = null;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                this.usuario = mainWindow.usuario;
                ctrEmpresas.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
                ctrEmpresas.loaded(usuario);

                ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
                ctrZonaOperativa.cargarControl(usuario.zonaOperativa, usuario.idUsuario);
                stpOperacion.IsEnabled = new PrivilegioSvc().consultarPrivilegio("ADMIN_COMBO_CLIENTES", usuario.idUsuario).typeResult == ResultTypes.success;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                this.IsEnabled = false;
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cargarOperacion();
            if (ctrEmpresas.empresaSelected.clave != 1)
            {
                cargarUnidades();
            }
        }
        void cargarOperacion()
        {
            try
            {
                Cursor = Cursors.Wait;
                cbxOperacion.ItemsSource = null;

                if (ctrEmpresas.empresaSelected.clave == 1)
                {
                    stpOperacion.Visibility = Visibility.Visible;
                    if (ctrZonaOperativa.zonaOperativa != null)
                    {
                        var resp = new ClienteSvc().getClientesByZonasOperativas(new List<string> { (ctrZonaOperativa.zonaOperativa.idZonaOperativa.ToString()) }, ctrEmpresas.empresaSelected.clave);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            var listaOperaciones = resp.result as List<OperacionFletera>;
                            cbxOperacion.ItemsSource = listaOperaciones;
                            var sel = cbxOperacion.ItemsSource.Cast<OperacionFletera>().ToList().Find(s => s.cliente.clave == usuario.cliente.clave);
                            if (sel != null)
                            {
                                cbxOperacion.SelectedItems.Add(sel);
                            }

                        }
                        else if (resp.typeResult != ResultTypes.recordNotFound)
                        {
                            imprimir(resp);
                        }
                    }
                }
                else
                {
                    stpOperacion.Visibility = Visibility.Hidden;
                    cargarUnidades();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        void cargarUnidades()
        {
            try
            {
                List<UnidadTransporte> listaUnidades = new List<UnidadTransporte>();
                chcTodos.IsChecked = false;
                cbxUnidades.SelectedItems.Clear();
                Cursor = Cursors.Wait;

                if (ctrEmpresas.empresaSelected.clave == 1)
                {
                    if (cbxOperacion.SelectedItems.Count >= 0)
                    {
                        foreach (OperacionFletera operacion in cbxOperacion.SelectedItems)
                        {
                            var resp = new FlotaSvc().getFlota(operacion);
                            if (resp.typeResult == ResultTypes.success)
                            {
                                foreach (Flota flota in resp.result as List<Flota>)
                                {
                                    if (flota.tractor != null)
                                    {
                                        listaUnidades.Add(flota.tractor);
                                    }
                                    //if (flota.remolque1 != null)
                                    //{
                                    //    listaUnidades.Add(flota.remolque1);
                                    //}
                                    //if (flota.dolly != null)
                                    //{
                                    //    listaUnidades.Add(flota.dolly);
                                    //}
                                    //if (flota.remolque2 != null)
                                    //{
                                    //    listaUnidades.Add(flota.remolque2);
                                    //}
                                }
                            }
                        }
                    }

                }
                else if(empresa.clave == 2)
                {
                    if (ctrZonaOperativa.zonaOperativa != null)
                    {
                        OperacionFletera operacion = new OperacionFletera
                        {
                            empresa = ctrEmpresas.empresaSelected,
                            cliente = null,
                            zonaOperativa = ctrZonaOperativa.zonaOperativa
                        };
                        var resp = new FlotaSvc().getFlota(operacion);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            foreach (Flota flota in resp.result as List<Flota>)
                            {
                                if (flota.tractor != null)
                                {
                                    listaUnidades.Add(flota.tractor);
                                }
                                //if (flota.remolque1 != null)
                                //{
                                //    listaUnidades.Add(flota.remolque1);
                                //}
                                //if (flota.dolly != null)
                                //{
                                //    listaUnidades.Add(flota.dolly);
                                //}
                                //if (flota.remolque2 != null)
                                //{
                                //    listaUnidades.Add(flota.remolque2);
                                //}
                            }
                        }
                    }
                }
                cbxUnidades.ItemsSource = listaUnidades.OrderBy(s => s.clave);


            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        Empresa empresa = null;
        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                chcTodos.IsChecked = false;
                cbxUnidades.ItemsSource = null;
                if (sender != null)
                {
                    empresa = ctrEmpresas.empresaSelected;
                    if (empresa.clave == 5)
                    {
                        var resp = new UnidadTransporteSvc().getTractores(ctrEmpresas.empresaSelected.clave);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            cbxUnidades.ItemsSource = resp.result as List<UnidadTransporte>;
                            chcTodos.IsChecked = true;
                        }
                        else
                        {
                            cbxUnidades.ItemsSource = null;
                        }
                        stpOperacion.Visibility = Visibility.Hidden;
                        ctrZonaOperativa.Visibility = Visibility.Hidden;
                    }
                    else if (empresa.clave == 2)
                    {
                        stpOperacion.Visibility = Visibility.Hidden;
                        ctrZonaOperativa.Visibility = Visibility.Visible;
                        buscarTcByFlotaAtlante();
                    }
                    else if (empresa.clave == 1)
                    {
                        ctrZonaOperativa.Visibility = Visibility.Visible;
                        stpOperacion.Visibility = Visibility.Visible;

                        if (ctrZonaOperativa.cbxZonaOpetativa == null) return;

                        ctrZonaOperativa.cbxZonaOpetativa.SelectedItem = null;

                        //if (ctrZonaOperativa.cbxZonaOpetativa.SelectedItem == null) return;
                        ctrZonaOperativa.cbxZonaOpetativa.SelectedIndex = 0;

                    }

                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        void buscarTcByFlotaAtlante()
        {
            try
            {
                List<UnidadTransporte> listaUnidades = new List<UnidadTransporte>();
                chcTodos.IsChecked = false;
                cbxUnidades.SelectedItems.Clear();
                Cursor = Cursors.Wait;
                OperacionFletera operacion = new OperacionFletera { empresa = empresa, zonaOperativa = ctrZonaOperativa.zonaOperativa, cliente = null };
                var resp = new FlotaSvc().getFlota(operacion);
                if (resp.typeResult == ResultTypes.success)
                {
                    foreach (Flota flota in resp.result as List<Flota>)
                    {
                        if (flota.tractor != null)
                        {
                            listaUnidades.Add(flota.tractor);
                        }
                    }
                    cbxUnidades.ItemsSource = listaUnidades.OrderBy(s => s.clave);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void CbxOperacion_ItemSelectionChanged(object sender, Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventArgs e)
        {
            if (ctrEmpresas.empresaSelected.clave == 1)
            {
                if (cbxOperacion.SelectedItems != null)
                {
                    cargarUnidades();
                }

            }
        }

        private void ChcTodosOperaciones_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    CheckBox check = sender as CheckBox;
                    if (check.IsChecked.Value)
                    {
                        cbxOperacion.SelectAll();
                    }
                    else
                    {
                        cbxOperacion.SelectedItems.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void ChcTodosUnidades_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    CheckBox check = sender as CheckBox;
                    if (check.IsChecked.Value)
                    {
                        cbxUnidades.SelectAll();
                    }
                    else
                    {
                        cbxUnidades.SelectedItems.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void ChcFilas_Checked(object sender, RoutedEventArgs e)
        {
            ocultarMostrarCalendario();
        }

        private void ChcColumnas_Checked(object sender, RoutedEventArgs e)
        {
            ocultarMostrarCalendario();
        }
    }
}
