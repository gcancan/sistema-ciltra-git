﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for cancelarOrden.xaml
    /// </summary>
    public partial class cancelarOrden : Window
    {
        public cancelarOrden()
        {
            InitializeComponent();
        }
        OrdenTaller orden;
        private string nombreUsuario;

        public cancelarOrden(OrdenTaller orden, string nombreUsuario)
        {
            this.nombreUsuario = nombreUsuario;
            this.orden = orden;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //this.FontFamily = new System.Windows.Media.FontFamily("Tahoma");
            this.Opacity = .92;

            MaxHeight = this.Height;
            MinHeight = this.Height;
            MaxWidth = Width;
            MinWidth = Width;

            txtFolio.valor = orden.idOrdenServ;
            txtOrdenTrabajo.valor = orden.idOrdenTrabajo;
            txtDescripcion.Text = orden.descripcion;
        }

        public bool? cancelarOrdenTrabajo()
        {
            return ShowDialog();
        }
        private bool v2 = false;
        public bool? cancelarOrdenTrabajo_v2()
        {
            v2 = true;
            return ShowDialog();
        }
        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            OperationResult resp = new OperationResult();
            if (v2)resp = new MasterOrdServSvc().cancelarOrdenTrabajo_v2(orden, txtMotivo.Text, nombreUsuario);                
            else resp = new MasterOrdServSvc().cancelarOrdenTrabajo(orden, txtMotivo.Text, nombreUsuario);
                
            if (resp.typeResult == ResultTypes.success)
            {
                ImprimirMensaje.imprimir(resp);
                DialogResult = true;
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
            }
        }
    }
}
