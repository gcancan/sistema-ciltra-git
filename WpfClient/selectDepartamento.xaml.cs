﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using Core.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectDepartamento.xaml
    /// </summary>
    public partial class selectDepartamento : Window
    {
        public selectDepartamento()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
        public Departamento obtenerDepartamento()
        {
            try
            {
                getAllDepartamentos();
                bool? result = ShowDialog();
                if (result.Value && lvlDepartamentos.SelectedItem != null)
                {
                    return (lvlDepartamentos.SelectedItem as ListViewItem).Content as Departamento;
                }
                return null;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }
        private void getAllDepartamentos()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new DepartamentoSvc().getALLDepartamentosById();
                if (resp.typeResult == ResultTypes.success)
                {
                    cargaLista(resp.result as List<Departamento>);
                }
                else 
                {
                    imprimir(resp);
                    IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void cargaLista(List<Departamento> listaDepto)
        {
            try
            {
                lvlDepartamentos.ItemsSource = null;
                List<ListViewItem> list = new List<ListViewItem>();
                foreach (Departamento depto in listaDepto)
                {
                    ListViewItem item = new ListViewItem
                    {
                        Content = depto
                    };
                    item.MouseDoubleClick += Item_MouseDoubleClick;
                    list.Add(item);
                }
                this.lvlDepartamentos.ItemsSource = list;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlDepartamentos.ItemsSource);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtBuscador.Text) ||
            ((((Departamento)(item as ListViewItem).Content).descripcion.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void Item_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void TxtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(this.lvlDepartamentos.ItemsSource).Refresh();
        }
        
    }
}
