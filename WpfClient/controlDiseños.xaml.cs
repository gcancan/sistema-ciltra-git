﻿using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for controlDiseños.xaml
    /// </summary>
    public partial class controlDiseños : UserControl
    {
        public controlDiseños()
        {
            InitializeComponent();
        }

        private void cbxDiseños_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            cbxDiseños.Width = Width - 5;
        }

        public void cargarControl()
        {
            try
            {
                OperationResult resp = new DiseñosLlantaSvc().getDiseñosLlantaAllOrById();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxDiseños.ItemsSource = (List<Diseño>)resp.result;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    this.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                this.IsEnabled = false;
            }
        }

        public Diseño diseñoSelect
        {
            get
            {
                if (cbxDiseños.SelectedItem != null)
                {
                    return (Diseño)cbxDiseños.SelectedItem;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                Diseño diseño = value;
                if (diseño == null)
                {
                    cbxDiseños.SelectedItem = null;
                }
                else
                {
                    int i = 0;
                    foreach (var item in (List<Diseño>)cbxDiseños.ItemsSource)
                    {
                        if (item.idDiseño == diseño.idDiseño)
                        {
                            cbxDiseños.SelectedIndex = i;
                            break;
                        }
                        i++;
                    }
                }
            }
        }
    }
}
