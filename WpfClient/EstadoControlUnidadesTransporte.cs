﻿using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    public static class EstadoControlUnidadesTransporte
    {
        public static OperationResult iniciarEstado(Viaje viaje)
        {
            try
            {

                TableroControlUnidades tableroControlUnidades = new TableroControlUnidades();
                tableroControlUnidades.viaje = viaje;
                tableroControlUnidades.fechaSalidaPlanta = viaje.FechaHoraViaje;
                tableroControlUnidades.taller = viaje.taller;
                tableroControlUnidades.tallerExterno = viaje.tallerExterno;

                tableroControlUnidades.listaDetalles = new List<DetalleTableroControlUnidades>();
                foreach (CartaPorte cp in viaje.listDetalles)
                {
                    tableroControlUnidades.listaDetalles.Add(new DetalleTableroControlUnidades
                    {
                        idDetalleTableroControlUnidades = 0,
                        cartaPorte = cp,
                        tiempo = 0,
                        fechaLLegada = null,
                        fechaSalida = null
                    });
                }
                var resp = new TableroControlUnidadesSvc().iniciarTrayectoGranja(ref tableroControlUnidades);
                if (resp.typeResult != ResultTypes.success)
                {
                    string mensaje = resp.mensaje;
                    resp.mensaje = "OCURRIO UN ERROR AL INICIAR EL EVENTO DE TABLERO DE CONTROL DE UNIDADES" + Environment.NewLine + mensaje;
                }
                return (resp);
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
