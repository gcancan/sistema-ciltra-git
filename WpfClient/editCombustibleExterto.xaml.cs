﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using CoreFletera.Interfaces;
    using CoreFletera.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;

    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Markup;
    using System.Windows.Media;
    using Xceed.Wpf.Toolkit;

    public partial class editCombustibleExterto : ViewBase
    {
        private bool isVentana;
        public OperationResult result;
        public Viaje _viaje;
        private int idOrden;

        public editCombustibleExterto()
        {
            this.isVentana = false;
            OperationResult result1 = new OperationResult
            {
                valor = 2,
                mensaje = "Cambios no realizados"
            };
            this.result = result1;
            this._viaje = null;
            this.idOrden = 0;
            this.InitializeComponent();
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                usuario = mainWindow.usuario;

                txtTractor.DataContextChanged += TxtTractor_DataContextChanged;
                txtRemolque1.DataContextChanged += TxtRemolque1_DataContextChanged;
                txtRemolque2.DataContextChanged += TxtRemolque2_DataContextChanged;


                this.cbxEmpresas.cbxEmpresa.SelectionChanged += new SelectionChangedEventHandler(this.CbxEmpresa_SelectionChanged);
                this.cbxEmpresas.loaded(usuario.empresa, (new PrivilegioSvc().consultarPrivilegio("isAdmin", usuario.idUsuario).typeResult == ResultTypes.success));

                ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
                ctrZonaOperativa.cargarControl(usuario.zonaOperativa, usuario.idUsuario);

                this.txtUltimokm.txtDecimal.TextChanged += new TextChangedEventHandler(this.TxtDecimal_TextChanged);
                this.txtKM.txtDecimal.TextChanged += new TextChangedEventHandler(this.TxtDecimal_TextChanged);
                //this.txtLtCombustible.txtDecimal.TextChanged += new TextChangedEventHandler(this.TxtDecimal_TextChanged1);
                this.txtCombustibleUtilizado.txtDecimal.TextChanged += new TextChangedEventHandler(this.TxtDecimal_TextChanged1);
                OperationResult result = new ProveedorSvc().getAllProveedoresV2();
                if (result.typeResult == ResultTypes.success)
                {
                    this.txtProveedor.ItemsSource = (result.result as List<ProveedorCombustible>).FindAll(s => s.diesel && s.estatus);
                }
                this.nuevo();
                if (this._viaje != null)
                {
                    this.txtTractor.unidadTransporte = this._viaje.tractor;
                    this.txtRemolque1.unidadTransporte = this._viaje.remolque;
                    this.txtDolly.unidadTransporte = this._viaje.dolly;
                    this.txtRemolque2.unidadTransporte = this._viaje.remolque2;
                    this.txtOperador.personal = this._viaje.operador;
                    this.dtpFecha.Value = new DateTime?(this._viaje.FechaHoraViaje);
                    this.consultaUltimoKm();
                }

                cbxEmpresas.lbl.Content = "EMPRESA:";
                //cbxEmpresas.cbxEmpresa.Width = 350;
                ctrZonaOperativa.lblZona.Content = "Z. OPERATIVA:";
                //ctrZonaOperativa.lblZona.Width = 120;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void TxtRemolque2_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            actualizarCombo();
        }

        private void TxtRemolque1_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            actualizarCombo();
        }

        private void actualizarCombo()
        {
            cbxUnidades.Items.Clear();
            if (txtTractor.unidadTransporte != null)
            {
                if (txtTractor.unidadTransporte.tipoUnidad.bCombustible)
                {
                    cbxUnidades.Items.Add(txtTractor.unidadTransporte);
                }
            }

            if (txtRemolque1.unidadTransporte != null)
            {
                if (txtRemolque1.unidadTransporte.tipoUnidad.bCombustible)
                {
                    cbxUnidades.Items.Add(txtRemolque1.unidadTransporte);
                }
            }

            if (txtRemolque2.unidadTransporte != null)
            {
                if (txtRemolque2.unidadTransporte.tipoUnidad.bCombustible)
                {
                    cbxUnidades.Items.Add(txtRemolque2.unidadTransporte);
                }
            }
            if (cbxUnidades.Items.Count > 0)
            {
                cbxUnidades.SelectedIndex = 0;
            }
        }
        private void TxtTractor_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            //consultaUltimoKm();
            actualizarCombo();
        }

        public editCombustibleExterto(int idOrden, MainWindow main, Viaje viaje)
        {
            this.isVentana = false;
            OperationResult result1 = new OperationResult
            {
                valor = 2,
                mensaje = "Cambios no realizados"
            };
            this.result = result1;
            this._viaje = null;
            this.idOrden = 0;
            this.idOrden = idOrden;
            this._viaje = viaje;
            base.mainWindow = main;
            this.isVentana = true;
            this.InitializeComponent();
        }
        private void addLista(Viaje viaje)
        {
            try
            {
                if (this.txtTractor.unidadTransporte.clave != viaje.tractor.clave)
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "El viaje no corresponde al mismo tractor"
                    };
                    ImprimirMensaje.imprimir(resp);
                }
                else if (this.lvlViajes.Items.Cast<ListViewItem>().ToList<ListViewItem>().Find(s => ((Viaje)s.Content).NumGuiaId == viaje.NumGuiaId) == null)
                {
                    ListViewItem newItem = new ListViewItem();
                    newItem.Selected += new RoutedEventHandler(this.Lvl_Selected);
                    newItem.Content = viaje;
                    this.lvlViajes.Items.Add(newItem);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                this.txtFolioViaje.valor = 0;
                this.lvlDetalles.ItemsSource = null;
            }
        }

        private void btnTicket_Click(object sender, RoutedEventArgs e)
        {
            Tickets tickets = new selectTickets().buscarTicketByFechas(cbxEmpresas.empresaSelected.clave);
            if (tickets != null)
            {
                this.buscarByTiket(tickets.ticket);
            }
        }

        private void buscarByTiket(string ticket)
        {
            OperationResult resp = new CargaCombustibleSvc().getCargaCombustibleExternoByTicket(ticket);
            if (resp.typeResult == ResultTypes.success)
            {
                CargaCombustible result = (CargaCombustible)resp.result;
                if ((this._viaje != null) && (this._viaje.tractor.clave != result.masterOrdServ.tractor.clave))
                {
                    OperationResult result1 = new OperationResult
                    {
                        valor = 2,
                        mensaje = "Este Vale No Corresponde al mismo Tractor al Del Viaje"
                    };
                    ImprimirMensaje.imprimir(result1);
                }
                else
                {
                    this.mapForm((CargaCombustible)resp.result);
                    this.grigControles.IsEnabled = true;
                }
            }
            else if (resp.typeResult != ResultTypes.recordNotFound)
            {
                ImprimirMensaje.imprimir(resp);
            }
            else
            {
                this.grigControles.IsEnabled = true;
                if (this._viaje != null)
                {
                    this.stpCabecero.IsEnabled = false;
                }
                this.ctrVale.IsEnabled = false;
            }
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (cbxEmpresas.empresaSelected != null)
            {
                var empresa = cbxEmpresas.empresaSelected;
                if (empresa.clave == 1)
                {
                    cargaClientesZonaOperativa();
                    txtTractor.loaded(etipoUniadBusqueda.TRACTOR, empresa,
                        (ctrZonaOperativa.zonaOperativa == null ? null : ctrZonaOperativa.zonaOperativa),
                        (cbxCliente.SelectedItem == null ? null : (cbxCliente.SelectedItem as Cliente)));

                    txtRemolque1.loaded(etipoUniadBusqueda.TOLVA, empresa,
                        (ctrZonaOperativa.zonaOperativa == null ? null : ctrZonaOperativa.zonaOperativa),
                        (cbxCliente.SelectedItem == null ? null : (cbxCliente.SelectedItem as Cliente)));

                    txtDolly.loaded(etipoUniadBusqueda.DOLLY, empresa,
                        (ctrZonaOperativa.zonaOperativa == null ? null : ctrZonaOperativa.zonaOperativa),
                        (cbxCliente.SelectedItem == null ? null : (cbxCliente.SelectedItem as Cliente)));

                    txtRemolque2.loaded(etipoUniadBusqueda.TOLVA, empresa,
                        (ctrZonaOperativa.zonaOperativa == null ? null : ctrZonaOperativa.zonaOperativa),
                        (cbxCliente.SelectedItem == null ? null : (cbxCliente.SelectedItem as Cliente)));

                    txtOperador.loaded(eTipoBusquedaPersonal.OPERADORES, (empresa.clave == 5 ? new Empresa { clave = 1 } : empresa));
                    stpCliente.Visibility = Visibility.Visible;
                }
                else if (empresa.clave == 5)
                {
                    txtTractor.loaded(etipoUniadBusqueda.TRACTOR, empresa);
                    txtRemolque1.loaded(etipoUniadBusqueda.TOLVA, empresa);
                    txtDolly.loaded(etipoUniadBusqueda.DOLLY, empresa);
                    txtRemolque2.loaded(etipoUniadBusqueda.TOLVA, empresa);

                    txtOperador.loaded(eTipoBusquedaPersonal.OPERADORES, empresa);

                    var resp = new ClienteSvc().getClientesALLorById(empresa.clave, 0, true);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        var listaCliente = resp.result as List<Cliente>;
                        cbxCliente.ItemsSource = listaCliente;

                        cbxCliente.SelectedItem = listaCliente.Find(s => s.clave == usuario.cliente.clave);
                        if (cbxCliente.SelectedItem == null)
                        {
                            if (listaCliente.Count == 1)
                            {
                                cbxCliente.SelectedIndex = 0;
                            }
                        }

                    }
                    stpCliente.Visibility = Visibility.Visible;
                }
                else
                {
                    txtTractor.loaded(etipoUniadBusqueda.TRACTOR, empresa);
                    txtRemolque1.loaded(etipoUniadBusqueda.TOLVA, empresa);
                    txtDolly.loaded(etipoUniadBusqueda.DOLLY, empresa);
                    txtRemolque2.loaded(etipoUniadBusqueda.TOLVA, empresa);
                    txtOperador.loaded(eTipoBusquedaPersonal.OPERADORES, (empresa.clave == 5 ? new Empresa { clave = 1 } : empresa));
                    stpCliente.Visibility = Visibility.Hidden;
                }

                return;
                this.txtOperador.loaded(eTipoBusquedaPersonal.OPERADORES, this.cbxEmpresas.empresaSelected);
                //}
                cbxCliente.ItemsSource = null;
                if (cbxEmpresas.empresaSelected.clave == 1)
                {
                    stpCliente.Visibility = Visibility.Visible;
                }
                else
                {
                    stpCliente.Visibility = Visibility.Hidden;
                }
                if (ctrZonaOperativa.zonaOperativa != null)
                {
                    ctrZonaOperativa.zonaOperativa = null;
                    ctrZonaOperativa.zonaOperativa = usuario.zonaOperativa;
                }
            }

        }
        void cargaClientesZonaOperativa()
        {
            try
            {
                Cursor = Cursors.Wait;
                Empresa empresa = cbxEmpresas.empresaSelected;
                ZonaOperativa zona = ctrZonaOperativa.zonaOperativa;
                if (zona == null) return;
                if (empresa.clave == 1)
                {
                    var resp = new ClienteSvc().getClientesByZonasOperativas(
                    new List<string> { ctrZonaOperativa.zonaOperativa.idZonaOperativa.ToString() }, empresa.clave);

                    if (resp.typeResult == ResultTypes.success)
                    {
                        var listaCliente = (resp.result as List<OperacionFletera>).Select(s => s.cliente).ToList<Cliente>();
                        cbxCliente.ItemsSource = listaCliente;
                        cbxCliente.SelectedItem = listaCliente.Find(s => s.clave == usuario.cliente.clave);
                        if (cbxCliente.SelectedItem == null)
                        {
                            if (listaCliente.Count == 1)
                            {
                                cbxCliente.SelectedIndex = 0;
                            }
                        }
                    }
                    else
                    {
                        cbxCliente.ItemsSource = null;
                    }
                }
                else if (empresa.clave == 2)
                {
                    txtTractor.loaded(etipoUniadBusqueda.TRACTOR, empresa, zona);
                    txtRemolque1.loaded(etipoUniadBusqueda.TOLVA, empresa, zona);
                    txtDolly.loaded(etipoUniadBusqueda.DOLLY, empresa, zona);
                    txtRemolque2.loaded(etipoUniadBusqueda.TOLVA, empresa, zona);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cargaClientesZonaOperativa();
            return;
            if (ctrZonaOperativa.zonaOperativa == null) return;

            cbxCliente.SelectedItem = null;
            cbxCliente.ItemsSource = null;

            txtTractor.loaded(etipoUniadBusqueda.TRACTOR, cbxEmpresas.empresaSelected, ctrZonaOperativa.zonaOperativa);
            txtRemolque1.loaded(etipoUniadBusqueda.TOLVA, cbxEmpresas.empresaSelected, ctrZonaOperativa.zonaOperativa);
            txtDolly.loaded(etipoUniadBusqueda.DOLLY, cbxEmpresas.empresaSelected, ctrZonaOperativa.zonaOperativa);
            txtRemolque2.loaded(etipoUniadBusqueda.TOLVA, cbxEmpresas.empresaSelected, ctrZonaOperativa.zonaOperativa);

            if (cbxEmpresas.empresaSelected.clave == 1)
            {
                var resp = new ClienteSvc().getClientesByZonasOperativas(
                    new List<string> { ctrZonaOperativa.zonaOperativa.idZonaOperativa.ToString() }, cbxEmpresas.empresaSelected.clave);

                if (resp.typeResult == ResultTypes.success)
                {
                    cbxCliente.ItemsSource = (resp.result as List<OperacionFletera>).Select(s => s.cliente).ToList<Cliente>();
                    cbxCliente.SelectedItem = cbxCliente.ItemsSource.Cast<Cliente>().ToList().Find(s => s.clave == mainWindow.usuario.cliente.clave);
                }
                else
                {
                    cbxCliente.ItemsSource = null;
                }
            }

            if (ctrZonaOperativa.zonaOperativa != null)
            {
                stpUnidades.IsEnabled = true;
            }
            else
            {
                stpUnidades.IsEnabled = false;
            }
        }
        private void CbxCliente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxCliente.SelectedItem != null)
            {
                Cliente cliente = cbxCliente.SelectedItem as Cliente;

                if (cliente != null)
                {
                    if (cbxEmpresas.empresaSelected.clave == 1)
                    {
                        var empresa = cbxEmpresas.empresaSelected;
                        ZonaOperativa zonaOperativa = ctrZonaOperativa.zonaOperativa;
                        txtTractor.loaded(etipoUniadBusqueda.TRACTOR, empresa, zonaOperativa, cliente);
                        txtRemolque1.loaded(etipoUniadBusqueda.TOLVA, empresa, zonaOperativa, cliente);
                        txtDolly.loaded(etipoUniadBusqueda.DOLLY, empresa, zonaOperativa, cliente);
                        txtRemolque2.loaded(etipoUniadBusqueda.TOLVA, empresa, zonaOperativa, cliente);

                        //ctrOperador.loaded(eTipoBusquedaPersonal.OPERADORES, (empresa.clave == 5 ? new Empresa { clave = 1 } : empresa));
                        txtOperador.loaded(eTipoBusquedaPersonal.OPERADORES, cliente.clave);
                    }
                }
            }
            return;
            if (cbxCliente.SelectedItem != null)
            {
                Cliente cliente = cbxCliente.SelectedItem as Cliente;
                txtTractor.loaded(etipoUniadBusqueda.TRACTOR, cbxEmpresas.empresaSelected, ctrZonaOperativa.zonaOperativa, cliente);
                txtRemolque1.loaded(etipoUniadBusqueda.TOLVA, cbxEmpresas.empresaSelected, ctrZonaOperativa.zonaOperativa, cliente);
                txtDolly.loaded(etipoUniadBusqueda.DOLLY, cbxEmpresas.empresaSelected, ctrZonaOperativa.zonaOperativa, cliente);
                txtRemolque2.loaded(etipoUniadBusqueda.TOLVA, cbxEmpresas.empresaSelected, ctrZonaOperativa.zonaOperativa, cliente);
                txtOperador.loaded(eTipoBusquedaPersonal.OPERADORES, cliente.clave);
            }
        }
        public override void cerrar()
        {
            if (this.isVentana)
            {
                base.mainWindow.DialogResult = false;
                OperationResult result1 = new OperationResult
                {
                    valor = 2,
                    mensaje = "Cambios no realizados"
                };
                this.result = result1;
            }
        }

        private void chcECM_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                CheckBox box = sender as CheckBox;
                if (box.IsChecked.Value)
                {
                    this.cbxTiempos.IsEnabled = true;
                    this.txtTiempoTotal.Text = "00:00:00";
                    this.txtTiempoPTO.Text = "00:00:00";
                    this.txtTiempoEst.Text = "00:00:00";
                    this.cbxCombustible.IsEnabled = true;
                    expECM.IsExpanded = true;
                    //txtRelleno.valor = txtLtCombustible.valor;
                }
                else
                {
                    this.cbxTiempos.IsEnabled = false;
                    this.txtTiempoTotal.Text = "00:00:00";
                    this.txtTiempoPTO.Text = "00:00:00";
                    this.txtTiempoEst.Text = "00:00:00";
                    this.cbxCombustible.IsEnabled = false;
                    //this.txtCombustibleUtilizado.valor = this.txtLtCombustible.valor;
                    this.txtCombustibleEST.valor = decimal.Zero;
                    this.txtCombustiblePTO.valor = decimal.Zero;
                    txtRelleno.valor = 0;
                    expECM.IsExpanded = false;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void consultaUltimoKm()
        {
            if ((cbxUnidades.SelectedItem != null) && this.dtpFecha.Value.HasValue)
            {
                this.txtUltimokm.valor = this.ultimoKm;
                if (txtUltimokm.valor == 0)
                {
                    txtUltimokm.soloLectura = false;
                }
                else
                {
                    txtUltimokm.soloLectura = true;
                }
            }
            else
            {
                this.txtUltimokm.valor = 0;
            }
        }

        private List<Viaje> getListaViajes()
        {
            List<Viaje> list = new List<Viaje>();
            foreach (ListViewItem item in this.lvlViajes.Items.Cast<ListViewItem>().ToList<ListViewItem>())
            {
                list.Add((Viaje)item.Content);
            }
            return list;
        }

        public override OperationResult guardar()
        {
            try
            {
                if (this.ctrVale.Tag != null)
                {
                    OperationResult result1 = new OperationResult
                    {
                        valor = 0,
                        mensaje = "INFORMACION ALMACENADA CON EXITO",
                        result = (CargaCombustible)this.ctrVale.Tag
                    };
                    this.result = result1;
                    if (this.isVentana)
                    {
                        base.mainWindow.DialogResult = true;
                    }
                    if (this.chcImprimir.IsChecked.Value)
                    {
                        ReportView view = new ReportView();
                        CargaCombustible salEnt = this.result.result as CargaCombustible;
                        if (salEnt.masterOrdServ.tractor.empresa.clave == 2)
                        {
                            view.createValeGasolinaNuevo(salEnt, "");
                        }
                        else
                        {
                            view.createValeGasolinaNuevo2(salEnt, "");
                        }
                    }
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                    return this.result;
                }
                OperationResult result = this.validarFomulario();
                if (result.typeResult > ResultTypes.success)
                {
                    return result;
                }
                CargaCombustible cargaGas = this.mapForm();
                OperationResult result2 = new OperacionSvc().saveCargaGasolinaExterno(ref cargaGas, cbxEmpresas.empresaSelected.clave, this.txtOperador.personal,
                    this.txtTractor.unidadTransporte, this.txtRemolque1.unidadTransporte, this.txtDolly.unidadTransporte,
                    this.txtRemolque2.unidadTransporte, ctrZonaOperativa.zonaOperativa, (cbxUnidades.SelectedItem as UnidadTransporte).clave);
                try
                {
                    if (result2.typeResult == ResultTypes.success)
                    {
                        this.result = result2;
                        if (this.chcImprimir.IsChecked.Value)
                        {
                            ReportView view2 = new ReportView();
                            MasterOrdServ serv1 = new MasterOrdServ
                            {
                                tractor = this.txtTractor.unidadTransporte,
                                tolva1 = this.txtRemolque1.unidadTransporte,
                                dolly = this.txtDolly.unidadTransporte,
                                tolva2 = this.txtRemolque2.unidadTransporte,
                                chofer = this.txtOperador.personal
                            };
                            cargaGas.masterOrdServ = serv1;
                            cargaGas.listCP = new List<CartaPorte>();
                            if (cbxEmpresas.empresaSelected.clave == 2)
                            {
                                view2.createValeGasolinaNuevo(cargaGas, "");
                            }
                            else
                            {
                                view2.createValeGasolinaNuevo2(cargaGas, "");
                            }

                        }
                        if (this.isVentana)
                        {
                            base.mainWindow.DialogResult = true;
                        }
                        this.mapForm((CargaCombustible)result2.result);
                        mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                    }
                }
                catch (Exception exception)
                {
                    return new OperationResult(exception);
                }
                return result2;
            }
            catch (Exception exception2)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception2.Message
                };
            }
        }

        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            this.lvlDetalles.ItemsSource = null;
            Viaje content = (Viaje)((ListViewItem)sender).Content;
            this.lvlDetalles.ItemsSource = content.listDetalles;
        }

        private CargaCombustible mapForm()
        {
            try
            {
                CargaCombustible combustible = new CargaCombustible
                {
                    idCargaCombustible = (this.ctrVale.Tag == null) ? 0 : ((CargaCombustible)this.ctrVale.Tag).idCargaCombustible,
                    despachador = base.mainWindow.inicio._usuario.personal,
                    fechaEntrada = this.dtpFecha.Value.Value,
                    fechaCombustible = new DateTime?(this.dtpFecha.Value.Value),
                    isExterno = true,
                    kmDespachador = Convert.ToDecimal(this.txtKM.valor),
                    kmInicial = this.txtUltimokm.valor,
                    kmFinal = this.txtKM.valor,
                    kmRecorridos = txtKmRecorridos.valor == 0 ? (chcHubodometro.IsChecked.Value ? Convert.ToDecimal(this.txtKM.valor) - this.txtUltimokm.valor : 0) : txtKmRecorridos.valor,
                    //ltCombustible = Convert.ToDecimal(this.txtLtCombustible.valor),
                    ltCombustibleUtilizado = Convert.ToDecimal(this.txtCombustibleUtilizado.valor),
                    ltCombustibleEST = this.txtCombustibleEST.valor,
                    ltCombustiblePTO = this.txtCombustiblePTO.valor,
                    sello1 = new int?(string.IsNullOrEmpty(this.txtSello1.Text) ? 0 : Convert.ToInt32(this.txtSello1.Text)),
                    sello2 = new int?(string.IsNullOrEmpty(this.txtSello2.Text) ? 0 : Convert.ToInt32(this.txtSello2.Text)),
                    selloReserva = new int?(string.IsNullOrEmpty(this.txtSello3.Text) ? 0 : Convert.ToInt32(this.txtSello3.Text)),
                    tiempoEST = this.txtTiempoEst.Text,
                    tiempoPTO = this.txtTiempoPTO.Text,
                    tiempoTotal = this.txtTiempoTotal.Text,
                    usuarioCrea = base.mainWindow.inicio._usuario.nombreUsuario,
                    //ticket = this.txtTiket.Text,
                    folioImpreso = this.txtFolioImpreso.Text,
                    observacioses = this.txtObservaciones.Text,
                    proveedor = (ProveedorCombustible)this.txtProveedor.SelectedItem,
                    ecm = this.chcECM.IsChecked.Value,
                    zonaOperativa = ctrZonaOperativa.zonaOperativa,
                    idCliente = cbxCliente.SelectedItem == null ? null : (int?)(cbxCliente.SelectedItem as Cliente).clave,
                    isCR = this.isCR,
                    isHubodometo = chcHubodometro.IsChecked.Value
                };
                //combustible.proveedor.precio = this.txtPrecio.valor;
                foreach (var ticket in listaTicket)
                {
                    ticket.cliente = cbxCliente.SelectedItem == null ? null : (cbxCliente.SelectedItem as Cliente);
                    ticket.empresa = cbxEmpresas.empresaSelected;
                    ticket.zonaOperativa = ctrZonaOperativa.zonaOperativa;
                    ticket.Proveedor = (ProveedorCombustible)this.txtProveedor.SelectedItem;
                    ticket.IEPS = .3521m;
                    ticket.isExtra = false;
                    ticket.folioImpreso = this.txtFolioImpreso.Text.Trim();
                }
                combustible.listaTickets = listaTicket;
                combustible.ltCombustible = listaTicket.Sum(s => s.litros);
                combustible.proveedor.precio = listaTicket.Sum(s => s.precio) / listaTicket.Count;
                string cadena = string.Empty;
                foreach (var item in combustible.listaTickets)
                {
                    cadena += item.ticket + ",";
                }
                combustible.ticket = cadena.Trim().Trim(',');

                combustible.listViajes = this.getListaViajes();
                return combustible;
            }
            catch (Exception)
            {
                return null;
            }
        }
        private void mapForm(CargaCombustible result)
        {
            if (result == null)
            {
                this.ctrVale.valor = 0;
                this.ctrVale.IsEnabled = true;
                this.ctrVale.Tag = null;
                if (this._viaje == null)
                {
                    this.txtTractor.limpiar();
                    this.txtRemolque1.limpiar();
                    this.txtDolly.limpiar();
                    this.txtRemolque2.limpiar();
                    this.txtOperador.limpiar();
                    this.dtpFecha.Value = new DateTime?(DateTime.Now);
                }
                //this.txtLtCombustible.valor = decimal.Zero;
                this.txtProveedor.SelectedItem = null;
                //this.txtPrecio.valor = decimal.Zero;
                this.txtSello1.Clear();
                this.txtSello2.Clear();
                this.txtSello3.Clear();
                this.txtKM.valor = decimal.Zero;
                this.txtFolioImpreso.Clear();
                this.txtObservaciones.Clear();
                base.mainWindow.scrollContainer.IsEnabled = true;
                this.txtUltimokm.valor = decimal.Zero;
                this.txtCombustibleUtilizado.valor = decimal.Zero;
                this.txtCombustiblePTO.valor = decimal.Zero;
                this.txtCombustibleEST.valor = decimal.Zero;
                this.txtTiempoTotal.Clear();
                this.txtTiempoEst.Clear();
                this.txtTiempoPTO.Clear();
                this.txtFolioViaje.valor = 0;
                this.txtUltimokm.IsEnabled = false;
            }
            else
            {
                this.lvlViajes.Items.Clear();
                this.lvlDetalles.ItemsSource = null;
                //this.ctrVale.Text = result.ticket;
                this.ctrVale.Tag = result;
                this.chcECM.IsChecked = new bool?(result.ecm);
                if (result.masterOrdServ != null)
                {
                    this.txtTractor.unidadTransporte = result.masterOrdServ.tractor;
                    this.txtOperador.personal = result.masterOrdServ.chofer;
                    this.txtRemolque1.unidadTransporte = result.masterOrdServ.tolva1;
                    this.txtDolly.unidadTransporte = result.masterOrdServ.dolly;
                    this.txtRemolque2.unidadTransporte = result.masterOrdServ.tolva2;
                }
                this.dtpFecha.Value = result.fechaCombustible;
                //this.txtLtCombustible.valor = result.ltCombustible;
                this.txtSello2.Text = result.sello2.ToString();
                this.txtSello1.Text = result.sello1.ToString();
                this.txtSello3.Text = result.selloReserva.ToString();
                this.txtKM.valor = result.kmDespachador;
                this.txtFolioImpreso.Text = result.folioImpreso;
                this.txtObservaciones.Text = result.observacioses;
                base.mainWindow.scrollContainer.IsEnabled = false;
                if (this.ultimoKm == decimal.Zero)
                {
                    this.txtUltimokm.valor = result.kmDespachador - result.kmRecorridos;
                }
                else
                {
                    this.txtUltimokm.valor = this.ultimoKm;
                }
                int num2 = 0;
                foreach (ProveedorCombustible combustible in (IEnumerable)this.txtProveedor.Items)
                {
                    if (combustible.idProveedor == result.proveedor.idProveedor)
                    {
                        this.txtProveedor.SelectedIndex = num2;
                        break;
                    }
                    num2++;
                }
                //this.txtPrecio.valor = result.precioCombustible;
                this.txtCombustibleUtilizado.valor = result.ltCombustibleUtilizado;
                this.txtCombustiblePTO.valor = result.ltCombustiblePTO;
                this.txtCombustibleEST.valor = result.ltCombustibleEST;
                this.txtTiempoTotal.Text = result.tiempoTotal.ToString();
                this.txtTiempoEst.Text = result.tiempoEST.ToString();
                this.txtTiempoPTO.Text = result.tiempoPTO.ToString();
            }
        }

        public override void nuevo()
        {
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
            this.mapForm(null);
            this.grigControles.IsEnabled = false;
            this.chcECM.IsChecked = true;
            stpDetalles.Children.Clear();
            agregarTicket(null, true);
            chcHubodometro.IsChecked = true;
        }

        private void TxtDecimal_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.txtUltimokm.valor <= decimal.Zero)
            {
                this.txtUltimokm.valor = this.ultimoKm;
            }
            decimal num = this.txtKM.valor - this.txtUltimokm.valor;
            if (num <= decimal.Zero)
            {
                this.txtKmRecorridos.valor = decimal.Zero;
            }
            else
            {
                this.txtKmRecorridos.valor = num;
            }
        }

        private void TxtDecimal_TextChanged1(object sender, TextChangedEventArgs e)
        {
            if (!this.chcECM.IsChecked.Value)
            {
                this.txtCombustibleUtilizado.valor = 0;// this.txtLtCombustible.valor;
                this.txtRelleno.valor = 0;
            }
            else
            {
                //this.txtRelleno.valor = this.txtLtCombustible.valor - this.txtCombustibleUtilizado.valor;
            }

        }

        private void txtFolioViaje_KeyDown(object sender, KeyEventArgs e)
        {
            if (((e.Key == Key.Enter) && (this.txtTractor.unidadTransporte != null)) && (this.txtFolioViaje.valor > 0))
            {
                OperationResult resp = new CartaPorteSvc().getCartaPorteById(this.txtFolioViaje.valor);
                if (resp.typeResult == ResultTypes.success)
                {
                    this.addLista((resp.result as List<Viaje>)[0]);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    this.txtFolioViaje.valor = 0;
                }
            }
        }

        private void txtProveedor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.txtProveedor.SelectedItem != null)
            {
                //this.txtPrecio.valor = ((ProveedorCombustible)this.txtProveedor.SelectedItem).precio;
            }
            else
            {
                //this.txtPrecio.valor = decimal.Zero;
            }
        }

        private void txtTiket_KeyDown(object sender, KeyEventArgs e)
        {
            //if ((e.Key == Key.Enter) && !string.IsNullOrEmpty(this.txtTiket.Text.Trim()))
            //{
            //    this.buscarByTiket(this.txtTiket.Text.Trim());
            //}
        }

        private void txtUltimoKm_Click(object sender, RoutedEventArgs e)
        {
            this.consultaUltimoKm();
        }

        private OperationResult validarFomulario()
        {
            try
            {
                OperationResult result = new OperationResult
                {
                    valor = 0,
                    mensaje = "PARA CONTINUAR:" + Environment.NewLine
                };
                if (this.txtTractor.unidadTransporte == null)
                {
                    result.valor = 2;
                    result.mensaje = result.mensaje + "    -*Seleccionar El Tractor" + Environment.NewLine;
                }
                if (this.txtRemolque1.unidadTransporte == null)
                {
                    result.valor = 2;
                    result.mensaje = result.mensaje + "    -*Seleccionar El Remolque 1" + Environment.NewLine;
                }
                if ((this.txtRemolque2.unidadTransporte != null) || (this.txtDolly.unidadTransporte != null))
                {
                    if (this.txtDolly.unidadTransporte == null)
                    {
                        result.valor = 2;
                        result.mensaje = result.mensaje + "    -*Seleccionar El Dolly" + Environment.NewLine;
                    }
                    if (this.txtRemolque2.unidadTransporte == null)
                    {
                        result.valor = 2;
                        result.mensaje = result.mensaje + "    -*Seleccionar El Remolque 2" + Environment.NewLine;
                    }
                }
                if (this.txtOperador.personal == null)
                {
                    result.valor = 2;
                    result.mensaje = result.mensaje + "    -*Seleccionar El Operador" + Environment.NewLine;
                }
                if (!this.dtpFecha.Value.HasValue)
                {
                    result.valor = 2;
                    result.mensaje = result.mensaje + "    -*Seleccionar Una Fecha" + Environment.NewLine;
                }
                if (this.txtProveedor.SelectedItem == null)
                {
                    result.valor = 2;
                    result.mensaje = result.mensaje + "    -*Seleccionar El Proveedor" + Environment.NewLine;
                }
                //if (this.txtPrecio.valor <= decimal.Zero)
                //{
                //    result.valor = 2;
                //    result.mensaje = result.mensaje + "    -*Proporcionar el precio del combustible" + Environment.NewLine;
                //}
                //if (this.txtLtCombustible.valor <= decimal.Zero)
                //{
                //    result.valor = 2;
                //    result.mensaje = result.mensaje + "    -*Proporcionar Lts Combustible" + Environment.NewLine;
                //}
                if (this.txtUltimokm.valor <= decimal.Zero)
                {
                    result.valor = 2;
                    result.mensaje = result.mensaje + "    -*Proporcionar el Ultimo KM(KM INICIAL)" + Environment.NewLine;
                }
                if (chcHubodometro.IsChecked.Value)
                {
                    if (this.txtKM.valor <= decimal.Zero)
                    {
                        result.valor = 2;
                        result.mensaje = result.mensaje + "    -*Proporcionar KM FINAL" + Environment.NewLine;
                    }
                }

                if (string.IsNullOrEmpty(this.txtFolioImpreso.Text.Trim()))
                {
                    result.valor = 2;
                    result.mensaje = result.mensaje + "    -*Proporcionar el Folio Impreso" + Environment.NewLine;
                }
                int num = 0;
                if (cbxEmpresas.empresaSelected.clave != 5)
                {
                    if (string.IsNullOrEmpty(this.txtSello1.Text.Trim()))
                    {
                        result.valor = 2;
                        result.mensaje = result.mensaje + "    -*Proporcionar el Sello 1" + Environment.NewLine;
                    }
                    else if (!int.TryParse(this.txtSello1.Text.Trim(), out num))
                    {
                        result.valor = 2;
                        result.mensaje = result.mensaje + "    -*Proporcionar el Sello 1" + Environment.NewLine;
                    }
                    if (isCR == false)
                    {
                        if (string.IsNullOrEmpty(this.txtSello2.Text.Trim()))
                        {
                            result.valor = 2;
                            result.mensaje = result.mensaje + "    -*Proporcionar el Sello 2" + Environment.NewLine;
                        }
                        else if (!int.TryParse(this.txtSello2.Text.Trim(), out num))
                        {
                            result.valor = 2;
                            result.mensaje = result.mensaje + "    -*Proporcionar el Sello 2" + Environment.NewLine;
                        }
                    }
                }
                

                if (this.txtTractor.unidadTransporte != null)
                {
                    decimal valor = this.txtUltimokm.valor;
                    if (valor > decimal.Zero)
                    {
                        if (this.txtKM.valor < valor)
                        {
                            if (chcHubodometro.IsChecked.Value)
                            {
                                result.valor = 2;
                                if (isCR == false)
                                {
                                    result.mensaje = result.mensaje + "    -*El KM FINAL no puede ser menor al ultimo KM" + Environment.NewLine;
                                }
                                else
                                {
                                    result.mensaje = result.mensaje + "    -*La H. INICIAL no puede ser menor a la H. FINAL" + Environment.NewLine;
                                }
                            }

                        }
                        valor += 5000M;
                        if (this.txtKM.valor > valor)
                        {
                            result.valor = 2;
                            result.mensaje = result.mensaje + "    -*El KM rebasa el Maximo permitido" + Environment.NewLine;
                        }
                    }
                }
                //if (this.txtLtCombustible.valor > 1600M)
                //{
                //    result.valor = 2;
                //    result.mensaje = result.mensaje + "    -*El combustible rebasa el limite permitido" + Environment.NewLine;
                //}
                char[] separator = new char[] { ':' };
                string[] strArray = this.txtTiempoTotal.Text.Split(separator);
                try
                {
                    TimeSpan span = new TimeSpan(Convert.ToInt32(strArray[0]), Convert.ToInt32(strArray[1]), Convert.ToInt32(strArray[2]));
                }
                catch (Exception)
                {
                    return new OperationResult
                    {
                        valor = 3,
                        mensaje = "El valor del tiempo Total no es valido"
                    };
                }
                char[] chArray2 = new char[] { ':' };
                strArray = this.txtTiempoPTO.Text.Split(chArray2);
                try
                {
                    TimeSpan span2 = new TimeSpan(Convert.ToInt32(strArray[0]), Convert.ToInt32(strArray[1]), Convert.ToInt32(strArray[2]));
                }
                catch (Exception)
                {
                    return new OperationResult
                    {
                        valor = 3,
                        mensaje = "El valor del tiempo PTO no es valido"
                    };
                }
                char[] chArray3 = new char[] { ':' };
                strArray = this.txtTiempoEst.Text.Split(chArray3);
                try
                {
                    TimeSpan span3 = new TimeSpan(Convert.ToInt32(strArray[0]), Convert.ToInt32(strArray[1]), Convert.ToInt32(strArray[2]));
                }
                catch (Exception)
                {
                    return new OperationResult
                    {
                        valor = 3,
                        mensaje = "El valor del tiempo EST no es valido"
                    };
                }
                return result;
            }
            catch (Exception exception4)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception4.Message
                };
            }
        }

        Usuario usuario = null;
        private decimal ultimoKm
        {
            get
            {
                if (this.cbxUnidades.SelectedItem == null)
                {
                    this.txtUltimokm.IsEnabled = true;
                    return decimal.Zero;

                }
                OperationResult result = new RegistroKilometrajeSvc().getUltimoKM((cbxUnidades.SelectedItem as UnidadTransporte).clave, this.dtpFecha.Value.Value);
                if (result.typeResult == ResultTypes.success)
                {
                    this.txtUltimokm.IsEnabled = false;
                    return (decimal)result.result;
                }
                this.txtUltimokm.IsEnabled = true;
                return decimal.Zero;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            txtTractor.limpiar();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            txtRemolque1.limpiar();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            txtDolly.limpiar();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            txtRemolque2.limpiar();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            txtOperador.limpiar();
        }

        private void DtpFecha_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            consultaUltimoKm();
        }
        private void BtnAñadirDetalle_Click(object sender, RoutedEventArgs e)
        {
            agregarTicket();
        }
        private void agregarTicket(TicketsExterno tickets = null, bool primero = false)
        {
            try
            {
                tickets = tickets ?? new TicketsExterno();
                StackPanel stpTicket = new StackPanel { Tag = tickets, Orientation = Orientation.Horizontal, Margin = new Thickness(2) };

                TextBox txtTicket = new TextBox
                {
                    Tag = tickets,
                    Width = 114,
                    Margin = new Thickness(2),
                    FontWeight = FontWeights.Bold,
                    CharacterCasing = CharacterCasing.Upper,
                    HorizontalContentAlignment = HorizontalAlignment.Center
                };
                txtTicket.TextChanged += TxtTicket_TextChanged;

                controlDecimal ctrLitros = new controlDecimal { Width = 120, Tag = tickets, FontWeight = FontWeights.Bold };
                ctrLitros.txtDecimal.Tag = ctrLitros;
                ctrLitros.txtDecimal.TextChanged += TxtDecimalLitros_TextChanged2;

                controlDecimal ctrPrecio = new controlDecimal { Width = 120, Tag = tickets, FontWeight = FontWeights.Bold };
                ctrPrecio.txtDecimal.Tag = ctrPrecio;
                ctrPrecio.txtDecimal.TextChanged += TxtDecimalPrecios_TextChanged2;

                controlDecimal ctrImporte = new controlDecimal
                {
                    Name = "ctrImporte",
                    Width = 120,
                    Tag = tickets,
                    FontWeight = FontWeights.Bold,
                    soloLectura = true
                };

                stpTicket.Children.Add(txtTicket);
                stpTicket.Children.Add(ctrLitros);
                stpTicket.Children.Add(ctrPrecio);
                stpTicket.Children.Add(ctrImporte);
                if (!primero)
                {
                    Button btnQuitar = new Button { Tag = stpTicket, Content = "X", Background = Brushes.Red, Foreground = Brushes.White, VerticalAlignment = VerticalAlignment.Center };
                    btnQuitar.Click += BtnQuitar_Click;
                    stpTicket.Children.Add(btnQuitar);
                }
                stpDetalles.Children.Add(stpTicket);
                calcular();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private void BtnQuitar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    Button btnQuitar = sender as Button;
                    stpDetalles.Children.Remove(btnQuitar.Tag as StackPanel);
                    calcular();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private void TxtDecimalLitros_TextChanged2(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    TextBox txtDecimal = sender as TextBox;
                    controlDecimal ctrDecimal = txtDecimal.Tag as controlDecimal;
                    TicketsExterno tickets = ctrDecimal.Tag as TicketsExterno;
                    tickets.litros = ctrDecimal.valor;
                    calcular();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private void TxtDecimalPrecios_TextChanged2(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    TextBox txtDecimal = sender as TextBox;
                    controlDecimal ctrDecimal = txtDecimal.Tag as controlDecimal;
                    TicketsExterno tickets = ctrDecimal.Tag as TicketsExterno;
                    tickets.precio = ctrDecimal.valor;
                    calcular();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private void TxtTicket_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    TextBox txt = sender as TextBox;
                    TicketsExterno tickets = txt.Tag as TicketsExterno;
                    tickets.ticket = txt.Text;
                    calcular();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        List<TicketsExterno> listaTicket
        {
            get
            {
                return stpDetalles.Children.Cast<StackPanel>().ToList().Select(s => s.Tag as TicketsExterno).ToList();
            }
        }
        private void calcular()
        {
            try
            {
                var listaTicket = this.listaTicket;
                lblContadorTickets.Content = listaTicket.Count().ToString("N0");
                lblSumaLitros.Content = listaTicket.Sum(s => s.litros).ToString("N4");
                lblSumaImportes.Content = listaTicket.Sum(s => s.importe).ToString("C4");

                ponerImportes();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private void ponerImportes()
        {
            try
            {
                foreach (StackPanel stpDetalles in stpDetalles.Children.Cast<StackPanel>().ToList())
                {
                    foreach (object stpTicket in stpDetalles.Children.Cast<object>().ToList())
                    {
                        if (stpTicket is controlDecimal)
                        {
                            controlDecimal ctrDecimal = stpTicket as controlDecimal;
                            if (ctrDecimal.Name == "ctrImporte")
                            {
                                TicketsExterno tickets = ctrDecimal.Tag as TicketsExterno;
                                ctrDecimal.valor = tickets.importe;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        bool isCR = false;
        private void CbxUnidades_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender != null)
            {
                chcECM.IsChecked = true;
                if (cbxUnidades.SelectedItem != null)
                {
                    UnidadTransporte unidad = cbxUnidades.SelectedValue as UnidadTransporte;
                    if (unidad.clave.Contains("CR"))
                    {
                        lblKmInicial.Content = "H. INICIAL:";
                        lblKmFinal.Content = "H. FINAL:";
                        lblKmRecorridos.Content = "H. OPERATIVAS:";
                        chcECM.IsChecked = false;
                        chcECM.IsEnabled = false;
                        stpSellos.Visibility = Visibility.Hidden;
                        isCR = true;
                    }
                    else
                    {
                        lblKmInicial.Content = "KM INICIAL:";
                        lblKmFinal.Content = "KM FINAL:";
                        lblKmRecorridos.Content = "KM RECORRIDOS:";
                        chcECM.IsEnabled = true;
                        stpSellos.Visibility = Visibility.Visible;
                        isCR = false;
                    }
                }
                else
                {
                    chcECM.IsEnabled = true;
                    stpSellos.Visibility = Visibility.Visible;
                    this.isCR = false;
                }
            }
            consultaUltimoKm();
        }

        private void ChcHubodometro_Checked(object sender, RoutedEventArgs e)
        {
            this.txtKM.IsEnabled = true;
        }

        private void ChcHubodometro_Unchecked(object sender, RoutedEventArgs e)
        {
            this.txtKM.IsEnabled = false;
            this.txtKM.valor = 0;
        }
    }
}
