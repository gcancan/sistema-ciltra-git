﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para BitacoraUbicacionTractores.xaml
    /// </summary>
    public partial class ReporteEstatusUnidades : ViewBase
    {
        public ReporteEstatusUnidades()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            Usuario usuario = mainWindow.usuario;
            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresa.loaded(usuario.empresa, (new PrivilegioSvc().consultarPrivilegio("isAdmin", usuario.idUsuario).typeResult == ResultTypes.success));
            nuevo();

            cbxIntervalo.Items.Add(20);
            cbxIntervalo.Items.Add(30);
            cbxIntervalo.Items.Add(60);
            cbxIntervalo.Items.Add(90);
            cbxIntervalo.Items.Add(120);
            cbxIntervalo.SelectedIndex = 0;

            cbxZonasOperativas.SelectedItem = cbxZonasOperativas.Items.Cast<ZonaOperativa>().ToList().Find(s => s.idZonaOperativa == usuario.zonaOperativa.idZonaOperativa);
            cbxOperacion.SelectedItem = cbxOperacion.Items.Cast<OperacionFletera>().ToList().Find(s => s.cliente.clave == usuario.cliente.clave);
        }
        public override void nuevo()
        {
            base.nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
        }
        Empresa empresa = null;
        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            empresa = ctrEmpresa.empresaSelected;
            cargarZonasOperativas();
        }
        List<ZonaOperativa> listaZonaOpertiva = new List<ZonaOperativa>();
        void cargarZonasOperativas()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new ZonaOperativaSvc().getAllZonasOperativas();
                if (resp.typeResult == ResultTypes.success)
                {
                    listaZonaOpertiva = resp.result as List<ZonaOperativa>;
                    cbxZonasOperativas.ItemsSource = listaZonaOpertiva;
                }
                else
                {
                    cbxZonasOperativas.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {
                //ImprimirMensaje.imprimir(ex);
                Dispatcher.Invoke(() =>
                {
                    //lblAccion.Content = ex.Message;
                    //lblFecha.Content = DateTime.Now.ToString("HH:mm:ss");
                });
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private async void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            await Task.Run(async () => await this.buscarResultados());
        }
        public async Task buscarResultados()
        {
            try
            {
                Dispatcher.Invoke(() =>
                {
                    stpResultados.Children.Clear();
                });
                _lista = new List<BitacoraUbicacionesCSI>();
                List<string> listaZonas = new List<string>();
                List<string> listaOperaciones = new List<string>();
                DateTime fecha = DateTime.Now;
                DateTime fechaFinal = DateTime.Now;
                int minutos = 0;
                Dispatcher.Invoke(() =>
                {
                    listaZonas = (from s in this.cbxZonasOperativas.SelectedItems.Cast<ZonaOperativa>().ToList<ZonaOperativa>() select s.idZonaOperativa.ToString()).ToList<string>();
                    listaOperaciones = (from s in this.cbxOperacion.SelectedItems.Cast<OperacionFletera>().ToList<OperacionFletera>() select s.cliente.clave.ToString()).ToList<string>();
                    fecha = ctrFechas.fechaInicial;
                    fechaFinal = ctrFechas.fechaFinal;
                    minutos = (int)cbxIntervalo.SelectedItem;
                });

                List<BitacoraUbicacionesCSI> lista = new List<BitacoraUbicacionesCSI>();

                bool salir = true;
                while (salir)
                {
                    BitacoraUbicacionesCSI bitacoraUbicaciones = new BitacoraUbicacionesCSI()
                    {
                        fechaInicio = fecha,
                        fechaFin = fecha.AddMinutes(minutos - 1),
                        listaResumen = new List<ResumenDetallesRegistrosCSI>()
                    };
                    fecha = fecha.AddMinutes(minutos);
                    if (bitacoraUbicaciones.fechaFin > fechaFinal)
                    {
                        salir = false;
                        bitacoraUbicaciones.fechaFin = Convert.ToDateTime(string.Format("{0}/{1}/{2} 23:59", fecha.Day, fecha.Month, fecha.Year));
                    }
                    lista.Add(bitacoraUbicaciones);
                }

                Dispatcher.Invoke(() =>
                {
                    popMensaje.IsOpen = true;
                });
                bool terminar = false;
                foreach (var item in lista)
                {
                    if (item.fechaInicio > DateTime.Now)
                    {
                        item.fechaInicio = DateTime.Now;
                        terminar = true;
                    }
                    Dispatcher.Invoke(() =>
                    {
                        lblMensaje.Content = string.Format("Buscando ... {0})", item.tituloFecha);

                    });
                    var resp = new BitacoraUbicacionesCSISvc().getBitacoraUbicacionCSI(item.fechaInicio, item.fechaFin, empresa.clave, listaZonas, listaOperaciones);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        
                        List<DetallesRegistosCSI> listaDetalles = resp.result as List<DetallesRegistosCSI>;
                        List<ResumenDetallesRegistrosCSI> listaResumen = (from g in listaDetalles
                                                                          group g by new
                                                                          {
                                                                              g.idTractor,
                                                                              g.tipoUbicacionBitacora
                                                                          }
                                                                          into grupo
                                                                          select new ResumenDetallesRegistrosCSI
                                                                          {
                                                                              idTractor = grupo.Select(s => s.idTractor).First(),
                                                                              tipoUbicacionBitacora = grupo.Select(s => s.tipoUbicacionBitacora).First(),
                                                                              listaDetalles = grupo.ToList()
                                                                          }
                        ).ToList();
                        item.listaResumen = listaResumen;

                        await Task.Run(async () => await this.llenarControles(item));

                        if (terminar)
                        {
                            return;
                        }

                    }
                    else if (resp.typeResult != ResultTypes.recordNotFound)
                    {
                        ImprimirMensaje.imprimir(resp);
                        return;
                    }
                }

                //Dispatcher.Invoke(() =>
                //{
                //    llenarControles(lista);
                //});
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Dispatcher.Invoke(() =>
                {
                    popMensaje.IsOpen = false;
                    lblMensaje.Content = string.Format("Buscando ... ");
                });
            }
        }
        List<BitacoraUbicacionesCSI> _lista = new List<BitacoraUbicacionesCSI>();
        private async Task llenarControles(BitacoraUbicacionesCSI bitacora)
        {
            try
            {
                Dispatcher.Invoke(() =>
                {
                    _lista.Add(bitacora);
                    StackPanel stpControles = new StackPanel { Margin = new Thickness(1) };

                    TextBox lblHeader = new TextBox
                    {
                        Text = bitacora.tituloFecha,
                        BorderBrush = Brushes.Blue,
                        Foreground = Brushes.DarkBlue,
                        BorderThickness = new Thickness(1),
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        FontWeight = FontWeights.Bold,
                        Background = null,
                        IsReadOnly = true,
                        Width = 100,
                        Height = 40,
                        TextWrapping = TextWrapping.Wrap
                    };
                    stpControles.Children.Add(lblHeader);

                    Label lblPlanta = new Label
                    {
                        Content = bitacora.cantEnPlanta,
                        Width = 100,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        FontWeight = FontWeights.Medium,
                        Foreground = Brushes.SteelBlue,
                        BorderBrush = Brushes.SteelBlue,
                        BorderThickness = new Thickness(1)
                    };
                    stpControles.Children.Add(lblPlanta);

                    Label lblBase = new Label
                    {
                        Content = bitacora.cantEnBase,
                        Width = 100,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        FontWeight = FontWeights.Medium,
                        Foreground = Brushes.SteelBlue,
                        BorderBrush = Brushes.SteelBlue,
                        BorderThickness = new Thickness(1)
                    };
                    stpControles.Children.Add(lblBase);

                    Label lblGranja = new Label
                    {
                        Content = bitacora.cantEnGranja,
                        Width = 100,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        FontWeight = FontWeights.Medium,
                        Foreground = Brushes.SteelBlue,
                        BorderBrush = Brushes.SteelBlue,
                        BorderThickness = new Thickness(1)
                    };
                    stpControles.Children.Add(lblGranja);

                    Label lblTrayecto = new Label
                    {
                        Content = bitacora.cantEnTrayecto,
                        Width = 100,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        FontWeight = FontWeights.Medium,
                        Foreground = Brushes.SteelBlue,
                        BorderBrush = Brushes.SteelBlue,
                        BorderThickness = new Thickness(1)
                    };
                    stpControles.Children.Add(lblTrayecto);

                    Label lblTallerExterno = new Label
                    {
                        Content = bitacora.cantEnTallerExterno,
                        Width = 100,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        FontWeight = FontWeights.Medium,
                        Foreground = Brushes.SteelBlue,
                        BorderBrush = Brushes.SteelBlue,
                        BorderThickness = new Thickness(1)
                    };
                    stpControles.Children.Add(lblTallerExterno);

                    Label lblSuma = new Label
                    {
                        Content = bitacora.sumaUbicaciones,
                        Width = 100,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        FontWeight = FontWeights.Bold,
                        Foreground = Brushes.Blue,
                        BorderBrush = Brushes.Blue,
                        BorderThickness = new Thickness(1)
                    };
                    stpControles.Children.Add(lblSuma);

                    stpResultados.Children.Add(stpControles);                   
                });
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private void llenarControles(List<BitacoraUbicacionesCSI> lista)
        {
            try
            {
                _lista = lista;
                Cursor = Cursors.Wait;
                stpResultados.Children.Clear();
                foreach (BitacoraUbicacionesCSI bitacora in lista)
                {
                    StackPanel stpControles = new StackPanel { Margin = new Thickness(1) };

                    TextBox lblHeader = new TextBox
                    {
                        Text = bitacora.tituloFecha,
                        BorderBrush = Brushes.Blue,
                        Foreground = Brushes.DarkBlue,
                        BorderThickness = new Thickness(1),
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        FontWeight = FontWeights.Bold,
                        Background = null,
                        IsReadOnly = true,
                        Width = 100,
                        Height = 40,
                        TextWrapping = TextWrapping.Wrap
                    };
                    stpControles.Children.Add(lblHeader);

                    Label lblPlanta = new Label
                    {
                        Content = bitacora.cantEnPlanta,
                        Width = 100,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        FontWeight = FontWeights.Medium,
                        Foreground = Brushes.SteelBlue,
                        BorderBrush = Brushes.SteelBlue,
                        BorderThickness = new Thickness(1)
                    };
                    stpControles.Children.Add(lblPlanta);

                    Label lblBase = new Label
                    {
                        Content = bitacora.cantEnBase,
                        Width = 100,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        FontWeight = FontWeights.Medium,
                        Foreground = Brushes.SteelBlue,
                        BorderBrush = Brushes.SteelBlue,
                        BorderThickness = new Thickness(1)
                    };
                    stpControles.Children.Add(lblBase);

                    Label lblGranja = new Label
                    {
                        Content = bitacora.cantEnGranja,
                        Width = 100,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        FontWeight = FontWeights.Medium,
                        Foreground = Brushes.SteelBlue,
                        BorderBrush = Brushes.SteelBlue,
                        BorderThickness = new Thickness(1)
                    };
                    stpControles.Children.Add(lblGranja);

                    Label lblTrayecto = new Label
                    {
                        Content = bitacora.cantEnTrayecto,
                        Width = 100,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        FontWeight = FontWeights.Medium,
                        Foreground = Brushes.SteelBlue,
                        BorderBrush = Brushes.SteelBlue,
                        BorderThickness = new Thickness(1)
                    };
                    stpControles.Children.Add(lblTrayecto);

                    Label lblSuma = new Label
                    {
                        Content = bitacora.sumaUbicaciones,
                        Width = 100,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        FontWeight = FontWeights.Bold,
                        Foreground = Brushes.Blue,
                        BorderBrush = Brushes.Blue,
                        BorderThickness = new Thickness(1)
                    };
                    stpControles.Children.Add(lblSuma);
                    stpResultados.Children.Add(stpControles);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        List<OperacionFletera> listaOperaciones = new List<OperacionFletera>();
        private void cbxZonasOperativas_ItemSelectionChanged(object sender, Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventArgs e)
        {
            try
            {
                cbxOperacion.ItemsSource = null;
                //chcTodosOperacion.IsChecked = false;
                var listaId = (from s in this.cbxZonasOperativas.SelectedItems.Cast<ZonaOperativa>().ToList<ZonaOperativa>() select s.idZonaOperativa.ToString()).ToList<string>();
                if (listaId != null)
                {
                    var resp = new ClienteSvc().getClientesByZonasOperativas(listaId, empresa.clave);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listaOperaciones = resp.result as List<OperacionFletera>;
                        cbxOperacion.ItemsSource = listaOperaciones;
                    }
                    else if (resp.typeResult != ResultTypes.recordNotFound)
                    {
                        //ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception ex)
            {
                //ImprimirMensaje.imprimir(ex);
                Dispatcher.Invoke(() =>
                {
                    ImprimirMensaje.imprimir(ex);
                });
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                CheckBox chc = sender as CheckBox;
                switch (chc.Name)
                {
                    case "chcTodosZonaOperativa":
                        cbxZonasOperativas.SelectedItems.Clear();
                        if (chc.IsChecked.Value)
                        {
                            foreach (var item in cbxZonasOperativas.Items)
                            {
                                cbxZonasOperativas.SelectedItems.Add(item);
                            }
                        }
                        break;
                    case "chcTodosOperacion":
                        cbxOperacion.SelectedItems.Clear();
                        if (chc.IsChecked.Value)
                        {
                            foreach (var item in cbxOperacion.Items)
                            {
                                cbxOperacion.SelectedItems.Add(item);
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                //ImprimirMensaje.imprimir(ex);
                Dispatcher.Invoke(() =>
                {
                    ImprimirMensaje.imprimir(ex);
                });
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void cbxOperacion_ItemSelectionChanged(object sender, Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;

            }
            catch (Exception ex)
            {
                //ImprimirMensaje.imprimir(ex);
                Dispatcher.Invoke(() =>
                {
                    ImprimirMensaje.imprimir(ex);
                });
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void btnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                new ReportView().reporteEstatusUnidades(_lista);
            }
            catch (Exception ex)
            {
                //ImprimirMensaje.imprimir(ex);
                Dispatcher.Invoke(() =>
                {
                    //lblAccion.Content = ex.Message;
                    //lblFecha.Content = DateTime.Now.ToString("HH:mm:ss");
                });
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
