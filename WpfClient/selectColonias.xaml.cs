﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using Core.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectColonias.xaml
    /// </summary>
    public partial class selectColonias : Window
    {
        public selectColonias()
        {
            InitializeComponent();
        }
        string nombreColonia = string.Empty;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtBuscador.Text = nombreColonia;
        }
        public Colonia getAllColoniaByNombre(string nombreColonia)
        {
            try
            {
                buscarTodasLasColonias();
                this.nombreColonia = nombreColonia;
                bool? result = ShowDialog();
                if (result.Value && lvlColonias.SelectedItem != null)
                {
                    return (lvlColonias.SelectedItem as ListViewItem).Content as Colonia;
                }
                return null;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }

        private void buscarTodasLasColonias()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new DireccionSvc().getALLColoniasById();
                if (resp.typeResult == ResultTypes.success)
                {
                    mapListaColonias(resp.result as List<Colonia>);
                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapListaColonias(List<Colonia> listColonias)
        {
            try
            {
                lvlColonias.ItemsSource = null;
                List<ListViewItem> list = new List<ListViewItem>();
                foreach (Colonia col in listColonias)
                {
                    ListViewItem item = new ListViewItem
                    {
                        Content = col
                    };
                    item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                    list.Add(item);
                }
                this.lvlColonias.ItemsSource = list;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlColonias.ItemsSource);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtBuscador.Text) ||
            ((((Colonia)(item as ListViewItem).Content).nombre.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(this.lvlColonias.ItemsSource).Refresh();
        }
    }
}
