﻿using LinqToExcel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
namespace WpfClient
{
    public class LeerExcel
    {
        public OperationResult leerDocumento(string archivo)
        {
            try
            {
                List<TareaProgramada> resultado = new List<TareaProgramada>();
                var book = new ExcelQueryFactory(archivo);
                var list = book.GetWorksheetNames();

                selectHojasCalculo select = new selectHojasCalculo((List<string>)list);
                //select.Title = ""
                var hoja = select.selectHoja();

                if (hoja != "")
                {
                    var findFecha = (from c in book.WorksheetRangeNoHeader("B2", "B2", hoja) select c).ToList();

                    var line = (from c in book.Worksheet(hoja) select c).ToList();

                    string limite = "N" + line.Count().ToString();

                    DateTime fecha = new DateTime();
                    foreach (var item in findFecha)
                    {
                        //DateTime fecha = Convert.ToDateTime(findFecha[0]);          
                        fecha = Convert.ToDateTime(item[0]);
                    }

                    resultado = (from row in book.WorksheetRange("A4", limite, hoja)
                                 let item = new TareaProgramada()
                                 {
                                     idTareaProgramada = 0,
                                     //idProducto = row["Product"].Cast<int>(),
                                     DescripcionProducto = row["Denominación"].Cast<string>(),
                                     valorProgramado = row["Cantidad entrega"].Cast<decimal>(),
                                     destino = row["Nombre destinatario de mercancías"].Cast<string>(),
                                     remision = row["Entrega"].Cast<int>(),
                                     hoja = hoja,
                                     fecha = fecha
                                 }
                                 where item.remision > 0
                                 select item).ToList();
                    book.Dispose();
                    return new OperationResult { mensaje = "Se Cargo correctamente el archivo", result = resultado, valor = 0 };
                }
                else
                {
                    book.Dispose();
                    return new OperationResult { mensaje = "No se eligio una hoja del libro de Excel", result = null, valor = 2 };
                }
            }
            catch (Exception e)
            {
                return new OperationResult { mensaje = "Ocurrio un error al cargar el archivo" + Environment.NewLine + e.Message, result = null, valor = 1 };
            }
        }

        public OperationResult leerDocumentoComparacionValesCombustible(string archivo)
        {
            try
            {
                List<ComparacionValesCombustible> resultado = new List<ComparacionValesCombustible>();
                var book = new ExcelQueryFactory(archivo);
                var list = book.GetWorksheetNames();

                selectHojasCalculo select = new selectHojasCalculo((List<string>)list);
                var hoja = select.selectHoja();
                if (hoja != "")
                {
                    resultado = (from row in book.Worksheet(hoja)
                                 let item = new ComparacionValesCombustible()
                                 {
                                     id = 0,
                                     mes = row["MES"].Cast<string>(),
                                     año = row["AÑO"].Cast<int>(),
                                     unidadTransporte = row["UNIDAD"].Cast<string>(),
                                     vale = row["VALE"].Cast<string>(),
                                     carga = row["LITROS"].Cast<decimal>()
                                 }
                                 select item).ToList();
                    book.Dispose();
                    return new OperationResult { mensaje = "Se Cargo correctamente el archivo", result = resultado, valor = 0 };
                }
                else
                {
                    book.Dispose();
                    return new OperationResult { mensaje = "No se eligio una hoja del libro de Excel", result = null, valor = 2 };
                }
            }
            catch (Exception e)
            {
                return new OperationResult { mensaje = "Ocurrio un error al cargar el archivo" + Environment.NewLine + e.Message, result = null, valor = 1 };
            }
        }
    }
}
