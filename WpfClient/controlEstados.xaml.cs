﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para controlEstados.xaml
    /// </summary>
    public partial class controlEstados : UserControl
    {
        public controlEstados()
        {
            InitializeComponent();
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                cbxEstados.Width = this.Width - 5;
                var resp = new EstadoSvc().getEstadosAllorById();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxEstados.ItemsSource = resp.result as List<Estado>;
                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void cbxEstados_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            estado = cbxEstados.SelectedItem as Estado;
        }
        public Estado estado
        {
            get
            {
                if (cbxEstados.SelectedItem != null)
                {
                    return cbxEstados.SelectedItem as Estado;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                Estado est = value;
                if (est == null)
                {
                    cbxEstados.SelectedItem = null;
                }
                else
                {
                    int i = 0;
                    foreach (Estado estado in cbxEstados.ItemsSource)
                    {
                        if (estado.idEstado == est.idEstado)
                        {
                            cbxEstados.SelectedIndex = i;
                            break;
                        }
                        i++;
                    }
                }               
            }
        }
    }
}
