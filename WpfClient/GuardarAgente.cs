﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfClient
{
    public static class GuardarAgente
    {
        public static bool guardarAgente(string codigoAgente, string nombreAgente)
        {
            int result = Declaraciones.fBuscaAgente(codigoAgente);
            //MessageBox.Show(result.ToString());
            if (hayError(result))
            {
                if (result == 3)
                {
                    if (hayError(Declaraciones.fInsertaAgente()))
                    {
                        return false;
                    }
                    else
                    {
                        Declaraciones.fSetDatoAgente("CCODIGOA01", codigoAgente);
                        Declaraciones.fSetDatoAgente("CNOMBREA01", nombreAgente);
                        Declaraciones.fSetDatoAgente("CFECHAAL01", DateTime.Now.ToString("MM/dd/yyyy"));
                        Declaraciones.fSetDatoAgente("CTIPOAGE01", "1");
                        // Tipo Agente 1=ventas 2= ventas/cobro 3= cobro

                        if (hayError(Declaraciones.fGuardaAgente()))
                        {

                            return false;
                        }
                        else
                        {
                            //MessageBox.Show("Agente nuevo");
                            return true;
                        }
                    }
                }
                else
                {
                    return false;
                }               
            }
            else
            {
                return true;
            }
        }

        internal static bool hayError(int error)
        {
            bool resultado;

            if (error == 3)
            {
                return true;
            }
            else if (error != 0)
            {
                Declaraciones.MuestraError(error);
                resultado = true;
            }
            else
            {
                resultado = false;
            }
            return resultado;
        }
    }
}
