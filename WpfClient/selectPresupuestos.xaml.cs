﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;


namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectPresupuestos.xaml
    /// </summary>
    public partial class selectPresupuestos : MetroWindow
    {
        public selectPresupuestos()
        {
            InitializeComponent();
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {

        }
        public Presupuesto buscarPresupuesto()
        {
            try
            {
                getAllPresupuestos();
                bool? resul = ShowDialog();
                if (resul.Value && lvlPresupuestos.SelectedItem != null)
                {
                    return (lvlPresupuestos.SelectedItem as ListViewItem).Content as Presupuesto;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        void getAllPresupuestos()
        {
            try
            {
                var resp = new PresupuestoSvc().getAllPresupuestos();
                if (resp.typeResult == ResultTypes.success)
                {
                    llenarLista(resp.result as List<Presupuesto>);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void llenarLista(List<Presupuesto> list)
        {
            lvlPresupuestos.ItemsSource = null;
            List<ListViewItem> lista = new List<ListViewItem>();
            foreach (var item in list)
            {
                ListViewItem lvl = new ListViewItem
                {
                    Content = item,
                };
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                lista.Add(lvl);
            }
            lvlPresupuestos.ItemsSource = lista;
            CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlPresupuestos.ItemsSource);
            defaultView.Filter = new Predicate<object>(this.UserFilter);
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }
        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtFind.Text) || ((((Presupuesto)(item as ListViewItem).Content).cliente.nombre.ToString().IndexOf(this.txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0) ||
            (((Presupuesto)(item as ListViewItem).Content).anio.ToString().IndexOf(this.txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                this.lvlPresupuestos.Focus();
            }
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(this.lvlPresupuestos.ItemsSource).Refresh();
        }

        private void MetroWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                base.DialogResult = false;
            }
        }

        private void lvlPresupuestos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                base.DialogResult = true;
            }
        }
    }
}
