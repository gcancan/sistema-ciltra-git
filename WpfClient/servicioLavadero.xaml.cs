﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using CoreFletera.Interfaces;
    using CoreFletera.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Text.RegularExpressions;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Markup;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    public partial class servicioLavadero : WpfClient.ViewBase
    {
        private Personal personal = new Personal();
        private DateTime? f = null;
        public servicioLavadero()
        {
            this.InitializeComponent();
        }
        private void agregarInsumo(ProductoInsumo insumoVal, Actividad _actividad = null)
        {
            if (this.scrollContenedor.Content == null)
            {
                StackPanel panel4 = new StackPanel
                {
                    Orientation = Orientation.Vertical
                };
                this.scrollContenedor.Content = panel4;
            }
            bool flag = true;
            Actividad actividad = null;
            if (this.lvlActividades.SelectedItem == null)
            {
                actividad = _actividad;
            }
            else
            {
                actividad = (Actividad)((ListViewItem)this.lvlActividades.SelectedItem).Content;
            }
            this.txtPersonal.personal = actividad.personal1;
            this.txtPersonal2.personal = actividad.personal2;
            this.txtPersonal3.personal = actividad.personal3;
            ProductosActividad actividad2 = new ProductosActividad
            {
                cantidad = decimal.One,
                cantMax = 10M,
                cantMin = decimal.One,
                costo = insumoVal.precio,
                existencia = decimal.One,
                nombre = insumoVal.nombre,
                codigo = insumoVal.clave,
                idActicidad = actividad.idActividad,
                idProducto = insumoVal.idInsumo,
                um = "",
                extra = decimal.Zero,
                llantera = insumoVal.llantera,
                lavadero = insumoVal.lavadero,
                taller = insumoVal.taller
            };
            StackPanel content = (StackPanel)this.scrollContenedor.Content;
            List<StackPanel> list = content.Children.Cast<StackPanel>().ToList<StackPanel>();
            foreach (StackPanel panel5 in list)
            {
                if (actividad2.idProducto == ((ProductosActividad)panel5.Tag).idProducto)
                {
                    return;
                }
            }
            StackPanel element = new StackPanel
            {
                VerticalAlignment = VerticalAlignment.Top,
                HorizontalAlignment = HorizontalAlignment.Left
            };
            element.Tag = actividad2;
            element.Orientation = Orientation.Horizontal;
            Label label1 = new Label
            {
                Content = actividad2.nombre,
                Width = 250.0,
                Height = 24.0
            };
            element.Children.Add(label1);
            TextBox box = new TextBox
            {
                Width = 60.0,
                Height = 22.0,
                Text = "0"
            };
            box.KeyDown += new KeyEventHandler(this.Txt_KeyDown);
            box.TextChanged += new TextChangedEventHandler(this.Txt_TextChanged);
            element.Children.Add(box);
            Button button = new Button
            {
                Margin = new Thickness(50.0, 2.0, 2.0, 2.0),
                Tag = actividad2,
                Content = "QUITAR",
                Width = 100.0,
                Height = 28.0,
                FontWeight = FontWeights.Medium,
                Foreground = Brushes.White,
                Background = Brushes.Red
            };
            button.Click += new RoutedEventHandler(this.Btn_Click);
            StackPanel panel3 = (StackPanel)this.scrollContenedor.Content;
            panel3.Children.Add(element);
            this.scrollContenedor.Content = panel3;
            this.spControles.IsEnabled = flag;
        }

        private void añadirInspecciones(List<Inspecciones> listaInspecciones, bool habilitar)
        {
            StackPanel panel = new StackPanel
            {
                Orientation = Orientation.Vertical,
                VerticalAlignment = VerticalAlignment.Top,
                HorizontalAlignment = HorizontalAlignment.Left,
                Margin = new Thickness(8.0)
            };
            foreach (Inspecciones inspecciones in listaInspecciones)
            {
                StackPanel element = new StackPanel
                {
                    IsEnabled = !inspecciones.checado,
                    Orientation = Orientation.Vertical,
                    VerticalAlignment = VerticalAlignment.Top,
                    HorizontalAlignment = HorizontalAlignment.Left,
                    Margin = new Thickness(4.0)
                };
                element.Tag = inspecciones;
                CheckBox box = new CheckBox
                {
                    Tag = inspecciones,
                    Content = inspecciones.nombreInspeccion,
                    IsEnabled = !inspecciones.checado,
                    IsChecked = new bool?(inspecciones.checado)
                };
                if (!habilitar)
                {
                    box.IsEnabled = habilitar;
                }
                element.Children.Add(box);
                foreach (ProductoInsumo insumo in inspecciones.listaInsumos)
                {
                    StackPanel panel3 = new StackPanel
                    {
                        Orientation = Orientation.Horizontal,
                        VerticalAlignment = VerticalAlignment.Top,
                        HorizontalAlignment = HorizontalAlignment.Left,
                        Margin = new Thickness(2.0)
                    };
                    panel3.Tag = insumo;
                    Label label = new Label
                    {
                        Foreground = Brushes.SteelBlue,
                        FontStyle = FontStyles.Italic,
                        Content = "   " + insumo.nombre,
                        HorizontalContentAlignment = HorizontalAlignment.Right,
                        Width = 270.0
                    };
                    controlDecimal num = new controlDecimal
                    {
                        valor = insumo.cantMaxima
                    };
                    panel3.Children.Add(label);
                    panel3.Children.Add(num);
                    element.Children.Add(panel3);
                }
                panel.Children.Add(element);
            }
            this.scrollInspecciones.Content = panel;
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<StackPanel> list = ((StackPanel)this.scrollContenedor.Content).Children.Cast<StackPanel>().ToList<StackPanel>();
                ProductosActividad tag = (ProductosActividad)((Button)sender).Tag;
                int num = 0;
                bool flag = false;
                foreach (StackPanel panel in list)
                {
                    ProductosActividad actividad2 = (ProductosActividad)panel.Tag;
                    if (tag.idProducto == actividad2.idProducto)
                    {
                        flag = true;
                        break;
                    }
                }
                List<StackPanel> list2 = new List<StackPanel>();
                if (flag)
                {
                    ((StackPanel)this.scrollContenedor.Content).Children.Remove(list[num]);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void btnCambiarPersonal_Click(object sender, RoutedEventArgs e)
        {
            this.txtPersonal.limpiar();
        }

        private void btnCambiarPersonal1_Click(object sender, RoutedEventArgs e)
        {
            this.txtPersonal2.limpiar();
        }

        private void btnCambiarPersonal2_Click(object sender, RoutedEventArgs e)
        {
            this.txtPersonal3.limpiar();
        }

        private void btnCambiarPersonalEnt_Click(object sender, RoutedEventArgs e)
        {
            this.txtPersonalEntrega.limpiar();
        }

        private void btnVerPanel_Click(object sender, RoutedEventArgs e)
        {
            new viewPanelOSTransito(4).abrirPanel();
        }

        private void buscarMasterOrdenSer(int i)
        {
            try
            {
                this.nuevo();
                OperationResult resp = new MasterOrdServSvc().getMasterOrdServ(i);
                if (resp.typeResult == ResultTypes.success)
                {
                    this.txtOrdenPadre.Text = i.ToString();
                    this.txtOrdenPadre.IsEnabled = false;
                    MasterOrdServ serv = (MasterOrdServ)resp.result;
                    this.txtPersonalEntrega.personal = serv.chofer;
                    this.maplvlOrdenes(serv.listTiposOrdenServicio);
                    switch (this.lvlOrdenes.Items.Count)
                    {
                        case 0:
                            {
                                OperationResult result1 = new OperationResult
                                {
                                    valor = 2,
                                    mensaje = "NO EXISTEN TRABAJOS PARA EL LAVADERO PARA ESTE FOLIO"
                                };
                                ImprimirMensaje.imprimir(result1);
                                this.nuevo();
                                return;
                            }
                        case 1:
                            this.lvlOrdenes.SelectedIndex = 0;
                            return;
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void chcBoxTodos_Checked(object sender, RoutedEventArgs e)
        {
            this.marcarDesmarcarInspecciones(true);
        }

        private void chcBoxTodos_Unchecked(object sender, RoutedEventArgs e)
        {
            this.marcarDesmarcarInspecciones(false);
        }

        private void chcBoxTodosProgramados_Checked(object sender, RoutedEventArgs e)
        {
            this.marcarDesmarcarInspeccionesProgramadas(true);
        }

        private void chcBoxTodosProgramados_Unchecked(object sender, RoutedEventArgs e)
        {
            this.marcarDesmarcarInspeccionesProgramadas(false);
        }

        private void eventoActividades(Actividad actividad)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                StackPanel panel = new StackPanel
                {
                    Orientation = Orientation.Vertical
                };
                bool flag = true;
                this.txtPersonal.personal = actividad.personal1;
                this.txtPersonal2.personal = actividad.personal2;
                this.txtPersonal3.personal = actividad.personal3;
                List<ProductosActividad> listProductos = actividad.listProductos;
                if (actividad.estatus.ToUpper() == "PRE")
                {
                    this.txtTipoActividad.Content = "ASIGNAR";
                    flag = false;
                    this.tbiPersonal.IsEnabled = true;
                }
                else if ((actividad.estatus.ToUpper() == "ASIG") | (actividad.estatus.ToUpper() == "DIAG"))
                {
                    this.txtTipoActividad.Content = "FINALIZAR";
                    flag = true;
                    this.tbiPersonal.IsEnabled = true;
                }
                else if (actividad.estatus.ToUpper() == "TER")
                {
                    this.txtTipoActividad.Content = "TERMINADO";
                    flag = false;
                    this.tbiPersonal.IsEnabled = false;
                }
                this.tbiInsumos.IsEnabled = flag;
                foreach (ProductosActividad actividad2 in listProductos)
                {
                    StackPanel element = new StackPanel
                    {
                        VerticalAlignment = VerticalAlignment.Top,
                        HorizontalAlignment = HorizontalAlignment.Left
                    };
                    element.Orientation = Orientation.Horizontal;
                    Label label1 = new Label
                    {
                        Content = actividad2.nombre,
                        Width = 270.0,
                        Height = 24.0,
                        FontWeight = FontWeights.Medium
                    };
                    element.Children.Add(label1);
                    TextBox box = new TextBox
                    {
                        Width = 75.0,
                        Height = 22.0,
                        Text = actividad2.cantidad.ToString(),
                        IsEnabled = false,
                        FontWeight = FontWeights.Medium
                    };
                    box.KeyDown += new KeyEventHandler(this.Txt_KeyDown);
                    box.TextChanged += new TextChangedEventHandler(this.Txt_TextChanged);
                    element.Children.Add(box);
                    Button button = new Button
                    {
                        Tag = actividad2,
                        Content = "QUITAR",
                        Width = 50.0,
                        Height = 24.0,
                        FontWeight = FontWeights.Medium,
                        Foreground = Brushes.White,
                        Background = Brushes.Red
                    };
                    element.Tag = actividad2;
                    panel.Children.Add(element);
                    this.scrollContenedor.Content = panel;
                }
                if ((listProductos.Count == 0) && (actividad.estatus.ToUpper() == "DIAG"))
                {
                    OperationResult resp = new ConsultasTallerSvc().getProductosCOM(AreaTrabajo.LAVADERO, "%", false);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        List<ProductoInsumo> list2 = FuncionesComunes.convertirProductos((List<ProductosCOM>)resp.result);
                        foreach (ProductoInsumo insumo in list2)
                        {
                            this.agregarInsumo(insumo, actividad);
                        }
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void eventoOrdenes(TipoOrdenServicio orden)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                this.lvlActividades.Items.Clear();
                this.scrollContenedor.Content = null;
                this.scrollInspecciones.IsEnabled = true;
                this.txtPersonal.limpiar();
                this.txtPersonal2.limpiar();
                this.txtTipoActividad.Content = "";
                this.scrollInspecciones.Content = null;
                if ((orden.estatus.ToUpper() == "DIAG") || (orden.estatus.ToUpper() == "TER"))
                {
                    OperationResult result2 = new InspeccionesSvc().getInspeccionesByIdTipoUniTrans(orden.unidadTrans.tipoUnidad.clave, orden.idTipoOrdenServicio, orden.idOrdenSer);
                    if (result2.typeResult == ResultTypes.success)
                    {
                        this.añadirInspecciones((List<Inspecciones>)result2.result, orden.estatus != "TER");
                    }
                    else
                    {
                        return;
                    }
                }
                OperationResult resp = new MasterOrdServSvc().getActividadesByOrdenSer(orden.idOrdenSer);
                if (orden.personaEntrega != null)
                {
                    this.txtPersonalEntrega.personal = orden.personaEntrega;
                }
                if (resp.typeResult == ResultTypes.success)
                {
                    this.mapActividades((List<Actividad>)resp.result);
                    this.lvlActividades.SelectedIndex = 0;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private decimal getCostoInsumos(List<ProductosActividad> listaInsumos)
        {
            try
            {
                decimal num = 0M;
                foreach (ProductosActividad actividad in listaInsumos)
                {
                    num += actividad.cantidad * actividad.costo;
                }
                return num;
            }
            catch (Exception)
            {
                return decimal.Zero;
            }
        }

        private List<Inspecciones> getListaInspecciones()
        {
            if (this.scrollInspecciones.Content == null)
            {
                return new List<Inspecciones>();
            }
            List<Inspecciones> list = new List<Inspecciones>();
            StackPanel content = (StackPanel)this.scrollInspecciones.Content;
            List<StackPanel> list2 = content.Children.Cast<StackPanel>().ToList<StackPanel>();
            foreach (StackPanel panel2 in list2)
            {
                List<object> list4 = panel2.Children.Cast<object>().ToList<object>();
                Inspecciones item = null;
                foreach (object obj2 in list4)
                {
                    if ((obj2 is CheckBox) && ((CheckBox)obj2).IsChecked.Value)
                    {
                        item = new Inspecciones();
                        Inspecciones tag = (Inspecciones)panel2.Tag;
                        item.idInspeccion = tag.idInspeccion;
                        item.nombreInspeccion = tag.nombreInspeccion;
                        item.tipoOrdenServicio = tag.tipoOrdenServicio;
                        item.listaInsumos = new List<ProductoInsumo>();
                    }
                    if ((obj2 is StackPanel) && (item != null))
                    {
                        List<Control> list5 = ((StackPanel)obj2).Children.Cast<Control>().ToList<Control>();
                        foreach (Control control in list5)
                        {
                            if (control is controlDecimal)
                            {
                                ProductoInsumo tag = (ProductoInsumo)((StackPanel)obj2).Tag;
                                tag.cantMaxima = ((controlDecimal)control).valor;
                                item.listaInsumos.Add(tag);
                            }
                        }
                    }
                }
                if (item != null)
                {
                    list.Add(item);
                }
            }
            return list;
        }

        private List<ProductosActividad> getListaInsumos()
        {
            try
            {
                if (this.scrollContenedor.Content == null)
                {
                    return null;
                }
                List<ProductosActividad> list = new List<ProductosActividad>();
                List<StackPanel> list2 = ((StackPanel)this.scrollContenedor.Content).Children.Cast<StackPanel>().ToList<StackPanel>();
                foreach (StackPanel panel in list2)
                {
                    ProductosActividad tag = (ProductosActividad)panel.Tag;
                    List<Control> list4 = panel.Children.Cast<Control>().ToList<Control>();
                    foreach (Control control in list4)
                    {
                        if (control is TextBox)
                        {
                            tag.cantidad = Convert.ToDecimal(((TextBox)control).Text);
                            if (tag.cantidad <= decimal.Zero)
                            {
                            }
                        }
                    }
                    if (tag.cantidad > decimal.Zero)
                    {
                        OperationResult result = new ConsultasTallerSvc().consultarExistenByProducto(tag.idProducto);
                        if (result.typeResult == ResultTypes.success)
                        {
                            tag.existencia = (decimal)result.result;
                        }
                        else
                        {
                            tag.existencia = decimal.Zero;
                        }
                        list.Add(tag);
                    }
                }
                return list;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override OperationResult guardar()
        {
            OperationResult result2;
            bool flag = false;
            int i = 0;
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult result = this.validar();
                if (result.typeResult > ResultTypes.success)
                {
                    return result;
                }
                Lavadero lavadero = this.mapForm();
                if (lavadero != null)
                {
                    OperationResult result3 = new LavaderoSvc().saveLavadero(ref lavadero);
                    if (result3.typeResult == ResultTypes.success)
                    {
                        if (lavadero.estatus == "DIAG")
                        {
                            List<ProductoInsumo> listaProductos = new List<ProductoInsumo>();
                            OperationResult result4 = new ConsultasTallerSvc().getProductosCOM(AreaTrabajo.LAVADERO, "%", false);
                            if (result4.typeResult == ResultTypes.success)
                            {
                                listaProductos = FuncionesComunes.convertirProductos(result4.result as List<ProductosCOM>);
                            }
                            TipoOrdenServicio content = (TipoOrdenServicio)((ListViewItem)this.lvlOrdenes.SelectedItem).Content;
                            OperationResult result5 = new InspeccionesSvc().getInspeccionesByIdTipoUniTrans(content.unidadTrans.tipoUnidad.clave, content.idTipoOrdenServicio, content.idOrdenSer);
                            List<Inspecciones> listaInspecciones = new List<Inspecciones>();
                            if (result5.typeResult == ResultTypes.success)
                            {
                                listaInspecciones = (List<Inspecciones>)result5.result;
                            }
                            bool flag6 = new ReportView().crearSolicitudSalidaInsumos(listaProductos, (TipoOrdenServicio)((ListViewItem)this.lvlOrdenes.SelectedItem).Content, lavadero.fechaEntrada, lavadero.lavador, listaInspecciones, base.mainWindow.inicio._usuario.personal.nombre, false);
                            BitmapImage image = new BitmapImage();
                            List<UbicacionUnidades> list1 = new List<UbicacionUnidades>();
                            UbicacionUnidades item = new UbicacionUnidades
                            {
                                unidadTrans = content.unidadTrans,
                                descripcion = "ENTRADA A LAVADERO",
                                folio = content.idOrdenSer,
                                tipoMovimiento = eTipoMovimiento.ENTRADA,
                                ubicacionActual = EstatusUbicacion.LAVADERO,
                                ubicacionNew = EstatusUbicacion.LAVADERO
                            };
                            list1.Add(item);
                            List<UbicacionUnidades> listaUbicaciones = list1;
                            OperationResult result6 = new UbicacionUnidadesSvc().saveUbicacionUnidades(listaUbicaciones);
                        }
                        i = Convert.ToInt32(this.txtOrdenPadre.Text.Trim());
                        flag = true;
                    }
                    return result3;
                }
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = "REVISAR LA INFORMACI\x00d3N PROPORCIONADA"
                };
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            finally
            {
                if (flag)
                {
                    int idOrdenSer = ((TipoOrdenServicio)((ListViewItem)this.lvlOrdenes.SelectedItem).Content).idOrdenSer;
                    this.nuevo();
                    this.buscarMasterOrdenSer(i);
                    int num3 = 0;
                    foreach (ListViewItem item in (IEnumerable)this.lvlOrdenes.Items)
                    {
                        if (idOrdenSer == ((TipoOrdenServicio)item.Content).idOrdenSer)
                        {
                            this.lvlOrdenes.SelectedIndex = num3;
                            break;
                        }
                        num3++;
                    }
                    TipoOrdenServicio content = (TipoOrdenServicio)((ListViewItem)this.lvlOrdenes.SelectedItem).Content;
                    List<Actividad> list4 = (List<Actividad>)new MasterOrdServSvc().getActividadesByOrdenSer(content.idOrdenSer).result;
                    base.mainWindow.habilitar = true;
                }
                base.Cursor = Cursors.Arrow;
            }
            return result2;
        }
        

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
            {
                this.eventoOrdenes((TipoOrdenServicio)((ListViewItem)sender).Content);
            }
        }

        private void Lvl_MouseDoubleClick1(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    this.eventoActividades((Actividad)((ListViewItem)sender).Content);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                this.eventoOrdenes((TipoOrdenServicio)((ListViewItem)sender).Content);
            }
        }

        private void Lvl_Selected1(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                this.eventoActividades((Actividad)((ListViewItem)sender).Content);
            }
        }

        private void mapActividades(List<Actividad> result)
        {
            this.lvlActividades.Items.Clear();
            foreach (Actividad actividad in result)
            {
                ListViewItem newItem = new ListViewItem
                {
                    Content = actividad
                };
                newItem.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick1);
                newItem.Selected += new RoutedEventHandler(this.Lvl_Selected1);
                this.lvlActividades.Items.Add(newItem);
            }
        }

        private Lavadero mapForm()
        {
            try
            {
                Actividad content = (Actividad)((ListBoxItem)this.lvlActividades.SelectedItem).Content;
                Lavadero lavadero = new Lavadero
                {
                    idLavadero = 0,
                    actividad = content,
                    fechaEntrada = DateTime.Now,
                    fechaSalida = (((TipoOrdenServicio)((ListViewItem)this.lvlOrdenes.SelectedItem).Content).estatus.ToUpper() == "PRE") ? this.f : new DateTime?(DateTime.Now),
                    estatus = (content.estatus.ToUpper() == "PRE") ? "DIAG" : "TER",
                    lavador = this.txtPersonal.personal,
                    lavadorAux = this.txtPersonal2.personal,
                    lavadorAux2 = this.txtPersonal3.personal,
                    ordenServicio = ((TipoOrdenServicio)((ListViewItem)this.lvlOrdenes.SelectedItem).Content).idOrdenSer,
                    personalEntrega = this.txtPersonalEntrega.personal,
                    usuario = base.mainWindow.inicio._usuario.nombreUsuario
                };
                lavadero.listaInsumos = this.getListaInsumos();
                if (lavadero.estatus == "DIAG")
                {
                    if (lavadero.listaInsumos == null)
                    {
                        lavadero.listaInsumos = new List<ProductosActividad>();
                    }
                }
                else if (lavadero.listaInsumos == null)
                {
                    return null;
                }
                lavadero.costo = lavadero.actividad.costo;
                lavadero.costoInsumos = this.getCostoInsumos(lavadero.listaInsumos);
                lavadero.listaInspecciones = this.getListaInspecciones();
                return lavadero;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void maplvlOrdenes(List<TipoOrdenServicio> list)
        {
            this.lvlOrdenes.Items.Clear();
            foreach (TipoOrdenServicio servicio in list)
            {
                if ((servicio.TipoServicio.ToUpper() == "LAVADERO") && (((servicio.estatus != "PRE") && (servicio.estatus != "REC")) || ReglasUbicacion.validar(EstatusUbicacion.LAVADERO, servicio.unidadTrans)))
                {
                    ListViewItem newItem = new ListViewItem
                    {
                        Content = servicio
                    };
                    newItem.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                    newItem.Selected += new RoutedEventHandler(this.Lvl_Selected);
                    this.lvlOrdenes.Items.Add(newItem);
                }
            }
        }

        private void marcarDesmarcarInspecciones(bool checado)
        {
            if (this.scrollInspecciones.Content != null)
            {
                StackPanel content = (StackPanel)this.scrollInspecciones.Content;
                List<StackPanel> list = content.Children.Cast<StackPanel>().ToList<StackPanel>();
                foreach (StackPanel panel2 in list)
                {
                    List<object> list2 = panel2.Children.Cast<object>().ToList<object>();
                    foreach (object obj2 in list2)
                    {
                        if (obj2 is CheckBox)
                        {
                            ((CheckBox)obj2).IsChecked = new bool?(checado);
                        }
                    }
                }
            }
        }

        private void marcarDesmarcarInspeccionesProgramadas(bool checado)
        {
        }

        public override void nuevo()
        {
            this.txtOrdenPadre.IsEnabled = true;
            this.txtTipoActividad.Content = "";
            this.txtPersonal.limpiar();
            this.txtPersonal2.limpiar();
            this.txtPersonal3.limpiar();
            this.txtPersonalEntrega.limpiar();
            this.txtOrdenPadre.Clear();
            this.lvlOrdenes.Items.Clear();
            this.scrollContenedor.Content = null;
            this.scrollInspecciones.Content = null;
            this.lvlActividades.Items.Clear();
            this.spControles.IsEnabled = true;
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
            this.txtOrdenPadre.Focus();
        }
        
        private void Txt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key < Key.D0 || e.Key > Key.D9)
            {
                if (e.Key < Key.NumPad0 || e.Key > Key.NumPad9)
                {
                    if (e.Key == Key.OemPeriod || e.Key == Key.Decimal)
                    {
                        if (((TextBox)sender).Text.Contains("."))
                        {
                            e.Handled = true;
                        }
                    }
                    else if (e.Key == Key.Tab)
                    {
                        //e.Handled = false;
                    }
                    else
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void Txt_TextChanged(object sender, TextChangedEventArgs e)
        {
            string str = ((TextBox)sender).Text.Trim();
            if (string.IsNullOrEmpty(str))
            {
                ((TextBox)sender).Text = "0";
                ((TextBox)sender).CaretIndex = 1;
            }
            else
            {
                decimal result = 0M;
                if (!decimal.TryParse(str, out result))
                {
                    if (((TextBox)sender).Text == ".")
                    {
                        ((TextBox)sender).Text = "0.";
                    }
                    else
                    {
                        ((TextBox)sender).Text = "0";
                        ((TextBox)sender).CaretIndex = 1;
                    }
                }
                else if (((TextBox)sender).Text == "0.")
                {
                    ((TextBox)sender).CaretIndex = ((TextBox)sender).Text.Length;
                }
                else if (!((TextBox)sender).Text.Contains("."))
                {
                    ((TextBox)sender).Text = result.ToString();
                    ((TextBox)sender).CaretIndex = ((TextBox)sender).Text.Length;
                }
            }
        }

        private void txtInsumo_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                try
                {
                    if (e.Key == Key.Enter)
                    {
                        if (this.lvlActividades.SelectedItem == null)
                        {
                            OperationResult resp = new OperationResult
                            {
                                valor = 2,
                                mensaje = "Para ingresa un insumo primero selecciona una actividad"
                            };
                            ImprimirMensaje.imprimir(resp);
                        }
                        else
                        {
                            ProductoInsumo insumoVal = new ProductoInsumo();
                            if (string.IsNullOrEmpty(this.txtInsumo.Text.Trim()))
                            {
                                insumoVal = new selectInsumo(this.txtInsumo.Text.Trim()).buscarTodos(false, true, false);
                            }
                            else
                            {
                                OperationResult resp = new OperacionSvc().getInsumosByNombre(this.txtInsumo.Text.Trim(), false, true, false);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    List<ProductoInsumo> result = (List<ProductoInsumo>)resp.result;
                                    if (result.Count == 1)
                                    {
                                        insumoVal = result[0];
                                    }
                                    else
                                    {
                                        insumoVal = new selectInsumo(this.txtInsumo.Text.Trim()).buscarTodos(false, true, false);
                                    }
                                }
                                else
                                {
                                    ImprimirMensaje.imprimir(resp);
                                }
                            }
                            if (insumoVal != null)
                            {
                                this.agregarInsumo(insumoVal, null);
                            }
                            this.txtInsumo.Clear();
                        }
                    }
                }
                catch (Exception exception)
                {
                    ImprimirMensaje.imprimir(exception);
                }
            }
            finally
            {
            }
        }

        private void txtOrdenPadre_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && !string.IsNullOrEmpty(this.txtOrdenPadre.Text.Trim()))
            {
                int result = 0;
                if (int.TryParse(this.txtOrdenPadre.Text.Trim(), out result))
                {
                    this.buscarMasterOrdenSer(result);
                }
                else
                {
                    MessageBox.Show("El folio de la orden de servicio no es valido", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }

        private void txtOrdenPadre_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = new Regex("[^0-9]+").IsMatch(e.Text);
        }

        private OperationResult validar()
        {
            try
            {
                OperationResult result = new OperationResult
                {
                    valor = 0,
                    mensaje = string.Empty
                };
                if (this.lvlOrdenes.SelectedItem == null)
                {
                    result.valor = 2;
                    result.mensaje = result.mensaje + " *Se requiere seleccionar una orden de servicio";
                }
                if (this.lvlActividades.SelectedItem == null)
                {
                    result.valor = 2;
                    result.mensaje = result.mensaje + " *Se requiere seleccionar una actividad" + Environment.NewLine;
                }
                if (this.txtPersonalEntrega.personal == null)
                {
                    result.valor = 2;
                    result.mensaje = result.mensaje + " *No se especifico al personal que entrega la orden de servicio" + Environment.NewLine;
                }
                if (this.txtPersonal.personal == null)
                {
                    result.valor = 2;
                    result.mensaje = result.mensaje + " *No se especifico al personal para esta actividad" + Environment.NewLine;
                }
                if (((TipoOrdenServicio)((ListViewItem)this.lvlOrdenes.SelectedItem).Content).estatus != "PRE")
                {
                    List<ProductosActividad> list = this.getListaInsumos();
                    if ((list == null) || (list.Count == 0))
                    {
                        result.valor = 2;
                        result.mensaje = result.mensaje + "NO SE AGREGARON INSUMOS O ALGUNO TIENE VALOR EN 0" + Environment.NewLine;
                        this.tbcControles.SelectedIndex = 1;
                    }
                }
                if (((TipoOrdenServicio)((ListViewItem)this.lvlOrdenes.SelectedItem).Content).estatus != "PRE")
                {
                    string str = this.ValidarInspecciones();
                    if (!string.IsNullOrEmpty(str))
                    {
                        result.valor = 2;
                        result.mensaje = result.mensaje + str;
                    }
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message.ToUpper()
                };
            }
        }

        private string ValidarInspecciones()
        {
            try
            {
                string str = string.Empty;
                if (this.scrollInspecciones.Content != null)
                {
                    StackPanel content = (StackPanel)this.scrollInspecciones.Content;
                    List<StackPanel> list = content.Children.Cast<StackPanel>().ToList<StackPanel>();
                    foreach (StackPanel panel2 in list)
                    {
                        List<object> list2 = panel2.Children.Cast<object>().ToList<object>();
                        foreach (object obj2 in list2)
                        {
                            if (obj2 is CheckBox)
                            {
                                CheckBox box = (CheckBox)obj2;
                                Inspecciones tag = (Inspecciones)box.Tag;
                                if (tag.dias == decimal.Zero)
                                {
                                    if (!box.IsChecked.Value)
                                    {
                                        str = str + " *NO SE MARCO LA INSPECCI\x00d3N " + tag.nombreInspeccion + Environment.NewLine;
                                    }
                                }
                                else if (tag.opcional)
                                {
                                    if ((tag.diasPasados >= tag.dias) && !box.IsChecked.Value)
                                    {
                                        string[] textArray1 = new string[] { str, " *NO SE MARCO LA INSPECCI\x00d3N ", tag.nombreInspeccion, " (YA HAN PASADO ", tag.diasPasados.ToString(), " D\x00cdAS DESDE SU ULTIMA INSPECCI\x00d3N)", Environment.NewLine };
                                        str = string.Concat(textArray1);
                                    }
                                }
                                else if ((tag.diasPasados >= tag.dias) && !box.IsChecked.Value)
                                {
                                    string[] textArray2 = new string[] { str, " *NO SE MARCO LA INSPECCI\x00d3N ", tag.nombreInspeccion, " (YA HAN PASADO ", tag.diasPasados.ToString(), " D\x00cdAS DESDE SU ULTIMA INSPECCI\x00d3N)", Environment.NewLine };
                                    str = string.Concat(textArray2);
                                }
                            }
                        }
                    }
                }
                else
                {
                    str = str + "NO HAY INSPECCIONES A VERIFICAR" + Environment.NewLine;
                }
                return str;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return string.Empty;
            }
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.nuevo();
            this.txtPersonalEntrega.loaded(eTipoBusquedaPersonal.TODOS, null);
            this.txtPersonal.loaded(eTipoBusquedaPersonal.LAVADORES, null);
            this.txtPersonal2.loaded(eTipoBusquedaPersonal.LAVADORES, null);
            this.txtPersonal3.loaded(eTipoBusquedaPersonal.LAVADORES, null);
        }
    }
}
