﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using CoreFletera.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Controls.Primitives;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Markup;
    using System.Windows.Media;

    public partial class editOrigenesDestinosAtlante : WpfClient.ViewBase
    {
        private List<Origen> _listOrigen = new List<Origen>();
        private List<Zona> _listDestinos = new List<Zona>();
        private GridViewColumnHeader listViewSortCol = null;
        private ClasificacionProductos clasificacion = null;


        public editOrigenesDestinosAtlante()
        {
            this.InitializeComponent();
        }
        public void actualizar(Cliente cliente)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                this.txtFilter.Clear();
                this.txtOrigen.Clear();
                this.lvUsers.ItemsSource = null;
                this.mapComboDestinos();
                OperationResult resp = new OrigenSvc().getOrigenesAll(cliente.clave, 0);
                if (resp.typeResult == ResultTypes.success)
                {
                    this._listOrigen = (List<Origen>)resp.result;
                    this.mapComboOrigenes();
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                }
                this.llenarLista(cliente);
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void btnGuardarDestino_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Zona destino = this.mapDestino();
                if (destino != null)
                {
                    OperationResult resp = new ZonasSvc().saveRutasDestinoAtlante(ref destino);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        this._listDestinos.Add(destino);
                        this.mapComboDes(this._listDestinos);
                        this.mapDestino(destino);
                    }
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void btnGuardarOrigen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!this.errorOrigen())
                {
                    Origen origen = this.mapOrigen();
                    if (origen != null)
                    {
                        OperationResult resp = new OrigenSvc().saveOrigenCliente(ref origen);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            this.mapOrigen(origen);
                            this._listOrigen.Add(origen);
                            this.mapComboOrigenes();
                        }
                        ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void btnGuardarRelacion_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                OrigenDestinos relacion = this.mapRelacion();
                if (relacion != null)
                {
                    OperationResult resp = new ZonasSvc().saveOrigenDestino(ref relacion);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        this.mapRelacion(relacion);
                    }
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void btnNuevoDestino_Click(object sender, RoutedEventArgs e)
        {
            this.mapDestino(null);
        }

        private void btnNuevoOrigen_Click(object sender, RoutedEventArgs e)
        {
            this.mapOrigen(null);
        }

        private void btnNuevoRelacion_Click(object sender, RoutedEventArgs e)
        {
            this.mapRelacion(null);
        }

        private void btnQuitarRelacion_Click(object sender, RoutedEventArgs e)
        {
            OrigenDestinos relacion = this.mapRelacion();
            if (new ZonasSvc().borrarOrigenDestino(relacion).typeResult == ResultTypes.success)
            {
                this.actualizar((Cliente)this.cbxClientes.SelectedItem);
                this.mapRelacion(null);
                this.btnQuitarRelacion.IsEnabled = false;
            }
        }

        private void cbxClientes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.mapOrigen(null);
            this.mapDestino(null);
            this.mapRelacion(null);
            this.lvUsers.ItemsSource = null;
            this.cbxDestino.Items.Clear();
            this.cbxOrigen.Items.Clear();
            if (this.cbxClientes.SelectedItem != null)
            {
                this.actualizar((Cliente)this.cbxClientes.SelectedItem);
            }
        }

        private void cbxOrigen_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
        }

        private void cbxOrigen_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private bool errorOrigen()
        {
            if (this.cbxClientes.SelectedItem == null)
            {
                this.cbxClasificacion.Focus();
                OperationResult resp = new OperationResult
                {
                    valor = 2,
                    mensaje = "Se debe elegir un cliente"
                };
                ImprimirMensaje.imprimir(resp);
                return true;
            }
            if (string.IsNullOrEmpty(this.txtDescripcionOrigen.Text.Trim()))
            {
                this.txtDescripcionOrigen.Focus();
                OperationResult resp = new OperationResult
                {
                    valor = 2,
                    mensaje = "Se Escribir la descripcion del destino"
                };
                ImprimirMensaje.imprimir(resp);
                return true;
            }
            if (this.cbxEstados.SelectedItem == null)
            {
                this.cbxEstados.Focus();
                OperationResult resp = new OperationResult
                {
                    valor = 2,
                    mensaje = "Se debe elegir un estado"
                };
                ImprimirMensaje.imprimir(resp);
                return true;
            }
            if (this.cbxClasificacion.SelectedItem == null)
            {
                this.cbxClasificacion.Focus();
                OperationResult resp = new OperationResult
                {
                    valor = 2,
                    mensaje = "Se debe elegir una clasificaci\x00f3n"
                };
                ImprimirMensaje.imprimir(resp);
                return true;
            }
            return false;
        }

        private void itemDestino_Selected(object sender, RoutedEventArgs e)
        {
            this.mapComboOrigenes();
        }
        private void llenarLista(Cliente cliente)
        {
            this.lvlDestinos.Items.Clear();
            OperationResult result = new ZonasSvc().getRelacionesOrigenDestinosByIdCliente(cliente.clave);
            if (result.typeResult == ResultTypes.success)
            {
                foreach (OrigenDestinoClientes clientes in (List<OrigenDestinoClientes>)result.result)
                {
                    this.lvlDestinos.Items.Add(clientes);
                }
                this.lvUsers.ItemsSource = (List<OrigenDestinoClientes>)result.result;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvUsers.ItemsSource);
                PropertyGroupDescription item = new PropertyGroupDescription("origen");
                defaultView.GroupDescriptions.Add(item);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
        }

        private void lvUsers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.lvUsers.SelectedItem != null)
            {
                OrigenDestinoClientes selectedItem = (OrigenDestinoClientes)this.lvUsers.SelectedItem;
                this.tapControl.SelectedIndex = 2;
                int num = 0;
                foreach (Origen origen in (IEnumerable)this.cbxOrigen.Items)
                {
                    if (origen.idOrigen == selectedItem.idOrigen)
                    {
                        this.cbxOrigen.SelectedIndex = num;
                        this.cbxOrigen.IsEnabled = false;
                        break;
                    }
                    num++;
                }
                num = 0;
                foreach (Zona zona in (IEnumerable)this.cbxDestino.Items)
                {
                    if (selectedItem.idDestino == zona.clave)
                    {
                        this.cbxDestino.SelectedIndex = num;
                        this.cbxDestino.IsEnabled = false;
                        break;
                    }
                    num++;
                }
                this.txtClaveRelacion.Tag = selectedItem;
                this.btnNuevoRelacion.IsEnabled = true;
                this.clasificacion = selectedItem.clasificacion;
                this.txtClaveRelacion.Tag = selectedItem;
                this.txtClaveRelacion.Text = selectedItem.idOrigenDestinoClientes.ToString();
                this.txtPreSencillo.Text = selectedItem.precioSencillo.ToString();
                this.txtPreFull.Text = selectedItem.precioFull.ToString();
                this.txtKm.Text = selectedItem.km.ToString();
                this.chcViaje.IsChecked = new bool?(selectedItem.viaje);
                this.txtViajeSencillo.Text = selectedItem.viajeSencillo.ToString();
                this.txtViajeFull.Text = selectedItem.viajeFull.ToString();
                this.controlesOrigen.IsEnabled = true;
                this.controlRelacion.IsEnabled = true;
                this.btnGuardarRelacion.IsEnabled = true;
                this.btnQuitarRelacion.IsEnabled = true;
                int num2 = 0;
                foreach (ClasificacionPagoChofer chofer in (IEnumerable)this.cbxTipoPagoOperador.Items)
                {
                    if (chofer == selectedItem.tipoPago)
                    {
                        break;
                    }
                    num2++;
                }
                this.cbxTipoPagoOperador.SelectedIndex = num2;
            }
        }

        private void mapClasificaciones()
        {
            try
            {
                OperationResult resp = new ClasificacionSvc().getAllClasificaciones();
                if (resp.typeResult == ResultTypes.success)
                {
                    foreach (Clasificacion clasificacion in (List<Clasificacion>)resp.result)
                    {
                        this.cbxClasificacion.Items.Add(clasificacion);
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void mapComboClientes()
        {
            try
            {
                OperationResult resp = new ClienteSvc().getClientesALLorById(base.mainWindow.empresa.clave, 0, true);
                if (resp.typeResult == ResultTypes.success)
                {
                    foreach (Cliente cliente in (List<Cliente>)resp.result)
                    {
                        this.cbxClientes.Items.Add(cliente);
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void mapComboDes(List<Zona> list)
        {
            this.cbxDestino.Items.Clear();
            foreach (Zona zona in list)
            {
                this.cbxDestino.Items.Add(zona);
            }
        }

        private void mapComboDestinos()
        {
            try
            {
                this.cbxDestino.Items.Clear();
                Cliente selectedItem = (Cliente)this.cbxClientes.SelectedItem;
                OperationResult result = new ZonasSvc().getDestinosAtlante(selectedItem.empresa.clave);
                if (result.typeResult == ResultTypes.success)
                {
                    this._listDestinos = (List<Zona>)result.result;
                    this.mapComboDes(this._listDestinos);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void mapComboOrigenes()
        {
            this.cbxOrigen.Items.Clear();
            foreach (Origen origen in this._listOrigen)
            {
                this.cbxOrigen.Items.Add(origen);
            }
        }

        private Zona mapDestino()
        {
            try
            {
                return new Zona
                {
                    clave_empresa = ((Cliente)this.cbxClientes.SelectedItem).empresa.clave,
                    descripcion = this.txtDescripcionDestino.Text.Trim(),
                    estado = (Estado)this.cbxEstadosDestino.SelectedItem,
                    correo = this.txtCorreo.Text.Trim()
                };
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return null;
            }
        }

        private void mapDestino(Zona destino)
        {
            if (destino != null)
            {
                this.txtClaveDestino.Text = destino.clave.ToString();
                this.txtDescripcionDestino.Text = destino.descripcion;
                this.btnNuevoDestino.IsEnabled = true;
                this.btnGuardarDestino.IsEnabled = false;
                this.controlesDestino.IsEnabled = false;
            }
            else
            {
                this.txtClaveDestino.Clear();
                this.txtDescripcionDestino.Clear();
                this.cbxEstadosDestino.SelectedItem = null;
                this.controlesDestino.IsEnabled = true;
                this.btnNuevoDestino.IsEnabled = false;
                this.btnGuardarDestino.IsEnabled = true;
            }
        }

        private void mapEstados()
        {
            try
            {
                OperationResult resp = new EstadoSvc().getEstadosAllorById(0);
                if (resp.typeResult == ResultTypes.success)
                {
                    foreach (Estado estado in (List<Estado>)resp.result)
                    {
                        this.cbxEstados.Items.Add(estado);
                        this.cbxEstadosDestino.Items.Add(estado);
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private Origen mapOrigen() =>
            new Origen
            {
                idCliente = ((Cliente)this.cbxClientes.SelectedItem).clave,
                idOrigen = 0,
                clasificacion = ((Clasificacion)this.cbxClasificacion.SelectedItem).clasificacion,
                estado = ((Estado)this.cbxEstados.SelectedItem).nombre,
                nombreOrigen = this.txtDescripcionOrigen.Text.Trim()
            };

        private void mapOrigen(Origen origen)
        {
            if (origen != null)
            {
                this.txtClaveOrigen.Text = origen.idOrigen.ToString();
                this.txtDescripcionOrigen.Text = origen.nombreOrigen;
                this.btnNuevoOrigen.IsEnabled = true;
                this.btnGuardarOrigen.IsEnabled = false;
                this.controlesOrigen.IsEnabled = false;
            }
            else
            {
                this.txtClaveOrigen.Clear();
                this.txtDescripcionOrigen.Clear();
                this.cbxClasificacion.SelectedItem = null;
                this.cbxEstados.SelectedItem = null;
                this.btnNuevoOrigen.IsEnabled = false;
                this.btnGuardarOrigen.IsEnabled = true;
                this.controlesOrigen.IsEnabled = true;
            }
        }

        private OrigenDestinos mapRelacion()
        {
            try
            {
                return new OrigenDestinos
                {
                    idOrigenDestino = (this.txtClaveRelacion.Tag == null) ? 0 : (this.txtClaveRelacion.Tag as OrigenDestinoClientes).idOrigenDestinoClientes,
                    origen = (Origen)this.cbxOrigen.SelectedItem,
                    destino = (Zona)this.cbxDestino.SelectedItem,
                    clasificacion = this.clasificacion,
                    precioSencillo = Convert.ToDecimal(this.txtPreSencillo.Text.Trim()),
                    precioFull = Convert.ToDecimal(this.txtPreFull.Text.Trim()),
                    km = Convert.ToDecimal(this.txtKm.Text.Trim()),
                    viaje = this.chcViaje.IsChecked.Value,
                    idCliente = ((Cliente)this.cbxClientes.SelectedItem).clave,
                    viajeSencillo = string.IsNullOrEmpty(this.txtViajeSencillo.Text.Trim()) ? decimal.Zero : Convert.ToDecimal(this.txtViajeSencillo.Text.Trim()),
                    viajeFull = string.IsNullOrEmpty(this.txtViajeFull.Text.Trim()) ? decimal.Zero : Convert.ToDecimal(this.txtViajeFull.Text.Trim()),
                    tipoPago = (ClasificacionPagoChofer)this.cbxTipoPagoOperador.SelectedItem
                };
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return null;
            }
        }

        private void mapRelacion(OrigenDestinos relacion)
        {
            if (relacion != null)
            {
                this.txtClaveRelacion.Tag = relacion;
                this.txtClaveRelacion.Text = relacion.idOrigenDestino.ToString();
                this.clasificacion = relacion.clasificacion;
                this.controlRelacion.IsEnabled = false;
                this.btnNuevoRelacion.IsEnabled = true;
                this.btnGuardarRelacion.IsEnabled = false;
                this.llenarLista((Cliente)this.cbxClientes.SelectedItem);
                this.chcViaje.IsChecked = new bool?(relacion.viaje);
            }
            else
            {
                this.txtClaveDestino.Tag = null;
                this.txtClaveRelacion.Clear();
                this.cbxDestino.SelectedItem = null;
                this.cbxOrigen.SelectedItem = null;
                this.txtPreFull.Clear();
                this.txtPreSencillo.Clear();
                this.txtKm.Clear();
                this.controlRelacion.IsEnabled = true;
                this.btnNuevoRelacion.IsEnabled = false;
                this.btnGuardarRelacion.IsEnabled = true;
                this.cbxOrigen.IsEnabled = true;
                this.cbxDestino.IsEnabled = true;
                this.chcViaje.IsChecked = false;
                this.txtClaveRelacion.Tag = null;
                this.btnQuitarRelacion.IsEnabled = false;
                this.txtViajeSencillo.Clear();
                this.txtViajeFull.Clear();
            }
        }

        public override void nuevo()
        {
            this.mapOrigen(null);
            this.mapDestino(null);
            this.mapRelacion(null);
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
        }
        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.lvUsers.ItemsSource != null)
            {
                CollectionViewSource.GetDefaultView(this.lvUsers.ItemsSource).Refresh();
            }
        }

        private bool UserFilter(object item) =>
            (((item as OrigenDestinoClientes).origen.IndexOf(this.txtOrigen.Text, StringComparison.OrdinalIgnoreCase) >= 0) && ((item as OrigenDestinoClientes).destino.IndexOf(this.txtFilter.Text, StringComparison.OrdinalIgnoreCase) >= 0));

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                this.mapComboClientes();
                this.mapEstados();
                this.mapClasificaciones();
                this.cbxTipoPagoOperador.ItemsSource = Enum.GetValues(typeof(ClasificacionPagoChofer));
                this.cbxTipoPagoOperador.SelectedIndex = 0;
                this.nuevo();
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        public ListSortDirection Direction { get; private set; }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if(lvUsers.ItemsSource == null )return;
                var listaOrigenesDestino = lvUsers.ItemsSource.Cast<OrigenDestinoClientes>().ToList();
                new ReportView().exportarListaOrigenesDestino(listaOrigenesDestino, (cbxClientes.SelectedItem as Cliente).nombre);

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }

}
