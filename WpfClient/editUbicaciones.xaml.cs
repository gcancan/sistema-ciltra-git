﻿using CoreFletera.Interfaces;
using Core.Interfaces;
using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editUbicaciones.xaml
    /// </summary>
    public partial class editUbicaciones : ViewBase
    {
        public editUbicaciones()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                cargarPais();
                cargarEstados();
                cargarCiudades();
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }   
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        List<Pais> listaPais = new List<Pais>();
        void cargarPais()
        {
            try
            {
                listaPais = new List<Pais>();
                var resp = new PaisSvc().getAllPais();
                if (resp.typeResult == ResultTypes.success)
                {
                    listaPais = resp.result as List<Pais>;
                    cbxPais.ItemsSource = listaPais.OrderBy(s => s.nombrePais);
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        List<Estado> listaEstados = new List<Estado>();
        void cargarEstados()
        {
            try
            {
                listaEstados = new List<Estado>();
                var resp = new EstadoSvc().getEstadosAllorById();
                if (resp.typeResult == ResultTypes.success)
                {
                    listaEstados = resp.result as List<Estado>;
                    cbxEstado.ItemsSource = listaEstados.OrderBy(s => s.nombre);
                    cbxEstado2.ItemsSource = listaEstados.OrderBy(s => s.nombre);
                }
                else if(resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        List<Ciudad> listaCiudad = new List<Ciudad>();
        void cargarCiudades()
        {
            try
            {
                listaCiudad = new List<Ciudad>();
                var resp = new CiudadSvc().getAllCiudad();
                if (resp.typeResult == ResultTypes.success)
                {
                    listaCiudad = resp.result as List<Ciudad>;
                    cbxCiudad.ItemsSource = listaCiudad.OrderBy(s => s.nombre);
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private void BtnGuardarPais_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (string.IsNullOrEmpty(txtNombrePais.Text.Trim()))
                {
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "SE REQUIERE EL NOMBRE DEL PAÍS"));
                    txtNombrePais.Focus();
                    return;
                }
                Pais pais = new Pais
                {
                    idPais = 0,
                    nombrePais = txtNombrePais.Text.Trim()
                };
                var resp = new PaisSvc().savePais(ref pais);
                if (resp.typeResult == ResultTypes.success)
                {
                    listaPais.Add(pais);
                    cbxPais.ItemsSource = listaPais.OrderBy(s => s.nombrePais);
                    ctrClavePais.valor = pais.idPais;
                    btnGuardarPais.IsEnabled = false;
                    stpControlesPais.IsEnabled = false;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BtnNuevoPais_Click(object sender, RoutedEventArgs e)
        {
            ctrClavePais.valor = 0;
            txtNombrePais.Clear();
            btnGuardarPais.IsEnabled = true;
            stpControlesPais.IsEnabled = true;
        }

        private void BtnGuardarEstado_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (string.IsNullOrEmpty(txtNombreEstado.Text.Trim()))
                {
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "SE REQUIERE EL NOMBRE DEL ESTADO"));
                    txtNombreEstado.Focus();
                    return;
                }

                if (cbxPais.SelectedItem == null)
                {
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "ELEGIR UN PAÍS"));
                    cbxPais.Focus();
                    return;
                }

                Estado estado = new Estado
                {
                    idEstado = 0,
                    nombre = txtNombreEstado.Text.Trim(),
                    pais = cbxPais.SelectedItem as Pais
                };
                var resp = new EstadoSvc().saveEstado(ref estado);
                if (resp.typeResult == ResultTypes.success)
                {
                    listaEstados.Add(estado);
                    cbxEstado.ItemsSource = listaEstados.OrderBy(s => s.nombre);
                    cbxEstado2.ItemsSource = listaEstados.OrderBy(s => s.nombre);
                    ctrClaveEstado.valor = estado.idEstado;
                    btnGuardarEstado.IsEnabled = false;
                    stpControlesEstado.IsEnabled = false;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void BtnNuevoEstado_Click(object sender, RoutedEventArgs e)
        {
            ctrClaveEstado.valor = 0;
            txtNombreEstado.Clear();
            btnGuardarEstado.IsEnabled = true;
            stpControlesEstado.IsEnabled = true;
        }

        private void BtnGuardarCiudad_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (string.IsNullOrEmpty(txtNombreCiudad.Text.Trim()))
                {
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "SE REQUIERE EL NOMBRE DE LA CIUDAD"));
                    txtNombreCiudad.Focus();
                    return;
                }

                if (cbxEstado.SelectedItem == null)
                {
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "ELEGIR UN ESTADO"));
                    cbxEstado.Focus();
                    return;
                }

                Ciudad ciudad = new Ciudad
                {
                    idCiudad = 0,
                    nombre = txtNombreCiudad.Text.Trim(),
                    estado = cbxEstado.SelectedItem as Estado
                };
                var resp = new CiudadSvc().saveCiudad(ref ciudad);
                if (resp.typeResult == ResultTypes.success)
                {
                    listaCiudad.Add(ciudad);
                    cbxCiudad.ItemsSource = listaCiudad.OrderBy(s => s.nombre);
                    ctrCleveCiudad.valor = ciudad.idCiudad;
                    btnGuardarCiudad.IsEnabled = false;
                    stpControlesCiudad.IsEnabled = false;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BtnBuevoCiudad_Click(object sender, RoutedEventArgs e)
        {
            ctrCleveCiudad.valor = 0;
            txtNombreCiudad.Clear();
            btnGuardarCiudad.IsEnabled = true;
            stpControlesCiudad.IsEnabled = true;
        }

        private void BtnNuevoGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (string.IsNullOrEmpty(txtNombreColonia.Text.Trim()))
                {
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "SE REQUIERE EL NOMBRE DE LA COLONIA"));
                    txtNombreCiudad.Focus();
                    return;
                }

                if (cbxCiudad.SelectedItem == null)
                {
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "ELEGIR UNA CIUDAD"));
                    cbxCiudad.Focus();
                    return;
                }
                if (ctrCP.valor == 0)
                {
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "EL CP ES OBLIGATORIO"));
                    ctrCP.txtEntero.Focus();
                    return;
                }

                Colonia colonia = new Colonia
                {
                    idColonia = 0,
                    nombre = txtNombreColonia.Text.Trim(),
                    ciudad = cbxCiudad.SelectedItem as Ciudad,
                    cp = ctrCP.valor.ToString()
                };
                var resp = new ColoniaSvc().saveColonia(ref colonia);
                if (resp.typeResult == ResultTypes.success)
                {
                    
                    ctrClaveColonia.valor = colonia.idColonia;
                    btnNuevoGuardar.IsEnabled = false;
                    stpControlesColonia.IsEnabled = false;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BtnNuevoColonia_Click(object sender, RoutedEventArgs e)
        {
            stpControlesColonia.IsEnabled = true;
            btnNuevoGuardar.IsEnabled = true;
            ctrClaveColonia.valor = 0;
            txtNombreColonia.Clear();
            ctrCP.valor = 0;
        }

        private void CbxEstado2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (cbxEstado2.SelectedItem != null)
                {
                    cbxCiudad.ItemsSource = listaCiudad.FindAll(s => s.estado.idEstado == (cbxEstado2.SelectedItem as Estado).idEstado).ToList();
                    if (cbxCiudad.Items.Count > 0) cbxCiudad.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new DireccionSvc().getALLColoniasById();
                if (resp.typeResult == ResultTypes.success)
                {
                   var r = (resp.result as List<Colonia>);
                    new ReportView().exportarListaColonias(r);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
