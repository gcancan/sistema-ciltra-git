﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para controlExistenciaEmpresas.xaml
    /// </summary>
    public partial class controlExistenciaEmpresas : UserControl
    {
        GrupoExistenciaDiario grupoExistencia = null;
        Empresa empresa = null;
        List<ExistenciaDiario> listaExixtencias = new List<ExistenciaDiario>();
        ZonaOperativa zonaOperativa = null;
        public controlExistenciaEmpresas()
        {
            InitializeComponent();
        }
        public controlExistenciaEmpresas(GrupoExistenciaDiario grupoExistencia)
        {
            InitializeComponent();
            this.grupoExistencia = grupoExistencia;
            this.empresa = grupoExistencia.empresa;
        }
        public controlExistenciaEmpresas(List<ExistenciaDiario> listaExixtencias, ZonaOperativa zonaOperativa)
        {
            InitializeComponent();
            this.listaExixtencias = listaExixtencias;
            this.zonaOperativa = zonaOperativa;

        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            stpExistencias.Children.Clear();


            if (empresa != null)
            {
                lblNombreEmpresa.Content = grupoExistencia.empresa.nombre.ToUpper();
                switch (empresa.clave)
                {
                    case 1:
                        borderHeader.Background = Brushes.Green;
                        stpExistencias.Background = Brushes.Green;
                        break;
                    case 2:
                        borderHeader.Background = Brushes.SteelBlue;
                        stpExistencias.Background = Brushes.SteelBlue;
                        break;
                    case 5:
                        borderHeader.Background = Brushes.Red;
                        stpExistencias.Background = Brushes.Red;
                        break;
                    default:
                        break;
                }
                llenarExistencias();
            }
            else
            {
                lblNombreEmpresa.Content = string.Format("TANQUE {0}", zonaOperativa.nombre.ToUpper());
                borderHeader.Background = Brushes.SteelBlue;
                llenarDiasExistencias();
            }
        }

        private void llenarDiasExistencias()
        {
            List<GrupoDiasExistenciaDiario> listaGrupoDias = (from v in listaExixtencias group v by v.fecha into grupo
                select new GrupoDiasExistenciaDiario
                {
                    fecha = grupo.Select(s=> s.fecha).First(),
                    listaExistencias = grupo.ToList()
                }).ToList();
            listaGrupoDias = listaGrupoDias.OrderBy(s => s.fecha).ToList();
            foreach (var lista in listaGrupoDias)
            {
                StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal };
                List<ExistenciaDiario> listaExis = lista.listaExistencias;
                Label lblInicial = new Label
                {
                    Content = listaExis.Sum(s=>s.inicial).ToString("N2"),
                    Width = 70,
                    BorderThickness = new Thickness(1),
                    BorderBrush = Brushes.Black,
                    HorizontalContentAlignment = HorizontalAlignment.Right,
                    FontSize = 10,
                    Foreground = empresa != null ? Brushes.White : Brushes.Black
                };
                Label lblEntradas = new Label
                {
                    Content = listaExis.Sum(s => s.entradas).ToString("N2"),
                    Width = 70,
                    BorderThickness = new Thickness(1),
                    BorderBrush = Brushes.Black,
                    HorizontalContentAlignment = HorizontalAlignment.Right,
                    FontSize = 10,
                    Foreground = empresa != null ? Brushes.White : Brushes.Black
                };
                Label lblSalidas = new Label
                {
                    Content = listaExis.Sum(s => s.salidas).ToString("N2"),
                    Width = 70,
                    BorderThickness = new Thickness(1),
                    BorderBrush = Brushes.Black,
                    HorizontalContentAlignment = HorizontalAlignment.Right,
                    FontSize = 10,
                    Foreground = empresa != null ? Brushes.White : Brushes.Black
                };
                Label lblFinal = new Label
                {
                    Content = listaExis.Sum(s => s.final).ToString("N2"),
                    Width = 70,
                    BorderThickness = new Thickness(1),
                    BorderBrush = Brushes.Black,
                    HorizontalContentAlignment = HorizontalAlignment.Right,
                    FontSize = 10,
                    Foreground = empresa != null ? Brushes.White : Brushes.Black,
                    FontWeight = FontWeights.Bold
                };
                stpContent.Children.Add(lblInicial);
                stpContent.Children.Add(lblEntradas);
                stpContent.Children.Add(lblSalidas);
                stpContent.Children.Add(lblFinal);

                stpExistencias.Children.Add(stpContent);
            }
        }

        private void llenarExistencias()
        {
            foreach (ExistenciaDiario existencia in grupoExistencia.listaExistencias.OrderBy(s => s.fecha).ToList())
            {
                StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal };
                Label lblInicial = new Label
                {
                    Content = existencia.inicial.ToString("N2"),
                    Width = 70,
                    BorderThickness = new Thickness(1),
                    BorderBrush = Brushes.Black,
                    HorizontalContentAlignment = HorizontalAlignment.Right,
                    FontSize = 10,
                    Foreground = empresa != null ? Brushes.White : Brushes.Black
                };
                Label lblEntradas = new Label
                {
                    Content = existencia.entradas.ToString("N2"),
                    Width = 70,
                    BorderThickness = new Thickness(1),
                    BorderBrush = Brushes.Black,
                    HorizontalContentAlignment = HorizontalAlignment.Right,
                    FontSize = 10,
                    Foreground = empresa != null ? Brushes.White : Brushes.Black
                };
                Label lblSalidas = new Label
                {
                    Content = existencia.salidas.ToString("N2"),
                    Width = 70,
                    BorderThickness = new Thickness(1),
                    BorderBrush = Brushes.Black,
                    HorizontalContentAlignment = HorizontalAlignment.Right,
                    FontSize = 10,
                    Foreground = empresa != null ? Brushes.White : Brushes.Black
                };
                Label lblFinal = new Label
                {
                    Content = existencia.final.ToString("N2"),
                    Width = 70,
                    BorderThickness = new Thickness(1),
                    BorderBrush = Brushes.Black,
                    HorizontalContentAlignment = HorizontalAlignment.Right,
                    FontSize = 10,
                    Foreground = empresa != null ? Brushes.White : Brushes.Black,
                    FontWeight = FontWeights.Bold
                };
                stpContent.Children.Add(lblInicial);
                stpContent.Children.Add(lblEntradas);
                stpContent.Children.Add(lblSalidas);
                stpContent.Children.Add(lblFinal);

                stpExistencias.Children.Add(stpContent);
            }
        }
    }
}
