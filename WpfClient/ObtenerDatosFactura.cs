﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;

namespace WpfClient
{
    public static class ObtenerDatosFactura
    {
        public static DatosFacturaXML obtenerDatos(string rutaArchivo)
        {
            try
            {
                DatosFacturaXML DatosFacturaXML = new DatosFacturaXML();
                XmlDocument xDoc = new XmlDocument();

                xDoc.Load(rutaArchivo);
                var mgr = new XmlNamespaceManager(xDoc.NameTable);
                mgr.AddNamespace("cfdi", "http://www.sat.gob.mx/cfd/3");
                XmlNode NodeComprobante = xDoc.SelectSingleNode("//cfdi:Comprobante", mgr);
                DatosFacturaXML.noCertificado = NodeComprobante.Attributes["noCertificado"].Value.ToLower();
                DatosFacturaXML.sello = NodeComprobante.Attributes["sello"].Value.ToLower();

                mgr.AddNamespace("tfd", "http://www.sat.gob.mx/TimbreFiscalDigital");
                XmlNode tf = xDoc.SelectSingleNode("//cfdi:Comprobante/cfdi:Complemento/tfd:TimbreFiscalDigital", mgr);
                DatosFacturaXML.selloSAT = tf.Attributes["selloSAT"].Value.ToLower();
                DatosFacturaXML.selloCFD = tf.Attributes["selloCFD"].Value.ToLower();
                DatosFacturaXML.UUID = tf.Attributes["UUID"].Value.ToLower();
                DatosFacturaXML.noCertificadoSAT = tf.Attributes["noCertificadoSAT"].Value.ToLower();
                return DatosFacturaXML;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
            
        }
    }
}
