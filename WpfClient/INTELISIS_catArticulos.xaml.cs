﻿using Core.Interfaces;
using Core.Models;
using CoreINTELISIS.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para INTELISIS_catArticulos.xaml
    /// </summary>
    public partial class INTELISIS_catArticulos : UserControl
    {
        SincronizadorINTELISIS pantalla = null;
        public INTELISIS_catArticulos(SincronizadorINTELISIS pantalla)
        {
            InitializeComponent();
            this.pantalla = pantalla;
        }
        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            pantalla.stpContenedor.Children.Clear();
        }
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lvlArticulos.ItemsSource = null;
                lblContador.Content = 0;
                lblActualizados.Content = 0;
                lblNuevos.Content = 0;
                Cursor = Cursors.Wait;
                var resp = new ArticulosServiceINTELISISSvc().getAllArticulos();
                if (resp.typeResult == ResultTypes.success)
                {
                    List<TipoUnidad> listaTipos = resp.result as List<TipoUnidad>;
                    lvlArticulos.ItemsSource = listaTipos;
                    lblContador.Content = listaTipos.Count;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                lblActualizados.Content = 0;
                lblNuevos.Content = 0;
                if (lvlArticulos.ItemsSource != null)
                {
                    List<TipoUnidad> listaTipoUnidad = lvlArticulos.ItemsSource as List<TipoUnidad>;
                    var resp = new TipoUnidadSvc().sincronizarTiposUnidad(listaTipoUnidad);
                    lblActualizados.Content = resp.noActualizados;
                    lblNuevos.Content = resp.noNuevos;
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        
    }
}
