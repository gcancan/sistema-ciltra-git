﻿using Core.Models;
using Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for selectCartaPorte.xaml
    /// </summary>
    public partial class selectCartaPorte : Window
    {
        public selectCartaPorte()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            dtpFechaInicio.SelectedDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            dtpFechaFin.SelectedDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
        }

        public Viaje buscarCartaPorte()
        {
            bool? sResult = ShowDialog();
            if (sResult.Value && lvlSalidas.SelectedItem != null)
            {
                return (Viaje)((ListViewItem)lvlSalidas.SelectedItem).Content;
            }
            return null;
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            buscarCartasPorte();
        }
        internal void buscarCartasPorte()
        {
            lvlSalidas.Items.Clear();
            List<Viaje> listViaje = new List<Viaje>();
            //OperationResult resp = new CartaPorteSvc().getCartaPorteByFecha(dtpFechaInicio.SelectedDate.Value, dtpFechaFin.SelectedDate.Value.AddDays(1));

            //if (resp.typeResult == ResultTypes.error)
            //{
            //    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
            //else if (resp.typeResult == ResultTypes.warning)
            //{
            //    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
            //}
            //else if (resp.typeResult == ResultTypes.recordNotFound)
            //{
            //    //MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            //}
            //else if (resp.typeResult == ResultTypes.success)
            //{
            //    foreach (var item in (List<Viaje>)resp.result)
            //    {
            //        listViaje.Add(item);
            //    }
            //}
           
            //if (listViaje.Count == 0)
            //{
            //    MessageBox.Show("No se encontraron registros", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            //}
            //else
            //{
            //    mapLista(listViaje);
            //}
        }

        private void mapLista(List<Viaje> listViaje)
        {
            foreach (Viaje item in listViaje)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.FontWeight = FontWeights.Bold;
                lvl.MouseDoubleClick += Lvl_Selected;
                if (item.EstatusGuia.ToUpper() == "ACTIVO")
                {
                    lvl.Foreground = Brushes.SteelBlue;
                }
                if (item.EstatusGuia.ToUpper() == "CANCELADO")
                {
                    lvl.Foreground = Brushes.OrangeRed;
                }
                if (item.EstatusGuia.ToUpper() == "FINALIZADO")
                {
                    lvl.Foreground = Brushes.Blue;
                }
                lvlSalidas.Items.Add(lvl);
            }
        }

        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
