﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editOrdenesServicioExterno.xaml
    /// </summary>
    public partial class editOrdenesServicioExterno : ViewBase
    {
        public editOrdenesServicioExterno()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            nuevo();
            mapProveedores();
            txtFolio.txtEntero.KeyDown += TxtEntero_KeyDown;
        }

        private void mapProveedores()
        {
            var resp = new ProveedorSvc().getAllProveedores();
            if (resp.typeResult == ResultTypes.success)
            {
                cbxProveedor.ItemsSource = resp.result as List<Proveedor>;
            }
            else if (resp.typeResult != ResultTypes.recordNotFound)
            {
                ImprimirMensaje.imprimir(resp);
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            mapOrdenServicio(null);
            lblMensaje.Visibility = Visibility.Hidden;
        }
        private void TxtEntero_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && !string.IsNullOrEmpty(txtFolio.txtEntero.Text.Trim()))
            {
                buscarOrdenTraabajo(txtFolio.valor);
            }
        }

        private void buscarOrdenTraabajo(int valor)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new OperacionSvc().getOrdenServById(valor);
                if (resp.typeResult == ResultTypes.success)
                {
                    TipoOrdenServicio orden = resp.result as TipoOrdenServicio;
                    if (orden.externo)
                    {
                        mapOrdenServicio(orden);
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "Esta Orden No Es Externa"));
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void mapOrdenServicio(TipoOrdenServicio orden)
        {
            if (orden != null)
            {
                txtFolio.valor = orden.idOrdenSer;
                txtFolio.Tag = orden;
                txtFolio.IsEnabled = false;
                txtUnidad.Text = orden.unidadTrans.clave;
                txtEmpresa.Text = orden.unidadTrans.empresa.nombre;
                txtTipoOrden.Text = orden.TipoServicio;
                txtEstatus.Text = orden.estatus;
                txtChofer.Text = orden.chofer;
                txtTipoServicio.Text = orden.tipoOrden.nombre;
                stpActividades.Children.Clear();
                txtFactura.Text = orden.factura;

                if (orden.proveedor != null)
                {
                    int i = 0;
                    foreach (Proveedor item in cbxProveedor.ItemsSource)
                    {
                        if (item.idProveedor == orden.proveedor.idProveedor)
                        {
                            cbxProveedor.SelectedIndex = i;
                            break;
                        }
                        i++;
                    }
                }

                var resp = new MasterOrdServSvc().getTrabajosByOrdenSer(orden.idOrdenSer);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<OrdenTaller> listaActividades = resp.result as List<OrdenTaller>;

                    foreach (var item in listaActividades)
                    {
                        agregarListaActividades(item);

                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            else
            {
                txtFolio.limpiar();
                txtFolio.Tag = null;
                txtFolio.IsEnabled = true;
                txtUnidad.Clear();
                txtTipoOrden.Clear();
                txtEstatus.Clear();
                stpActividades.Children.Clear();
                IsEnabled = true;
                txtTipoServicio.Clear();
                txtEmpresa.Clear();
                txtChofer.Clear();
                _listaOrdenes = new List<OrdenTaller>();
                txtFactura.Clear();
                cbxProveedor.SelectedItem = null;
            }
        }

        private void agregarListaActividades(OrdenTaller actividad)
        {
            try
            {
                var act = _listaOrdenes.Find(s => s.idOrdenTrabajo == actividad.idOrdenTrabajo);
                actividad.costoActividad = act == null ? 0 : act.costoActividad;
                actividad.listaProductosCOM = act == null ? new List<ProductosCOM>() : act.listaProductosCOM;

                Button btnActividad = new Button
                {
                    HorizontalContentAlignment = HorizontalAlignment.Left,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(1),
                    Background = null,
                    Tag = actividad
                };

                if (actividad.actividad == null)
                {
                    ContextMenu menu = new ContextMenu();
                    MenuItem menuCandelar = new MenuItem { Header = "Cancelar" };
                    menuCandelar.Tag = actividad;
                    menuCandelar.Click += MenuCandelar_Click;
                    menu.Items.Add(menuCandelar);

                    MenuItem menuCambiar = new MenuItem { Tag = actividad, Header = "Cambiar" };
                    menuCambiar.Click += MenuCambiar_Click;
                    menu.Items.Add(menuCambiar);

                    btnActividad.ContextMenu = menu;
                }
                StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal, Tag = actividad };

                Label lblNo = new Label
                {
                    Content = actividad.idOrdenTrabajo,
                    Width = 80,
                    Margin = new Thickness(1)
                };
                stpContent.Children.Add(lblNo);

                Label lblTipo = new Label
                {
                    Content = actividad.tipoDescripcion,
                    Width = 200,
                    Margin = new Thickness(1)
                };
                stpContent.Children.Add(lblTipo);

                TextBox lblDescripcion = new TextBox
                {
                    Text = actividad.descripcionActividad,
                    Width = 280,
                    TextWrapping = TextWrapping.Wrap,
                    IsReadOnly = true,
                    BorderBrush = null,
                    Background = null,
                    Focusable = false,
                    IsEnabled = false,
                    Margin = new Thickness(1)
                };
                stpContent.Children.Add(lblDescripcion);

                controlDecimal ctrDecimal = new controlDecimal
                {
                    valor = actividad.costoActividad,
                    Width = 90,
                    Margin = new Thickness(1),
                    Tag = actividad,
                };
                ctrDecimal.txtDecimal.Tag = ctrDecimal;
                ctrDecimal.txtDecimal.TextChanged += TxtDecimal_TextChanged;
                stpContent.Children.Add(ctrDecimal);


                btnActividad.Content = stpContent;
                btnActividad.Click += BtnActividad_Click;
                stpActividades.Children.Add(btnActividad);

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void BtnActividad_Click(object sender, RoutedEventArgs e)
        {
            OrdenTaller ordenTaller = (sender as Button).Tag as OrdenTaller;
            if (ordenTaller.actividad == null)
            {
                lblMensaje.Visibility = Visibility.Visible;
                gBoxOrdenes.IsEnabled = false;
                _orden = null;
                actualizarListaInsumos(null);
            }
            else
            {
                lblMensaje.Visibility = Visibility.Hidden;
                gBoxOrdenes.IsEnabled = true;
                _orden = ordenTaller;
                actualizarListaInsumos(_orden);
            }
        }

        private void TxtDecimal_TextChanged(object sender, TextChangedEventArgs e)
        {
            controlDecimal controlDecimal = (sender as TextBox).Tag as controlDecimal;
            OrdenTaller orden = controlDecimal.Tag as OrdenTaller;
            orden.costoActividad = controlDecimal.valor;
        }
        List<OrdenTaller> _listaOrdenes = new List<OrdenTaller>();
        private void MenuCambiar_Click(object sender, RoutedEventArgs e)
        {
            var menuItem = sender as MenuItem;
            var orden = menuItem.Tag as OrdenTaller;
            _listaOrdenes = getListaActividades();

            //foreach (ListViewItem item in lvlActividades.Items)
            //{
            //    listaOrdenes.Add((OrdenTaller)item.Content);
            //}

            agregarOrdenLlantera agregarOrd = new agregarOrdenLlantera(orden, txtFolio.Tag as TipoOrdenServicio, (txtFolio.Tag as TipoOrdenServicio).TipoServicio == "LLANTAS", _listaOrdenes, true);
            if (agregarOrd.ShowDialog().Value)
            {
                buscarOrdenTraabajo(txtFolio.valor);
            }
        }

        private void MenuCandelar_Click(object sender, RoutedEventArgs e)
        {
            var menuItem = sender as MenuItem;
            var orden = menuItem.Tag as OrdenTaller;
            var resp = new cancelarOrden(orden, mainWindow.inicio._usuario.nombreUsuario).cancelarOrdenTrabajo();
            if (resp.Value)
            {
                buscarOrdenTraabajo(txtFolio.valor);
            }
        }
        private void btnAgregarOrden_Click(object sender, RoutedEventArgs e)
        {
            _listaOrdenes = getListaActividades();
            agregarOrdenLlantera agregarOrd = new agregarOrdenLlantera(txtFolio.Tag as TipoOrdenServicio, (txtFolio.Tag as TipoOrdenServicio).TipoServicio == "LLANTAS", _listaOrdenes, true);
            if (agregarOrd.ShowDialog().Value)
            {
                buscarOrdenTraabajo(txtFolio.valor);
            }
        }

        List<OrdenTaller> getListaActividades()
        {
            List<Button> listButon = stpActividades.Children.Cast<Button>().ToList();
            List<OrdenTaller> list = new List<OrdenTaller>();
            foreach (var item in listButon)
            {
                list.Add(item.Tag as OrdenTaller);
            }
            return list;
        }

        private void txtInsumo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                buscarProducto();
            }
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            buscarProducto();
        }
        OrdenTaller _orden = null;
        void buscarProducto()
        {
            //lvlOrdenesInsumos.Colums
            var lista = new selectProductosCOM(txtInsumo.Text.Trim()).consultasTallerSinExistencias(AreaTrabajo.TOTAS);
            if (lista != null)
            {
                if (_orden != null && lista.Count > 0)
                {
                    _orden.listaProductosCOM = lista;
                    actualizarListaInsumos(_orden);
                }
            }
            txtInsumo.Clear();
        }

        private void actualizarListaInsumos(OrdenTaller orden)
        {
            stpInsumos.Children.Clear();
            if (orden != null)
            {
                foreach (var item in orden.listaProductosCOM)
                {
                    mapListaProducto(item);
                }
            }
        }

        private void mapListaProducto(ProductosCOM producto)
        {
            try
            {
                Button btnProducto = new Button
                {
                    HorizontalContentAlignment = HorizontalAlignment.Left,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(1),
                    Background = null,
                    Tag = producto
                };

                ContextMenu menu = new ContextMenu();
                MenuItem menuCandelar = new MenuItem { Header = "Quitar" };
                menuCandelar.Tag = producto;
                menuCandelar.Click += MenuCandelar_Click;
                menu.Items.Add(menuCandelar);

                btnProducto.ContextMenu = menu;

                StackPanel stpContentProducto = new StackPanel { Orientation = Orientation.Horizontal, Tag = producto };

                TextBox lblDescripcion = new TextBox
                {
                    Text = producto.nombre,
                    Width = 250,
                    TextWrapping = TextWrapping.Wrap,
                    IsReadOnly = true,
                    BorderBrush = null,
                    Background = null,
                    Focusable = false,
                    IsEnabled = false,
                    Margin = new Thickness(1)
                };
                stpContentProducto.Children.Add(lblDescripcion);

                controlDecimal ctrCantidad = new controlDecimal
                {
                    valor = producto.cantidad,
                    Width = 100,
                    Margin = new Thickness(1),
                    Tag = producto,
                };
                ctrCantidad.txtDecimal.Tag = ctrCantidad;
                ctrCantidad.txtDecimal.TextChanged += TxtDecimal_CantidadProducto; ;
                stpContentProducto.Children.Add(ctrCantidad);

                controlDecimal ctrCosto = new controlDecimal
                {
                    valor = producto.precio,
                    Width = 100,
                    Margin = new Thickness(1),
                    Tag = producto,
                };
                ctrCosto.txtDecimal.Tag = ctrCosto;
                ctrCosto.txtDecimal.TextChanged += TxtDecimal_CostoProducto; ; ;
                stpContentProducto.Children.Add(ctrCosto);

                btnProducto.Content = stpContentProducto;
                btnProducto.Click += BtnProducto_Click; ;
                stpInsumos.Children.Add(btnProducto);

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void TxtDecimal_CostoProducto(object sender, TextChangedEventArgs e)
        {
            controlDecimal controlDecimal = (sender as TextBox).Tag as controlDecimal;
            ProductosCOM producto = controlDecimal.Tag as ProductosCOM;
            producto.precio = controlDecimal.valor;
        }

        private void BtnProducto_Click(object sender, RoutedEventArgs e)
        {

        }

        private void TxtDecimal_CantidadProducto(object sender, TextChangedEventArgs e)
        {
            controlDecimal controlDecimal = (sender as TextBox).Tag as controlDecimal;
            ProductosCOM producto = controlDecimal.Tag as ProductosCOM;
            producto.cantidad = controlDecimal.valor;
        }
        public override OperationResult guardar()
        {
            try
            {
                OperationResult val = validar();
                if (val.typeResult != ResultTypes.success)
                {
                    return val;
                }
                Cursor = Cursors.Wait;
                List<OrdenTaller> listaOrden = getListaActividades();
                var resp = new OperacionSvc().saveDetOrdenActividadExterno(listaOrden, cbxProveedor.SelectedItem as Proveedor, txtFactura.Text);
                if (resp.typeResult == ResultTypes.success)
                {
                    buscarOrdenTraabajo(listaOrden[0].idOrdenServ);
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private OperationResult validar()
        {
            OperationResult resp = new OperationResult(ResultTypes.success, "");
            var list = getListaActividades();
            foreach (OrdenTaller orden in list)
            {
                if (orden.actividad != null)
                {
                    if (orden.costoActividad <= 0)
                    {
                        resp.valor = 2;
                        resp.mensaje += string.Format("El precio de la actividad {0} tiene que ser mayor a 0", orden.actividad.nombre) + Environment.NewLine;
                    }
                    foreach (ProductosCOM producto in orden.listaProductosCOM)
                    {
                        if (producto.importe <= 0)
                        {
                            resp.valor = 2;
                            resp.mensaje += string.Format("El costo del insumo/refacción {0} de la orden de trabajo {1} tiene que ser mayor a 0", producto.nombre, orden.actividad.nombre) + Environment.NewLine;
                        }
                    }
                }
                else
                {
                    resp.valor = 2;
                    resp.mensaje += string.Format("La orden de trabajo {0} es falla reportada", orden.idOrdenTrabajo.ToString()) + Environment.NewLine;
                }
                
            }
            return resp;
        }
    }
}
