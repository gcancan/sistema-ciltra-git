﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoreFletera.Models;
using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for detalleLavadero.xaml
    /// </summary>
    public partial class detalleLavadero : ViewBase
    {
        private BitacoraLavadero bitacora;

        public detalleLavadero()
        {
            InitializeComponent();
        }

        public detalleLavadero(BitacoraLavadero bitacora)
        {
            this.bitacora = bitacora;
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            buscarMasterOrdenSer(bitacora.idPadreOrdSer);
        }

        private void buscarMasterOrdenSer(int idPadreOrdSer)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new MasterOrdServSvc().getMasterOrdServ(idPadreOrdSer);
                if (resp.typeResult == ResultTypes.success)
                {
                    
                    MasterOrdServ master = (MasterOrdServ)resp.result;
                    foreach (var item in master.listTiposOrdenServicio)
                    {
                        if (item.idOrdenSer == bitacora.idOrdenServ)
                        {
                            mapOrdenServ(item);
                        }
                    }                   
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapOrdenServ(TipoOrdenServicio item)
        {
            txtFolio.Text = item.idOrdenSer.ToString();
            txtUnidad.Text = item.unidadTrans.clave;
            txtEstatusOrden.Text = item.estatus;
            var resp = new MasterOrdServSvc().getActividadesByOrdenSer(item.idOrdenSer);
            if (resp.typeResult == ResultTypes.success)
            {
                var act = ((List<Actividad>)resp.result)[0];
                txtActividad.Text = act.nombre;
                txtPersonal.Text = act.personal1.nombre;
                txtEstatusAct.Text = act.estatusCompleto;
                mapListaInsumos(act.listProductos);
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
                IsEnabled = false;
            }

            if (item.estatus == "TER")
            {
                var respInsp = new InspeccionesSvc().getInspeccionesRealizadas(item.idOrdenSer);                
                if (respInsp.typeResult == ResultTypes.success)
                {
                    mapInspecciones((List<Inspecciones>)respInsp.result, item.estatus != "TER");
                }
                else
                {
                    return;
                }
            }
        }

        private void mapListaInsumos(List<ProductosActividad> listProductos)
        {
            lvlInsumos.ItemsSource = listProductos;
        }

        private void mapInspecciones(List<Inspecciones> listaInspecciones, bool habilitar)
        {
            //bool habilitar = false;
            StackPanel stpPrincipal = new StackPanel()
            {
                Orientation = Orientation.Vertical,
                VerticalAlignment = VerticalAlignment.Top,
                HorizontalAlignment = HorizontalAlignment.Left,
                Margin = new Thickness(8)
            };
            //StackPanel stpPrincipalPeriodicas = new StackPanel()
            //{
            //    Orientation = Orientation.Vertical,
            //    VerticalAlignment = VerticalAlignment.Top,
            //    HorizontalAlignment = HorizontalAlignment.Left,
            //    Margin = new Thickness(8)
            //};
            foreach (var inspeccion in listaInspecciones)
            {
                if (!inspeccion.checado)
                {
                    continue;
                }
                StackPanel stpContenedor = new StackPanel() { IsEnabled = !inspeccion.checado, Orientation = Orientation.Vertical, VerticalAlignment = VerticalAlignment.Top, HorizontalAlignment = HorizontalAlignment.Left, Margin = new Thickness(4) };
                stpContenedor.Tag = inspeccion;
                CheckBox chxBox = new CheckBox() { Tag = inspeccion, Content = inspeccion.nombreInspeccion, IsEnabled = !inspeccion.checado, IsChecked = inspeccion.checado };
                if (!habilitar)
                {
                    chxBox.IsEnabled = habilitar;
                }
                stpContenedor.Children.Add(chxBox);

                foreach (var insumo in inspeccion.listaInsumos)
                {
                    StackPanel stpInsumos = new StackPanel() { Orientation = Orientation.Horizontal, VerticalAlignment = VerticalAlignment.Top, HorizontalAlignment = HorizontalAlignment.Left, Margin = new Thickness(2) };
                    stpInsumos.Tag = insumo;
                    Label texto = new Label() { Foreground = Brushes.SteelBlue, FontStyle = FontStyles.Italic, Content = "   " + insumo.nombre, HorizontalContentAlignment = HorizontalAlignment.Right, Width = 270 };
                    controlDecimal txt = new controlDecimal() { valor = insumo.cantMaxima };
                    stpInsumos.Children.Add(texto);
                    stpInsumos.Children.Add(txt);

                    stpContenedor.Children.Add(stpInsumos);
                }
                //if (inspeccion.dias > 0)
                //{
                //    stpPrincipalPeriodicas.Children.Add(stpContenedor);
                //}
                //else
                {
                    stpPrincipal.Children.Add(stpContenedor);
                }
            }
            scrollInspecciones.Content = stpPrincipal;
        }
    }
}
