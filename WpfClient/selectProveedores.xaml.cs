﻿using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectProveedores.xaml
    /// </summary>
    public partial class selectProveedores : Window
    {
        public selectProveedores()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtFind.Text = parametro;
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (lvlProveedores.ItemsSource != null)
            {
                CollectionViewSource.GetDefaultView(lvlProveedores.ItemsSource).Refresh();
            }
        }
        string parametro = string.Empty;
        public Proveedor getProveedor(List<Proveedor> lista, string parametro)
        {
            try
            {
                this.parametro = parametro;
                llenarLista(lista);
                bool? result = ShowDialog();
                if (result.Value && lvlProveedores.SelectedItem != null)
                {
                    return (Proveedor)((ListViewItem)lvlProveedores.SelectedItem).Content;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }

        private void llenarLista(List<Proveedor> lista)
        {
            List<ListViewItem> listaLvl = new List<ListViewItem>();
            foreach (var item in lista)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick; ; ; ;
                listaLvl.Add(lvl);
            }
            lvlProveedores.ItemsSource = listaLvl;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlProveedores.ItemsSource);
            view.Filter = UserFilter;
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((Proveedor)(item as ListViewItem).Content).razonSocial.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Proveedor)(item as ListViewItem).Content).RFC.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Proveedor)(item as ListViewItem).Content).direccion.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }
    }
}
