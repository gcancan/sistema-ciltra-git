﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Core.Utils;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;

namespace WpfClient
{
    class EnvioCorreo
    {


        public string envioCorreo()
        {
            MailMessage mensajeCorreo = new MailMessage();
            SmtpClient smtp = new Util().getSmtpClient(); //new SmtpClient("smtp.gmail.com", 587);
            var path = System.Windows.Forms.Application.StartupPath;
            string archivo = path + @"\config.ini";

            mensajeCorreo.To.Add(new MailAddress(@"gil.can.can@gmail.com")); //para quien es el correo
            mensajeCorreo.From = new MailAddress(new Util().Read("CONFIGURACION_CORREO", "USUARIO", archivo));
            mensajeCorreo.Subject = "Aviso de salida de unidad";
            mensajeCorreo.Body = ("Texto del correo de aviso de salida de unidad");
            mensajeCorreo.IsBodyHtml = true;
            mensajeCorreo.Priority = MailPriority.Normal;

            //smtp.Credentials = new NetworkCredential(@"gil.can.can@gmail.com", @"Gilverto1#");
            //smtp.EnableSsl = true;
            string output = null;
            try
            {
                smtp.Send(mensajeCorreo);

                output = "Corre electrónico fue enviado satisfactoriamente.";
            }
            catch (Exception ex)
            {
                output = "Error enviando correo electrónico: " + ex.Message;
            }
            finally
            {
                mensajeCorreo.Dispose();
                smtp.Dispose();
            }
            return output;
        }

        public string envioCorreoPrueba(SmtpClient smtp, string usuario)
        {
            MailMessage mensajeCorreo = new MailMessage();
            var path = System.Windows.Forms.Application.StartupPath;
            string archivo = path + @"\config.ini";

            mensajeCorreo.To.Add(new MailAddress(@"gil.can.can@gmail.com")); //para quien es el correo
            mensajeCorreo.From = new MailAddress(usuario);
            mensajeCorreo.Subject = "Correo de prueba";
            mensajeCorreo.Body = (string.Format("Correo de prueba enviado desde {0}, {1}", Environment.MachineName, Environment.UserDomainName));
            mensajeCorreo.IsBodyHtml = true;
            mensajeCorreo.Priority = MailPriority.Normal;

            //smtp.Credentials = new NetworkCredential(@"gil.can.can@gmail.com", @"Gilverto1#");
            //smtp.EnableSsl = true;
            string output = null;
            try
            {
                smtp.Send(mensajeCorreo);

                output = "Correo de prueba enviado correctamente.";
            }
            catch (Exception ex)
            {
                output = "Error enviando correo electrónico: " + ex.Message;
            }
            finally
            {
                mensajeCorreo.Dispose();
                smtp.Dispose();
            }
            return output;
        }

        public string envioCorreoDeCartaPorte(Viaje viaje)
        {
            MailMessage mensajeCorreo = new MailMessage();
            SmtpClient smtp = new Util().getSmtpClient(); //new SmtpClient("smtp.gmail.com", 587);
            var path = System.Windows.Forms.Application.StartupPath;
            string archivo = path + @"\config.ini";

            string output = null;
            try
            {
                foreach (var cp in viaje.listDetalles)
                {
                    mensajeCorreo.To.Add(new MailAddress(/*cp.zonaSelect.correo == ""? "gil.can.can@gmail.com": */cp.zonaSelect.correo)); //para quien es el correo
                    mensajeCorreo.From = new MailAddress(new Util().Read("CONFIGURACION_CORREO", "USUARIO", archivo));
                    mensajeCorreo.Subject = "Aviso de salida de unidad";
                    mensajeCorreo.Body = string.Format("La unidad {0} salio rumbo a {1} con {2} toneladas de {3}",
                        viaje.tractor.clave,
                        cp.zonaSelect.descripcion,
                        cp.volumenDescarga,
                        cp.producto.descripcion);
                    mensajeCorreo.IsBodyHtml = true;
                    mensajeCorreo.Priority = MailPriority.Normal;

                    smtp.Send(mensajeCorreo);
                }



                output = "Corre electrónico fue enviado satisfactoriamente.";
            }
            catch (Exception ex)
            {
                output = "Error enviando correo electrónico: " + ex.Message;
            }
            finally
            {
                mensajeCorreo.Dispose();
                smtp.Dispose();
            }
            return output;
        }

        public string envioCorreoCSI(Viaje viaje, bool cancelacion = false)
        {
            //var resp = new ConfiguracionEnvioCorreoSvc().getCorreosByProceso(cancelacion ? enumProcesosEnvioCorreo.CANCELACION_VIAJES: enumProcesosEnvioCorreo.SALIDA_VIAJES);
            //if (resp.typeResult == ResultTypes.success)
            //{
            MailMessage mensajeCorreo = new MailMessage();
            SmtpClient smtp = new Util().getSmtpClient(); //new SmtpClient("smtp.gmail.com", 587);

            var path = System.Windows.Forms.Application.StartupPath;
            string archivo = path + @"\config.ini";

            var correos = (new Util().getCorreoCSI()).Split(',');
            foreach (var item in correos)
            {
                mensajeCorreo.To.Add(new MailAddress(item)); //para quien es el correo
            }
            //mensajeCorreo.To.Add(new MailAddress("gcan@ciltra.com"));

            List<string> listaGranjas = new List<string> { "SAN ROMAN 1", "SAN ROMAN 2", "SAN ROMAN 3", "SAN ROMAN 4", "SAN ROMAN 5", "SAN ROMAN 6" };

            bool enviarCopia = false;
            foreach (var cp in viaje.listDetalles)
            {
                foreach (var granja in listaGranjas)
                {
                    if (cp.zonaSelect.descripcion == granja)
                    {
                        enviarCopia = true;
                    }
                }
            }
            if (enviarCopia)
            {
                //mensajeCorreo.To.Add("acastro@ciltra.com");
            }

            //List<ConfiguracionEnvioCorreo> listaCorreos = resp.result as List<ConfiguracionEnvioCorreo>;
            //    foreach (var dirCorreo in listaCorreos.Select(s => s.direccionCorreo)) 
            //    {
            //        mensajeCorreo.To.Add(dirCorreo);
            //    }

            mensajeCorreo.From = new MailAddress(new Util().Read("CONFIGURACION_CORREO", "USUARIO", archivo));
            mensajeCorreo.Subject = cancelacion ? "Cancelacion de viaje de Unidad" : "Aviso de Salida de Unidad";
            string body = "<!DOCTYPE html><html lang=\"es\"><head><meta charset=\"utf - 8\" /> ";
            body += "<style>table, td, th, tr {border: 1px solid black;}";
            body += "table {border-collapse: collapse;width: 100%;}";
            body += "th {height: 50px;background-color: yellow;}";
            body += "td{width: 50px}</style></head><body>";
            body += "<table ><colgroup><col /><col /><col /><col /><col /><col /><col /><col /></colgroup><caption>Formato de Remisiones</caption>";
            body += "<thead><tr bgcolor=\"yellow\"><th>Unidad</th><th>Folio</th><th>Operador</th><th>Tolvas</th><th>Peso</th><th># de Pedido</th><th>Producto</th><th>Destino</th></tr></thead>";
            body += "<tbody>";
            int i = 0;
            foreach (var item in viaje.listDetalles)
            {
                if (i == 0)
                {
                    body += string.Format("<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td><td>{7}</td></tr>",
                        viaje.tractor.clave
                        , viaje.NumGuiaId
                        , viaje.operador.nombre
                        , viaje.remolque2 == null ? viaje.remolque.clave : viaje.remolque.clave + "-" + viaje.remolque2.clave
                        , item.volumenDescarga.ToString()
                        , string.IsNullOrEmpty(item.remision) ? string.Empty : item.remision.ToString()
                        , item.producto.descripcion
                        , item.zonaSelect.descripcion);
                }
                else
                {
                    body += string.Format("<tr><td></td><td></td><td></td><td></td><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td></tr>"
                        , item.volumenDescarga.ToString()
                        , string.IsNullOrEmpty(item.remision) ? string.Empty : item.remision.ToString()
                        , item.producto.descripcion
                        , item.zonaSelect.descripcion);
                }

                i++;
            }

            body += "</tbody>";
            body += "</table></body></html>";
            mensajeCorreo.Body = body;

            mensajeCorreo.IsBodyHtml = true;
            mensajeCorreo.Priority = MailPriority.Normal;

            string output = null;
            try
            {
                smtp.Send(mensajeCorreo);
                output = "Corre electrónico fue enviado satisfactoriamente.";
            }
            catch (Exception ex)
            {
                output = "Error enviando correo electrónico: " + ex.Message;
            }
            finally
            {
                mensajeCorreo.Dispose();
                smtp.Dispose();
            }
            return output;
            //}
            //else
            //{
            //    return resp.mensaje;
            //}            
        }

        public string envioSolicitudAnticipo(Viaje viaje, List<string> ListaRutasArchivo)
        {
            var resp = new ConfiguracionEnvioCorreoSvc().getCorreosByProceso(enumProcesosEnvioCorreo.FORMATO_ANTICIPOS);
            if (resp.typeResult == ResultTypes.success)
            {

                MailMessage mensajeCorreo = new MailMessage();
                SmtpClient smtp = new Util().getSmtpClient(); //new SmtpClient("smtp.gmail.com", 587);

                var path = System.Windows.Forms.Application.StartupPath;
                string archivo = path + @"\config.ini";

                //var correos = (new Util().getCorreoCSI()).Split(',');
                //foreach (var item in correos)
                //{
                //    mensajeCorreo.To.Add(new MailAddress(item)); //para quien es el correo
                //}

                List<ConfiguracionEnvioCorreo> listaCorreos = resp.result as List<ConfiguracionEnvioCorreo>;
                foreach (var dirCorreo in listaCorreos.Select(s => s.direccionCorreo))
                {
                    mensajeCorreo.To.Add(dirCorreo);

                }
                mensajeCorreo.From = new MailAddress(new Util().Read("CONFIGURACION_CORREO", "USUARIO", archivo));
                mensajeCorreo.Subject = "SOLICITUD DE ANTICIPOS VIAJE " + viaje.NumGuiaId.ToString();
                string body = string.Format("SE ADJUNTA DOCUMENTO DE SOLICITUD DE ANTICIPO DEL VIAJE {0} OPERADOR {1}", viaje.NumGuiaId.ToString(), viaje.operador.nombre);

                mensajeCorreo.Body = body;

                mensajeCorreo.IsBodyHtml = false;
                mensajeCorreo.Priority = MailPriority.Normal;

                foreach (var item in ListaRutasArchivo)
                {
                    mensajeCorreo.Attachments.Add(new Attachment(@item));
                }

                string output = null;
                try
                {
                    smtp.Send(mensajeCorreo);
                    output = "Corre electrónico fue enviado satisfactoriamente.";
                }
                catch (Exception ex)
                {
                    output = "Error enviando correo electrónico: " + ex.Message;
                }
                finally
                {
                    mensajeCorreo.Dispose();
                    smtp.Dispose();
                }
                return output;
            }
            else
            {
                return resp.mensaje;
            }
        }
        public string envioCorreoViajeFalso(Viaje viaje, List<string> ListaRutasArchivo)
        {
            var resp = new ConfiguracionEnvioCorreoSvc().getCorreosByProceso(enumProcesosEnvioCorreo.VIAJE_EN_FALSO);
            if (resp.typeResult == ResultTypes.success)
            {

                MailMessage mensajeCorreo = new MailMessage();
                SmtpClient smtp = new Util().getSmtpClient(); //new SmtpClient("smtp.gmail.com", 587);

                var path = System.Windows.Forms.Application.StartupPath;
                string archivo = path + @"\config.ini";

                //var correos = (new Util().getCorreoCSI()).Split(',');
                //foreach (var item in correos)
                //{
                //    mensajeCorreo.To.Add(new MailAddress(item)); //para quien es el correo
                //}

                List<ConfiguracionEnvioCorreo> listaCorreos = resp.result as List<ConfiguracionEnvioCorreo>;
                foreach (var dirCorreo in listaCorreos.Select(s => s.direccionCorreo))
                {
                    mensajeCorreo.To.Add(dirCorreo);

                }
                mensajeCorreo.From = new MailAddress(new Util().Read("CONFIGURACION_CORREO", "USUARIO", archivo));
                mensajeCorreo.Subject = "VIAJE EN FALSO " + viaje.NumGuiaId.ToString();
                string body = string.Format("SE ADJUNTA DOCUMENTO DE VIAJE EN FALSO {0} {1}", viaje.NumGuiaId.ToString(), viaje.cliente.nombre);

                mensajeCorreo.Body = body;

                mensajeCorreo.IsBodyHtml = false;
                mensajeCorreo.Priority = MailPriority.Normal;

                foreach (var item in ListaRutasArchivo)
                {
                    mensajeCorreo.Attachments.Add(new Attachment(@item));
                }

                string output = null;
                try
                {
                    smtp.Send(mensajeCorreo);
                    output = "Corre electrónico fue enviado satisfactoriamente.";
                }
                catch (Exception ex)
                {
                    output = "Error enviando correo electrónico: " + ex.Message;
                }
                finally
                {
                    mensajeCorreo.Dispose();
                    smtp.Dispose();
                }
                return output;
            }
            else
            {
                return resp.mensaje;
            }
        }

        public string envioNotificacionGranjaNueva(string Granja, string Usuario, string cliente)
        {
            MailMessage mensajeCorreo = new MailMessage();
            SmtpClient smtp = new Util().getSmtpClient(); //new SmtpClient("smtp.gmail.com", 587);
            var path = System.Windows.Forms.Application.StartupPath;
            string archivo = path + @"\config.ini";

            var destino = new Util().Read("CORREO_CSI", "CORREO", archivo);
            mensajeCorreo.To.Add(new MailAddress("angelica.garrido@fleterapegaso.com")); //para quien es el correo
            mensajeCorreo.To.Add(new MailAddress("marquizuri.may@fleterapegaso.com")); //para quien es el correo
            mensajeCorreo.To.Add(new MailAddress("gil.can.can@gmail.com")); //para quien es el correo
            mensajeCorreo.From = new MailAddress(new Util().Read("CONFIGURACION_CORREO", "USUARIO", archivo));
            mensajeCorreo.Subject = "AVISO DE GRANJA NUEVA";
            mensajeCorreo.Body = string.Format("Se creo la nueva granja {0} para el cliente {1}", Granja, cliente); ;
            string output = null;
            try
            {
                smtp.Send(mensajeCorreo);
                output = "Corre electrónico fue enviado satisfactoriamente.";
            }
            catch (Exception e)
            {
                output = "Error enviando correo electrónico: " + e.Message;
            }
            finally
            {
                mensajeCorreo.Dispose();
                smtp.Dispose();
            }
            return output;
        }
    }
}
