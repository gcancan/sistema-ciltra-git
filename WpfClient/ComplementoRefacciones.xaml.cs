﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
using ProcesosComercial;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para ComplementoRefacciones.xaml
    /// </summary>
    public partial class ComplementoRefacciones : ViewBase
    {
        public ComplementoRefacciones()
        {
            InitializeComponent();
        }
        private Parametros parametros;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            var svcParam = new ParametrosSvc().getParametrosSistema();
            if (svcParam.typeResult == ResultTypes.success)
            {
                parametros = svcParam.result as Parametros;
            }
            else
            {
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                IsEnabled = false;                
                imprimir(svcParam);
                return;
            }

            ctlUnidadTrans.DataContextChanged += CtlUnidadTrans_DataContextChanged;
            ctlUnidadTrans.loaded(etipoUniadBusqueda.TODAS, null);
            txtFolio.txtEntero.KeyDown += TxtEntero_KeyDown;
            txtPersonaSolicita.loaded(eTipoBusquedaPersonal.TODOS, null);
            nuevo();
        }

        private void TxtEntero_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                buscarOrdenTraabajo(txtFolio.valor);
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.buscar | ToolbarCommands.cerrar);
            ctlUnidadTrans.limpiar();
            lblMensaje.Visibility = Visibility.Hidden;
            gBoxOrdenes.IsEnabled = false;
            limpiar();
        }

        private void CtlUnidadTrans_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (ctlUnidadTrans.unidadTransporte != null)
                {
                    //var resp = new OperacionSvc().getUltimaOrdenServDiagByIdUnidadTrans(ctlUnidadTrans.unidadTransporte.clave);
                    //if (resp.typeResult == ResultTypes.success)
                    //{
                    //    TipoOrdenServicio orderServicio = resp.result as TipoOrdenServicio;
                    //    mapOrdenServicio(orderServicio);
                    //}
                    //else
                    //{
                    //    imprimir(resp);
                    //    limpiar();
                    //}
                }
                else
                {
                    limpiar();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void limpiar()
        {
            ctlUnidadTrans.limpiar();
            mapOrdenServicio(null);
            mapPersonal(null);
            mapListaInsumos(new List<ProductosCOM>());
            stpInsumos.Children.Clear();
            txtPersonaSolicita.limpiar();
            lblMensaje.Visibility = Visibility.Hidden;
            gBoxOrdenes.IsEnabled = false;
            lvlOrdenesInsumos.Items.Clear();
        }
        private void mapOrdenServicio(TipoOrdenServicio orden)
        {
            if (orden != null)
            {
                txtFolio.valor = orden.idOrdenSer;
                txtFolio.Tag = orden;
                txtFolio.IsEnabled = false;
                //txtUnidad.Text = orden.unidadTrans.clave;
                ctlUnidadTrans.unidadTransporte = orden.unidadTrans;
                txtTipoOrden.Text = orden.TipoServicio;
                txtEstatus.Text = orden.estatus;
                txtChofer.Text = orden.chofer;
                //txtResponsable.Text = MasterOrdServ
                lvlActividades.Items.Clear();
                var resp = new MasterOrdServSvc().getTrabajosByOrdenSer(orden.idOrdenSer);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<OrdenTaller> listaOrdenes = resp.result as List<OrdenTaller>;
                    //bool isPRE = true;
                    foreach (var item in listaOrdenes)
                    {
                        ListViewItem lvl = new ListViewItem();
                        lvl.Content = item;
                        lvl.Selected += Lvl_Selected;

                        if (item.estatus.ToUpper() == "PRE" || item.estatus.ToUpper() == "REC" || item.estatus.ToUpper() == "ASIG" || item.estatus.ToUpper() == "ENT" || item.estatus.ToUpper() == "DIAG")
                        {
                            //if (item.actividad == null)
                            //{
                                ContextMenu menu = new ContextMenu();
                                MenuItem menuCandelar = new MenuItem { Header = "Cancelar" };
                                menuCandelar.Tag = item;
                                menuCandelar.Click += MenuCandelar_Click;
                                menu.Items.Add(menuCandelar);

                                MenuItem menuCambiar = new MenuItem { Tag = item, Header = "Cambiar" };
                                menuCambiar.Click += MenuCambiar_Click;
                                menu.Items.Add(menuCambiar);
                                lvl.ContextMenu = menu;
                            //} 
                        }
                        lvl.FontWeight = FontWeights.Medium;
                        lvlActividades.Items.Add(lvl);
                    }

                    //btnCancelar.IsEnabled = isPRE;
                    //lvlActividades.ItemsSource = listaOrdenes;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            else
            {
                txtFolio.limpiar();
                txtFolio.Tag = null;
                txtFolio.IsEnabled = true;
                //txtUnidad.Clear();
                txtTipoOrden.Clear();
                txtChofer.Clear();
                txtEstatus.Clear();
                lvlActividades.Items.Clear();
                IsEnabled = true;
            }
        }
        private void MenuCandelar_Click(object sender, RoutedEventArgs e)
        {
            var menuItem = sender as MenuItem;
            var orden = menuItem.Tag as OrdenTaller;
            var resp = new cancelarOrden(orden, mainWindow.inicio._usuario.nombreUsuario).cancelarOrdenTrabajo();
            if (resp.Value)
            {
                buscarOrdenTraabajo(txtFolio.valor);
            }
        }
        private void MenuCambiar_Click(object sender, RoutedEventArgs e)
        {
            var menuItem = sender as MenuItem;
            var orden = menuItem.Tag as OrdenTaller;

            List<OrdenTaller> listaOrdenes = new List<OrdenTaller>();
            foreach (ListViewItem item in lvlActividades.Items)
            {
                listaOrdenes.Add((OrdenTaller)item.Content);
            }

            agregarOrdenLlantera agregarOrd = new agregarOrdenLlantera(orden, txtFolio.Tag as TipoOrdenServicio, (txtFolio.Tag as TipoOrdenServicio).TipoServicio == "LLANTAS", listaOrdenes, true);
            if (agregarOrd.ShowDialog().Value)
            {
                buscarOrdenTraabajo(txtFolio.valor);
            }
            lvlActividades.SelectedItem = null;
            lblMensaje.Visibility = Visibility.Hidden;
        }
        private void buscarOrdenTraabajo(int valor)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new OperacionSvc().getOrdenServById(valor);
                if (resp.typeResult == ResultTypes.success)
                {
                    TipoOrdenServicio orden = resp.result as TipoOrdenServicio;
                    if (orden.estatus.ToUpper() == "ASIG")
                    {
                        mapOrdenServicio(orden);
                    }
                    else
                    {
                        imprimir(new OperationResult { valor = 2, mensaje = "La orden de servicio no esta asignada" });
                    }
                    
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        ListViewItem index = null;
        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                lvlInsumos.Items.Clear();
                ListViewItem lvl = sender as ListViewItem;
                OrdenTaller orden = lvl.Content as OrdenTaller;
                mapPersonal(orden);
                var resp = new ProductosCOMSvc().getInsumosCOMbyIdOrdenActividad(orden.idOrdenActividad);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapListaInsumos(resp.result as List<ProductosCOM>);
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    imprimir(resp);
                }
                if (orden.actividad == null)
                {
                    lblMensaje.Visibility = Visibility.Visible;
                    gBoxOrdenes.IsEnabled = false;
                }
                else
                {
                    lblMensaje.Visibility = Visibility.Hidden;
                    gBoxOrdenes.IsEnabled = true;
                }
                index = lvl;
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapListaInsumos(List<ProductosCOM> list)
        {
            lvlInsumos.Items.Clear();
            foreach (ProductosCOM item in list)
            {
                ListViewItem lvl = new ListViewItem() { Content = item };
                lvlInsumos.Items.Add(lvl);
            }
        }

        private void mapPersonal(OrdenTaller orden)
        {
            if (orden != null)
            {
                txtResponsable.Text = orden.personal1 == null ? string.Empty : orden.personal1.nombre;
                txtAuxiliar1.Text = orden.personal2 == null ? string.Empty : orden.personal2.nombre;
                txtAuxiliar2.Text = orden.personal3 == null ? string.Empty : orden.personal3.nombre;
                txtPersonaSolicita.personal = orden.personal1;
            }
            else
            {
                txtPersonaSolicita.limpiar();
                txtResponsable.Clear();
                txtAuxiliar1.Clear();
                txtAuxiliar2.Clear();
            }
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            buscarProducto();
        }

        void buscarProducto()
        {
            var lista = new selectProductosCOM(txtInsumo.Text.Trim()).consultasTaller(AreaTrabajo.TOTAS);
            if (lista != null)
            {
                OrdenTaller orden = null;
                orden = ((ListViewItem)lvlActividades.SelectedItem).Content as OrdenTaller;
                if (orden != null && lista.Count > 0)
                {
                    foreach (ProductosCOM producto in lista)
                    {
                        ListViewItem lvl = new ListViewItem();
                        lvl.Content = new RelOrdenesInsumos { orden = orden, productosCOM = producto };
                        lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                        lvlOrdenesInsumos.Items.Add(lvl);
                    }
                    stpInsumos.Children.Clear();
                }
            }
            txtInsumo.Clear();
        }

        private void añadirProducto(ProductosCOM insumo)
        {
            try
            {
                StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal, Tag = insumo };
                Label lblProducto = new Label { Content = stpContent , Tag = insumo};
                #region contenido
                Label lblNombreProducto = new Label
                {
                    Content = insumo.nombre,
                    Width = 350,
                    Tag = insumo,
                    Margin = new Thickness(2)
                };
                stpContent.Children.Add(lblNombreProducto);

                controlDecimal ctrDecimal = new controlDecimal { Tag = insumo, valor = insumo.cantidad };
                ctrDecimal.txtDecimal.Tag = insumo;
                ctrDecimal.txtDecimal.TextChanged += TxtDecimal_TextChanged;
                //ctrDecimal.DataContextChanged += CtrDecimal_DataContextChanged;
                stpContent.Children.Add(ctrDecimal);

                Button btnQuitar = new Button
                {
                    Content = "Quitar",
                    Tag = insumo,
                    Foreground = Brushes.White,
                    Background = Brushes.Orange,
                    Margin = new Thickness(2),
                    Width = 100
                };
                btnQuitar.Click += BtnQuitar_Click;
                stpContent.Children.Add(btnQuitar);
                #endregion

                
                stpInsumos.Children.Add(lblProducto);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void CtrDecimal_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                controlDecimal ctrl = sender as controlDecimal;
                ProductosCOM producto = ctrl.Tag as ProductosCOM;
                List<Label> listLBL = stpInsumos.Children.Cast<Label>().ToList();
                //int i = 0;
                foreach (Label item in listLBL)
                {
                    ProductosCOM pro = (item.Tag as ProductosCOM);
                    if (producto == pro)
                    {
                        pro.cantidad = ctrl.valor;
                    }
                    //i++;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void TxtDecimal_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                TextBox ctrl = sender as TextBox;
                ProductosCOM producto = ctrl.Tag as ProductosCOM;
                List<Label> listLBL = stpInsumos.Children.Cast<Label>().ToList();
                //int i = 0;
                foreach (Label item in listLBL)
                {
                    ProductosCOM pro = (item.Tag as ProductosCOM);
                    if (producto == pro)
                    {
                        pro.cantidad = Convert.ToDecimal(ctrl.Text.Trim());
                    }
                    //i++;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void BtnQuitar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                ProductosCOM producto = btn.Tag as ProductosCOM;
                List<Label> listLBL = stpInsumos.Children.Cast<Label>().ToList();
                int i = 0;
                foreach (Label item in listLBL)
                {
                    if (producto == (item.Tag as ProductosCOM))
                    {
                        stpInsumos.Children.Remove(stpInsumos.Children[i]);
                        return;
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        public override OperationResult guardar()
        {
            try
            {
                OrdenTaller orden = null;
                Personal personaSolicita = null;

                List<OrdenTaller> listOrdenes = armarListaOrdenes();
                if (txtPersonaSolicita.personal == null)
                {
                    txtPersonaSolicita.txtPersonal.Focus();
                    return new OperationResult { valor = 2, mensaje = "No Se especifico quien solicita" };
                }
                personaSolicita = txtPersonaSolicita.personal;
                //if (lvlActividades.SelectedItem == null)
                //{
                //    return new OperationResult { valor = 2, mensaje = "Seleccionar una actividad para continuar" };
                //}
                //else
                //{
                //    orden = ((ListViewItem)lvlActividades.SelectedItem).Content as OrdenTaller;
                //    
                //    bool r = validarPersonal(orden, personaSolicita);
                //    if (!r)
                //    {
                //        return new OperationResult { valor = 2, mensaje = "La persona que solicita, no esta dentro la orden de trabajo" };
                //    }
                //}
                OperationResult respNew = new OperationResult();
                foreach (OrdenTaller _orden in listOrdenes)
                {
                    ProcesosForm proceso = new ProcesosForm(parametros);
                    respNew = proceso.realizarSalidaEntradaAlmacen(_orden, _orden.listaProductosCOM, personaSolicita, mainWindow.inicio._usuario.nombreUsuario);
                    proceso.Dispose();
                    if (respNew.typeResult != ResultTypes.success)
                    {
                        return respNew;
                    }
                }
                
                if (respNew.typeResult == ResultTypes.success)
                {
                    int folio = txtFolio.valor;
                    nuevo();
                    buscarOrdenTraabajo(folio);
                    mainWindow.habilitar = true;
                }
                return respNew;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }       

        private bool validarPersonal(OrdenTaller orden, Personal personaSolicita)
        {
            
            if (orden.personal1.clave == personaSolicita.clave)
            {
                return true;
            }
            if (orden.personal2 != null)
            {
                if (orden.personal2.clave == personaSolicita.clave)
                {
                    return true;
                }
            }
            if (orden.personal3 != null)
            {
                if (orden.personal3.clave == personaSolicita.clave)
                {
                    return true;
                }
            }
            return false;
        }

        private List<ProductosCOM> getListaProductos()
        {
            List<ProductosCOM> lista = new List<ProductosCOM>();
            foreach (Label item in stpInsumos.Children.Cast<Label>().ToList())
            {
                lista.Add(item.Tag as ProductosCOM);
            }
            return lista;
        }

        private void btnCambiar_Click(object sender, RoutedEventArgs e)
        {
            txtPersonaSolicita.limpiar();
        }

        private void txtInsumo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                buscarProducto();
            }
        }

        private class RelOrdenesInsumos
        {
            public OrdenTaller orden { get; set; }
            public ProductosCOM productosCOM { get; set; }
        }
            
        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            OrdenTaller orden = null;
            orden = ((ListViewItem)lvlActividades.SelectedItem).Content as OrdenTaller;
            List<ProductosCOM> listaProductos = new List<ProductosCOM>();
            listaProductos = getListaProductos();
            if (orden != null && listaProductos.Count > 0)
            {
                foreach (ProductosCOM producto in listaProductos)
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = new RelOrdenesInsumos { orden = orden, productosCOM = producto };
                    lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                    lvlOrdenesInsumos.Items.Add(lvl);
                }
                stpInsumos.Children.Clear();
            }            
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            lvlOrdenesInsumos.Items.Remove(lvlOrdenesInsumos.SelectedItem);
        }
        private List<OrdenTaller> armarListaOrdenes()
        {
            List<OrdenTaller> listaOrdenes = new List<OrdenTaller>();
            foreach (ListViewItem lvl in lvlOrdenesInsumos.Items)
            {
                RelOrdenesInsumos relOrdenesInsumos = lvl.Content as RelOrdenesInsumos;
                int i = 0;
                bool existe = false;
                foreach (OrdenTaller orden in listaOrdenes)
                {
                    if (orden.idOrdenTrabajo == relOrdenesInsumos.orden.idOrdenTrabajo)
                    {
                        existe = true;
                        break;
                    }
                    i++;
                }
                if (existe)
                {
                    listaOrdenes[i].listaProductosCOM.Add(relOrdenesInsumos.productosCOM);
                }
                else
                {
                    OrdenTaller newOrden = relOrdenesInsumos.orden;
                    newOrden.listaProductosCOM = new List<ProductosCOM>();
                    newOrden.listaProductosCOM.Add(relOrdenesInsumos.productosCOM);
                    listaOrdenes.Add(newOrden);
                }
            }
            return listaOrdenes;
        }

        private void btnNewActividad_Click(object sender, RoutedEventArgs e)
        {
            List<OrdenTaller> listaOrdenes = new List<OrdenTaller>();
            foreach (ListViewItem item in lvlActividades.Items)
            {
                listaOrdenes.Add((OrdenTaller)item.Content);
            }
            if (txtFolio.Tag == null)
            {
                return;
            }
            agregarOrdenLlantera agregarOrd = new agregarOrdenLlantera(txtFolio.Tag as TipoOrdenServicio, ((txtFolio.Tag as TipoOrdenServicio).TipoServicio == "LLANTAS"), listaOrdenes, true, true);
            if (agregarOrd.ShowDialog().Value)
            {
                buscarOrdenTraabajo(txtFolio.valor);
            }
        }
        public override void buscar()
        {
            selectOrdenServicio sel = new selectOrdenServicio();
            var orden = sel.getOrdenServicio();
            if (orden != null)
            {
                buscarOrdenTraabajo(orden.idOrdenSer);
            }
            //mapOrdenServicio(orden);
        }
    }
}
