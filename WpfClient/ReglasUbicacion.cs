﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
namespace WpfClient
{
    public static class ReglasUbicacion
    {
        public static bool validar(EstatusUbicacion lugarEntrada, UnidadTransporte unidadTransporte)
        {

            if (unidadTransporte.ubicacion == EstatusUbicacion.DESCONOCIDO)
            {
                return true;
            }
            switch (lugarEntrada)
            {
                case EstatusUbicacion.RUTA:
                    if (unidadTransporte.ubicacion == EstatusUbicacion.BASE)
                    {
                        return true;
                    }
                    ImprimirMensaje.imprimir(new OperationResult
                    {
                        valor = 2,
                        mensaje = string.Format("Esta operación no se puede realizar por que la unidad {0} esta en {1}"
                        , unidadTransporte.clave, unidadTransporte.ubicacion)
                    });
                    return false;
                case EstatusUbicacion.BASE:
                    switch (unidadTransporte.ubicacion)
                    {
                        case EstatusUbicacion.RUTA:
                            return true;
                        case EstatusUbicacion.TALLER:
                            return true;
                        case EstatusUbicacion.LAVADERO:
                            return true;
                        case EstatusUbicacion.LLANTAS:
                            return true;
                        case EstatusUbicacion.TALLER_EX:
                            return true;
                        case EstatusUbicacion.GASOLINERA:
                            return true;
                        case EstatusUbicacion.DESCONOCIDO:
                            return true;
                        default:
                            ImprimirMensaje.imprimir(new OperationResult
                            {
                                valor = 2,
                                mensaje = string.Format("Esta operación no se puede realizar por que la unidad {0} esta en {1}"
                        , unidadTransporte.clave, unidadTransporte.ubicacion)
                            });
                            return false;
                    }
                case EstatusUbicacion.TALLER:
                    switch (unidadTransporte.ubicacion)
                    {
                        case EstatusUbicacion.BASE:
                            return true;
                        case EstatusUbicacion.DESCONOCIDO:
                            return true;
                        default:
                            ImprimirMensaje.imprimir(new OperationResult
                            {
                                valor = 2,
                                mensaje = string.Format("Esta operación no se puede realizar por que la unidad {0} esta en {1}"
                        , unidadTransporte.clave, unidadTransporte.ubicacion)
                            });
                            return false;
                    }
                case EstatusUbicacion.LAVADERO:
                    switch (unidadTransporte.ubicacion)
                    {
                        case EstatusUbicacion.BASE:
                            return true;
                        case EstatusUbicacion.DESCONOCIDO:
                            return true;
                        default:
                            ImprimirMensaje.imprimir(new OperationResult
                            {
                                valor = 2,
                                mensaje = string.Format("Esta operación no se puede realizar por que la unidad {0} esta en {1}"
                        , unidadTransporte.clave, unidadTransporte.ubicacion)
                            });
                            return false;
                    }
                case EstatusUbicacion.LLANTAS:
                    switch (unidadTransporte.ubicacion)
                    {
                        case EstatusUbicacion.BASE:
                            return true;
                        case EstatusUbicacion.DESCONOCIDO:
                            return true;
                        default:
                            ImprimirMensaje.imprimir(new OperationResult
                            {
                                valor = 2,
                                mensaje = string.Format("Esta operación no se puede realizar por que la unidad {0} esta en {1}"
                        , unidadTransporte.clave, unidadTransporte.ubicacion)
                            });
                            return false;
                    }
                //case EstatusUbicacion.TALLER_EX:
                //    switch (ubicacionActual)
                //    {
                //        case EstatusUbicacion.PATIOS:
                //            return true;
                //        case EstatusUbicacion.DESCONOCIDO:
                //            return true;
                //        default:
                //            return false;
                //    }
                case EstatusUbicacion.GASOLINERA:
                    switch (unidadTransporte.ubicacion)
                    {
                        case EstatusUbicacion.BASE:
                            return true;
                        case EstatusUbicacion.DESCONOCIDO:
                            return true;
                        default:
                            ImprimirMensaje.imprimir(new OperationResult
                            {
                                valor = 2,
                                mensaje = string.Format("Esta operación no se puede realizar por que la unidad {0} esta en {1}"
                        , unidadTransporte.clave, unidadTransporte.ubicacion)
                            });
                            return false;
                    }
                default:
                    return false;
            }
        }
    }
}
