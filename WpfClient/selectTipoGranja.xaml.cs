﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectTipoGranja.xaml
    /// </summary>
    public partial class selectTipoGranja : MetroWindow
    {
        public selectTipoGranja()
        {
            InitializeComponent();
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {

        }
        public TipoGranja getTipoGranja()
        {
            try
            {
                buscarAllTipoGranja();
                bool? resp = ShowDialog();
                if (resp.Value && lvlTiposGranja.SelectedItem != null)
                {
                    return (lvlTiposGranja.SelectedItem as ListViewItem).Content as TipoGranja;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        void buscarAllTipoGranja()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new TipoGranjaSvc().getAllTiposGranjas();
                if (resp.typeResult == ResultTypes.success)
                {
                    llenarLista(resp.result as List<TipoGranja>);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void llenarLista(List<TipoGranja> listaTipoGranja)
        {
            try
            {
                lvlTiposGranja.ItemsSource = null;
                List<ListViewItem> list = new List<ListViewItem>();
                foreach (TipoGranja tipo in listaTipoGranja)
                {
                    ListViewItem item = new ListViewItem
                    {
                        Content = tipo
                    };
                    item.MouseDoubleClick += Item_MouseDoubleClick;
                    list.Add(item);
                }
                this.lvlTiposGranja.ItemsSource = list;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlTiposGranja.ItemsSource);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtBuscador.Text) ||
            ((((TipoGranja)(item as ListViewItem).Content).tipoGranja.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void Item_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }
        private void TxtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(this.lvlTiposGranja.ItemsSource).Refresh();
        }
    }
}
