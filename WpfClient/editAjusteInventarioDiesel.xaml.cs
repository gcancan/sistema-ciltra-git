﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editAjusteInventarioDiesel.xaml
    /// </summary>
    public partial class editAjusteInventarioDiesel : ViewBase
    {
        public editAjusteInventarioDiesel()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
                ctrZonaOperativa.cargarControl(mainWindow.zonaOperativa, mainWindow.usuario.idUsuario);
                var resp = new PrivilegioSvc().consultarPrivilegio("ISADMIN", mainWindow.usuario.idUsuario);
                bool isAdmin = resp.typeResult == ResultTypes.success;
                ctrEmpresa.loaded((isAdmin ? null : mainWindow.empresa), isAdmin);
                nuevo();
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ctrZonaOperativa.zonaOperativa != null)
            {
                buscarTanqueCombustible(ctrZonaOperativa.zonaOperativa);
            }
            else
            {
                buscarTanqueCombustible(null);
            }
        }
        private void buscarTanqueCombustible(ZonaOperativa zonaOperativa)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new TanqueCombustibleSvc().getTanqueByZonaOperativa(zonaOperativa);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapTanqueCombustible(resp.result as TanqueCombustible);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    mapTanqueCombustible(null);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void mapTanqueCombustible(TanqueCombustible tanqueCombustible, bool mapDet = true)
        {
            if (tanqueCombustible != null)
            {
                txtTanque.Text = tanqueCombustible.nombre;
                txtTanque.Tag = tanqueCombustible;
            }
            else
            {
                txtTanque.Tag = null;
                txtTanque.Clear();
            }
        }
        public override void nuevo()
        {
            base.nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            mapForm(null);
        }

        private void mapForm(KardexCombustible inventario)
        {
            if (inventario != null)
            {
                ctrClave.Tag = inventario;
                ctrClave.valor = inventario.idInventarioDiesel;
                ctrCantidad.valor = inventario.cantidad;
                rbnEntrada.IsChecked = inventario.tipoMovimiento == eTipoMovimiento.ENTRADA;
                rbnSalida.IsChecked = inventario.tipoMovimiento == eTipoMovimiento.SALIDA;
                txtObservaciones.Text = inventario.observaciones;

            }
            else
            {
                ctrClave.Tag = null;
                ctrClave.valor = 0;
                ctrEmpresa.cbxEmpresa.SelectedItem = null;
                ctrCantidad.valor = 0;
                rbnEntrada.IsChecked = true;
                txtObservaciones.Clear();
            }
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                KardexCombustible kardexCombustible = mapForm();
                if (kardexCombustible != null)
                {
                    var resp = new KardexCombustibleSvc().savekardexCombustible(ref kardexCombustible);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mapForm(kardexCombustible);
                        mainWindow.enableToolBarCommands(ToolbarCommands.nuevo|ToolbarCommands.cerrar);
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult(ResultTypes.error, "OCURRIO ERROR AL LEER LA INFORMACIÓN DE LA PANTALLA");
                }                
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private KardexCombustible mapForm()
        {
            try
            {
                return new KardexCombustible
                {
                    idInventarioDiesel = 0,
                    tanque = txtTanque.Tag as TanqueCombustible,
                    empresa = ctrEmpresa.empresaSelected,
                    idOperacion = null,
                    personal = mainWindow.usuario.personal,
                    unidadTransporte = null,
                    tipoOperacion = TipoOperacionDiesel.AJUSTE,
                    tipoMovimiento = rbnEntrada.IsChecked.Value ? eTipoMovimiento.ENTRADA : eTipoMovimiento.SALIDA,
                    cantidad = ctrCantidad.valor,
                    observaciones = txtObservaciones.Text,
                    cantidadInicial = 0,
                    cantidadFinal = 0                    
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
