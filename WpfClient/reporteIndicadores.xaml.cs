﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para reporteIndicadores.xaml
    /// </summary>
    public partial class reporteIndicadores : ViewBase
    {
        public reporteIndicadores()
        {
            InitializeComponent();
        }
        Usuario usuario = null;
        Empresa empresa = null;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.usuario = mainWindow.usuario;

            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresa.loaded(usuario);
            for (int i = (DateTime.Now.Year + 1); i > 2016; i--)
            {
                cbxAño.Items.Add(i);
            }
            cbxAño.SelectedItem = cbxAño.Items.Cast<int>().ToList().Find(s => s == DateTime.Now.Year);
            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
        }
        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            empresa = ctrEmpresa.empresaSelected;

            cargarZonasOperativas();
        }
        List<ZonaOperativa> listaZonaOpertiva = new List<ZonaOperativa>();
        void cargarZonasOperativas()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new ZonaOperativaSvc().getAllZonasOperativas();
                if (resp.typeResult == ResultTypes.success)
                {
                    listaZonaOpertiva = resp.result as List<ZonaOperativa>;
                    cbxZonasOperativas.ItemsSource = listaZonaOpertiva;
                }
                else
                {
                    cbxZonasOperativas.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        List<OperacionFletera> listaOperaciones = new List<OperacionFletera>();
        private void cbxZonasOperativas_ItemSelectionChanged(object sender, Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventArgs e)
        {
            try
            {
                cbxOperacion.ItemsSource = null;
                chcTodosOperacion.IsChecked = false;
                var listaId = (from s in this.cbxZonasOperativas.SelectedItems.Cast<ZonaOperativa>().ToList<ZonaOperativa>() select s.idZonaOperativa.ToString()).ToList<string>();
                if (listaId != null)
                {
                    var resp = new ClienteSvc().getClientesByZonasOperativas(listaId, empresa.clave);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listaOperaciones = resp.result as List<OperacionFletera>;
                        cbxOperacion.ItemsSource = listaOperaciones;
                    }
                    else if (resp.typeResult != ResultTypes.recordNotFound)
                    {
                        imprimir(resp);
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void chcTodosZonaOperativa_Unchecked(object sender, RoutedEventArgs e)
        {
            CheckBox chc = sender as CheckBox;
            switch (chc.IsChecked.Value)
            {
                case true:
                    cbxZonasOperativas.SelectAll();
                    break;
                case false:
                    cbxZonasOperativas.SelectedItems.Clear();
                    break;
            }
        }

        private void chcTodosOperacion_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox chc = sender as CheckBox;
            switch (chc.IsChecked.Value)
            {
                case true:
                    cbxOperacion.SelectAll();
                    break;
                case false:
                    cbxOperacion.SelectedItems.Clear();
                    break;
            }
        }
        List<ReporteIndicadorViajes> listaIndicadorViajes = new List<ReporteIndicadorViajes>();
        List<GrupoIndicadoresViajes> listaGrupoIndicadoresViajes = new List<GrupoIndicadoresViajes>();

        List<ReporteIndicadorCombustible> listaIndicadoresCombustible = new List<ReporteIndicadorCombustible>();
        List<GrupoIndicadoresCombustible> listaGrupoIndicadoresCombustible = new List<GrupoIndicadoresCombustible>();

        List<Presupuesto> listaPresupuestos = new List<Presupuesto>();
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;

                if (cbxOperacion.SelectedItem != null)
                {
                    List<string> listaZonas = cbxZonasOperativas.SelectedItems.Cast<ZonaOperativa>().ToList().Select(s => s.idZonaOperativa.ToString()).ToList();
                    List<string> listaIdCliente = cbxOperacion.SelectedItems.Cast<OperacionFletera>().ToList().Select(s => s.cliente.clave.ToString()).ToList();
                    int año = (int)cbxAño.SelectedItem;

                    var respIndicadoresViajes = new PresupuestoSvc().getAllIndicadoresViajes(año, listaIdCliente);
                    if (respIndicadoresViajes.typeResult == ResultTypes.success)
                    {
                        this.listaIndicadorViajes = (respIndicadoresViajes.result as List<ReporteIndicadorViajes>).OrderBy(s => s.idCliente).ToList();

                        listaGrupoIndicadoresViajes = (from v in listaIndicadorViajes
                                                       group v by v.idCliente into grupo
                                                       select new GrupoIndicadoresViajes
                                                       {
                                                           idCliente = (from s in grupo select s.idCliente).First(),
                                                           cliente = (from s in grupo select s.cliente).First(),
                                                           listaDetalles = grupo.ToList<ReporteIndicadorViajes>()
                                                       }).ToList();

                        crearControlesToneladasReales(listaGrupoIndicadoresViajes);
                        crearControlesViajesReales(listaGrupoIndicadoresViajes);
                        crearControlesVentasReales(listaGrupoIndicadoresViajes);
                    }
                    else
                    {
                        imprimir(respIndicadoresViajes);
                    }

                    var respCom = new PresupuestoSvc().getAllIndicadoresCombustible(año, listaZonas, listaIdCliente);
                    if (respCom.typeResult == ResultTypes.success)
                    {
                        this.listaIndicadoresCombustible = (respCom.result as List<ReporteIndicadorCombustible>).OrderBy(s => s.idCliente).ToList();

                        listaGrupoIndicadoresCombustible = (from v in listaIndicadoresCombustible
                                                            group v by v.idCliente into grupo
                                                            select new GrupoIndicadoresCombustible
                                                            {
                                                                idCliente = (from s in grupo select s.idCliente).First(),
                                                                cliente = (from s in grupo select s.cliente).First(),
                                                                listaDetalles = grupo.ToList<ReporteIndicadorCombustible>()
                                                            }).ToList();

                        crearControlesKmReales(listaGrupoIndicadoresCombustible);
                        crearControlesLitroReales(listaGrupoIndicadoresCombustible);
                        crearControlesRendimientoReales(listaGrupoIndicadoresCombustible);
                    }

                    var respPresupuestos = new PresupuestoSvc().getPresupuestosByClientes((int)cbxAño.SelectedItem, listaIdCliente);
                    if (respPresupuestos.typeResult == ResultTypes.success)
                    {
                        listaPresupuestos = (respPresupuestos.result as List<Presupuesto>).OrderBy(s => s.cliente.clave).ToList();

                        crearControlesToneladasPresupuesto(listaPresupuestos);
                        crearControlesViajesPresupuesto(listaPresupuestos);
                        crearControlesVentasPresupuesto(listaPresupuestos);

                        crearControlesKmPresupuesto(listaPresupuestos);
                        crearControlesLitrosPresupuesto(listaPresupuestos);
                        crearControlesRendimientoPresupuesto(listaPresupuestos);
                    }
                    else if (respPresupuestos.typeResult != ResultTypes.recordNotFound)
                    {
                        imprimir(respPresupuestos);
                    }



                    crearDiferenciaToneladas();
                    crearDiferenciaViajes();
                    crearDiferenciaVentas();

                    crearDiferenciaKm();
                    crearDiferenciaLitros();
                    crearDiferenciaRendimiento();

                    crearGrupoIndicadoresPrecioKm(listaGrupoIndicadoresViajes);
                    crearControlesPreciosKmReales(listaGrupoIndicadoresViajes);
                    crearControlesPrecioKmPresupuesto(listaPresupuestos);
                    crearDiferenciaPrecioKm();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void crearControlesToneladasReales(List<GrupoIndicadoresViajes> listaIndicadores)
        {
            try
            {
                stpTonReal.Children.Clear();
                foreach (var grupo in listaIndicadores)
                {
                    StackPanel stpPrincipal = new StackPanel { Tag = grupo, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = grupo.cliente, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };

                    foreach (var detalle in grupo.listaDetalles)
                    {
                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label { Content = detalle.desModalidad, Width = 120, FontWeight = FontWeights.Medium, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblEnero = new Label { Content = Convert.ToInt32(detalle.eneroTon).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblFebrero = new Label { Content = Convert.ToInt32(detalle.febreroTon).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMarzo = new Label { Content = Convert.ToInt32(detalle.marzoTon).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAbril = new Label { Content = Convert.ToInt32(detalle.abrilTon).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMayo = new Label { Content = Convert.ToInt32(detalle.mayoTon).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lbljunio = new Label { Content = Convert.ToInt32(detalle.junioTon).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblJulio = new Label { Content = Convert.ToInt32(detalle.julioTon).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAgosto = new Label { Content = Convert.ToInt32(detalle.agostoTon).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblSeptiembre = new Label { Content = Convert.ToInt32(detalle.septiembreTon).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblOctubre = new Label { Content = Convert.ToInt32(detalle.octubreTon).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblNoviembre = new Label { Content = Convert.ToInt32(detalle.noviembreTon).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblDiciembre = new Label { Content = Convert.ToInt32(detalle.diciembreTon).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        Label lblSuma = new Label { Content = Convert.ToInt32(detalle.sumaTotalToneladas).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }
                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.eneroTon)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.febreroTon)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.marzoTon)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.abrilTon)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.mayoTon)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.junioTon)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.julioTon)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.agostoTon)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.septiembreTon)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.octubreTon)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.noviembreTon)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.diciembreTon)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.sumaTotalToneladas)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpTonReal.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumEneroTon)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumFebreroTon)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumMarzoTon)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumAbrilTon)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumMayoTon)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumJunioTon)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumJulioTon)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumAgostoTon)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumSeptiembreTon)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumOctubreTon)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumNoviembreTon)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumDiciembreTon)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumTotalTon)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpTonReal.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void crearControlesToneladasPresupuesto(List<Presupuesto> listaPresupuestos)
        {
            try
            {
                stpTonPresupuesto.Children.Clear();
                foreach (var presupuesto in listaPresupuestos)
                {
                    StackPanel stpPrincipal = new StackPanel { Tag = presupuesto, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = presupuesto.cliente.nombre, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };

                    foreach (var detalle in presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.TONELADAS))
                    {
                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label { Content = detalle.modalidad.descripcion, Width = 120, FontWeight = FontWeights.Medium, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblEnero = new Label { Content = Convert.ToInt32(detalle.enero).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblFebrero = new Label { Content = Convert.ToInt32(detalle.febrero).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMarzo = new Label { Content = Convert.ToInt32(detalle.marzo).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAbril = new Label { Content = Convert.ToInt32(detalle.abril).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMayo = new Label { Content = Convert.ToInt32(detalle.mayo).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lbljunio = new Label { Content = Convert.ToInt32(detalle.junio).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblJulio = new Label { Content = Convert.ToInt32(detalle.julio).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAgosto = new Label { Content = Convert.ToInt32(detalle.agosto).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblSeptiembre = new Label { Content = Convert.ToInt32(detalle.septiembre).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblOctubre = new Label { Content = Convert.ToInt32(detalle.octubre).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblNoviembre = new Label { Content = Convert.ToInt32(detalle.noviembre).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblDiciembre = new Label { Content = Convert.ToInt32(detalle.diciembre).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        Label lblSuma = new Label { Content = Convert.ToInt32(detalle.sumaAnual).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }
                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.TONELADAS).Sum(s => s.enero)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.TONELADAS).Sum(s => s.febrero)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.TONELADAS).Sum(s => s.marzo)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.TONELADAS).Sum(s => s.abril)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.TONELADAS).Sum(s => s.mayo)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.TONELADAS).Sum(s => s.junio)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.TONELADAS).Sum(s => s.julio)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.TONELADAS).Sum(s => s.agosto)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.TONELADAS).Sum(s => s.septiembre)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.TONELADAS).Sum(s => s.octubre)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.TONELADAS).Sum(s => s.noviembre)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.TONELADAS).Sum(s => s.diciembre)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.TONELADAS).Sum(s => s.sumaAnual)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpTonPresupuesto.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                List<DetallePresupuesto> listaDet = new List<DetallePresupuesto>();
                foreach (var item in listaPresupuestos)
                {
                    listaDet.AddRange(item.listaDetalles.FindAll(s => s.indicador == Indicador.TONELADAS));
                }


                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.enero)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.febrero)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.marzo)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.abril)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.mayo)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.junio)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.julio)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.agosto)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.septiembre)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.octubre)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.noviembre)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.diciembre)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.sumaAnual)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpTonPresupuesto.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        void crearDiferenciaToneladas()
        {
            try
            {
                stpTonRealPresupuesto.Children.Clear();
                int _sumEnero = 0, _sumFebrero = 0, _sumMarzo = 0, _sumAbril = 0, _sumMayo = 0, _sumJunio = 0, _sumJulio = 0, _sumAgosto = 0, _sumSeptiembre = 0, _sumOctubre = 0, _sumNoviembre = 0, _sumDiciembre = 0, _sumTotal = 0;
                foreach (var grupo in listaGrupoIndicadoresViajes)
                {
                    Presupuesto presupuesto = listaPresupuestos.Find(s => s.cliente.clave == grupo.idCliente);
                    if (presupuesto == null) continue;

                    StackPanel stpPrincipal = new StackPanel { Tag = grupo, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = grupo.cliente, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };
                    int sumEnero = 0, sumFebrero = 0, sumMarzo = 0, sumAbril = 0, sumMayo = 0, sumJunio = 0, sumJulio = 0, sumAgosto = 0, sumSeptiembre = 0, sumOctubre = 0, sumNoviembre = 0, sumDiciembre = 0, sumTotal = 0;
                    foreach (var detalle in grupo.listaDetalles)
                    {
                        DetallePresupuesto detPres = presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.TONELADAS).Find(s => s.modalidad.subModalidad == detalle.subModalidad);

                        int sumaAnual = 0;

                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label
                        {
                            Content = detalle.desModalidad,
                            Width = 120,
                            FontWeight = FontWeights.Medium,
                            FontSize = 11,
                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0)
                        };
                        int valor = Convert.ToInt32(detalle.eneroTon - (detPres == null ? 0 : detPres.enero));
                        sumaAnual += valor;
                        sumEnero += valor;
                        _sumEnero += valor;
                        Label lblEnero = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.febreroTon - (detPres == null ? 0 : detPres.febrero));
                        sumaAnual += valor;
                        sumFebrero += valor;
                        _sumFebrero += valor;
                        Label lblFebrero = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.marzoTon - (detPres == null ? 0 : detPres.marzo));
                        sumaAnual += valor;
                        sumMarzo += valor;
                        _sumMarzo += valor;
                        Label lblMarzo = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.abrilTon - (detPres == null ? 0 : detPres.abril));
                        sumaAnual += valor;
                        sumAbril += valor;
                        _sumAbril += valor;
                        Label lblAbril = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.mayoTon - (detPres == null ? 0 : detPres.mayo));
                        sumaAnual += valor;
                        sumMayo += valor;
                        _sumMayo += valor;
                        Label lblMayo = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.junioTon - (detPres == null ? 0 : detPres.junio));
                        sumaAnual += valor;
                        sumJunio += valor;
                        _sumJunio += valor;
                        Label lbljunio = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.julioTon - (detPres == null ? 0 : detPres.julio));
                        sumaAnual += valor;
                        sumJulio += valor;
                        _sumJulio += valor;
                        Label lblJulio = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.agostoTon - (detPres == null ? 0 : detPres.agosto));
                        sumaAnual += valor;
                        sumAgosto += valor;
                        _sumAgosto += valor;
                        Label lblAgosto = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.septiembreTon - (detPres == null ? 0 : detPres.septiembre));
                        sumaAnual += valor;
                        sumSeptiembre += valor;
                        _sumSeptiembre += valor;
                        Label lblSeptiembre = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.octubreTon - (detPres == null ? 0 : detPres.octubre));
                        sumaAnual += valor;
                        sumOctubre += valor;
                        _sumOctubre += valor;
                        Label lblOctubre = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.noviembreTon - (detPres == null ? 0 : detPres.noviembre));
                        sumaAnual += valor;
                        sumNoviembre += valor;
                        _sumNoviembre += valor;
                        Label lblNoviembre = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.diciembreTon - (detPres == null ? 0 : detPres.diciembre));
                        sumaAnual += valor;
                        sumDiciembre += valor;
                        _sumDiciembre += valor;
                        Label lblDiciembre = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        sumTotal += sumaAnual;
                        _sumTotal += sumaAnual;
                        Label lblSuma = new Label
                        {
                            Content = sumaAnual.ToString("N0"),
                            Width = 100,
                            FontWeight = FontWeights.Bold,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = sumaAnual < 0 ? Brushes.Red : Brushes.Black
                        };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }
                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumEnero.ToString("N0"), Foreground = (sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumFebrero.ToString("N0"), Foreground = (sumFebrero < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumMarzo.ToString("N0"), Foreground = (sumMarzo < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumAbril.ToString("N0"), Foreground = (sumAbril < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumMayo.ToString("N0"), Foreground = (sumMayo < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumJunio.ToString("N0"), Foreground = (sumJunio < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumJulio.ToString("N0"), Foreground = (sumJulio < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumAgosto.ToString("N0"), Foreground = (sumAgosto < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumSeptiembre.ToString("N0"), Foreground = (sumSeptiembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumOctubre.ToString("N0"), Foreground = (sumOctubre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumNoviembre.ToString("N0"), Foreground = (sumNoviembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumDiciembre.ToString("N0"), Foreground = (sumDiciembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumTotal.ToString("N0"), Foreground = (sumTotal < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpTonRealPresupuesto.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumEnero.ToString("N0"), Foreground = (_sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumFebrero.ToString("N0"), Foreground = (_sumFebrero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumMarzo.ToString("N0"), Foreground = (_sumMarzo < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumAbril.ToString("N0"), Foreground = (_sumAbril < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumMayo.ToString("N0"), Foreground = (_sumMayo < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumJunio.ToString("N0"), Foreground = (_sumJunio < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumJulio.ToString("N0"), Foreground = (_sumJulio < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumAgosto.ToString("N0"), Foreground = (_sumAgosto < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumSeptiembre.ToString("N0"), Foreground = (_sumSeptiembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumOctubre.ToString("N0"), Foreground = (_sumOctubre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumNoviembre.ToString("N0"), Foreground = (_sumNoviembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumDiciembre.ToString("N0"), Foreground = (_sumDiciembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumTotal.ToString("N0"), Foreground = (_sumTotal < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpTonRealPresupuesto.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void crearControlesViajesReales(List<GrupoIndicadoresViajes> listaIndicadores)
        {
            try
            {
                stpViajesReal.Children.Clear();
                foreach (var grupo in listaIndicadores)
                {
                    StackPanel stpPrincipal = new StackPanel { Tag = grupo, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = grupo.cliente, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };

                    foreach (var detalle in grupo.listaDetalles)
                    {
                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label { Content = detalle.desModalidad, Width = 120, FontWeight = FontWeights.Medium, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblEnero = new Label { Content = Convert.ToInt32(detalle.eneroViaje).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblFebrero = new Label { Content = Convert.ToInt32(detalle.febreroViaje).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMarzo = new Label { Content = Convert.ToInt32(detalle.marzoViaje).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAbril = new Label { Content = Convert.ToInt32(detalle.abrilViaje).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMayo = new Label { Content = Convert.ToInt32(detalle.mayoViaje).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lbljunio = new Label { Content = Convert.ToInt32(detalle.junioViaje).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblJulio = new Label { Content = Convert.ToInt32(detalle.julioViaje).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAgosto = new Label { Content = Convert.ToInt32(detalle.agostoViaje).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblSeptiembre = new Label { Content = Convert.ToInt32(detalle.septiembreViaje).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblOctubre = new Label { Content = Convert.ToInt32(detalle.octubreViaje).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblNoviembre = new Label { Content = Convert.ToInt32(detalle.noviembreViaje).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblDiciembre = new Label { Content = Convert.ToInt32(detalle.diciembreViaje).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        Label lblSuma = new Label { Content = Convert.ToInt32(detalle.sumaTotalViajes).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }
                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.eneroViaje)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.febreroViaje)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.marzoViaje)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.abrilViaje)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.mayoViaje)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.junioViaje)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.julioViaje)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.agostoViaje)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.septiembreViaje)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.octubreViaje)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.noviembreViaje)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.diciembreViaje)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.sumaTotalViajes)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpViajesReal.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumEneroViaje)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumFebreroViaje)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumMarzoViaje)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumAbrilViaje)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumMayoViaje)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumJunioViaje)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumJulioViaje)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumAgostoViaje)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumSeptiembreViaje)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumOctubreViaje)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumNoviembreViaje)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumDiciembreViaje)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumTotalViaje)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpViajesReal.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void crearControlesViajesPresupuesto(List<Presupuesto> listaPresupuestos)
        {
            try
            {
                stpViajesPresupuesto.Children.Clear();
                foreach (var presupuesto in listaPresupuestos)
                {
                    StackPanel stpPrincipal = new StackPanel { Tag = presupuesto, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = presupuesto.cliente.nombre, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };

                    foreach (var detalle in presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VIAJES))
                    {
                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label { Content = detalle.modalidad.descripcion, Width = 120, FontWeight = FontWeights.Medium, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblEnero = new Label { Content = Convert.ToInt32(detalle.enero).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblFebrero = new Label { Content = Convert.ToInt32(detalle.febrero).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMarzo = new Label { Content = Convert.ToInt32(detalle.marzo).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAbril = new Label { Content = Convert.ToInt32(detalle.abril).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMayo = new Label { Content = Convert.ToInt32(detalle.mayo).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lbljunio = new Label { Content = Convert.ToInt32(detalle.junio).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblJulio = new Label { Content = Convert.ToInt32(detalle.julio).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAgosto = new Label { Content = Convert.ToInt32(detalle.agosto).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblSeptiembre = new Label { Content = Convert.ToInt32(detalle.septiembre).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblOctubre = new Label { Content = Convert.ToInt32(detalle.octubre).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblNoviembre = new Label { Content = Convert.ToInt32(detalle.noviembre).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblDiciembre = new Label { Content = Convert.ToInt32(detalle.diciembre).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        Label lblSuma = new Label { Content = Convert.ToInt32(detalle.sumaAnual).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }
                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VIAJES).Sum(s => s.enero)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VIAJES).Sum(s => s.febrero)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VIAJES).Sum(s => s.marzo)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VIAJES).Sum(s => s.abril)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VIAJES).Sum(s => s.mayo)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VIAJES).Sum(s => s.junio)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VIAJES).Sum(s => s.julio)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VIAJES).Sum(s => s.agosto)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VIAJES).Sum(s => s.septiembre)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VIAJES).Sum(s => s.octubre)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VIAJES).Sum(s => s.noviembre)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VIAJES).Sum(s => s.diciembre)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VIAJES).Sum(s => s.sumaAnual)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpViajesPresupuesto.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                List<DetallePresupuesto> listaDet = new List<DetallePresupuesto>();
                foreach (var item in listaPresupuestos)
                {
                    listaDet.AddRange(item.listaDetalles.FindAll(s => s.indicador == Indicador.VIAJES));
                }


                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.enero)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.febrero)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.marzo)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.abril)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.mayo)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.junio)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.julio)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.agosto)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.septiembre)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.octubre)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.noviembre)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.diciembre)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.sumaAnual)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpViajesPresupuesto.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        void crearDiferenciaViajes()
        {
            try
            {
                stpViajesRealPresupuesto.Children.Clear();
                int _sumEnero = 0, _sumFebrero = 0, _sumMarzo = 0, _sumAbril = 0, _sumMayo = 0, _sumJunio = 0, _sumJulio = 0, _sumAgosto = 0, _sumSeptiembre = 0, _sumOctubre = 0, _sumNoviembre = 0, _sumDiciembre = 0, _sumTotal = 0;
                foreach (var grupo in listaGrupoIndicadoresViajes)
                {
                    Presupuesto presupuesto = listaPresupuestos.Find(s => s.cliente.clave == grupo.idCliente);
                    if (presupuesto == null) continue;

                    StackPanel stpPrincipal = new StackPanel { Tag = grupo, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = grupo.cliente, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };
                    int sumEnero = 0, sumFebrero = 0, sumMarzo = 0, sumAbril = 0, sumMayo = 0, sumJunio = 0, sumJulio = 0, sumAgosto = 0, sumSeptiembre = 0, sumOctubre = 0, sumNoviembre = 0, sumDiciembre = 0, sumTotal = 0;
                    foreach (var detalle in grupo.listaDetalles)
                    {
                        DetallePresupuesto detPres = presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VIAJES).Find(s => s.modalidad.subModalidad == detalle.subModalidad);

                        int sumaAnual = 0;

                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label
                        {
                            Content = detalle.desModalidad,
                            Width = 120,
                            FontWeight = FontWeights.Medium,
                            FontSize = 11,
                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0)
                        };
                        int valor = Convert.ToInt32(detalle.eneroViaje - (detPres == null ? 0 : detPres.enero));
                        sumaAnual += valor;
                        sumEnero += valor;
                        _sumEnero += valor;
                        Label lblEnero = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.febreroViaje - (detPres == null ? 0 : detPres.febrero));
                        sumaAnual += valor;
                        sumFebrero += valor;
                        _sumFebrero += valor;
                        Label lblFebrero = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.marzoViaje - (detPres == null ? 0 : detPres.marzo));
                        sumaAnual += valor;
                        sumMarzo += valor;
                        _sumMarzo += valor;
                        Label lblMarzo = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.abrilViaje - (detPres == null ? 0 : detPres.abril));
                        sumaAnual += valor;
                        sumAbril += valor;
                        _sumAbril += valor;
                        Label lblAbril = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.mayoViaje - (detPres == null ? 0 : detPres.mayo));
                        sumaAnual += valor;
                        sumMayo += valor;
                        _sumMayo += valor;
                        Label lblMayo = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.junioViaje - (detPres == null ? 0 : detPres.junio));
                        sumaAnual += valor;
                        sumJunio += valor;
                        _sumJunio += valor;
                        Label lbljunio = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.julioViaje - (detPres == null ? 0 : detPres.julio));
                        sumaAnual += valor;
                        sumJulio += valor;
                        _sumJulio += valor;
                        Label lblJulio = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.agostoViaje - (detPres == null ? 0 : detPres.agosto));
                        sumaAnual += valor;
                        sumAgosto += valor;
                        _sumAgosto += valor;
                        Label lblAgosto = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.septiembreViaje - (detPres == null ? 0 : detPres.septiembre));
                        sumaAnual += valor;
                        sumSeptiembre += valor;
                        _sumSeptiembre += valor;
                        Label lblSeptiembre = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.octubreViaje - (detPres == null ? 0 : detPres.octubre));
                        sumaAnual += valor;
                        sumOctubre += valor;
                        _sumOctubre += valor;
                        Label lblOctubre = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.noviembreViaje - (detPres == null ? 0 : detPres.noviembre));
                        sumaAnual += valor;
                        sumNoviembre += valor;
                        _sumNoviembre += valor;
                        Label lblNoviembre = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.diciembreViaje - (detPres == null ? 0 : detPres.diciembre));
                        sumaAnual += valor;
                        sumDiciembre += valor;
                        _sumDiciembre += valor;
                        Label lblDiciembre = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        sumTotal += sumaAnual;
                        _sumTotal += sumaAnual;
                        Label lblSuma = new Label
                        {
                            Content = sumaAnual.ToString("N0"),
                            Width = 100,
                            FontWeight = FontWeights.Bold,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = sumaAnual < 0 ? Brushes.Red : Brushes.Black
                        };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }
                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumEnero.ToString("N0"), Foreground = (sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumFebrero.ToString("N0"), Foreground = (sumFebrero < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumMarzo.ToString("N0"), Foreground = (sumMarzo < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumAbril.ToString("N0"), Foreground = (sumAbril < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumMayo.ToString("N0"), Foreground = (sumMayo < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumJunio.ToString("N0"), Foreground = (sumJunio < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumJulio.ToString("N0"), Foreground = (sumJulio < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumAgosto.ToString("N0"), Foreground = (sumAgosto < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumSeptiembre.ToString("N0"), Foreground = (sumSeptiembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumOctubre.ToString("N0"), Foreground = (sumOctubre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumNoviembre.ToString("N0"), Foreground = (sumNoviembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumDiciembre.ToString("N0"), Foreground = (sumDiciembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumTotal.ToString("N0"), Foreground = (sumTotal < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpViajesRealPresupuesto.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumEnero.ToString("N0"), Foreground = (_sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumFebrero.ToString("N0"), Foreground = (_sumFebrero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumMarzo.ToString("N0"), Foreground = (_sumMarzo < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumAbril.ToString("N0"), Foreground = (_sumAbril < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumMayo.ToString("N0"), Foreground = (_sumMayo < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumJunio.ToString("N0"), Foreground = (_sumJunio < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumJulio.ToString("N0"), Foreground = (_sumJulio < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumAgosto.ToString("N0"), Foreground = (_sumAgosto < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumSeptiembre.ToString("N0"), Foreground = (_sumSeptiembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumOctubre.ToString("N0"), Foreground = (_sumOctubre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumNoviembre.ToString("N0"), Foreground = (_sumNoviembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumDiciembre.ToString("N0"), Foreground = (_sumDiciembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumTotal.ToString("N0"), Foreground = (_sumTotal < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpViajesRealPresupuesto.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void crearControlesVentasReales(List<GrupoIndicadoresViajes> listaIndicadores)
        {
            try
            {
                stpVentasReal.Children.Clear();
                foreach (var grupo in listaIndicadores)
                {
                    StackPanel stpPrincipal = new StackPanel { Tag = grupo, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = grupo.cliente, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };

                    foreach (var detalle in grupo.listaDetalles)
                    {
                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label { Content = detalle.desModalidad, Width = 120, FontWeight = FontWeights.Medium, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblEnero = new Label { Content = Convert.ToInt32(detalle.eneroVen).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblFebrero = new Label { Content = Convert.ToInt32(detalle.febreroVen).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMarzo = new Label { Content = Convert.ToInt32(detalle.marzoVen).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAbril = new Label { Content = Convert.ToInt32(detalle.abrilVen).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMayo = new Label { Content = Convert.ToInt32(detalle.mayoVen).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lbljunio = new Label { Content = Convert.ToInt32(detalle.junioVen).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblJulio = new Label { Content = Convert.ToInt32(detalle.julioVen).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAgosto = new Label { Content = Convert.ToInt32(detalle.agostoVen).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblSeptiembre = new Label { Content = Convert.ToInt32(detalle.septiembreVen).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblOctubre = new Label { Content = Convert.ToInt32(detalle.octubreVen).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblNoviembre = new Label { Content = Convert.ToInt32(detalle.noviembreVen).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblDiciembre = new Label { Content = Convert.ToInt32(detalle.diciembreVen).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        Label lblSuma = new Label { Content = Convert.ToInt32(detalle.sumaTotalVentas).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }
                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.eneroVen)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.febreroVen)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.marzoVen)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.abrilVen)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.mayoVen)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.junioVen)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.julioVen)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.agostoVen)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.septiembreVen)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.octubreVen)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.noviembreVen)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.diciembreVen)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.sumaTotalVentas)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpVentasReal.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumEneroVen)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumFebreroVen)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumMarzoVen)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumAbrilVen)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumMayoVen)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumJunioVen)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumJulioVen)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumAgostoVen)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumSeptiembreVen)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumOctubreVen)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumNoviembreVen)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumDiciembreVen)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumTotalVen)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpVentasReal.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void crearControlesVentasPresupuesto(List<Presupuesto> listaPresupuestos)
        {
            try
            {
                stpVentasPresupuesto.Children.Clear();
                foreach (var presupuesto in listaPresupuestos)
                {
                    StackPanel stpPrincipal = new StackPanel { Tag = presupuesto, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = presupuesto.cliente.nombre, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };

                    foreach (var detalle in presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VENTAS))
                    {
                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label { Content = detalle.modalidad.descripcion, Width = 120, FontWeight = FontWeights.Medium, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblEnero = new Label { Content = Convert.ToInt32(detalle.enero).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblFebrero = new Label { Content = Convert.ToInt32(detalle.febrero).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMarzo = new Label { Content = Convert.ToInt32(detalle.marzo).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAbril = new Label { Content = Convert.ToInt32(detalle.abril).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMayo = new Label { Content = Convert.ToInt32(detalle.mayo).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lbljunio = new Label { Content = Convert.ToInt32(detalle.junio).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblJulio = new Label { Content = Convert.ToInt32(detalle.julio).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAgosto = new Label { Content = Convert.ToInt32(detalle.agosto).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblSeptiembre = new Label { Content = Convert.ToInt32(detalle.septiembre).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblOctubre = new Label { Content = Convert.ToInt32(detalle.octubre).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblNoviembre = new Label { Content = Convert.ToInt32(detalle.noviembre).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblDiciembre = new Label { Content = Convert.ToInt32(detalle.diciembre).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        Label lblSuma = new Label { Content = Convert.ToInt32(detalle.sumaAnual).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }
                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VENTAS).Sum(s => s.enero)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VENTAS).Sum(s => s.febrero)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VENTAS).Sum(s => s.marzo)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VENTAS).Sum(s => s.abril)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VENTAS).Sum(s => s.mayo)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VENTAS).Sum(s => s.junio)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VENTAS).Sum(s => s.julio)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VENTAS).Sum(s => s.agosto)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VENTAS).Sum(s => s.septiembre)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VENTAS).Sum(s => s.octubre)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VENTAS).Sum(s => s.noviembre)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VENTAS).Sum(s => s.diciembre)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VENTAS).Sum(s => s.sumaAnual)).ToString("C0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpVentasPresupuesto.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                List<DetallePresupuesto> listaDet = new List<DetallePresupuesto>();
                foreach (var item in listaPresupuestos)
                {
                    listaDet.AddRange(item.listaDetalles.FindAll(s => s.indicador == Indicador.VENTAS));
                }


                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.enero)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.febrero)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.marzo)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.abril)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.mayo)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.junio)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.julio)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.agosto)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.septiembre)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.octubre)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.noviembre)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.diciembre)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.sumaAnual)).ToString("C0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpVentasPresupuesto.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        void crearDiferenciaVentas()
        {
            try
            {
                stpVentasRealPresupuesto.Children.Clear();
                int _sumEnero = 0, _sumFebrero = 0, _sumMarzo = 0, _sumAbril = 0, _sumMayo = 0, _sumJunio = 0, _sumJulio = 0, _sumAgosto = 0, _sumSeptiembre = 0, _sumOctubre = 0, _sumNoviembre = 0, _sumDiciembre = 0, _sumTotal = 0;
                foreach (var grupo in listaGrupoIndicadoresViajes)
                {
                    Presupuesto presupuesto = listaPresupuestos.Find(s => s.cliente.clave == grupo.idCliente);
                    if (presupuesto == null) continue;

                    StackPanel stpPrincipal = new StackPanel { Tag = grupo, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = grupo.cliente, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };
                    int sumEnero = 0, sumFebrero = 0, sumMarzo = 0, sumAbril = 0, sumMayo = 0, sumJunio = 0, sumJulio = 0, sumAgosto = 0, sumSeptiembre = 0, sumOctubre = 0, sumNoviembre = 0, sumDiciembre = 0, sumTotal = 0;
                    foreach (var detalle in grupo.listaDetalles)
                    {
                        DetallePresupuesto detPres = presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.VENTAS).Find(s => s.modalidad.subModalidad == detalle.subModalidad);

                        int sumaAnual = 0;

                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label
                        {
                            Content = detalle.desModalidad,
                            Width = 120,
                            FontWeight = FontWeights.Medium,
                            FontSize = 11,
                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0)
                        };
                        int valor = Convert.ToInt32(detalle.eneroVen - (detPres == null ? 0 : detPres.enero));
                        sumaAnual += valor;
                        sumEnero += valor;
                        _sumEnero += valor;
                        Label lblEnero = new Label
                        {
                            Content = valor.ToString("C0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.febreroVen - (detPres == null ? 0 : detPres.febrero));
                        sumaAnual += valor;
                        sumFebrero += valor;
                        _sumFebrero += valor;
                        Label lblFebrero = new Label
                        {
                            Content = valor.ToString("C0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.marzoVen - (detPres == null ? 0 : detPres.marzo));
                        sumaAnual += valor;
                        sumMarzo += valor;
                        _sumMarzo += valor;
                        Label lblMarzo = new Label
                        {
                            Content = valor.ToString("C0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.abrilVen - (detPres == null ? 0 : detPres.abril));
                        sumaAnual += valor;
                        sumAbril += valor;
                        _sumAbril += valor;
                        Label lblAbril = new Label
                        {
                            Content = valor.ToString("C0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.mayoVen - (detPres == null ? 0 : detPres.mayo));
                        sumaAnual += valor;
                        sumMayo += valor;
                        _sumMayo += valor;
                        Label lblMayo = new Label
                        {
                            Content = valor.ToString("C0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.junioVen - (detPres == null ? 0 : detPres.junio));
                        sumaAnual += valor;
                        sumJunio += valor;
                        _sumJunio += valor;
                        Label lbljunio = new Label
                        {
                            Content = valor.ToString("C0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.julioVen - (detPres == null ? 0 : detPres.julio));
                        sumaAnual += valor;
                        sumJulio += valor;
                        _sumJulio += valor;
                        Label lblJulio = new Label
                        {
                            Content = valor.ToString("C0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.agostoVen - (detPres == null ? 0 : detPres.agosto));
                        sumaAnual += valor;
                        sumAgosto += valor;
                        _sumAgosto += valor;
                        Label lblAgosto = new Label
                        {
                            Content = valor.ToString("C0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.septiembreVen - (detPres == null ? 0 : detPres.septiembre));
                        sumaAnual += valor;
                        sumSeptiembre += valor;
                        _sumSeptiembre += valor;
                        Label lblSeptiembre = new Label
                        {
                            Content = valor.ToString("C0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.octubreVen - (detPres == null ? 0 : detPres.octubre));
                        sumaAnual += valor;
                        sumOctubre += valor;
                        _sumOctubre += valor;
                        Label lblOctubre = new Label
                        {
                            Content = valor.ToString("C0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.noviembreVen - (detPres == null ? 0 : detPres.noviembre));
                        sumaAnual += valor;
                        sumNoviembre += valor;
                        _sumNoviembre += valor;
                        Label lblNoviembre = new Label
                        {
                            Content = valor.ToString("C0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.diciembreVen - (detPres == null ? 0 : detPres.diciembre));
                        sumaAnual += valor;
                        sumDiciembre += valor;
                        _sumDiciembre += valor;
                        Label lblDiciembre = new Label
                        {
                            Content = valor.ToString("C0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        sumTotal += sumaAnual;
                        _sumTotal += sumaAnual;
                        Label lblSuma = new Label
                        {
                            Content = sumaAnual.ToString("C0"),
                            Width = 100,
                            FontWeight = FontWeights.Bold,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = sumaAnual < 0 ? Brushes.Red : Brushes.Black
                        };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }
                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumEnero.ToString("C0"), Foreground = (sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumFebrero.ToString("C0"), Foreground = (sumFebrero < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumMarzo.ToString("C0"), Foreground = (sumMarzo < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumAbril.ToString("C0"), Foreground = (sumAbril < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumMayo.ToString("C0"), Foreground = (sumMayo < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumJunio.ToString("C0"), Foreground = (sumJunio < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumJulio.ToString("C0"), Foreground = (sumJulio < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumAgosto.ToString("C0"), Foreground = (sumAgosto < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumSeptiembre.ToString("C0"), Foreground = (sumSeptiembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumOctubre.ToString("C0"), Foreground = (sumOctubre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumNoviembre.ToString("C0"), Foreground = (sumNoviembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumDiciembre.ToString("C0"), Foreground = (sumDiciembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumTotal.ToString("C0"), Foreground = (sumTotal < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpVentasRealPresupuesto.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumEnero.ToString("C0"), Foreground = (_sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumFebrero.ToString("C0"), Foreground = (_sumFebrero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumMarzo.ToString("C0"), Foreground = (_sumMarzo < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumAbril.ToString("C0"), Foreground = (_sumAbril < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumMayo.ToString("C0"), Foreground = (_sumMayo < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumJunio.ToString("C0"), Foreground = (_sumJunio < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumJulio.ToString("C0"), Foreground = (_sumJulio < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumAgosto.ToString("C0"), Foreground = (_sumAgosto < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumSeptiembre.ToString("C0"), Foreground = (_sumSeptiembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumOctubre.ToString("C0"), Foreground = (_sumOctubre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumNoviembre.ToString("C0"), Foreground = (_sumNoviembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumDiciembre.ToString("C0"), Foreground = (_sumDiciembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumTotal.ToString("C0"), Foreground = (_sumTotal < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpVentasRealPresupuesto.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        /// ///////////////////////////////////////////////////////////////////////////////////        
        private void crearControlesKmReales(List<GrupoIndicadoresCombustible> listaIndicadores)
        {
            try
            {
                stpKmsReal.Children.Clear();
                foreach (var grupo in listaIndicadores)
                {
                    StackPanel stpPrincipal = new StackPanel { Tag = grupo, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = grupo.cliente, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };

                    foreach (var detalle in grupo.listaDetalles)
                    {
                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label { Content = detalle.desModalidad, Width = 120, FontWeight = FontWeights.Medium, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblEnero = new Label { Content = Convert.ToInt32(detalle.eneroKm).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblFebrero = new Label { Content = Convert.ToInt32(detalle.febreroKm).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMarzo = new Label { Content = Convert.ToInt32(detalle.marzoKm).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAbril = new Label { Content = Convert.ToInt32(detalle.abrilKm).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMayo = new Label { Content = Convert.ToInt32(detalle.mayoKm).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lbljunio = new Label { Content = Convert.ToInt32(detalle.junioKm).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblJulio = new Label { Content = Convert.ToInt32(detalle.julioKm).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAgosto = new Label { Content = Convert.ToInt32(detalle.agostoKm).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblSeptiembre = new Label { Content = Convert.ToInt32(detalle.septiembreKm).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblOctubre = new Label { Content = Convert.ToInt32(detalle.octubreKm).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblNoviembre = new Label { Content = Convert.ToInt32(detalle.noviembreKm).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblDiciembre = new Label { Content = Convert.ToInt32(detalle.diciembreKm).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        Label lblSuma = new Label { Content = Convert.ToInt32(detalle.sumaTotalKm).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }
                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.eneroKm)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.febreroKm)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.marzoKm)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.abrilKm)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.mayoKm)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.junioKm)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.julioKm)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.agostoKm)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.septiembreKm)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.octubreKm)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.noviembreKm)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.diciembreKm)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.sumaTotalKm)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpKmsReal.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumEneroKm)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumFebreroKm)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumMarzoKm)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumAbrilKm)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumMayoKm)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumJunioKm)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumJulioKm)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumAgostoKm)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumSeptiembreKm)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumOctubreKm)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumNoviembreKm)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumDiciembreKm)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumTotalKm)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpKmsReal.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void crearControlesKmPresupuesto(List<Presupuesto> listaPresupuestos)
        {
            try
            {
                stpKmPresupuesto.Children.Clear();
                foreach (var presupuesto in listaPresupuestos)
                {
                    StackPanel stpPrincipal = new StackPanel { Tag = presupuesto, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = presupuesto.cliente.nombre, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };

                    foreach (var detalle in presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS))
                    {
                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label { Content = detalle.modalidad.descripcion, Width = 120, FontWeight = FontWeights.Medium, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblEnero = new Label { Content = Convert.ToInt32(detalle.enero).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblFebrero = new Label { Content = Convert.ToInt32(detalle.febrero).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMarzo = new Label { Content = Convert.ToInt32(detalle.marzo).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAbril = new Label { Content = Convert.ToInt32(detalle.abril).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMayo = new Label { Content = Convert.ToInt32(detalle.mayo).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lbljunio = new Label { Content = Convert.ToInt32(detalle.junio).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblJulio = new Label { Content = Convert.ToInt32(detalle.julio).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAgosto = new Label { Content = Convert.ToInt32(detalle.agosto).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblSeptiembre = new Label { Content = Convert.ToInt32(detalle.septiembre).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblOctubre = new Label { Content = Convert.ToInt32(detalle.octubre).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblNoviembre = new Label { Content = Convert.ToInt32(detalle.noviembre).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblDiciembre = new Label { Content = Convert.ToInt32(detalle.diciembre).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        Label lblSuma = new Label { Content = Convert.ToInt32(detalle.sumaAnual).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }
                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).Sum(s => s.enero)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).Sum(s => s.febrero)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).Sum(s => s.marzo)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).Sum(s => s.abril)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).Sum(s => s.mayo)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).Sum(s => s.junio)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).Sum(s => s.julio)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).Sum(s => s.agosto)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).Sum(s => s.septiembre)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).Sum(s => s.octubre)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).Sum(s => s.noviembre)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).Sum(s => s.diciembre)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).Sum(s => s.sumaAnual)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpKmPresupuesto.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                List<DetallePresupuesto> listaDet = new List<DetallePresupuesto>();
                foreach (var item in listaPresupuestos)
                {
                    listaDet.AddRange(item.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS));
                }


                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.enero)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.febrero)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.marzo)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.abril)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.mayo)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.junio)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.julio)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.agosto)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.septiembre)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.octubre)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.noviembre)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.diciembre)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.sumaAnual)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpKmPresupuesto.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        void crearDiferenciaKm()
        {
            try
            {
                stpKmRealPresupuesto.Children.Clear();
                int _sumEnero = 0, _sumFebrero = 0, _sumMarzo = 0, _sumAbril = 0, _sumMayo = 0, _sumJunio = 0, _sumJulio = 0, _sumAgosto = 0, _sumSeptiembre = 0, _sumOctubre = 0, _sumNoviembre = 0, _sumDiciembre = 0, _sumTotal = 0;
                foreach (var grupo in listaGrupoIndicadoresCombustible)
                {
                    Presupuesto presupuesto = listaPresupuestos.Find(s => s.cliente.clave == grupo.idCliente);
                    if (presupuesto == null) continue;

                    StackPanel stpPrincipal = new StackPanel { Tag = grupo, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = grupo.cliente, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };
                    int sumEnero = 0, sumFebrero = 0, sumMarzo = 0, sumAbril = 0, sumMayo = 0, sumJunio = 0, sumJulio = 0, sumAgosto = 0, sumSeptiembre = 0, sumOctubre = 0, sumNoviembre = 0, sumDiciembre = 0, sumTotal = 0;
                    foreach (var detalle in grupo.listaDetalles)
                    {
                        DetallePresupuesto detPres = presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).Find(s => s.modalidad.subModalidad == detalle.subModalidad);

                        int sumaAnual = 0;

                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label
                        {
                            Content = detalle.desModalidad,
                            Width = 120,
                            FontWeight = FontWeights.Medium,
                            FontSize = 11,
                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0)
                        };
                        int valor = Convert.ToInt32(detalle.eneroKm - (detPres == null ? 0 : detPres.enero));
                        sumaAnual += valor;
                        sumEnero += valor;
                        _sumEnero += valor;
                        Label lblEnero = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.febreroKm - (detPres == null ? 0 : detPres.febrero));
                        sumaAnual += valor;
                        sumFebrero += valor;
                        _sumFebrero += valor;
                        Label lblFebrero = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.marzoKm - (detPres == null ? 0 : detPres.marzo));
                        sumaAnual += valor;
                        sumMarzo += valor;
                        _sumMarzo += valor;
                        Label lblMarzo = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.abrilKm - (detPres == null ? 0 : detPres.abril));
                        sumaAnual += valor;
                        sumAbril += valor;
                        _sumAbril += valor;
                        Label lblAbril = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.mayoKm - (detPres == null ? 0 : detPres.mayo));
                        sumaAnual += valor;
                        sumMayo += valor;
                        _sumMayo += valor;
                        Label lblMayo = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.junioKm - (detPres == null ? 0 : detPres.junio));
                        sumaAnual += valor;
                        sumJunio += valor;
                        _sumJunio += valor;
                        Label lbljunio = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.julioKm - (detPres == null ? 0 : detPres.julio));
                        sumaAnual += valor;
                        sumJulio += valor;
                        _sumJulio += valor;
                        Label lblJulio = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.agostoKm - (detPres == null ? 0 : detPres.agosto));
                        sumaAnual += valor;
                        sumAgosto += valor;
                        _sumAgosto += valor;
                        Label lblAgosto = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.septiembreKm - (detPres == null ? 0 : detPres.septiembre));
                        sumaAnual += valor;
                        sumSeptiembre += valor;
                        _sumSeptiembre += valor;
                        Label lblSeptiembre = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.octubreKm - (detPres == null ? 0 : detPres.octubre));
                        sumaAnual += valor;
                        sumOctubre += valor;
                        _sumOctubre += valor;
                        Label lblOctubre = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.noviembreKm - (detPres == null ? 0 : detPres.noviembre));
                        sumaAnual += valor;
                        sumNoviembre += valor;
                        _sumNoviembre += valor;
                        Label lblNoviembre = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.diciembreKm - (detPres == null ? 0 : detPres.diciembre));
                        sumaAnual += valor;
                        sumDiciembre += valor;
                        _sumDiciembre += valor;
                        Label lblDiciembre = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        sumTotal += sumaAnual;
                        _sumTotal += sumaAnual;
                        Label lblSuma = new Label
                        {
                            Content = sumaAnual.ToString("N0"),
                            Width = 100,
                            FontWeight = FontWeights.Bold,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = sumaAnual < 0 ? Brushes.Red : Brushes.Black
                        };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }
                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumEnero.ToString("N0"), Foreground = (sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumFebrero.ToString("N0"), Foreground = (sumFebrero < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumMarzo.ToString("N0"), Foreground = (sumMarzo < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumAbril.ToString("N0"), Foreground = (sumAbril < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumMayo.ToString("N0"), Foreground = (sumMayo < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumJunio.ToString("N0"), Foreground = (sumJunio < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumJulio.ToString("N0"), Foreground = (sumJulio < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumAgosto.ToString("N0"), Foreground = (sumAgosto < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumSeptiembre.ToString("N0"), Foreground = (sumSeptiembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumOctubre.ToString("N0"), Foreground = (sumOctubre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumNoviembre.ToString("N0"), Foreground = (sumNoviembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumDiciembre.ToString("N0"), Foreground = (sumDiciembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumTotal.ToString("N0"), Foreground = (sumTotal < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpKmRealPresupuesto.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumEnero.ToString("N0"), Foreground = (_sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumFebrero.ToString("N0"), Foreground = (_sumFebrero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumMarzo.ToString("N0"), Foreground = (_sumMarzo < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumAbril.ToString("N0"), Foreground = (_sumAbril < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumMayo.ToString("N0"), Foreground = (_sumMayo < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumJunio.ToString("N0"), Foreground = (_sumJunio < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumJulio.ToString("N0"), Foreground = (_sumJulio < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumAgosto.ToString("N0"), Foreground = (_sumAgosto < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumSeptiembre.ToString("N0"), Foreground = (_sumSeptiembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumOctubre.ToString("N0"), Foreground = (_sumOctubre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumNoviembre.ToString("N0"), Foreground = (_sumNoviembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumDiciembre.ToString("N0"), Foreground = (_sumDiciembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumTotal.ToString("N0"), Foreground = (_sumTotal < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpKmRealPresupuesto.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void crearControlesLitroReales(List<GrupoIndicadoresCombustible> listaIndicadores)
        {
            try
            {
                stpLitrosReal.Children.Clear();
                foreach (var grupo in listaIndicadores)
                {
                    StackPanel stpPrincipal = new StackPanel { Tag = grupo, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = grupo.cliente, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };

                    foreach (var detalle in grupo.listaDetalles)
                    {
                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label { Content = detalle.desModalidad, Width = 120, FontWeight = FontWeights.Medium, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblEnero = new Label { Content = Convert.ToInt32(detalle.eneroLitro).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblFebrero = new Label { Content = Convert.ToInt32(detalle.febreroLitro).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMarzo = new Label { Content = Convert.ToInt32(detalle.marzoLitro).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAbril = new Label { Content = Convert.ToInt32(detalle.abrilLitro).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMayo = new Label { Content = Convert.ToInt32(detalle.mayoLitro).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lbljunio = new Label { Content = Convert.ToInt32(detalle.junioLitro).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblJulio = new Label { Content = Convert.ToInt32(detalle.julioLitro).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAgosto = new Label { Content = Convert.ToInt32(detalle.agostoLitro).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblSeptiembre = new Label { Content = Convert.ToInt32(detalle.septiembreLitro).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblOctubre = new Label { Content = Convert.ToInt32(detalle.octubreLitro).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblNoviembre = new Label { Content = Convert.ToInt32(detalle.noviembreLitro).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblDiciembre = new Label { Content = Convert.ToInt32(detalle.diciembreLitro).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        Label lblSuma = new Label { Content = Convert.ToInt32(detalle.sumaTotalLitros).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }
                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.eneroLitro)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.febreroLitro)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.marzoLitro)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.abrilLitro)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.mayoLitro)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.junioLitro)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.julioLitro)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.agostoLitro)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.septiembreLitro)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.octubreLitro)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.noviembreLitro)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.diciembreLitro)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(grupo.listaDetalles.Sum(s => s.sumaTotalLitros)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpLitrosReal.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumEneroLitro)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumFebreroLitro)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumMarzoLitro)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumAbrilLitro)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumMayoLitro)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumJunioLitro)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumJulioLitro)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumAgostoLitro)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumSeptiembreLitro)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumOctubreLitro)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumNoviembreLitro)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumDiciembreLitro)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaIndicadores.Sum(s => s.sumTotalLitro)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpLitrosReal.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void crearControlesLitrosPresupuesto(List<Presupuesto> listaPresupuestos)
        {
            try
            {
                stpLitrosPresupuesto.Children.Clear();
                foreach (var presupuesto in listaPresupuestos)
                {
                    StackPanel stpPrincipal = new StackPanel { Tag = presupuesto, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = presupuesto.cliente.nombre, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };

                    foreach (var detalle in presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS))
                    {
                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label { Content = detalle.modalidad.descripcion, Width = 120, FontWeight = FontWeights.Medium, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblEnero = new Label { Content = Convert.ToInt32(detalle.enero).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblFebrero = new Label { Content = Convert.ToInt32(detalle.febrero).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMarzo = new Label { Content = Convert.ToInt32(detalle.marzo).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAbril = new Label { Content = Convert.ToInt32(detalle.abril).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMayo = new Label { Content = Convert.ToInt32(detalle.mayo).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lbljunio = new Label { Content = Convert.ToInt32(detalle.junio).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblJulio = new Label { Content = Convert.ToInt32(detalle.julio).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAgosto = new Label { Content = Convert.ToInt32(detalle.agosto).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblSeptiembre = new Label { Content = Convert.ToInt32(detalle.septiembre).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblOctubre = new Label { Content = Convert.ToInt32(detalle.octubre).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblNoviembre = new Label { Content = Convert.ToInt32(detalle.noviembre).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblDiciembre = new Label { Content = Convert.ToInt32(detalle.diciembre).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        Label lblSuma = new Label { Content = Convert.ToInt32(detalle.sumaAnual).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }
                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS).Sum(s => s.enero)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS).Sum(s => s.febrero)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS).Sum(s => s.marzo)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS).Sum(s => s.abril)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS).Sum(s => s.mayo)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS).Sum(s => s.junio)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS).Sum(s => s.julio)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS).Sum(s => s.agosto)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS).Sum(s => s.septiembre)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS).Sum(s => s.octubre)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS).Sum(s => s.noviembre)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS).Sum(s => s.diciembre)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToInt32(presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS).Sum(s => s.sumaAnual)).ToString("N0"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpLitrosPresupuesto.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                List<DetallePresupuesto> listaDet = new List<DetallePresupuesto>();
                foreach (var item in listaPresupuestos)
                {
                    listaDet.AddRange(item.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS));
                }


                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.enero)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.febrero)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.marzo)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.abril)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.mayo)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.junio)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.julio)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.agosto)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.septiembre)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.octubre)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.noviembre)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.diciembre)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToInt32(listaDet.Sum(s => s.sumaAnual)).ToString("N0"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpLitrosPresupuesto.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        void crearDiferenciaLitros()
        {
            try
            {
                stpLitrosRealPresupuesto.Children.Clear();
                int _sumEnero = 0, _sumFebrero = 0, _sumMarzo = 0, _sumAbril = 0, _sumMayo = 0, _sumJunio = 0, _sumJulio = 0, _sumAgosto = 0, _sumSeptiembre = 0, _sumOctubre = 0, _sumNoviembre = 0, _sumDiciembre = 0, _sumTotal = 0;
                foreach (var grupo in listaGrupoIndicadoresCombustible)
                {
                    Presupuesto presupuesto = listaPresupuestos.Find(s => s.cliente.clave == grupo.idCliente);
                    if (presupuesto == null) continue;

                    StackPanel stpPrincipal = new StackPanel { Tag = grupo, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = grupo.cliente, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };
                    int sumEnero = 0, sumFebrero = 0, sumMarzo = 0, sumAbril = 0, sumMayo = 0, sumJunio = 0, sumJulio = 0, sumAgosto = 0, sumSeptiembre = 0, sumOctubre = 0, sumNoviembre = 0, sumDiciembre = 0, sumTotal = 0;
                    foreach (var detalle in grupo.listaDetalles)
                    {
                        DetallePresupuesto detPres = presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS).Find(s => s.modalidad.subModalidad == detalle.subModalidad);

                        int sumaAnual = 0;

                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label
                        {
                            Content = detalle.desModalidad,
                            Width = 120,
                            FontWeight = FontWeights.Medium,
                            FontSize = 11,
                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0)
                        };
                        int valor = Convert.ToInt32(detalle.eneroLitro - (detPres == null ? 0 : detPres.enero));
                        sumaAnual += valor;
                        sumEnero += valor;
                        _sumEnero += valor;
                        Label lblEnero = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.febreroLitro - (detPres == null ? 0 : detPres.febrero));
                        sumaAnual += valor;
                        sumFebrero += valor;
                        _sumFebrero += valor;
                        Label lblFebrero = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.marzoLitro - (detPres == null ? 0 : detPres.marzo));
                        sumaAnual += valor;
                        sumMarzo += valor;
                        _sumMarzo += valor;
                        Label lblMarzo = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.abrilLitro - (detPres == null ? 0 : detPres.abril));
                        sumaAnual += valor;
                        sumAbril += valor;
                        _sumAbril += valor;
                        Label lblAbril = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.mayoLitro - (detPres == null ? 0 : detPres.mayo));
                        sumaAnual += valor;
                        sumMayo += valor;
                        _sumMayo += valor;
                        Label lblMayo = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.junioLitro - (detPres == null ? 0 : detPres.junio));
                        sumaAnual += valor;
                        sumJunio += valor;
                        _sumJunio += valor;
                        Label lbljunio = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.julioLitro - (detPres == null ? 0 : detPres.julio));
                        sumaAnual += valor;
                        sumJulio += valor;
                        _sumJulio += valor;
                        Label lblJulio = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.agostoLitro - (detPres == null ? 0 : detPres.agosto));
                        sumaAnual += valor;
                        sumAgosto += valor;
                        _sumAgosto += valor;
                        Label lblAgosto = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.septiembreLitro - (detPres == null ? 0 : detPres.septiembre));
                        sumaAnual += valor;
                        sumSeptiembre += valor;
                        _sumSeptiembre += valor;
                        Label lblSeptiembre = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.octubreLitro - (detPres == null ? 0 : detPres.octubre));
                        sumaAnual += valor;
                        sumOctubre += valor;
                        _sumOctubre += valor;
                        Label lblOctubre = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.noviembreLitro - (detPres == null ? 0 : detPres.noviembre));
                        sumaAnual += valor;
                        sumNoviembre += valor;
                        _sumNoviembre += valor;
                        Label lblNoviembre = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToInt32(detalle.diciembreLitro - (detPres == null ? 0 : detPres.diciembre));
                        sumaAnual += valor;
                        sumDiciembre += valor;
                        _sumDiciembre += valor;
                        Label lblDiciembre = new Label
                        {
                            Content = valor.ToString("N0"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        sumTotal += sumaAnual;
                        _sumTotal += sumaAnual;
                        Label lblSuma = new Label
                        {
                            Content = sumaAnual.ToString("N0"),
                            Width = 100,
                            FontWeight = FontWeights.Bold,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = sumaAnual < 0 ? Brushes.Red : Brushes.Black
                        };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }
                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumEnero.ToString("N0"), Foreground = (sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumFebrero.ToString("N0"), Foreground = (sumFebrero < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumMarzo.ToString("N0"), Foreground = (sumMarzo < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumAbril.ToString("N0"), Foreground = (sumAbril < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumMayo.ToString("N0"), Foreground = (sumMayo < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumJunio.ToString("N0"), Foreground = (sumJunio < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumJulio.ToString("N0"), Foreground = (sumJulio < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumAgosto.ToString("N0"), Foreground = (sumAgosto < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumSeptiembre.ToString("N0"), Foreground = (sumSeptiembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumOctubre.ToString("N0"), Foreground = (sumOctubre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumNoviembre.ToString("N0"), Foreground = (sumNoviembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumDiciembre.ToString("N0"), Foreground = (sumDiciembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = sumTotal.ToString("N0"), Foreground = (sumTotal < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpLitrosRealPresupuesto.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumEnero.ToString("N0"), Foreground = (_sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumFebrero.ToString("N0"), Foreground = (_sumFebrero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumMarzo.ToString("N0"), Foreground = (_sumMarzo < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumAbril.ToString("N0"), Foreground = (_sumAbril < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumMayo.ToString("N0"), Foreground = (_sumMayo < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumJunio.ToString("N0"), Foreground = (_sumJunio < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumJulio.ToString("N0"), Foreground = (_sumJulio < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumAgosto.ToString("N0"), Foreground = (_sumAgosto < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumSeptiembre.ToString("N0"), Foreground = (_sumSeptiembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumOctubre.ToString("N0"), Foreground = (_sumOctubre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumNoviembre.ToString("N0"), Foreground = (_sumNoviembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumDiciembre.ToString("N0"), Foreground = (_sumDiciembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumTotal.ToString("N0"), Foreground = (_sumTotal < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpLitrosRealPresupuesto.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void crearControlesRendimientoReales(List<GrupoIndicadoresCombustible> listaIndicadores)
        {
            try
            {
                stpRendimientoReal.Children.Clear();
                foreach (var grupo in listaIndicadores)
                {
                    StackPanel stpPrincipal = new StackPanel { Tag = grupo, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = grupo.cliente, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };

                    foreach (var detalle in grupo.listaDetalles)
                    {
                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label { Content = detalle.desModalidad, Width = 120, FontWeight = FontWeights.Medium, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblEnero = new Label { Content = Convert.ToDecimal(detalle.eneroRendimiento).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblFebrero = new Label { Content = Convert.ToDecimal(detalle.febreroRendimiento).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMarzo = new Label { Content = Convert.ToDecimal(detalle.marzoRendimiento).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAbril = new Label { Content = Convert.ToDecimal(detalle.abrilRendimiento).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMayo = new Label { Content = Convert.ToDecimal(detalle.mayoRendimiento).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lbljunio = new Label { Content = Convert.ToDecimal(detalle.junioRendimiento).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblJulio = new Label { Content = Convert.ToDecimal(detalle.julioRendimiento).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAgosto = new Label { Content = Convert.ToDecimal(detalle.agostoRendimiento).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblSeptiembre = new Label { Content = Convert.ToDecimal(detalle.septiembreRendimiento).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblOctubre = new Label { Content = Convert.ToDecimal(detalle.octubreRendimiento).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblNoviembre = new Label { Content = Convert.ToDecimal(detalle.noviembreRendimiento).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblDiciembre = new Label { Content = Convert.ToDecimal(detalle.diciembreRendimiento).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        Label lblSuma = new Label { Content = Convert.ToDecimal(detalle.sumaTotalRendimientos).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }
                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(grupo.listaDetalles.Sum(s => s.eneroLitro) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.eneroKm) / grupo.listaDetalles.Sum(s => s.eneroLitro)).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(grupo.listaDetalles.Sum(s => s.febreroLitro) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.febreroKm) / grupo.listaDetalles.Sum(s => s.febreroLitro)).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(grupo.listaDetalles.Sum(s => s.marzoLitro) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.marzoKm) / grupo.listaDetalles.Sum(s => s.marzoLitro)).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(grupo.listaDetalles.Sum(s => s.abrilLitro) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.abrilKm) / grupo.listaDetalles.Sum(s => s.abrilLitro)).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(grupo.listaDetalles.Sum(s => s.mayoLitro) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.mayoKm) / grupo.listaDetalles.Sum(s => s.mayoLitro)).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(grupo.listaDetalles.Sum(s => s.junioLitro) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.junioKm) / grupo.listaDetalles.Sum(s => s.junioLitro)).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(grupo.listaDetalles.Sum(s => s.julioLitro) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.julioKm) / grupo.listaDetalles.Sum(s => s.julioLitro)).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(grupo.listaDetalles.Sum(s => s.agostoLitro) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.agostoKm) / grupo.listaDetalles.Sum(s => s.agostoLitro)).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(grupo.listaDetalles.Sum(s => s.septiembreLitro) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.septiembreKm) / grupo.listaDetalles.Sum(s => s.septiembreLitro)).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(grupo.listaDetalles.Sum(s => s.octubreLitro) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.octubreKm) / grupo.listaDetalles.Sum(s => s.octubreLitro)).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(grupo.listaDetalles.Sum(s => s.noviembreLitro) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.noviembreKm) / grupo.listaDetalles.Sum(s => s.noviembreLitro)).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(grupo.listaDetalles.Sum(s => s.diciembreLitro) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.diciembreKm) / grupo.listaDetalles.Sum(s => s.diciembreLitro)).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    decimal ts = grupo.listaDetalles.Sum(s => s.sumaTotalKm);
                    decimal y = grupo.listaDetalles.Sum(s => s.sumaTotalLitros);
                    decimal sss = y <= 0 ? 0 : ts / y;
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(sss).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });


                    border.Child = stpTotal;
                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpRendimientoReal.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadores.Sum(s => s.sumEneroLitro) <= 0 ? 0 : listaIndicadores.Sum(s => s.sumEneroKm) / listaIndicadores.Sum(s => s.sumEneroLitro)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadores.Sum(s => s.sumFebreroLitro) <= 0 ? 0 : listaIndicadores.Sum(s => s.sumFebreroKm) / listaIndicadores.Sum(s => s.sumFebreroLitro)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadores.Sum(s => s.sumMarzoLitro) <= 0 ? 0 : listaIndicadores.Sum(s => s.sumMarzoKm) / listaIndicadores.Sum(s => s.sumMarzoLitro)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadores.Sum(s => s.sumAbrilLitro) <= 0 ? 0 : listaIndicadores.Sum(s => s.sumAbrilKm) / listaIndicadores.Sum(s => s.sumAbrilLitro)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadores.Sum(s => s.sumMayoLitro) <= 0 ? 0 : listaIndicadores.Sum(s => s.sumMayoKm) / listaIndicadores.Sum(s => s.sumMayoLitro)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadores.Sum(s => s.sumJunioLitro) <= 0 ? 0 : listaIndicadores.Sum(s => s.sumJunioKm) / listaIndicadores.Sum(s => s.sumJunioLitro)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadores.Sum(s => s.sumJulioLitro) <= 0 ? 0 : listaIndicadores.Sum(s => s.sumJulioKm) / listaIndicadores.Sum(s => s.sumJulioLitro)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadores.Sum(s => s.sumAgostoLitro) <= 0 ? 0 : listaIndicadores.Sum(s => s.sumAgostoKm) / listaIndicadores.Sum(s => s.sumAgostoLitro)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadores.Sum(s => s.sumSeptiembreLitro) <= 0 ? 0 : listaIndicadores.Sum(s => s.sumSeptiembreKm) / listaIndicadores.Sum(s => s.sumSeptiembreLitro)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadores.Sum(s => s.sumAgostoLitro) <= 0 ? 0 : listaIndicadores.Sum(s => s.sumAgostoKm) / listaIndicadores.Sum(s => s.sumAgostoLitro)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadores.Sum(s => s.sumNoviembreLitro) <= 0 ? 0 : listaIndicadores.Sum(s => s.sumNoviembreKm) / listaIndicadores.Sum(s => s.sumNoviembreLitro)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadores.Sum(s => s.sumDiciembreLitro) <= 0 ? 0 : listaIndicadores.Sum(s => s.sumDiciembreKm) / listaIndicadores.Sum(s => s.sumDiciembreLitro)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadores.Sum(s => s.sumTotalLitro) <= 0 ? 0 : listaIndicadores.Sum(s => s.sumTotalKm) / listaIndicadores.Sum(s => s.sumTotalLitro)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpRendimientoReal.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void crearControlesRendimientoPresupuesto(List<Presupuesto> listaPresupuestos)
        {
            try
            {
                stpRendimientoPresupuesto.Children.Clear();
                foreach (var presupuesto in listaPresupuestos)
                {
                    StackPanel stpPrincipal = new StackPanel { Tag = presupuesto, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = presupuesto.cliente.nombre, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };
                    List<DetallePresupuesto> listaKMPre = presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS);
                    List<DetallePresupuesto> listaLitroPre = presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS);

                    foreach (var detalle in presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.RENDIMIENTO))
                    {
                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label { Content = detalle.modalidad.descripcion, Width = 120, FontWeight = FontWeights.Medium, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblEnero = new Label { Content = Convert.ToDecimal(detalle.enero).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblFebrero = new Label { Content = Convert.ToDecimal(detalle.febrero).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMarzo = new Label { Content = Convert.ToDecimal(detalle.marzo).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAbril = new Label { Content = Convert.ToDecimal(detalle.abril).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMayo = new Label { Content = Convert.ToDecimal(detalle.mayo).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lbljunio = new Label { Content = Convert.ToDecimal(detalle.junio).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblJulio = new Label { Content = Convert.ToDecimal(detalle.julio).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAgosto = new Label { Content = Convert.ToDecimal(detalle.agosto).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblSeptiembre = new Label { Content = Convert.ToDecimal(detalle.septiembre).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblOctubre = new Label { Content = Convert.ToDecimal(detalle.octubre).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblNoviembre = new Label { Content = Convert.ToDecimal(detalle.noviembre).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblDiciembre = new Label { Content = Convert.ToDecimal(detalle.diciembre).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        decimal totalKm = listaKMPre.FindAll(s => s.modalidad.subModalidad == detalle.modalidad.subModalidad).Sum(s => s.sumaAnual);
                        decimal totalLitro = listaLitroPre.FindAll(s => s.modalidad.subModalidad == detalle.modalidad.subModalidad).Sum(s => s.sumaAnual);

                        Label lblSuma = new Label { Content = Convert.ToDecimal(totalKm / totalLitro).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }
                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    var listaRendLitros = presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS).ToList();
                    var listaRendKM = presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).ToList();
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaRendLitros.Sum(s => s.enero) <= 0 ? 0 : (listaRendKM.Sum(s => s.enero) / listaRendLitros.Sum(s => s.enero))).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaRendLitros.Sum(s => s.febrero) <= 0 ? 0 : (listaRendKM.Sum(s => s.febrero) / listaRendLitros.Sum(s => s.febrero))).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaRendLitros.Sum(s => s.marzo) <= 0 ? 0 : (listaRendKM.Sum(s => s.marzo) / listaRendLitros.Sum(s => s.marzo))).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaRendLitros.Sum(s => s.abril) <= 0 ? 0 : (listaRendKM.Sum(s => s.abril) / listaRendLitros.Sum(s => s.abril))).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaRendLitros.Sum(s => s.mayo) <= 0 ? 0 : (listaRendKM.Sum(s => s.mayo) / listaRendLitros.Sum(s => s.mayo))).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaRendLitros.Sum(s => s.junio) <= 0 ? 0 : (listaRendKM.Sum(s => s.junio) / listaRendLitros.Sum(s => s.junio))).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaRendLitros.Sum(s => s.julio) <= 0 ? 0 : (listaRendKM.Sum(s => s.julio) / listaRendLitros.Sum(s => s.julio))).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaRendLitros.Sum(s => s.agosto) <= 0 ? 0 : (listaRendKM.Sum(s => s.agosto) / listaRendLitros.Sum(s => s.agosto))).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaRendLitros.Sum(s => s.septiembre) <= 0 ? 0 : (listaRendKM.Sum(s => s.septiembre) / listaRendLitros.Sum(s => s.septiembre))).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaRendLitros.Sum(s => s.octubre) <= 0 ? 0 : (listaRendKM.Sum(s => s.octubre) / listaRendLitros.Sum(s => s.octubre))).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaRendLitros.Sum(s => s.noviembre) <= 0 ? 0 : (listaRendKM.Sum(s => s.noviembre) / listaRendLitros.Sum(s => s.noviembre))).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaRendLitros.Sum(s => s.diciembre) <= 0 ? 0 : (listaRendKM.Sum(s => s.diciembre) / listaRendLitros.Sum(s => s.diciembre))).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    var sumaLitros = listaRendLitros.Sum(s => s.sumaAnual);
                    var sumaKM = listaRendKM.Sum(s => s.sumaAnual);
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(sumaKM / sumaLitros).ToString("N2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpRendimientoPresupuesto.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                List<DetallePresupuesto> listaDet = new List<DetallePresupuesto>();
                foreach (var item in listaPresupuestos)
                {
                    listaDet.AddRange(item.listaDetalles.FindAll(s => s.indicador == Indicador.RENDIMIENTO));
                }

                List<DetallePresupuesto> listaDetTotalKm = new List<DetallePresupuesto>();
                foreach (var item in listaPresupuestos)
                {
                    listaDetTotalKm.AddRange(item.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS));
                }
                List<DetallePresupuesto> listaDetTotalLitros = new List<DetallePresupuesto>();
                foreach (var item in listaPresupuestos)
                {
                    listaDetTotalLitros.AddRange(item.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS));
                }


                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetTotalLitros.Sum(s => s.enero) <= 0 ? 0 : listaDetTotalKm.Sum(s => s.enero) / listaDetTotalLitros.Sum(s => s.enero)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetTotalLitros.Sum(s => s.febrero) <= 0 ? 0 : listaDetTotalKm.Sum(s => s.febrero) / listaDetTotalLitros.Sum(s => s.febrero)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetTotalLitros.Sum(s => s.marzo) <= 0 ? 0 : listaDetTotalKm.Sum(s => s.marzo) / listaDetTotalLitros.Sum(s => s.marzo)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetTotalLitros.Sum(s => s.abril) <= 0 ? 0 : listaDetTotalKm.Sum(s => s.abril) / listaDetTotalLitros.Sum(s => s.abril)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetTotalLitros.Sum(s => s.mayo) <= 0 ? 0 : listaDetTotalKm.Sum(s => s.mayo) / listaDetTotalLitros.Sum(s => s.mayo)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetTotalLitros.Sum(s => s.junio) <= 0 ? 0 : listaDetTotalKm.Sum(s => s.junio) / listaDetTotalLitros.Sum(s => s.junio)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetTotalLitros.Sum(s => s.julio) <= 0 ? 0 : listaDetTotalKm.Sum(s => s.julio) / listaDetTotalLitros.Sum(s => s.julio)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetTotalLitros.Sum(s => s.agosto) <= 0 ? 0 : listaDetTotalKm.Sum(s => s.agosto) / listaDetTotalLitros.Sum(s => s.agosto)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetTotalLitros.Sum(s => s.septiembre) <= 0 ? 0 : listaDetTotalKm.Sum(s => s.septiembre) / listaDetTotalLitros.Sum(s => s.septiembre)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetTotalLitros.Sum(s => s.octubre) <= 0 ? 0 : listaDetTotalKm.Sum(s => s.octubre) / listaDetTotalLitros.Sum(s => s.octubre)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetTotalLitros.Sum(s => s.noviembre) <= 0 ? 0 : listaDetTotalKm.Sum(s => s.noviembre) / listaDetTotalLitros.Sum(s => s.noviembre)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetTotalLitros.Sum(s => s.diciembre) <= 0 ? 0 : listaDetTotalKm.Sum(s => s.diciembre) / listaDetTotalLitros.Sum(s => s.diciembre)).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                var sumKm = listaDetTotalKm.Sum(s => s.sumaAnual);
                var sumLt = listaDetTotalLitros.Sum(s => s.sumaAnual);
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(sumKm / sumLt).ToString("N2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpRendimientoPresupuesto.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        void crearDiferenciaRendimiento()
        {
            try
            {
                stpRendimientoRealPresupuesto.Children.Clear();
                decimal _sumEnero = 0, _sumFebrero = 0, _sumMarzo = 0, _sumAbril = 0, _sumMayo = 0, _sumJunio = 0, _sumJulio = 0, _sumAgosto = 0, _sumSeptiembre = 0, _sumOctubre = 0, _sumNoviembre = 0, _sumDiciembre = 0, _sumTotal = 0;
                foreach (var grupo in listaGrupoIndicadoresCombustible)
                {
                    Presupuesto presupuesto = listaPresupuestos.Find(s => s.cliente.clave == grupo.idCliente);
                    if (presupuesto == null) continue;

                    StackPanel stpPrincipal = new StackPanel { Tag = grupo, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = grupo.cliente, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };
                    decimal sumEnero = 0, sumFebrero = 0, sumMarzo = 0, sumAbril = 0, sumMayo = 0, sumJunio = 0, sumJulio = 0, sumAgosto = 0, sumSeptiembre = 0, sumOctubre = 0, sumNoviembre = 0, sumDiciembre = 0, sumTotal = 0;
                    foreach (var detalle in grupo.listaDetalles)
                    {
                        DetallePresupuesto detPres = presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.RENDIMIENTO).Find(s => s.modalidad.subModalidad == detalle.subModalidad);

                        decimal sumaAnual = 0;

                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label
                        {
                            Content = detalle.desModalidad,
                            Width = 120,
                            FontWeight = FontWeights.Medium,
                            FontSize = 11,
                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0)
                        };
                        decimal valor = Convert.ToDecimal(detalle.eneroRendimiento - (detPres == null ? 0 : detPres.enero));
                        sumaAnual += valor;
                        sumEnero += valor;
                        _sumEnero += valor;
                        Label lblEnero = new Label
                        {
                            Content = valor.ToString("N2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.febreroRendimiento - (detPres == null ? 0 : detPres.febrero));
                        sumaAnual += valor;
                        sumFebrero += valor;
                        _sumFebrero += valor;
                        Label lblFebrero = new Label
                        {
                            Content = valor.ToString("N2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.marzoRendimiento - (detPres == null ? 0 : detPres.marzo));
                        sumaAnual += valor;
                        sumMarzo += valor;
                        _sumMarzo += valor;
                        Label lblMarzo = new Label
                        {
                            Content = valor.ToString("N2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.abrilRendimiento - (detPres == null ? 0 : detPres.abril));
                        sumaAnual += valor;
                        sumAbril += valor;
                        _sumAbril += valor;
                        Label lblAbril = new Label
                        {
                            Content = valor.ToString("N2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.mayoRendimiento - (detPres == null ? 0 : detPres.mayo));
                        sumaAnual += valor;
                        sumMayo += valor;
                        _sumMayo += valor;
                        Label lblMayo = new Label
                        {
                            Content = valor.ToString("N2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.junioRendimiento - (detPres == null ? 0 : detPres.junio));
                        sumaAnual += valor;
                        sumJunio += valor;
                        _sumJunio += valor;
                        Label lbljunio = new Label
                        {
                            Content = valor.ToString("N2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.julioRendimiento - (detPres == null ? 0 : detPres.julio));
                        sumaAnual += valor;
                        sumJulio += valor;
                        _sumJulio += valor;
                        Label lblJulio = new Label
                        {
                            Content = valor.ToString("N2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.agostoRendimiento - (detPres == null ? 0 : detPres.agosto));
                        sumaAnual += valor;
                        sumAgosto += valor;
                        _sumAgosto += valor;
                        Label lblAgosto = new Label
                        {
                            Content = valor.ToString("N2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.septiembreRendimiento - (detPres == null ? 0 : detPres.septiembre));
                        sumaAnual += valor;
                        sumSeptiembre += valor;
                        _sumSeptiembre += valor;
                        Label lblSeptiembre = new Label
                        {
                            Content = valor.ToString("N2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.octubreRendimiento - (detPres == null ? 0 : detPres.octubre));
                        sumaAnual += valor;
                        sumOctubre += valor;
                        _sumOctubre += valor;
                        Label lblOctubre = new Label
                        {
                            Content = valor.ToString("N2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.noviembreRendimiento - (detPres == null ? 0 : detPres.noviembre));
                        sumaAnual += valor;
                        sumNoviembre += valor;
                        _sumNoviembre += valor;
                        Label lblNoviembre = new Label
                        {
                            Content = valor.ToString("N2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.diciembreRendimiento - (detPres == null ? 0 : detPres.diciembre));
                        sumaAnual += valor;
                        sumDiciembre += valor;
                        _sumDiciembre += valor;
                        Label lblDiciembre = new Label
                        {
                            Content = valor.ToString("N2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        var listaKmPre = presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).Find(s => s.modalidad.subModalidad == detalle.subModalidad);
                        var listaLitroPre = presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS).Find(s => s.modalidad.subModalidad == detalle.subModalidad);

                        decimal rendReal = detalle.sumaTotalLitros <= 0 ? 0 : detalle.sumaTotalKm / detalle.sumaTotalLitros;
                        decimal rendPre = detPres == null ? 0 : (listaLitroPre.sumaAnual <= 0 ? 0 : listaKmPre.sumaAnual / listaLitroPre.sumaAnual);
                        valor = rendReal - rendPre; //Convert.ToDecimal(detalle.diciembreRendimiento - (detPres == null ? 0 : detPres.diciembre));
                        sumTotal += sumaAnual;
                        _sumTotal += sumaAnual;
                        Label lblSuma = new Label
                        {
                            Content = valor.ToString("N2"),
                            Width = 100,
                            FontWeight = FontWeights.Bold,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }

                    decimal valorReal = 0m;
                    decimal valorPre = 0m;
                    decimal valorDif = valorReal - valorPre;

                    List<ReporteIndicadorCombustible> listaComReal = grupo.listaDetalles;

                    List<DetallePresupuesto> listaRendKM = presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).ToList();
                    List<DetallePresupuesto> listaRendLitros = presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS).ToList();

                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaComReal.Sum(s => s.eneroLitro) <= 0 ? 0 : listaComReal.Sum(s => s.eneroKm) / listaComReal.Sum(s => s.eneroLitro);
                    valorPre = listaRendLitros.Sum(s => s.enero) <= 0 ? 0 : listaRendKM.Sum(s => s.enero) / listaRendLitros.Sum(s => s.enero);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("N2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaComReal.Sum(s => s.febreroLitro) <= 0 ? 0 : listaComReal.Sum(s => s.febreroKm) / listaComReal.Sum(s => s.febreroLitro);
                    valorPre = listaRendLitros.Sum(s => s.febrero) <= 0 ? 0 : listaRendKM.Sum(s => s.febrero) / listaRendLitros.Sum(s => s.febrero);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("N2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaComReal.Sum(s => s.marzoLitro) <= 0 ? 0 : listaComReal.Sum(s => s.marzoKm) / listaComReal.Sum(s => s.marzoLitro);
                    valorPre = listaRendLitros.Sum(s => s.marzo) <= 0 ? 0 : listaRendKM.Sum(s => s.marzo) / listaRendLitros.Sum(s => s.marzo);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("N2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaComReal.Sum(s => s.abrilLitro) <= 0 ? 0 : listaComReal.Sum(s => s.abrilKm) / listaComReal.Sum(s => s.abrilLitro);
                    valorPre = listaRendLitros.Sum(s => s.abril) <= 0 ? 0 : listaRendKM.Sum(s => s.abril) / listaRendLitros.Sum(s => s.abril);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("N2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaComReal.Sum(s => s.mayoLitro) <= 0 ? 0 : listaComReal.Sum(s => s.mayoKm) / listaComReal.Sum(s => s.mayoLitro);
                    valorPre = listaRendLitros.Sum(s => s.mayo) <= 0 ? 0 : listaRendKM.Sum(s => s.mayo) / listaRendLitros.Sum(s => s.mayo);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("N2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaComReal.Sum(s => s.junioLitro) <= 0 ? 0 : listaComReal.Sum(s => s.junioKm) / listaComReal.Sum(s => s.junioLitro);
                    valorPre = listaRendLitros.Sum(s => s.junio) <= 0 ? 0 : listaRendKM.Sum(s => s.junio) / listaRendLitros.Sum(s => s.junio);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("N2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaComReal.Sum(s => s.julioLitro) <= 0 ? 0 : listaComReal.Sum(s => s.julioKm) / listaComReal.Sum(s => s.julioLitro);
                    valorPre = listaRendLitros.Sum(s => s.julio) <= 0 ? 0 : listaRendKM.Sum(s => s.julio) / listaRendLitros.Sum(s => s.julio);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("N2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaComReal.Sum(s => s.agostoLitro) <= 0 ? 0 : listaComReal.Sum(s => s.agostoKm) / listaComReal.Sum(s => s.agostoLitro);
                    valorPre = listaRendLitros.Sum(s => s.agosto) <= 0 ? 0 : listaRendKM.Sum(s => s.agosto) / listaRendLitros.Sum(s => s.agosto);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("N2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaComReal.Sum(s => s.septiembreLitro) <= 0 ? 0 : listaComReal.Sum(s => s.septiembreKm) / listaComReal.Sum(s => s.septiembreLitro);
                    valorPre = listaRendLitros.Sum(s => s.septiembre) <= 0 ? 0 : listaRendKM.Sum(s => s.septiembre) / listaRendLitros.Sum(s => s.septiembre);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("N2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaComReal.Sum(s => s.octubreLitro) <= 0 ? 0 : listaComReal.Sum(s => s.octubreKm) / listaComReal.Sum(s => s.octubreLitro);
                    valorPre = listaRendLitros.Sum(s => s.octubre) <= 0 ? 0 : listaRendKM.Sum(s => s.octubre) / listaRendLitros.Sum(s => s.octubre);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("N2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaComReal.Sum(s => s.noviembreLitro) <= 0 ? 0 : listaComReal.Sum(s => s.noviembreKm) / listaComReal.Sum(s => s.noviembreLitro);
                    valorPre = listaRendLitros.Sum(s => s.noviembre) <= 0 ? 0 : listaRendKM.Sum(s => s.noviembre) / listaRendLitros.Sum(s => s.noviembre);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("N2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaComReal.Sum(s => s.diciembreLitro) <= 0 ? 0 : listaComReal.Sum(s => s.diciembreKm) / listaComReal.Sum(s => s.diciembreLitro);
                    valorPre = listaRendLitros.Sum(s => s.diciembre) <= 0 ? 0 : listaRendKM.Sum(s => s.diciembre) / listaRendLitros.Sum(s => s.diciembre);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("N2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaComReal.Sum(s => s.sumaTotalLitros) <= 0 ? 0 : listaComReal.Sum(s => s.sumaTotalKm) / listaComReal.Sum(s => s.sumaTotalLitros);
                    valorPre = listaRendLitros.Sum(s => s.sumaAnual) <= 0 ? 0 : listaRendKM.Sum(s => s.sumaAnual) / listaRendLitros.Sum(s => s.sumaAnual);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("N2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpRendimientoRealPresupuesto.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                List<ReporteIndicadorCombustible> listaComReal2 = new List<ReporteIndicadorCombustible>();
                foreach (var item in listaGrupoIndicadoresCombustible)
                {
                    listaComReal2.AddRange(item.listaDetalles);
                }

                List<DetallePresupuesto> listaRendKM2 = new List<DetallePresupuesto>();
                List<DetallePresupuesto> listaRendLitros2 = new List<DetallePresupuesto>();
                foreach (var item in listaPresupuestos)
                {
                    listaRendKM2.AddRange(item.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).ToList());
                    listaRendLitros2.AddRange(item.listaDetalles.FindAll(s => s.indicador == Indicador.LITROS).ToList());
                }
                decimal varReal = 0m;
                decimal varPre = 0m;
                decimal varDif = 0m;

                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });

                varReal = listaComReal2.Sum(s => s.eneroLitro) <= 0 ? 0 : listaComReal2.Sum(s => s.eneroKm) / listaComReal2.Sum(s => s.eneroLitro);
                varPre = listaRendLitros2.Sum(s => s.enero) <= 0 ? 0 : listaRendKM2.Sum(s => s.enero) / listaRendLitros2.Sum(s => s.enero);
                varDif = Decimal.Round(varReal, 2) - Decimal.Round(varPre, 2);
                stpTotalesInt.Children.Add(new Label { Content = varDif.ToString("N2"), Foreground = (varDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                varReal = listaComReal2.Sum(s => s.febreroLitro) <= 0 ? 0 : listaComReal2.Sum(s => s.febreroKm) / listaComReal2.Sum(s => s.febreroLitro);
                varPre = listaRendLitros2.Sum(s => s.febrero) <= 0 ? 0 : listaRendKM2.Sum(s => s.febrero) / listaRendLitros2.Sum(s => s.febrero);
                varDif = Decimal.Round(varReal, 2) - Decimal.Round(varPre, 2);
                stpTotalesInt.Children.Add(new Label { Content = varDif.ToString("N2"), Foreground = (varDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                varReal = listaComReal2.Sum(s => s.marzoLitro) <= 0 ? 0 : listaComReal2.Sum(s => s.marzoKm) / listaComReal2.Sum(s => s.marzoLitro);
                varPre = listaRendLitros2.Sum(s => s.marzo) <= 0 ? 0 : listaRendKM2.Sum(s => s.marzo) / listaRendLitros2.Sum(s => s.marzo);
                varDif = Decimal.Round(varReal, 2) - Decimal.Round(varPre, 2);
                stpTotalesInt.Children.Add(new Label { Content = varDif.ToString("N2"), Foreground = (varDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                varReal = listaComReal2.Sum(s => s.abrilLitro) <= 0 ? 0 : listaComReal2.Sum(s => s.abrilKm) / listaComReal2.Sum(s => s.abrilLitro);
                varPre = listaRendLitros2.Sum(s => s.abril) <= 0 ? 0 : listaRendKM2.Sum(s => s.abril) / listaRendLitros2.Sum(s => s.abril);
                varDif = Decimal.Round(varReal, 2) - Decimal.Round(varPre, 2);
                stpTotalesInt.Children.Add(new Label { Content = varDif.ToString("N2"), Foreground = (varDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                varReal = listaComReal2.Sum(s => s.mayoLitro) <= 0 ? 0 : listaComReal2.Sum(s => s.mayoKm) / listaComReal2.Sum(s => s.mayoLitro);
                varPre = listaRendLitros2.Sum(s => s.mayo) <= 0 ? 0 : listaRendKM2.Sum(s => s.mayo) / listaRendLitros2.Sum(s => s.mayo);
                varDif = Decimal.Round(varReal, 2) - Decimal.Round(varPre, 2);
                stpTotalesInt.Children.Add(new Label { Content = varDif.ToString("N2"), Foreground = (varDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                varReal = listaComReal2.Sum(s => s.junioLitro) <= 0 ? 0 : listaComReal2.Sum(s => s.junioKm) / listaComReal2.Sum(s => s.junioLitro);
                varPre = listaRendLitros2.Sum(s => s.junio) <= 0 ? 0 : listaRendKM2.Sum(s => s.junio) / listaRendLitros2.Sum(s => s.junio);
                varDif = Decimal.Round(varReal, 2) - Decimal.Round(varPre, 2);
                stpTotalesInt.Children.Add(new Label { Content = varDif.ToString("N2"), Foreground = (varDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                varReal = listaComReal2.Sum(s => s.julioLitro) <= 0 ? 0 : listaComReal2.Sum(s => s.julioKm) / listaComReal2.Sum(s => s.julioLitro);
                varPre = listaRendLitros2.Sum(s => s.julio) <= 0 ? 0 : listaRendKM2.Sum(s => s.julio) / listaRendLitros2.Sum(s => s.julio);
                varDif = Decimal.Round(varReal, 2) - Decimal.Round(varPre, 2);
                stpTotalesInt.Children.Add(new Label { Content = varDif.ToString("N2"), Foreground = (varDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                varReal = listaComReal2.Sum(s => s.agostoLitro) <= 0 ? 0 : listaComReal2.Sum(s => s.agostoKm) / listaComReal2.Sum(s => s.agostoLitro);
                varPre = listaRendLitros2.Sum(s => s.agosto) <= 0 ? 0 : listaRendKM2.Sum(s => s.agosto) / listaRendLitros2.Sum(s => s.agosto);
                varDif = Decimal.Round(varReal, 2) - Decimal.Round(varPre, 2);
                stpTotalesInt.Children.Add(new Label { Content = varDif.ToString("N2"), Foreground = (varDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                varReal = listaComReal2.Sum(s => s.septiembreLitro) <= 0 ? 0 : listaComReal2.Sum(s => s.septiembreKm) / listaComReal2.Sum(s => s.septiembreLitro);
                varPre = listaRendLitros2.Sum(s => s.septiembre) <= 0 ? 0 : listaRendKM2.Sum(s => s.septiembre) / listaRendLitros2.Sum(s => s.septiembre);
                varDif = Decimal.Round(varReal, 2) - Decimal.Round(varPre, 2);
                stpTotalesInt.Children.Add(new Label { Content = varDif.ToString("N2"), Foreground = (varDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                varReal = listaComReal2.Sum(s => s.octubreLitro) <= 0 ? 0 : listaComReal2.Sum(s => s.octubreKm) / listaComReal2.Sum(s => s.octubreLitro);
                varPre = listaRendLitros2.Sum(s => s.octubre) <= 0 ? 0 : listaRendKM2.Sum(s => s.octubre) / listaRendLitros2.Sum(s => s.octubre);
                varDif = Decimal.Round(varReal, 2) - Decimal.Round(varPre, 2);
                stpTotalesInt.Children.Add(new Label { Content = varDif.ToString("N2"), Foreground = (varDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                varReal = listaComReal2.Sum(s => s.noviembreLitro) <= 0 ? 0 : listaComReal2.Sum(s => s.noviembreKm) / listaComReal2.Sum(s => s.noviembreLitro);
                varPre = listaRendLitros2.Sum(s => s.noviembre) <= 0 ? 0 : listaRendKM2.Sum(s => s.noviembre) / listaRendLitros2.Sum(s => s.noviembre);
                varDif = Decimal.Round(varReal, 2) - Decimal.Round(varPre, 2);
                stpTotalesInt.Children.Add(new Label { Content = varDif.ToString("N2"), Foreground = (varDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                varReal = listaComReal2.Sum(s => s.diciembreLitro) <= 0 ? 0 : listaComReal2.Sum(s => s.diciembreKm) / listaComReal2.Sum(s => s.diciembreLitro);
                varPre = listaRendLitros2.Sum(s => s.diciembre) <= 0 ? 0 : listaRendKM2.Sum(s => s.diciembre) / listaRendLitros2.Sum(s => s.diciembre);
                varDif = Decimal.Round(varReal, 2) - Decimal.Round(varPre, 2);
                stpTotalesInt.Children.Add(new Label { Content = varDif.ToString("N2"), Foreground = (varDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                varReal = listaComReal2.Sum(s => s.sumaTotalLitros) <= 0 ? 0 : listaComReal2.Sum(s => s.sumaTotalKm) / listaComReal2.Sum(s => s.sumaTotalLitros);
                varPre = listaRendLitros2.Sum(s => s.sumaAnual) <= 0 ? 0 : listaRendKM2.Sum(s => s.sumaAnual) / listaRendLitros2.Sum(s => s.sumaAnual);
                varDif = Decimal.Round(varReal, 2) - Decimal.Round(varPre, 2);
                stpTotalesInt.Children.Add(new Label { Content = varDif.ToString("N2"), Foreground = (varDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpRendimientoRealPresupuesto.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        ///////////////////////////////////////////////////////

        void crearGrupoIndicadoresPrecioKm(List<GrupoIndicadoresViajes> listaIndicadores)
        {
            foreach (var grupo in listaIndicadores)
            {
                GrupoIndicadoresCombustible grupoCombustible = listaGrupoIndicadoresCombustible.Find(s => s.idCliente == grupo.idCliente);

                foreach (var detalle in grupo.listaDetalles)
                {
                    ReporteIndicadorCombustible detCombustible = grupoCombustible.listaDetalles.Find(s => s.subModalidad == detalle.subModalidad);
                    if (detCombustible == null) continue;

                    detalle.eneroPrecioKm = detCombustible.eneroKm <= 0 ? 0 : (detalle.eneroVen / detCombustible.eneroKm);
                    detalle.febreroPrecioKm = detCombustible.febreroKm <= 0 ? 0 : (detalle.febreroVen / detCombustible.febreroKm);
                    detalle.marzoPrecioKm = detCombustible.marzoKm <= 0 ? 0 : (detalle.marzoVen / detCombustible.marzoKm);
                    detalle.abrilPrecioKm = detCombustible.abrilKm <= 0 ? 0 : (detalle.abrilVen / detCombustible.abrilKm);
                    detalle.mayoPrecioKm = detCombustible.mayoKm <= 0 ? 0 : (detalle.mayoVen / detCombustible.mayoKm);
                    detalle.junioPrecioKm = detCombustible.junioKm <= 0 ? 0 : (detalle.junioVen / detCombustible.junioKm);
                    detalle.julioPrecioKm = detCombustible.julioKm <= 0 ? 0 : (detalle.julioVen / detCombustible.julioKm);
                    detalle.agostoPrecioKm = detCombustible.agostoKm <= 0 ? 0 : (detalle.agostoVen / detCombustible.agostoKm);
                    detalle.septiembrePrecioKm = detCombustible.septiembreKm <= 0 ? 0 : (detalle.septiembreVen / detCombustible.septiembreKm);
                    detalle.octubrePrecioKm = detCombustible.octubreKm <= 0 ? 0 : (detalle.octubreVen / detCombustible.octubreKm);
                    detalle.noviembrePrecioKm = detCombustible.noviembreKm <= 0 ? 0 : (detalle.noviembreVen / detCombustible.noviembreKm);
                    detalle.diciembrePrecioKm = detCombustible.diciembreKm <= 0 ? 0 : (detalle.diciembreVen / detCombustible.diciembreKm);
                }

            }
        }
        private void crearControlesPreciosKmReales(List<GrupoIndicadoresViajes> listaIndicadores)
        {
            try
            {
                stpPrecioKmReal.Children.Clear();
                foreach (var grupo in listaIndicadores)
                {

                    StackPanel stpPrincipal = new StackPanel { Tag = grupo, Orientation = Orientation.Horizontal };
                    Label lblOperacion = new Label { Content = grupo.cliente, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };
                    StackPanel stpSecundario = new StackPanel { };

                    foreach (var detalle in grupo.listaDetalles)
                    {

                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label { Content = detalle.desModalidad, Width = 120, FontWeight = FontWeights.Medium, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblEnero = new Label { Content = Convert.ToDecimal(detalle.eneroPrecioKm).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblFebrero = new Label { Content = Convert.ToDecimal(detalle.febreroPrecioKm).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMarzo = new Label { Content = Convert.ToDecimal(detalle.marzoPrecioKm).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAbril = new Label { Content = Convert.ToDecimal(detalle.abrilPrecioKm).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMayo = new Label { Content = Convert.ToDecimal(detalle.mayoPrecioKm).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lbljunio = new Label { Content = Convert.ToDecimal(detalle.junioPrecioKm).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblJulio = new Label { Content = Convert.ToDecimal(detalle.julioPrecioKm).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAgosto = new Label { Content = Convert.ToDecimal(detalle.agostoPrecioKm).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblSeptiembre = new Label { Content = Convert.ToDecimal(detalle.septiembrePrecioKm).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblOctubre = new Label { Content = Convert.ToDecimal(detalle.octubrePrecioKm).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblNoviembre = new Label { Content = Convert.ToDecimal(detalle.noviembrePrecioKm).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblDiciembre = new Label { Content = Convert.ToDecimal(detalle.diciembrePrecioKm).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        ReporteIndicadorCombustible combustible = listaGrupoIndicadoresCombustible.Find(s => s.idCliente == grupo.idCliente).listaDetalles.Find(s => s.subModalidad == detalle.subModalidad);

                        var totalVentas = detalle.sumaTotalVentas;
                        var totalKm = combustible == null ? 0 : combustible.sumaTotalKm;
                        var toral = Decimal.Round((totalKm <= 0 ? 0 : totalVentas / totalKm), 2);
                        Label lblSuma = new Label { Content = Convert.ToDecimal(toral).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }
                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    var listaIndCombustible = listaGrupoIndicadoresCombustible.FindAll(s => s.idCliente == grupo.idCliente);

                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaIndCombustible.Sum(s => s.sumEneroKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.eneroVen) / listaIndCombustible.Sum(s => s.sumEneroKm)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaIndCombustible.Sum(s => s.sumFebreroKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.febreroVen) / listaIndCombustible.Sum(s => s.sumFebreroKm)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaIndCombustible.Sum(s => s.sumMarzoKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.marzoVen) / listaIndCombustible.Sum(s => s.sumMarzoKm)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaIndCombustible.Sum(s => s.sumAbrilKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.abrilVen) / listaIndCombustible.Sum(s => s.sumAbrilKm)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaIndCombustible.Sum(s => s.sumMayoKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.mayoVen) / listaIndCombustible.Sum(s => s.sumMayoKm)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaIndCombustible.Sum(s => s.sumJunioKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.junioVen) / listaIndCombustible.Sum(s => s.sumJunioKm)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaIndCombustible.Sum(s => s.sumJulioKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.julioVen) / listaIndCombustible.Sum(s => s.sumJulioKm)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaIndCombustible.Sum(s => s.sumAgostoKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.agostoVen) / listaIndCombustible.Sum(s => s.sumAgostoKm)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaIndCombustible.Sum(s => s.sumSeptiembreKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.septiembreVen) / listaIndCombustible.Sum(s => s.sumSeptiembreKm)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaIndCombustible.Sum(s => s.sumOctubreKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.octubreVen) / listaIndCombustible.Sum(s => s.sumOctubreKm)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaIndCombustible.Sum(s => s.sumNoviembreKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.noviembreVen) / listaIndCombustible.Sum(s => s.sumNoviembreKm)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaIndCombustible.Sum(s => s.sumDiciembreKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.diciembreVen) / listaIndCombustible.Sum(s => s.sumDiciembreKm)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaIndCombustible.Sum(s => s.sumTotalKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.sumaTotalVentas) / listaIndCombustible.Sum(s => s.sumTotalKm)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpPrecioKmReal.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                List<ReporteIndicadorViajes> listaViajes = new List<ReporteIndicadorViajes>();
                foreach (var item in listaIndicadores)
                {
                    listaViajes.AddRange(item.listaDetalles);
                }

                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadoresCombustible.Sum(s => s.eneroKm) <= 0 ? 0 : (listaViajes.Sum(s => s.eneroVen) / listaIndicadoresCombustible.Sum(s => s.eneroKm))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadoresCombustible.Sum(s => s.febreroKm) <= 0 ? 0 : (listaViajes.Sum(s => s.febreroVen) / listaIndicadoresCombustible.Sum(s => s.febreroKm))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadoresCombustible.Sum(s => s.marzoKm) <= 0 ? 0 : (listaViajes.Sum(s => s.marzoVen) / listaIndicadoresCombustible.Sum(s => s.marzoKm))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadoresCombustible.Sum(s => s.abrilKm) <= 0 ? 0 : (listaViajes.Sum(s => s.abrilVen) / listaIndicadoresCombustible.Sum(s => s.abrilKm))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadoresCombustible.Sum(s => s.mayoKm) <= 0 ? 0 : (listaViajes.Sum(s => s.mayoVen) / listaIndicadoresCombustible.Sum(s => s.mayoKm))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadoresCombustible.Sum(s => s.junioKm) <= 0 ? 0 : (listaViajes.Sum(s => s.junioVen) / listaIndicadoresCombustible.Sum(s => s.junioKm))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadoresCombustible.Sum(s => s.julioKm) <= 0 ? 0 : (listaViajes.Sum(s => s.julioVen) / listaIndicadoresCombustible.Sum(s => s.julioKm))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadoresCombustible.Sum(s => s.agostoKm) <= 0 ? 0 : (listaViajes.Sum(s => s.agostoVen) / listaIndicadoresCombustible.Sum(s => s.agostoKm))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadoresCombustible.Sum(s => s.septiembreKm) <= 0 ? 0 : (listaViajes.Sum(s => s.septiembreVen) / listaIndicadoresCombustible.Sum(s => s.septiembreKm))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadoresCombustible.Sum(s => s.octubreKm) <= 0 ? 0 : (listaViajes.Sum(s => s.octubreVen) / listaIndicadoresCombustible.Sum(s => s.octubreKm))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadoresCombustible.Sum(s => s.noviembreKm) <= 0 ? 0 : (listaViajes.Sum(s => s.noviembreVen) / listaIndicadoresCombustible.Sum(s => s.noviembreKm))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadoresCombustible.Sum(s => s.diciembreKm) <= 0 ? 0 : (listaViajes.Sum(s => s.diciembreVen) / listaIndicadoresCombustible.Sum(s => s.diciembreKm))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaIndicadoresCombustible.Sum(s => s.sumaTotalKm) <= 0 ? 0 : (listaViajes.Sum(s => s.sumaTotalVentas) / listaIndicadoresCombustible.Sum(s => s.sumaTotalKm))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpPrecioKmReal.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void crearControlesPrecioKmPresupuesto(List<Presupuesto> listaPresupuestos)
        {
            try
            {
                stpPrecioKmPresupuesto.Children.Clear();
                foreach (var presupuesto in listaPresupuestos)
                {
                    StackPanel stpPrincipal = new StackPanel { Tag = presupuesto, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = presupuesto.cliente.nombre, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };

                    foreach (var detalle in presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.PRECIO_KM))
                    {
                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label { Content = detalle.modalidad.descripcion, Width = 120, FontWeight = FontWeights.Medium, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblEnero = new Label { Content = Convert.ToDecimal(detalle.enero).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblFebrero = new Label { Content = Convert.ToDecimal(detalle.febrero).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMarzo = new Label { Content = Convert.ToDecimal(detalle.marzo).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAbril = new Label { Content = Convert.ToDecimal(detalle.abril).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblMayo = new Label { Content = Convert.ToDecimal(detalle.mayo).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lbljunio = new Label { Content = Convert.ToDecimal(detalle.junio).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblJulio = new Label { Content = Convert.ToDecimal(detalle.julio).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblAgosto = new Label { Content = Convert.ToDecimal(detalle.agosto).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblSeptiembre = new Label { Content = Convert.ToDecimal(detalle.septiembre).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblOctubre = new Label { Content = Convert.ToDecimal(detalle.octubre).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblNoviembre = new Label { Content = Convert.ToDecimal(detalle.noviembre).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };
                        Label lblDiciembre = new Label { Content = Convert.ToDecimal(detalle.diciembre).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        var venta = presupuesto.listaDetalles.Find(s => s.indicador == Indicador.VENTAS && s.modalidad.subModalidad == detalle.modalidad.subModalidad);
                        var kilometro = presupuesto.listaDetalles.Find(s => s.indicador == Indicador.KILOMETROS && s.modalidad.subModalidad == detalle.modalidad.subModalidad);

                        var val = decimal.Round((kilometro.sumaAnual <= 0 ? 0 : venta.sumaAnual / kilometro.sumaAnual), 2);

                        Label lblSuma = new Label { Content = Convert.ToDecimal(val).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, HorizontalContentAlignment = HorizontalAlignment.Right, FontSize = 11, VerticalContentAlignment = VerticalAlignment.Center, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0) };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }
                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    List<Presupuesto> listaPresupuestosTotal = new List<Presupuesto>();
                    listaPresupuestosTotal = listaPresupuestos.FindAll(s => s.cliente.clave == presupuesto.cliente.clave);
                    List<DetallePresupuesto> listaKm = new List<DetallePresupuesto>();// listaPresupuestos.FindAll(s => s.cliente.clave == presupuesto.cliente.clave);
                    foreach (var item in listaPresupuestosTotal)
                    {
                        listaKm.AddRange(item.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).ToList());
                    }
                    List<DetallePresupuesto> listaVentas = new List<DetallePresupuesto>();// listaIndicadorViajes.FindAll(s => s.idCliente == presupuesto.cliente.clave);
                    foreach (var item in listaPresupuestosTotal)
                    {
                        listaVentas.AddRange(item.listaDetalles.FindAll(s => s.indicador == Indicador.VENTAS).ToList());
                    }

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaKm.Sum(s => s.enero) <= 0 ? 0 : listaVentas.Sum(s => s.enero) / listaKm.Sum(s => s.enero)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaKm.Sum(s => s.febrero) <= 0 ? 0 : listaVentas.Sum(s => s.febrero) / listaKm.Sum(s => s.febrero)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaKm.Sum(s => s.marzo) <= 0 ? 0 : listaVentas.Sum(s => s.marzo) / listaKm.Sum(s => s.marzo)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaKm.Sum(s => s.abril) <= 0 ? 0 : listaVentas.Sum(s => s.abril) / listaKm.Sum(s => s.abril)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaKm.Sum(s => s.mayo) <= 0 ? 0 : listaVentas.Sum(s => s.mayo) / listaKm.Sum(s => s.mayo)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaKm.Sum(s => s.junio) <= 0 ? 0 : listaVentas.Sum(s => s.junio) / listaKm.Sum(s => s.junio)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaKm.Sum(s => s.julio) <= 0 ? 0 : listaVentas.Sum(s => s.julio) / listaKm.Sum(s => s.julio)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaKm.Sum(s => s.agosto) <= 0 ? 0 : listaVentas.Sum(s => s.agosto) / listaKm.Sum(s => s.agosto)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaKm.Sum(s => s.septiembre) <= 0 ? 0 : listaVentas.Sum(s => s.septiembre) / listaKm.Sum(s => s.septiembre)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaKm.Sum(s => s.octubre) <= 0 ? 0 : listaVentas.Sum(s => s.octubre) / listaKm.Sum(s => s.octubre)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaKm.Sum(s => s.noviembre) <= 0 ? 0 : listaVentas.Sum(s => s.noviembre) / listaKm.Sum(s => s.noviembre)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaKm.Sum(s => s.diciembre) <= 0 ? 0 : listaVentas.Sum(s => s.diciembre) / listaKm.Sum(s => s.diciembre)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });
                    stpTotal.Children.Add(new Label { Content = Convert.ToDecimal(listaKm.Sum(s => s.sumaAnual) <= 0 ? 0 : listaVentas.Sum(s => s.sumaAnual) / listaKm.Sum(s => s.sumaAnual)).ToString("C2"), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpPrecioKmPresupuesto.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                List<DetallePresupuesto> listaDet = new List<DetallePresupuesto>();
                foreach (var item in listaPresupuestos)
                {
                    listaDet.AddRange(item.listaDetalles.FindAll(s => s.indicador == Indicador.PRECIO_KM));
                }
                List<DetallePresupuesto> listaDetalle = new List<DetallePresupuesto>();
                foreach (var pres in listaPresupuestos)
                {
                    listaDetalle.AddRange(pres.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS));
                }
                List<DetallePresupuesto> listaDetalleVentas = new List<DetallePresupuesto>();
                foreach (var pres in listaPresupuestos)
                {
                    listaDetalleVentas.AddRange(pres.listaDetalles.FindAll(s => s.indicador == Indicador.VENTAS));
                }
                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetalle.Sum(s => s.enero) <= 0 ? 0 : (listaDetalleVentas.Sum(s => s.enero) / listaDetalle.Sum(s => s.enero))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetalle.Sum(s => s.febrero) <= 0 ? 0 : (listaDetalleVentas.Sum(s => s.febrero) / listaDetalle.Sum(s => s.febrero))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetalle.Sum(s => s.marzo) <= 0 ? 0 : (listaDetalleVentas.Sum(s => s.marzo) / listaDetalle.Sum(s => s.marzo))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetalle.Sum(s => s.abril) <= 0 ? 0 : (listaDetalleVentas.Sum(s => s.abril) / listaDetalle.Sum(s => s.abril))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetalle.Sum(s => s.mayo) <= 0 ? 0 : (listaDetalleVentas.Sum(s => s.mayo) / listaDetalle.Sum(s => s.mayo))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetalle.Sum(s => s.junio) <= 0 ? 0 : (listaDetalleVentas.Sum(s => s.junio) / listaDetalle.Sum(s => s.junio))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetalle.Sum(s => s.julio) <= 0 ? 0 : (listaDetalleVentas.Sum(s => s.julio) / listaDetalle.Sum(s => s.julio))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetalle.Sum(s => s.agosto) <= 0 ? 0 : (listaDetalleVentas.Sum(s => s.agosto) / listaDetalle.Sum(s => s.agosto))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetalle.Sum(s => s.septiembre) <= 0 ? 0 : (listaDetalleVentas.Sum(s => s.septiembre) / listaDetalle.Sum(s => s.septiembre))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetalle.Sum(s => s.octubre) <= 0 ? 0 : (listaDetalleVentas.Sum(s => s.octubre) / listaDetalle.Sum(s => s.octubre))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetalle.Sum(s => s.noviembre) <= 0 ? 0 : (listaDetalleVentas.Sum(s => s.noviembre) / listaDetalle.Sum(s => s.noviembre))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = Convert.ToDecimal(listaDetalle.Sum(s => s.diciembre) <= 0 ? 0 : (listaDetalleVentas.Sum(s => s.diciembre) / listaDetalle.Sum(s => s.diciembre))).ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                var sumVentas = listaDetalleVentas.Sum(s => s.sumaAnual);
                var sumKm = listaDetalle.Sum(s => s.sumaAnual);
                var di = decimal.Round((sumKm <= 0 ? 0 : sumVentas/sumKm), 2);
                stpTotalesInt.Children.Add(new Label { Content = di.ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpPrecioKmPresupuesto.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        void crearDiferenciaPrecioKm()
        {
            try
            {
                stpPrecioKmRealPresupuesto.Children.Clear();
                decimal _sumEnero = 0, _sumFebrero = 0, _sumMarzo = 0, _sumAbril = 0, _sumMayo = 0, _sumJunio = 0, _sumJulio = 0, _sumAgosto = 0, _sumSeptiembre = 0, _sumOctubre = 0, _sumNoviembre = 0, _sumDiciembre = 0, _sumTotal = 0;
                foreach (var grupo in listaGrupoIndicadoresViajes)
                {
                    Presupuesto presupuesto = listaPresupuestos.Find(s => s.cliente.clave == grupo.idCliente);
                    if (presupuesto == null) continue;

                    StackPanel stpPrincipal = new StackPanel { Tag = grupo, Orientation = Orientation.Horizontal };

                    Label lblOperacion = new Label { Content = grupo.cliente, Width = 260, FontSize = 11, FontWeight = FontWeights.Bold, BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 0, 1, 1) };

                    StackPanel stpSecundario = new StackPanel { };
                    decimal sumEnero = 0, sumFebrero = 0, sumMarzo = 0, sumAbril = 0, sumMayo = 0, sumJunio = 0, sumJulio = 0, sumAgosto = 0, sumSeptiembre = 0, sumOctubre = 0, sumNoviembre = 0, sumDiciembre = 0, sumTotal = 0;
                    foreach (var detalle in grupo.listaDetalles)
                    {
                        DetallePresupuesto detPres = presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.PRECIO_KM).Find(s => s.modalidad.subModalidad == detalle.subModalidad);

                        decimal sumaAnual = 0;

                        StackPanel stpDetalles = new StackPanel { Orientation = Orientation.Horizontal };

                        Label lblModalidad = new Label
                        {
                            Content = detalle.desModalidad,
                            Width = 120,
                            FontWeight = FontWeights.Medium,
                            FontSize = 11,
                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0)
                        };
                        decimal valor = Convert.ToDecimal(detalle.eneroPrecioKm - (detPres == null ? 0 : detPres.enero));
                        sumaAnual += valor;
                        sumEnero += valor;
                        _sumEnero += valor;
                        Label lblEnero = new Label
                        {
                            Content = valor.ToString("C2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.febreroPrecioKm - (detPres == null ? 0 : detPres.febrero));
                        sumaAnual += valor;
                        sumFebrero += valor;
                        _sumFebrero += valor;
                        Label lblFebrero = new Label
                        {
                            Content = valor.ToString("C2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.marzoPrecioKm - (detPres == null ? 0 : detPres.marzo));
                        sumaAnual += valor;
                        sumMarzo += valor;
                        _sumMarzo += valor;
                        Label lblMarzo = new Label
                        {
                            Content = valor.ToString("C2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.abrilPrecioKm - (detPres == null ? 0 : detPres.abril));
                        sumaAnual += valor;
                        sumAbril += valor;
                        _sumAbril += valor;
                        Label lblAbril = new Label
                        {
                            Content = valor.ToString("C2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.mayoPrecioKm - (detPres == null ? 0 : detPres.mayo));
                        sumaAnual += valor;
                        sumMayo += valor;
                        _sumMayo += valor;
                        Label lblMayo = new Label
                        {
                            Content = valor.ToString("C2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.junioPrecioKm - (detPres == null ? 0 : detPres.junio));
                        sumaAnual += valor;
                        sumJunio += valor;
                        _sumJunio += valor;
                        Label lbljunio = new Label
                        {
                            Content = valor.ToString("C2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.julioPrecioKm - (detPres == null ? 0 : detPres.julio));
                        sumaAnual += valor;
                        sumJulio += valor;
                        _sumJulio += valor;
                        Label lblJulio = new Label
                        {
                            Content = valor.ToString("C2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.agostoPrecioKm - (detPres == null ? 0 : detPres.agosto));
                        sumaAnual += valor;
                        sumAgosto += valor;
                        _sumAgosto += valor;
                        Label lblAgosto = new Label
                        {
                            Content = valor.ToString("C2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.septiembrePrecioKm - (detPres == null ? 0 : detPres.septiembre));
                        sumaAnual += valor;
                        sumSeptiembre += valor;
                        _sumSeptiembre += valor;
                        Label lblSeptiembre = new Label
                        {
                            Content = valor.ToString("C2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.octubrePrecioKm - (detPres == null ? 0 : detPres.octubre));
                        sumaAnual += valor;
                        sumOctubre += valor;
                        _sumOctubre += valor;
                        Label lblOctubre = new Label
                        {
                            Content = valor.ToString("C2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.noviembrePrecioKm - (detPres == null ? 0 : detPres.noviembre));
                        sumaAnual += valor;
                        sumNoviembre += valor;
                        _sumNoviembre += valor;
                        Label lblNoviembre = new Label
                        {
                            Content = valor.ToString("C2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        valor = Convert.ToDecimal(detalle.diciembrePrecioKm - (detPres == null ? 0 : detPres.diciembre));
                        sumaAnual += valor;
                        sumDiciembre += valor;
                        _sumDiciembre += valor;
                        Label lblDiciembre = new Label
                        {
                            Content = valor.ToString("C2"),
                            Width = 100,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = valor < 0 ? Brushes.Red : Brushes.Black
                        };

                        sumTotal += sumaAnual;
                        _sumTotal += sumaAnual;
                        Label lblSuma = new Label
                        {
                            Content = sumaAnual.ToString("C2"),
                            Width = 100,
                            FontWeight = FontWeights.Bold,
                            HorizontalContentAlignment = HorizontalAlignment.Right,
                            FontSize = 11,
                            VerticalContentAlignment = VerticalAlignment.Center,

                            BorderBrush = Brushes.Black,
                            BorderThickness = new Thickness(1, 0, 1, 0),
                            Foreground = sumaAnual < 0 ? Brushes.Red : Brushes.Black
                        };

                        stpDetalles.Children.Add(lblModalidad);
                        stpDetalles.Children.Add(lblEnero);
                        stpDetalles.Children.Add(lblFebrero);
                        stpDetalles.Children.Add(lblMarzo);
                        stpDetalles.Children.Add(lblAbril);
                        stpDetalles.Children.Add(lblMayo);
                        stpDetalles.Children.Add(lbljunio);
                        stpDetalles.Children.Add(lblJulio);
                        stpDetalles.Children.Add(lblAgosto);
                        stpDetalles.Children.Add(lblSeptiembre);
                        stpDetalles.Children.Add(lblOctubre);
                        stpDetalles.Children.Add(lblNoviembre);
                        stpDetalles.Children.Add(lblDiciembre);
                        stpDetalles.Children.Add(lblSuma);

                        stpSecundario.Children.Add(stpDetalles);


                    }

                    Border border = new Border { BorderBrush = Brushes.Black, BorderThickness = new Thickness(0, 1, 0, 1) };
                    StackPanel stpTotal = new StackPanel { Orientation = Orientation.Horizontal };

                    decimal valorReal = 0m;
                    decimal valorPre = 0m;
                    decimal valorDif = 0m;


                    List<ReporteIndicadorViajes> listaVentas = grupo.listaDetalles;
                    #region listasReal
                    var listaIndCombustible = listaGrupoIndicadoresCombustible.FindAll(s => s.idCliente == grupo.idCliente);
                    #endregion

                    #region ListasPresupuestos
                    List<Presupuesto> listaPresupuestosTotal = new List<Presupuesto>();
                    listaPresupuestosTotal = listaPresupuestos.FindAll(s => s.cliente.clave == presupuesto.cliente.clave);
                    List<DetallePresupuesto> listaKm = new List<DetallePresupuesto>();// listaPresupuestos.FindAll(s => s.cliente.clave == presupuesto.cliente.clave);
                    foreach (var item in listaPresupuestosTotal)
                    {
                        listaKm.AddRange(item.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS).ToList());
                    }
                    List<DetallePresupuesto> listaVentasPre = new List<DetallePresupuesto>();// listaIndicadorViajes.FindAll(s => s.idCliente == presupuesto.cliente.clave);
                    foreach (var item in listaPresupuestosTotal)
                    {
                        listaVentasPre.AddRange(item.listaDetalles.FindAll(s => s.indicador == Indicador.VENTAS).ToList());
                    }
                    #endregion

                    stpTotal.Children.Add(new Label { Content = "Total", Width = 120, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaIndCombustible.Sum(s => s.sumEneroKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.eneroVen) / listaIndCombustible.Sum(s => s.sumEneroKm);
                    valorPre = listaKm.Sum(s => s.enero) <= 0 ? 0 : listaVentasPre.Sum(s => s.enero) / listaKm.Sum(s => s.enero);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("C2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaIndCombustible.Sum(s => s.sumFebreroKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.febreroVen) / listaIndCombustible.Sum(s => s.sumFebreroKm);
                    valorPre = listaKm.Sum(s => s.febrero) <= 0 ? 0 : listaVentasPre.Sum(s => s.febrero) / listaKm.Sum(s => s.febrero);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("C2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaIndCombustible.Sum(s => s.sumMarzoKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.marzoVen) / listaIndCombustible.Sum(s => s.sumFebreroKm);
                    valorPre = listaKm.Sum(s => s.marzo) <= 0 ? 0 : listaVentasPre.Sum(s => s.marzo) / listaKm.Sum(s => s.marzo);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("C2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaIndCombustible.Sum(s => s.sumAbrilKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.abrilVen) / listaIndCombustible.Sum(s => s.sumAbrilKm);
                    valorPre = listaKm.Sum(s => s.abril) <= 0 ? 0 : listaVentasPre.Sum(s => s.abril) / listaKm.Sum(s => s.abril);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("C2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaIndCombustible.Sum(s => s.sumMayoKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.mayoVen) / listaIndCombustible.Sum(s => s.sumMayoKm);
                    valorPre = listaKm.Sum(s => s.mayo) <= 0 ? 0 : listaVentasPre.Sum(s => s.mayo) / listaKm.Sum(s => s.mayo);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("C2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaIndCombustible.Sum(s => s.sumJunioKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.junioVen) / listaIndCombustible.Sum(s => s.sumJunioKm);
                    valorPre = listaKm.Sum(s => s.junio) <= 0 ? 0 : listaVentasPre.Sum(s => s.junio) / listaKm.Sum(s => s.junio);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("C2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaIndCombustible.Sum(s => s.sumJulioKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.julioVen) / listaIndCombustible.Sum(s => s.sumJulioKm);
                    valorPre = listaKm.Sum(s => s.junio) <= 0 ? 0 : listaVentasPre.Sum(s => s.junio) / listaKm.Sum(s => s.junio);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("C2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaIndCombustible.Sum(s => s.sumAgostoKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.agostoVen) / listaIndCombustible.Sum(s => s.sumAgostoKm);
                    valorPre = listaKm.Sum(s => s.agosto) <= 0 ? 0 : listaVentasPre.Sum(s => s.agosto) / listaKm.Sum(s => s.agosto);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("C2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaIndCombustible.Sum(s => s.sumSeptiembreKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.septiembreVen) / listaIndCombustible.Sum(s => s.sumSeptiembreKm);
                    valorPre = listaKm.Sum(s => s.septiembre) <= 0 ? 0 : listaVentasPre.Sum(s => s.septiembre) / listaKm.Sum(s => s.septiembre);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("C2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaIndCombustible.Sum(s => s.sumOctubreKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.octubreVen) / listaIndCombustible.Sum(s => s.sumOctubreKm);
                    valorPre = listaKm.Sum(s => s.octubre) <= 0 ? 0 : listaVentasPre.Sum(s => s.octubre) / listaKm.Sum(s => s.octubre);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("C2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaIndCombustible.Sum(s => s.sumNoviembreKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.noviembreVen) / listaIndCombustible.Sum(s => s.sumNoviembreKm);
                    valorPre = listaKm.Sum(s => s.noviembre) <= 0 ? 0 : listaVentasPre.Sum(s => s.noviembre) / listaKm.Sum(s => s.noviembre);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = sumNoviembre.ToString("C2"), Foreground = (sumNoviembre < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaIndCombustible.Sum(s => s.sumDiciembreKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.diciembreVen) / listaIndCombustible.Sum(s => s.sumDiciembreKm);
                    valorPre = listaKm.Sum(s => s.diciembre) <= 0 ? 0 : listaVentasPre.Sum(s => s.diciembre) / listaKm.Sum(s => s.diciembre);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("C2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    valorReal = listaIndCombustible.Sum(s => s.sumTotalKm) <= 0 ? 0 : grupo.listaDetalles.Sum(s => s.sumaTotalVentas) / listaIndCombustible.Sum(s => s.sumTotalKm);
                    valorPre = listaKm.Sum(s => s.sumaAnual) <= 0 ? 0 : listaVentasPre.Sum(s => s.sumaAnual) / listaKm.Sum(s => s.sumaAnual);
                    valorDif = valorReal - valorPre;
                    stpTotal.Children.Add(new Label { Content = valorDif.ToString("C2"), Foreground = (valorDif < 0 ? Brushes.Red : Brushes.Black), Width = 100, HorizontalContentAlignment = HorizontalAlignment.Right, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), FontSize = 11, FontWeight = FontWeights.Bold });

                    border.Child = stpTotal;

                    stpSecundario.Children.Add(border);

                    stpPrincipal.Children.Add(lblOperacion);
                    stpPrincipal.Children.Add(stpSecundario);

                    stpPrecioKmRealPresupuesto.Children.Add(stpPrincipal);
                }

                StackPanel stpTotales = new StackPanel { Orientation = Orientation.Horizontal };
                stpTotales.Children.Add(new Label { Content = string.Empty, Width = 259 });
                StackPanel stpTotalesInt = new StackPanel { Orientation = Orientation.Horizontal };

                stpTotalesInt.Children.Add(new Label { Content = "Gran Total", Width = 120, FontWeight = FontWeights.Bold, FontSize = 11, HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumEnero.ToString("C2"), Foreground = (_sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumFebrero.ToString("C2"), Foreground = (_sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumMarzo.ToString("C2"), Foreground = (_sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumAbril.ToString("C2"), Foreground = (_sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumMayo.ToString("C2"), Foreground = (_sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumJunio.ToString("C2"), Foreground = (_sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumJulio.ToString("C2"), Foreground = (_sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumAgosto.ToString("C2"), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumSeptiembre.ToString("C2"), Foreground = (_sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumOctubre.ToString("C2"), Foreground = (_sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumNoviembre.ToString("C2"), Foreground = (_sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumDiciembre.ToString("C2"), Foreground = (_sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });
                stpTotalesInt.Children.Add(new Label { Content = _sumTotal.ToString("C2"), Foreground = (_sumEnero < 0 ? Brushes.Red : Brushes.Black), Width = 100, FontWeight = FontWeights.Bold, FontSize = 11, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1, 0, 1, 0), HorizontalContentAlignment = HorizontalAlignment.Right });

                stpTotales.Children.Add(new Border { Child = stpTotalesInt, BorderBrush = Brushes.Black, BorderThickness = new Thickness(1) });

                stpPrecioKmRealPresupuesto.Children.Add(stpTotales);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
    }
    public class GrupoIndicadoresViajes
    {
        public int idCliente { get; set; }
        public string cliente { get; set; }
        public List<ReporteIndicadorViajes> listaDetalles { get; set; }
        public List<ReporteIndicadorCombustible> listaDetallesCombustible { get; set; }
        /////////////////////////////////////////////////////////////////////////////////
        public decimal sumEneroTon => listaDetalles.Sum(s => s.eneroTon);
        public decimal sumFebreroTon => listaDetalles.Sum(s => s.febreroTon);
        public decimal sumMarzoTon => listaDetalles.Sum(s => s.marzoTon);
        public decimal sumAbrilTon => listaDetalles.Sum(s => s.abrilTon);
        public decimal sumMayoTon => listaDetalles.Sum(s => s.mayoTon);
        public decimal sumJunioTon => listaDetalles.Sum(s => s.junioTon);
        public decimal sumJulioTon => listaDetalles.Sum(s => s.julioTon);
        public decimal sumAgostoTon => listaDetalles.Sum(s => s.agostoTon);
        public decimal sumSeptiembreTon => listaDetalles.Sum(s => s.septiembreTon);
        public decimal sumOctubreTon => listaDetalles.Sum(s => s.octubreTon);
        public decimal sumNoviembreTon => listaDetalles.Sum(s => s.noviembreTon);
        public decimal sumDiciembreTon => listaDetalles.Sum(s => s.diciembreTon);
        public decimal sumTotalTon => listaDetalles.Sum(s => s.sumaTotalToneladas);
        /////////////////////////////////////////////////////////////////////////////////
        public decimal sumEneroViaje => listaDetalles.Sum(s => s.eneroViaje);
        public decimal sumFebreroViaje => listaDetalles.Sum(s => s.febreroViaje);
        public decimal sumMarzoViaje => listaDetalles.Sum(s => s.marzoViaje);
        public decimal sumAbrilViaje => listaDetalles.Sum(s => s.abrilViaje);
        public decimal sumMayoViaje => listaDetalles.Sum(s => s.mayoViaje);
        public decimal sumJunioViaje => listaDetalles.Sum(s => s.junioViaje);
        public decimal sumJulioViaje => listaDetalles.Sum(s => s.julioViaje);
        public decimal sumAgostoViaje => listaDetalles.Sum(s => s.agostoViaje);
        public decimal sumSeptiembreViaje => listaDetalles.Sum(s => s.septiembreViaje);
        public decimal sumOctubreViaje => listaDetalles.Sum(s => s.octubreViaje);
        public decimal sumNoviembreViaje => listaDetalles.Sum(s => s.noviembreViaje);
        public decimal sumDiciembreViaje => listaDetalles.Sum(s => s.diciembreViaje);
        public decimal sumTotalViaje => listaDetalles.Sum(s => s.sumaTotalViajes);
        /////////////////////////////////////////////////////////////////////////////////
        public decimal sumEneroVen => listaDetalles.Sum(s => s.eneroVen);
        public decimal sumFebreroVen => listaDetalles.Sum(s => s.febreroVen);
        public decimal sumMarzoVen => listaDetalles.Sum(s => s.marzoVen);
        public decimal sumAbrilVen => listaDetalles.Sum(s => s.abrilVen);
        public decimal sumMayoVen => listaDetalles.Sum(s => s.mayoVen);
        public decimal sumJunioVen => listaDetalles.Sum(s => s.junioVen);
        public decimal sumJulioVen => listaDetalles.Sum(s => s.julioVen);
        public decimal sumAgostoVen => listaDetalles.Sum(s => s.agostoVen);
        public decimal sumSeptiembreVen => listaDetalles.Sum(s => s.septiembreVen);
        public decimal sumOctubreVen => listaDetalles.Sum(s => s.octubreVen);
        public decimal sumNoviembreVen => listaDetalles.Sum(s => s.noviembreVen);
        public decimal sumDiciembreVen => listaDetalles.Sum(s => s.diciembreVen);
        public decimal sumTotalVen => listaDetalles.Sum(s => s.sumaTotalVentas);
        /////////////////////////////////////////////////////////////////////////////////
        public decimal sumEneroPrecioKm => listaDetalles.Sum(s => s.eneroPrecioKm);
        public decimal sumFebreroPrecioKm => listaDetalles.Sum(s => s.febreroPrecioKm);
        public decimal sumMarzoPrecioKm => listaDetalles.Sum(s => s.marzoPrecioKm);
        public decimal sumAbrilPrecioKm => listaDetalles.Sum(s => s.abrilPrecioKm);
        public decimal sumMayoPrecioKm => listaDetalles.Sum(s => s.mayoPrecioKm);
        public decimal sumJunioPrecioKm => listaDetalles.Sum(s => s.junioPrecioKm);
        public decimal sumJulioPrecioKm => listaDetalles.Sum(s => s.julioPrecioKm);
        public decimal sumAgostoPrecioKm => listaDetalles.Sum(s => s.agostoPrecioKm);
        public decimal sumSeptiembrePrecioKm => listaDetalles.Sum(s => s.septiembrePrecioKm);
        public decimal sumOctubrePrecioKm => listaDetalles.Sum(s => s.octubrePrecioKm);
        public decimal sumNoviembrePrecioKm => listaDetalles.Sum(s => s.noviembrePrecioKm);
        public decimal sumDiciembrePrecioKm => listaDetalles.Sum(s => s.diciembrePrecioKm);
        public decimal sumTotalPrecioKm => listaDetalles.Sum(s => s.sumaTotalPrecioKm);
    }
    public class GrupoIndicadoresCombustible
    {
        public int idCliente { get; set; }
        public string cliente { get; set; }
        public List<ReporteIndicadorCombustible> listaDetalles { get; set; }
        /////////////////////////////////////////////////////////////////////////////////
        public decimal sumEneroKm => listaDetalles.Sum(s => s.eneroKm);
        public decimal sumFebreroKm => listaDetalles.Sum(s => s.febreroKm);
        public decimal sumMarzoKm => listaDetalles.Sum(s => s.marzoKm);
        public decimal sumAbrilKm => listaDetalles.Sum(s => s.abrilKm);
        public decimal sumMayoKm => listaDetalles.Sum(s => s.mayoKm);
        public decimal sumJunioKm => listaDetalles.Sum(s => s.junioKm);
        public decimal sumJulioKm => listaDetalles.Sum(s => s.julioKm);
        public decimal sumAgostoKm => listaDetalles.Sum(s => s.agostoKm);
        public decimal sumSeptiembreKm => listaDetalles.Sum(s => s.septiembreKm);
        public decimal sumOctubreKm => listaDetalles.Sum(s => s.octubreKm);
        public decimal sumNoviembreKm => listaDetalles.Sum(s => s.noviembreKm);
        public decimal sumDiciembreKm => listaDetalles.Sum(s => s.diciembreKm);
        public decimal sumTotalKm => listaDetalles.Sum(s => s.sumaTotalKm);
        /////////////////////////////////////////////////////////////////////////////////       
        public decimal sumEneroLitro => listaDetalles.Sum(s => s.eneroLitro);
        public decimal sumFebreroLitro => listaDetalles.Sum(s => s.febreroLitro);
        public decimal sumMarzoLitro => listaDetalles.Sum(s => s.marzoLitro);
        public decimal sumAbrilLitro => listaDetalles.Sum(s => s.abrilLitro);
        public decimal sumMayoLitro => listaDetalles.Sum(s => s.mayoLitro);
        public decimal sumJunioLitro => listaDetalles.Sum(s => s.junioLitro);
        public decimal sumJulioLitro => listaDetalles.Sum(s => s.julioLitro);
        public decimal sumAgostoLitro => listaDetalles.Sum(s => s.agostoLitro);
        public decimal sumSeptiembreLitro => listaDetalles.Sum(s => s.septiembreLitro);
        public decimal sumOctubreLitro => listaDetalles.Sum(s => s.octubreLitro);
        public decimal sumNoviembreLitro => listaDetalles.Sum(s => s.noviembreLitro);
        public decimal sumDiciembreLitro => listaDetalles.Sum(s => s.diciembreLitro);
        public decimal sumTotalLitro => listaDetalles.Sum(s => s.sumaTotalLitros);
        /////////////////////////////////////////////////////////////////////////////////
        public decimal sumEneroRendimiento => listaDetalles.Sum(s => s.eneroRendimiento);
        public decimal sumFebreroRendimiento => listaDetalles.Sum(s => s.febreroRendimiento);
        public decimal sumMarzoRendimiento => listaDetalles.Sum(s => s.marzoRendimiento);
        public decimal sumAbrilRendimiento => listaDetalles.Sum(s => s.abrilRendimiento);
        public decimal sumMayoRendimiento => listaDetalles.Sum(s => s.mayoRendimiento);
        public decimal sumJunioRendimiento => listaDetalles.Sum(s => s.junioRendimiento);
        public decimal sumJulioRendimiento => listaDetalles.Sum(s => s.julioRendimiento);
        public decimal sumAgostoRendimiento => listaDetalles.Sum(s => s.agostoRendimiento);
        public decimal sumSeptiembreRendimiento => listaDetalles.Sum(s => s.septiembreRendimiento);
        public decimal sumOctubreRendimiento => listaDetalles.Sum(s => s.octubreRendimiento);
        public decimal sumNoviembreRendimiento => listaDetalles.Sum(s => s.noviembreRendimiento);
        public decimal sumDiciembreRendimiento => listaDetalles.Sum(s => s.diciembreRendimiento);
        public decimal sumTotalRendimiento => (sumEneroRendimiento + sumFebreroRendimiento + sumMarzoRendimiento + sumAbrilRendimiento +
            sumMayoRendimiento + sumJunioRendimiento + sumJulioRendimiento + sumAgostoRendimiento + sumSeptiembreRendimiento + sumOctubreRendimiento + sumNoviembreRendimiento + sumDiciembreRendimiento);
    }
}
