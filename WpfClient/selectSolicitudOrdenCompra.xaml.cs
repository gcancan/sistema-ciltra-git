﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for selectSolicitudOrdenCompra.xaml
    /// </summary>
    public partial class selectSolicitudOrdenCompra : Window
    {
        string parametro = string.Empty;
        public selectSolicitudOrdenCompra()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtFind.Text = parametro;
            lvlSolicitud.KeyDown += LvlSolicitud_KeyDown; 
        }

        private void LvlSolicitud_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }
        bool isllantas = false;
        public SolicitudOrdenCompra buscarTodasLasSolicitudes(bool isllantas = false)
        {
            try
            {
                this.isllantas = isllantas;
                buscarTodas();
                bool? resp = ShowDialog();
                if (resp.Value && lvlSolicitud.SelectedItem != null)
                {
                    return (SolicitudOrdenCompra)((ListViewItem)lvlSolicitud.SelectedItem).Content;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }

        private void buscarTodas()
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult resp = new SolicitudOrdenCompraSvc().getSolicitudOrdComAllOrById();
                validarResp(resp);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void validarResp(OperationResult resp)
        {
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    mapLista((List<SolicitudOrdenCompra>)resp.result);
                    break;
                case ResultTypes.error:
                    ImprimirMensaje.imprimir(resp);
                    break;
                case ResultTypes.warning:
                    ImprimirMensaje.imprimir(resp);
                    break;
                case ResultTypes.recordNotFound:
                    ImprimirMensaje.imprimir(resp);
                    break;
                default:
                    break;
            }
        }

        private void mapLista(List<SolicitudOrdenCompra> result)
        {
            lvlSolicitud.Items.Clear();
            List<ListViewItem> lvlList = new List<ListViewItem>();
            foreach (SolicitudOrdenCompra item in result)
            {
                if (isllantas)
                {
                    continue;
                }
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                lvl.FontWeight = FontWeights.Medium;
                if (item.estatus == "ACTIVO")
                {
                    lvl.Foreground = Brushes.SteelBlue;
                }
                else if (item.estatus == "CANCELADO")
                {
                    lvl.Foreground = Brushes.Red;
                }
                else if (item.estatus == "COTIZADO")
                {
                    lvl.Foreground = Brushes.Blue;
                }
                else if (item.estatus == "APROVADO")
                {
                    lvl.Foreground = Brushes.Green;
                }

                lvlList.Add(lvl);
            }
            lvlSolicitud.ItemsSource = lvlList;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlSolicitud.ItemsSource);
            view.Filter = UserFilter;
            lvlSolicitud.Focus();
        }

        private bool UserFilter(object item)
        {
            if (string.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((SolicitudOrdenCompra)(item as ListViewItem).Content).idSolicitudOrdenCompra.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((SolicitudOrdenCompra)(item as ListViewItem).Content).fecha.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((SolicitudOrdenCompra)(item as ListViewItem).Content).estatus.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((SolicitudOrdenCompra)(item as ListViewItem).Content).empresa.nombre.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }
        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlSolicitud.ItemsSource).Refresh();
        }
        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                lvlSolicitud.Focus();
            }
        }
    }
}
