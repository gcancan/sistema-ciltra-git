﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editCitasMantenimiento.xaml
    /// </summary>
    public partial class editCitasMantenimiento : ViewBase
    {
        public editCitasMantenimiento()
        {
            InitializeComponent();
           
        }
        controlCitasMantenimiento control = null;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            control = new controlCitasMantenimiento(mainWindow, null);
            stpContenedor.Children.Add(control);
            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            control.nuevo();
        }
        public override OperationResult guardar()
        {
            var resp = control.guardarForm();
            if (resp.typeResult == ResultTypes.success)
            {
                base.guardar();
            }
            return resp;
        }
        public override void buscar()
        {
            CitaMantenimiento cita = control.buscarForm();
            if (cita != null)
            {
                base.buscar();
                if (cita.fechaCita < DateTime.Now)
                {
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                }
            }
        }
        public override OperationResult eliminar()
        {
            var resp = control.eliminarForm();
            if (resp.typeResult == ResultTypes.success)
            {
                base.eliminar();
            }
            return resp;
        }
    }
}
