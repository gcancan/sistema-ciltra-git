﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para catMotivosCancelacion.xaml
    /// </summary>
    public partial class catMotivosCancelacion : ViewBase
    {
        public catMotivosCancelacion()
        {
            InitializeComponent();
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            cbxProcesos.ItemsSource = Enum.GetValues(typeof(enumProcesos));
            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            mapform(null);
        }
        void mapform(MotivoCancelacion motivoCancelacion)
        {
            try
            {
                if (motivoCancelacion != null)
                {
                    ctrClave.Tag = motivoCancelacion;
                    ctrClave.valor = motivoCancelacion.idMotivoCancelacion;
                    txtDescripcion.Text = motivoCancelacion.descripcion;
                    chcActivo.IsChecked = motivoCancelacion.activo;
                    chcRequiere.IsChecked = motivoCancelacion.requiereObservacion;
                    cbxProcesos.SelectedItems.Clear();
                    foreach (var item in motivoCancelacion.listaProcesos)
                    {
                        cbxProcesos.SelectedItems.Add(item);
                    }
                }
                else
                {
                    ctrClave.Tag = null;
                    ctrClave.valor = 0;
                    txtDescripcion.Clear();
                    chcActivo.IsChecked = true;
                    chcRequiere.IsChecked = false;
                    cbxProcesos.SelectedItems.Clear();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
                mapform(null);
            }
        }
        MotivoCancelacion mapForm()
        {
            try
            {
                MotivoCancelacion motivo = new MotivoCancelacion
                {
                    idMotivoCancelacion = ctrClave.Tag == null ? 0 : (ctrClave.Tag as MotivoCancelacion).idMotivoCancelacion,
                    activo = chcActivo.IsChecked.Value,
                    descripcion = txtDescripcion.Text.Trim(),
                    usuario = mainWindow.usuario.nombreUsuario,
                    requiereObservacion = chcRequiere.IsChecked.Value,
                    listaProcesos = new List<enumProcesos>()
                };
                motivo.listaProcesos = cbxProcesos.SelectedItems.Cast<enumProcesos>().ToList();
            return motivo;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                MotivoCancelacion motivo = mapForm();
                if (motivo != null)
                {
                    var resp = new MotivoCancelacionSvc().saveMotivosCancelacion(ref motivo);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mapform(motivo);
                        base.guardar();
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult(ResultTypes.warning, "Ocurrio un error al leer la información de la pantalla");
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public override void buscar()
        {
            MotivoCancelacion motivo = new selectMotivosCancelacion().getMotivoCancelacion();
            if (motivo != null)
            {
                mapform(motivo);
                base.buscar();
            }
        }
    }
}
