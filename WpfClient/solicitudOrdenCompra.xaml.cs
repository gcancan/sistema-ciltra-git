﻿using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for solicitudOrdenCompra.xaml
    /// </summary>
    public partial class solicitudOrdenCompra : ViewBase
    {
        public solicitudOrdenCompra()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            nuevo();
            if (!buscarMedidasLlantas())
            {
                mainWindow.scrollContainer.IsEnabled = false;
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                return;
            }
            cbxEmpresas.loaded(mainWindow.empresa, true);
        }

        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);
        }

        private bool buscarLlantasProducto()
        {
            try
            {
                OperationResult resp = new LlantaSvc().getAllLLantasProducto();
                if (resp.typeResult == ResultTypes.success)
                {
                    return llenatPanel((List<LLantasProducto>)resp.result);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    return false;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return false;
            }
        }
        private bool buscarMedidasLlantas()
        {
            try
            {
                OperationResult resp = new LlantaSvc().getMedidasLlantas();
                if (resp.typeResult == ResultTypes.success)
                {
                    return llenatPanel((List<string>)resp.result);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    return false;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return false;
            }
        }

        private bool llenatPanel(List<string> result)
        {
            try
            {
                foreach (string medida in result)
                {
                    CheckBox chc = new CheckBox() { VerticalContentAlignment = VerticalAlignment.Center, Tag = medida };
                    StackPanel stpChc = new StackPanel { Orientation = Orientation.Horizontal };
                    chc.Unchecked += Chc_Unchecked;
                    Label lblMedida = new Label() { Content = medida, Width = 200, Name = "lblMedida" };
                    stpChc.Children.Add(lblMedida);


                    chc.Content = stpChc;
                    stpListTiposLlantas.Children.Add(chc);
                }
                return true;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return false;
            }
        }

        private bool llenatPanel(List<LLantasProducto> Lista)
        {
            try
            {
                foreach (LLantasProducto llProducto in Lista)
                {
                    CheckBox chc = new CheckBox() { VerticalContentAlignment = VerticalAlignment.Center, Tag = llProducto };
                    StackPanel stpChc = new StackPanel { Orientation = Orientation.Horizontal };
                    chc.Unchecked += Chc_Unchecked;
                    Label lblMarca = new Label() { Content = llProducto.marca.descripcion, Margin = new Thickness(2), Width = 200 };
                    stpChc.Children.Add(lblMarca);

                    Label lblDiseño = new Label() { Content = llProducto.diseño.descripcion, Margin = new Thickness(2), Width = 120, HorizontalContentAlignment = HorizontalAlignment.Center };
                    stpChc.Children.Add(lblDiseño);

                    Label lblMedida = new Label() { Content = llProducto.medida, Margin = new Thickness(2), Width = 120, HorizontalContentAlignment = HorizontalAlignment.Center };
                    stpChc.Children.Add(lblMedida);

                    Label lblProfundidad = new Label() { Content = llProducto.profundidad, Margin = new Thickness(2), Width = 120, HorizontalContentAlignment = HorizontalAlignment.Center };
                    stpChc.Children.Add(lblProfundidad);
                    chc.Content = stpChc;
                    stpListTiposLlantas.Children.Add(chc);
                }
                return true;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return false;
            }
        }

        private bool omitir = false;
        private void Chc_Unchecked(object sender, RoutedEventArgs e)
        {
            omitir = true;
            chcTodos.IsChecked = false;
            omitir = false;
        }
        private void chcTodos_Checked(object sender, RoutedEventArgs e)
        {
            if (stpListTiposLlantas.Children.Count > 0)
            {
                marcarDesmarcar(true);
            }
        }

        private void chcTodos_Unchecked(object sender, RoutedEventArgs e)
        {
            if (omitir)
            {
                return;
            }
            if (stpListTiposLlantas.Children.Count > 0)
            {
                marcarDesmarcar(false);
            }
        }
        public void marcarDesmarcar(bool check)
        {
            List<CheckBox> listaChecBox = stpListTiposLlantas.Children.Cast<CheckBox>().ToList();
            foreach (CheckBox chcBox in listaChecBox)
            {
                chcBox.IsChecked = check;
            }
        }
        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            //if (cbxEmpresas.empresaSelected == null)
            //{
            //    ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "Se tiene que elegir una empresa" });
            //    return;
            //}

            List<string> listaMedidas = new List<string>();
            List<CheckBox> listaChecBox = stpListTiposLlantas.Children.Cast<CheckBox>().ToList();
            foreach (CheckBox chcBox in listaChecBox)
            {
                if (chcBox.IsChecked.Value)
                {
                    listaMedidas.Add((string)chcBox.Tag);
                }
            }
            foreach (var item in listaMedidas)
            {
                StackPanel stp = new StackPanel() { Orientation = Orientation.Horizontal, Margin = new Thickness(0), VerticalAlignment = VerticalAlignment.Center };
                stp.Tag = item;
                Label lbl = new Label() { FontSize = 11, Margin = new Thickness(0), Width = 310, Tag = item, Content = item };
                stp.Children.Add(lbl);

                controlDecimal ctrDecimal = new controlDecimal();
                ctrDecimal.FontSize = 11;
                ctrDecimal.Margin = new Thickness(0);
                ctrDecimal.Width = 60;
                stp.Children.Add(ctrDecimal);

                Label lbluni = new Label() { FontSize = 11, Margin = new Thickness(0), Content = "Para:", FontWeight = FontWeights.Medium };
                stp.Children.Add(lbluni);

                controlUnidadesTransporte ctrUnidades = new controlUnidadesTransporte();
                ctrUnidades.loaded(etipoUniadBusqueda.TODAS, null);
                ctrUnidades.Margin = new Thickness(0);
                ctrUnidades.FontSize = 11;
                stp.Children.Add(ctrUnidades);

                Button btnCambiar = new Button() { Height = 28, FontSize = 8, Margin = new Thickness(0), Tag = item, Content = "CAMBIAR", Foreground = Brushes.White, Background = Brushes.SteelBlue, Width = 100 };
                btnCambiar.Click += BtnCambiar_Click;
                stp.Children.Add(btnCambiar);

                Button btnQuitar = new Button() { Height = 28, FontSize = 8, Margin = new Thickness(0), Tag = stp, Content = "QUITAR", Foreground = Brushes.White, Background = Brushes.Red, Width = 100 };
                btnQuitar.Click += Btn_Click;
                stp.Children.Add(btnQuitar);

                stpDetalles.Children.Add(stp);
            }

        }
        private void mapDetalles(List<DetalleSolicitudOrdenCompra> listaDetalles)
        {
            stpDetalles.Children.Clear();
            foreach (var item in listaDetalles)
            {
                StackPanel stp = new StackPanel() { Orientation = Orientation.Horizontal, Margin = new Thickness(0), VerticalAlignment = VerticalAlignment.Center };
                stp.Tag = item;
                Label lbl = new Label() { FontSize = 11, Margin = new Thickness(0), Width = 110, Tag = item, Content = item.medidaLlanta };
                stp.Children.Add(lbl);

                controlDecimal ctrDecimal = new controlDecimal();
                ctrDecimal.FontSize = 11;
                ctrDecimal.Margin = new Thickness(0);
                ctrDecimal.Width = 60;
                ctrDecimal.valor = item.cantidad;
                ctrDecimal.IsEnabled = false;
                stp.Children.Add(ctrDecimal);

                Label lbluni = new Label() { FontSize = 11, Margin = new Thickness(0), Content = "Para:", FontWeight = FontWeights.Medium };
                stp.Children.Add(lbluni);

                controlUnidadesTransporte ctrUnidades = new controlUnidadesTransporte();
                ctrUnidades.loaded(etipoUniadBusqueda.TODAS, cbxEmpresas.empresaSelected);
                ctrUnidades.Margin = new Thickness(0);
                ctrUnidades.FontSize = 11;
                ctrUnidades.unidadTransporte = item.unidadTrans;
                stp.Children.Add(ctrUnidades);

                stpDetalles.Children.Add(stp);
            }
        }
        void agregarOld()
        {
            if (cbxEmpresas.empresaSelected == null)
            {
                ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "Se tiene que elegir una empresa" });
                return;
            }
            List<LLantasProducto> listaProductos = new List<LLantasProducto>();
            List<CheckBox> listaChecBox = stpListTiposLlantas.Children.Cast<CheckBox>().ToList();
            foreach (CheckBox chcBox in listaChecBox)
            {
                if (chcBox.IsChecked.Value)
                {
                    listaProductos.Add((LLantasProducto)chcBox.Tag);
                }
            }

            foreach (var item in listaProductos)
            {
                //if (!existeProductoLista(item))
                //{

                StackPanel stp = new StackPanel() { Orientation = Orientation.Horizontal, Margin = new Thickness(0), VerticalAlignment = VerticalAlignment.Center };
                stp.Tag = item;
                Label lbl = new Label() { FontSize = 11, Margin = new Thickness(0), Width = 270, Tag = item, Content = item.marca.descripcion + " " + item.diseño.descripcion + " " + item.medida };
                stp.Children.Add(lbl);

                controlDecimal ctrDecimal = new controlDecimal();
                ctrDecimal.FontSize = 11;
                ctrDecimal.Margin = new Thickness(0);
                ctrDecimal.Width = 60;
                stp.Children.Add(ctrDecimal);

                Label lbluni = new Label() { FontSize = 11, Margin = new Thickness(0), Content = "Para:", FontWeight = FontWeights.Medium };
                stp.Children.Add(lbluni);

                controlUnidadesTransporte ctrUnidades = new controlUnidadesTransporte();
                ctrUnidades.loaded(etipoUniadBusqueda.TODAS, cbxEmpresas.empresaSelected);
                ctrUnidades.Margin = new Thickness(0);
                ctrUnidades.FontSize = 11;
                stp.Children.Add(ctrUnidades);

                Button btnCambiar = new Button() { Height = 28, FontSize = 8, Margin = new Thickness(0), Tag = item, Content = "CAMBIAR", Foreground = Brushes.White, Background = Brushes.SteelBlue, Width = 100 };
                btnCambiar.Click += BtnCambiar_Click;
                stp.Children.Add(btnCambiar);

                Button btnQuitar = new Button() { Height = 28, FontSize = 8, Margin = new Thickness(0), Tag = stp, Content = "QUITAR", Foreground = Brushes.White, Background = Brushes.Red, Width = 100 };
                btnQuitar.Click += Btn_Click;
                stp.Children.Add(btnQuitar);

                stpDetalles.Children.Add(stp);
                //}                
            }
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            int i = 0;
            Button btn = (Button)sender;
            StackPanel llPro = (StackPanel)btn.Tag;
            
                    stpDetalles.Children.Remove(llPro);
            
        }

        private void BtnCambiar_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            LLantasProducto llPro = (LLantasProducto)btn.Tag;
            List<StackPanel> listaSp = stpDetalles.Children.Cast<StackPanel>().ToList();
            foreach (StackPanel stp in listaSp)
            {
                if (((LLantasProducto)stp.Tag).idLLantasProducto == llPro.idLLantasProducto)
                {
                    List<object> listaObj = stp.Children.Cast<object>().ToList();
                    foreach (object item in listaObj)
                    {
                        if (item is controlUnidadesTransporte)
                        {
                            controlUnidadesTransporte ctr = (controlUnidadesTransporte)item;
                            ctr.limpiar();
                        }
                    }
                }
            }
        }

        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                SolicitudOrdenCompra solicitud = mapForm();
                if (solicitud == null)
                {
                    return new OperationResult { valor = 2, mensaje = "Ocurrio un error al intentar obtener los valores de la pantalla" };
                }
                var resp = new SolicitudOrdenCompraSvc().saveSolicitudOrdenCompra(ref solicitud, new List<int>());
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(solicitud);
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        public override OperationResult eliminar()
        {
            try
            {
                Cursor = Cursors.Wait;
                int idSolicitud = ((SolicitudOrdenCompra)txtFolioCompra.Tag).idSolicitudOrdenCompra;
                var resp = new SolicitudOrdenCompraSvc().cancelarSolicitudOrdCom(idSolicitud);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(null);
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public override void buscar()
        {
            SolicitudOrdenCompra solicitud = new selectSolicitudOrdenCompra().buscarTodasLasSolicitudes(true);
            mapForm(solicitud);
        }
        private void mapForm(SolicitudOrdenCompra solicitud)
        {
            if (solicitud != null)
            {
                txtFolioCompra.Tag = solicitud;
                txtFolioCompra.Text = solicitud.idSolicitudOrdenCompra.ToString();
                txtEstatus.Text = solicitud.estatus;
                txtObservaciones.Text = solicitud.obsevaciones;
                if (solicitud.estatus == "ACTIVO")
                {
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.eliminar | ToolbarCommands.cerrar);
                }
                else
                {
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                }
                mapDetalles(solicitud.listaDetalles);
                mainWindow.scrollContainer.IsEnabled = false;
            }
            else
            {
                txtFolioCompra.Tag = solicitud;
                txtFolioCompra.Clear();
                txtEstatus.Text = "NUEVO";
                txtObservaciones.Clear();
                mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.buscar | ToolbarCommands.cerrar);
                stpDetalles.Children.Clear();
                mainWindow.scrollContainer.IsEnabled = true;
            }
        }
        

        private SolicitudOrdenCompra mapForm()
        {
            try
            {
                SolicitudOrdenCompra solicitud = new SolicitudOrdenCompra
                {
                    idSolicitudOrdenCompra = txtFolioCompra.Tag == null ? 0 : ((SolicitudOrdenCompra)txtFolioCompra.Tag).idSolicitudOrdenCompra,
                    isLlantas = true,
                    empresa = cbxEmpresas.empresaSelected,
                    fecha = DateTime.Now,
                    obsevaciones = txtObservaciones.Text.Trim(),
                    usuario = mainWindow.inicio._usuario.nombreUsuario,
                    estatus = "ACTIVO"
                };
                solicitud.listaDetalles = new List<DetalleSolicitudOrdenCompra>();
                var resp = getDetalles();
                if (resp.typeResult == ResultTypes.success)
                {
                    solicitud.listaDetalles = (List<DetalleSolicitudOrdenCompra>)resp.result;
                    if (solicitud.listaDetalles.Count <= 0)
                    {
                        ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "NO HAY DATOS PARA GUARDAR" });
                        return null;
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    return null;
                }

                return solicitud;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private OperationResult getDetalles()
        {
            try
            {
                OperationResult resp = new OperationResult { valor = 0 };
                List<DetalleSolicitudOrdenCompra> lista = new List<DetalleSolicitudOrdenCompra>();

                foreach (StackPanel stpPrincipal in stpDetalles.Children.Cast<StackPanel>().ToList())
                {
                    DetalleSolicitudOrdenCompra detalle = new DetalleSolicitudOrdenCompra();
                    string medida = (string)stpPrincipal.Tag;
                    detalle.medidaLlanta = medida;
                    foreach (object item in stpPrincipal.Children.Cast<object>().ToList())
                    {
                        if (item is controlDecimal)
                        {
                            decimal cantidad = ((controlDecimal)item).valor;
                            if (cantidad <= 0)
                            {
                                resp.valor = 2;
                                resp.mensaje += " -*La cantidad no puede ser menor o igual a 0 para: " + medida + Environment.NewLine;
                            }
                            else
                            {
                                detalle.cantidad = Convert.ToInt32(cantidad);
                            }
                        }
                        if (item is controlUnidadesTransporte)
                        {
                            if (((controlUnidadesTransporte)item).unidadTransporte == null)
                            {
                                resp.valor = 2;
                                resp.mensaje += " -*No se selcciono la Unidad de Transporte para: " + medida + Environment.NewLine;
                            }
                            else
                            {
                                detalle.unidadTrans = ((controlUnidadesTransporte)item).unidadTransporte;
                            }
                        }
                    }
                    lista.Add(detalle);
                }
                resp.result = lista;
                return resp;

            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        private OperationResult getDetallesOld()
        {
            try
            {
                OperationResult resp = new OperationResult { valor = 0 };
                List<DetalleSolicitudOrdenCompra> lista = new List<DetalleSolicitudOrdenCompra>();

                foreach (StackPanel stpPrincipal in stpDetalles.Children.Cast<StackPanel>().ToList())
                {
                    DetalleSolicitudOrdenCompra detalle = new DetalleSolicitudOrdenCompra();
                    LLantasProducto llantaPro = (LLantasProducto)stpPrincipal.Tag;
                    detalle.productoLlanta = llantaPro;
                    foreach (object item in stpPrincipal.Children.Cast<object>().ToList())
                    {
                        if (item is controlDecimal)
                        {
                            decimal cantidad = ((controlDecimal)item).valor;
                            if (cantidad <= 0)
                            {
                                resp.valor = 2;
                                resp.mensaje += " -*La cantidad no puede ser menor o igual a 0 para: " + llantaPro.marca.descripcion + " " + llantaPro.diseño.descripcion
                                             + " " + llantaPro.medida + " " + llantaPro.profundidad + Environment.NewLine;
                            }
                            else
                            {
                                detalle.cantidad = Convert.ToInt32(cantidad);
                            }
                        }
                        if (item is controlUnidadesTransporte)
                        {
                            if (((controlUnidadesTransporte)item).unidadTransporte == null)
                            {
                                resp.valor = 2;
                                resp.mensaje += " -*No se selcciono la Unidad de Transporte para: " + llantaPro.marca.descripcion + " " + llantaPro.diseño.descripcion
                                             + " " + llantaPro.medida + " " + llantaPro.profundidad + Environment.NewLine;
                            }
                            else
                            {
                                detalle.unidadTrans = ((controlUnidadesTransporte)item).unidadTransporte;
                            }
                        }
                    }
                    lista.Add(detalle);
                }
                resp.result = lista;
                return resp;

            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        private void txtFolioCompra_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrEmpty(txtFolioCompra.Text.Trim()))
                {
                    int i = 0;
                    if (int.TryParse(txtFolioCompra.Text.Trim(), out i))
                    {
                        buscarSolicitudOrden(i);
                    }
                }
                else
                {
                    buscar();
                }
            }
        }

        private void buscarSolicitudOrden(int i)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new SolicitudOrdenCompraSvc().getSolicitudOrdComAllOrById(i);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm((SolicitudOrdenCompra)resp.result);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
