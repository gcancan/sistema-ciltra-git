﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editRutasDestinoGlobal.xaml
    /// </summary>
    public partial class editRutasDestinoGlobal : ViewBase
    {
        public editRutasDestinoGlobal()
        {
            InitializeComponent();
        }
        Usuario usuario = null;
        
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            Usuario usuario = mainWindow.usuario;
            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresa.loaded(usuario);

            var result = new EstadoSvc().getEstadosAllorById(0);
            if (result.typeResult == ResultTypes.success)
            {
                this.cbxEstado.ItemsSource = ((List<Estado>)result.result).OrderBy(s => s.nombre).ToList();
            }
            ctrEmpresa.empresaSelected = new Empresa { clave = 5};
            nuevo();
        }
        public override void nuevo()
        { 
            base.nuevo();
            stpAnticipos.Children.Clear();
            mapForm(null);
            
        }
        private void addAnticipo(Anticipo anticipo)
        {
            var list = stpAnticipos.Children.Cast<StackPanel>().ToList().Select(s => s.Tag as Anticipo).ToList();
            if (list.Exists(s => s.tipoAnticipo.idTipoAnticipo == anticipo.tipoAnticipo.idTipoAnticipo)) return;

            StackPanel stpContent = new StackPanel { Tag = anticipo, Orientation = Orientation.Horizontal };

            Label lbl = new Label { Content = anticipo.tipoAnticipo.anticipo, Width = 150, VerticalAlignment = VerticalAlignment.Center, FontWeight = FontWeights.Bold };

            controlDecimal ctrSencillo2Ejes = new controlDecimal { Name = "ctrSencillo2Ejes", Width = 125, Tag = anticipo, FontWeight = FontWeights.Medium };
            ctrSencillo2Ejes.txtDecimal.Tag = ctrSencillo2Ejes;            
            ctrSencillo2Ejes.txtDecimal.TextChanged += TxtDecimalSencillo2Ejes_TextChanged;
            ctrSencillo2Ejes.valor = anticipo.sencillo2Ejes;

            controlDecimal ctrSencillo3Ejes = new controlDecimal { Name = "ctrSencillo3Ejes", Width = 125, Tag = anticipo, FontWeight = FontWeights.Medium };
            ctrSencillo3Ejes.txtDecimal.Tag = ctrSencillo3Ejes;           
            ctrSencillo3Ejes.txtDecimal.TextChanged += TxtDecimalSencillo3Ejes_TextChanged;
            ctrSencillo3Ejes.valor = anticipo.sencillo3Ejes;

            controlDecimal ctrFull = new controlDecimal { Name = "ctrFull", Width = 125, Tag = anticipo, FontWeight = FontWeights.Medium };
            ctrFull.txtDecimal.Tag = ctrFull;            
            ctrFull.txtDecimal.TextChanged += TxtDecimalFull_TextChanged;
            ctrFull.valor = anticipo.full;

            stpContent.Children.Add(lbl);
            stpContent.Children.Add(ctrSencillo2Ejes);
            stpContent.Children.Add(ctrSencillo3Ejes);
            stpContent.Children.Add(ctrFull);

            stpAnticipos.Children.Add(stpContent);
        }

        private void TxtDecimalSencillo2Ejes_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    TextBox txtDecimal = sender as TextBox;
                    controlDecimal ctrDecimal = txtDecimal.Tag as controlDecimal;
                    Anticipo anticipo = ctrDecimal.Tag as Anticipo;
                    anticipo.sencillo2Ejes = ctrDecimal.valor;
                    //calcular();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void TxtDecimalSencillo3Ejes_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    TextBox txtDecimal = sender as TextBox;
                    controlDecimal ctrDecimal = txtDecimal.Tag as controlDecimal;
                    Anticipo anticipo = ctrDecimal.Tag as Anticipo;
                    anticipo.sencillo3Ejes = ctrDecimal.valor;
                    //calcular();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private void TxtDecimalFull_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    TextBox txtDecimal = sender as TextBox;
                    controlDecimal ctrDecimal = txtDecimal.Tag as controlDecimal;
                    Anticipo anticipo = ctrDecimal.Tag as Anticipo;
                    anticipo.full = ctrDecimal.valor;
                    //calcular();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }


        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (ctrEmpresa.empresaSelected != null)
                {
                    //Empresa empresa = ctrEmpresa.empresaSelected as Empresa;
                    //var result = new ClienteSvc().getClientesALLorById(empresa.clave, 0, true);
                    //if (result.typeResult == ResultTypes.success)
                    //{
                    //    this.cbxCliente.ItemsSource = ((List<Cliente>)result.result).OrderBy(s => s.nombre).ToList();
                    //}
                    //else
                    //{
                    //    cbxCliente.ItemsSource = null;
                    //}
                    buscarOrigenes(ctrEmpresa.empresaSelected.clave);
                }
                else
                {
                    cbxOrigen.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CbxCliente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (this.cbxCliente.SelectedItem == null)
            //{
            //    //this.btnImprimir.IsEnabled = false;
            //    cbxOrigen.ItemsSource = null;
            //}
            //else
            //{
            //    this.buscarOrigenes((this.cbxCliente.SelectedItem as Cliente).clave);
            //    //this.btnImprimir.IsEnabled = true;
            //}
        }

        private void buscarOrigenes(int clave)
        {
            try
            {
                this.cbxOrigen.ItemsSource = null;
                Cursor = Cursors.Wait;
                OperationResult resp = new ZonasSvc().getOrigenByIdEmpresa(clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<OrigenPegaso> list = ((List<OrigenPegaso>)resp.result).OrderBy(s => s.nombre).ToList();
                    this.cbxOrigen.ItemsSource = list;
                    if (list.Count == 1)
                    {
                        this.cbxOrigen.SelectedIndex = 0;
                    }
                }
                else if(resp.typeResult != ResultTypes.recordNotFound)
                {
                    imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                imprimir(exception);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BtnNuevoOrigen_Click(object sender, RoutedEventArgs e)
        {
            //if (cbxCliente.SelectedItem != null)
            //{
                OrigenPegaso origen = new addNuevoOrigen().saveNewOrigen(ctrEmpresa.empresaSelected);
                if (origen != null)
                {
                    if (cbxOrigen.ItemsSource == null)
                    {
                        cbxOrigen.ItemsSource = new List<OrigenPegaso>() { origen };
                    }
                    
                    List<OrigenPegaso> list = cbxOrigen.ItemsSource.Cast<OrigenPegaso>().ToList();
                    list.Add(origen);
                    cbxOrigen.ItemsSource = list.OrderBy(s => s.nombre).ToList();
                    cbxOrigen.SelectedItem = cbxOrigen.ItemsSource.Cast<OrigenPegaso>().ToList().Find(s => s.idOrigen == origen.idOrigen);
                }
            //}

        }

        private void CbxOrigen_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            calcularNombreRuta();
        }
        private void TxtNombreDestino_TextChanged(object sender, TextChangedEventArgs e)
        {
            calcularNombreRuta();
        }
        private void calcularNombreRuta()
        {
            OrigenPegaso origen = null;
            if (cbxOrigen.SelectedItem != null)
            {
                origen = cbxOrigen.SelectedItem as OrigenPegaso;
            }
            txtNombreRuta.Text = string.Format("{0}-{1}", (origen == null ? string.Empty : origen.nombre), txtNombreDestino.Text.Trim());
        }

        private void BtnBuscarCasetas_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<Caseta> list = new selectCasetas().getListaCasetas();
                if (list != null)
                {
                    this.llenarListaCasetas(list);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }
        private void llenarListaCasetas(List<Caseta> list)
        {
            
            foreach (Caseta caseta in list)
            {
                //if (this.lvlCasetas.Items.Cast<Caseta>().ToList<Caseta>().Find(s => s.idCaseta == caseta.idCaseta) == null)
                //{
                this.lvlCasetas.Items.Add(caseta);
                //}
            }
        }
        public override OperationResult guardar()
        {
            var val = validar();
            if (val.typeResult != ResultTypes.success) return val;
            Zona zona = mapForm();

            var resp = new ZonasSvc().saveRutasDestino(ref zona);
            if (resp.typeResult == ResultTypes.success)
            {
                mapForm(zona);
            }
            return resp;
        }
        public override OperationResult eliminar()
        {
            var val = validar();
            if (val.typeResult != ResultTypes.success) return val;
            Zona zona = mapForm();
            zona.activo = false;
            var resp = new ZonasSvc().saveRutasDestino(ref zona);
            if (resp.typeResult == ResultTypes.success)
            {
                mapForm(null);
                nuevo();
            }
            return resp;
        }
        public OperationResult validar()
        {
            try
            {
                OperationResult resp = new OperationResult(ResultTypes.success, "PARA CONTINUAR POR FAVOR:" + Environment.NewLine);
                if (ctrEmpresa.empresaSelected == null)
                {
                    resp.valor = 2;
                    resp.mensaje += "   * Seleccionar una Empresa" + Environment.NewLine;
                }

                //if (cbxCliente.SelectedItem == null)
                //{
                //    resp.valor = 2;
                //    resp.mensaje += "   * Seleccionar Un Cliente" + Environment.NewLine;
                //}

                if (ctrKm.valor <= 0)
                {
                    resp.valor = 2;
                    resp.mensaje += "   * Proporcionar El KM" + Environment.NewLine;
                }

                if (cbxEstado.SelectedItem == null)
                {
                    resp.valor = 2;
                    resp.mensaje += "   * Seleccionar Un Estado" + Environment.NewLine;
                }
                TimeSpan t;
                string[] strTiempo = txtTiempoRuta.Text.Split(':');
                if (!TimeSpan.TryParse(string.Format("{0}:{1}:{2}", strTiempo[0].Trim('_'), strTiempo[1].Trim('_'), strTiempo[2].Trim('_')), out t))
                {
                    resp.valor = 2;
                    resp.mensaje += "   * El Tiempo de Ruta No Es Valido" + Environment.NewLine;
                }

                if (cbxOrigen.SelectedItem == null)
                {
                    resp.valor = 2;
                    resp.mensaje += "   * Seleccionar Un Origen" + Environment.NewLine;
                }

                if (string.IsNullOrEmpty(txtNombreDestino.Text.Trim()))
                {
                    resp.valor = 2;
                    resp.mensaje += "   * Proporcionar El nombre Del Destino" + Environment.NewLine;
                }

                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        private Zona mapForm()
        {
            try
            {
                Zona zona = new Zona
                {
                    clave = ctrClave.Tag == null ? 0 : (ctrClave.Tag as Zona).clave,
                    estatus = chcActivo.IsChecked.Value,
                    clave_empresa = ctrEmpresa.empresaSelected.clave,
                    cliente = null,
                    claveCliente = null,// (cbxCliente.SelectedItem as Cliente).clave,
                    km = ctrKm.valor,
                    estado = cbxEstado.SelectedItem as Estado,
                    tiempoRetorno = txtTiempoRuta.Text,
                    origenPegaso = cbxOrigen.SelectedItem as OrigenPegaso,
                    descripcion = txtNombreDestino.Text.Trim(),
                    precioFijoSencillo = ctrPrecioFijoSencillo.valor,
                    precioFijoFull = ctrPrecioFijoFull.valor,
                    
                    sencillo_24 = ctrPagoOperadorSencillo.valor,
                    sencillo_30 = ctrPagoOperadorSencillo.valor,
                    sencillo_35 = ctrPagoOperadorSencillo.valor,
                    full = ctrPagoOperadorFull.valor,
                    viaje = true,
                    activo = chcActivo.IsChecked.Value
                };
                zona.listaCasetas = lvlCasetas.Items.Cast<Caseta>().ToList();
                zona.listaAnticipo = stpAnticipos.Children.Cast<StackPanel>().ToList().Select(s => s.Tag as Anticipo).ToList();

                return zona;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private void mapForm(Zona zona)
        {
            if (zona!= null)
            {
                ctrClave.Tag = zona;
                ctrClave.valor = zona.clave;
                chcActivo.IsChecked = zona.activo;
                ctrEmpresa.seleccionarEmpresa(new Empresa { clave = zona.clave_empresa });
                //cbxCliente.SelectedItem = cbxCliente.ItemsSource.Cast<Cliente>().ToList().Find(s => s.clave == zona.cliente.clave);
                ctrKm.valor = zona.km;
                cbxEstado.SelectedItem = cbxEstado.ItemsSource.Cast<Estado>().ToList().Find(s => s.idEstado == zona.estado.idEstado);
                txtTiempoRuta.Text = zona.tiempoRetorno;
                cbxOrigen.SelectedItem = cbxOrigen.ItemsSource.Cast<OrigenPegaso>().ToList().Find(s => s.idOrigen == zona.origenPegaso.idOrigen);
                txtNombreDestino.Text = zona.descripcion;
                ctrPrecioFijoSencillo.valor = zona.precioFijoSencillo;
                ctrPrecioFijoFull.valor = zona.precioFijoFull;
                ctrPagoOperadorSencillo.valor = zona.sencillo_24;
                ctrPagoOperadorFull.valor = zona.full;

                stpAnticipos.Children.Clear();
                foreach (var item in zona.listaAnticipo)
                {
                    addAnticipo(item);
                }
                lvlCasetas.Items.Clear();
                llenarListaCasetas(zona.listaCasetas);
            }
            else
            {
                ctrClave.Tag = null;
                ctrClave.valor = 0;
                chcActivo.IsChecked = true;
                ctrKm.valor = 0;
                txtTiempoRuta.Text = string.Format(@"000:00:00");
                ctrPrecioFijoSencillo.valor =0;
                ctrPrecioFijoFull.valor = 0;
                ctrPagoOperadorSencillo.valor =0;
                ctrPagoOperadorFull.valor = 0;
                txtNombreDestino.Clear();
                lvlCasetas.Items.Clear();
                llenarListaCasetas(new List<Caseta>());
            }

            var resp = new AnticipoSvc().getTipoAnticipos();
            if (resp.typeResult == ResultTypes.success)
            {
                foreach (var tipoAnticipo in resp.result as List<TipoAnticipo>)
                {
                    Anticipo anticipo = new Anticipo
                    {
                        tipoAnticipo = tipoAnticipo,
                        idAnticipo = 0,
                        idDestino = 0,
                        sencillo2Ejes = 0,
                        sencillo3Ejes = 0,
                        full = 0
                    };
                    addAnticipo(anticipo);
                }
            }
            else
            {
                imprimir(resp);
                this.IsEnabled = false;
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                return;
            }
        }
        public override void buscar()
        {
            //if (cbxCliente.SelectedItem == null) return;

            Zona resultado = new selectZonasGlobal("").findZonasByEmpresaCliente(ctrEmpresa.empresaSelected.clave, 0);
            if (resultado != null)
            {
                //var resp = new ZonasSvc().getAnticipos(resultado.clave);
                //if (resp.typeResult == ResultTypes.success)
                //{
                //    resultado.listaAnticipo = resp.result as List<Anticipo>;
                //}
                //else
                //{
                //    resultado.listaAnticipo = new List<Anticipo>();
                //}

                //var respCaseta = new ZonasSvc().getCasetasByDestino(resultado.clave);
                //if (respCaseta.typeResult == ResultTypes.success)
                //{
                //    resultado.listaCasetas = respCaseta.result as List<Caseta>;
                //}

                this.mapForm(resultado);
                base.buscar();
            }
        }

        private void BtnQuitarCasetas_Click(object sender, RoutedEventArgs e)
        {
            if (lvlCasetas.SelectedItem != null)
            {
                lvlCasetas.Items.Remove(lvlCasetas.SelectedItem);
            }
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult resp = new ZonasSvc().getZonasByEmpresaCliente(ctrEmpresa.empresaSelected.clave, 0);

                if (resp.typeResult == ResultTypes.success)
                {
                    var listaZonas = resp.result as List<Zona>;
                    foreach (var resultado in listaZonas)
                    {
                        var respAnt = new ZonasSvc().getAnticipos(resultado.clave);
                        if (respAnt.typeResult == ResultTypes.success)
                        {
                            resultado.listaAnticipo = respAnt.result as List<Anticipo>;
                        }
                        else
                        {
                            resultado.listaAnticipo = new List<Anticipo>();
                        }

                        var respCaseta = new ZonasSvc().getCasetasByDestino(resultado.clave);
                        if (respCaseta.typeResult == ResultTypes.success)
                        {
                            resultado.listaCasetas = respCaseta.result as List<Caseta>;
                        }
                    }
                    new ReportView().exportarListaDestinosGlobal(listaZonas, ctrEmpresa.empresaSelected);
                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
