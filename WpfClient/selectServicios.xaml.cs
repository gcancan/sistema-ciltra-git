﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static WpfClient.ImprimirMensaje;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectServicios.xaml
    /// </summary>
    public partial class selectServicios : Window
    {
        public selectServicios()
        {
            InitializeComponent();
        }
        private string parametro = string.Empty;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtFind.Text = parametro;
            lvlServicios.Focus();
        }
        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlServicios.ItemsSource).Refresh();
        }
        public Servicio getServicio()
        {
            try
            {
                buscarAllServicios();
                bool? resp = ShowDialog();
                if (resp.Value && lvlServicios.SelectedItem != null)
                {
                    return ((Servicio)((ListViewItem)lvlServicios.SelectedItem).Content);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }
        void buscarAllServicios()
        {
            var resp = new ServiciosSvc().getAllServicios();
            if (resp.typeResult == ResultTypes.success)
            {
                mapLista(resp.result as List<Servicio>);
            }
            else if (resp.typeResult != ResultTypes.recordNotFound)
            {
                imprimir(resp);
                IsEnabled = false;
            }
        }
        private void mapLista(List<Servicio> list)
        {
            lvlServicios.ItemsSource = null;
            try
            {
                List<ListViewItem> listLvl = new List<ListViewItem>();
                foreach (var item in list)
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = item;
                    lvl.MouseDoubleClick += Lvl_MouseDoubleClick; ;
                    listLvl.Add(lvl);
                }
                lvlServicios.ItemsSource = listLvl;
                CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlServicios.ItemsSource);
                view.Filter = UserFilter;
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((Servicio)(item as ListViewItem).Content).idServicio.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Servicio)(item as ListViewItem).Content).nombre.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void lvlActividades_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }
    }
}
