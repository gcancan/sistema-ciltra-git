﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Interfaces;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for asignarPersonalOrdenServicio.xaml
    /// </summary>
    public partial class asignarPersonalOrdenServicio : Window
    {
        private MasterOrdServ master;
        private TipoOrdenServicio ordenServ;
        private MainWindow mainWindow;
        public asignarPersonalOrdenServicio()
        {
            InitializeComponent();
        }

        public asignarPersonalOrdenServicio(TipoOrdenServicio ordenServ, MasterOrdServ master, MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            this.ordenServ = ordenServ;
            this.master = master;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtOrdenServ.Text = ordenServ.idOrdenSer.ToString();
            txtTipoOrden.Text = ordenServ.TipoServicio;
            txtUnidad.Text = ordenServ.unidadTrans.clave + " " + ordenServ.unidadTrans.tipoUnidad.descripcion;
            txtEstatus.Text = ordenServ.estatus;
            txtEntrega.personal = master.chofer;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            txtEntrega.limpiar();
            txtEntrega.abrirBuscador();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (txtEntrega.personal == null)
                {
                    ImprimirMensaje.imprimir(new OperationResult { valor = 3, mensaje = "NO SE ESPECIFICO EL PERSONAL QUE ENTREGA LA ORDEN" });
                    return;
                }
                if (txtResponsable.personal == null)
                {
                    ImprimirMensaje.imprimir(new OperationResult { valor = 3, mensaje = "NO SE ESPECIFICO EL PERSONAL QUE ENTREGA LA ORDEN" });
                    return;
                }

                var resp = new LlantaSvc().asignarOrdenLlantera(ordenServ.idOrdenSer, mainWindow.inicio._usuario, txtEntrega.personal, txtResponsable.personal, txtAuxiliar.personal);
                ImprimirMensaje.imprimir(resp);
                if (resp.typeResult == ResultTypes.success)
                {
                    DialogResult = true;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
