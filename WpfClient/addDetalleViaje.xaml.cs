﻿using Core.Interfaces;
using Core.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para addDetalleViaje.xaml
    /// </summary>
    public partial class addDetalleViaje : Window
    {
        private List<Producto> listaProducto;
        private List<Zona> listaDestinos;
        private Viaje viaje;        
        public addDetalleViaje()
        {
            this.listaProducto = new List<Producto>();
            this.listaDestinos = new List<Zona>();
            this.InitializeComponent();
        }
        public addDetalleViaje(List<Producto> listaProducto, List<Zona> listaDestinos, Viaje viaje)
        {
            this.listaProducto = new List<Producto>();
            this.listaDestinos = new List<Zona>();
            this.InitializeComponent();
            this.listaProducto = listaProducto;
            this.listaDestinos = listaDestinos;
            this.viaje = viaje;
        }
        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                List<CartaPorte> listaDetalles = this.getLista();
                OperationResult resp = new CartaPorteSvc().saveDetalleViaje(listaDetalles, this.viaje.NumGuiaId);
                ImprimirMensaje.imprimir(resp);
                if (resp.typeResult == ResultTypes.success)
                {
                    base.DialogResult = true;
                }
                else
                {
                    base.DialogResult = false;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        private List<CartaPorte> getLista()
        {
            List<CartaPorte> list = new List<CartaPorte>();
            List<ListViewItem> list2 = this.lvlCrtaPortes.Items.Cast<ListViewItem>().ToList<ListViewItem>();
            foreach (ListViewItem item in list2)
            {
                list.Add(item.Content as CartaPorte);
            }
            return list;
        }
        private void limpiarDetalles()
        {
            this.mapDestino(null);
            this.mapProducto(null);
            this.txtRemision.Clear();
            this.txtVolDescarga.valor = decimal.Zero;
        }

        private void mapDestino(Zona zona)
        {
            if (zona != null)
            {
                this.txtDestino.Text = zona.descripcion;
                this.txtDestino.Tag = zona;
                this.txtDestino.IsEnabled = false;
                this.txtVolDescarga.txtDecimal.Focus();
            }
            else
            {
                this.txtDestino.Clear();
                this.txtDestino.Tag = null;
                this.txtDestino.IsEnabled = true;
            }
        }
        private void mapDetalles()
        {
            try
            {
                if (((this.viaje.cliente != null) && (this.viaje.remolque != null)) && this.validarDetalles())
                {
                    CartaPorte porte1 = new CartaPorte
                    {
                        producto = (Producto)this.txtProducto.Tag,
                        zonaSelect = (Zona)this.txtDestino.Tag,
                        PorcIVA = this.viaje.cliente.impuesto.valor,
                        PorcRetencion = this.viaje.cliente.impuestoFlete.valor
                    };
                    Empresa empresa = new Empresa
                    {
                        clave = this.viaje.IdEmpresa
                    };
                    porte1.tipoPrecio = EstablecerPrecios.establecerPrecio(this.viaje.remolque, this.viaje.dolly, this.viaje.remolque2, empresa, this.viaje.cliente);
                    Empresa empresa2 = new Empresa
                    {
                        clave = this.viaje.IdEmpresa
                    };
                    porte1.tipoPrecioChofer = EstablecerPrecios.establecerPrecioChofer(this.viaje.remolque, this.viaje.dolly, this.viaje.remolque2, empresa2);
                    porte1.remision = this.txtRemision.Text.Trim();
                    porte1.volumenCarga = decimal.Zero;
                    porte1.volumenDescarga = this.txtVolDescarga.valor;
                    porte1.idTarea = 0;
                    porte1.cobroXkm = this.viaje.cobroXKm;
                    porte1.viajeFalso = this.viaje.viajeFalso;
                    CartaPorte porte = porte1;
                    ListViewItem newItem = new ListViewItem
                    {
                        Content = porte
                    };
                    this.lvlCrtaPortes.Items.Add(newItem);
                    this.limpiarDetalles();
                    this.txtRemision.Focus();
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }
        private void mapProducto(Producto producto)
        {
            if (producto != null)
            {
                this.txtProducto.Text = producto.descripcion;
                this.txtProducto.Tag = producto;
                this.txtProducto.IsEnabled = false;
                this.txtDestino.Focus();
            }
            else
            {
                this.txtProducto.Clear();
                this.txtProducto.Tag = null;
                this.txtProducto.IsEnabled = true;
            }
        }
        private void txtDestino_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && (this.listaProducto.Count > 0))
            {
                List<Zona> list = this.listaDestinos.FindAll(S => (S.descripcion.IndexOf(this.txtDestino.Text, StringComparison.OrdinalIgnoreCase) >= 0) || (S.descripcion.IndexOf(this.txtDestino.Text, StringComparison.OrdinalIgnoreCase) >= 0));
                if (list.Count == 1)
                {
                    this.mapDestino(list[0]);
                }
                else
                {
                    Zona zona = new selectDestinos(this.txtDestino.Text.Trim()).buscarZona(this.listaDestinos);
                    if (zona != null)
                    {
                        this.mapDestino(zona);
                    }
                }
            }
        }

        private void txtProducto_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && (this.listaProducto.Count > 0))
            {
                List<Producto> list = this.listaProducto.FindAll(S => (S.descripcion.IndexOf(this.txtProducto.Text, StringComparison.OrdinalIgnoreCase) >= 0) || (S.descripcion.IndexOf(this.txtProducto.Text, StringComparison.OrdinalIgnoreCase) >= 0));
                if (list.Count == 1)
                {
                    this.mapProducto(list[0]);
                }
                else
                {
                    Producto producto = new selectProducto(this.txtProducto.Text.Trim()).buscarProductos(this.listaProducto);
                    if (producto != null)
                    {
                        this.mapProducto(producto);
                    }
                }
            }
        }

        private void txtRemision_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && !string.IsNullOrEmpty(this.txtRemision.Text.Trim()))
            {
                this.txtProducto.Focus();
            }
        }

        private void txtVolDescarga_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && (this.txtVolDescarga.valor > decimal.Zero))
            {
                this.mapDetalles();
            }
        }

        private bool validarDetalles()
        {
            int result = 0;
            if (!int.TryParse(this.txtRemision.Text.Trim(), out result))
            {
                this.txtRemision.Clear();
                this.txtRemision.Focus();
                return false;
            }
            if (this.txtProducto.Tag == null)
            {
                this.txtProducto.Clear();
                this.txtProducto.Focus();
                return false;
            }
            if (this.txtDestino.Tag == null)
            {
                this.txtDestino.Clear();
                this.txtDestino.Focus();
                return false;
            }
            if (this.txtVolDescarga.valor <= decimal.Zero)
            {
                this.txtVolDescarga.txtDecimal.Focus();
            }
            return true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }
    }
}
