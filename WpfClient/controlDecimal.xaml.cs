﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for controlDecimal.xaml
    /// </summary>
    public partial class controlDecimal : UserControl
    {
        public controlDecimal()
        {
            InitializeComponent();
        }
        public decimal valor
        {
            get
            {
                return Convert.ToDecimal(txtDecimal.Text.Trim());
            }
            set
            {
                txtDecimal.Text = value.ToString();
                //DataContext = Convert.ToDecimal(txtDecimal.Text.Trim());
            }
        }
        public void Clear()
        {
            txtDecimal.Text = "0";
        }
        private void txtDecimal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key < Key.D0 || e.Key > Key.D9)
            {
                if (e.Key < Key.NumPad0 || e.Key > Key.NumPad9)
                {
                    if (e.Key == Key.OemPeriod || e.Key == Key.Decimal)
                    {
                        if (((TextBox)sender).Text.Contains("."))
                        {
                            e.Handled = true;
                        }
                    }
                    else if (e.Key == Key.Tab)
                    {
                        //e.Handled = false;
                    }
                    else if (e.Key == Key.Enter)
                    {
                        //e.Handled = false;
                    }
                    else
                    {
                        e.Handled = true;
                    }
                }
            }
        }

        private void txtDecimal_TextChanged(object sender, TextChangedEventArgs e)
        {
            var texto = ((TextBox)sender).Text.Trim();
            if (string.IsNullOrEmpty(texto))
            {
                ((TextBox)sender).Text = "0";
                ((TextBox)sender).CaretIndex = 1;
                return;
            }
            decimal d = 0;
            if (!decimal.TryParse(texto, out d))
            {
                if (((TextBox)sender).Text == ".")
                {
                    ((TextBox)sender).Text = "0.";
                    return;
                }
                ((TextBox)sender).Text = "0";
                ((TextBox)sender).CaretIndex = 1;
                return;
            }
            else
            {
                if (((TextBox)sender).Text == "0.")
                {
                    ((TextBox)sender).CaretIndex = ((TextBox)sender).Text.Length;
                    return;
                }
                if (((TextBox)sender).Text.Contains("."))
                {
                    return;
                }
                ((TextBox)sender).Text = d.ToString();
                ((TextBox)sender).CaretIndex = ((TextBox)sender).Text.Length;
            }
            //DataContext = Convert.ToDecimal(txtDecimal.Text.Trim());
        }
        private bool _soloLectura { get; set; }
        public HorizontalAlignment alienacionTexto { get; set; }
        public bool soloLectura
        {
            get { return _soloLectura; }
            set
            {
                _soloLectura = value;
                txtDecimal.IsReadOnly = _soloLectura;
            }
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            txtDecimal.Width = Width - 5;
            txtDecimal.HorizontalContentAlignment = alienacionTexto;
        }

        private void txtDecimal_GotFocus(object sender, RoutedEventArgs e)
        {
            this.txtDecimal.SelectAll();
        }
    }
}
