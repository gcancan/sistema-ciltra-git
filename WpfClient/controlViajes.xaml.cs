﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Markup;

    public partial class controlViajes : WpfClient.ViewBase
    {
        private int idViaje;
        private MainWindow mainWindow;        
        public controlViajes()
        {
            this.idViaje = 0;
            this.mainWindow = null;
            this.InitializeComponent();
        }

        public controlViajes(MainWindow mainWindow)
        {
            this.idViaje = 0;
            this.mainWindow = null;
            this.InitializeComponent();
            this.mainWindow = mainWindow;
        }

        public controlViajes(int idViaje, MainWindow mainWindow)
        {
            this.idViaje = 0;
            this.mainWindow = null;
            this.idViaje = idViaje;
            this.InitializeComponent();
            this.mainWindow = mainWindow;
        }
        private void CtrCabezara_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Viaje viaje = this.ctrCabezara.viaje;
            if (viaje != null)
            {
                this.mapLista(viaje.listDetalles);
            }
        }
        private void mapLista(List<CartaPorte> listDetalles)
        {
            foreach (CartaPorte porte in listDetalles)
            {
                ListViewItem newItem = new ListViewItem
                {
                    Content = porte
                };
                this.lvlDetalles.Items.Add(newItem);
                this.lblImporte.Content = (Convert.ToDecimal(this.lblImporte.Content.ToString().Replace('$', '0')) + porte.importeReal).ToString("C4");
            }
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.ctrCabezara.DataContextChanged += CtrCabezara_DataContextChanged;
            this.ctrCabezara.buscar(this.idViaje);
            if (new PrivilegioSvc().consultarPrivilegio("VER_PRECIOS", this.mainWindow.inicio._usuario.idUsuario).typeResult == ResultTypes.success)
            {
                GridView view = this.lvlDetalles.View as GridView;
                GridViewColumnCollection columns = view.Columns;
                GridViewColumn item = new GridViewColumn
                {
                    Header = "Precio",
                    Width = 80.0
                };
                Binding binding1 = new Binding("precio")
                {
                    StringFormat = "C4"
                };
                item.DisplayMemberBinding = binding1;
                view.Columns.Add(item);
                GridViewColumn column2 = new GridViewColumn
                {
                    Header = "Importe",
                    Width = 100.0
                };
                Binding binding2 = new Binding("importeReal")
                {
                    StringFormat = "C4"
                };
                column2.DisplayMemberBinding = binding2;
                view.Columns.Add(column2);
                GridViewColumn column3 = new GridViewColumn
                {
                    Header = "Casetas",
                    Width = 100.0
                };
                Binding binding3 = new Binding("precioCaseta")
                {
                    StringFormat = "C4"
                };
                column3.DisplayMemberBinding = binding3;
                view.Columns.Add(column3);
                this.lblImporte.Visibility = Visibility.Visible;
            }
            else
            {
                this.lblImporte.Visibility = Visibility.Collapsed;
            }
        }
    }
}
