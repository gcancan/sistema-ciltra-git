﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using Core.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for selectTareaProgramada.xaml
    /// </summary>
    public partial class selectTareaProgramada : Window
    {
        int idCliente = 0;
        string remision = "";
        public selectTareaProgramada()
        {
            InitializeComponent();
        }

        public selectTareaProgramada(int idCliente)
        {
            InitializeComponent();
            this.idCliente = idCliente;
        }

        public selectTareaProgramada(int idCliente, string remision)
        {
            InitializeComponent();
            this.idCliente = idCliente;
            this.remision = remision;
        }
        public List<TareaProgramada> selectTareas()
        {
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlTareas != null && lvlTareas.SelectedItems != null  )
            {
                List<TareaProgramada> list = new List<TareaProgramada>();
                foreach (ListViewItem item in lvlTareas.SelectedItems)
                {
                    list.Add( (TareaProgramada)item.Content);
                }
                return list;
            }
            return null;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            buscar();
            txtFind.Text = this.remision;
        }

        private void buscar()
        {
            try
            {
                var resp = new TareasProgramadasSvc().getTareasProgramadas(idCliente);
                if (resp.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (resp.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                if (resp.typeResult == ResultTypes.success)
                {
                    mapLista((List<TareaProgramada>)resp.result);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void mapLista(List<TareaProgramada> result)
        {
            List<ListViewItem> listLvl = new List<ListViewItem>();
            foreach (var item in result)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                listLvl.Add(lvl);
            }
            lvlTareas.ItemsSource = listLvl;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlTareas.ItemsSource);
            view.Filter = UserFilter;
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((TareaProgramada)(item as ListViewItem).Content).remision.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((TareaProgramada)(item as ListViewItem).Content).destino.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((TareaProgramada)(item as ListViewItem).Content).idProducto.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((TareaProgramada)(item as ListViewItem).Content).DescripcionProducto.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;

        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlTareas.ItemsSource).Refresh();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (lvlTareas.SelectedItems.Count > 0)
            {
                DialogResult = true;
            }
            else
            {
                MessageBox.Show("Elegir una o mas opciones", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
