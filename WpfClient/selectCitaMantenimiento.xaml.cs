﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectCitaMantenimiento.xaml
    /// </summary>
    public partial class selectCitaMantenimiento : Window
    {
        MainWindow mainWindow = null;
        public selectCitaMantenimiento(MainWindow mainWindow)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
        }
        public OperationResult agregarCita(SemaforoMtto semaforo)
        {
            var ctrCitasMantenimiento = new controlCitasMantenimiento(mainWindow, semaforo);
            ctrCitasMantenimiento.DataContextChanged += CtrCitasMantenimiento_DataContextChanged;
            stpContenedor.Children.Add(ctrCitasMantenimiento);

            ShowDialog();
            if (ctrCitasMantenimiento.DataContext == null)
            {
                return null;
            }
            else
            {
                return ctrCitasMantenimiento.DataContext as OperationResult;
            }

        }
        private void CtrCitasMantenimiento_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
