﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoreFletera.Models;
using Core.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para addNuevoOrigen.xaml
    /// </summary>
    public partial class addNuevoOrigen : Window
    {
        public addNuevoOrigen()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtCliente.Text = cliente == null ? string.Empty : this.cliente.nombre;
        }
        private OrigenPegaso origen;
        private Cliente cliente;
        private Empresa empresa;
        public OrigenPegaso saveNewOrigen(Empresa empresa, Cliente cliente = null)
        {
            this.empresa = empresa;
            this.cliente = cliente;
            bool? resp = ShowDialog();
            return this.origen;
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtNombreOrigen.Text)) return;

                OrigenPegaso origen = new OrigenPegaso
                {
                    idOrigen = 0,
                    idCliente = cliente == null ? null : (int?)this.cliente.clave,
                    nombre = txtNombreOrigen.Text.Trim(),
                    idEmpresa = empresa.clave
                };
                var resp = new ZonasSvc().saveNewOrigenPegaso(ref origen);
                if (resp.typeResult == ResultTypes.success)
                {
                    this.origen = origen;
                    DialogResult = true;
                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
    }
}
