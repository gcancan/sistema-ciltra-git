﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static WpfClient.ImprimirMensaje;
using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para reporteKardexMttoPrev.xaml
    /// </summary>
    public partial class reporteKardexMttoPrev : ViewBase
    {
        public reporteKardexMttoPrev()
        {
            InitializeComponent();
        }
        Usuario usuario = null;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                this.usuario = mainWindow.usuario;
                ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
                ctrEmpresa.loaded(usuario);

                ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
                ctrZonaOperativa.cargarControl(usuario.zonaOperativa, usuario.idUsuario);
                stpOperacion.IsEnabled = new PrivilegioSvc().consultarPrivilegio("ADMIN_COMBO_CLIENTES", usuario.idUsuario).typeResult == ResultTypes.success;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                this.IsEnabled = false;
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cargarOperacion();
            if (ctrEmpresa.empresaSelected.clave != 1)
            {
                cargarUnidades();
            }
        }
        void cargarOperacion()
        {
            try
            {
                Cursor = Cursors.Wait;
                cbxOperacion.ItemsSource = null;

                if (ctrEmpresa.empresaSelected.clave == 1)
                {
                    stpOperacion.Visibility = Visibility.Visible;
                    if (ctrZonaOperativa.zonaOperativa != null)
                    {
                        var resp = new ClienteSvc().getClientesByZonasOperativas(new List<string> { (ctrZonaOperativa.zonaOperativa.idZonaOperativa.ToString()) }, ctrEmpresa.empresaSelected.clave);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            var listaOperaciones = resp.result as List<OperacionFletera>;
                            cbxOperacion.ItemsSource = listaOperaciones;
                            var sel = cbxOperacion.ItemsSource.Cast<OperacionFletera>().ToList().Find(s => s.cliente.clave == usuario.cliente.clave);
                            if (sel != null)
                            {
                                cbxOperacion.SelectedItems.Add(sel);
                            }

                        }
                        else if (resp.typeResult != ResultTypes.recordNotFound)
                        {
                            imprimir(resp);
                        }
                    }
                }
                else
                {
                    stpOperacion.Visibility = Visibility.Hidden;
                    cargarUnidades();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        void cargarUnidades()
        {
            try
            {
                List<UnidadTransporte> listaUnidades = new List<UnidadTransporte>();
                chcTodosUnidades.IsChecked = false;
                cbxUnidades.SelectedItems.Clear();
                Cursor = Cursors.Wait;

                if (ctrEmpresa.empresaSelected.clave == 1)
                {
                    if (cbxOperacion.SelectedItems.Count >= 0)
                    {
                        foreach (OperacionFletera operacion in cbxOperacion.SelectedItems)
                        {
                            var resp = new FlotaSvc().getFlota(operacion);
                            if (resp.typeResult == ResultTypes.success)
                            {
                                foreach (Flota flota in resp.result as List<Flota>)
                                {
                                    if (flota.tractor != null)
                                    {
                                        listaUnidades.Add(flota.tractor);
                                    }
                                }
                            }
                        }
                    }

                }
                else if(ctrEmpresa.empresaSelected.clave == 2)
                {
                    if (ctrZonaOperativa.zonaOperativa != null)
                    {
                        OperacionFletera operacion = new OperacionFletera
                        {
                            empresa = ctrEmpresa.empresaSelected,
                            cliente = null,
                            zonaOperativa = ctrZonaOperativa.zonaOperativa
                        };
                        var resp = new FlotaSvc().getFlota(operacion);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            foreach (Flota flota in resp.result as List<Flota>)
                            {
                                if (flota.tractor != null)
                                {
                                    listaUnidades.Add(flota.tractor);
                                }                                
                            }
                        }
                    }
                }
                else if(ctrEmpresa.empresaSelected.clave == 5)
                {
                    var resp = new UnidadTransporteSvc().getTractores(ctrEmpresa.empresaSelected.clave);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        foreach (var item in resp.result as List<UnidadTransporte>)
                        {
                            listaUnidades.Add(item);
                        }
                    }
                }
                cbxUnidades.ItemsSource = listaUnidades.OrderBy(s => s.clave);


            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                cbxUnidades.ItemsSource = null;
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    ComboBox combo = sender as ComboBox;
                    if (combo.SelectedItem != null)
                    {
                        //var resp = new UnidadTransporteSvc().getUnidadesALLorById(ctrEmpresa.empresaSelected.clave);
                        //if (resp.typeResult == ResultTypes.success)
                        //{
                        //    cbxUnidades.ItemsSource = resp.result as List<UnidadTransporte>;
                        //}
                        cargarOperacion();
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void CbxOperacion_ItemSelectionChanged(object sender, Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventArgs e)
        {
            if (ctrEmpresa.empresaSelected.clave == 1)
            {
                if (cbxOperacion.SelectedItems != null)
                {
                    cargarUnidades();
                }

            }
        }
        private void ChcTodosOperaciones_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    CheckBox check = sender as CheckBox;
                    if (check.IsChecked.Value)
                    {
                        cbxOperacion.SelectAll();
                    }
                    else
                    {
                        cbxOperacion.SelectedItems.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void ChcTodosUnidades_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    CheckBox check = sender as CheckBox;
                    if (check.IsChecked.Value)
                    {
                        cbxUnidades.SelectAll();
                    }
                    else
                    {
                        cbxUnidades.SelectedItems.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                lvlResultados.ItemsSource = null;
                if (cbxUnidades.SelectedItems.Count == 0) return;

                List<string> listaUnidades = cbxUnidades.SelectedItems.Cast<UnidadTransporte>().ToList().Select(s=> s.clave).ToList();
                var resp = new KardexMttoPreventivoSvc().getKardexMttoPreventivo(listaUnidades);
                if (resp.typeResult == ResultTypes.success)
                {
                    llenarLista(resp.result as List<KardexMttoPreventivo>);
                }
                else
                {
                    imprimir(resp);
                }

            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void llenarLista(List<KardexMttoPreventivo> listKardex)
        {
            try
            {
                lvlResultados.ItemsSource = listKardex;
                CollectionView viewDet = (CollectionView)CollectionViewSource.GetDefaultView(lvlResultados.ItemsSource);
                PropertyGroupDescription groupDescriptionDet = new PropertyGroupDescription("idUnidad");
                viewDet.GroupDescriptions.Add(groupDescriptionDet);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (lvlResultados.ItemsSource == null) return;

                List<KardexMttoPreventivo> listaKadexMtto = new List<KardexMttoPreventivo>();
                listaKadexMtto = lvlResultados.ItemsSource.Cast<KardexMttoPreventivo>().ToList();
                new ReportView().exportarListaKardexMtto(listaKadexMtto);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
