﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
namespace WpfClient
{
    class AgruparCartaPortes
    {
        public List<CartaPorte> _listaAgrupada = new List<CartaPorte>();

        public List<List<CartaPorte>> superLista = new List<List<CartaPorte>>();

        public AgruparCartaPortes(List<CartaPorte> listDetalles, bool precio = false)
        {
            if (precio)
            {
                agruparPrecio(listDetalles);
            }
            else
            {
                agrupar(listDetalles);
            }            
        }

        public AgruparCartaPortes()
        {
        }

        public void agruparByZona(List<CartaPorte> listDetalles)
        {
            foreach (var item in listDetalles)
            {
                agruparByZona(item);
            }
            List<List<CartaPorte>> nuevaSuperLista = new List<List<CartaPorte>>();
            foreach (var item in superLista)
            {
                agrupar(item);
                nuevaSuperLista.Add(_listaAgrupada);
                _listaAgrupada = new List<CartaPorte>();
            }
            superLista = new List<List<CartaPorte>>();
            superLista = nuevaSuperLista;
        }

        private void agruparByZona(CartaPorte item)
        {
            if (superLista.Count == 0)
            {
                superLista.Add(new List<CartaPorte> { item });
            }
            else
            {
                int i = 0;
                bool existe = false;
                foreach (var lis in superLista)
                {
                    if (lis[0].zonaSelect.clave == item.zonaSelect.clave)
                    {
                        existe = true;
                        break;
                    }
                    else
                    {
                        i++;
                    }
                }
                if (existe)
                {
                    superLista[i].Add(item);
                }
                else
                {
                    superLista.Add(new List<CartaPorte> { item });
                }
            }
        }

        private void agruparPrecio(List<CartaPorte> listCartaPorte)
        {
            foreach (var item in listCartaPorte)
            {
                agregarListaPorPrecio(item);
            }
        }

        private void agregarListaPorPrecio(CartaPorte cp)
        {
            decimal suma = 0;
            foreach (var item in _listaAgrupada)
            {
                if ((item.precio == cp.precio))
                {
                    item.omitePesoBruto = true;
                    suma = item.volumenDescarga + cp.volumenDescarga;
                    item.volumenDescarga = suma;
                    if (!item.grupoZonas.Contains(cp.zonaSelect.descripcion))
                    {
                        item.grupoZonas += cp.zonaSelect.descripcion + ", ";
                    }
                    if (!item.grupoProducto.Contains(cp.producto.descripcion))
                    {
                        item.grupoProducto += cp.producto.descripcion + ", ";
                    }
                    item.grupoRemisiones += cp.remision.ToString() + ", ";

                    item.grupoImportes += cp.importeReal;
                    return;
                }
            }
            cp.grupoImportes += cp.importeReal;
            cp.grupoRemisiones = cp.remision.ToString() + ", ";
            cp.grupoProducto = cp.producto.descripcion + ", ";
            cp.grupoZonas =cp.zonaSelect.descripcion +", ";
            _listaAgrupada.Add(cp);
        }

        public void agrupar(List<CartaPorte> listCartaPorte)
        {
            foreach (var item in listCartaPorte)
            {
                agregarLista(item);
            }
        }
        void agregarLista(CartaPorte cp)
        {           
            foreach (var item in _listaAgrupada)
            {
                if ((item.producto.clave == cp.producto.clave) &&(item.zonaSelect.descripcion==cp.zonaSelect.descripcion))
                {
                    item.omitePesoBruto = true;
                    item.volumenDescarga = item.volumenDescarga + cp.volumenDescarga;
                    if (!item.grupoCartaPorte.Contains(cp.Consecutivo.ToString()))
                    {
                        item.grupoCartaPorte += cp.Consecutivo.ToString() + "-";
                    }  
                    
                    return;
                }
            }
            cp.grupoCartaPorte = cp.Consecutivo.ToString() + "-";
            _listaAgrupada.Add(cp);
            
        }
    }
}
