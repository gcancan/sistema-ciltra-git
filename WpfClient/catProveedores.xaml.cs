﻿using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para catProveedores.xaml
    /// </summary>
    public partial class catProveedores : ViewBase
    {
        public catProveedores()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ctrClaveProveedor.txtEntero.IsReadOnly = true;
            ctrClaveProveedor.txtEntero.Foreground = Brushes.SteelBlue;
            ctrClaveProveedor.txtEntero.FontWeight = FontWeights.Bold;
            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);
        }
        public override OperationResult guardar()
        {
            try
            {
                var val = validar();
                if (val.typeResult != ResultTypes.success) return val;
                ProveedorCombustible proveedor = mapForm();
                if (proveedor == null )
                {
                    return new OperationResult(ResultTypes.warning, "ERROR AL LEER LA INFOAMCIÓN DE LA PANTALLA");
                }
                var resp = new ProveedorSvc().saveProveedor(ref proveedor);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(proveedor);
                    base.guardar();
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        OperationResult validar()
        {
            try
            {
                OperationResult op = new OperationResult { valor = 0, mensaje = ("PARA CONTINUAR DEBE:" + Environment.NewLine) };

                if (string.IsNullOrEmpty(txtRazonSocial.Text.Trim()))
                {
                    op.valor = 2;
                    op.mensaje += " * Proporcionar Razón Social." + Environment.NewLine;
                }
                if (string.IsNullOrEmpty(txtRFC.Text.Trim()))
                {
                    op.valor = 2;
                    op.mensaje += " * Proporcionar RFC." + Environment.NewLine;
                }
                if (string.IsNullOrEmpty(txtDireccion.Text.Trim()))
                {
                    op.valor = 2;
                    op.mensaje += " * Proporcionar Dirección." + Environment.NewLine;
                }
                if (txtColonia.Tag == null)
                {
                    op.valor = 2;
                    op.mensaje += " * Proporcionar la Colonia." + Environment.NewLine;
                }
                return op;
            }
            catch (Exception ex)
            {
                return  new OperationResult(ex);
            }
        }
        public override OperationResult eliminar()
        {
            try
            {
                ProveedorCombustible proveedor = ctrClaveProveedor.Tag as ProveedorCombustible;
                if (proveedor == null)
                {
                    return new OperationResult(ResultTypes.warning, "ERROR AL LEER LA INFOAMCIÓN DE LA PANTALLA");
                }
                proveedor.estatus = false;
                var resp = new ProveedorSvc().saveProveedor(ref proveedor);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(null);
                    base.nuevo();
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        private ProveedorCombustible mapForm()
        {
            try
            {
                ProveedorCombustible proveedor = new ProveedorCombustible
                {
                    idProveedor = ctrClaveProveedor.Tag == null ? 0 : (ctrClaveProveedor.Tag as ProveedorCombustible).idProveedor,
                    nombre = txtRazonSocial.Text.Trim(),
                    RFC = txtRFC.Text.Trim(),
                    direccion = txtDireccion.Text.Trim(),
                    telefono = txtTelefono.Text.Trim(),
                    colonia = txtColonia.Tag as Colonia,
                    vehiculos = chcVehiculos.IsChecked.Value,
                    llantasNuevas = chcLlantasNuevas.IsChecked.Value,
                    renovadorLlantas = chcRenovadorLlantas.IsChecked.Value,
                    reparadorLlantas = chcReparadorLlantas.IsChecked.Value,
                    diesel = chcDiesel.IsChecked.Value,
                    precio = 0,
                    estatus = true
                };
                return proveedor;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        void mapForm(ProveedorCombustible proveedor)
        {
            try
            {
                if (proveedor != null)
                {
                    ctrClaveProveedor.Tag = proveedor;
                    ctrClaveProveedor.valor = proveedor.idProveedor;
                    txtRazonSocial.Text = proveedor.nombre;
                    txtRFC.Text = proveedor.RFC;
                    txtDireccion.Text = proveedor.direccion;
                    txtTelefono.Text = proveedor.telefono;
                    mapColonia(proveedor.colonia);
                    chcVehiculos.IsChecked = proveedor.vehiculos;
                    chcLlantasNuevas.IsChecked = proveedor.llantasNuevas;
                    chcRenovadorLlantas.IsChecked = proveedor.renovadorLlantas;
                    chcReparadorLlantas.IsChecked = proveedor.reparadorLlantas;
                    chcDiesel.IsChecked = proveedor.diesel;

                    txtEstatus.Text = proveedor.strEstatus;
                    txtEstatus.Foreground = proveedor.estatus ?  Brushes.SteelBlue : Brushes.Red;
                }
                else
                {
                    ctrClaveProveedor.Tag = null;
                    ctrClaveProveedor.valor = 0;
                    txtRazonSocial.Clear();
                    txtRFC.Clear();
                    txtDireccion.Clear();
                    txtTelefono.Clear();
                    mapColonia(null);
                    chcVehiculos.IsChecked = false;
                    chcLlantasNuevas.IsChecked = false;
                    chcRenovadorLlantas.IsChecked = false;
                    chcReparadorLlantas.IsChecked = false;
                    chcDiesel.IsChecked = false;

                    txtEstatus.Text = "ACTIVO";
                    txtEstatus.Foreground = Brushes.SteelBlue;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void txtColonia_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (e.Key == Key.Enter)
                {
                    if (string.IsNullOrEmpty(txtColonia.Text.Trim()))
                    {
                        var colonia = new selectColonias().getAllColoniaByNombre("");
                        if (colonia != null)
                        {
                            mapColonia(colonia);
                        }
                    }

                    List<Colonia> listaColonias = new List<Colonia>();
                    var resp = new DireccionSvc().getALLColoniasByNombre(txtColonia.Text.Trim());
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listaColonias = resp.result as List<Colonia>;
                        if (listaColonias.Count == 1)
                        {
                            mapColonia(listaColonias[0]);
                        }
                        else
                        {
                            var colonia = new selectColonias().getAllColoniaByNombre(txtColonia.Text.Trim());
                            if (colonia != null)
                            {
                                mapColonia(colonia);
                            }
                        }
                    }
                    else if (resp.typeResult == ResultTypes.recordNotFound)
                    {
                        var colonia = new selectColonias().getAllColoniaByNombre("");
                        if (colonia != null)
                        {
                            mapColonia(colonia);
                        }
                    }
                    else
                    {
                        imprimir(resp);
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void mapColonia(Colonia colonia)
        {
            try
            {
                if (colonia != null)
                {
                    txtColonia.Text = colonia.nombre;
                    txtColonia.IsEnabled = false;
                    txtColonia.Tag = colonia;
                    txtCP.Text = colonia.cp;
                    txtCiudad.Text = colonia.ciudad.nombre;
                    txtEstado.Text = colonia.ciudad.estado.nombre;
                    txtPais.Text = colonia.ciudad.estado.pais.nombrePais;
                }
                else
                {
                    txtColonia.Clear();
                    txtColonia.IsEnabled = true;
                    txtColonia.Tag = null;
                    txtCP.Clear();
                    txtCiudad.Clear();
                    txtEstado.Clear();
                    txtPais.Clear();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void btnCambiarColonia_Click(object sender, RoutedEventArgs e)
        {
            mapColonia(null);
        }
        public override void buscar()
        {
            try
            {
                ProveedorCombustible proveedor = new selectProveedoresV2().getAllProveedor();
                if (proveedor != null)
                {
                    mapForm(proveedor);
                    base.buscar();
                }
                else
                {
                    nuevo();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new ProveedorSvc().getAllProveedoresV2();
                if (resp.typeResult == ResultTypes.success)
                {
                    var lista = resp.result as List<ProveedorCombustible>;
                    new ReportView().exportarListaProveedores(lista);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
