﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectCotizacionesOrdenCompra.xaml
    /// </summary>
    public partial class selectCotizacionesOrdenCompra : Window
    {
        public selectCotizacionesOrdenCompra()
        {
            InitializeComponent();
        }

        private void txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlCotizacion.ItemsSource).Refresh();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        public CotizacionSolicitudOrdCompra getCotizacion()
        {
            try
            {
                Cursor = Cursors.Wait;
                buscarAllCotizaciones();
                bool? resp = ShowDialog();
                if (resp.Value && lvlCotizacion.SelectedItem != null)
                {
                    return (CotizacionSolicitudOrdCompra)((ListViewItem)lvlCotizacion.SelectedItem).Content;
                }
                return null;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void buscarAllCotizaciones()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new CotizacionesSvc().getCotizacionAllOrById(0, false);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapLista(resp.result as List<CotizacionSolicitudOrdCompra>);
                }
                else if(resp.typeResult != ResultTypes.recordNotFound)
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapLista(List<CotizacionSolicitudOrdCompra> list)
        {
            lvlCotizacion.Items.Clear();
            List<ListViewItem> lvlList = new List<ListViewItem>();
            foreach (CotizacionSolicitudOrdCompra item in list)
            {
                
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick; ;
                lvl.FontWeight = FontWeights.Medium;
                if (item.estatus == "ACTIVO")
                {
                    lvl.Foreground = Brushes.SteelBlue;
                }
                else if (item.estatus == "CANCELADO")
                {
                    lvl.Foreground = Brushes.Red;
                }
                else if (item.estatus == "COTIZADO")
                {
                    lvl.Foreground = Brushes.Blue;
                }
                else if (item.estatus == "APROVADO")
                {
                    lvl.Foreground = Brushes.Green;
                }

                lvlList.Add(lvl);
            }
            lvlCotizacion.ItemsSource = lvlList;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlCotizacion.ItemsSource);
            view.Filter = UserFilter;
            lvlCotizacion.Focus();
        }

        private bool UserFilter(object item)
        {
            if (string.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((CotizacionSolicitudOrdCompra)(item as ListViewItem).Content).idCotizacionSolicitudOrdCompra.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((CotizacionSolicitudOrdCompra)(item as ListViewItem).Content).fecha.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((CotizacionSolicitudOrdCompra)(item as ListViewItem).Content).estatus.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((CotizacionSolicitudOrdCompra)(item as ListViewItem).Content).solicitud.idSolicitudOrdenCompra.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }
    }
}
