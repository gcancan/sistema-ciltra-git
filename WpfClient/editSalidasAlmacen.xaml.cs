﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using ProcesosComercial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editSalidasAlmacen.xaml
    /// </summary>
    public partial class editSalidasAlmacen : ViewBase
    {
        public editSalidasAlmacen()
        {
            InitializeComponent();
        }
        private Parametros parametros;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            var svcParam = new ParametrosSvc().getParametrosSistema();
            if (svcParam.typeResult == ResultTypes.success)
            {
                parametros = svcParam.result as Parametros;
            }
            else
            {
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                IsEnabled = false;
                imprimir(svcParam);
                return;
            }
            nuevo();
            txtFolio.txtEntero.KeyDown += TxtEntero_KeyDown;
        }
        public override void nuevo()
        {

            base.nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.buscar | ToolbarCommands.cerrar);
            ctlUnidadTrans.limpiar();
            lblMensaje.Visibility = Visibility.Hidden;
            gBoxOrdenes.IsEnabled = false;
            txtPersonaSolicita.limpiar();
            lvlInsumos.Items.Clear();
            limpiar();
        }
        private void limpiar()
        {
            ctlUnidadTrans.limpiar();
            mapOrdenServicio(null);
            txtChofer.Clear();
            mapListaInsumos(new List<ProductosCOM>());
            stpInsumos.Children.Clear();
            txtPersonaSolicita.limpiar();
            lblMensaje.Visibility = Visibility.Hidden;
            gBoxOrdenes.IsEnabled = false;
            lvlInsumos.Items.Clear();
            txtPersonaSolicita.limpiar();
        }
        private void TxtEntero_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && !string.IsNullOrEmpty(txtFolio.txtEntero.Text.Trim()))
            {
                buscarOrdenTraabajo(txtFolio.valor);
            }
        }
        private void buscarOrdenTraabajo(int valor)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new OperacionSvc().getOrdenServById(valor);
                if (resp.typeResult == ResultTypes.success)
                {
                    TipoOrdenServicio orden = resp.result as TipoOrdenServicio;
                    if (!orden.externo)
                    {
                        mapOrdenServicio(orden);
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "Esta Orden Es Externa"));
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        List<OrdenTaller>  _listaOrdenes = new List<OrdenTaller>();
        private void mapOrdenServicio(TipoOrdenServicio orden)
        {
            if (orden != null)
            {
                txtFolio.valor = orden.idOrdenSer;
                txtFolio.Tag = orden;
                txtFolio.IsEnabled = false;
                ctlUnidadTrans.unidadTransporte = orden.unidadTrans;
                //txtEmpresa.Text = orden.unidadTrans.empresa.nombre;
                txtTipoOrden.Text = orden.TipoServicio;
                txtEstatus.Text = orden.estatus;
                txtChofer.Text = orden.chofer;
                txtTipoServicio.Text = orden.tipoOrden.nombre;
                stpActividades.Children.Clear();             
                

                var resp = new MasterOrdServSvc().getTrabajosByOrdenSer(orden.idOrdenSer);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<OrdenTaller> listaActividades = resp.result as List<OrdenTaller>;

                    foreach (var item in listaActividades)
                    {
                        agregarListaActividades(item);

                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            else
            {
                txtFolio.limpiar();
                txtFolio.Tag = null;
                txtFolio.IsEnabled = true;
                ctlUnidadTrans.limpiar();
                txtTipoOrden.Clear();
                txtEstatus.Clear();
                stpActividades.Children.Clear();
                IsEnabled = true;
                txtTipoServicio.Clear();
                //txtEmpresa.Clear();
                txtChofer.Clear();
                _listaOrdenes = new List<OrdenTaller>();                
            }
        }
        private void agregarListaActividades(OrdenTaller actividad)
        {
            try
            {
                var act = _listaOrdenes.Find(s => s.idOrdenTrabajo == actividad.idOrdenTrabajo);
                actividad.costoActividad = act == null ? 0 : act.costoActividad;
                actividad.listaProductosCOM = act == null ? new List<ProductosCOM>() : act.listaProductosCOM;

                Button btnActividad = new Button
                {
                    HorizontalContentAlignment = HorizontalAlignment.Left,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(1),
                    Background = null,
                    Tag = actividad
                };
                
                StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal, Tag = actividad };

                Label lblNo = new Label
                {
                    Content = actividad.idOrdenTrabajo,
                    Width = 80,
                    Margin = new Thickness(1)
                };
                stpContent.Children.Add(lblNo);

                Label lblTipo = new Label
                {
                    Content = actividad.tipoDescripcion,
                    Width = 200,
                    Margin = new Thickness(1)
                };
                stpContent.Children.Add(lblTipo);

                TextBox lblDescripcion = new TextBox
                {
                    Text = actividad.descripcionActividad,
                    Width = 260,
                    TextWrapping = TextWrapping.Wrap,
                    IsReadOnly = true,
                    BorderBrush = null,
                    Background = null,
                    Focusable = false,
                    IsEnabled = false,
                    Margin = new Thickness(1)
                };
                stpContent.Children.Add(lblDescripcion);

                TextBox lblPreSalidas = new TextBox
                {
                    Text = actividad.preSalidas,
                    Width = 105,
                    TextWrapping = TextWrapping.Wrap,
                    IsReadOnly = true,
                    BorderBrush = null,
                    Background = null,
                    Focusable = false,
                    IsEnabled = false,
                    Margin = new Thickness(1)
                };
                stpContent.Children.Add(lblPreSalidas);

                btnActividad.Content = stpContent;
                btnActividad.Click += BtnActividad_Click;
                stpActividades.Children.Add(btnActividad);

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        OrdenTaller _orden = null;
        private void BtnActividad_Click(object sender, RoutedEventArgs e)
        {
            OrdenTaller ordenTaller = (sender as Button).Tag as OrdenTaller;
            if (ordenTaller.actividad == null)
            {
                lblMensaje.Visibility = Visibility.Visible;
                gBoxOrdenes.IsEnabled = false;
                _orden = null;
                actualizarListaInsumos(null);

                lvlInsumos.Items.Clear();
                txtPersonaSolicita.limpiar();
            }
            else
            {
                lblMensaje.Visibility = Visibility.Hidden;
                gBoxOrdenes.IsEnabled = true;
                _orden = ordenTaller;
                actualizarListaInsumos(_orden);

                lvlInsumos.Items.Clear();
                var resp = new ProductosCOMSvc().getInsumosCOMbyIdOrdenActividad(_orden.idOrdenActividad);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapListaInsumos(resp.result as List<ProductosCOM>);
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    imprimir(resp);
                }
                txtPersonaSolicita.personal = ordenTaller.personal1;
            }
        }
        private void mapListaInsumos(List<ProductosCOM> list)
        {
            lvlInsumos.Items.Clear();
            foreach (ProductosCOM item in list)
            {
                ListViewItem lvl = new ListViewItem() { Content = item };
                lvlInsumos.Items.Add(lvl);
            }
        }
        private void actualizarListaInsumos(OrdenTaller orden)
        {
            stpInsumos.Children.Clear();
            if (orden != null)
            {
                foreach (var item in orden.listaProductosCOM)
                {
                    mapListaProducto(item);
                }
            }
        }
        private void mapListaProducto(ProductosCOM producto)
        {
            try
            {
                Button btnProducto = new Button
                {
                    HorizontalContentAlignment = HorizontalAlignment.Left,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(1),
                    Background = null,
                    Tag = producto
                };

                ContextMenu menu = new ContextMenu();
                MenuItem menuCandelar = new MenuItem { Header = "Quitar" };
                menuCandelar.Tag = producto;
                menuCandelar.Click += MenuCandelar_Click;
                menu.Items.Add(menuCandelar);

                btnProducto.ContextMenu = menu;

                StackPanel stpContentProducto = new StackPanel { Orientation = Orientation.Horizontal, Tag = producto };

                TextBox lblDescripcion = new TextBox
                {
                    Text = producto.nombre,
                    Width = 250,
                    TextWrapping = TextWrapping.Wrap,
                    IsReadOnly = true,
                    BorderBrush = null,
                    Background = null,
                    Focusable = false,
                    IsEnabled = false,
                    Margin = new Thickness(1)
                };
                stpContentProducto.Children.Add(lblDescripcion);

                controlDecimal ctrCantidad = new controlDecimal
                {
                    valor = producto.cantidad,
                    Width = 100,
                    Margin = new Thickness(1),
                    Tag = producto,
                };
                ctrCantidad.txtDecimal.Tag = ctrCantidad;
                ctrCantidad.txtDecimal.TextChanged += TxtDecimal_CantidadProducto; ;
                stpContentProducto.Children.Add(ctrCantidad);

                
                btnProducto.Content = stpContentProducto;
                //btnProducto.Click += BtnProducto_Click; 
                stpInsumos.Children.Add(btnProducto);

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private void MenuCandelar_Click(object sender, RoutedEventArgs e)
        {
            var menuItem = sender as MenuItem;
            var orden = menuItem.Tag as OrdenTaller;
            var resp = new cancelarOrden(orden, mainWindow.inicio._usuario.nombreUsuario).cancelarOrdenTrabajo();
            if (resp.Value)
            {
                buscarOrdenTraabajo(txtFolio.valor);
            }
        }
        private void TxtDecimal_CantidadProducto(object sender, TextChangedEventArgs e)
        {
            controlDecimal controlDecimal = (sender as TextBox).Tag as controlDecimal;
            ProductosCOM producto = controlDecimal.Tag as ProductosCOM;
            producto.existencia = controlDecimal.valor; //comentar estas lineas.
            producto.cantidad = controlDecimal.valor;
        }

        private void txtInsumo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                buscarProducto();
            }
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            buscarProducto();
        }
        void buscarProducto()
        {
            var lista = new selectProductosCOM(txtInsumo.Text.Trim()).consultasTaller(AreaTrabajo.TOTAS, false);
            foreach (var item in lista)
            {
                decimal cantPreSal = item.cantPreOC;
                item.existencia = item.cantidad;
            }
            if (lista != null)
            {
                if (_orden != null && lista.Count > 0)
                {
                    _orden.listaProductosCOM = lista;
                    actualizarListaInsumos(_orden);
                }
            }
            txtInsumo.Clear();
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                var personaSolicita = txtPersonaSolicita.personal;
                if (personaSolicita == null)
                {
                    return new OperationResult(ResultTypes.warning, "Se tiene que especificar quien solicita.");
                }
                List<OrdenTaller> listaOrden = getListaActividades();
                OperationResult respNew = new OperationResult();
                foreach (OrdenTaller _orden in listaOrden)
                {
                    if (_orden.listaProductosCOM.Count > 0)
                    {
                        ProcesosForm proceso = new ProcesosForm(parametros);
                        respNew = proceso.realizarSalidaEntradaAlmacen(_orden, _orden.listaProductosCOM, personaSolicita, mainWindow.inicio._usuario.nombreUsuario);
                        proceso.Dispose();
                        if (respNew.typeResult != ResultTypes.success)
                        {
                            return respNew;
                        }
                    }
                   
                }

                if (respNew.typeResult == ResultTypes.success)
                {
                    int folio = txtFolio.valor;
                    nuevo();
                    buscarOrdenTraabajo(folio);
                    mainWindow.habilitar = true;
                }
                return respNew;
                //return base.guardar();
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        List<OrdenTaller> getListaActividades()
        {
            List<Button> listButon = stpActividades.Children.Cast<Button>().ToList();
            List<OrdenTaller> list = new List<OrdenTaller>();
            foreach (var item in listButon)
            {
                list.Add(item.Tag as OrdenTaller);
            }
            return list;
        }

        private void btnCambiar_Click(object sender, RoutedEventArgs e)
        {
            txtPersonaSolicita.limpiar();
        }
        public override void buscar()
        {
            selectOrdenServicio sel = new selectOrdenServicio();
            var orden = sel.getOrdenServicio();
            if (orden != null)
            {
                buscarOrdenTraabajo(orden.idOrdenSer);
            }
            //mapOrdenServicio(orden);
        }
    }
}
