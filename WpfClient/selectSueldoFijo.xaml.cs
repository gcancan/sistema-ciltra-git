﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectSueldoFijo.xaml
    /// </summary>
    public partial class selectSueldoFijo : Window
    {
        public selectSueldoFijo()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        public SueldoFijo getAllSueldosFijos()
        {
            try
            {
                buscarTodos();
                bool? resp = ShowDialog();
                if (resp.Value && lvlResultados.SelectedItem != null)
                {
                    return  (SueldoFijo)((ListViewItem)lvlResultados.SelectedItem).Content;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void buscarTodos()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new SueldoFijoSvc().getAllSueldoFijoOrByIdOperador();
                if (resp.typeResult == ResultTypes.success)
                {
                    llenarLista(resp.result as List<SueldoFijo>);
                }
                else if(resp.typeResult == ResultTypes.recordNotFound)
                {
                    IsEnabled = false;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void llenarLista(List<SueldoFijo> list)
        {
            lvlResultados.Items.Clear();
            List<ListViewItem> lvlList = new List<ListViewItem>();
            foreach (SueldoFijo item in list)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick; ;
                lvlList.Add(lvl);
                //lvlUnidades.Items.Add(lvl);
            }
            lvlResultados.ItemsSource = lvlList;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlResultados.ItemsSource);
            view.Filter = UserFilter;
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFiltro.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((SueldoFijo)(item as ListViewItem).Content).idSueldoFijo.ToString().IndexOf(txtFiltro.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((SueldoFijo)(item as ListViewItem).Content).operador.nombre.IndexOf(txtFiltro.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlResultados.ItemsSource).Refresh();
        }
    }
}
