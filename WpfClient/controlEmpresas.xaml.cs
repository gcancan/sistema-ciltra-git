﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for controlEmpresas.xaml
    /// </summary>
    public partial class controlEmpresas : UserControl
    {
        private Empresa empresa = new Empresa();
        private bool habilitado = false;
        public string lblContent = string.Empty;
        //public double tamaño
        //{
        //    get
        //    {
        //        return cbxEmpresa.Width;
        //    }
        //    set
        //    {
        //        cbxEmpresa.Width = value;
        //    }
        //}

        public controlEmpresas()
        {
            InitializeComponent();
        }        

        public void loaded(Empresa empresa, bool habilitado = false)
        {
            this.empresa = empresa;
            this.habilitado = habilitado;
            this.IsEnabled = habilitado;
            if (mapEmpresa())
            {
                if (empresa != null)
                {
                    int i = 0;
                    foreach (Empresa item in cbxEmpresa.ItemsSource)
                    {
                        if (item.clave == empresa.clave)
                        {
                            cbxEmpresa.SelectedIndex = i;
                            break;
                        }
                        i++;
                    }
                }
            }
            else
            {
                this.IsEnabled = false;
            }
            //if (string.IsNullOrEmpty(lblContent))
            //{
            //    lbl.Content = "EMPRESA:";
            //}
            //else
            //{
            //    lbl.Content = lblContent;
            //}
            
        }
        public void loaded(Usuario usuario)
        {
            this.empresa = usuario.empresa;
            this.habilitado = (new PrivilegioSvc().consultarPrivilegio("isAdmin", usuario.idUsuario).typeResult == ResultTypes.success);
            this.IsEnabled = habilitado;
            if (mapEmpresa())
            {
                if (empresa != null)
                {
                    int i = 0;
                    foreach (Empresa item in cbxEmpresa.ItemsSource)
                    {
                        if (item.clave == empresa.clave)
                        {
                            cbxEmpresa.SelectedIndex = i;
                            break;
                        }
                        i++;
                    }
                }
            }
            else
            {
                this.IsEnabled = false;
            }
            //if (string.IsNullOrEmpty(lblContent))
            //{
            //    lbl.Content = "EMPRESA:";
            //}
            //else
            //{
            //    lbl.Content = lblContent;
            //}

        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            cbxEmpresa.Width = this.Width - 62;            
        }

        private bool mapEmpresa()
        {
            try
            {
                OperationResult respEmp = new EmpresaSvc().getEmpresasALLorById();
                if (respEmp.typeResult == ResultTypes.success)
                {
                    cbxEmpresa.ItemsSource = (List<Empresa>)respEmp.result;
                    return true;
                }
                else
                {
                    ImprimirMensaje.imprimir(respEmp);
                    return false;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return false;
            }
        }
        public void seleccionarEmpresa(Empresa empresa)
        {
            cbxEmpresa.SelectedItem = empresa == null ? null : cbxEmpresa.ItemsSource.Cast<Empresa>().ToList().Find(s => s.clave == empresa.clave);
        }
        public Empresa empresaSelected
        {
            get
            {
                if (cbxEmpresa.SelectedItem == null)
                {
                    return null;
                }
                else
                {
                    var emp = (Empresa)cbxEmpresa.SelectedItem;
                    return emp;
                }                
            }
            set
            {
                Empresa empresa = value;
                cbxEmpresa.SelectedItem = cbxEmpresa.ItemsSource.Cast<Empresa>().ToList().Find(s=> s.clave == empresa.clave);
            }
        }
        private double _WidthCombo { get; set; }
        public double WidthCombo
        {
            get
            {
                return _WidthCombo;
            }
            set
            {
                _WidthCombo = value;
                cbxEmpresa.Width = _WidthCombo;
                Width = _WidthCombo + 62;
                HorizontalContentAlignment = HorizontalAlignment.Left;
            }
        }

        private void cbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxEmpresa.SelectedItem == null)
            {
                DataContext = null;
            }
            else
            {
                var emp = (Empresa)cbxEmpresa.SelectedItem;
                DataContext = emp;
            }
        }
    }
}
