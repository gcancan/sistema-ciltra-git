﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectResumenCargasCombustible.xaml
    /// </summary>
    public partial class selectResumenCargasCombustible : Window
    {
        public selectResumenCargasCombustible()
        {
            InitializeComponent();
        }
        public ResumenCargaCombustible getValeByDatosViaje(string idUnidadTrans, DateTime fecha)
        {
            try
            {
                fnGetValeByDatosViaje(idUnidadTrans, fecha);
                bool? result = ShowDialog();
                if (result.Value && lvlCargas.SelectedItem != null)
                {
                    return (lvlCargas.SelectedItem as ListViewItem).Content as ResumenCargaCombustible;
                }
                return null;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }
        void fnGetValeByDatosViaje(string idUnidadTrans, DateTime fecha)
        {
            try
            {
                var resp = new CargaCombustibleSvc().getValesCargaByViaje(idUnidadTrans, fecha);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<ResumenCargaCombustible> listaResumen = resp.result as List<ResumenCargaCombustible>;
                    llenarLista(listaResumen);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void llenarLista(List<ResumenCargaCombustible> listaResumen)
        {
            try
            {
                lvlCargas.ItemsSource = null;
                List<ListViewItem> list = new List<ListViewItem>();
                foreach (ResumenCargaCombustible pro in listaResumen)
                {
                    ListViewItem item = new ListViewItem
                    {
                        Content = pro
                    };
                    item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                    list.Add(item);
                }
                this.lvlCargas.ItemsSource = list;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlCargas.ItemsSource);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private bool UserFilter(object item) =>
           (string.IsNullOrEmpty(this.txtBuscador.Text) ||
           ((((ResumenCargaCombustible)(item as ListViewItem).Content).idValeCarga.ToString().IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(this.lvlCargas.ItemsSource).Refresh();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
