﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para pantallaVerificacionActividades.xaml
    /// </summary>
    public partial class pantallaVerificacionActividades : Window
    {
        List<ActividadOrdenServicio> listActServ = new List<ActividadOrdenServicio>();
        public pantallaVerificacionActividades(List<ActividadOrdenServicio> listActServ, enumProcesos tipoProcesos)
        {
            InitializeComponent();
            this.listActServ = listActServ;
            this.tipoProcesos = tipoProcesos;
        }
        enumProcesos tipoProcesos;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            gboxActNoRealizados.Visibility = Visibility.Collapsed;
            gboxActPendientes.Visibility = Visibility.Collapsed;

            //mapListaNoRealizadas(listActServ.FindAll(s=> s.enumActividadOrdenServ == enumActividadOrdenServ.NO));
            mapListaPendientes(listActServ.FindAll(s => s.enumActividadOrdenServ == enumActividadOrdenServ.PENDIENTE));
        }
        public List<ActividadOrdenServicio> getListaActServicio()
        {
            try
            {
                bool? result = ShowDialog();
                if (result.Value)
                {
                    listActServ = listActServ.FindAll(s => s.enumActividadOrdenServ == enumActividadOrdenServ.SI);
                    foreach (Border item in stpActNoRealizadas.Children.Cast<Border>().ToList())
                    {
                        listActServ.Add((item.Tag as ActividadOrdenServicio));
                    }
                    foreach (StackPanel item in stpActPendientes.Children.Cast<StackPanel>().ToList())
                    {
                        listActServ.Add((item.Tag as ActividadOrdenServicio));
                    }
                    return listActServ;
                }
                return null;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }
        private void mapListaNoRealizadas(List<ActividadOrdenServicio> list)
        {
            try
            {
                Cursor = Cursors.Wait;
                foreach (var actServ in list)
                {
                    gboxActNoRealizados.Visibility = Visibility.Visible;
                    StackPanel stp = new StackPanel
                    {
                        Margin = new Thickness(1),
                        Tag = actServ,
                        Orientation = Orientation.Horizontal
                    };

                    TextBox txtActividad = new TextBox
                    {
                        Text = actServ.actividad != null ? actServ.actividad.nombre : actServ.servicio.nombre,
                        Width = 250,
                        TextWrapping = TextWrapping.Wrap,
                        IsReadOnly = true,
                        Background = null,
                        BorderBrush = null,
                        Foreground = Brushes.SteelBlue,
                        FontWeight = FontWeights.Medium,
                        FontSize = 10
                    };
                    stp.Children.Add(txtActividad);

                    controlMotivosCancelacion controlMotivos = new controlMotivosCancelacion(this.tipoProcesos);
                    stp.Children.Add(controlMotivos);

                    stpActNoRealizadas.Children.Add(stp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void mapListaPendientes(List<ActividadOrdenServicio> list)
        {
            try
            {
                Cursor = Cursors.Wait;
                foreach (var actServ in list)
                {
                    gboxActPendientes.Visibility = Visibility.Visible;
                    StackPanel stp = new StackPanel
                    {
                        Orientation = Orientation.Horizontal,
                        Margin = new Thickness(1),
                        Tag = actServ
                    };

                    TextBox txtActividad = new TextBox
                    {
                        Text = actServ.actividad != null ? actServ.actividad.nombre : actServ.servicio.nombre,
                        Width = 250,
                        TextWrapping = TextWrapping.Wrap,
                        IsReadOnly = true,
                        Background = null,
                        BorderBrush = null,
                        Foreground = Brushes.SteelBlue,
                        FontWeight = FontWeights.Bold,
                        VerticalContentAlignment = VerticalAlignment.Center,
                        //FontStyle = FontStyles.Italic,
                        FontSize = 10
                    };
                    stp.Children.Add(txtActividad);

                    controlMotivosCancelacion controlMotivos = new controlMotivosCancelacion(this.tipoProcesos);
                    controlMotivos.Tag = stp;
                    controlMotivos.cbxMotivos.SelectionChanged += CbxMotivos_SelectionChanged;
                    stp.Children.Add(controlMotivos);

                    stpActPendientes.Children.Add(stp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CbxMotivos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string texto = (sender as TextBox).Text;
            foreach (Border item in stpActNoRealizadas.Children.Cast<Border>().ToList())
            {
                (item.Tag as ActividadOrdenServicio).motivoCancelacion = texto;
            }
        }

        private void txtMotivoPendientes_TextChanged(object sender, TextChangedEventArgs e)
        {
            string texto = (sender as TextBox).Text;
            foreach (Border item in stpActPendientes.Children.Cast<Border>().ToList())
            {
                (item.Tag as ActividadOrdenServicio).motivoCancelacion = texto;
            }
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            foreach (StackPanel item in stpActPendientes.Children.Cast<StackPanel>().ToList())
            {
                ActividadOrdenServicio actServ = ((item.Tag as ActividadOrdenServicio));
                foreach (var ctr in item.Children.Cast<Object>().ToList())
                {
                    if (ctr is controlMotivosCancelacion)
                    {
                        var control = ctr as controlMotivosCancelacion;
                        actServ.motivoCancelacionObj = control.motivoCancelacion;
                        actServ.motivoCancelacion = control.obsMotivo;
                    }
                }
            }

            //if (!string.IsNullOrEmpty(txtMotivoPendientes.Text.Trim()))
            //{
            DialogResult = true;
            //}
        }
    }
}
