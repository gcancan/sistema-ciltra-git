﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfClient
{
    public interface IView 
    {
        MainWindow mainWindow { get; set; }
        Boolean bandeja { get; set; }
        void cerrar();
        OperationResult guardar();
        OperationResult eliminar();
        void nuevo();
        void editar();
        void buscar();
    }
}
