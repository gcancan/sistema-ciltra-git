﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for selectZonas.xaml
    /// </summary>
    public partial class selectUsuario : Window
    {
        public string parametro = "";
        public string idCliente = "";
        public selectUsuario()
        {
            InitializeComponent();
        }

        public selectUsuario(string parametro)
        {
            InitializeComponent();
            this.parametro = parametro;
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            txtFind.Text = parametro;            
        }
        public Usuario findUsuarios()
        {
            findAllUsuarios();
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlUsuario != null && lvlUsuario.SelectedItem != null)
            {
                var usuario = (Usuario)((ListViewItem)lvlUsuario.SelectedItem).Content;

                OperationResult resp = new PrivilegioSvc().getPrivilegiosByIdUsuario(usuario.idUsuario);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Privilegio> list = (List<Privilegio>)resp.result;
                    usuario.listPrivilegios = list;
                    return usuario;
                }
                else if(resp.typeResult == ResultTypes.recordNotFound)
                {
                    usuario.listPrivilegios = new List<Privilegio>();
                    return usuario;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        private void mapList(List<Usuario> list)
        {
            lvlUsuario.Items.Clear();
            List<ListViewItem> lvlList = new List<ListViewItem>();
            foreach (Usuario item in list)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                lvlList.Add(lvl);
                //lvlUnidades.Items.Add(lvl);
            }
            lvlUsuario.ItemsSource = lvlList;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlUsuario.ItemsSource);
            view.Filter = UserFilter;
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return/* ((Usuario)(item as ListViewItem).Content).cliente.nombre.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||*/
                        ((Usuario)(item as ListViewItem).Content).personal.nombre.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Usuario)(item as ListViewItem).Content).nombreUsuario.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;

        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlUsuario.ItemsSource).Refresh();
        }

        private void findAllUsuarios()
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult resp = new UsuarioSvc().getAllUserOrById_v2(0);
                if (resp.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.success)
                {
                    //lvlList = ((List<Personal>)resp.result);
                    mapList(((List<Usuario>)resp.result));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format(ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                controles.IsEnabled = false;
            }
            finally { Cursor = Cursors.Arrow; }

        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                DialogResult = false;
            }
        }

        private void txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                lvlUsuario.Focus();
            }
        }
    }
}
