﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editCombustibleExtra.xaml
    /// </summary>
    public partial class editCombustibleExtra : ViewBase
    {
        public editCombustibleExtra()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e) //GRCC
        {
            try
            {
                base.Cursor = Cursors.Wait;
                this.ctrUnidades.DataContextChanged += ctrUnidades_DataContextChanged;
                this.ctrUnidades.loaded(etipoUniadBusqueda.TODAS, null);
                OperationResult resp = new CargaCombustibleSvc().getTipoOrdenCombustibleExtra();
                if (resp.typeResult == ResultTypes.success)
                {
                    this.cbxOpcionCombustible.ItemsSource = resp.result as List<TipoOrdenCombustible>;
                }
                else
                {
                    this.error(resp);
                    return;
                }
                this.nuevo();
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void ctrUnidades_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e) //GRCC
        {
            try
            {
                if (ctrUnidades.DataContext != null)
                {
                    txtEmpresa.Text = (ctrUnidades.DataContext as UnidadTransporte).empresa.nombre;
                    //var respEmp = new EmpresaSvc().getEmpresasALLorById((ctrUnidades.DataContext as Viaje).IdEmpresa);
                    //txtEmpresa.Text = respEmp.typeResult == ResultTypes.success ? string.Empty : (respEmp.result as List<Empresa>)[0].nombre;
                }
                else
                {
                    txtEmpresa.Clear();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        public override void nuevo() //GRCC
        {
            base.nuevo();
            this.ctrViaje.IsEnabled = false;
            this.ctrViaje.valor = 0;
            this.ctrViaje.Tag = null;
            this.txtFecha.Clear();
            this.ctrUnidades.IsEnabled = false;
            this.ctrUnidades.limpiar();
            this.ctrOperador.IsEnabled = false;
            this.ctrOperador.limpiar();
            this.cbxOpcionCombustible.SelectedItem = null;
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
            this.mapForm(null);
        }
        private void error(OperationResult resp)  //GRCC
        {
            ImprimirMensaje.imprimir(resp);
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
            mainWindow.scrollContainer.IsEnabled = false;
        }
        private void cbxOpcionCombustible_SelectionChanged(object sender, SelectionChangedEventArgs e) //GRCC
        {
            TipoOrdenCombustible selectedItem = (sender as ComboBox).SelectedItem as TipoOrdenCombustible;
            if (selectedItem != null)
            {
                if (selectedItem.requiereViaje)
                {
                    this.ctrViaje.IsEnabled = true;
                    this.ctrViaje.limpiar();
                    this.ctrUnidades.IsEnabled = false;
                    this.ctrUnidades.limpiar();
                    this.ctrOperador.IsEnabled = false;
                    this.ctrOperador.limpiar();
                    this.lvlRemolques.Items.Clear();
                }
                else
                {
                    this.ctrViaje.IsEnabled = false;
                    this.ctrViaje.limpiar();
                    this.ctrUnidades.IsEnabled = true;
                    this.ctrUnidades.limpiar();
                    this.ctrOperador.IsEnabled = true;
                    this.ctrOperador.limpiar();
                    this.lvlRemolques.Items.Clear();
                }
            }
            else
            {
                this.ctrViaje.IsEnabled = false;
                this.ctrViaje.valor = 0;
                this.txtFecha.Clear();
                this.ctrUnidades.IsEnabled = false;
                this.ctrUnidades.limpiar();
                this.ctrOperador.IsEnabled = false;
                this.ctrOperador.limpiar();
                this.lvlRemolques.Items.Clear();
            }
        }

        private void ctrViaje_KeyDown(object sender, KeyEventArgs e) //GRCC
        {
            if (e.Key == Key.Enter)
            {
                var resp = new CartaPorteSvc().getCartaPorteById(ctrViaje.valor);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapDatosViaje((resp.result as List<Viaje>)[0]);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
        }

        private void mapDatosViaje(Viaje viaje) //GRCC
        {
            if (viaje != null)
            {
                ctrViaje.Tag = viaje;
                ctrOperador.personal = viaje.operador;
                var respEmp = new EmpresaSvc().getEmpresasALLorById(viaje.IdEmpresa);
                if (respEmp.typeResult == ResultTypes.success)
                {
                    txtEmpresa.Text = (respEmp.result as List<Empresa>)[0].nombre.ToUpper();
                }
                
                if (viaje.remolque2 == null)
                {
                    ctrUnidades.unidadTransporte = viaje.remolque;
                }
                else
                {
                    lvlRemolques.Items.Add(viaje.remolque);
                    lvlRemolques.Items.Add(viaje.remolque2);
                }
            }
        }
        public override OperationResult guardar() //GRCC
        {
            OperationResult result3;
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult result = this.validarForm();
                if (result.typeResult > ResultTypes.success)
                {
                    return result;
                }
                KardexCombustible kardexCombustible = null;
                CargaCombustibleExtra carga = this.mapForm();
                OperationResult result2 = new CargaCombustibleSvc().saveCargaCombustibleEXtra(ref carga, ref kardexCombustible);
                if (result2.typeResult == ResultTypes.success)
                {
                    this.mapForm(carga);
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                }
                result3 = result2;
            }
            catch (Exception exception)
            {
                result3 = new OperationResult(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return result3;
        }

        private OperationResult validarForm() //GRCC
        {
            try
            {
                OperationResult result = new OperationResult(ResultTypes.success, "", null);
                if (this.cbxOpcionCombustible.SelectedItem == null)
                {
                    return new OperationResult(ResultTypes.warning, "Seleccionar La Opci\x00f3n Combustible.", null);
                }
                if (this.ctrUnidades.unidadTransporte == null)
                {
                    return new OperationResult(ResultTypes.warning, "Seleccionar La Unidad de Transporte.", null);
                }
                if (this.ctrOperador.personal == null)
                {
                    return new OperationResult(ResultTypes.warning, "Seleccionar el Operador.", null);
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }

        private void mapForm(CargaCombustibleExtra cargaCombustibleExtra) //GRCC
        {
            if (cargaCombustibleExtra != null)
            {
                this.ctrEnteros.Tag = cargaCombustibleExtra;
                this.ctrEnteros.valor = cargaCombustibleExtra.idCargaCombustibleExtra;
                this.txtFecha.Text = cargaCombustibleExtra.fecha.ToString();
                base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
            }
            else
            {
                this.ctrEnteros.Tag = null;
                this.ctrEnteros.valor = 0;
                this.txtFecha.Clear();
                base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
                this.lvlRemolques.Items.Clear();
            }
        }

        private CargaCombustibleExtra mapForm() //GRCC
        {
            try
            {
                return new CargaCombustibleExtra
                {
                    idCargaCombustibleExtra = (this.ctrEnteros.Tag == null) ? 0 : (this.ctrEnteros.Tag as CargaCombustibleExtra).idCargaCombustibleExtra,
                    externo = false,
                    despachador = null,
                    numViaje = (this.ctrViaje.Tag == null) ? null : new int?((this.ctrViaje.Tag as Viaje).NumGuiaId),
                    userCrea = base.mainWindow.usuario.nombreUsuario,
                    chofer = this.ctrOperador.personal,
                    fechaCombustible = null,
                    unidadTransporte = this.ctrUnidades.unidadTransporte,
                    tipoOrdenCombustible = this.cbxOpcionCombustible.SelectedItem as TipoOrdenCombustible,
                    fecha = DateTime.Now
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void lvlRemolques_MouseDoubleClick(object sender, MouseButtonEventArgs e) //GRCC
        {
            if (lvlRemolques.SelectedItem != null)
            {
                ctrUnidades.unidadTransporte = lvlRemolques.SelectedItem as UnidadTransporte;
            }
            else
            {
                ctrUnidades.unidadTransporte = null;
            }
        }
    }
}
