﻿using Core.Models;
using CoreFletera.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static WpfClient.ImprimirMensaje;
using CoreFletera.Models;
using Core.Interfaces;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editPrecargaTicketsDiesel.xaml
    /// </summary>
    public partial class editCargaTicketsDiesel : ViewBase
    {
        public editCargaTicketsDiesel()
        {
            InitializeComponent();
        }
        Usuario usuario;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;

                this.usuario = mainWindow.usuario;

                ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
                ctrEmpresa.loaded(usuario);

                ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
                ctrZonaOperativa.cargarControl(usuario.zonaOperativa, usuario.idUsuario);

                OperationResult respProveedor = new ProveedorSvc().getAllProveedoresV2();
                if (respProveedor.typeResult == ResultTypes.success)
                {
                    //List<ProveedorCombustible> listaProveedores = new List<ProveedorCombustible> { new ProveedorCombustible { idProveedor = 0, nombre = "INTEGRA" } };
                    List<ProveedorCombustible> listaProveedores = new List<ProveedorCombustible>();
                    listaProveedores.AddRange((respProveedor.result as List<ProveedorCombustible>).FindAll(s => s.estatus && s.diesel).OrderBy(s => s.nombre).ToList());
                    cbxProveedores.ItemsSource = listaProveedores;
                }
                else
                {
                    imprimir(respProveedor);
                    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    return;
                }
                this.ctrVale.KeyDown += new KeyEventHandler(this.CtrVale_KeyDown);
                this.ctrOrdenServicio.KeyDown += new KeyEventHandler(this.CtrOrdenServicio_KeyDown);
                nuevo();

                habilitarEdicion = new PrivilegioSvc().consultarPrivilegio("HABILITAR_EDICION_CARGA_TICKETS", usuario.idUsuario).typeResult == ResultTypes.success;
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        bool habilitarEdicion = false;
        private void CtrOrdenServicio_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    if (ctrTractor.unidadTransporte == null) return;

                    base.Cursor = Cursors.Wait;
                    if (this.ctrOrdenServicio.valor != 0)
                    {
                        OperationResult resp = new OperacionSvc().getSalidasEntradasByIdOrdenServ(this.ctrOrdenServicio.valor);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            CargaCombustible result = (CargaCombustible)resp.result;
                            this.mapVale(result);
                        }
                        else
                        {
                            ImprimirMensaje.imprimir(resp);
                        }
                    }
                }
                catch (Exception exception)
                {
                    ImprimirMensaje.imprimir(exception);
                }
                finally
                {
                    base.Cursor = Cursors.Arrow;
                }
            }
        }
        private void CtrVale_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                buscarCargaByVale(ctrVale.valor);
            }
        }

        private void buscarCargaByVale(int idVale)
        {
            try
            {
                if (ctrTractor.unidadTransporte == null) return;

                base.Cursor = Cursors.Wait;
                if (this.ctrVale.valor != 0)
                {
                    OperationResult resp = new OperacionSvc().cargaCombustibleByIdVale(idVale);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        CargaCombustible result = (CargaCombustible)resp.result;
                        this.mapVale(result);
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void mapVale(CargaCombustible vale)
        {
            try
            {
                if (vale != null)
                {
                    if (ctrTractor.unidadTransporte.clave != vale.masterOrdServ.tractor.clave)
                    {
                        imprimir(new OperationResult(ResultTypes.warning, "ESTE VALE NO CORRESPONDE A LA MISMA UNIDAD DE TRANSPORTE AL DEL TICKET"));
                        return;
                    }

                    if (vale.ltsExtra > 0)
                    {
                        imprimir(new OperationResult(ResultTypes.warning, "ESTE VALE YA TIENE UN TICKET EXTRA ASIGNADO"));
                        return;
                    }
                    if (!string.IsNullOrEmpty(vale.ticket))
                    {
                        imprimir(new OperationResult(ResultTypes.warning, "ESTE VALE YA ES UNA CARGA EXTERNA"));
                        return;
                    }

                    ctrVale.Tag = vale;
                    ctrVale.valor = vale.idCargaCombustible;
                    ctrOrdenServicio.valor = vale.idOrdenServ;
                    dtpFecha.Text = vale.fechaCombustible.Value.ToString("dd/MM/yyyy HH:mm");
                    ctrTractorVale.unidadTransporte = vale.masterOrdServ.tractor;
                    txtLitrosVale.valor = vale.ltCombustible;
                    stpControlesVale.IsEnabled = false;
                }
                else
                {
                    ctrVale.Tag = null;
                    ctrVale.valor = 0;
                    ctrOrdenServicio.valor = 0;
                    dtpFecha.Clear();
                    ctrTractorVale.limpiar();
                    txtLitrosVale.valor = 0;
                    stpControlesVale.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            txtOperadores.loaded(eTipoBusquedaPersonal.OPERADORES, ctrEmpresa.empresaSelected);
            if (ctrZonaOperativa.zonaOperativa != null)
            {
                ctrTractor.loaded(etipoUniadBusqueda.TRACTOR, ctrEmpresa.empresaSelected, ctrZonaOperativa.zonaOperativa);
            }
        }

        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ctrTractor.loaded(etipoUniadBusqueda.TRACTOR, ctrEmpresa.empresaSelected, ctrZonaOperativa.zonaOperativa);
        }

        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.buscar | ToolbarCommands.cerrar);
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Arrow;
                var val = validar();
                if (val.typeResult != ResultTypes.success) return val;

                CargaTicketDiesel carga = mapForm();
                var resp = new CargaTicketDieselSvc().saveCargaticketDiesel(ref carga);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(carga);                    
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        OperationResult validar()
        {
            try
            {
                OperationResult resp = new OperationResult(ResultTypes.success, ("PARA CONTINUAR DEBE:" + Environment.NewLine));

                if (ctrEmpresa.empresaSelected == null)
                {
                    resp.valor = 2;
                    resp.mensaje += "   * Seleccionar La Empresa." + Environment.NewLine;
                }
                if (ctrZonaOperativa.zonaOperativa == null)
                {
                    resp.valor = 2;
                    resp.mensaje += "   * Seleccionar La Zona Operativa." + Environment.NewLine;
                }
                if (ctrTractor.unidadTransporte == null)
                {
                    resp.valor = 2;
                    resp.mensaje += "   * Proporcionar La Unidad De Transporte." + Environment.NewLine;
                }
                if (cbxProveedores.SelectedItem == null)
                {
                    resp.valor = 2;
                    resp.mensaje += "   * Seleccionar Al Proveedor." + Environment.NewLine;
                }
                if (string.IsNullOrEmpty(txtTicket.Text.Trim()))
                {
                    resp.valor = 2;
                    resp.mensaje += "   * Proporcionar El Ticket." + Environment.NewLine;
                }
                if (txtPrecio.valor <= 0)
                {
                    resp.valor = 2;
                    resp.mensaje += "   * Proporcionar El Precio." + Environment.NewLine;
                }
                if (txtLitros.valor <= 0)
                {
                    resp.valor = 2;
                    resp.mensaje += "   * Proporcionar Los Litros." + Environment.NewLine;
                }
                if (string.IsNullOrEmpty(txtFolioImpreso.Text))
                {
                    resp.valor = 2;
                    resp.mensaje += "   * Proporcionar El Folio Impreso." + Environment.NewLine;
                }
                if (txtOperadores.personal == null)
                {
                    resp.valor = 2;
                    resp.mensaje += "   * Proporcionar El Chofer." + Environment.NewLine;
                }

                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public override void editar()
        {
            base.editar();
            stpControles.IsEnabled = habilitarEdicion;
        }
        private void mapForm(CargaTicketDiesel carga)
        {
            try
            {
                if (carga != null)
                {
                    ctrEmpresa.empresaSelected = carga.empresa;
                    ctrZonaOperativa.zonaOperativa = carga.zonaOperativa;
                    ctrClave.Tag = carga;
                    ctrClave.valor = carga.clave;
                    ctrTractor.unidadTransporte = carga.tractor;
                    cbxProveedores.SelectedItem = (cbxProveedores.ItemsSource as List<ProveedorCombustible>).Find(s => s.idProveedor == carga.proveedor.idProveedor);
                    txtTicket.Text = carga.ticket;
                    txtPrecio.valor = carga.precio;
                    txtLitros.valor = carga.litros;
                    txtFolioImpreso.Text = carga.folioImpreso;
                    txtObservaciones.Text = carga.observaciones;

                    if (string.IsNullOrEmpty(carga.operador.nombre))
                    {
                        var respPer = new OperadorSvc().getOperadoresALLorById(carga.operador.clave);
                        if (respPer.typeResult == ResultTypes.success)
                        {
                            carga.operador = (respPer.result as List<Personal>)[0];
                        }
                    }

                    txtOperadores.personal = carga.operador;
                    if (carga.carga != null)
                    {
                        buscarCargaByVale(carga.carga.idCargaCombustible);
                    }
                    mapVale(carga.carga);
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.editar | ToolbarCommands.cerrar);
                    if (carga.carga != null)
                    {
                        mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                    }
                }
                else
                {
                    ctrEmpresa.empresaSelected = usuario.empresa;
                    ctrZonaOperativa.zonaOperativa = usuario.zonaOperativa;
                    ctrClave.Tag = null;
                    ctrClave.valor = 0;
                    ctrTractor.limpiar();
                    cbxProveedores.SelectedItem = null;
                    txtTicket.Clear();
                    txtPrecio.valor = 0;
                    txtLitros.valor = 0;
                    txtFolioImpreso.Clear();
                    txtObservaciones.Clear();
                    txtOperadores.limpiar();
                    mapVale(null);
                    stpControles.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private CargaTicketDiesel mapForm()
        {
            try
            {
                return new CargaTicketDiesel
                {
                    clave = ctrClave.Tag == null ? 0 : (ctrClave.Tag as CargaTicketDiesel).clave,
                    empresa = ctrEmpresa.empresaSelected,
                    zonaOperativa = ctrZonaOperativa.zonaOperativa,
                    tractor = ctrTractor.unidadTransporte,
                    proveedor = cbxProveedores.SelectedItem as ProveedorCombustible,
                    ticket = txtTicket.Text.Trim(),
                    precio = txtPrecio.valor,
                    litros = txtLitros.valor,
                    folioImpreso = txtFolioImpreso.Text.Trim(),
                    observaciones = txtObservaciones.Text.Trim(),
                    operador = txtOperadores.personal,
                    carga = ctrClave.Tag == null ? null : (ctrVale.Tag as CargaCombustible)
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public override void buscar()
        {
            
            var cargaTicket = new selectCargaTicketsDiesel(usuario).seleccionarCargaDisel();
            if (cargaTicket != null)
            {
                mapForm(cargaTicket);
                base.buscar();
                mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.editar | ToolbarCommands.cerrar);
            }
        }
    }
}
