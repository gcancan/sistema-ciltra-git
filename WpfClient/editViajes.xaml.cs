﻿using Core.Interfaces;
using Core.Models;
using Core.Utils;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Markup;
using Xceed.Wpf.Toolkit;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editViajes.xaml
    /// </summary>
    public partial class editViajes : ViewBase
    {
        private string archivo = (System.Windows.Forms.Application.StartupPath + @"\config.ini");
        private string DispString;
        private Puerto pRFID = new Puerto();
        private Puerto pRFID_Secundario = new Puerto();
        private System.Timers.Timer timer;
        private int contador;
        private int claveEmpresa;
        private List<string> lista;
        private List<string> list1;
        private List<string> listaG;
        private bool correoCSI;
        private crearDocumento doc;
        private List<Zona> _listZonas;
        private bool habilitarChcRemisiones;
        private List<Producto> listaProductos;
        private bool omitirRemision;
        private Viaje _viaje;
        private int _idTareaProgramada;
        //private bool _contentLoaded;
        public editViajes()
        {
            System.Timers.Timer timer1 = new System.Timers.Timer
            {
                Interval = 1000.0
            };
            this.timer = timer1;
            this.contador = 0;
            this.claveEmpresa = 0;
            this.lista = new List<string>();
            this.list1 = new List<string>();
            this.listaG = new List<string>();
            this.correoCSI = false;
            this.doc = null;
            this._listZonas = new List<Zona>();
            this.habilitarChcRemisiones = false;
            this.listaProductos = new List<Producto>();
            this.omitirRemision = false;
            this._viaje = new Viaje();
            this._idTareaProgramada = 0;
            this.InitializeComponent();
        }
        private void abrirDocs(List<string> listDoc)
        {
            try
            {
                foreach (string str in listDoc)
                {
                    Process.Start(str);
                }
            }
            catch (Exception exception)
            {
                System.Windows.MessageBox.Show(exception.Message);
            }
        }

        private void addLista(List<TareaProgramada> resp)
        {
            string str = "Ya se agregaron las siguientes remisiones:" + Environment.NewLine;
            string str2 = "Las siguientes remisiones no corresponden al dia de hoy:" + Environment.NewLine;
            bool flag = false;
            bool flag2 = false;
            string messageBoxText = "";
            foreach (TareaProgramada programada in resp)
            {
                if (this.existeLista(programada))
                {
                    str = str + programada.remision.ToString() + Environment.NewLine;
                    flag = true;
                }
                else
                {
                    decimal num2;
                    this.txtRemision.Text = programada.remision.ToString();
                    //this.selectProducto(((Cliente)this.txtFraccion.Tag).fraccion.listProductos, programada.DescripcionProducto);
                    this.selectZona(this._listZonas, programada.destino);
                    this.txtVolProgramado.Text = this.chxTipoMedidaKilogramos.IsChecked.Value ? (num2 = programada.valorProgramado * 1000M).ToString() : (num2 = programada.valorProgramado * 1000M).ToString();
                    this._idTareaProgramada = programada.idTareaProgramada;
                    this.txtVolDescarga.Focus();
                }
            }
            if (flag | flag2)
            {
                if (flag)
                {
                    messageBoxText = messageBoxText + str;
                }
                if (flag2)
                {
                    messageBoxText = messageBoxText + str2;
                }
                System.Windows.MessageBox.Show(messageBoxText, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void addNewZona_Click(object sender, RoutedEventArgs e)
        {
            Zona zona = new addNewDestino((Cliente)this.cbxClientes.SelectedItem).addNewZona();
            if (zona != null)
            {
                this._listZonas.Add(zona);
                foreach (System.Windows.Controls.ListViewItem item in (IEnumerable)this.lvlProductos.Items)
                {
                    ((CartaPorte)item.Content).listZonas = this._listZonas;
                }
            }
        }

        private List<CartaPorte> agrupar(List<CartaPorte> listDetalles)
        {
            AgruparCartaPortes portes = new AgruparCartaPortes(listDetalles, false);
            return portes._listaAgrupada;
        }

        private List<List<CartaPorte>> agruparByZona(List<CartaPorte> listDetalles)
        {
            AgruparCartaPortes portes = new AgruparCartaPortes();
            portes.agruparByZona(listDetalles);
            return portes.superLista;
        }

        private List<CartaPorte> agruparCartaPortes(List<CartaPorte> listDetalles)
        {
            List<CartaPorte> list4;
            try
            {
                List<CartaPorte> list = new List<CartaPorte>();
                List<CartaPorte> list2 = new List<CartaPorte>();
                List<CartaPorte> list3 = new List<CartaPorte>();
                foreach (CartaPorte porte in listDetalles)
                {
                    list2.Add(porte);
                    list3.Add(porte);
                }
                foreach (CartaPorte porte2 in list2)
                {
                    list3.Remove(list3[0]);
                    if (list3.Count > 0)
                    {
                        foreach (CartaPorte porte3 in list3)
                        {
                            if ((porte2.producto.clave == porte3.producto.clave) && (porte2.zonaSelect.clave == porte3.zonaSelect.clave))
                            {
                                porte2.volumenDescarga += porte3.volumenDescarga;
                            }
                        }
                        list.Add(porte2);
                    }
                }
                list4 = list;
            }
            catch (Exception)
            {
                throw;
            }
            return list4;
        }

        private void btnAddRemision_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((this.cbxClientes.SelectedItem != null) && (this.ctrRemolque1.unidadTransporte != null))
                {
                    List<TareaProgramada> resp = new selectTareaProgramada(((Cliente)this.cbxClientes.SelectedItem).clave).selectTareas();
                    if (resp != null)
                    {
                        this.addLista(resp);
                    }
                }
            }
            catch (Exception exception)
            {
                System.Windows.MessageBox.Show(exception.Message);
            }
        }

        private void btnCalcular_Click(object sender, RoutedEventArgs e)
        {
            List<CartaPorte> list = new List<CartaPorte>();
            foreach (System.Windows.Controls.ListViewItem item in (IEnumerable)this.lvlProductos.Items)
            {
                list.Add((CartaPorte)item.Content);
            }
            this.lvlProductos.Items.Clear();
            foreach (CartaPorte porte in list)
            {
                System.Windows.Controls.ListViewItem newItem = new System.Windows.Controls.ListViewItem
                {
                    Content = porte
                };
                this.lvlProductos.Items.Add(newItem);
            }
        }

        private void btnCargaArchivoXML_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog
            {
                Filter = "|*.XML",
                Title = "Elige el archivo XML para subir"
            };
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                string str = new Core.Utils.Util().setRutaXML(dialog.FileName);
                this.txtRutaXML.Text = str;
            }
        }

        private void btnNewDetalle_Click(object sender, RoutedEventArgs e)
        {
            if ((this.cbxClientes.SelectedItem != null) && (ctrRemolque1.unidadTransporte != null))
            {
                this.mapDetalles();
            }
        }

        private void btnQuitar_Click(object sender, RoutedEventArgs e)
        {
            object selectedItem = this.lvlProductos.SelectedItem;
            this.lvlProductos.Items.Remove(selectedItem);
            this.sumarVolumenCargas();
        }

        private List<Personal> buscarPersonal(string nombre)
        {
            OperationResult result = new OperadorSvc().getOperadoresByNombreAndCliente(nombre, ((Cliente)this.cbxClientes.SelectedItem).clave);
            switch (result.typeResult)
            {
                case ResultTypes.success:
                    return (List<Personal>)result.result;

                case ResultTypes.error:
                    System.Windows.MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
                    return null;

                case ResultTypes.warning:
                    System.Windows.MessageBox.Show(result.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return null;

                case ResultTypes.recordNotFound:
                    return new List<Personal>();
            }
            return null;
        }

        private void buscarRemision()
        {
            OperationResult result = new TareasProgramadasSvc().getTareasProgramadas(((Cliente)this.cbxClientes.SelectedItem).clave, Convert.ToInt32(this.txtBusquedaRemision.Text.Trim()));
            if (result.typeResult == ResultTypes.error)
            {
                System.Windows.MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
            }
            else if (result.typeResult == ResultTypes.warning)
            {
                System.Windows.MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else if (result.typeResult == ResultTypes.recordNotFound)
            {
                this.funcionBuscarTareas();
            }
            else if (result.typeResult == ResultTypes.success)
            {
                if (((List<TareaProgramada>)result.result).Count == 1)
                {
                    this.addLista((List<TareaProgramada>)result.result);
                }
                else
                {
                    this.funcionBuscarTareas();
                }
            }
            this.txtBusquedaRemision.Clear();
        }

        private void buscarRFID(string RFID)
        {
            //Dispatcher.Invoke(() =>
            //{
            //    try
            //    {
            //        int num = 0;
            //        foreach (UnidadTransporte transporte in (IEnumerable)this.cbxCamion.Items)
            //        {
            //            if ((transporte.rfid != "") && (transporte.rfid.Trim() == RFID))
            //            {
            //                this.cbxCamion.SelectedIndex = num;
            //                this.cbxCamion.IsEnabled = false;
            //                this.mapCamposTractor((UnidadTransporte)this.cbxCamion.SelectedItem, ((UnidadTransporte)this.cbxCamion.SelectedItem).clave);
            //                break;
            //            }
            //            num++;
            //        }
            //        num = 0;
            //        foreach (UnidadTransporte transporte2 in (IEnumerable)this.cbxTolva1.Items)
            //        {
            //            if (this.cbxTolva1.SelectedItem != null)
            //            {
            //                break;
            //            }
            //            if ((transporte2.rfid != "") && (transporte2.rfid.Trim() == RFID))
            //            {
            //                this.cbxTolva1.SelectedIndex = num;
            //                this.cbxTolva1.IsEnabled = false;
            //                this.mapCamposTolva1((UnidadTransporte)this.cbxTolva1.SelectedItem, ((UnidadTransporte)this.cbxTolva1.SelectedItem).clave);
            //                break;
            //            }
            //            num++;
            //        }
            //        num = 0;
            //        foreach (UnidadTransporte transporte3 in (IEnumerable)this.cbxDolly.Items)
            //        {
            //            if ((transporte3.rfid != "") && (transporte3.rfid.Trim() == RFID))
            //            {
            //                this.cbxDolly.SelectedIndex = num;
            //                this.cbxDolly.IsEnabled = false;
            //                this.mapCamposDolly((UnidadTransporte)this.cbxDolly.SelectedItem, ((UnidadTransporte)this.cbxDolly.SelectedItem).clave);
            //                break;
            //            }
            //            num++;
            //        }
            //        num = 0;
            //        foreach (UnidadTransporte transporte4 in (IEnumerable)this.cbxTolva2.Items)
            //        {
            //            if (((transporte4.rfid != "") && (transporte4.rfid.Trim() == RFID)) && (((UnidadTransporte)this.cbxTolva1.SelectedItem).rfid != transporte4.rfid))
            //            {
            //                this.cbxTolva2.SelectedIndex = num;
            //                this.cbxTolva2.IsEnabled = false;
            //                this.mapCampostolva2((UnidadTransporte)this.cbxTolva2.SelectedItem, ((UnidadTransporte)this.cbxTolva2.SelectedItem).clave);
            //                break;
            //            }
            //            num++;
            //        }
            //        num = 0;
            //        foreach (Personal personal in (IEnumerable)this.cbxChofer.Items)
            //        {
            //            if (personal.rfid.Trim() == RFID)
            //            {
            //                this.cbxChofer.SelectedIndex = num;
            //                this.cbxChofer.IsEnabled = false;
            //                this.mapChofer((Personal)this.cbxChofer.SelectedItem);
            //                break;
            //            }
            //            num++;
            //        }
            //        num = 0;
            //    }
            //    catch (Exception exception)
            //    {
            //        System.Windows.MessageBox.Show(exception.Message);
            //    }
            //});
        }

        private List<UnidadTransporte> buscarUnidades(string clave)
        {
            OperationResult result = new UnidadTransporteSvc().getUnidadesALLorById(this.claveEmpresa, clave);
            switch (result.typeResult)
            {
                case ResultTypes.success:
                    return (List<UnidadTransporte>)result.result;

                case ResultTypes.error:
                    System.Windows.MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
                    return null;

                case ResultTypes.warning:
                    System.Windows.MessageBox.Show(result.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return null;

                case ResultTypes.recordNotFound:
                    return new List<UnidadTransporte>();
            }
            return null;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            new ReportView().crearArchivo(this.mapForm());
        }

        private void cbxClientes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                base.Cursor = System.Windows.Input.Cursors.Wait;
                if ((sender != null) && (this.cbxClientes.SelectedItem != null))
                {
                    //Cliente selectedItem = (Cliente)this.cbxClientes.SelectedItem;
                    Empresa empresa = ctrEmpresa.empresaSelected;
                    Cliente cliente = cbxClientes.SelectedItem as Cliente;
                    if (empresa.clave == 1)
                    {
                        this.ctrTractor.loaded(etipoUniadBusqueda.TRACTOR, empresa, ctrZonaOperaiva.zonaOperativa, cliente);
                        this.ctrRemolque1.loaded(etipoUniadBusqueda.TOLVA, empresa, ctrZonaOperaiva.zonaOperativa, cliente);
                        this.ctrDolly.loaded(etipoUniadBusqueda.DOLLY, empresa);
                        this.ctrRemolque2.loaded(etipoUniadBusqueda.TOLVA, empresa, ctrZonaOperaiva.zonaOperativa, cliente);

                        ctrChofer.loaded(eTipoBusquedaPersonal.OPERADORES, cliente.clave);
                        ctrChoferCapacitacion.loaded(eTipoBusquedaPersonal.OPERADORES, cliente.clave);
                        txtAyudante.loaded(eTipoBusquedaPersonal.OPERADORES, cliente.clave);
                    }
                    else
                    {
                        this.ctrTractor.loaded(etipoUniadBusqueda.TRACTOR, empresa);
                        this.ctrRemolque1.loaded(etipoUniadBusqueda.TOLVA, empresa);
                        this.ctrDolly.loaded(etipoUniadBusqueda.DOLLY, empresa);
                        this.ctrRemolque2.loaded(etipoUniadBusqueda.TOLVA, empresa);

                        ctrChofer.loaded(eTipoBusquedaPersonal.OPERADORES, empresa);
                        ctrChoferCapacitacion.loaded(eTipoBusquedaPersonal.OPERADORES, empresa);
                        txtAyudante.loaded(eTipoBusquedaPersonal.OPERADORES, empresa);
                    }

                    switch (cliente.clave)
                    {
                        case 1:
                            txtFechaPrograma.Value = DateTime.Now;
                            break;
                        case 3:
                            if (DateTime.Now.Hour < 7)
                            {
                                txtFechaPrograma.Value = DateTime.Now.AddDays(-1);
                            }
                            break;
                        case 5:
                            if (DateTime.Now.Hour < 8)
                            {
                                txtFechaPrograma.Value = DateTime.Now.AddDays(-1);
                            }
                            break;
                        default:
                            txtFechaPrograma.Value = DateTime.Now;
                            break;
                    }

                    this.omitirRemision = cliente.omitirRemision;
                    this.chcOmitirRemisiones.IsChecked = new bool?(!this.omitirRemision);
                    if (this.omitirRemision)
                    {
                        this.chcOmitirRemisiones.Visibility = this.habilitarChcRemisiones ? Visibility.Visible : Visibility.Hidden;
                    }
                    else
                    {
                        this.chcOmitirRemisiones.Visibility = Visibility.Hidden;
                    }

                    //txtAyudante.loaded(eTipoBusquedaPersonal.OPERADORES, cliente.clave);

                    OperationResult resp = new ZonasSvc().getZonasByEmpresaCliente(empresa.clave, cliente.clave);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        this._listZonas = (List<Zona>)resp.result;
                    }
                    else
                    {
                        this._listZonas = new List<Zona>();
                        ImprimirMensaje.imprimir(resp);
                    }
                    OperationResult result2 = new ProductoSvc().getALLProductosByidFraccionEmpresa(ctrEmpresa.empresaSelected.clave, cliente.clave);
                    if (result2.typeResult == ResultTypes.success)
                    {
                        this.listaProductos = (List<Producto>)result2.result;
                    }
                    else
                    {
                        this.listaProductos = new List<Producto>();
                        ImprimirMensaje.imprimir(result2);
                    }
                    this.lvlProductos.Items.Clear();
                    this.txtRetencion.Text = cliente.impuesto.valor.ToString("0.00");
                    this.txtFlete.Text = cliente.impuestoFlete.valor.ToString("0.00");
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }

        private void cbxTolva1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void cbxTolva2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //this.txtCargaTotal.Text = this.sumarTotalCargas().ToString("0.00");
        }

        public override void cerrar()
        {
            if (this.pRFID_Secundario.RFID != null)
            {
                this.pRFID_Secundario.RFID.Close();
            }
            if (this.pRFID.RFID != null)
            {
                this.pRFID.RFID.Close();
            }
            base.cerrar();
        }

        private void chcEnviarCorreo_Checked(object sender, RoutedEventArgs e)
        {
            if (chcEnviarTaller.IsChecked.Value) return;
            new Core.Utils.Util().Write("PERMISOS_CORREO_CSI", "CSI", "true", this.archivo);
        }

        private void chcEnviarCorreo_Unchecked(object sender, RoutedEventArgs e)
        {
            if (chcEnviarTaller.IsChecked.Value) return;
            new Core.Utils.Util().Write("PERMISOS_CORREO_CSI", "CSI", "false", this.archivo);
        }

        private void chcImprimir_Checked(object sender, RoutedEventArgs e)
        {
            if (chcEnviarTaller.IsChecked.Value) return;
            new Core.Utils.Util().Write("IMPRIMIR", "ACTIVO", "true", this.archivo);
        }

        private void chcImprimir_Unchecked(object sender, RoutedEventArgs e)
        {
            if (chcEnviarTaller.IsChecked.Value) return;
            new Core.Utils.Util().Write("IMPRIMIR", "ACTIVO", "false", this.archivo);
        }

        private void chcTablero_Checked(object sender, RoutedEventArgs e)
        {
            if (chcEnviarTaller.IsChecked.Value) return;
            new Core.Utils.Util().Write("ACTIVAR_EVENTO_TABLERO", "ACTIVO", "true", this.archivo);
        }

        private void chcTablero_Unchecked(object sender, RoutedEventArgs e)
        {
            if (chcEnviarTaller.IsChecked.Value) return;
            new Core.Utils.Util().Write("ACTIVAR_EVENTO_TABLERO", "ACTIVO", "false", this.archivo);
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            this.omitirRemision = false;
            this.stpRemision.IsEnabled = !this.omitirRemision;
            this.stpVolDescarga.IsEnabled = !this.omitirRemision;
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            this.omitirRemision = true;
            this.txtRemision.Clear();
            this.stpRemision.IsEnabled = !this.omitirRemision;
            this.txtVolDescarga.Clear();
            this.stpVolDescarga.IsEnabled = !this.omitirRemision;
        }

        private void chxTipoMedidaKilogramos_Checked(object sender, RoutedEventArgs e)
        {
            this.lblVolPro.Content = "V. Prog";
            this.lblVolDes.Content = "V. Despachado";
        }

        private void chxTipoMedidaToneladas_Checked(object sender, RoutedEventArgs e)
        {
            this.lblVolPro.Content = "V. Prog (Tns.)";
            this.lblVolDes.Content = "V. Des (Tns.)";
        }

        private void completarClave(ref string clave)
        {
            if (clave.Length < 3)
            {
                for (int i = 0; i <= (3 - clave.Length); i++)
                {
                    clave = "0" + clave;
                }
            }
        }



        private void envioCorreo(Viaje viaje)
        {
            EnvioCorreo correo = new EnvioCorreo();
            if (this.chcEnviarCorreo.IsChecked.Value)
            {
                string str = correo.envioCorreoDeCartaPorte(viaje);
            }
            if (new Core.Utils.Util().isActivoCorreoCSI())
            {
                string str2 = correo.envioCorreoCSI(viaje);
            }
        }

        private TipoPrecio establecerPrecio()
        {
            UnidadTransporte tag = ctrRemolque1.unidadTransporte;
            switch (((Cliente)this.cbxClientes.SelectedItem).clave)
            {
                case 3:
                case 5:
                    if ((ctrDolly.unidadTransporte != null) || (ctrRemolque2.unidadTransporte != null))
                    {
                        return TipoPrecio.FULL;
                    }
                    if (tag.tipoUnidad.descripcion == "TOLVA 35 TON")
                    {
                        return TipoPrecio.SENCILLO35;
                    }
                    return TipoPrecio.SENCILLO;
            }
            if ((ctrDolly.unidadTransporte != null) || (ctrRemolque2.unidadTransporte != null))
            {
                return TipoPrecio.FULL;
            }
            return TipoPrecio.SENCILLO;
        }

        private void establecerRutaXML()
        {
            string path = new Core.Utils.Util().getRutaXML();
            if (File.Exists(path))
            {
                this.txtRutaXML.Text = path;
            }
            else
            {
                this.txtRutaXML.Text = new Core.Utils.Util().setRutaXML("");
            }
        }

        private bool existeLista(TareaProgramada item)
        {
            foreach (System.Windows.Controls.ListViewItem item2 in (IEnumerable)this.lvlProductos.Items)
            {
                if (item.idTareaProgramada == ((CartaPorte)item2.Content).idTarea)
                {
                    return true;
                }
            }
            return false;
        }

        public void funcionBuscarTareas()
        {
            List<TareaProgramada> resp = new selectTareaProgramada(((Cliente)this.cbxClientes.SelectedItem).clave, this.txtBusquedaRemision.Text.Trim()).selectTareas();
            if (resp != null)
            {
                this.addLista(resp);
            }
        }

        public override OperationResult guardar()
        {
            OperationResult result2;
            try
            {
                base.Cursor = System.Windows.Input.Cursors.Wait;
                this.txtBusquedaRemision.Focus();
                OperationResult result = this.validar();
                if (result.typeResult > ResultTypes.success)
                {
                    return result;
                }
                Viaje viaje = this.mapForm();
                if (!new verificacionViajes(viaje, !this.omitirRemision).ShowDialog().Value)
                {
                    return new OperationResult(ResultTypes.warning, "REVISAR LA INFORMACIÓN", null);
                }
                if (viaje != null)
                {
                    if (!this.chxTimbrar.IsChecked.Value)
                    {
                        string str = new Core.Utils.Util().getRutaXML();
                        if (string.IsNullOrEmpty(str))
                        {
                            return new OperationResult
                            {
                                valor = 2,
                                mensaje = "Para proceder se requiere la ruta del archivo XML"
                            };
                        }
                        ComercialCliente respCliente = new ComercialCliente
                        {
                            nombre = ((Cliente)this.cbxClientes.SelectedItem).nombre,
                            calle = ((Cliente)this.cbxClientes.SelectedItem).domicilio,
                            rfc = ((Cliente)this.cbxClientes.SelectedItem).rfc,
                            colonia = ((Cliente)this.cbxClientes.SelectedItem).colonia,
                            estado = ((Cliente)this.cbxClientes.SelectedItem).estado,
                            municipio = ((Cliente)this.cbxClientes.SelectedItem).municipio,
                            pais = ((Cliente)this.cbxClientes.SelectedItem).pais,
                            telefono = ((Cliente)this.cbxClientes.SelectedItem).telefono,
                            cp = ((Cliente)this.cbxClientes.SelectedItem).cp
                        };
                        ComercialEmpresa respEmpresa = new ComercialEmpresa
                        {
                            nombre = base.mainWindow.inicio._usuario.personal.empresa.nombre,
                            calle = base.mainWindow.inicio._usuario.personal.empresa.direccion,
                            rfc = base.mainWindow.inicio._usuario.personal.empresa.rfc,
                            colonia = base.mainWindow.inicio._usuario.personal.empresa.colonia,
                            estado = base.mainWindow.inicio._usuario.personal.empresa.estado,
                            municipio = base.mainWindow.inicio._usuario.personal.empresa.municipio,
                            pais = base.mainWindow.inicio._usuario.personal.empresa.pais,
                            telefono = base.mainWindow.inicio._usuario.personal.empresa.telefono,
                            cp = base.mainWindow.inicio._usuario.personal.empresa.cp
                        };
                        DatosFacturaXML datosFactura = ObtenerDatosFactura.obtenerDatos(str);
                        if (datosFactura == null)
                        {
                            return new OperationResult
                            {
                                valor = 1,
                                mensaje = "Ocurrio un error al intentar obtener datos del XML"
                            };
                        }
                        int ordenTrabajo = 0;
                        OperationResult result3 = new CartaPorteSvc().saveCartaPorte_V2(ref viaje, ref ordenTrabajo, false, false, false);
                        if (result3.typeResult == ResultTypes.success)
                        {
                            this.mapForm(viaje);
                            this.envioCorreo(viaje);
                            if (this.chcImprimir.IsChecked.Value)
                            {
                                List<List<CartaPorte>> list = this.agruparByZona(viaje.listDetalles);
                                foreach (List<CartaPorte> list2 in list)
                                {
                                    this.imprimirReportes(respCliente, respEmpresa, datosFactura, list2, viaje);
                                }
                            }
                            if (this.chcTablero.IsChecked.Value)
                            {
                                ImprimirMensaje.imprimir(EstadoControlUnidadesTransporte.iniciarEstado(viaje));
                            }
                            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                        }
                        return result3;
                    }
                    List<string> listDoc = this.doc.llenarEstructuraDocumento(ref viaje);
                    if (listDoc != null)
                    {
                        int ordenTrabajo = 0;
                        OperationResult result4 = new CartaPorteSvc().saveCartaPorte(viaje, ref ordenTrabajo, false, false, false);
                        if (result4.typeResult == ResultTypes.success)
                        {
                            this.mapForm((Viaje)result4.result);
                            this.abrirDocs(listDoc);
                            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                            return result4;
                        }
                        return result4;
                    }
                    return new OperationResult
                    {
                        valor = 1,
                        mensaje = "Ocurrio un error"
                    };
                }
                result2 = new OperationResult
                {
                    valor = 1,
                    result = null,
                    mensaje = "Error al contruir el objeto"
                };
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    result = null,
                    mensaje = exception.Message
                };
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
            return result2;
        }

        private void guardarLista(string txt)
        {
            txt = txt.Replace("?", " ");
            txt = txt.Replace(")", " ");
            txt = txt.Replace("&", " ");
            txt = txt.Replace("&", " ");
            txt = txt.Replace("*", " ");
            txt = txt.Replace(" ", "");
            txt = txt.Replace("\r", "");
            txt = txt.Replace(@"\u", "");
            txt = txt.Replace(@"\", "");
            txt = txt.Trim();
            txt = Regex.Replace(txt, @"[^\w\.@-]", "", RegexOptions.None, TimeSpan.FromSeconds(1.5));
            if (txt.Length >= 0x18)
            {
                this.DispString = txt.Replace("\r", " ").Trim();
                if (this.list1.Count <= 0)
                {
                    Dispatcher.Invoke(() =>
                    {
                        this.list1.Add(this.DispString);
                        this.listaG.Add(this.DispString);
                        this.buscarRFID(this.DispString);
                    });
                }
                else
                {
                    Dispatcher.Invoke(() =>
                    {
                        this.list1.Add(this.DispString);
                        this.listaG.Add(this.DispString);
                        this.buscarRFID(this.DispString);
                    });
                }
            }
        }

        private void imprimeMensaje(OperationResult resp)
        {
            switch (resp.typeResult)
            {
                case ResultTypes.error:
                    System.Windows.MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
                    break;

                case ResultTypes.warning:
                    System.Windows.MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    break;

                case ResultTypes.recordNotFound:
                    System.Windows.MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    break;
            }
        }

        private void imprimirReportes(ComercialCliente respCliente, ComercialEmpresa respEmpresa, DatosFacturaXML datosFactura, List<CartaPorte> listaAgrupada, Viaje viaje)
        {
            try
            {
                base.Cursor = System.Windows.Input.Cursors.Wait;
                new ReportView(base.mainWindow).cartaPorteFormatoNuevo(respCliente, respEmpresa, datosFactura, listaAgrupada, viaje, "");
            }
            catch (Exception exception)
            {
                System.Windows.MessageBox.Show(exception.Message);
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }

        private void iniciar()
        {
            try
            {
                this.pRFID = new Puerto();
                System.Timers.Timer timer1 = new System.Timers.Timer
                {
                    Interval = 1000.0
                };
                this.timer = timer1;
                this.timer.Elapsed += new ElapsedEventHandler(this.Timer_Elapsed);
                Dispatcher.Invoke(() =>
                {
                    Dictionary<string, object> dictionary = this.pRFID.abrirPuerto();
                    this.contador = new Core.Utils.Util().getTiempoEspera();
                    if ((bool)dictionary["isOpen"])
                    {
                        this.listaG.Clear();
                        this.list1.Clear();
                        this.pRFID.RFID.DataReceived += new SerialDataReceivedEventHandler(this.RFID_DataReceived1);
                        this.timer.Start();
                    }
                    else
                    {
                        System.Windows.MessageBox.Show(dictionary["mensaje"].ToString());
                        this.pRFID.RFID.Close();
                    }
                });
            }
            catch (Exception exception)
            {
                System.Windows.MessageBox.Show(exception.Message);
            }
        }
        private void limpiarDetalles()
        {
            this.mapDestinos(null);
            this.mapProductos(null);
            this.txtRemision.Clear();
            this.txtVolProgramado.Clear();
            this.txtVolDescarga.Clear();
        }

        private void mapCamposDolly(UnidadTransporte unidadTransporte, string clave = "")
        {
            if (unidadTransporte != null)
            {
                //this.txtDolly.Text = clave;
                //this.txtDolly.Tag = unidadTransporte;
                //this.txtDolly.IsEnabled = false;
                ctrDolly.unidadTransporte = unidadTransporte;
                //this.txtTolva2.Focus();
                ctrRemolque2.txtUnidadTrans.Focus();
            }
            else
            {
                //this.txtDolly.Text = "";
                //this.txtDolly.Tag = null;
                //this.txtDolly.IsEnabled = true;
                ctrDolly.limpiar();
            }
        }

        private void mapCamposTolva1(UnidadTransporte unidadTransporte, string clave = "")
        {
            if (unidadTransporte != null)
            {
                ctrRemolque1.unidadTransporte = unidadTransporte;
                ctrDolly.txtUnidadTrans.Focus();
                //this.txtDolly.Focus();
                //this.txtCargaTotal.Text = this.sumarTotalCargas().ToString("0.00");
            }
            else
            {
                ctrRemolque1.limpiar();
            }
        }

        private void mapCampostolva2(UnidadTransporte unidadTransporte, string clave = "")
        {
            if (unidadTransporte != null)
            {
                //this.txtTolva2.Text = clave;
                //this.txtTolva2.Tag = unidadTransporte;
                //this.txtTolva2.IsEnabled = false;
                //this.txtChofer.Focus();
                ctrRemolque2.unidadTransporte = unidadTransporte;
                //this.txtCargaTotal.Text = this.sumarTotalCargas().ToString("0.00");
            }
            else
            {
                //this.txtTolva2.Text = "";
                //this.txtTolva2.Tag = null;
                //this.txtTolva2.IsEnabled = true;
                ctrRemolque2.limpiar();
            }
        }

        private void mapCamposTractor(UnidadTransporte unidadTransporte, string clave = "")
        {
            if (unidadTransporte != null)
            {
                this.ctrTractor.unidadTransporte = unidadTransporte;
            }
            else
            {
                this.ctrTractor.limpiar();
            }
        }

        private void mapChofer(Personal personal)
        {
            if (personal != null)
            {
                //this.txtChofer.Text = personal.nombre;
                //this.txtChofer.Tag = personal;
                //this.txtChofer.IsEnabled = false;
                ctrChofer.personal = personal;
                ctrChoferCapacitacion.txtPersonal.Focus();
                //this.txtChoferCapacitacion.Focus();
            }
            else
            {
                //this.txtChofer.Text = "";
                //this.txtChofer.Tag = null;
                //this.txtChofer.IsEnabled = true;
                ctrChofer.personal = null;
            }
        }

        private void mapChoferCapacitacion(Personal personal)
        {
            if (personal != null)
            {
                //this.txtChoferCapacitacion.Text = personal.nombre;
                //this.txtChoferCapacitacion.Tag = personal;
                //this.txtChoferCapacitacion.IsEnabled = false;
                ctrChoferCapacitacion.personal = personal;
                this.txtRemision.Focus();
            }
            else
            {
                //this.txtChoferCapacitacion.Text = "";
                //this.txtChoferCapacitacion.Tag = null;
                //this.txtChoferCapacitacion.IsEnabled = true;
                ctrChoferCapacitacion.personal = null;
            }
        }

        private void mapComboClientes(List<Cliente> listCliente)
        {
            //this.cbxClientes.ItemsSource = listCliente;
        }

        private void mapCombos(List<UnidadTransporte> list)
        {
            //foreach (UnidadTransporte transporte in list)
            //{
            //    if (transporte.tipoUnidad.clasificacion.ToUpper() == "DOLLY")
            //    {
            //        this.cbxDolly.Items.Add(transporte);
            //    }
            //    else if (transporte.tipoUnidad.clasificacion.ToUpper() == "TRACTOR")
            //    {
            //        this.cbxCamion.Items.Add(transporte);
            //    }
            //    if (transporte.tipoUnidad.clasificacion.ToUpper() == "TOLVA")
            //    {
            //        this.cbxTolva1.Items.Add(transporte);
            //        this.cbxTolva2.Items.Add(transporte);
            //    }
            //}
        }

        private void mapCombosOperador(List<Personal> listOperadores)
        {
            //foreach (Personal personal in listOperadores)
            //{
            //    this.cbxChofer.Items.Add(personal);
            //}
        }       

        private void mapDestinos(Zona zona)
        {
            if (zona != null)
            {
                this.txtDestino.Tag = zona;
                this.txtDestino.Text = zona.descripcion;
                this.txtDestino.IsEnabled = false;
                this.txtVolProgramado.Focus();
            }
            else
            {
                this.txtDestino.Tag = null;
                this.txtDestino.Clear();
                this.txtDestino.IsEnabled = true;
            }
        }

        public void mapDetalles()
        {
            if (((this.cbxClientes.SelectedItem != null) && (ctrRemolque1.unidadTransporte != null)) && this.validarDetalles())
            {
                CartaPorte porte = new CartaPorte
                {
                    cliente = cbxClientes.SelectedItem as Cliente,
                    producto = (Producto)this.txtProducto.Tag,
                    zonaSelect = (Zona)this.txtDestino.Tag,
                    viaje = ((Zona)this.txtDestino.Tag).viaje,
                    PorcIVA = ((Cliente)this.cbxClientes.SelectedItem).impuesto.valor,
                    PorcRetencion = ((Cliente)this.cbxClientes.SelectedItem).impuestoFlete.valor,
                    tipoPrecio = EstablecerPrecios.establecerPrecio(ctrRemolque1.unidadTransporte, ctrDolly.unidadTransporte, ctrRemolque2.unidadTransporte, base.mainWindow.empresa, (Cliente)this.cbxClientes.SelectedItem),
                    tipoPrecioChofer = EstablecerPrecios.establecerPrecioChofer(ctrRemolque1.unidadTransporte, ctrDolly.unidadTransporte, ctrRemolque2.unidadTransporte, base.mainWindow.empresa),
                    remision = this.txtRemision.Text.Trim(),
                    volumenCarga = this.chxTipoMedidaKilogramos.IsChecked.Value ? (Convert.ToDecimal(this.txtVolProgramado.Text.Trim()) / 1000M) : Convert.ToDecimal(this.txtVolProgramado.Text.Trim()),
                    volumenDescarga = this.chxTipoMedidaKilogramos.IsChecked.Value ? (string.IsNullOrEmpty(this.txtVolDescarga.Text.Trim()) ? decimal.Zero : (Convert.ToDecimal(this.txtVolDescarga.Text.Trim()) / 1000M)) : (string.IsNullOrEmpty(this.txtVolDescarga.Text.Trim()) ? decimal.Zero : Convert.ToDecimal(this.txtVolDescarga.Text.Trim())),
                    idTarea = this._idTareaProgramada
                };
                System.Windows.Controls.ListViewItem newItem = new System.Windows.Controls.ListViewItem
                {
                    Content = porte
                };
                this.lvlProductos.Items.Add(newItem);
                this.limpiarDetalles();
                this._idTareaProgramada = 0;
                this.sumarVolumenCargas();
                this.txtRemision.Focus();
            }
        }

        private Viaje mapForm()
        {
            try
            {
                Viaje viaje = new Viaje
                {
                    NumGuiaId = (this.txtFolio.Tag == null) ? 0 : ((Viaje)this.txtFolio.Tag).NumGuiaId,
                    EstatusGuia = "ACTIVO",                    
                    cliente = (Cliente)this.cbxClientes.SelectedItem,
                    operador = ctrChofer.personal, //(Personal)this.txtChofer.Tag,
                    operadorCapacitacion = ctrChoferCapacitacion.personal,// (Personal)this.txtChoferCapacitacion.Tag,
                    IdEmpresa = ctrEmpresa.empresaSelected.clave,
                    tractor = this.ctrTractor.unidadTransporte,
                    remolque = ctrRemolque1.unidadTransporte == null ? null : ctrRemolque1.unidadTransporte,
                    dolly = (ctrDolly.unidadTransporte == null) ? null : ctrDolly.unidadTransporte,
                    remolque2 = ctrRemolque2.unidadTransporte == null ? null : ctrRemolque2.unidadTransporte,
                    SerieGuia = "B",
                    FechaHoraViaje = this.txtFecha2.Value.Value,
                    FechaPrograma = (DateTime)txtFechaPrograma.Value,
                    UserViaje = base.mainWindow.inicio._usuario.idUsuario,
                    tipoViaje = this.validarIsFull() ? 'F' : 'S',
                    km = decimal.Zero,
                    cobroXKm = ((Cliente)this.cbxClientes.SelectedItem).cobroXkm,
                    ayudante = this.txtAyudante.personal,
                    taller = !chcEnviarTaller.IsChecked.Value ? false : rbnBase.IsChecked.Value,
                    tallerExterno = !chcEnviarTaller.IsChecked.Value ? false : rbnExterno.IsChecked.Value,
                    zonaOperativa = ctrZonaOperaiva.zonaOperativa,
                    FechaInicioCarga = dtpFechaCarga.Value,
                    noViaje = ctrNumero.valor,
                    editoNoViaje = this.editoNoViaje
                };
                viaje.listDetalles = new List<CartaPorte>();

                foreach (System.Windows.Controls.ListViewItem lvl in lvlProductos.Items)
                {
                    CartaPorte cp = lvl.Content as CartaPorte;
                    cp.tipoPrecio = EstablecerPrecios.establecerPrecio(ctrRemolque1.unidadTransporte, ctrDolly.unidadTransporte, ctrRemolque2.unidadTransporte, base.mainWindow.empresa, (Cliente)this.cbxClientes.SelectedItem);
                    cp.tipoPrecioChofer = EstablecerPrecios.establecerPrecioChofer(ctrRemolque1.unidadTransporte, ctrDolly.unidadTransporte, ctrRemolque2.unidadTransporte, base.mainWindow.empresa);
                }

                if (!chcEnviarTaller.IsChecked.Value)
                {
                    foreach (System.Windows.Controls.ListViewItem item in lvlProductos.Items)
                    {
                        viaje.listDetalles.Add((CartaPorte)item.Content);
                    }
                    if (viaje.cliente.cobroXkm)
                    {
                        viaje.listDetalles = (from s in viaje.listDetalles
                                              orderby s.zonaSelect.km descending
                                              select s).ToList<CartaPorte>();
                        for (int i = 0; i < viaje.listDetalles.Count; i++)
                        {
                            if (i == 0)
                            {
                                viaje.listDetalles[i].importeReal = (viaje.listDetalles[i].precio * viaje.listDetalles[i].zonaSelect.km) + viaje.listDetalles[i].precioFijo;
                            }
                            else
                            {
                                viaje.listDetalles[i].importeReal = decimal.Zero;
                            }
                            viaje.listDetalles[i].cobroXkm = true;
                        }
                    }
                    else if(viaje.listDetalles.Exists(s => s.viaje))
                    {
                        viaje.listDetalles = (from s in viaje.listDetalles
                                              orderby s.zonaSelect.km descending
                                              select s).ToList<CartaPorte>();
                        for (int i = 0; i < viaje.listDetalles.Count; i++)
                        {
                            if (i == 0)
                            {
                                viaje.listDetalles[i].importeReal = viaje.listDetalles[i].precioFijo;
                            }
                            else
                            {
                                viaje.listDetalles[i].importeReal = decimal.Zero;
                            }
                            viaje.listDetalles[i].viaje = true;
                        }
                    }
                    
                }

                return viaje;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void mapForm(Viaje result)
        {
            if (result != null)
            {
                this._viaje = result;
                this.txtFolio.Tag = result;
                this.txtFolio.Text = result.NumGuiaId.ToString();
                this.txtFecha2.Value = new DateTime?(result.FechaHoraViaje);                
            }
            else
            {
                this._viaje = null;
                this.txtFolio.Tag = null;
                this.txtFolio.Clear();
                this.txtFecha2.Value = new DateTime?(DateTime.Now);
                //this.cbxChofer.SelectedItem = null;
                //this.cbxChofer.IsEnabled = true;
                //this.cbxCamion.SelectedItem = null;
                //this.cbxCamion.IsEnabled = true;
                //this.cbxTolva1.SelectedItem = null;
                //this.cbxTolva1.IsEnabled = true;
                //this.cbxTolva2.SelectedItem = null;
                //this.cbxTolva2.IsEnabled = true;
                //this.cbxDolly.SelectedItem = null;
                //this.cbxDolly.IsEnabled = true;
                this.lvlProductos.Items.Clear();                
                this.list1.Clear();
                this.listaG.Clear();
                this.mapCamposTractor(null, "");
                this.mapCamposTolva1(null, "");
                this.mapCampostolva2(null, "");
                this.mapCamposDolly(null, "");
                this.mapChofer(null);
                this.mapChoferCapacitacion(null);
                this.txtAyudante.limpiar();
                //this.txtCargaTotal.Text = this.sumarTotalCargas().ToString("0.00");
                this.sumarVolumenCargas();
            }
        }

        private void mapProductos(Producto producto)
        {
            if (producto != null)
            {
                this.txtProducto.Tag = producto;
                this.txtProducto.Text = producto.descripcion;
                this.txtProducto.IsEnabled = false;
                this.txtDestino.Focus();
            }
            else
            {
                this.txtProducto.Tag = null;
                this.txtProducto.Clear();
                this.txtProducto.IsEnabled = true;
            }
        }

        public override void nuevo()
        {
            this.mapForm(null);
            this.establecerRutaXML();
            txtFechaPrograma.Value = DateTime.Now;
            base.nuevo();
            this.limpiarDetalles();
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
            this.ctrTractor.txtUnidadTrans.Focus();

            chcEnviarTaller.IsChecked = false;

            this.chcImprimir.IsChecked = new bool?(new Core.Utils.Util().isActivoImpresion());
            this.chcEnviarCorreo.IsChecked = new bool?(new Core.Utils.Util().isActivoCorreoCSI());
            this.chcTablero.IsChecked = new bool?(new Core.Utils.Util().isEventoTableroControlUnidades());

            dtpFechaCarga.Value = null;

            ctrNumero.valor = 0;
            ctrNumero.IsEnabled = false;
            editoNoViaje = false;
        }

        private void RFID_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                this.timer.Stop();
                this.pRFID_Secundario.RFID.Close();
                this.iniciar();
            }
            catch (Exception exception)
            {
                System.Windows.MessageBox.Show(exception.Message);
            }
        }

        private void RFID_DataReceived1(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                this.contador = new Core.Utils.Util().getTiempoEspera();
                this.DispString = this.pRFID.RFID.ReadLine();
                this.guardarLista(this.DispString);
            }
            catch (Exception)
            {
            }
        }

        private void selectProducto(List<Producto> listProductos, string producto)
        {
            //List<Producto> list = new List<Producto>();
            ////List<Producto> list2 = ((Cliente)this.txtFraccion.Tag).fraccion.listProductos.FindAll(S => S.descripcion.IndexOf(producto, StringComparison.OrdinalIgnoreCase) >= 0);
            //foreach (Producto producto3 in list2)
            //{
            //    list.Add(producto3);
            //}
            //WpfClient.selectProducto producto2 = new WpfClient.selectProducto(producto);
            //if (list.Count == 1)
            //{
            //    this.mapProductos(list[0]);
            //}
            //else
            //{
            //    Producto producto4 = producto2.buscarProductos(((Cliente)this.txtFraccion.Tag).fraccion.listProductos);
            //    this.mapProductos(producto4);
            //}
        }

        private void selectZona(List<Zona> _listZonas, string destino)
        {
            List<Zona> list = new List<Zona>();
            List<Zona> list2 = _listZonas.FindAll(S => S.descripcion.IndexOf(destino, StringComparison.OrdinalIgnoreCase) >= 0);
            foreach (Zona zona in list2)
            {
                list.Add(zona);
            }
            selectDestinos destinos = new selectDestinos(destino);
            if (list.Count == 1)
            {
                this.mapDestinos(list[0]);
            }
            else
            {
                Zona zona2 = destinos.buscarZona(_listZonas);
                this.mapDestinos(zona2);
            }
        }

        internal decimal sumarTotalCargas()
        {
            decimal tonelajeMax = 0;
            //decimal tonelajeMax = new decimal(0, 0, 0, false, 2);
            if (ctrRemolque1.unidadTransporte != null)
            {
                tonelajeMax = ctrRemolque1.unidadTransporte.tipoUnidad.TonelajeMax;
            }
            if (ctrRemolque2.unidadTransporte != null)
            {
                tonelajeMax += ctrRemolque2.unidadTransporte.tipoUnidad.TonelajeMax;
            }
            return tonelajeMax;
        }

        internal void sumarVolumenCargas()
        {
            decimal num = new decimal(0, 0, 0, false, 2);
            decimal num2 = new decimal(0, 0, 0, false, 2);
            decimal num3 = new decimal(0, 0, 0, false, 2);
            decimal impRetencion = new decimal(0, 0, 0, false, 2);
            foreach (System.Windows.Controls.ListViewItem item in (IEnumerable)this.lvlProductos.Items)
            {
                CartaPorte content = (CartaPorte)item.Content;
                num += content.volumenDescarga;
                num2 += content.importeReal;
                num3 += content.ImporIVA;
                impRetencion = content.ImpRetencion;
            }
            this.txtSumValProgramados.Text = num.ToString("0.00");
            this.txtTotalImporte.Text = num2.ToString("0.00");
            this.txtTotalRetencion.Text = num3.ToString("0.00");
            this.txtTotalFlete.Text = impRetencion.ToString("0.00");
            this.txtSumaTotal.Text = ((num3 + num2) - impRetencion).ToString("0.00");
        }
        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            this.sumarVolumenCargas();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.sumarVolumenCargas();
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
        }

        private void txtBusquedaRemision_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                int result = 0;
                if (int.TryParse(this.txtBusquedaRemision.Text.Trim(), out result))
                {
                    this.buscarRemision();
                }
                else
                {
                    System.Windows.MessageBox.Show("Se requiere una remisi\x00f3n valida", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }

        private void txtChofer_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            //if (e.Key == Key.Enter)
            //{
            //    string nombre = this.txtChofer.Text.Trim();
            //    List<Personal> list = this.buscarPersonal(nombre);
            //    if (list.Count == 1)
            //    {
            //        this.mapChofer(list[0]);
            //    }
            //    else if (list.Count == 0)
            //    {
            //        Personal personal = new selectPersonal(nombre, this.claveEmpresa).buscarOperadores(((Cliente)this.cbxClientes.SelectedItem).clave);
            //        this.mapChofer(personal);
            //    }
            //    else
            //    {
            //        Personal personal = new selectPersonal(nombre, this.claveEmpresa).buscarOperadores(((Cliente)this.cbxClientes.SelectedItem).clave);
            //        this.mapChofer(personal);
            //    }
            //}
        }

        private void txtChoferCapacitacion_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            //if (e.Key == Key.Enter)
            //{
            //    string nombre = this.txtChoferCapacitacion.Text.Trim();
            //    List<Personal> list = this.buscarPersonal(nombre);
            //    if (list.Count == 1)
            //    {
            //        this.mapChoferCapacitacion(list[0]);
            //    }
            //    else if (list.Count == 0)
            //    {
            //        Personal personal = new selectPersonal(nombre, this.claveEmpresa).buscarOperadores(((Cliente)this.cbxClientes.SelectedItem).clave);
            //        this.mapChoferCapacitacion(personal);
            //    }
            //    else
            //    {
            //        Personal personal = new selectPersonal(nombre, this.claveEmpresa).buscarOperadores(((Cliente)this.cbxClientes.SelectedItem).clave);
            //        this.mapChoferCapacitacion(personal);
            //    }
            //}
        }

        private void txtDestino_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                List<Zona> list = new List<Zona>();
                List<Zona> list2 = this._listZonas.FindAll(S => S.descripcion.IndexOf(this.txtDestino.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (Zona zona in list2)
                {
                    list.Add(zona);
                }
                selectDestinos destinos = new selectDestinos(this.txtDestino.Text);
                if (list.Count == 1)
                {
                    this.mapDestinos(list[0]);
                }
                else
                {
                    Zona zona2 = destinos.buscarZona(this._listZonas);
                    this.mapDestinos(zona2);
                }
            }
        }

        private void txtDolly_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string str = ctrDolly.txtUnidadTrans.Text.Trim(); // this.txtDolly.Text.Trim();
                if (!string.IsNullOrEmpty(str))
                {
                    this.completarClave(ref str);
                }
                List<UnidadTransporte> list = this.buscarUnidades("DL" + str);
                if (list != null)
                {
                    if (list.Count == 1)
                    {
                        if (this.validarDolly(list[0]))
                        {
                            this.mapCamposDolly(list[0], list[0].clave);
                        }
                    }
                    else if (list.Count == 0)
                    {
                        UnidadTransporte unidadTransporte = new selectUnidadTransporte("DL", this.claveEmpresa, str).buscarUnidades();
                        this.mapCamposDolly(unidadTransporte, (unidadTransporte == null) ? "" : unidadTransporte.clave);
                    }
                    else
                    {
                        UnidadTransporte unidadTransporte = new selectUnidadTransporte(list).selectUnidadTransporteFind();
                        this.mapCamposDolly(unidadTransporte, (unidadTransporte == null) ? "" : unidadTransporte.clave);
                    }
                }
            }
        }

        private void txtDolly_KeyDown_1(object sender, System.Windows.Input.KeyEventArgs e)
        {
        }

        private void txtDolly_KeyDown_2(object sender, System.Windows.Input.KeyEventArgs e)
        {
        }

        private void txtKM_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
            }
        }

        private void txtProducto_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                List<Producto> list = new List<Producto>();
                List<Producto> list2 = this.listaProductos.FindAll(S => S.descripcion.IndexOf(this.txtProducto.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (Producto producto2 in list2)
                {
                    list.Add(producto2);
                }
                WpfClient.selectProducto producto = new WpfClient.selectProducto(this.txtProducto.Text);
                if (list.Count == 1)
                {
                    this.mapProductos(list[0]);
                }
                else
                {
                    Producto producto3 = producto.buscarProductos(this.listaProductos);
                    this.mapProductos(producto3);
                }
            }
        }

        private void txtRemision_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && !string.IsNullOrEmpty(this.txtRemision.Text.Trim()))
            {
                int num = 0;
                if (int.TryParse(this.txtRemision.Text.Trim(), out num))
                {
                    OperationResult result = new TareasProgramadasSvc().getTareasProgramadas(((Cliente)this.cbxClientes.SelectedItem).clave, num);
                    if (result.typeResult == ResultTypes.success)
                    {
                        if (((List<TareaProgramada>)result.result).Count == 1)
                        {
                            this.addLista((List<TareaProgramada>)result.result);
                        }
                    }
                    else
                    {
                        this.txtProducto.Focus();
                    }
                }
                else
                {
                    System.Windows.MessageBox.Show("REMISION INVALIDA");
                }
            }
        }

        private void txtTolva1_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string str = ctrRemolque1.txtUnidadTrans.Text.Trim();
                if (!string.IsNullOrEmpty(str))
                {
                    this.completarClave(ref str);
                }
                List<UnidadTransporte> list = this.buscarUnidades("TV" + str);
                if (list != null)
                {
                    if (list.Count == 1)
                    {
                        if (this.validarTolva1(list[0]))
                        {
                            this.mapCamposTolva1(list[0], list[0].clave);
                        }
                    }
                    else if (list.Count == 0)
                    {
                        UnidadTransporte unidadTransporte = new selectUnidadTransporte("TV", this.claveEmpresa, str).buscarUnidades();
                        this.mapCamposTolva1(unidadTransporte, (unidadTransporte == null) ? "" : unidadTransporte.clave);
                    }
                    else
                    {
                        UnidadTransporte unidadTransporte = new selectUnidadTransporte(list).selectUnidadTransporteFind();
                        this.mapCamposTolva1(unidadTransporte, (unidadTransporte == null) ? "" : unidadTransporte.clave);
                    }
                }
            }
        }

        private void txtTolva2_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string str = ctrRemolque2.txtUnidadTrans.Text.Trim(); // this.txtTolva2.Text.Trim();
                if (!string.IsNullOrEmpty(str))
                {
                    this.completarClave(ref str);
                }
                List<UnidadTransporte> list = this.buscarUnidades("TV" + str);
                if (list != null)
                {
                    if (list.Count == 1)
                    {
                        if (this.validarTolva2(list[0]))
                        {
                            this.mapCampostolva2(list[0], list[0].clave);
                        }
                    }
                    else if (list.Count == 0)
                    {
                        UnidadTransporte unidadTransporte = new selectUnidadTransporte("TV", this.claveEmpresa, str).buscarUnidades();
                        this.mapCampostolva2(unidadTransporte, (unidadTransporte == null) ? "" : unidadTransporte.clave);
                    }
                    else
                    {
                        UnidadTransporte unidadTransporte = new selectUnidadTransporte(list).selectUnidadTransporteFind();
                        this.mapCampostolva2(unidadTransporte, (unidadTransporte == null) ? "" : unidadTransporte.clave);
                    }
                }
            }
        }

        private void txtTractor_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
        }

        private void txtVolDescarga_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && !string.IsNullOrEmpty(this.txtRemision.Text.Trim()))
            {
                this.mapDetalles();
            }
        }

        private void txtVolProgramado_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (this.omitirRemision)
                {
                    this.mapDetalles();
                }
                else if (!string.IsNullOrEmpty(this.txtRemision.Text.Trim()))
                {
                    this.txtVolDescarga.Focus();
                }
            }
        }

        private OperationResult validar()
        {
            bool correcto = true;
            List<string> list = new List<string>();
            if (this.cbxClientes.SelectedItem == null)
            {
                correcto = false;
                list.Add("Elegir un Cliente." + Environment.NewLine);
            }
            if (this.ctrTractor.unidadTransporte == null)
            {
                correcto = false;
                list.Add("Elegir el tractor." + Environment.NewLine);
            }
            if (ctrRemolque1.unidadTransporte == null)
            {
                if (!chcEnviarTaller.IsChecked.Value)
                {
                    correcto = false;
                    list.Add("Elegir el remolque 1." + Environment.NewLine);
                }
            }
            if (ctrChofer.personal == null)//(this.txtChofer.Tag == null)
            {
                correcto = false;
                list.Add("Elegir el chofer." + Environment.NewLine);
            }
            if ((ctrDolly.unidadTransporte != null) && (ctrRemolque2.unidadTransporte == null))
            {
                correcto = false;
                list.Add("Seleccionar el remolque 2." + Environment.NewLine);
            }
            if ((ctrRemolque2.unidadTransporte != null) && (ctrDolly.unidadTransporte == null))
            {
                correcto = false;
                list.Add("Seleccionar Dolly." + Environment.NewLine);
            }
            if ((this.txtAyudante.personal != null) && (ctrRemolque2.unidadTransporte == null))
            {
                correcto = false;
                list.Add("Solo los viajes en FULL pueden llevar ayudante." + Environment.NewLine);
            }
            if (this.validarLista())
            {
                correcto = false;
                list.Add("Existen errores en la lista, o faltan proporcionar valores." + Environment.NewLine);
            }
            string str = "Para continuar se requiere corregir lo siguiente:" + Environment.NewLine;
            foreach (string str2 in list)
            {
                str = str + str2;
            }
            return new OperationResult
            {
                valor = new int?(correcto ? 0 : 2),
                result = correcto,
                mensaje = str
            };
        }

        private bool validarDetalles()
        {
            int result = 0;
            if (!this.omitirRemision && !int.TryParse(this.txtRemision.Text.Trim(), out result))
            {
                this.txtRemision.Clear();
                this.txtRemision.Focus();
                return false;
            }
            if (this.txtProducto.Tag == null)
            {
                this.txtProducto.Clear();
                this.txtProducto.Focus();
                return false;
            }
            if (this.txtDestino.Tag == null)
            {
                this.txtDestino.Clear();
                this.txtDestino.Focus();
                return false;
            }
            decimal num2 = 0M;
            if (!decimal.TryParse(this.txtVolProgramado.Text.Trim(), out num2))
            {
                this.txtVolProgramado.Clear();
                this.txtVolProgramado.Focus();
                return false;
            }
            if (!this.omitirRemision)
            {
                if (!decimal.TryParse(this.txtVolDescarga.Text.Trim(), out num2))
                {
                    this.txtVolDescarga.Clear();
                    this.txtVolDescarga.Focus();
                    return false;
                }
                if (this.chxTipoMedidaKilogramos.IsChecked.Value)
                {
                    if (Convert.ToDecimal(this.txtVolDescarga.Text.Trim()) < 100M)
                    {
                        System.Windows.MessageBox.Show("Esta marcada la opcion de captura en kilogramos. Revisa tu valor capturado", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        this.txtVolDescarga.Focus();
                        return false;
                    }
                }
                else if (Convert.ToDecimal(this.txtVolDescarga.Text.Trim()) > 40M)
                {
                    System.Windows.MessageBox.Show("Esta marcada la opcion de captura en Toneladas. Revisa tu valor capturado", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    this.txtVolDescarga.Focus();
                    return false;
                }
            }
            return true;
        }

        private bool validarDolly(UnidadTransporte s) =>
            (s.tipoUnidad.clasificacion.ToUpper() == "DOLLY");

        private bool validarIsFull()
        {
            try
            {
                UnidadTransporte tag = ctrRemolque1.unidadTransporte;
                return (((ctrDolly.unidadTransporte != null) || (ctrRemolque2.unidadTransporte != null)) || ((tag.tipoUnidad.descripcion == "TOLVA 35 TON") && false));
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        private bool validarLista()
        {
            if (chcEnviarTaller.IsChecked.Value) return false;

            if (this.lvlProductos.Items.Count <= 0)
            {
                return true;
            }
            foreach (System.Windows.Controls.ListViewItem item in (IEnumerable)this.lvlProductos.Items)
            {
                CartaPorte content = (CartaPorte)item.Content;
                if (!this.omitirRemision)
                {
                    if (((string.IsNullOrEmpty(content.remision) || (content.volumenDescarga <= decimal.Zero)) || ((content.volumenCarga <= decimal.Zero) || (content.zonaSelect == null))) || (content.producto == null))
                    {
                        return true;
                    }
                }
                else if (((content.volumenCarga <= decimal.Zero) || (content.zonaSelect == null)) || (content.producto == null))
                {
                    return true;
                }
            }
            return false;
        }

        private bool validarTolva1(UnidadTransporte s) =>
            (s.tipoUnidad.clasificacion.ToUpper() == "TOLVA");

        private bool validarTolva2(UnidadTransporte s)
        {
            if (s.tipoUnidad.clasificacion.ToUpper() == "TOLVA")
            {
                if (s.clave == ctrRemolque1.txtUnidadTrans.Text)
                {
                    System.Windows.MessageBox.Show("Se esta duplicando el valor para el remolque 1");
                    return false;
                }
                return true;
            }
            return false;
        }

        private bool validarTractor(UnidadTransporte s) =>
            (s.tipoUnidad.clasificacion.ToUpper() == "TRACTOR");

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            Empresa empresa = mainWindow.usuario.empresa;
            Usuario usuario = mainWindow.usuario;

            ctrChofer.DataContextChanged += CtrChofer_DataContextChanged;

            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresa.loaded(empresa, new PrivilegioSvc().consultarPrivilegio("isAdmin", usuario.idUsuario).typeResult == ResultTypes.success);
            ctrZonaOperaiva.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
            ctrZonaOperaiva.cargarControl(usuario.zonaOperativa, usuario.idUsuario);

            this.chcOmitirRemisiones.IsChecked = true;
            this.chcOmitirRemisiones.Visibility = Visibility.Collapsed;
            OperationResult result = new PrivilegioSvc().consultarPrivilegio("HABILITAR_CHC_CAP_REMISIONES", base.mainWindow.usuario.idUsuario);
            this.habilitarChcRemisiones = result.typeResult == ResultTypes.success;
            if (new Core.Utils.Util().isActivoAntena())
            {
                this.pRFID_Secundario = new Puerto();
                Dictionary<string, object> dictionary = this.pRFID_Secundario.abrirPuerto();
                if (!((bool)dictionary["isOpen"]) && (System.Windows.MessageBox.Show(string.Format(dictionary["mensaje"].ToString() + Environment.NewLine + "\x00bfDesea continuar?", new object[0]), "Aviso", MessageBoxButton.YesNo, MessageBoxImage.Asterisk) == MessageBoxResult.No))
                {
                    this.pRFID_Secundario.RFID.Close();
                    this.controles.IsEnabled = false;
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    return;
                }
                this.pRFID_Secundario.RFID.DataReceived += new SerialDataReceivedEventHandler(this.RFID_DataReceived);
            }
            this.claveEmpresa = base.mainWindow.inicio._usuario.empresa.clave;
            OperationResult resp = new OperationResult();
            //resp = new ClienteSvc().getClientesALLorById(base.mainWindow.empresa.clave, 0, true);
            //if (resp.typeResult > ResultTypes.success)
            //{
            //    this.imprimeMensaje(resp);
            //    this.nuevo();
            //}
            //else
            //{
            //List<Cliente> listCliente = (List<Cliente>)resp.result;
            //this.mapComboClientes(listCliente);
            this.txtFecha2.Value = new DateTime?(DateTime.Now);
            this.cbxClientes.SelectedItem = this.cbxClientes.ItemsSource.Cast<Cliente>().ToList<Cliente>().Find(s => s.clave == base.mainWindow.usuario.cliente.clave);
            if (this.cbxClientes.SelectedItem != null)
            {
                OperationResult result4 = new PrivilegioSvc().consultarPrivilegio("ADMIN_COMBO_CLIENTES", base.mainWindow.usuario.idUsuario);
                this.cbxClientes.IsEnabled = result4.typeResult == ResultTypes.success;
            }
            this.nuevo();
            this.chxTipoMedidaKilogramos.IsChecked = true;
            if (new PrivilegioSvc().consultarPrivilegio("APLICAR_FECHA_VIAJE", base.mainWindow.inicio._usuario.idUsuario).typeResult == ResultTypes.success)
            {
                this.txtFecha2.IsEnabled = true;
            }
            else
            {
                this.txtFecha2.IsEnabled = false;
            }


            this.ctrTractor.DataContextChanged += CtrTractor_DataContextChanged;
            this.ctrRemolque1.DataContextChanged += CtrRemolque1_DataContextChanged;
            this.ctrDolly.DataContextChanged += CtrDolly_DataContextChanged;
            this.ctrRemolque2.DataContextChanged += CtrRemolque2_DataContextChanged;

            dtpFechaCarga.IsEnabled = new PrivilegioSvc().consultarPrivilegio("HABILITAR_FECHA_INICIO_CARGA", base.mainWindow.usuario.idUsuario).typeResult == ResultTypes.success;
            //}
        }

        private void CtrChofer_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            controlPersonal ctr = sender as controlPersonal;
            if (ctr.personal != null)
            {
                buscarNoViajeOperador();   
            }
            else
            {
                ctrNumero.IsEnabled = false;
                ctrNumero.valor = 0;
            }
        }
        public void buscarNoViajeOperador()
        {
            if (ctrChofer.personal != null)
            {
                DateTime fecha = txtFecha2.IsEnabled ? txtFecha2.Value.Value : DateTime.Now;
                var resp = new CartaPorteSvc().getNoViajeOperador(fecha, ctrChofer.personal.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    NoViajeOperador noViaje = resp.result as NoViajeOperador;

                    TimeSpan time = fecha - noViaje.fechaFin;

                    TimeSpan time2 = txtFecha2.Value.Value - noViaje.fechaFin;

                    if (time2.TotalHours < noViaje.horas)
                    {
                        noViaje.noViaje += 1;
                    }
                    else
                    {
                        noViaje.noViaje = 1;
                    }

                    ctrNumero.valor = noViaje.noViaje;
                    ctrNumero.IsEnabled = false;
                    editoNoViaje = false;
                }
                else
                {
                    ctrNumero.IsEnabled = false;
                    ctrNumero.valor = 0;
                }
            }
        }
        private void CtrRemolque2_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.ctrDolly.unidadTransporte != null)
            {
                //this.txtChofer.Focus();
                ctrChofer.txtPersonal.Focus();
            }
        }

        private void CtrDolly_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.ctrDolly.unidadTransporte != null)
            {
                this.ctrRemolque2.txtUnidadTrans.Focus();
            }
        }

        private void CtrRemolque1_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.ctrRemolque1.unidadTransporte != null)
            {
                this.ctrDolly.txtUnidadTrans.Focus();
            }
        }

        private void CtrTractor_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                Cursor = System.Windows.Input.Cursors.Wait;
                if (this.ctrTractor.unidadTransporte != null)
                {
                    this.ctrRemolque1.txtUnidadTrans.Focus();
                    var resp = new CartaPorteSvc().getUltimaFechaEntradaPlanta(ctrTractor.unidadTransporte.clave, txtFecha2.Value);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        dtpFechaCarga.Value = (DateTime?)resp.result;
                    }
                    else
                    {
                        dtpFechaCarga.Value = DateTime.Now;
                    }
                }
                else
                {
                    this.ctrTractor.txtUnidadTrans.Focus();
                    dtpFechaCarga.Value = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }
        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cargarClientesByZonaOperativa();
        }

        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cargarClientesByZonaOperativa();
        }
        void cargarClientesByZonaOperativa()
        {
            try
            {
                Cursor = System.Windows.Input.Cursors.Wait;
                cbxClientes.ItemsSource = null;
                Empresa empresa = ctrEmpresa.empresaSelected;
                if (empresa.clave == 1)
                {
                    if (ctrZonaOperaiva.zonaOperativa == null) return;

                    var resp = new ClienteSvc().getClientesByZonasOperativas(new List<string> { ctrZonaOperaiva.zonaOperativa.idZonaOperativa.ToString() }, empresa.clave);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        var listaOperaciones = resp.result as List<OperacionFletera>;
                        List<Cliente> listaCliente = listaOperaciones.Select(s => s.cliente).ToList();
                        cbxClientes.ItemsSource = listaCliente;
                        if (listaOperaciones.Count == 1)
                        {
                            cbxClientes.SelectedIndex = 0;
                        }
                    }
                    else if (resp.typeResult != ResultTypes.recordNotFound)
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
                else
                {
                    var resp = new ClienteSvc().getClientesALLorById(empresa.clave, 0, true);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        var listaClientes = resp.result as List<Cliente>;
                        cbxClientes.ItemsSource = listaClientes;
                        if (listaClientes.Count == 1)
                        {
                            cbxClientes.SelectedIndex = 0;
                        }
                    }
                    else if (resp.typeResult != ResultTypes.recordNotFound)
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }

                
            }
            catch (Exception ex)
            {

            }
            finally
            {
                Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }

        private void chcEnviarTaller_Checked(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.CheckBox checkBox = sender as System.Windows.Controls.CheckBox;
            if (checkBox != null)
            {
                if (checkBox.IsChecked.Value)
                {
                    stpTalleres.IsEnabled = true;
                    gbxCartaPortes.IsEnabled = false;
                    stpComportamientos.IsEnabled = false;
                    chcImprimir.IsChecked = false;
                    chcEnviarCorreo.IsChecked = false;
                    chcTablero.IsChecked = true;
                    txtFechaPrograma.IsEnabled = false;
                }
                else
                {
                    stpTalleres.IsEnabled = false;
                    gbxCartaPortes.IsEnabled = true;
                    stpComportamientos.IsEnabled = true;
                    txtFechaPrograma.IsEnabled = true;
                }
            }

        }
        private void TxtAyudante_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void CbOrigen_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void BtnLimpiarTc_Click(object sender, RoutedEventArgs e)
        {
            ctrTractor.limpiar();
        }

        private void BtnLimpiarRem1_Click(object sender, RoutedEventArgs e)
        {
            ctrRemolque1.limpiar();
        }

        private void BtnLimpiarDolly_Click(object sender, RoutedEventArgs e)
        {
            ctrDolly.limpiar();
        }

        private void BtnLimpiarRem2_Click(object sender, RoutedEventArgs e)
        {
            ctrRemolque2.limpiar();
        }

        private void BtnLimpiarOperador_Click(object sender, RoutedEventArgs e)
        {
            ctrChofer.limpiar();
        }
        private void BtnLimpiarOperadorCap_Click(object sender, RoutedEventArgs e)
        {
            ctrChoferCapacitacion.limpiar();
        }
        private void BtnLimpiarOperadorFull_Click(object sender, RoutedEventArgs e)
        {
            txtAyudante.limpiar();
        }
        bool editoNoViaje = false;
        private void BtnLimpiarCont_Click(object sender, RoutedEventArgs e)
        {
            //ctrNumero.valor = 0;
            ctrNumero.IsEnabled = true;
            editoNoViaje = true;
        }

        private void txtFecha2_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (txtFecha2.Value != null)
            {
                buscarNoViajeOperador();
            }
        }
    }
}
