﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using Core.Interfaces;
using CoreFletera.Models;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for editAjusteViajes.xaml
    /// </summary>
    public partial class editAjusteViajes : ViewBase
    {
        private Viaje _viaje = null;
        public editAjusteViajes()
        {
            InitializeComponent();
        }
        int claveEmpresa = 0;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e) //GRCC
        {
            mapComboMotivos();
            cbxTipoViajeFalse.SelectionChanged += CbxTipoViajeFalse_SelectionChanged;
            stpObs.Visibility = Visibility.Hidden;

            txtVolDescarga.txtDecimal.TextChanged += TxtDecimal_TextChanged;
            txtVolDescargaReal.txtDecimal.TextChanged += TxtDecimal_TextChanged;

            OperationResult result = new ClienteSvc().getClientesALLorById(base.mainWindow.inicio._usuario.personal.empresa.clave, 0, false);
            this.claveEmpresa = base.mainWindow.inicio._usuario.personal.empresa.clave;
            this.txtClave.txtEntero.KeyDown += new KeyEventHandler(this.txtClave_KeyDown);
            this.txtPrecio.txtDecimal.KeyDown += new KeyEventHandler(this.TxtDecimal_KeyDown);
            if (new PrivilegioSvc().consultarPrivilegio("VER_PRECIOS_AJUSTE_VIAJE", base.mainWindow.usuario.idUsuario).typeResult == ResultTypes.success)
            {
                GridView view = this.lvlCrtaPortes.View as GridView;
                GridViewColumnCollection columns = view.Columns;
                GridViewColumn itemPrecio = new GridViewColumn
                {
                    Header = "Precio",
                    Width = 90
                };
                Binding binding1 = new Binding("precio")
                {
                    StringFormat = "C4"
                };
                itemPrecio.DisplayMemberBinding = binding1;
                view.Columns.Add(itemPrecio);

                GridViewColumn itemPrecioFijo = new GridViewColumn
                {
                    Header = "Precio Fijo",
                    Width = 90
                };
                Binding bindingPrecioFijo = new Binding("precioFijo")
                {
                    StringFormat = "C4"
                };
                itemPrecioFijo.DisplayMemberBinding = bindingPrecioFijo;
                view.Columns.Add(itemPrecioFijo);

                GridViewColumn itemImporteIVA = new GridViewColumn
                {
                    Header = "Importe I.V.A",
                    Width = 110.0
                };
                Binding binding2 = new Binding("ImporIVA")
                {
                    StringFormat = "C4"
                };
                itemImporteIVA.DisplayMemberBinding = binding2;
                view.Columns.Add(itemImporteIVA);

                GridViewColumn itemImporteRetencion = new GridViewColumn
                {
                    Header = "Retenci\x00f3n",
                    Width = 110.0
                };
                Binding binding3 = new Binding("ImpRetencion")
                {
                    StringFormat = "C4"
                };
                itemImporteRetencion.DisplayMemberBinding = binding3;
                view.Columns.Add(itemImporteRetencion);

                GridViewColumn itemImporte = new GridViewColumn
                {
                    Header = "Importe",
                    Width = 120.0
                };
                Binding binding4 = new Binding("importeReal")
                {
                    StringFormat = "C4"
                };
                itemImporte.DisplayMemberBinding = binding4;
                view.Columns.Add(itemImporte);

                this.stpPrecio.Visibility = Visibility.Visible;

                this.stpPrecioFijo.Visibility = Visibility.Visible;
            }
            else
            {
                this.stpPrecio.Visibility = Visibility.Collapsed;
            }
            if (new PrivilegioSvc().consultarPrivilegio("MARCAR_VIAJE_EN_FALSO", base.mainWindow.usuario.idUsuario).typeResult == ResultTypes.success)
            {
                this.stpFalso.Visibility = Visibility.Visible;
            }
            else
            {
                this.stpFalso.Visibility = Visibility.Hidden;
            }
            cbxTipoViajeFalse.Items.Add(eImputable.CLIENTE);
            cbxTipoViajeFalse.Items.Add(eImputable.PEGASO);

            cbxEstatusProducto.Items.Add(EstatusProducto.REPROCESO);
            cbxEstatusProducto.Items.Add(EstatusProducto.REDESTINO);
            cbxEstatusProducto.Items.Add(EstatusProducto.REDESTINO_MISMA_GRANJA);
            cbxEstatusProducto.SelectedIndex = 0;



            cbxTipoViajeFalse.SelectedIndex = 0;
            this.nuevo();
            stpCampoImputable.Visibility = Visibility.Hidden;
        }

        private void CbxTipoViajeFalse_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender != null)
            {
                eImputable tipo = (eImputable)(sender as ComboBox).SelectedItem;
                cbxMotivoViaje.ItemsSource = listaMotivos.FindAll(s => s.imputable == tipo);
                if (cbxMotivoViaje.Items.Count > 0)
                {
                    cbxMotivoViaje.SelectedIndex = 0;
                }
            }
        }

        private List<MotivoViajeFalso> listaMotivos = new List<MotivoViajeFalso>();
        private void mapComboMotivos()
        {
            var resp = new MotivosViajeFalsoSvc().getMotivosAllOrById();
            if (resp.typeResult == ResultTypes.success)
            {
                listaMotivos = (List<MotivoViajeFalso>)resp.result;
            }
        }
        private void TxtDecimal_TextChanged(object sender, TextChangedEventArgs e)
        {
            txtSinDescargas.valor = txtVolDescarga.valor - txtVolDescargaReal.valor;
        }

        private void TxtDecimal_KeyDown(object sender, KeyEventArgs e) //GRCC
        {
            if (e.Key == Key.Enter)
            {
                txtVolDescarga.txtDecimal.Focus();
            }
        }

        public override void nuevo() //GRCC
        {
            this.mapForm(null);
            base.nuevo();
            this.chcViajeEnFalso.IsChecked = false;
            this.stpVolReal.Visibility = Visibility.Hidden;
            chcExportar.Visibility = Visibility.Hidden;
            GridView view = (GridView)this.lvlCrtaPortes.View;
            List<GridViewColumn> list = view.Columns.Cast<GridViewColumn>().ToList<GridViewColumn>();
            view.Columns.Remove(list.Find(s => s.Header.ToString() == "Cant Real"));
            this.stpVolReal.Visibility = Visibility.Hidden;
            base.mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            dtFechaInicioCarga.Value = null;
            dtFechaPrograma.Value = null;
            dtFechaFin.Value = null;
            //ctrEmpresa.empresaSelected= null;
            ctrZonaOperativa.zonaOperativa = null;
            txtCliente.Clear();
            stpNoCambiar.IsEnabled = false;
            //chcOperadorFin.IsChecked = false;
            stpObs.Visibility = Visibility.Hidden;
            txtObsFalso.Clear();
        }

        public override OperationResult guardar() //GRCC
        {
            try
            {
                Cursor = Cursors.Wait;
                if (this.txtOperador.Tag == null)
                {
                    return new OperationResult
                    {
                        valor = 2,
                        mensaje = "Se requiere el operador"
                    };
                }
                Viaje viaje = this.mapForm();
                OperationResult result = new CartaPorteSvc().saveAjusteViaje(ref viaje, (viaje.OperadorFinViaje == null ? null : (int?)viaje.OperadorFinViaje.clave));
                if (result.typeResult == ResultTypes.success)
                {
                    this.buscarViaje(viaje.NumGuiaId);
                    if (chcViajeEnFalso.IsChecked.Value && chcViajeEnFalso.Visibility == Visibility.Visible)
                    {
                        //if(chcExportar.IsChecked.Value)  
                        new ReportView().exportarViajeEnFalso(_viaje, mainWindow.usuario.personal.nombre);
                    }
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        public override void buscar() //GRCC
        {
            selectCartaPorte select = new selectCartaPorte();
            var viaje = select.buscarCartaPorte();
            if (viaje != null)
            {
                mapForm(viaje);
                base.buscar();
                mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.editar | ToolbarCommands.cerrar);
            }
            else
            {
                nuevo();
            }

        }
        private Viaje mapForm() //GRCC
        {
            try
            {
                Viaje viaje = (Viaje)this.txtClave.Tag;
                viaje.operador = (Personal)this.txtOperador.Tag;
                viaje.operadorCapacitacion = (this.txtOperador2.Tag != null) ? ((Personal)this.txtOperador2.Tag) : null;
                DateTime? nullable = this.dtFecha.Value;
                viaje.FechaHoraViaje = nullable.Value;
                viaje.FechaInicioCarga = dtFechaInicioCarga.Value;
                viaje.FechaPrograma = dtFechaPrograma.Value;
                viaje.FechaHoraFin = (((Viaje)this.txtClave.Tag).EstatusGuia == "FINALIZADO") ? this.dtFechaFin.Value : null;
                viaje.usuarioCambia = base.mainWindow.usuario.nombreUsuario;
                viaje.viajeFalso = this.chcViajeEnFalso.IsChecked.Value;
                viaje.ayudante = (this.txtAyudante.Tag == null) ? null : (this.txtAyudante.Tag as Personal);
                viaje.listDetalles = new List<CartaPorte>();
                viaje.OperadorFinViaje = txtOperadorFin.Tag == null ? null : (txtOperadorFin.Tag as Personal);
                viaje.noViaje = ctrNoViajeOperador.valor;
                foreach (ListViewItem item in lvlCrtaPortes.Items)
                {
                    viaje.listDetalles.Add((CartaPorte)item.Content);
                }
                //foreach (ListViewItem item2 in lvlCrtaPortesBaja.Items)
                //{
                //    viaje.listDetalles.Add((CartaPorte)item2.Content);
                //}
                foreach (var item in wrapRemisionesBaja.Children)
                {
                    viaje.listDetalles.Add((item as Border).Tag as CartaPorte);
                }
                if (viaje.cobroXKm)
                {
                    List<CartaPorte> listaCartaPortes = viaje.listDetalles.FindAll(s => !s.baja);
                    List<CartaPorte> list2 = viaje.listDetalles.FindAll(s => s.baja);
                    listaCartaPortes = (from s in listaCartaPortes
                                        orderby s.zonaSelect.km descending
                                        select s).ToList<CartaPorte>();
                    if (!this.chcViajeEnFalso.IsChecked.Value)
                    {
                        foreach (CartaPorte porte in listaCartaPortes)
                        {
                            porte.vDescarga = porte.volumenDescarga;
                        }
                    }
                    decimal num = listaCartaPortes.Sum<CartaPorte>(s => s.volumenDescarga);
                    decimal num2 = listaCartaPortes.Sum<CartaPorte>(s => s.vDescarga);
                    if (num2 != num)
                    {
                        for (int i = 0; i < listaCartaPortes.Count; i++)
                        {
                            if (i == 0)
                            {
                                decimal num4 = (num2 * 100M) / num;
                                decimal importeReal = (listaCartaPortes[i].precio * listaCartaPortes[i].zonaSelect.km) + listaCartaPortes[i].precioFijo;
                                decimal nuevoImporte = (importeReal * num4) / 100M;
                                listaCartaPortes[i].importeReal = (eImputable)cbxTipoViajeFalse.SelectedItem == eImputable.PEGASO ? nuevoImporte : importeReal;
                            }
                            else
                            {
                                listaCartaPortes[i].importeReal = decimal.Zero;
                            }
                            listaCartaPortes[i].cobroXkm = true;
                            listaCartaPortes[i].viajeFalso = this.chcViajeEnFalso.IsChecked.Value;
                        }
                    }
                    else
                    {
                        for (int i = 0; i < listaCartaPortes.Count; i++)
                        {
                            if (i == 0)
                            {
                                listaCartaPortes[i].importeReal = (listaCartaPortes[i].precio * listaCartaPortes[i].zonaSelect.km) + listaCartaPortes[i].precioFijo;
                            }
                            else
                            {
                                listaCartaPortes[i].importeReal = decimal.Zero;
                            }
                            listaCartaPortes[i].cobroXkm = true;
                            listaCartaPortes[i].viajeFalso = this.chcViajeEnFalso.IsChecked.Value;
                        }
                    }
                    viaje.listDetalles = new List<CartaPorte>();
                    foreach (CartaPorte porte2 in listaCartaPortes)
                    {
                        viaje.listDetalles.Add(porte2);
                    }
                    foreach (CartaPorte porte3 in list2)
                    {
                        viaje.listDetalles.Add(porte3);
                    }
                }
                else if (viaje.listDetalles.Exists(s => s.viaje))
                {
                    List<CartaPorte> listaCartaPortes = viaje.listDetalles.FindAll(s => !s.baja);
                    List<CartaPorte> list2 = viaje.listDetalles.FindAll(s => s.baja);
                    listaCartaPortes = (from s in listaCartaPortes
                                        orderby s.zonaSelect.km descending
                                        select s).ToList<CartaPorte>();
                    if (!this.chcViajeEnFalso.IsChecked.Value)
                    {
                        foreach (CartaPorte porte in listaCartaPortes)
                        {
                            porte.vDescarga = porte.volumenDescarga;
                        }
                    }
                    decimal sumaDespachado = listaCartaPortes.Sum<CartaPorte>(s => s.volumenDescarga);
                    decimal sumaCargado = listaCartaPortes.Sum<CartaPorte>(s => s.vDescarga);
                    if (sumaCargado != sumaDespachado)
                    {
                        for (int i = 0; i < listaCartaPortes.Count; i++)
                        {
                            if (i == 0)
                            {
                                decimal porcentaje = (sumaCargado * 100M) / sumaDespachado;
                                decimal importeReal = listaCartaPortes[i].precioFijo;
                                decimal nuevoImporte = (importeReal * porcentaje) / 100M;
                                listaCartaPortes[i].importeReal = (eImputable)cbxTipoViajeFalse.SelectedItem == eImputable.PEGASO ? nuevoImporte : importeReal;
                            }
                            else
                            {
                                listaCartaPortes[i].importeReal = decimal.Zero;
                            }
                            listaCartaPortes[i].viaje = true;
                            listaCartaPortes[i].viajeFalso = this.chcViajeEnFalso.IsChecked.Value;
                        }
                    }
                    else
                    {
                        for (int i = 0; i < listaCartaPortes.Count; i++)
                        {
                            if (i == 0)
                            {
                                listaCartaPortes[i].importeReal = listaCartaPortes[i].precioFijo;
                            }
                            else
                            {
                                listaCartaPortes[i].importeReal = decimal.Zero;
                            }
                            listaCartaPortes[i].viaje = true;
                            listaCartaPortes[i].viajeFalso = this.chcViajeEnFalso.IsChecked.Value;
                        }
                    }
                    viaje.listDetalles = new List<CartaPorte>();
                    foreach (CartaPorte porte2 in listaCartaPortes)
                    {
                        viaje.listDetalles.Add(porte2);
                    }
                    foreach (CartaPorte porte3 in list2)
                    {
                        viaje.listDetalles.Add(porte3);
                    }
                }
                else
                {
                    foreach (var item in viaje.listDetalles)
                    {
                        if (this.chcViajeEnFalso.IsChecked.Value)
                        {
                            if ((eImputable)cbxTipoViajeFalse.SelectedItem == eImputable.PEGASO)
                            {
                                item.importeReal = item.precio * item.vDescarga;
                            }
                            else
                            {
                                item.importeReal = item.precio * item.volumenDescarga;
                            }
                            item.viajeFalso = true;
                        }
                        else
                        {
                            item.vDescarga = item.volumenDescarga;
                            item.importeReal = item.precio * item.volumenDescarga;
                            item.viajeFalso = false;
                        }

                    }
                }

                if (this.chcViajeEnFalso.IsChecked.Value)
                {
                    viaje.imputable = (int)(eImputable)cbxTipoViajeFalse.SelectedItem;
                }
                else
                {
                    viaje.imputable = null;
                }

                foreach (var item in viaje.listDetalles)
                {
                    item.tipoPrecioChofer = EstablecerPrecios.establecerPrecioChofer(viaje.remolque, viaje.dolly, viaje.remolque2, (new Empresa { clave = viaje.IdEmpresa }));
                }

                if (chcViajeEnFalso.IsChecked.Value)
                {
                    viaje.estatusProducto = (EstatusProducto)cbxEstatusProducto.SelectedItem;
                    viaje.obsViajeFalso = txtObsFalso.Text.Trim();
                    MotivoViajeFalso motivo = cbxMotivoViaje.SelectedItem as MotivoViajeFalso;
                    viaje.motivoViajeFalso = motivo.motivo;
                    viaje.idMotivoViajeFalso = motivo.idMotivo;
                }
                else
                {
                    viaje.estatusProducto = EstatusProducto.NINGUNO;
                    viaje.obsViajeFalso = string.Empty;
                    viaje.motivoViajeFalso = string.Empty;
                    viaje.idMotivoViajeFalso = null;
                }
                foreach (var item in viaje.listDetalles)
                {
                    item.idCargaGasolina = ctrNoVale.valor;
                }
                return viaje;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private bool validarIsFull() //GRCC
        {
            UnidadTransporte x = (UnidadTransporte)txtTolva1.Tag;//cbxTolva1.SelectedItem;
            if (txtDolly.Tag != null || txtTolva2.Tag != null/*cbxDolly.SelectedItem != null || cbxTolva2.SelectedItem != null*/)
            {
                return true;
            }
            if (x.tipoUnidad.descripcion == "TOLVA 35 TON")
            {
                return false;
            }

            return false;
        }

        private void imprimirMensaje(OperationResult resp) //GRCC
        {
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    break;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    break;
                case ResultTypes.recordNotFound:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    break;
                default:
                    break;
            }
        }
        public override void editar() //GRCC
        {

            base.editar();
            stpNoCambiar.IsEnabled = false;
        }
        private void txtClave_KeyDown(object sender, KeyEventArgs e) //GRCC
        {
            if (e.Key == Key.Enter)
            {
                buscarViaje(txtClave.valor);
            }
        }

        private void mapForm(Viaje viaje) //GRCC
        {
            if (viaje != null)
            {
                ctrEmpresa.loaded(new Empresa { clave = viaje.IdEmpresa }, false);
                ctrZonaOperativa.cargarControl(viaje.zonaOperativa, viaje.UserViaje);
                txtCliente.Text = viaje.cliente.nombre;
                this._viaje = viaje;
                this.claveEmpresa = viaje.IdEmpresa;
                this.txtClave.Tag = viaje;
                this.txtClave.valor = viaje.NumGuiaId;
                this.txtClave.IsEnabled = false;
                this.cbxClientes.Tag = viaje.cliente;
                this.cbxClientes.Text = viaje.cliente.nombre;
                this.dtFecha.Value = new DateTime?(viaje.FechaHoraViaje);
                this.mapOperador(viaje.operador);
                this.mapOperador2(viaje.operadorCapacitacion);
                this.mapAyudante(viaje.ayudante);
                mapOperadorFinViaje(viaje.OperadorFinViaje);
                this.mapUnidades(TipoUnidad.TRACTOR, viaje.tractor);
                this.mapUnidades(TipoUnidad.TOLVA, viaje.remolque);
                this.mapUnidades(TipoUnidad.DOLLY, viaje.dolly);
                this.mapUnidades(TipoUnidad.TOLVA_2, viaje.remolque2);
                this.stpNoCambiar.IsEnabled = false;
                ctrNoViajeOperador.valor = viaje.noViaje;
                //this.lvlCrtaPortesBaja.Items.Clear();
                wrapRemisionesBaja.Children.Clear();
                eEstatus fINALIZADO = eEstatus.FINALIZADO;
                dtFechaPrograma.Value = viaje.FechaPrograma;
                dtFechaInicioCarga.Value = viaje.FechaInicioCarga;
                if (viaje.EstatusGuia.ToString() == fINALIZADO.ToString())
                {
                    this.dtFechaFin.Value = viaje.FechaHoraFin;
                    this.dtFechaFin.IsEnabled = true;
                }
                else
                {
                    this.dtFechaFin.Value = null;
                    this.dtFechaFin.IsEnabled = false;
                }
                OperationResult result = new ProductoSvc().getALLProductosByidFraccionEmpresa(viaje.IdEmpresa, viaje.cliente.clave);// ((Cliente)this.cbxClientes.Tag).clave, 0);
                if (result.typeResult == ResultTypes.success)
                {
                    this.listaProductos = result.result as List<Producto>;
                }
                else
                {
                    this.listaProductos = new List<Producto>();
                }
                OperationResult result2 = new ZonasSvc().getZonasALLorById(0, ((Cliente)this.cbxClientes.Tag).clave.ToString());
                if (result2.typeResult == ResultTypes.success)
                {
                    this._listZonas = result2.result as List<Zona>;
                }
                else
                {
                    this._listZonas = new List<Zona>();
                }
                this.mapDetalles(viaje.listDetalles);
                this.chcViajeEnFalso.IsChecked = new bool?(viaje.viajeFalso);
                this.stpAyudante.IsEnabled = viaje.remolque2 != null;
                txtObsFalso.Text = viaje.obsViajeFalso;
                cbxEstatusProducto.SelectedItem = viaje.estatusProducto;


                if (viaje.viajeFalso)
                {
                    cbxTipoViajeFalse.SelectedItem = (eImputable)viaje.imputable;
                }
                else
                {
                    cbxTipoViajeFalse.SelectedIndex = 0;
                }

                if (viaje.idMotivoViajeFalso != null)
                {
                    cbxMotivoViaje.SelectedItem = cbxMotivoViaje.ItemsSource.Cast<MotivoViajeFalso>().ToList().Find(s => s.idMotivo == viaje.idMotivoViajeFalso.Value);
                }
                else
                {
                    cbxMotivoViaje.SelectedIndex = 0;
                }
                int idVale = viaje.listDetalles.Select(s => s.idCargaGasolina).First();
                if (idVale != 0)
                    buscarValeCombustible(idVale);
            }
            else
            {
                this._viaje = null;
                this.txtClave.Tag = null;
                this.txtClave.limpiar();
                this.txtClave.IsEnabled = true;
                this.cbxClientes.Tag = null;
                this.cbxClientes.Clear();
                this.dtFecha.Value = new DateTime?(DateTime.Now);
                this.mapOperador(null);
                this.mapOperador2(null);
                mapAyudante(null);
                mapOperadorFinViaje(null);
                this.mapUnidades(TipoUnidad.TRACTOR, null);
                this.mapUnidades(TipoUnidad.TOLVA, null);
                this.mapUnidades(TipoUnidad.DOLLY, null);
                this.mapUnidades(TipoUnidad.TOLVA_2, null);
                this.lvlCrtaPortes.Items.Clear();
                //this.lvlCrtaPortesBaja.Items.Clear();
                wrapRemisionesBaja.Children.Clear();
                this.stpNoCambiar.IsEnabled = true;
                this.limpiarDetalles();
                this._listZonas = new List<Zona>();
                this.listaProductos = new List<Producto>();
                mapFormVale(null);
                ctrNoViajeOperador.valor = 0;
            }
        }

        private void mapDetalles(List<CartaPorte> listDetalles) //GRCC
        {
            lvlCrtaPortes.Items.Clear();
            foreach (var item in listDetalles)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                lvl.Selected += Lvl_Selected;
                lvl.Content = item;
                lvlCrtaPortes.Items.Add(lvl);
            }
        }

        private void Lvl_Selected(object sender, RoutedEventArgs e) //GRCC
        {
            ListViewItem item = sender as ListViewItem;
            CartaPorte content = (CartaPorte)item.Content;
            this.mapDetalle(content);
        }
        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e) //GRCC
        {
            CartaPorte content = (CartaPorte)((ListViewItem)this.lvlCrtaPortes.SelectedItem).Content;
            this.mapDetalle(content);
        }

        private void mapUnidades(TipoUnidad tipo, UnidadTransporte Unidad) //GRCC
        {
            switch (tipo)
            {
                case TipoUnidad.TRACTOR:
                    if (Unidad != null)
                    {
                        txtTractor.Tag = Unidad;
                        txtTractor.Text = Unidad.clave;
                    }
                    else
                    {
                        txtTractor.Tag = null;
                        txtTractor.Clear();
                    }
                    break;
                case TipoUnidad.TOLVA:
                    if (Unidad != null)
                    {
                        txtTolva1.Tag = Unidad;
                        txtTolva1.Text = Unidad.clave;
                    }
                    else
                    {
                        txtTolva1.Tag = null;
                        txtTolva1.Clear();
                    }
                    break;
                case TipoUnidad.DOLLY:
                    if (Unidad != null)
                    {
                        txtDolly.Tag = Unidad;
                        txtDolly.Text = Unidad.clave;
                    }
                    else
                    {
                        txtDolly.Tag = null;
                        txtDolly.Clear();
                    }
                    break;
                case TipoUnidad.TOLVA_2:
                    if (Unidad != null)
                    {
                        txtTolva2.Tag = Unidad;
                        txtTolva2.Text = Unidad.clave;
                    }
                    else
                    {
                        txtTolva2.Tag = null;
                        txtTolva2.Clear();
                    }
                    break;
                default:
                    break;
            }
        }

        private void mapOperador(Personal operador) //GRCC
        {
            if (operador != null)
            {
                txtOperador.Tag = operador;
                txtOperador.Text = operador.nombre;
                txtOperador.IsEnabled = false;
            }
            else
            {
                txtOperador.Tag = null;
                txtOperador.Text = "";
                txtOperador.IsEnabled = true;
            }
        }

        private void txtOperador_KeyDown(object sender, KeyEventArgs e) //GRCC
        {
            if ((e.Key == Key.Enter) && (this.txtClave.Tag != null))
            {
                string nombre = this.txtOperador.Text.Trim();
                List<Personal> list = this.buscarPersonal(nombre);
                if (list.Count == 1)
                {
                    this.mapOperador(list[0]);
                }
                else if (list.Count == 0)
                {
                    Personal operador = new selectPersonal(nombre, ((Viaje)this.txtClave.Tag).IdEmpresa).buscarPersonal();
                    this.mapOperador(operador);
                }
                else
                {
                    Personal operador = new selectPersonal(nombre, ((Viaje)this.txtClave.Tag).IdEmpresa).buscarPersonal();
                    this.mapOperador(operador);
                }
            }
        }
        private List<Personal> buscarPersonal(string nombre) //GRCC
        {
            OperationResult resp = new OperadorSvc().getOperadoresByNombre(nombre);
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    return (List<Personal>)resp.result;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return null;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return null;
                case ResultTypes.recordNotFound:
                    return new List<Personal>();

                default:
                    return null;
            }
        }

        private void txtOperador_KeyUp(object sender, KeyEventArgs e) //GRCC
        {
            if (e.Key == Key.Back || e.Key == Key.Delete)
            {
                mapOperador(null);
            }
        }

        enum TipoUnidad
        {
            TRACTOR = 0,
            TOLVA = 1,
            DOLLY = 2,
            TOLVA_2 = 3
        }

        private void txtTractor_KeyUp(object sender, KeyEventArgs e) //GRCC
        {
            if (e.Key == Key.Back || e.Key == Key.Delete)
            {
                mapUnidades(TipoUnidad.TRACTOR, null);
            }
        }

        private void txtTolva1_KeyUp(object sender, KeyEventArgs e) //GRCC
        {
            if (e.Key == Key.Back || e.Key == Key.Delete)
            {
                mapUnidades(TipoUnidad.TOLVA, null);
            }
        }

        private void txtDolly_KeyUp(object sender, KeyEventArgs e) //GRCC
        {
            if (e.Key == Key.Back || e.Key == Key.Delete)
            {
                mapUnidades(TipoUnidad.DOLLY, null);
            }
        }

        private void txtTolva2_KeyUp(object sender, KeyEventArgs e) //GRCC
        {
            if (e.Key == Key.Back || e.Key == Key.Delete)
            {
                mapUnidades(TipoUnidad.TOLVA_2, null);
            }
        }

        private List<UnidadTransporte> buscarUnidades(string clave) //GRCC
        {
            OperationResult resp = new UnidadTransporteSvc().getUnidadesALLorById(claveEmpresa, clave);
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    return (List<UnidadTransporte>)resp.result;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return null;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return null;
                case ResultTypes.recordNotFound:
                    return new List<UnidadTransporte>();

                default:
                    return null;
            }
        }

        private void completarClave(ref string clave) //GRCC
        {
            if (clave.Length < 3)
            {
                for (int i = 0; i <= (3 - clave.Length); i++)
                {
                    clave = "0" + clave;
                }
            }
        }
        private void txtTractor_KeyDown(object sender, KeyEventArgs e) //GRCC
        {
            if (e.Key == Key.Enter)
            {
                string clave = txtTractor.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                completarClave(ref clave);

                List<UnidadTransporte> ListUnidadT = buscarUnidades("TC" + clave);
                if (ListUnidadT.Count == 1)
                {
                    mapUnidades(TipoUnidad.TRACTOR, ListUnidadT[0]);

                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("TC", claveEmpresa, clave);
                    var s = buscardor.buscarUnidades();
                    mapUnidades(TipoUnidad.TRACTOR, s);
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    mapUnidades(TipoUnidad.TRACTOR, s);
                }
            }
        }

        private void txtTolva1_KeyDown(object sender, KeyEventArgs e) //GRCC
        {
            if (e.Key == Key.Enter)
            {

                string clave = txtTolva1.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                completarClave(ref clave);

                List<UnidadTransporte> ListUnidadT = buscarUnidades("TV" + clave);
                if (ListUnidadT.Count == 1)
                {
                    mapUnidades(TipoUnidad.TOLVA, ListUnidadT[0]);

                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("TV", claveEmpresa, clave);
                    var s = buscardor.buscarUnidades();
                    mapUnidades(TipoUnidad.TOLVA, s);
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    mapUnidades(TipoUnidad.TOLVA, s);
                }
            }
        }

        private void txtDolly_KeyDown(object sender, KeyEventArgs e) //GRCC
        {
            if (e.Key == Key.Enter)
            {

                string clave = txtDolly.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                completarClave(ref clave);

                List<UnidadTransporte> ListUnidadT = buscarUnidades("DL" + clave);
                if (ListUnidadT.Count == 1)
                {
                    mapUnidades(TipoUnidad.DOLLY, ListUnidadT[0]);

                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("DL", claveEmpresa, clave);
                    var s = buscardor.buscarUnidades();
                    mapUnidades(TipoUnidad.DOLLY, s);
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    mapUnidades(TipoUnidad.DOLLY, s);
                }
            }
        }

        private void txtTolva2_KeyDown(object sender, KeyEventArgs e) //GRCC
        {
            if (e.Key == Key.Enter)
            {

                string clave = txtTolva2.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                completarClave(ref clave);

                List<UnidadTransporte> ListUnidadT = buscarUnidades("TV" + clave);
                if (ListUnidadT.Count == 1)
                {
                    mapUnidades(TipoUnidad.TOLVA_2, ListUnidadT[0]);

                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("TV", claveEmpresa, clave);
                    var s = buscardor.buscarUnidades();
                    mapUnidades(TipoUnidad.TOLVA_2, s);
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    mapUnidades(TipoUnidad.TOLVA_2, s);
                }
            }
        }

        private void btnQuitar_Click(object sender, RoutedEventArgs e)//GRCC
        {
            quitar();
        }

        private void quitar()//GRCC
        {
            if (this.lvlCrtaPortes.SelectedItem != null)
            {
                CartaPorte content = (CartaPorte)((ListViewItem)this.lvlCrtaPortes.SelectedItem).Content;
                if (content.Consecutivo > 0)
                {
                    content.baja = true;
                    content.usuarioAjuste = new int?(base.mainWindow.inicio._usuario.idUsuario);
                    ListViewItem newItem = new ListViewItem
                    {
                        Foreground = Brushes.Red,
                        Content = content
                    };
                    //this.lvlCrtaPortesBaja.Items.Add(newItem);
                    this.lvlCrtaPortes.Items.Remove(this.lvlCrtaPortes.SelectedItem);
                    addRemBaja(content);
                }
                else
                {
                    this.lvlCrtaPortes.Items.Remove(this.lvlCrtaPortes.SelectedItem);
                }
            }
            this.limpiarDetalles();
        }
        private void addRemBaja(CartaPorte cartaPorte)
        {
            try
            {
                Border border = new Border { Tag = cartaPorte, BorderBrush = Brushes.SteelBlue, CornerRadius = new CornerRadius(6), BorderThickness = new Thickness(1), Margin = new Thickness(2) };
                StackPanel stp = new StackPanel { Orientation = Orientation.Horizontal };

                Label lbl = new Label
                {
                    Content = cartaPorte.remision.ToString(),
                    VerticalAlignment = VerticalAlignment.Center,
                    FontWeight = FontWeights.Bold,
                    Foreground = Brushes.SteelBlue,
                    Width = 100,
                    HorizontalContentAlignment = HorizontalAlignment.Center
                };
                Button btn = new Button
                {
                    Margin = new Thickness(2),
                    Content = "X",
                    Foreground = Brushes.White,
                    Background = Brushes.Red,
                    Tag = border
                };
                btn.Click += Btn_Click;

                stp.Children.Add(lbl);
                stp.Children.Add(btn);

                border.Child = stp;

                wrapRemisionesBaja.Children.Add(border);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    Button btn = sender as Button;
                    Border border = btn.Tag as Border;
                    CartaPorte cp = border.Tag as CartaPorte;

                    cp.baja = false;
                    ListViewItem lvl = new ListViewItem();
                    lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                    lvl.Selected += Lvl_Selected;
                    lvl.Content = cp;
                    lvlCrtaPortes.Items.Add(lvl);

                    wrapRemisionesBaja.Children.Remove(border);
                    //lvlCrtaPortesBaja.Items.Remove(lvlCrtaPortesBaja.SelectedItem);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void txtAyudante_KeyDown(object sender, KeyEventArgs e) //GRCC
        {
            if ((e.Key == Key.Enter) && (this.txtClave.Tag != null))
            {
                string nombre = this.txtAyudante.Text.Trim();
                List<Personal> list = this.buscarPersonal(nombre);
                if (list.Count == 1)
                {
                    this.mapAyudante(list[0]);
                }
                else if (list.Count == 0)
                {
                    Personal operador = new selectPersonal(nombre, ((Viaje)this.txtClave.Tag).IdEmpresa).buscarPersonal();
                    this.mapAyudante(operador);
                }
                else
                {
                    Personal operador = new selectPersonal(nombre, ((Viaje)this.txtClave.Tag).IdEmpresa).buscarPersonal();
                    this.mapAyudante(operador);
                }
            }
        }
        private void txtOperadorFin_KeyDown(object sender, KeyEventArgs e) //GRCC
        {
            if ((e.Key == Key.Enter) && (this.txtClave.Tag != null))
            {
                string nombre = this.txtOperadorFin.Text.Trim();
                List<Personal> list = this.buscarPersonal(nombre);
                if (list.Count == 1)
                {
                    this.mapOperadorFinViaje(list[0]);
                }
                else if (list.Count == 0)
                {
                    Personal operador = new selectPersonal(nombre, ((Viaje)this.txtClave.Tag).IdEmpresa).buscarPersonal();
                    this.mapOperadorFinViaje(operador);
                }
                else
                {
                    Personal operador = new selectPersonal(nombre, ((Viaje)this.txtClave.Tag).IdEmpresa).buscarPersonal();
                    this.mapOperadorFinViaje(operador);
                }
            }
        }
        private void mapAyudante(Personal operador) //GRCC
        {
            if (operador != null)
            {
                this.txtAyudante.Tag = operador;
                this.txtAyudante.Text = operador.nombre;
                this.txtAyudante.IsEnabled = false;
            }
            else
            {
                this.txtAyudante.Tag = null;
                this.txtAyudante.Text = "";
                this.txtAyudante.IsEnabled = true;
            }
        }
        private void mapOperadorFinViaje(Personal operador) //GRCC
        {
            if (operador != null)
            {
                this.txtOperadorFin.Tag = operador;
                this.txtOperadorFin.Text = operador.nombre;
                this.txtOperadorFin.IsEnabled = false;
            }
            else
            {
                this.txtOperadorFin.Tag = null;
                this.txtOperadorFin.Text = "";
                this.txtOperadorFin.IsEnabled = true;
            }
        }
        private void chcViajeEnFalso_Checked(object sender, RoutedEventArgs e) //GRCC
        {
            stpCampoImputable.Visibility = Visibility.Visible;
            //chcExportar.Visibility = Visibility.Visible;
            stpVolReal.Visibility = Visibility.Visible;
            //if ((this._viaje != null) && (this._viaje.cobroXKm || _viaje.listDetalles.Exists(S=>S.viaje)))
            //{
            GridView view = this.lvlCrtaPortes.View as GridView;
            GridViewColumnCollection columns = view.Columns;
            GridViewColumn columVolDescargado = new GridViewColumn
            {
                Header = "V. Descargado",
                Width = 100.0
            };
            Binding bindingVolDescargado = new Binding("vDescarga")
            {
                StringFormat = "N4"
            };
            columVolDescargado.DisplayMemberBinding = bindingVolDescargado;
            view.Columns.Insert(5, columVolDescargado);


            GridViewColumn columVolSinDescargar = new GridViewColumn
            {
                Header = "V. Sin Descargar",
                Width = 120,
                DisplayMemberBinding = new Binding("volSinDescargar") { StringFormat = "N4" }
            };
            view.Columns.Insert(6, columVolSinDescargar);

            stpObs.Visibility = Visibility.Visible;
            //}
        }

        private void chcViajeEnFalso_Unchecked(object sender, RoutedEventArgs e) //GRCC
        {
            stpCampoImputable.Visibility = Visibility.Hidden;
            stpVolReal.Visibility = Visibility.Hidden;
            chcExportar.Visibility = Visibility.Hidden;
            //if ((this._viaje != null) && this._viaje.cobroXKm)
            //{
            GridView view = this.lvlCrtaPortes.View as GridView;
            List<GridViewColumn> list = view.Columns.Cast<GridViewColumn>().ToList<GridViewColumn>();
            view.Columns.Remove(list.Find(s => s.Header.ToString() == "V. Descargado"));
            view.Columns.Remove(list.Find(s => s.Header.ToString() == "V. Sin Descargar"));
            stpObs.Visibility = Visibility.Hidden;
            txtObsFalso.Clear();
            //}
        }
        private void btnSubir_Click(object sender, RoutedEventArgs e) //GRCC
        {
            //if (lvlCrtaPortesBaja.SelectedItem != null)
            //{
            //    CartaPorte cp = ((CartaPorte)((ListViewItem)lvlCrtaPortesBaja.SelectedItem).Content);

            //    cp.baja = false;
            //    ListViewItem lvl = new ListViewItem();
            //    lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
            //    lvl.Selected += Lvl_Selected;
            //    lvl.Content = cp;
            //    lvlCrtaPortes.Items.Add(lvl);
            //    lvlCrtaPortesBaja.Items.Remove(lvlCrtaPortesBaja.SelectedItem);
            //}
            List<CartaPorte> list = new List<CartaPorte>();
            foreach (var item in wrapRemisionesBaja.Children)
            {
                list.Add((item as Border).Tag as CartaPorte);
            }
        }
        private void txtRemision_KeyDown(object sender, KeyEventArgs e) //GRCC
        {
            if (e.Key == Key.Enter)
            {
                int i = 0;
                if (int.TryParse(txtRemision.Text.Trim(), out i))
                {
                    txtProducto.Focus();
                }
            }
        }
        List<Producto> listaProductos = new List<Producto>(); //GRCC
        private void txtProducto_KeyDown(object sender, KeyEventArgs e) //GRCC
        {
            if (e.Key == Key.Enter)
            {
                List<Producto> listProductos = new List<Producto>();
                if (this.listaProductos.Count != 0)
                {
                    List<Producto> list2 = this.listaProductos.FindAll(S => S.descripcion.IndexOf(this.txtProducto.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                    foreach (Producto producto2 in list2)
                    {
                        listProductos.Add(producto2);
                    }
                    selectProducto producto = new selectProducto(this.txtProducto.Text);
                    if (listProductos.Count == 1)
                    {
                        this.mapProductos(listProductos[0]);
                    }
                    else
                    {
                        Producto producto3 = producto.buscarProductos(listProductos);
                        this.mapProductos(producto3);
                    }
                }
            }
        }

        private void mapProductos(Producto producto) //GRCC
        {
            if (producto != null)
            {
                txtProducto.Tag = producto;
                txtProducto.Text = producto.descripcion;
                txtProducto.IsEnabled = false;
                txtDestino.Focus();
            }
            else
            {
                txtProducto.Tag = null;
                txtProducto.Clear();
                txtProducto.IsEnabled = true;
            }
        }
        List<Zona> _listZonas = new List<Zona>(); //GRCC
        private void txtDestino_KeyDown(object sender, KeyEventArgs e) //GRCC
        {
            if ((e.Key == Key.Enter) && (this._listZonas.Count != 0))
            {
                List<Zona> list = new List<Zona>();
                List<Zona> list2 = this._listZonas.FindAll(S => S.descripcion.IndexOf(this.txtDestino.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (Zona zona in list2)
                {
                    list.Add(zona);
                }
                selectDestinos destinos = new selectDestinos(this.txtDestino.Text);
                if (list.Count == 1)
                {
                    this.mapDestinos(list[0]);
                }
                else
                {
                    Zona zona2 = destinos.buscarZona(this._listZonas);
                    this.mapDestinos(zona2);
                }
            }
        }

        private void mapDestinos(Zona zona) //GRCC
        {
            if (zona != null)
            {
                txtDestino.Tag = zona;
                txtDestino.Text = zona.descripcion;
                txtDestino.IsEnabled = false;
                txtPrecio.valor = calcularNewPrecio(zona);
                txtPrecioFijo.valor = calcularNewPrecioFijo(zona);
                txtPrecioFijo.txtDecimal.Focus();
            }
            else
            {
                txtDestino.Tag = null;
                txtDestino.IsEnabled = true;
                txtDestino.Clear();
            }
        }
        private void txtVolDescarga_KeyDown(object sender, KeyEventArgs e) //GRCC
        {
            if ((e.Key == Key.Enter) && !string.IsNullOrEmpty(this.txtRemision.Text.Trim()))
            {
                if (this.chcViajeEnFalso.IsChecked.Value)
                {
                    if (this.stpVolReal.Visibility == Visibility.Visible)
                    {
                        this.txtVolDescargaReal.txtDecimal.Focus();
                    }
                    else
                    {
                        this.mapDetalles();
                    }
                }
                else
                {
                    this.mapDetalles();
                }
            }
        }
        private void txtVolDescargaReal_KeyDown(object sender, KeyEventArgs e) //GRCC
        {
            if ((e.Key == Key.Enter) && !string.IsNullOrEmpty(this.txtRemision.Text.Trim()))
            {
                this.mapDetalles();
            }
        }
        public void mapDetalles() //GRCC
        {
            if (((this.cbxClientes.Tag != null) && (this.txtTolva1.Tag != null)) && this.validarDetalles())
            {
                CartaPorte cp = new CartaPorte
                {
                    Consecutivo = (this.txtRemision.Tag == null) ? 0 : ((CartaPorte)this.txtRemision.Tag).Consecutivo,
                    producto = (Producto)this.txtProducto.Tag,
                    zonaSelect = (Zona)this.txtDestino.Tag,
                    PorcIVA = ((Cliente)this.cbxClientes.Tag).impuesto.valor,
                    PorcRetencion = ((Cliente)this.cbxClientes.Tag).impuestoFlete.valor,
                    isReEnvio = chcReEnvio.IsChecked.Value
                };

                cp.cobroXkm = _viaje.cobroXKm;
                Empresa empresa = new Empresa
                {
                    clave = ((Viaje)this.txtClave.Tag).IdEmpresa
                };
                cp.tipoPrecio = EstablecerPrecios.establecerPrecio((UnidadTransporte)this.txtTolva1.Tag, (UnidadTransporte)this.txtDolly.Tag, (UnidadTransporte)this.txtTolva2.Tag, empresa, (Cliente)this.cbxClientes.Tag);
                Empresa empresa2 = new Empresa
                {
                    clave = ((Viaje)this.txtClave.Tag).IdEmpresa
                };
                cp.limpiarImporte();
                cp.viaje = cp.zonaSelect.viaje;
                cp.tipoPrecio = EstablecerPrecios.establecerPrecio((UnidadTransporte)this.txtTolva1.Tag, (UnidadTransporte)this.txtDolly.Tag, (UnidadTransporte)this.txtTolva2.Tag, empresa2, _viaje.cliente);
                cp.tipoPrecioChofer = EstablecerPrecios.establecerPrecioChofer((UnidadTransporte)this.txtTolva1.Tag, (UnidadTransporte)this.txtDolly.Tag, (UnidadTransporte)this.txtTolva2.Tag, empresa2);
                cp.precio = this.txtPrecio.valor;
                cp.precioFijo = this.txtPrecioFijo.valor;
                cp.remision = this.txtRemision.Text.Trim();
                cp.volumenDescarga = this.txtVolDescarga.valor;
                cp.vDescarga = (this.chcViajeEnFalso.IsChecked.Value && (this.stpVolReal.Visibility == Visibility.Visible)) ? this.txtVolDescargaReal.valor : this.txtVolDescarga.valor;
                CartaPorte det = cp;
                if (det.Consecutivo == 0)
                {
                    ListViewItem newItem = new ListViewItem
                    {
                        Content = det
                    };
                    newItem.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                    newItem.Selected += new RoutedEventHandler(this.Lvl_Selected);
                    this.lvlCrtaPortes.Items.Add(newItem);
                }
                else
                {
                    decimal precioOperador = det.precioOperador;
                    this.actualizarLista(det);
                }
                this.limpiarDetalles();
                this.txtRemision.Focus();
            }

        }

        private void actualizarLista(CartaPorte det)//GRCC
        {
            ListViewItem lvl = lvlCrtaPortes.SelectedItem as ListViewItem;
            det.usuarioAjuste = mainWindow.inicio._usuario.idUsuario;
            decimal precioFijo = det.precioFijo;
            det.limpiarImporte();
            det.precioFijo = precioFijo;
            lvl.Content = det;
            //lvl.Foreground = Brushes.SteelBlue;
            lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
            lvl.Selected += Lvl_Selected;
        }

        public void mapDetalle(CartaPorte cp) //GRCC
        {
            this.txtRemision.Tag = cp;
            this.txtRemision.Text = cp.remision.ToString();
            this.mapProductos(cp.producto);
            this.mapDestinos(cp.zonaSelect);
            this.txtProducto.IsEnabled = true;
            this.txtDestino.IsEnabled = true;
            this.txtPrecio.valor = cp.precio;
            this.txtPrecioFijo.valor = cp.precioFijo;
            this.txtVolDescarga.valor = cp.volumenDescarga;
            this.txtVolDescargaReal.valor = cp.vDescarga;
            this.chcReEnvio.IsChecked = cp.isReEnvio;
        }

        private void limpiarDetalles() //GRCC
        {
            this.txtRemision.Clear();
            this.mapProductos(null);
            this.mapDestinos(null);
            this.txtVolDescarga.valor = decimal.Zero;
            this.txtVolDescargaReal.valor = decimal.Zero;
            this.txtPrecio.valor = decimal.Zero;
            this.txtPrecioFijo.valor = decimal.Zero;
            this.chcReEnvio.IsChecked = false;
        }

        private TipoPrecio establecerPrecio() //GRCC
        {
            UnidadTransporte tag = (UnidadTransporte)this.txtTolva1.Tag;
            if (((Cliente)this.cbxClientes.Tag).clave == 3)
            {
                if ((this.txtDolly.Tag != null) || (this.txtTolva2.Tag != null))
                {
                    return TipoPrecio.FULL;
                }
                if (tag.tipoUnidad.descripcion == "TOLVA 35 TON")
                {
                    return TipoPrecio.SENCILLO35;
                }
                return TipoPrecio.SENCILLO;
            }
            if ((this.txtDolly.Tag != null) || (this.txtTolva2.Tag != null))
            {
                return TipoPrecio.FULL;
            }
            return TipoPrecio.SENCILLO;
        }
        private TipoPrecio establecerPrecioChoferPegaso() //GRCC
        {
            UnidadTransporte tag = (UnidadTransporte)this.txtTolva1.Tag;
            if ((this.txtDolly.Tag != null) || (this.txtTolva2.Tag != null))
            {
                return TipoPrecio.FULL;
            }
            if (tag.tipoUnidad.descripcion == "TOLVA 35 TON")
            {
                return TipoPrecio.SENCILLO35;
            }
            if (tag.tipoUnidad.TonelajeMax == 30M)
            {
                return TipoPrecio.SENCILLO30;
            }
            if (tag.tipoUnidad.TonelajeMax == 24M)
            {
                return TipoPrecio.SENCILLO24;
            }
            return TipoPrecio.SENCILLO;
        }

        public decimal calcularNewPrecio(Zona zona) //GRCC
        {
            switch (establecerPrecio())
            {
                case TipoPrecio.SENCILLO:
                    return zona.costoSencillo;
                case TipoPrecio.SENCILLO35:
                    return zona.costoSencillo35;
                case TipoPrecio.FULL:
                    return zona.costoFull;
                default:
                    return 0;
            }
        }
        public decimal calcularNewPrecioFijo(Zona zona) //GRCC
        {
            switch (establecerPrecio())
            {
                case TipoPrecio.SENCILLO:
                    return zona.precioFijoSencillo;
                case TipoPrecio.SENCILLO35:
                    return zona.precioFijoSencillo35;
                case TipoPrecio.FULL:
                    return zona.precioFijoFull;
                default:
                    return 0;
            }
        }
        private bool validarDetalles() //GRCC
        {
            int result = 0;
            //if (!int.TryParse(this.txtRemision.Text.Trim(), out result))
            //{
            //    this.txtRemision.Clear();
            //    this.txtRemision.Focus();
            //    return false;
            //}
            if (string.IsNullOrEmpty(this.txtRemision.Text.Trim()))
            {
                this.txtRemision.Focus();
            }
            if (this.txtProducto.Tag == null)
            {
                this.txtProducto.Clear();
                this.txtProducto.Focus();
                return false;
            }
            if (this.txtDestino.Tag == null)
            {
                this.txtDestino.Clear();
                this.txtDestino.Focus();
                return false;
            }
            if (this.txtVolDescarga.valor > 100M)
            {
                System.Windows.MessageBox.Show("La captura es en Toneladas. Revisa tu valor capturado", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                this.txtVolDescarga.Focus();
                return false;
            }
            return true;
        }
        private void btnNewDetalle_Click(object sender, RoutedEventArgs e)  //GRCC
        {
            Viaje tag = this.txtClave.Tag as Viaje;
            if (new addDetalleViaje(this.listaProductos, this._listZonas, tag).ShowDialog().Value)
            {
                this.buscarViaje(tag.NumGuiaId);
                base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
                base.mainWindow.scrollContainer.IsEnabled = true;
            }
        }
        private void buscarViaje(int folio) //GRCC
        {
            OperationResult resp = new CartaPorteSvc().getCartaPorteById(folio);
            if (resp.typeResult == ResultTypes.success)
            {
                this.mapForm(((List<Viaje>)resp.result)[0]);
                base.buscar();
                if (this._viaje.EstatusGuia.ToUpper() == "FACTURADO" || this._viaje.EstatusGuia.ToUpper() == "LISTO PARA FACTURAR")
                {
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "Este Viaje ya no se puede editar", null));
                }
                else
                {
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.editar | ToolbarCommands.nuevo);
                }
            }
            else
            {
                this.imprimirMensaje(resp);
            }
        }

        private void txtOperador2_KeyDown(object sender, KeyEventArgs e) //GRCC
        {
            if ((e.Key == Key.Enter) && (this.txtClave.Tag != null))
            {
                string nombre = this.txtOperador2.Text.Trim();
                List<Personal> list = this.buscarPersonal(nombre);
                if (list.Count == 1)
                {
                    this.mapOperador2(list[0]);
                }
                else if (list.Count == 0)
                {
                    Personal operador = new selectPersonal(nombre, ((Viaje)this.txtClave.Tag).IdEmpresa).buscarPersonal();
                    this.mapOperador2(operador);
                }
                else
                {
                    Personal operador = new selectPersonal(nombre, ((Viaje)this.txtClave.Tag).IdEmpresa).buscarPersonal();
                    this.mapOperador2(operador);
                }
            }
        }

        private void mapOperador2(Personal operador) //GRCC
        {
            if (operador != null)
            {
                txtOperador2.Tag = operador;
                txtOperador2.Text = operador.nombre;
                txtOperador2.IsEnabled = false;
            }
            else
            {
                txtOperador2.Tag = null;
                txtOperador2.Text = "";
                txtOperador2.IsEnabled = true;
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e) //GRCC
        {
            //if (this.chcOperador.IsChecked.Value)
            //{
            //    this.mapOperador(null);
            //}
            //if (this.chcOperador2.IsChecked.Value)
            //{
            //    this.mapOperador2(null);
            //}
            //if (this.chcAyudante.IsChecked.Value)
            //{
            //    this.mapOperador2(null);
            //}
            //if (chcOperadorFin.IsChecked.Value)
            //{
            //    mapOperadorFinViaje(null);
            //}
        }

        private void BtnCambiarOperador_Click(object sender, RoutedEventArgs e)
        {
            mapOperador(null);
        }

        private void BtnCambiarOperadorCapacitacion_Click(object sender, RoutedEventArgs e)
        {
            mapOperador2(null);
        }

        private void BtnCambiarAyuFull_Click(object sender, RoutedEventArgs e)
        {
            mapAyudante(null);
        }

        private void BtnCambiarOperadorFin_Click(object sender, RoutedEventArgs e)
        {
            mapOperadorFinViaje(null);

        }

        private void btnQuitarVale_Click(object sender, RoutedEventArgs e)
        {
            mapFormVale(null);
            ResumenCargaCombustible resumen = new selectResumenCargasCombustible().getValeByDatosViaje(_viaje.tractor.clave, _viaje.FechaHoraViaje);
            if (resumen != null)
            {
                buscarValeCombustible(resumen.idValeCarga);
            }
        }
        public void buscarValeCombustible(int vale)
        {
            OperationResult resp = new OperacionSvc().cargaCombustibleByIdVale(vale);
            if (resp.typeResult == ResultTypes.success)
            {
                CargaCombustible result = (CargaCombustible)resp.result;
                this.mapFormVale(result);
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
            }
        }

        private void mapFormVale(CargaCombustible carga)
        {
            if (carga != null)
            {
                ctrNoVale.valor = carga.idCargaCombustible;
                ctrOrdenServ.valor = carga.idOrdenServ;
                ctrLitros.valor = carga.ltCombustible;
                txtFechaCombustible.Text = carga.fechaCombustible.Value.ToString("dd/MM/yy HH:mm");
            }
            else
            {
                ctrNoVale.valor = 0;
                ctrOrdenServ.valor = 0;
                ctrLitros.valor = 0;
                txtFechaCombustible.Clear();
            }
        }
    }
}
