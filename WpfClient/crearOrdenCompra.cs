﻿using Core.Models;
using Core.Utils;
using CoreFletera.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ap = System.Windows.Forms;
namespace WpfClient
{
    public class crearOrdenCompra
    {
        public crearOrdenCompra(ref bool resp)
        {
            resp = iniciaSDK();
        }
        string _empresa = string.Empty;

        public bool generarOrdenCompra(string empresa, OrdenCompra ordenCompra)
        {
            _empresa = empresa;
            //if (iniciaSDK())
            //{
            bool resp = abrirEmpresa(_empresa);
            if (resp)
            {
                var r = realizarOrdenCompra(ordenCompra);
                //Declaraciones.fCierraEmpresa();
                cerrar();
                return r;
            }
            else
            {
                //Declaraciones.fCierraEmpresa();
                cerrar();
                return false;
            }
            //}
            //else
            //{
            //    return false;
            //}
        }
        public List<OrdenCompra> newListOrdenCompra = new List<OrdenCompra>();
        public bool generarOrdenCompra(List<OrdenCompra> ListOrdenCompra)
        {
            newListOrdenCompra = new List<OrdenCompra>();
            foreach (var item in ListOrdenCompra)
            {
                _empresa = "adREMOLQUES_ATLAS";// item.empresa.nombreComercial;
                bool resp = abrirEmpresa(_empresa);
                if (resp)
                {
                    var r = realizarOrdenCompra(item);
                    Declaraciones.fCierraEmpresa();
                    if (!r)
                    {
                        cerrar();
                        return false;
                    }
                }
                else
                {
                    cerrar();
                    return false;
                }
            }
            cerrar();
            return true;
        }


        public bool cerrar()
        {
            try
            {
                Declaraciones.fCierraEmpresa();
                Declaraciones.fTerminaSDK();
                KeySistema.Close();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        RegistryKey KeySistema = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Computación en Acción, SA CV\CONTPAQ I COMERCIAL");

        private int lResult = 0;
        public bool iniciaSDK()
        {
            try
            {

                object lEntrada = KeySistema.GetValue("DirectorioBase");

                lResult = Declaraciones.SetCurrentDirectory(lEntrada.ToString());

                if (lResult != 0)
                {
                    var y = Declaraciones.fInicializaSDK();
                    var s = Declaraciones.fSetNombrePAQ("CONTPAQ I COMERCIAL");
                    return true;
                }
                else
                {
                    //txtMensajes.Text = "Hubo un problema";
                    Declaraciones.MuestraError(lResult);
                    return false;
                }
            }
            catch (System.ArithmeticException ex)
            {
                MessageBox.Show("Ocurrio un error al tratar de iniciar el SDK." + Environment.NewLine
                    + ex.Message, "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return false;
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrio un error al tratar de iniciar el SDK." + Environment.NewLine
                    + e.Message, "Error",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return false;
            }
        }//Fin método Abrir Conexión 

        string path = ap.Application.StartupPath;
        #region Abrir Empresa
        private bool abrirEmpresa(string empresa)
        {
            lResult = Declaraciones.fAbreEmpresa(@"C:\Compac\Empresas\" + empresa);
            if (lResult == 0)
            {
                return true;
            }
            else
            {
                Declaraciones.MuestraError(lResult);
                return false;
            }
        }//Fin abrir empresa 
        #endregion
        public int folioOrdenCompra;
        public double folio;
        public string serie;
        private bool realizarOrdenCompra(OrdenCompra ordenCompra)
        {
            try
            {
                foreach (var item in ordenCompra.listaDetalles)
                {
                    if (!validarProducto.validarExistenProductos(item, _empresa, ordenCompra.isLLanta))
                    {
                        ImprimirMensaje.imprimir(new OperationResult { valor = 1, mensaje = "Ocurrio un error al dar de alta al producto" });
                        return false;
                    }
                }

                if (!GuardarProveedor.guardarNewProveedor(ordenCompra.proveedor))
                {
                    return false;
                }

                Declaraciones.tDocumento ltDocto = new Declaraciones.tDocumento();
                int lIdDocto = 0;
                double lFolio = 0;
                StringBuilder lSerieDocto = new StringBuilder(12);
                lSerieDocto.Append("");
                if (hayError(Declaraciones.fSiguienteFolio("19", lSerieDocto, ref lFolio)))
                {
                    return false;
                }
                else
                {
                    ltDocto.aCodConcepto = "19";
                    ltDocto.aSerie = lSerieDocto.ToString();
                    //ltDocto.aFolio = lFolio;
                    ltDocto.aFecha = ordenCompra.fecha.ToString("MM/dd/yyyy");
                    ltDocto.aCodigoCteProv = "P" + ordenCompra.proveedor.idProveedor.ToString();
                    //ltDocto.aCodigoAgente = chofer;
                    ltDocto.aSistemaOrigen = 205; //0 = AdminPAQ ¿Cual es comercial????
                    ltDocto.aNumMoneda = 1;
                    ltDocto.aTipoCambio = 1;
                    ltDocto.aImporte = 0;
                    ltDocto.aDescuentoDoc1 = 0;
                    ltDocto.aDescuentoDoc2 = 0;
                    ltDocto.aAfecta = 1;

                    if (hayError(Declaraciones.fAltaDocumento(ref lIdDocto, ref ltDocto)))
                    {
                        return false;
                    }
                    else
                    {

                        //MessageBox.Show(lSerieDocto.ToString());
                        bool r = false;
                        foreach (var item in ordenCompra.listaDetalles)
                        {
                            Declaraciones.tMovimiento ltMovto = new Declaraciones.tMovimiento();
                            int LIdMovto = 0;
                            ltMovto.aCodAlmacen = "1";
                            ltMovto.aConsecutivo = 1;
                            ltMovto.aCodProdSer = ordenCompra.isLLanta ? ("LL" + item.llantaProducto.idLLantasProducto) : item.producto.codigo.ToString();
                            ltMovto.aUnidades = double.Parse(item.cantidad.ToString());
                            ltMovto.aPrecio = (double)item.costo;
                            //ltMovto.aCosto = (double)item.costo;

                            if (hayError(Declaraciones.fAltaMovimiento(lIdDocto, ref LIdMovto, ref ltMovto)))
                            {
                                r = false;
                                break;
                            }
                            else
                            {
                                r = true;
                            }
                        }
                        folioOrdenCompra = lIdDocto;
                        folio = lFolio;
                        serie = lSerieDocto.ToString();

                        ordenCompra.CIDDOCUMENTO = folioOrdenCompra;
                        ordenCompra.CFOLIO = folio;
                        ordenCompra.CSERIEDOCUMENTO = serie;
                        newListOrdenCompra.Add(ordenCompra);
                        return r;
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return false;
            }
        }
        private bool hayError(int error)
        {
            if (error != 0)
            {
                Declaraciones.MuestraError(error);
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
