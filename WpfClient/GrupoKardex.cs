﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfClient
{
    public class GrupoKardex
    {
        public Empresa empresa { get; set; }
        public List<KardexCombustible> listaKardex { get; set; }
    }
}
