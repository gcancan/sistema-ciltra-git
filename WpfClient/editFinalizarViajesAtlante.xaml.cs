﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Markup;
    using System.Windows.Media;

    public partial class editFinalizarViajesAtlante : WpfClient.ViewBase
    {
        private Viaje _viaje = new Viaje();
        private List<Origen> _listOrigen = new List<Origen>();
        private List<Producto> _lisProducto = new List<Producto>();
        private bool combustible = false;
        private List<NewViajes> listNewViejes = new List<NewViajes>();
        private List<Zona> _listZonas = new List<Zona>();
        private List<ValeCarga> listItems = new List<ValeCarga>();
        private List<Producto> listaProductos = new List<Producto>();
        
        public editFinalizarViajesAtlante()
        {
            this.InitializeComponent();
        }
        private void addLista(ValeCarga vale)
        {
            foreach (NewViajes viajes in this.listNewViejes)
            {
                if (viajes.cliente.clave == vale.cliente.clave)
                {
                    viajes.listaVales.Add(vale);
                    return;
                }
            }
            NewViajes item = new NewViajes
            {
                cliente = vale.cliente
            };
            List<ValeCarga> list1 = new List<ValeCarga> {
                vale
            };
            item.listaVales = list1;
            this.listNewViejes.Add(item);
        }

        private List<List<CartaPorte>> agruparByZona(List<CartaPorte> listDetalles)
        {
            AgruparCartaPortes portes = new AgruparCartaPortes();
            portes.agruparByZona(listDetalles);
            return portes.superLista;
        }

        private void buscarDestinos()
        {
            try
            {
                if (this.txtOrigen.Tag != null)
                {
                    OperationResult resp = new ZonasSvc().getDestinosByOrigenAndIdCliente(((Cliente)this.cbxClienteVale.SelectedItem).clave, ((Origen)this.txtOrigen.Tag).idOrigen);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        this._listZonas = (List<Zona>)resp.result;
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void cabecera_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            object dataContext = ((cabeceraViajes)sender).DataContext;
            if (dataContext.ToString() != "System.Object")
            {
                this._viaje = (Viaje)dataContext;
                this.mapPantalla(this._viaje);
                this.gBoxValeCombustible.IsEnabled = !this.combustible;
            }
            else
            {
                this._viaje = null;
                this.gBoxValeCombustible.IsEnabled = false;
            }
        }

        private void cbxClienteVale_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (this.cbxClienteVale.SelectedItem != null)
                {
                    Cliente selectedItem = (Cliente)this.cbxClienteVale.SelectedItem;
                    //if (this.listaProductos.Count <= 0)
                    //{
                    //    OperationResult result2 = new ProductoSvc().getALLProductosByIdORidFraccion(selectedItem.clave, 0);
                    //    if (result2.typeResult == ResultTypes.success)
                    //    {
                    //        this.listaProductos = (List<Producto>)result2.result;
                    //    }
                    //    else
                    //    {
                    //        this.listaProductos = new List<Producto>();
                    //    }
                    //}
                    OperationResult resp = new OrigenSvc().getOrigenesAll(selectedItem.clave, 0);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        this._listOrigen = (List<Origen>)resp.result;
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                    this.mapOrigen(null);
                    this.mapDestino(null);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
            
        }

        private bool establecerImpresora()
        {
            try
            {
                if (string.IsNullOrEmpty(new Core.Utils.Util().getRutasImpresora("VIAJES_ATLANTE")))
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "Se requiere seleccionar la impresora"
                    };
                    ImprimirMensaje.imprimir(resp);
                    System.Windows.Forms.PrintDialog dialog = new System.Windows.Forms.PrintDialog();
                    if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    {
                        new Core.Utils.Util().setRutasImpresora("VIAJES_ATLANTE", dialog.PrinterSettings.PrinterName);
                        return this.establecerImpresora();
                    }
                    return false;
                }
                return true;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return false;
            }
        }

        private OperationResult generarNuevosViajes(List<ValeCarga> lista)
        {
            try
            {
                this.listNewViejes = new List<NewViajes>();
                foreach (ValeCarga carga in lista)
                {
                    this.addLista(carga);
                }
                OperationResult result = new OperationResult();
                foreach (NewViajes viajes in this.listNewViejes)
                {
                    Viaje newVieje = this._viaje;
                    newVieje.cliente = viajes.cliente;
                    newVieje.NumGuiaId = 0;
                    newVieje.NumViaje = 0;
                    newVieje.listDetalles = new List<CartaPorte>();
                    foreach (ValeCarga carga2 in viajes.listaVales)
                    {
                        CartaPorte item = new CartaPorte
                        {
                            Consecutivo = 0,
                            producto = carga2.producto,
                            zonaSelect = carga2.destino,
                            origen = carga2.origen,
                            PorcIVA = viajes.cliente.impuesto.valor,
                            PorcRetencion = viajes.cliente.impuestoFlete.valor,
                            tipoPrecio = (newVieje.tipoViaje == 'F') ? TipoPrecio.FULL : TipoPrecio.SENCILLO,
                            remision = carga2.remision,
                            volumenDescarga = carga2.volDescarga
                        };
                        newVieje.listDetalles.Add(item);
                    }
                    result = this.guardarNewViajes(viajes.cliente, newVieje);
                    if (result.typeResult != ResultTypes.success)
                    {
                        return result;
                    }
                    Viaje viaje2 = (Viaje)result.result;
                    int num = 0;
                    foreach (CartaPorte porte in viaje2.listDetalles)
                    {
                        viajes.listaVales[num].cartaPorte = porte.Consecutivo;
                        viajes.listaVales[num].numGuiaId = viaje2.NumGuiaId;
                        num++;
                    }
                    result = new CartaPorteSvc().cambiarValesCarga(viajes.listaVales, ((CargaCombustible)this.txtOrdenCarga.Tag).idCargaCombustible, 
                        base.mainWindow.inicio._usuario.idUsuario, dtpFechaInicio.Value.Value, dtpFechaFin.Value.Value, false);
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
        }

        private List<ValeCarga> getListaVales()
        {
            try
            {
                List<ValeCarga> list = new List<ValeCarga>();
                foreach (System.Windows.Controls.ListViewItem item in (IEnumerable)this.lvlValesCarga.Items)
                {
                    ValeCarga valeCarga = (ValeCarga)item.Content;
                    if (valeCarga.validado && (valeCarga.cliente.clave == this._viaje.cliente.clave))
                    {
                        list.Add(valeCarga); 
                    }
                }
                return list;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return new List<ValeCarga>();
            }
        }

        private List<ValeCarga> getListaValesOtrosClientes()
        {
            try
            {
                List<ValeCarga> list = new List<ValeCarga>();
                foreach (System.Windows.Controls.ListViewItem item in (IEnumerable)this.lvlValesCarga.Items)
                {
                    ValeCarga valeCarga = (ValeCarga)item.Content;
                    if (valeCarga.validado && (valeCarga.cliente.clave != this._viaje.cliente.clave))
                    {
                        list.Add(valeCarga);
                    }
                }
                return list;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return new List<ValeCarga>();
            }
        }

        public override OperationResult guardar()
        {
            OperationResult result2;
            try
            {
                base.Cursor = System.Windows.Input.Cursors.Wait;


                if (_viaje.EstatusGuia == "FINALIZADO" && txtOrdenCarga.Tag == null)
                {
                    return new OperationResult(ResultTypes.warning, "NO SE ESPECIFICO EL VALE DE COMBUSTIBLE");
                }
                else if (_viaje.EstatusGuia == "FINALIZADO" && txtOrdenCarga.Tag != null)
                {
                    int idViaje = _viaje.NumGuiaId;
                    var respViaje = new CartaPorteSvc().ligarViajeVale(idViaje, (txtOrdenCarga.Tag as CargaCombustible).idCargaCombustible);
                    if (respViaje.typeResult == ResultTypes.success)
                    {
                        this.cabecera.buscar(idViaje);
                        mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                    }
                    return respViaje;
                }

                List<ValeCarga> listVales = this.getListaVales();
                List<ValeCarga> listaValesClientes = this.getListaValesOtrosClientes();
                if ((listVales.Count + listaValesClientes.Count) == 0)
                {
                    return new OperationResult
                    {
                        valor = 3,
                        mensaje = "No se ha realizado cambio alguno para guardar"
                    };
                }
                //if (this.txtOrdenCarga.Tag == null)
                //{
                //    return new OperationResult
                //    {
                //        valor = 3,
                //        mensaje = "Se requiere proporcionar la orden de servicio correspondiente al vale de carga de combustible"
                //    };
                //}

                OperationResult valResp = validar();
                if (valResp.typeResult != ResultTypes.success) return valResp;

                if ((listaValesClientes.Count > 0) && (System.Windows.MessageBox.Show("Se cambio el cliente a uno o mas vales, esto generar\x00e1 viajes nuevos" + Environment.NewLine + " \x00bfDesea continuar?", "Aviso", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No))
                {
                    return new OperationResult
                    {
                        valor = 2,
                        mensaje = "Operacion Cancelada"
                    };
                }
                int idCarga = this.txtOrdenCarga.Tag == null ? 0 : ((CargaCombustible)this.txtOrdenCarga.Tag).idCargaCombustible;
                OperationResult result = new CartaPorteSvc().cambiarValesCarga(listVales, idCarga, 
                    base.mainWindow.inicio._usuario.idUsuario, dtpFechaInicio.Value.Value, dtpFechaFin.Value.Value, true);
                if (result.typeResult == ResultTypes.success)
                {
                    int numGuiaId = this._viaje.NumGuiaId;
                    if (listaValesClientes.Count > 0)
                    {
                        return this.generarNuevosViajes(listaValesClientes);
                    }
                    this.cabecera.buscar(numGuiaId);
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                }
                result2 = result;
            }
            catch (Exception exception)
            {
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
            return result2;
        }

        private OperationResult validar()
        {
            try
            {
                var viaje = _viaje;
                if (dtpFechaFin.Value == null)
                {
                    return new OperationResult(ResultTypes.warning, "SE REQUIERE ESPECIFICAR LA FECHA DE FINALIZACIÓN");
                }
                DateTime fechaFin = dtpFechaFin.Value.Value;

                List<ValeCarga> listVales = this.getListaVales();
                List<ValeCarga> listaValesClientes = this.getListaValesOtrosClientes();
                var fecha = _viaje.FechaHoraViaje;
                foreach (ValeCarga valeCarga in listVales)
                {
                    if (valeCarga.fechaCarga == null)
                    {
                        return new OperationResult(ResultTypes.warning, string.Format("EL VALE DE CARGA {0} NO TIENE FECHA DE CARGA", valeCarga.idValeCarga));
                    }
                    else
                    {
                        if (valeCarga.fechaCarga < dtpFechaInicio.Value)
                        {
                            return new OperationResult(ResultTypes.warning, string.Format("LA FECHA DE CARGA DEL VALE {0} TIENE QUE SE MAYOR A LA FECHA DE INICIO DEL VIAJE", valeCarga.idValeCarga));
                        }
                        if (valeCarga.fechaCarga > fechaFin)
                        {
                            return new OperationResult(ResultTypes.warning, string.Format("LA FECHA DE CARGA DEL VALE {0} TIENE QUE SE MENOR A LA FECHA DE FIN DEL VIAJE", valeCarga.idValeCarga));
                        }
                    }
                    if (valeCarga.fechaDescarga == null)
                    {
                        return new OperationResult(ResultTypes.warning, string.Format("EL VALE DE CARGA {0} NO TIENE FECHA DE DESCARGA", valeCarga.idValeCarga));
                    }
                    else
                    {
                        if (valeCarga.fechaDescarga < dtpFechaInicio.Value)
                        {
                            return new OperationResult(ResultTypes.warning, string.Format("LA FECHA DE DESCARGA DEL VALE {0} TIENE QUE SE MAYOR A LA FECHA DE INICIO DEL VIAJE", valeCarga.idValeCarga));
                        }
                        if (valeCarga.fechaDescarga > fechaFin)
                        {
                            return new OperationResult(ResultTypes.warning, string.Format("LA FECHA DE DESCARGA DEL VALE {0} TIENE QUE SE MENOR A LA FECHA DE FIN DEL VIAJE", valeCarga.idValeCarga));
                        }
                    }

                    if (valeCarga.fechaCarga > valeCarga.fechaDescarga)
                    {
                        return new OperationResult(ResultTypes.warning, string.Format("LA FECHA DE CARGA DEL VALE {0} TIENE QUE SE MENOR A LA FECHA DE DESCARGA", valeCarga.idValeCarga));
                    }
                    //if (valeCarga.vDescargado > valeCarga.volDescarga )
                    //{
                    //    return new OperationResult(ResultTypes.warning, string.Format("EL VOLUMEN DE DESCARGA NO PUEDE SER MAYOR AL VOLUMEN DE CARGA(VALE DE CARGA {0}) ", valeCarga.idValeCarga));
                    //}
                }
                foreach (ValeCarga valeCarga in listaValesClientes)
                {
                    if (valeCarga.fechaCarga == null)
                    {
                        return new OperationResult(ResultTypes.warning, string.Format("EL VALE DE CARGA {0} NO TIENE FECHA DE CARGA", valeCarga.idValeCarga));
                    }
                    else
                    {
                        if (valeCarga.fechaCarga < dtpFechaInicio.Value)
                        {
                            return new OperationResult(ResultTypes.warning, string.Format("LA FECHA DE CARGA DEL VALE {0} TIENE QUE SE MAYOR A LA FECHA DE INICIO DEL VIAJE", valeCarga.idValeCarga));
                        }
                        if (valeCarga.fechaCarga > fechaFin)
                        {
                            return new OperationResult(ResultTypes.warning, string.Format("LA FECHA DE CARGA DEL VALE {0} TIENE QUE SE MENOR A LA FECHA DE FIN DEL VIAJE", valeCarga.idValeCarga));
                        }
                    }
                    if (valeCarga.fechaDescarga == null)
                    {
                        return new OperationResult(ResultTypes.warning, string.Format("EL VALE DE CARGA {0} NO TIENE FECHA DE DESCARGA", valeCarga.idValeCarga));
                    }
                    else
                    {
                        if (valeCarga.fechaDescarga < dtpFechaInicio.Value)
                        {
                            return new OperationResult(ResultTypes.warning, string.Format("LA FECHA DE DESCARGA DEL VALE {0} TIENE QUE SE MAYOR A LA FECHA DE INICIO DEL VIAJE", valeCarga.idValeCarga));
                        }
                        if (valeCarga.fechaDescarga > fechaFin)
                        {
                            return new OperationResult(ResultTypes.warning, string.Format("LA FECHA DE DESCARGA DEL VALE {0} TIENE QUE SE MENOR A LA FECHA DE FIN DEL VIAJE", valeCarga.idValeCarga));
                        }
                    }

                    if (valeCarga.fechaCarga > valeCarga.fechaDescarga)
                    {
                        return new OperationResult(ResultTypes.warning, string.Format("LA FECHA DE CARGA DEL VALE {0} TIENE QUE SE MENOR A LA FECHA DE DESCARGA", valeCarga.idValeCarga));
                    }
                }

                return new OperationResult(ResultTypes.success, "");
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        private OperationResult guardarNewViajes(Cliente cliente, Viaje newVieje)
        {
            newVieje.EstatusGuia = "FINALIZADO";
            newVieje.FechaHoraFin = DateTime.Now;
            newVieje.UserViaje = mainWindow.usuario.idUsuario;
            
            string str = new Core.Utils.Util().getRutaXML();
            if (string.IsNullOrEmpty(str))
            {
                return new OperationResult
                {
                    valor = 2,
                    mensaje = "Para proceder se requiere la ruta del archivo XML"
                };
            }
            ComercialCliente respCliente = new ComercialCliente
            {
                nombre = cliente.nombre,
                calle = cliente.domicilio,
                rfc = cliente.rfc,
                colonia = cliente.colonia,
                estado = cliente.estado,
                municipio = cliente.municipio,
                pais = cliente.pais,
                telefono = cliente.telefono,
                cp = cliente.cp
            };
            ComercialEmpresa respEmpresa = new ComercialEmpresa
            {
                nombre = base.mainWindow.inicio._usuario.personal.empresa.nombre,
                calle = base.mainWindow.inicio._usuario.personal.empresa.direccion,
                rfc = base.mainWindow.inicio._usuario.personal.empresa.rfc,
                colonia = base.mainWindow.inicio._usuario.personal.empresa.colonia,
                estado = base.mainWindow.inicio._usuario.personal.empresa.estado,
                municipio = base.mainWindow.inicio._usuario.personal.empresa.municipio,
                pais = base.mainWindow.inicio._usuario.personal.empresa.pais,
                telefono = base.mainWindow.inicio._usuario.personal.empresa.telefono,
                cp = base.mainWindow.inicio._usuario.personal.empresa.cp
            };
            DatosFacturaXML datosFactura = ObtenerDatosFactura.obtenerDatos(str);
            if (datosFactura == null)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = "Ocurrio un error al intentar obtener datos del XML"
                };
            }
            int ordenTrabajo = 0;
            OperationResult result = new CartaPorteSvc().saveCartaPorte_V2(ref newVieje, ref ordenTrabajo, true, false, false);
            if (result.typeResult == ResultTypes.success)
            {
                if (!new Core.Utils.Util().isActivoImpresion())
                {
                    result.result = newVieje;
                    return result;
                }
                List<List<CartaPorte>> list = this.agruparByZona(((Viaje)result.result).listDetalles);
                foreach (List<CartaPorte> list2 in list)
                {
                    this.imprimirReportes(respCliente, respEmpresa, datosFactura, list2, (Viaje)result.result);
                }
            }
            return result;
        }

        private void imprimirReportes(ComercialCliente respCliente, ComercialEmpresa respEmpresa, DatosFacturaXML datosFactura, List<CartaPorte> listaAgrupada, Viaje viaje)
        {
            try
            {
                base.Cursor = System.Windows.Input.Cursors.Wait;
                new ReportView(base.mainWindow).cartaPorteFormatoNuevo(respCliente, respEmpresa, datosFactura, listaAgrupada, viaje, new Core.Utils.Util().getRutasImpresora("VIAJES_ATLANTE"));
            }
            catch (Exception exception)
            {
                System.Windows.MessageBox.Show(exception.Message);
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }
        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                ValeCarga content = (ValeCarga)((System.Windows.Controls.ListViewItem)sender).Content;
                this.mapControles(content);
            }
        }

        private bool mapComboClientes()
        {
            try
            {
                OperationResult resp = new ClienteSvc().getClientesALLorById(base.mainWindow.empresa.clave, 0, true);
                if (resp.typeResult == ResultTypes.success)
                {
                    this.cbxClienteVale.ItemsSource = (List<Cliente>)resp.result;
                    return true;
                }
                ImprimirMensaje.imprimir(resp);
                return false;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return false;
            }
        }

        private void mapControles(ValeCarga valeCarga)
        {
            if (valeCarga != null)
            {
                this.txtValeCarga.Text = valeCarga.idValeCarga.ToString();
                this.txtRemision.Text = string.IsNullOrEmpty(valeCarga.remision) ? string.Empty : valeCarga.remision.ToString();
                this.selectCombo(valeCarga.cliente);
                this.cbxClienteVale.IsEnabled = valeCarga.cartaPorte == 0;
                this.mapOrigen(valeCarga.origen);
                this.mapDestino(valeCarga.destino);
                this.txtCantidad.valor = valeCarga.volDescarga; // (valeCarga.volDescarga <= decimal.Zero) ? string.Empty : valeCarga.volDescarga.ToString();
                this.txtVDescargado.valor = valeCarga.vDescargado;
                this.mapProducto(valeCarga.producto);
                this.txtRemision.Focus();
                this.stpControles.IsEnabled = true;
                this.txtVDescargado.valor = valeCarga.vDescargado;
                this.dtpFechaCarga.Value = valeCarga.fechaCarga;
                this.dtpFechaDescarga.Value = valeCarga.fechaDescarga;
            }
            else
            {
                this.txtValeCarga.Clear();
                this.txtRemision.Clear();
                this.mapOrigen(null);
                this.mapDestino(null);
                this.mapProducto(null);
                this.txtCantidad.Clear();
                this.txtVDescargado.valor = 0;
                dtpFechaCarga.Value = null;
                dtpFechaDescarga.Value = null;
                this.cbxClienteVale.SelectedItem = null;
                this.stpControles.IsEnabled = false;
            }
            this.txtRemision.Focus();
        }

        private void mapDestino(Zona destino)
        {
            if (destino != null)
            {
                this.txtDestino.Text = destino.descripcion;
                this.txtDestino.Tag = destino;
                this.txtProducto.Focus();

                Cliente selectedItem = cbxClienteVale.SelectedItem as Cliente;
                OperationResult resp = new FraccionSvc().getFraccionALLorById(selectedItem.clave, destino.clasificacion.idClasificacionProducto);
                if (resp.typeResult == ResultTypes.success)
                {
                    this.listaProductos = ((List<Fraccion>)resp.result)[0].listProductos;
                }
                else
                {
                    this.listaProductos = new List<Producto>();
                    ImprimirMensaje.imprimir(resp);
                }
            }
            else
            {
                this.txtDestino.Clear();
                this.txtDestino.Tag = null;
                this.listaProductos = new List<Producto>();
            }
        }

        private void mapDetalles(List<CartaPorte> listDetalles)
        {
            int idVale = 0;
            this.lvlCartaPortes.Items.Clear();
            foreach (CartaPorte porte in listDetalles)
            {
                System.Windows.Controls.ListViewItem newItem = new System.Windows.Controls.ListViewItem
                {
                    Content = porte
                };
                this.combustible = porte.idCargaGasolina != 0;
                if (this.combustible)
                {
                    idVale = porte.idCargaGasolina;
                }
                this.lvlCartaPortes.Items.Add(newItem);
            }
            if (idVale != 0)
            {
                OperationResult resp = new OperacionSvc().cargaCombustibleByIdVale(idVale);
                if (resp.typeResult == ResultTypes.success)
                {
                    this.mapFormCargaCombustible((CargaCombustible)resp.result);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    this.mapFormCargaCombustible(null);
                }
            }
        }

        private Viaje mapForm()
        {
            try
            {
                Viaje viaje = this._viaje;
                viaje.NumGuiaId = 0;
                viaje.EstatusGuia = "ACTIVO";
                viaje.listDetalles = new List<CartaPorte>();
                foreach (System.Windows.Controls.ListViewItem item in (IEnumerable)this.lvlCartaPortes.Items)
                {
                    CartaPorte content = new CartaPorte();
                    content = (CartaPorte)item.Content;
                    content.Consecutivo = 0;
                    viaje.listDetalles.Add(content);
                }
                return viaje;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void mapFormCargaCombustible(CargaCombustible result)
        {
            if (result != null)
            {
                if (!result.fechaCombustible.HasValue)
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "NO SE HA REALIZADO LA CARGA DE COMBUSTIBLE PARA ESTA ORDEN DE SERVICIO"
                    };
                    ImprimirMensaje.imprimir(resp);
                    this.mapFormCargaCombustible(null);
                }
                this.txtOrdenCarga.Text = result.idOrdenServ.ToString();
                this.txtOrdenCarga.Tag = result;
                this.txtNoVale.Text = result.idCargaCombustible.ToString();
                this.txtFechaCarga.Text = !result.fechaCombustible.HasValue ? "" : result.fechaCombustible.ToString();
                this.txtVolumenCarga.Text = result.ltsSumaTotales.ToString();
                this.gBoxValeCombustible.IsEnabled = false;
                this.LBL.Content = "No. Orden:";
                if (result.masterOrdServ.tractor.clave != this._viaje.tractor.clave)
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "LA ORDEN DE SERVICIO NO CORRESPONDE AL MISMO TRACTOR DEL VIAJE"
                    };
                    ImprimirMensaje.imprimir(resp);
                    this.mapFormCargaCombustible(null);
                }
            }
            else
            {
                this.txtOrdenCarga.Clear();
                this.txtOrdenCarga.Tag = null;
                this.txtNoVale.Clear();
                this.txtFechaCarga.Clear();
                this.txtVolumenCarga.Clear();
                this.gBoxValeCombustible.IsEnabled = true;
            }
        }

        private void mapOrigen(Origen origen)
        {
            if (origen != null)
            {
                this.txtOrigen.Text = origen.nombreOrigen;
                this.txtOrigen.Tag = origen;
                this.buscarDestinos();
                this.txtDestino.Focus();
            }
            else
            {
                this.txtOrigen.Clear();
                this.txtOrigen.Tag = null;
                this._listZonas = new List<Zona>();
            }
        }

        public void mapPantalla(Viaje _viaje)
        {
            try
            {
                base.Cursor = System.Windows.Input.Cursors.Wait;
                this.mapDetalles(_viaje.listDetalles);
                dtpFechaInicio.Value = _viaje.FechaHoraViaje;
                dtpFechaFin.Value = _viaje.FechaHoraFin == null ? DateTime.Now : _viaje.FechaHoraFin; 
                OperationResult resp = new CartaPorteSvc().getValesCargaByIdViaje(_viaje);
                if (resp.typeResult == ResultTypes.success)
                {
                    this.mapValesCarga((List<ValeCarga>)resp.result);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    base.mainWindow.scrollContainer.IsEnabled = false;
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.nuevo);
                }
                if (((_viaje.EstatusGuia.ToUpper() == "CANCELADO") || (_viaje.EstatusGuia.ToUpper() == "FINALIZADO")) || (_viaje.EstatusGuia.ToUpper() == "FACTURADO"))
                {
                    base.mainWindow.scrollContainer.IsEnabled = false;
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.nuevo);
                }
                else if (_viaje.EstatusGuia.ToUpper() == "EXTERNO")
                {
                    this.LBL.Content = "Ticket:";
                }

                if ((_viaje.EstatusGuia.ToUpper() == "FINALIZADO") && txtOrdenCarga.Tag == null)
                {
                    base.mainWindow.scrollContainer.IsEnabled = true;
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.nuevo|ToolbarCommands.guardar|ToolbarCommands.cerrar);
                    stpDetalles.IsEnabled = false;
                    stpFechas.IsEnabled = false;
                    gBoxValeCombustible.IsEnabled = true;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }

        private void mapProducto(Producto producto)
        {
            if (producto != null)
            {
                this.txtProducto.Text = producto.descripcion;
                this.txtProducto.Tag = producto;
                this.txtCantidad.Focus();
                //if (!string.IsNullOrEmpty(this.txtCantidad.Text))
                //{
                //    this.txtCantidad.Select(0, this.txtCantidad.Text.Length);
                //}
                this.lblCantidad.Content = "Cantidad " + producto.clave_unidad_de_medida;
            }
            else
            {
                this.txtProducto.Clear();
                this.txtProducto.Tag = null;
                this.lblCantidad.Content = "Cantidad";
            }
        }

        private void mapValesCarga(List<ValeCarga> result)
        {
            this.lvlValesCarga.Items.Clear();
            if (result != null)
            {
                foreach (ValeCarga carga in result)
                {
                    System.Windows.Controls.ListViewItem newItem = new System.Windows.Controls.ListViewItem
                    {
                        Content = carga
                    };
                    newItem.Selected += new RoutedEventHandler(this.Lvl_Selected);
                    this.lvlValesCarga.Items.Add(newItem);
                }
            }
        }

        private void modificar()
        {
            try
            {
                this.listItems = new List<ValeCarga>();
                ValeCarga content = (ValeCarga)((System.Windows.Controls.ListViewItem)this.lvlValesCarga.SelectedItem).Content;
                content.remision = this.txtRemision.Text;
                content.origen = (Origen)this.txtOrigen.Tag;
                content.destino = (Zona)this.txtDestino.Tag;
                content.precio = (this._viaje.tipoViaje == 'S') ? ((Zona)this.txtDestino.Tag).costoSencillo : ((Zona)this.txtDestino.Tag).costoFull;
                content.producto = (Producto)this.txtProducto.Tag;
                content.volDescarga = txtCantidad.valor; //. Convert.ToDecimal(this.txtCantidad.Text);
                content.vDescargado = txtVDescargado.valor;
                content.fechaCarga = dtpFechaCarga.Value;
                content.fechaDescarga = dtpFechaDescarga.Value;
                content.validado = true;
                content.cliente = (Cliente)this.cbxClienteVale.SelectedItem;
                System.Windows.Controls.ListViewItem insertItem = new System.Windows.Controls.ListViewItem();
                if (content.cliente.clave != this._viaje.cliente.clave)
                {
                    insertItem.Foreground = Brushes.Red;
                }
                else
                {
                    insertItem.Foreground = Brushes.SteelBlue;
                }
                insertItem.Selected += new RoutedEventHandler(this.Lvl_Selected);
                insertItem.Content = content;
                int selectedIndex = this.lvlValesCarga.SelectedIndex;
                this.lvlValesCarga.Items.Remove(this.lvlValesCarga.SelectedItem);
                this.lvlValesCarga.Items.Insert(selectedIndex, insertItem);
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        public override void nuevo()
        {
            this.gBoxValeCombustible.IsEnabled = true;
            this.LBL.Content = "No. Orden:";
            this.combustible = false;
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
            this.mapFormCargaCombustible(null);
            this.gboxVales.IsEnabled = true;
            this.cabecera.nuevo();
            this.lvlCartaPortes.Items.Clear();
            this.lvlValesCarga.Items.Clear();
            this.mapControles(null);
            this._viaje = null;
            dtpFechaFin.Value = DateTime.Now;
            dtpFechaInicio.Value = DateTime.Now;

            stpDetalles.IsEnabled = true;
            stpFechas.IsEnabled = true;
            gBoxValeCombustible.IsEnabled = true;
        }

        private void selectCombo(Cliente cliente)
        {
            if (cliente != null)
            {
                int num = 0;
                foreach (Cliente cliente2 in (IEnumerable)this.cbxClienteVale.Items)
                {
                    if (cliente.clave == cliente2.clave)
                    {
                        this.cbxClienteVale.SelectedIndex = num;
                        break;
                    }
                    num++;
                }
            }
            else
            {
                this.cbxClienteVale.SelectedItem = null;
            }
        }

        private void txtCantidad_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtCantidad.valor > 0)
                {
                    txtVDescargado.txtDecimal.Focus();
                }
                txtVDescargado.txtDecimal.Focus();
                //string str = this.txtCantidad.Text.Trim();
                //if (!string.IsNullOrEmpty(str))
                //{
                //    decimal result = 0M;
                //    if (decimal.TryParse(str, out result) && (result > decimal.Zero))
                //    {
                //        this.modificar();
                //        this.mapControles(null);
                //    }
                //}
            }
        }
        private void TxtVDescargado_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtVDescargado.valor > 0)
                {
                    this.modificar();
                    this.mapControles(null);
                }
            }
        }
        private void txtDestino_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && (this._listZonas.Count > 0))
            {
                List<Zona> list = new List<Zona>();
                List<Zona> list2 = this._listZonas.FindAll(S => S.descripcion.IndexOf(this.txtDestino.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (Zona zona in list2)
                {
                    list.Add(zona);
                }
                selectDestinos destinos = new selectDestinos(this.txtDestino.Text);
                if (list.Count == 1)
                {
                    this.mapDestino(list[0]);
                }
                else
                {
                    Zona destino = destinos.buscarZona(this._listZonas);
                    this.mapDestino(destino);
                }
            }
        }

        private void txtOrdenCarga_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (this._viaje.EstatusGuia == "EXTERNO")
                {
                    OperationResult resp = new CargaCombustibleSvc().getCargaCombustibleExternoByTicket(this.txtOrdenCarga.Text.Trim());
                    if (resp.typeResult == ResultTypes.success)
                    {
                        this.mapFormCargaCombustible((CargaCombustible)resp.result);
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                        this.mapFormCargaCombustible(null);
                    }
                }
                else
                {
                    int result = 0;
                    if (int.TryParse(this.txtOrdenCarga.Text.Trim(), out result))
                    {
                        OperationResult resp = new OperacionSvc().getSalidasEntradasByIdOrdenServ(result);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            this.mapFormCargaCombustible((CargaCombustible)resp.result);
                        }
                        else
                        {
                            ImprimirMensaje.imprimir(resp);
                            this.mapFormCargaCombustible(null);
                        }
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("El folio de la orden de servicio no es valido", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        this.mapFormCargaCombustible(null);
                    }
                }
            }
        }

        private void txtOrigen_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                List<Origen> list = new List<Origen>();
                if (this._listOrigen.Count > 0)
                {
                    List<Origen> list2 = this._listOrigen.FindAll(S => S.nombreOrigen.IndexOf(this.txtOrigen.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                    foreach (Origen origen in list2)
                    {
                        list.Add(origen);
                    }
                    selectOrigenes origenes = new selectOrigenes(this.txtOrigen.Text);
                    if (list.Count == 1)
                    {
                        this.mapOrigen(list[0]);
                    }
                    else
                    {
                        Origen origen2 = origenes.buscarOrigenes(this._listOrigen);
                        this.mapOrigen(origen2);
                    }
                }
            }
        }

        private void txtOrigen_TextChanged(object sender, TextChangedEventArgs e)
        {
            this._listZonas = new List<Zona>();
            this.mapDestino(null);
        }

        private void txtProducto_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                List<Producto> list = new List<Producto>();
                if (this.listaProductos.Count > 0)
                {
                    List<Producto> list2 = this.listaProductos.FindAll(S => S.descripcion.IndexOf(this.txtProducto.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                    foreach (Producto producto2 in list2)
                    {
                        list.Add(producto2);
                    }
                    selectProducto producto = new selectProducto(this.txtProducto.Text);
                    if (list.Count == 1)
                    {
                        this.mapProducto(list[0]);
                    }
                    else
                    {
                        Producto producto3 = producto.buscarProductos(this.listaProductos);
                        this.mapProducto(producto3);
                    }
                }
            }
        }

        private void txtRemision_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && !string.IsNullOrEmpty(this.txtRemision.Text.Trim()))
            {
                this.txtOrigen.Focus();
            }
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                txtCantidad.txtDecimal.Foreground = Brushes.SteelBlue;
                txtVDescargado.txtDecimal.Foreground = Brushes.SteelBlue;

                txtCantidad.txtDecimal.FontSize = 9.5;
                txtVDescargado.txtDecimal.FontSize = 9.5;

                txtCantidad.txtDecimal.VerticalContentAlignment = VerticalAlignment.Center;

                if (!this.establecerImpresora())
                {
                    base.mainWindow.scrollContainer.IsEnabled = false;
                }
                else
                {
                    base.Cursor = System.Windows.Input.Cursors.Wait;
                    this.nuevo();
                    if (!this.mapComboClientes())
                    {
                        base.mainWindow.scrollContainer.IsEnabled = false;
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                base.mainWindow.scrollContainer.IsEnabled = false;
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }
        private class NewViajes
        {            
            public Cliente cliente { get; set; }
            public List<ValeCarga> listaVales { get; set; }
        }

        
    }
}
