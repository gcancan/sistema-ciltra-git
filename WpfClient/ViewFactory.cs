﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Windows;
using menu = Fletera;
using menu2 = Fletera2;
using ap = System.Windows.Forms;
using Core.Utils;
using System.Net.NetworkInformation;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using Core.Models;

namespace WpfClient
{
    public partial class ViewFactory
    {
        public static IView createView(string claveMenu, MainWindow mainWindow)
        {
            if (!Enum.IsDefined(typeof(Menus), claveMenu))
            {
                //MessageBox.Show("No se ha agregado el menú " + claveMenu + " a la enumeración Menus.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return null;
                //throw new Exception("No se ha agregado el menú " + claveMenu + " a la enumeración Menus.");
            }

            BitacoraAcceso bitacoraAcceso = new BitacoraAcceso
            {
                idBitacoraAcceso = 0,
                acceso = claveMenu,
                fecha = DateTime.Now,
                usuario = mainWindow.usuario.nombreUsuario,
                contraseña = mainWindow.usuario.contrasena,
                nombreEquipo = mainWindow.nombreEquipo
            };

            var resp = new BitacoraAccesoSvc().saveBitacoraAcceso(ref bitacoraAcceso);
            if (resp.typeResult != ResultTypes.success)
            {
                ImprimirMensaje.imprimir(resp);
            }

            IView view = null;
            var con = Core.Utils.BoundedContextFactory.ConnectionFactory().ConnectionString;
            var conComercial = Core.Utils.BoundedContextFactory.ConnectionFactory(true).ConnectionString;
            var conConfigl = Core.Utils.BoundedContextFactory.ConnectionFactoryCompacWAdmin().ConnectionString;
            var conCOMRemolques = Core.Utils.BoundedContextFactory.ConnectionFactoryTaller(true).ConnectionString;
            //var CatalogComercial = Core.Utils.BoundedContextFactory.ConnectionFactory 
            string connectionString = BoundedContextFactory.ConnectionFactory(false, "").ConnectionString;
            string sqlConComercial = BoundedContextFactory.ConnectionFactory(true, "").ConnectionString;
            string str4 = BoundedContextFactory.ConnectionFactoryTaller(true).ConnectionString;
            switch ((Menus)Enum.Parse(typeof(Menus), claveMenu))
            {
                //case Menus.MENU_CATALOGOS_EDIT_UNIDAD_TRANSPORTE:
                //    view = new editUnidadTransporte();
                //    break;
                case Menus.CARTA_PORTE:
                    //view = new EditCartaPorte();
                    view = new editViajes(); //GRCC
                    break;
                case Menus.CARGA_SALIDAS_PROGRAMADAS:
                    view = new salidasProgramadas();
                    break;
                case Menus.MENU_SALIDAS_CERRAR_SALIDAS: //GRCC 12/10/2018
                    view = new cerrarViaje();
                    break;
                case Menus.MENU_CATALOGOS_RUTAS_DESTINO: //GRCC 12/10/2018
                    view = new editRutasDestino();
                    //view = new editRutasDestinoCompleto();
                    break;
                case Menus.MENU_CATALOGOS_RUTAS_DESTINO_RECALCULADO:
                    //view = new editRutasDestino();
                    view = new editRutasDestinoCompleto();
                    break;
                case Menus.MENU_SALIDAS_REPORTE_CARTA_PORTES:
                    view = new exportarExcel();
                    //view = new editViajesEnFalso();|
                    break;
                case Menus.MENU_USUARIOS_ASIGNACION_PRIVILEGIOS:
                    view = new editUsuariosPrivilelgios();
                    break;
                case Menus.MENU_SALIDAS_CARTA_PORTES_BACHOCO:
                    view = new editSalidasBachoco();
                    break;
                case Menus.MENU_SALIDAS_REPORTE_PENALIZACIONES:
                    view = new reportePenalizaciones();
                    break;
                case Menus.MENU_SALIDA_REPORTE_BACHOCO:
                    view = new ReporteBachoco();
                    //view = new asignacionRFID();
                    break;
                case Menus.MENU_FACTURACION_NUTRICARSA:
                    var fac = new menu.fFacturaViajesAtl(con, conComercial, conConfigl, mainWindow.inicio._usuario.personal.empresa.clave, ap.Application.StartupPath);
                    fac.ShowDialog();

                    //if (mainWindow.inicio._usuario.personal.empresa.clave == 1)
                    //{
                    //    var fac = new menu.fFacturaViajes(con, conComercial, conConfigl, mainWindow.inicio._usuario.personal.empresa.clave);
                    //    fac.ShowDialog();
                    //}
                    //else if (mainWindow.inicio._usuario.personal.empresa.clave == 2)
                    //{
                    //    var fac = new menu.fFacturaViajesAtl(con, conComercial, conConfigl, mainWindow.inicio._usuario.personal.empresa.clave);
                    //    fac.ShowDialog();
                    //}

                    //var fac = new menu.fFiltroViajesFac(con, mainWindow.inicio._usuario.nombreUsuario,
                    //mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario);


                    break;
                case Menus.MENU_FACTURACION_CANCELACION:
                    var CanFac = new menu.fCancelaFactura(con, mainWindow.inicio._usuario.nombreUsuario,
                 mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario);
                    CanFac.ShowDialog();

                    break;

                case Menus.MENU_FACTURACION_FILTROFACT:
                    var facfilt = new menu.fFiltroViajesFac(con, mainWindow.inicio._usuario.nombreUsuario,
                    mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario);
                    facfilt.ShowDialog();
                    break;
                case Menus.MENU_CATALOGO_PERSONAL:

                    var personalCat = new menu.frmCatPersonal(con, true);
                    personalCat.ShowDialog();
                    //return new catalogoPersonal();
                    break;


                case Menus.MENU_CATALOGO_MARCAS:
                    var catMarcas = new menu.frmCatMarcas(con, true);
                    catMarcas.ShowDialog();
                    break;
                case Menus.MENU_CATALOGO_PRODUCTOS: //GRCC 12/10/2018
                    view = new editProductos();
                    break;
                case Menus.MENU_SALIDAS_AJUSTE_VIAJES: //GRCC 12/10/2018
                    view = new editAjusteViajes();
                    break;
                case Menus.MENU_CATALOGOS_UNIDADES_TRANSPORTE:
                    var catUnidades = new menu.frmCatUniTransp(con, true);
                    catUnidades.ShowDialog();
                    //return new catUnidadesTransporte();
                    break;
                case Menus.MENU_CATALOGOS_TIPOS_UNIDADES_TRANSPORTE:
                    var catTiposUnidades = new menu.frmCatTipoUniTrans(con, true);
                    catTiposUnidades.ShowDialog();
                    //return new catTipoUnidades();
                    break;
                case Menus.MENU_CATALOGOS_TIPOS_SERVICIO:
                    var catTiposServicio = new menu.frmCatTipoServ(con, true);
                    catTiposServicio.ShowDialog();
                    break;
                case Menus.MENU_CATALOGOS_SERVICIOS:
                    return new catServicios();
                //var catServicios = new menu.frmCatServicios(con, true);
                //catServicios.ShowDialog();
                //break;
                case Menus.MENU_CATALOGOS_PUESTOS:
                    var catPuestos = new menu.frmCatPuestos(con, true);
                    catPuestos.ShowDialog();
                    break;
                case Menus.MENU_CATALOGOS_TIPO_DE_ORDEN:
                    var catTipoOrden = new menu.frmCatTipoOrden(con, true);
                    catTipoOrden.ShowDialog();
                    break;
                case Menus.MENU_CATALOGOS_DEPTOS:
                    var catDeptos = new menu.frmCatDeptos(con, true);
                    catDeptos.ShowDialog();
                    break;
                case Menus.MENU_RECURSOS_HUMANOS_PERSONAL:
                    //var CatPerRH = new menu.frmCatPersonal(con, true);
                    //CatPerRH.ShowDialog();
                    new catalogoPersonal();
                    break;
                case Menus.MENU_CONTROL_ES_ENTRADAS_SALIDAS:
                    view = new editSalidasEntradas();
                    break;
                case Menus.MENU_CONTROL_CARGA_GASOLINA:
                    view = new editCargaCombustibleV2();
                    break;
                case Menus.MENU_RECURSOS_HUMANOS_DEPTOS:
                    var CatDeptosRH = new menu.frmCatDeptos(con, true);
                    CatDeptosRH.ShowDialog();
                    break;
                case Menus.MENU_RECURSOS_HUMANOS_PUESTOS:
                    var catPuestosRH = new menu.frmCatPuestos(con, true);
                    catPuestosRH.ShowDialog();
                    break;
                case Menus.MENU_RECURSOS_HUMANOS_COLONIA:
                    var catColoniaRH = new menu.frmCatColonias(con, true);
                    catColoniaRH.ShowDialog();
                    break;
                case Menus.MENU_RECURSOS_HUMANOS_CIUDAD:
                    var catCiudadRH = new menu.frmCatCiudad(con, true);
                    catCiudadRH.ShowDialog();
                    break;
                case Menus.MENU_RECURSOS_HUMANOS_ESTADO:
                    var catEstadoRH = new menu.frmCatEstados(con, true);
                    catEstadoRH.ShowDialog();
                    break;
                case Menus.MENU_RECURSOS_HUMANOS_PAIS:
                    var catPaisRH = new menu.frmCatPais(con, true);
                    catPaisRH.ShowDialog();
                    break;
                case Menus.MENU_SERVICIOS_SOLORDENSERV:
                    //var ServSol = new menu.frmSolServ(con, mainWindow.inicio._usuario.nombreUsuario,
                    //mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario);
                    //ServSol.ShowDialog();

                    var nsolOS = new menu2.Procesos.fSolicitudOS(mainWindow.inicio._usuario);
                    nsolOS.StartPosition = ap.FormStartPosition.CenterParent;
                    nsolOS.ShowDialog();

                    //return new ProcesosOS.EditSolicitudOrdenServicio();
                    break;
                case Menus.MENU_SALIDAS_VIAJE_EN_FALSO:
                    view = new editViajesEnFalso();
                    break;
                case Menus.MENU_CATALOGOS_ASIGNACION_RFID:
                    view = new asignacionRFID();
                    break;
                case Menus.MENU_SALIDAS_CARTA_PORTE_ATLANTE: //GRCC 12/10/2018
                    view = new editCartaPorteAtlante();
                    break;
                case Menus.ORIGENES_DESTINOS_ATLANTE: //GRCC 11/10/2018
                    view = new editOrigenesDestinosAtlante();
                    break;
                case Menus.MENU_SALIDAS_CHECAR_VIAJES:
                    view = new checkViajes();
                    break;
                case Menus.MENU_COMBUSTIBLE_TIPO_COMBUSTIBLE:
                    view = new editTipoCombustible();
                    break;
                case Menus.MENU_MONITORES_MASTER:
                    //var ServSol = new menu.frmSolServ(con, mainWindow.inicio._usuario.nombreUsuario,
                    //mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario);
                    //ServSol.ShowDialog();
                    var MonUni = new menu.FMonitorGuardias(con);
                    MonUni.ShowDialog();
                    break;
                case Menus.MENU_CATALOGOS_TABLAS_ACTIVIDADES:
                    var catAct = new menu.frmCatActividad(con, true);
                    catAct.ShowDialog();
                    break;
                case Menus.MENU_CATALOGOS_TABLAS_COLONIAS:
                    var catCol1 = new menu.frmCatColonias(con, true);
                    catCol1.ShowDialog();

                    break;
                case Menus.MENU_CATALOGOS_TABLAS_DIVISION:
                    var catDiv = new menu.frmCatDivision(con, true);
                    catDiv.ShowDialog();

                    break;

                case Menus.MENU_CATALOGOS_TABLAS_ALMACENES:
                    var catAlm = new menu.frmCatAlmacen(con, true);
                    catAlm.ShowDialog();
                    break;
                case Menus.MENU_CATALOGOS_TABLAS_ESTADOS:
                    var catEst = new menu.frmCatEstados(con, true);
                    catEst.ShowDialog();

                    break;
                case Menus.MENU_CATALOGOS_TABLAS_FAMILIAS:
                    var catFam = new menu.frmCatFamilias(con, true);
                    catFam.ShowDialog();

                    break;
                case Menus.MENU_CATALOGOS_TABLAS_HERRAMIENTAS:
                    var catHerr = new menu.frmCatHerramienta(con, true);
                    catHerr.ShowDialog();

                    break;
                case Menus.MENU_CATALOGOS_TABLAS_MARCAS:
                    //var catMar = new menu.frmCatMarcas(con, true);
                    //catMar.ShowDialog();
                    view = new catMarcasLlantas();
                    break;
                case Menus.MENU_CATALOGOS_TABLAS_PROVEEDORES:
                    var catProv = new menu.frmCatProveedor(con, true);
                    catProv.ShowDialog();
                    //catProv.ShowDialog();

                    break;
                case Menus.MENU_CATALOGOS_TABLAS_SERVICIOS:
                    var catServ = new menu.frmCatServicios(con, true);
                    catServ.ShowDialog();

                    break;
                case Menus.MENU_CATALOGOS_TABLAS_SUCURSAL:
                    var catSuc = new menu.frmCatSucursal(con, true);
                    catSuc.ShowDialog();

                    break;
                case Menus.MENU_CATALOGOS_TABLAS_TIPOHERRAMIENTA:
                    var catTHer = new menu.frmCatTipoHerr(con, true);

                    catTHer.ShowDialog();

                    break;
                case Menus.MENU_CATALOGOS_TABLAS_TIPOORDENSERV:
                    var catTOrSer = new menu.frmCatTipoOrden(con, true);
                    catTOrSer.ShowDialog();
                    break;
                case Menus.MENU_CATALOGOS_TABLAS_TIPOUNITRANS:
                    var catTUniTra = new menu.frmCatCombExtra(con, true);
                    catTUniTra.ShowDialog();
                    break;
                // 
                case Menus.MENU_FACTURACION_CATSERVFACT:
                    var catServFact = new menu.frmCatServFact(con, true, mainWindow.inicio._usuario.personal.empresa.clave);
                    catServFact.ShowDialog();
                    break;
                case Menus.MENU_COMBUSTIBLE_PRECIO_COMBUSTIBLE:
                    view = new PreciosCombustibles();
                    break;
                case Menus.MENU_CATALOGO_CAMBIO_DE_PRECIOS:
                    view = new editCambioPrecios();
                    break;
                case Menus.MENU_REPORTES_INGRESOS_EGRESOS:
                    view = new ReporteIngresosEgresos();
                    break;
                case Menus.MENU_REPORTES_CAMBIO_PRECIOS:
                    view = new ReporteBitacoraPrecios();
                    break;
                case Menus.MENU_SERVICIOS_LLANTAS_CATLLANTAS:
                    var catLla = new menu.fCatLlantasF(con, true, mainWindow.inicio._usuario.nombreUsuario);
                    catLla.ShowDialog();
                    break;
                case Menus.MENU_SERVICIOS_LLANTAS_CATTIPOLLANTA:
                    var catTipLla = new menu.CatTipoLlanta(con, true);
                    catTipLla.ShowDialog();
                    break;
                case Menus.MENU_SERVICIOS_LLANTAS_CATCLASLLAN:
                    var catClasLla = new menu.frmCatClasLlan(con, true, mainWindow.inicio._usuario.nombreUsuario);
                    catClasLla.ShowDialog();
                    break;
                case Menus.MENU_REPORTES_BITACORA_ENTRADAS_SALIDAS:
                    view = new ReporteEntradasSalidasPatios();
                    break;
                case Menus.MENU_REPORTES_CARGA_DE_COMBUSTIBLE:
                    view = new ReporteBitacoraCargaCombustible();  //GRCC 10/10/2018
                    break;
                case Menus.MENU_REPORTES_KARDEX_MOV_UNI_TRANS:
                    view = new ReporteHistorialUnidad();
                    break;
                case Menus.MENU_SALIDAS_FINALIZAR_VIAJES_ATLANTE: // GRCC 12/10/2018 
                    view = new editFinalizarViajesAtlante();
                    break;
                case Menus.MENU_CATALOGOS_COMBUSTIBLE_EXTRA:
                    var catcombExt = new menu.frmCatCombExtra(con, true);
                    catcombExt.ShowDialog();
                    break;
                case Menus.MENU_COMBUSTIBLE_LLENADO_COMBUSTIBLE_EX:
                    //'
                    view = new editLlenadoCombustibleExtra();
                    break;
                    var CombExCap = new menu.fCombExtraCap(con, mainWindow.inicio._usuario.nombreUsuario,
                    mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario);
                    CombExCap.ShowDialog();

                    // var CombExCap = new menu.fEntregaDineroOpe(con, mainWindow.inicio._usuario.nombreUsuario,
                    //mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario,
                    //mainWindow.inicio._usuario.personal.nombre, mainWindow.inicio._usuario.personal.clave );
                    // CombExCap.ShowDialog();
                    break;
                case Menus.MENU_COMBUSTIBLE_CARGA_COMBUSTIBLE_EXTRA:
                    view = new editCombustibleExtra();
                    break;
                    var CombEx = new menu.fCombExtra(con, mainWindow.inicio._usuario.nombreUsuario,
                    mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario);

                    CombEx.ShowDialog();
                    break;
                case Menus.MENU_FACTURACION_CATCLIENTES:
                    var catcli = new menu.frmCatClientes(con, mainWindow.inicio._usuario.nombreUsuario,
                    mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario);
                    catcli.ShowDialog();
                    break;

                case Menus.MENU_COMBUSTIBLE_COMBUSTIBLE_EXTERNO:
                    view = new editCombustibleExterto();
                    break;
                case Menus.MENU_SALIDAS_VIAJES_V2:
                    view = new editCartaPortesV2();
                    break;
                case Menus.MENU_SERVICIOS_LAVADERO: //GRCC 12/10/2018
                    view = new servicioLavadero();
                    break;
                //case Menus.MENU_SERVICIOS_RECEPCIONORDENSERV:
                //view = new 
                //  break;

                case Menus.MENU_SERVICIOS_ENTREGA_ORDEN:
                    view = new entregaOrdenAumentoEnInsumos();
                    break;
                case Menus.MENU_SALIDAS_VIAJES_PENDIENTES_ATL:
                    view = new editViajesPendientesAtlante();
                    break;
                case Menus.MENU_SALIDAS_LIQUIDACIONES_RECIBODINERO:
                    //var recdin = new menu.fEntregaDineroPrinc(con, mainWindow.inicio._usuario.nombreUsuario,
                    //mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario,
                    //mainWindow.inicio._usuario.personal.nombre);
                    var recdin = new menu.fEntregaDineroPrinc(con, mainWindow.inicio._usuario.nombreUsuario,
                    mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario,
                    mainWindow.inicio._usuario.personal.nombre, mainWindow.inicio._usuario.personal.clave);
                    recdin.ShowDialog();
                    break;
                case Menus.MENU_SALIDAS_LIQUIDACIONES_FOLIOSDINERO:
                    var foldin = new menu.fEntregaDineroOpe(con, mainWindow.inicio._usuario.nombreUsuario,
                    mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario,
                    mainWindow.inicio._usuario.personal.nombre, mainWindow.inicio._usuario.personal.clave);
                    foldin.ShowDialog();
                    break;
                case Menus.MENU_CATALOGOS_INSPECCIONES:
                    view = new editInspecciones();
                    break;
                case Menus.MENU_CATALOGOS_PRODUCTOS_INSUMOS:
                    view = new editProductosCom();
                    break;
                case Menus.MENU_SERVICIOS_SERV_LLANTAS:
                    view = new servicioLLantera();
                    break;
                case Menus.MENU_IMPRESION_REIMPRESION_DOC: //GRCC 12/10/2018
                    view = new reimprecionDocumentos();
                    break;
                case Menus.MENU_SALIDAS_LIQUIDACIONES_COMPGASTOS:
                    var FolDinComp = new menu.fComprGastosFolioDinero(con, mainWindow.inicio._usuario.nombreUsuario,
                  mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario,
                  mainWindow.inicio._usuario.personal.nombre, mainWindow.inicio._usuario.personal.clave);
                    FolDinComp.ShowDialog();
                    break;
                case Menus.MENU_SALIDAS_AGREGAR_VALES_CARGA:
                    view = new agregarValesDeCarga();
                    break;
                case Menus.MENU_ORDEN_COMPRA_LLANTAS_LLANTAS:
                    view = new CrearOrdenCompraLlanta();
                    break;
                case Menus.MENU_ORDEN_COMPRA_RECEPCION_ORDENES:
                    view = new asignarOrdenCompra();
                    break;
                case Menus.MENU_SERVICIOS_RESCATES:
                    view = new servicioRescate();
                    break;
                case Menus.MENU_SERVICIOS_LLANTAS_REVITALIZACION:
                    view = new editRevitalizaciones();
                    break;
                case Menus.MENU_ORDEN_COMPRA_SOLICITUD_LLANTAS:
                    view = new solicitudOrdenCompra();
                    break;
                case Menus.MENU_ORDEN_COMPRA_COTIZACION_SOLICITUDES:
                    view = new cotizacionSolicitudOrdenCompra();
                    break;
                case Menus.MENU_SERVICIOS_LLANTAS_SERVINT:
                    //var catllaser = new menu.fServInternosLlantas(con, true, mainWindow.inicio._usuario.nombreUsuario);
                    //catllaser.ShowDialog();
                    var movllan = new menu2.Procesos.fMovllantas(mainWindow.inicio._usuario, "", 0, 0);
                    movllan.ShowDialog();
                    //Fletera.TipoOrdenServicio.LLANTAS, mainWindow.inicio._usuario.personal.nombre, conComercial);
                    break;
                case Menus.MENU_SERVICIOS_LLANTAS_RECEP:
                    // var RecLlantas = new menu.frmRecpcionTaller(con, mainWindow.inicio._usuario.nombreUsuario,
                    //mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario,
                    //Fletera.TipoOrdenServicio.LLANTAS, mainWindow.inicio._usuario.personal.nombre);
                    // RecLlantas.ShowDialog();

                    var RecLlantas = new menu2.Procesos.fOrdenServicioTaller(menu2.Clases.Inicio.eEstatusOS.REC,
               menu2.Clases.Inicio.eTipoOrdenServicio.LLANTAS, mainWindow.inicio._usuario);
                    RecLlantas.StartPosition = ap.FormStartPosition.CenterParent;
                    RecLlantas.ShowDialog();
                    break;
                case Menus.MENU_SERVICIOS_LLANTAS_ASIG:
                    //  var fAsigLlantas = new menu.frmAsignacionTaller(con, mainWindow.inicio._usuario.nombreUsuario,
                    //mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario,
                    //Fletera.TipoOrdenServicio.LLANTAS, mainWindow.inicio._usuario.personal.nombre);
                    //  fAsigLlantas.ShowDialog();
                    var fAsigLlantas = new menu2.Procesos.fOrdenServicioTaller(menu2.Clases.Inicio.eEstatusOS.ASIG,
                menu2.Clases.Inicio.eTipoOrdenServicio.LLANTAS, mainWindow.inicio._usuario);
                    fAsigLlantas.StartPosition = ap.FormStartPosition.CenterParent;
                    fAsigLlantas.ShowDialog();
                    break;
                case Menus.MENU_SERVICIOS_LLANTAS_ENTREGA:
                    var EntLlantas = new menu.frmEntregaOS(con, mainWindow.inicio._usuario.nombreUsuario,
                mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario,
                Fletera.TipoOrdenServicio.LLANTAS, mainWindow.inicio._usuario.personal.nombre);
                    EntLlantas.ShowDialog();
                    break;
                case Menus.MENU_SERVICIOS_LLANTAS_DIAGNOSTICO:
                    var DiagLlantas = new menu.frmDiagnosticoOS(con, mainWindow.inicio._usuario.nombreUsuario,
                    mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario,
                    Fletera.TipoOrdenServicio.LLANTAS, mainWindow.inicio._usuario.personal.nombre, conComercial);
                    DiagLlantas.ShowDialog();

                    break;
                case Menus.MENU_SERVICIOS_LLANTAS_INVENTARIO:
                    var catLlaInv = new menu.fInventarioLlantas(con, true, mainWindow.inicio._usuario.nombreUsuario);
                    //var catLlaInv = new menu.fCatLlantasF(con, true, mainWindow.inicio._usuario.nombreUsuario);
                    catLlaInv.ShowDialog();
                    break;
                case Menus.MENU_SERVICIOS_LLANTAS_CATLLANTASPROD:
                    var catllaProd = new menu.frmCatLlantasProd(con, true);
                    catllaProd.ShowDialog();
                    break;
                case Menus.MENU_SERVICIOS_LLANTAS_CATDISENIO:
                    var catDislla = new menu.frmCatDiseñoLlanta(con, true);
                    catDislla.ShowDialog();
                    break;
                case Menus.MENU_SERVICIOS_TALLER_DIAGNOSTICO:
                    //var fdiag = new menu.frmDiagnosticoTaller(con, mainWindow.inicio._usuario.nombreUsuario,
                    //mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario);

                    var fdiag = new menu.frmDiagnosticoOS(con, mainWindow.inicio._usuario.nombreUsuario,
              mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario,
              Fletera.TipoOrdenServicio.TALLER, mainWindow.inicio._usuario.personal.nombre, conCOMRemolques);
                    fdiag.ShowDialog();
                    break;
                case Menus.MENU_SERVICIOS_TALLER_RECEPCION:
                    //var RecTaller = new menu.frmRecpcionTaller(con, mainWindow.inicio._usuario.nombreUsuario,
                    //mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario,
                    //Fletera.TipoOrdenServicio.TALLER, mainWindow.inicio._usuario.personal.nombre);
                    //RecTaller.ShowDialog();

                    var RecTallNw = new menu2.Procesos.fOrdenServicioTaller(menu2.Clases.Inicio.eEstatusOS.REC, menu2.Clases.Inicio.eTipoOrdenServicio.TALLER, mainWindow.inicio._usuario);
                    RecTallNw.StartPosition = ap.FormStartPosition.CenterParent;
                    RecTallNw.ShowDialog();
                    break;
                //OpenMainWindow("MENU_MOVIMIENTOS_RECEPCION", True)
                case Menus.MENU_SERVICIOS_TALLER_MONITOR:
                    //var fmonTaller = new menu.FMonitorTaller(con, mainWindow.inicio._usuario.nombreUsuario,
                    //mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario, 
                    //Fletera.TipoOrdenServicio.TODAS  ,true);
                    //fmonTaller.StartPosition = ap.FormStartPosition.CenterParent;
                    //fmonTaller.WindowState = ap.FormWindowState.Maximized;
                    //fmonTaller.ShowDialog();

                    var fMonOS = new menu2.Procesos.fMonitorOS();
                    fMonOS.StartPosition = ap.FormStartPosition.CenterParent;
                    fMonOS.WindowState = ap.FormWindowState.Maximized;
                    fMonOS.ShowDialog();
                    break;
                case Menus.MENU_SERVICIOS_TALLER_ASIGNACION:
                    //var fAsigTaller = new menu.frmAsignacionTaller(con, mainWindow.inicio._usuario.nombreUsuario,
                    //mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario,
                    //Fletera.TipoOrdenServicio.TALLER, mainWindow.inicio._usuario.personal.nombre);
                    //fAsigTaller.ShowDialog();

                    var AsigTallNw = new menu2.Procesos.fOrdenServicioTaller(menu2.Clases.Inicio.eEstatusOS.ASIG,
                    menu2.Clases.Inicio.eTipoOrdenServicio.TALLER, mainWindow.inicio._usuario);
                    AsigTallNw.StartPosition = ap.FormStartPosition.CenterParent;
                    AsigTallNw.ShowDialog();
                    break;
                case Menus.MENU_SERVICIOS_CATALOGOS_CONDICION:
                    var catCond = new menu.frmCatCondicion(con, true);
                    catCond.ShowDialog();
                    break;
                case Menus.MENU_SERVICIOS_CATALOGOS_DESTINOS:
                    var catDes = new menu.frmCatDestinos(con, true);
                    catDes.ShowDialog();
                    break;
                case Menus.MENU_SERVICIOS_RELACIONES_MARCASTOS:
                    //var relMarTos = new menu.frmRelServiciosActividades(con, true);
                    var relMarTos = new menu.frmRelMarcaTipoOrdSer(con, true);
                    //
                    //var relMarTos = new menu.frmRelTipoOrdServ(con, true);
                    relMarTos.ShowDialog();
                    break;
                case Menus.MENU_SERVICIOS_RELACIONES_PROVETOS:
                    var relProvTos = new menu.frmRelppROVTipoOrdSer(con, true);
                    relProvTos.ShowDialog();
                    break;
                case Menus.MENU_REPORTES_CONSUMO_UNIDADES:
                    view = new reporteConsumoUnidades();
                    break;
                case Menus.MENU_SERVICIOS_ACTIVIDADES_TALLER:
                    view = new servicioTaller();
                    break;
                case Menus.MENU_CATALOGOS_REL_TIPOS_ORD_SER:
                    view = new editRelacionTiposOrdenServ();
                    break;
                case Menus.MENU_CATALOGOS_REL_SERVICIOS_ACTIVIDADES:
                    view = new RelacionServicioActividades();
                    break;
                case Menus.MENU_SERVICIOS_TALLER_ENTREGA:
                    //    var EntTaller = new menu.frmEntregaOS(con, mainWindow.inicio._usuario.nombreUsuario,
                    //mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario,
                    //Fletera.TipoOrdenServicio.TALLER , mainWindow.inicio._usuario.personal.nombre);
                    //    EntTaller.ShowDialog();

                    var EntTallNw = new menu2.Procesos.fOrdenServicioTaller(menu2.Clases.Inicio.eEstatusOS.ENT,
                   menu2.Clases.Inicio.eTipoOrdenServicio.TALLER, mainWindow.inicio._usuario);
                    EntTallNw.StartPosition = ap.FormStartPosition.CenterParent;
                    EntTallNw.ShowDialog();
                    break;
                case Menus.MENU_SERVICIOS_TALLER_FINALIZA:
                    //      var FinTaller = new menu.frmFinalizaOS(con, mainWindow.inicio._usuario.nombreUsuario,
                    //mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario,
                    //Fletera.TipoOrdenServicio.TALLER, mainWindow.inicio._usuario.personal.nombre, conComercial);
                    //      FinTaller.ShowDialog();

                    var FinTallNw = new menu2.Procesos.fOrdenServicioTaller(menu2.Clases.Inicio.eEstatusOS.TER,
                  menu2.Clases.Inicio.eTipoOrdenServicio.TALLER, mainWindow.inicio._usuario);
                    FinTallNw.StartPosition = ap.FormStartPosition.CenterParent;
                    FinTallNw.ShowDialog();
                    break;
                case Menus.MENU_ORDEN_COMPRA_PRE_COTIZACIONES:
                    view = new editCotizarOrdenes();
                    break;
                case Menus.MENU_SERVICIOS_PRESALIDAS:
                    //var entMer = new menu.fPreSalidaPen(con, mainWindow.inicio._usuario.nombreUsuario,
                    //  mainWindow.inicio._usuario.idUsuario, mainWindow.inicio._usuario.personal.nombre,
                    //  conComercial, conConfigl);
                    //entMer.ShowDialog();

                    var preEnt = new menu2.Procesos.fPenPreSalidas(mainWindow.inicio._usuario);
                    preEnt.WindowState = ap.FormWindowState.Normal;
                    preEnt.StartPosition = ap.FormStartPosition.CenterParent;
                    preEnt.ShowDialog();
                    break;
                case Menus.MENU_LIQUIDACIONES_ATLANTE_CATTIPOFOLDIN:
                    var ctfd = new menu2.Catalogos.fCatTipoFolDin();
                    ctfd.ShowDialog();
                    break;

                case Menus.MENU_LIQUIDACIONES_ATLANTE_CATCONCEPTOS:
                    var ccl = new menu2.Catalogos.fCatConceptosLiq();
                    ccl.ShowDialog();
                    break;
                case Menus.MENU_LIQUIDACIONES_ATLANTE_FOLIOSDINERO:
                    var fdin = new menu2.Procesos.fFoliosDinero(mainWindow.inicio._usuario);
                    fdin.ShowDialog();
                    break;
                case Menus.MENU_LIQUIDACIONES_ATLANTE_OTROSCONCEP:
                    var otcon = new menu2.Procesos.fOtroConceptos(mainWindow.inicio._usuario);
                    otcon.ShowDialog();
                    break;
                case Menus.MENU_LIQUIDACIONES_ATLANTE_COMPGASTOS:
                    //  var FolDinComp2 = new menu.fComprGastosFolioDinero(con, mainWindow.inicio._usuario.nombreUsuario,
                    //mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario,
                    //mainWindow.inicio._usuario.personal.nombre, mainWindow.inicio._usuario.personal.clave);
                    //  FolDinComp2.ShowDialog();
                    var compfoldin = new menu2.Procesos.fCompGastosFoliosDin(mainWindow.inicio._usuario);
                    compfoldin.ShowDialog();
                    break;
                case Menus.MENU_LIQUIDACIONES_ATLANTE_LIQUIDA:
                    var liqAtl = new menu2.Procesos.fLiquidacionesA(mainWindow.inicio._usuario);
                    liqAtl.ShowDialog();
                    break;
                case Menus.MENU_LIQUIDACIONES_ATLANTE_CATGASTOS:
                    var lcga = new menu2.Catalogos.fCatGastos();
                    lcga.ShowDialog();
                    break;
                case Menus.MENU_COMBUSTIBLE_COMP_VALES_DIESEL:
                    view = new ConciliadorValesCombustible();
                    break;
                case Menus.MENU_CATALOGOS_CLASF_RUTAS:
                    view = new editClasificacionesRutas();
                    break;
                case Menus.MENU_CATALOGOS_PAGO_X_MODALIDAD:
                    //view = new editPagoPorModalidad();
                    view = new editConfiClienteLiquidacion();
                    break;
                case Menus.MENU_LIQUIDACIONES_PEGASO:
                    view = new editLiquidacionesTotales();
                    break;
                case Menus.MENU_LIQUIDACIONES_SUELDOS_FIJOS:
                    view = new editSuedosFijos();
                    break;
                case Menus.MENU_FACTURACION_OS:
                    var facOS = new menu2.Procesos.fPorFacturarOS(mainWindow.inicio._usuario);
                    facOS.WindowState = ap.FormWindowState.Normal;
                    facOS.StartPosition = ap.FormStartPosition.CenterParent;
                    facOS.ShowDialog();


                    break;
                case Menus.MENU_SERVICIOS_COMP_REFACCIONES:
                    view = new ComplementoRefacciones();
                    break;
                case Menus.MENU_CATALOGOS_ACTIVIDADES:
                    //view = new editActividades();
                    view = new catActividades();
                    break;
                case Menus.MENU_SERVICIOS_LLANTAS_FINALIZA:
                    var FinLlantasNw = new menu2.Procesos.fOrdenServicioTaller(menu2.Clases.Inicio.eEstatusOS.TER,
                menu2.Clases.Inicio.eTipoOrdenServicio.LLANTAS, mainWindow.inicio._usuario);
                    FinLlantasNw.StartPosition = ap.FormStartPosition.CenterParent;
                    FinLlantasNw.ShowDialog();
                    break;
                case Menus.MENU_SALIDAS_REG_CTR_UNIDADES:
                    view = new editTableroControlUnidades();
                    break;
                case Menus.MENU_REPORTES_TAB_CTR_UNIDADES:
                    view = new ReporteTableroControlUnidades();
                    break;
                case Menus.MENU_CATALOGOS_BASES: //GRCC 12/10/2018
                    view = new editCatBases();
                    break;
                case Menus.MENU_CATALOGOS_ORIGENES: //GRCC 11/10/2018
                    view = new editOrigenesAtlante();
                    break;
                case Menus.MENU_CATALOGOS_RUTAS_ATL: //GRCC 11/10/2018
                    view = new editCatalogoRutasAtlante();
                    break;
                case Menus.MENU_CATALOGOS_TRAMOS:
                    view = new editTramos();
                    break;
                case Menus.MENU_CATALOGOS_DESTINOS: //GRCC 12/10/2018
                    view = new editDestinosAtlante();
                    break;
                case Menus.MENU_INVENTARIOS_MES_CONTABLE:
                    view = new editMesContable();
                    break;
                case Menus.MENU_INVENTARIOS_NIVELES_TANQUE:
                    view = new editAbrirNivelesTanque();
                    break;
                case Menus.MENU_INVENTARIOS_AJUSTE_INVEN_DIESEL:
                    view = new editAjusteInventarioDiesel();
                    break;
                case Menus.MENU_SERVICIOS_OSTES:
                    view = new editOrdenesServicioExterno();
                    break;
                case Menus.MENU_REPORTES_REP_MTTO:
                    view = new ReporteMantenimiento();
                    break;
                case Menus.MENU_INVENTARIOS_SUMINISTRO_COMB:
                    view = new editSuministrosCombustible();
                    break;
                case Menus.MENU_REPORTES_KARDEX_DIESEL:
                    view = new reporteKardexCombustible();
                    break;
                case Menus.MENU_SERVICIOS_SALIDAS_ALMACEN:
                    view = new editSalidasAlmacen();
                    break;
                case Menus.MENU_SERVICIOS_PRE_OC:
                    view = new editPreOrdenesCompra();
                    //view = new reporteParadas();
                    //view = new editCotizacionesDirectas();
                    break;
                case Menus.MENU_FACTURACION_DIR:
                    var FactDirecta = new menu2.Procesos.fFacturaDirecta(mainWindow.inicio._usuario);
                    FactDirecta.StartPosition = ap.FormStartPosition.CenterParent;
                    FactDirecta.ShowDialog();
                    break;
                case Menus.MENU_FACTURACION_CAN:
                    var FactCancela = new menu2.Procesos.fCancelaFactura(mainWindow.inicio._usuario);
                    FactCancela.StartPosition = ap.FormStartPosition.CenterParent;
                    FactCancela.ShowDialog();
                    break;
                case Menus.MENU_SERVICIOS_DEVINSUMOS:
                    var DevIns = new menu2.Procesos.fDevolucionInsumoOS(mainWindow.inicio._usuario);
                    DevIns.StartPosition = ap.FormStartPosition.CenterParent;
                    DevIns.ShowDialog();
                    break;
                //////////////////////////////////////////////////Recuperacion de info////////////////////////////////////////////////////////////////////////////////////////

                case Menus.MENU_SERVICIOS_PRE_OC_DIRECTAS: //GRCC 10/10/2018
                    view = new editPreOCDirectas();
                    break;
                case Menus.MENU_REPORTES_DESPACHOS:
                    view = new reporteDespachosCombustible(); //GRCC
                    break;
                case Menus.MENU_REPORTES_REQUISICION_COMPRA: //GRCC
                    view = new reporteRequisicionesCompra();
                    break;
                case Menus.MENU_LLANTERA_REVISION_LLANTAS: //GRCC
                    view = new EditRevisionLlantas();
                    break;
                case Menus.MENU_LLANTERA_ADMIN_CAT_MARCAS: //GRCC
                    view = new catMarcasLlantas();
                    break;
                case Menus.MENU_LLANTERA_ADMIN_CAT_DISEÑOS: //GRCC
                    view = new catDiseñoLlantas();
                    break;
                case Menus.MENU_LLANTERA_ADMIN_CAT_MEDIDA: //GRCC
                    view = new catMedidasLlantas();
                    break;
                case Menus.MENU_LLANTERA_ADMIN_CONDICION_LLANTA: //GRCC
                    view = new catCondicionLlanta();
                    break;
                case Menus.MENU_LLANTERA_ADMIN_CLASS_LLANTAS: //GRCC
                    view = new catClasProductoLlanta();
                    break;
                case Menus.MENU_REPORTES_INV_LLANTAS:
                    view = new reporteinventarioLlantas(); //GRCC 09/10/2018
                    break;
                case Menus.MENU_REPORTE_SUMINISTROS_DIESEL:
                    view = new reporteSuministrosCombustible(); //GRCC 09/10/2018
                    break;
                case Menus.MENU_REPORTE_EXISTENCIAS_COMBUSTIBLE:
                    view = new reporteExistenciasCombustible(); //GRCC 09/10/2018
                    break;
                case Menus.MENU_COMBUSTIBLE_AJUSTE_VALE_COMBUSTIBLE: // GRCC 11/10/2018
                    view = new editAjusteValeDiesel();
                    break;
                case Menus.MENU_REPORTE_PNA_TE: //por recuperar //GRCC 11/10/2018
                    view = new reporteParadas();
                    break;
                case Menus.MENU_COMBUSTIBLE_CAT_TIPOS_COM_EXT:
                    view = new editCatTipoCombustibleExtra(); //GRCC 09/10/2018
                    break;
                case Menus.MENU_CATALOGOS_CLASIFICACION_PRODUCTOS: //GRCC 09/10/2018
                    view = new editClasificacionProductos();
                    break;
                case Menus.MENU_CATALOGOS_GEOCERCAS: //GRCC 09/10/2018
                    view = new catGeoCercas();
                    break;
                case Menus.MENU_CATALOGOS_CUENTAS_CSI: //GRCC 09/10/2018
                    view = new catUsuariosCsi();
                    break;
                case Menus.MENU_REPORTES_VIAJES_V2:
                    view = new ReporteCartaPortesV2();
                    break;
                case Menus.MENU_REPORTES_MOVIMIENTOS_LLANTAS: //GRCC 09/10/2018 /pendiente por probar
                    view = new ReporteMovimientoLlantas();
                    break;
                case Menus.MENU_REPORTES_PRECIOS_COMBUSTIBLE: //GRCC
                    return new ReportePreciosPorDespachos();

                case Menus.MENU_PEGASO_CATALOGOS_PRODUCTOS:
                    return new editProductos();

                case Menus.MENU_PEGASO_CATALOGOS_CLASIFICACION_RUTAS:
                    return new editClasificacionesRutas();

                case Menus.MENU_PEGASO_CATALOGOS_PAGO_POR_MODALIDAD:
                    return new editPagoPorModalidad();

                case Menus.MENU_PEGASO_CATALOGOS_SUELDOS_FIJOS_OPERADORES:
                    return new editSuedosFijos();

                case Menus.MENU_PEGASO_CATALOGOS_TIPOS_COMBUSTIBLE_EXTRA:
                    view = new editCatTipoCombustibleExtra();
                    break;
                case Menus.MENU_PEGASO_MODULOS_SOLICITUD_ORD_SERV:
                    new menu2.Procesos.fSolicitudOS(mainWindow.inicio._usuario) { StartPosition = ap.FormStartPosition.CenterParent }.ShowDialog();
                    return view;
                case Menus.MENU_PEGASO_MODULOS_CARGA_COMBUSTIBLE_EXTRA:
                    return new editCombustibleExtra();
                case Menus.MENU_PEGASO_MODULOS_REIMPRESION_DE_DOCUMENTOS:
                    return new reimprecionDocumentos();
                case Menus.MENU_PEGASO_MODULOS_CONTROL_MANTENIMIENTO: //GRCC 10/10/2018
                    return new servicioTaller();
                case Menus.MENU_PEGASO_MODULOS_OSTES_EXTERNOS:
                    return new editOrdenesServicioExterno();
                case Menus.MENU_PEGASO_REPORTES_CARTA_PORTES:
                    return new exportarExcel();
                case Menus.MENU_PEGASO_REPORTES_BITACORA_CAMBIO_PRECIOS:
                    return new ReporteBitacoraPrecios();
                case Menus.MENU_PEGASO_REPORTES_BITACORA_ENT_SAL:
                    return new ReporteEntradasSalidasPatios();
                case Menus.MENU_PEGASO_REPORTES_CARGAS_COMBUSTIBLE:
                    return new ReporteBitacoraCargaCombustible(); //GRCC 10/10/2018
                case Menus.MENU_PEGASO_REPORTES_KARDEX_MOVIMIENTOS_UNI_TRANS:
                    return new ReporteHistorialUnidad();
                case Menus.MENU_PEGASO_REPORTES_CONSUMO_UNIDADES: //GRCC 12/10/2018
                    return new reporteConsumoUnidades();
                case Menus.MENU_PEGASO_MODULOS_PANEL_CONTROL_UNIDADES:
                    return new editTableroControlUnidades();
                case Menus.MENU_ATLANTE_CATALOGOS_CLIENTES:
                    new menu.frmCatClientes(connectionString, mainWindow.inicio._usuario.nombreUsuario, mainWindow.inicio._usuario.personal.empresa.clave, mainWindow.inicio._usuario.idUsuario).ShowDialog();
                    return view;
                case Menus.MENU_ATLANTE_REPORTES_REPORTEADOR_VIAJES:
                    return new ReporteCartaPortesV2();
                case Menus.ADMINISTRACION_CONTROL_FLOTAS:
                    return new AsignacionFlota();
                case Menus.MENU_COMBUSTIBLE_DESPACHO_COMBUSTIBLE:
                    return new DespachoDiesel();
                case Menus.MENUS_REPORTES_CIERRE_TURNO:
                    return new reporteCierreTurnos();
                case Menus.MENE_SERVICIOS_SOLICITUD_ORDEN_SERVICIO_V2:
                    return new ProcesosOS.EditSolicitudOrdenServicio();
                case Menus.MENU_REPORTES_ESTATUS_UNIDADES:
                    return new ReporteEstatusUnidades(); //
                case Menus.MENU_SERVICIOS_PROCESAMIENTO_DE_OS:
                    return new ProcesosOS.ProcesamientoOrdenServicio();
                case Menus.MENU_CATALOGOS_PROVEEDORES:
                    return new catProveedores();
                case Menus.MENU_CATALOGOS_PERSONAL_V2:
                    return new catalogoPersonal();
                case Menus.MENU_CATALOGOS_TIPO_UNIDAD_V2:
                    return new catTipoUnidades();
                case Menus.MENU_CATALOGOS_UNIDADES_TRANSPORTE_V2:
                    return new catUnidadesTransporte();
                case Menus.MENU_SERVICIOS_ACTIVIDAD_PENDIENTES_UNIDADES:
                    return new editActividadPendienteUnidad();
                case Menus.MENU_REPORTES_ACTIVIDAD_PENDIENTES_UNIDADES:
                    return new reporteActividadesPendientesUnidades();
                case Menus.MENU_CATALOGOS_MOTIVOS_CANCELACION:
                    return new catMotivosCancelacion();
                case Menus.MENU_CATALOGOS_CONFIG_ENVIO_CORREOS:
                    return new editConfiguracionEnvioCorreos();
                case Menus.MENU_FACTURACION_FACTURACION_V2:
                    return new editFacturacionV2();
                case Menus.MENU_REPORTES_REPORTE_ORDENES_SERVICIO:
                    return new reporteOrdenesServicio();
                case Menus.MENU_SERVICIOS_INSERSION_KM:
                    return new editInsersionKM();
                case Menus.MENU_REPORTES_SEMAFORO_MTTO:
                    return new reporteSemaforoMantenimiento();
                case Menus.MENU_CATALOGOS_CASETAS:
                    return new editCasetas();
                case Menus.ADMINISTRACION_CONTROL_FLOTAS_ATL:
                    return new AsignacionFlotaAtlante();
                case Menus.MENU_CATALOGOS_UBICACIONES:
                    return new editUbicaciones();
                case Menus.MENU_CATALOGOS_ORGANIGRAMA:
                    return new editOrganigrama();
                case Menus.MENU_CATALOGOS_CLIENTES_V2:
                    return new catClientesV2();
                case Menus.MENU_REPORTES_INSESION_KM:                    
                    return new reporteInsersionKm();
                case Menus.MENU_CATALOGOS_MODELOS:                    
                    return new catModelos();
                case Menus.MENU_COMBUSTIBLE_TIKECTS_PROVEEDOR:
                    return new editLigarTicketFacturaProveedor();
                case Menus.MENU_REPORTES_REPORTE_COMBUSTIBLE:                    
                    return new reporteCombustible();
                case Menus.MENU_REPORTES_REPORTE_CONCILIACION_TICKETS_DIESEL:                    
                    return new reporteConciliacionTicketsDiesel();
                case Menus.MENU_COMBUSTIBLE_CARGA_TICKETS_DIESEL:
                    return new editCargaTicketsDiesel();
                case Menus.MENU_COMBUSTIBLE_CONCILIACION_DIESEL:                    
                    return new editCierreProcesoCombustible();
                case Menus.MENU_REPORTES_REPORTE_ULTIMOS_KM:
                    return new reporteUltimosKm();
                case Menus.MENU_CATALOGOS_RUTAS_DESTINO_GLOBAL:
                    return new editRutasDestinoGlobal();
                case Menus.MENU_SALIDAS_VIAJES_GLOBAL:
                    return new editViajesGlobal();
                case Menus.MENU_SALIDAS_CIERRE_PROCESO_VIAJES:
                    return new editCierreProcesosViajes();
                case Menus.MENU_COMBUSTIBLE_INTEGRACION_VALES_COMBUSTIBLE:
                    return new editIntegracionValesCombustible();
                case Menus.MENU_LIQUIDACIONES_LIQUIDACIONES:                    
                    return new editLiquidaciones();
                case Menus.MENU_LIQUIDACIONES_PAGOS_APOYO:
                    return new editPagosApoyo();
                case Menus.MENU_SERVICIOS_CITAS_MTTO:                    
                    return new editCitasMantenimiento();
                case Menus.MENU_REPORTES_CITAS_MTTO:
                    return new reporteCitasMatto();
                case Menus.MENU_REPORTES_KARDEX_MTTO_PREV:
                    return new reporteKardexMttoPrev();
                case Menus.MENU_CATALOGOS_SINC_INTELISIS:
                    return new SincronizadorINTELISIS();
                case Menus.MENU_CATALOGOS_PRESUPUESTOS:
                    return new editPresupuestos();
                case Menus.MENU_REPORTES_INDICADORES_PRESUPUESTOS:
                    return new reporteIndicadores();
                case Menus.MENU_CATALOGOS_TIPOS_GRANJA:                    
                    return new editTipoGranja();
                case Menus.MENU_EVALUACIONES_EVALUACIONES_DESEMPEÑO:
                    return new editEvaluacionDesempeño();
                default:
                    return null;
            }
            return view;
        }
    }
}
