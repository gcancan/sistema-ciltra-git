﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for controlMarcas.xaml
    /// </summary>
    public partial class controlMarcas : UserControl
    {
        public controlMarcas()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            cbxMarcas.Width = Width - 4;
        }

        public void cargarControl(string medida)
        {
            var resp = new LlantaSvc().getProductoLlantasByMedida(medida);
            if (resp.typeResult == ResultTypes.success)
            {
                cbxMarcas.ItemsSource = (List<LLantasProducto>)resp.result;
            }
        }

        public LLantasProducto productoLlanta
        {
            get
            {
                if (cbxMarcas.SelectedItem == null)
                    return null;
                else
                    return (LLantasProducto)cbxMarcas.SelectedItem;
            }
            set
            {
                LLantasProducto producto = value;
                int i = 0;
                if (producto == null)
                {
                    cbxMarcas.SelectedItem = null;
                }
                else
                {
                    foreach (LLantasProducto item in cbxMarcas.Items)
                    {
                        if (producto.idLLantasProducto == item.idLLantasProducto)
                        {
                            cbxMarcas.SelectedIndex = i;
                            break;
                        }
                        i++;
                    }
                }
                
            }
            
        }
    }
}
