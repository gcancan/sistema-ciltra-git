﻿using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para catDiseñoLlantas.xaml
    /// </summary>
    public partial class catDiseñoLlantas : ViewBase
    {
        public catDiseñoLlantas()
        {
            InitializeComponent();
        }
        public override void buscar()
        {
            Diseño diseño = new selectDiseñoLlantas().getDiseñoLlanta();
            if (diseño != null)
            {
                base.buscar();
                this.mapForm(diseño);
                base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.buscar | ToolbarCommands.editar | ToolbarCommands.nuevo);
            }
        }

        public bool cargarCombos()
        {
            bool flag2;
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult resp = new MedidaLlantaSvc().getMedidasLlantas();
                if (resp.typeResult == ResultTypes.success)
                {
                    this.cbxMedida.ItemsSource = resp.result as List<MedidaLlanta>;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    return false;
                }
                OperationResult result2 = new LlantaSvc().getTiposLlantas();
                if (result2.typeResult == ResultTypes.success)
                {
                    this.cbxTipoLlanta.ItemsSource = result2.result as List<TipoLlanta>;
                }
                else
                {
                    ImprimirMensaje.imprimir(result2);
                    return false;
                }
                flag2 = true;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                flag2 = false;
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return flag2;
        }

        public override OperationResult guardar()
        {
            OperationResult result2;
            try
            {
                base.Cursor = Cursors.Wait;
                if (string.IsNullOrEmpty(this.txtNombreMedida.Text.Trim()))
                {
                    return new OperationResult(ResultTypes.warning, "SE NECESITA EL DISE\x00d1O", null);
                }
                if (this.cbxMedida.SelectedItem == null)
                {
                    return new OperationResult(ResultTypes.warning, "SELECCIONAR UNA MEDIDA", null);
                }
                if (this.ctrProfundidad.valor <= 0)
                {
                    return new OperationResult(ResultTypes.warning, "LA PROFUNDIDAD NO PUEDE SER MENOR O IGUAL A 0", null);
                }
                if (this.cbxTipoLlanta.SelectedItem == null)
                {
                    return new OperationResult(ResultTypes.warning, "SELECCIONAR EL TIPO DE LLANTA", null);
                }
                Diseño diseño = this.mapForm();
                if (diseño == null)
                {
                    return new OperationResult(ResultTypes.warning, "Ocurrio un error al leer la informaci\x00f3n de la pantalla", null);
                }
                OperationResult result = new DiseñosLlantaSvc().saveDiseñoLlanta(ref diseño);
                if (result.typeResult == ResultTypes.success)
                {
                    this.mapForm(diseño);
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.buscar | ToolbarCommands.editar | ToolbarCommands.nuevo);
                }
                result2 = result;
            }
            catch (Exception exception)
            {
                result2 = new OperationResult(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return result2;
        }
        public Diseño mapForm()
        {
            try
            {
                return new Diseño
                {
                    idDiseño = (this.ctrClave.Tag == null) ? 0 : (this.ctrClave.Tag as Diseño).idDiseño,
                    descripcion = this.txtNombreMedida.Text.Trim(),
                    medida = this.cbxMedida.SelectedItem as MedidaLlanta,
                    profundidad = this.ctrProfundidad.valor,
                    tipoLlanta = this.cbxTipoLlanta.SelectedItem as TipoLlanta
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void mapForm(Diseño diseño)
        {
            try
            {
                if (diseño != null)
                {
                    this.ctrClave.Tag = diseño;
                    this.ctrClave.valor = diseño.idDiseño;
                    this.txtNombreMedida.Text = diseño.descripcion;
                    this.cbxMedida.SelectedItem = (this.cbxMedida.ItemsSource as List<MedidaLlanta>).Find(delegate (MedidaLlanta s) {
                        int? nullable = (diseño.medida == null) ? null : new int?(diseño.medida.idMedidaLlanta);
                        return (s.idMedidaLlanta == nullable.GetValueOrDefault()) ? nullable.HasValue : false;
                    });
                    this.cbxTipoLlanta.SelectedItem = (this.cbxTipoLlanta.ItemsSource as List<TipoLlanta>).Find(delegate (TipoLlanta s) {
                        int? nullable = (diseño.tipoLlanta == null) ? null : new int?(diseño.tipoLlanta.clave);
                        return (s.clave == nullable.GetValueOrDefault()) ? nullable.HasValue : false;
                    });
                    this.ctrProfundidad.valor = diseño.profundidad;
                }
                else
                {
                    this.ctrClave.Tag = null;
                    this.ctrClave.limpiar();
                    this.txtNombreMedida.Clear();
                    this.cbxMedida.SelectedItem = null;
                    this.ctrProfundidad.limpiar();
                    this.cbxTipoLlanta.SelectedItem = null;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            this.mapForm(null);
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.cargarCombos())
            {
                this.nuevo();
            }
            else
            {
                base.IsEnabled = false;
            }
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult resp = new DiseñosLlantaSvc().getDiseñosLlanta();
                if (resp.typeResult == ResultTypes.success)
                {
                    var lista = resp.result as List<Diseño>;
                    new ReportView().exportarListaDiseños(lista);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
