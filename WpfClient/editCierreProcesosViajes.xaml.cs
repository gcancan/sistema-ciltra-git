﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editCierreProcesosViajes.xaml
    /// </summary>
    public partial class editCierreProcesosViajes : ViewBase
    {
        public editCierreProcesosViajes()
        {
            InitializeComponent();
        }
        Usuario usuario = null;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            usuario = mainWindow.usuario;

            this.dtpFechaInicio.SelectedDate = new DateTime?(DateTime.Now);
            this.dtpFechaFin.SelectedDate = new DateTime?(DateTime.Now);

            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresa.loaded(usuario);

            ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
            ctrZonaOperativa.cargarControl(usuario);
            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            limpiar();
            chcTodos.IsChecked = false;
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
        }
        void limpiar()
        {
            try
            {
                Cursor = Cursors.Wait;
                this.lvlCp.Items.Clear();
                lvlDetalles.ItemsSource = null;
                listaReporte = new List<ReporteCartaPorte>();
                listaReporteViaje = new List<ReporteCartaPorteViajes>();
                llenarResumen();
                listaViajesSeleccionados = new List<ReporteCartaPorteViajes>();
                listaCartaPortesSeleccionado = new List<ReporteCartaPorte>();
                llenarResumenSeleccionador();
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            limpiar();
            cargarListaOperaciones();
            cargarDestinos();
            if (ctrEmpresa.empresaSelected.clave == 2)
            {
                ctrZonaOperativa.Visibility = Visibility.Hidden;
            }
            else
            {
                ctrZonaOperativa.Visibility = Visibility.Visible;
            }

            stpClientes.Visibility = ctrEmpresa.empresaSelected.clave == 5 ? Visibility.Hidden : Visibility.Visible;

        }
        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            limpiar();
            if (ctrEmpresa.empresaSelected == null) return;

            if (ctrEmpresa.empresaSelected.clave == 1)
            {
                cargarListaOperaciones();
            }

        }
        void cargarListaOperaciones()
        {
            try
            {
                if (ctrZonaOperativa.zonaOperativa == null) return;
                Cursor = Cursors.Wait;
                cbxOperacion.ItemsSource = null;

                if (ctrEmpresa.empresaSelected.clave == 1)
                {
                    if (ctrZonaOperativa.zonaOperativa != null)
                    {
                        var resp = new ClienteSvc().getClientesByZonasOperativas(new List<string> { (ctrZonaOperativa.zonaOperativa.idZonaOperativa.ToString()) }, ctrEmpresa.empresaSelected.clave);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            var listaOperaciones = resp.result as List<OperacionFletera>;
                            listaOperaciones = listaOperaciones.OrderBy(s => s.cliente.nombre).ToList();
                            cbxOperacion.ItemsSource = listaOperaciones.Select(s => s.cliente).ToList();
                            cbxOperacion.SelectedItem = cbxOperacion.Items.Cast<Cliente>().ToList().Find(s => s.clave == usuario.cliente.clave);

                            if (cbxOperacion.SelectedItem == null)
                            {
                                cbxOperacion.SelectedIndex = 0;
                            }
                        }
                        else if (resp.typeResult != ResultTypes.recordNotFound)
                        {
                            imprimir(resp);
                        }
                    }
                }
                else
                {
                    var respCli = new ClienteSvc().getClientesALLorById(ctrEmpresa.empresaSelected.clave, 0, true);
                    if (respCli.typeResult == ResultTypes.success)
                    {
                        List<Cliente> listaClientes = respCli.result as List<Cliente>;
                        cbxOperacion.ItemsSource = listaClientes;
                        cbxOperacion.ItemsSource = listaClientes.OrderBy(s => s.nombre).ToList();
                        cbxOperacion.SelectedItem = cbxOperacion.Items.Cast<Cliente>().ToList().Find(s => s.clave == usuario.cliente.clave);

                        if (cbxOperacion.SelectedItem == null)
                        {
                            cbxOperacion.SelectedIndex = 0;
                        }
                    }
                    else if (respCli.typeResult != ResultTypes.recordNotFound)
                    {
                        imprimir(respCli);
                    }
                }


            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private List<Zona> listaZonas = new List<Zona>();
        void cargarDestinos()
        {
            OperationResult resp = new ZonasSvc().getDestinosByIdEmpresa(this.ctrEmpresa.empresaSelected.clave);
            if (resp.typeResult == ResultTypes.success)
            {
                this.listaZonas = resp.result as List<Zona>;
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
                base.IsEnabled = false;
                this.listaZonas = new List<Zona>();
            }
        }
        private async void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            chcTodos.IsChecked = false;
            await Task.Run(async () => await this.buscarRegistros());
        }
        private async Task buscarRegistros()
        {
            await Task.Run(async () => await this.buscarCartasPortes());
            await Task.Run(async () => await this.llenarLista());
        }

        private List<ReporteCartaPorte> listaReporte = new List<ReporteCartaPorte>();
        private List<ReporteCartaPorteViajes> listaReporteViaje = new List<ReporteCartaPorteViajes>();

        private DateTime? fechaInicio = null;
        private DateTime? fechaFin = null;
        public async Task buscarCartasPortes(bool actualizar = false)
        {
            try
            {
                Dispatcher.Invoke(() =>
                {
                    popMensaje.IsOpen = true;
                    lblMensaje.Content = !actualizar ? "BUSCANDO REGISTROS..." : "ACTUALIZANDO REGISTROS...";
                    this.lvlCp.Items.Clear();
                    lvlDetalles.ItemsSource = null;
                    btnBuscar.IsEnabled = false;
                });

                this.listaReporte = new List<ReporteCartaPorte>();
                listaReporteViaje = new List<ReporteCartaPorteViajes>();

                //base.Cursor = Cursors.Wait;
                int idEmpresa = 0;
                List<string> listaTractores = new List<string>();
                List<string> listaOperadores = new List<string>();
                List<string> listaRutas = new List<string>();
                List<string> listaClientes = new List<string>();
                List<string> listaModalidades = new List<string>();
                List<string> listaEstatus = new List<string>();
                bool todosOperadores = false;
                bool todosTractores = false;
                Dispatcher.Invoke(() =>
                {
                    idEmpresa = ctrEmpresa.empresaSelected.clave;
                    this.fechaInicio = new DateTime?(Convert.ToDateTime($"{this.dtpFechaInicio.SelectedDate.Value.Day}/{this.dtpFechaInicio.SelectedDate.Value.Month}/{this.dtpFechaInicio.SelectedDate.Value.Year} {this.horaInicio.Value}:{this.minutoInicio.Value}:00"));
                    this.fechaFin = new DateTime?(Convert.ToDateTime($"{this.dtpFechaFin.SelectedDate.Value.Day}/{this.dtpFechaFin.SelectedDate.Value.Month}/{this.dtpFechaFin.SelectedDate.Value.Year} {this.horaFin.Value}:{this.minutoFin.Value}:59"));
                    listaTractores = new List<string>(); // (from s in this.cbxTractores.SelectedItems.Cast<UnidadTransporte>().ToList<UnidadTransporte>() select s.clave).ToList<string>();
                    listaOperadores = new List<string>(); // (from s in this.cbxOperadores.SelectedItems.Cast<Personal>().ToList<Personal>() select s.clave.ToString()).ToList<string>();

                    if (ctrEmpresa.empresaSelected.clave == 5)
                    {
                        listaClientes = cbxOperacion.ItemsSource.Cast<Cliente>().ToList().Select(s => s.clave.ToString()).ToList();
                    }
                    else
                    {
                        listaClientes = new List<string> { (cbxOperacion.SelectedItem as Cliente).clave.ToString() }; // (from s in this.cbxClientes.SelectedItems.Cast<Cliente>().ToList<Cliente>() select s.clave.ToString()).ToList<string>();    
                    }

                    listaRutas = this.listaZonas.Select(s => s.clave.ToString()).ToList();
                    listaModalidades = new List<string> { "SENCILLO", "FULL" };// (from s in this.cbxModalidad.SelectedItems.Cast<Modalidad>().ToList<Modalidad>() select s.nombre.ToString()).ToList<string>();
                    listaEstatus = new List<string> { "FINALIZADO" };// this.cbxEstatus.SelectedItems.Cast<string>().ToList<string>();
                    todosOperadores = true;// chcTodosOperadores.IsChecked.Value;
                    todosTractores = true;// chcTodosTractores.IsChecked.Value;
                });
                OperationResult resp = new ReporteCartaPorteSvc().getReporteCartaPorteV3(idEmpresa, fechaInicio.Value, fechaFin.Value, listaTractores, listaOperadores,
                    listaClientes, listaRutas, listaModalidades, listaEstatus, todosOperadores, todosTractores);
                if (resp.typeResult == ResultTypes.success)
                {
                    this.listaReporte = resp.result as List<ReporteCartaPorte>;

                    listaReporte = listaReporte.OrderBy(s => s.fechaInicio).ToList();

                    List<GrupoRuta> listaGrupos = (from v in listaReporte
                                                   group v by v.cartaPorte into grupo
                                                   select new GrupoRuta
                                                   {
                                                       folio = (from s in grupo select s.folio).First(),
                                                       cartaPorte = (from s in grupo select s.cartaPorte).First(),
                                                       idOrigen = (from s in grupo select s.idOrigen).First(),
                                                       idDestino = (from s in grupo select s.idDestino).First(),
                                                       kmOrigen = (from s in grupo select s.kmOrigen).First(),
                                                       kmOrigenDestino = (from s in grupo select s.kmOrigenDestino).First(),
                                                       kmDestino = (from s in grupo select s.kmDestino).First(),
                                                       //km = (from s in grupo select s.km).First()
                                                   }).ToList();



                    foreach (ReporteCartaPorte reporteCp in listaReporte)
                    {
                        reporteCp.listaGrupo = new List<GrupoRuta>();
                        reporteCp.listaGrupo = listaGrupos.FindAll(s => s.folio == reporteCp.folio);
                    }
                    foreach (ReporteCartaPorte reporteCp in listaReporte)
                    {
                        if (reporteCp.vale == 36517)
                        {

                        }
                        reporteCp.sumatoriaKm = listaReporte.FindAll(s => s.vale == reporteCp.vale).Sum(z => z.km);
                    }

                    //if (idEmpresa == 5)
                    //{
                    //    listaReporteViaje = (from v in this.listaReporte
                    //                                group v by new { v.folio, v.idCliente } into grupoViaje
                    //                                select new ReporteCartaPorteViajes
                    //                                {
                    //                                    lista = grupoViaje.ToList<ReporteCartaPorte>(),
                    //                                    fechaInicioCarga = (from s in grupoViaje select s.fechaInicioCarga).First(),
                    //                                    fechaInicio = (from s in grupoViaje select s.fechaInicio).First(),
                    //                                    fechaFin = (from s in grupoViaje select s.fechaFin).First(),
                    //                                    fechaPrograma = (from s in grupoViaje select s.fechaPrograma).First(),
                    //                                    isReEnviar = (from s in grupoViaje select s.isReEnviar).First(),
                    //                                    imputable = (from s in grupoViaje select s.imputable).First(),
                    //                                    isSeleccionado = false
                    //                                }).ToList();
                    //}
                    //else
                    //{
                        listaReporteViaje = (from v in this.listaReporte
                                             group v by v.folio into grupoViaje
                                             select new ReporteCartaPorteViajes
                                             {
                                                 lista = grupoViaje.ToList<ReporteCartaPorte>(),
                                                 fechaInicioCarga = (from s in grupoViaje select s.fechaInicioCarga).First(),
                                                 fechaInicio = (from s in grupoViaje select s.fechaInicio).First(),
                                                 fechaFin = (from s in grupoViaje select s.fechaFin).First(),
                                                 fechaPrograma = (from s in grupoViaje select s.fechaPrograma).First(),
                                                 isReEnviar = (from s in grupoViaje select s.isReEnviar).First(),
                                                 imputable = (from s in grupoViaje select s.imputable).First(),
                                                 isSeleccionado = false
                                             }).ToList();
                    //}
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    await Dispatcher.Invoke(async () =>
                    {
                        listaReporte = new List<ReporteCartaPorte>();
                        listaReporteViaje = new List<ReporteCartaPorteViajes>();
                        //await Task.Run(async () => await this.llenarLista());
                    });

                }
                else
                {
                    await Dispatcher.Invoke(async () =>
                    {
                        listaReporte = new List<ReporteCartaPorte>();
                        listaReporteViaje = new List<ReporteCartaPorteViajes>();
                        //await Task.Run(async () => await this.llenarLista());
                    });
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                //base.Cursor = Cursors.Arrow;
                Dispatcher.Invoke(() =>
                {
                    popMensaje.IsOpen = false;
                    btnBuscar.IsEnabled = true;
                });
                //await Dispatcher.Invoke(async () =>
                //{
                //    await Task.Run(async () => await this.llenarLista());
                //});
            }
        }
        private async Task llenarLista()
        {
            try
            {
                Dispatcher.Invoke(() =>
                {
                    popMensaje.IsOpen = true;
                    lblMensaje.Content = "MOSTRANDO RESULTADO...";
                    this.lvlCp.Items.Clear();
                    List<string> listaId = listaReporteViaje.Select(s => s.folio.ToString()).ToList();

                    var resp = new CartaPorteSvc().getListaComentariosViajes(listaId);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        List<ComentarioViaje> listaComentario = resp.result as List<ComentarioViaje>;
                        foreach (var item in listaReporteViaje)
                        {
                            List<ComentarioViaje> list = listaComentario.FindAll(s=> s.idViaje == item.folio);
                            if (list != null)
                            {
                                item.listaComentarios = list;
                            }
                            else
                            {
                                item.listaComentarios = new List<ComentarioViaje>();
                            }
                        }
                    }

                    foreach (var viaje in listaReporteViaje.OrderBy(s => s.fechaFin).ToList())
                    {
                        ListViewItem lvl = new ListViewItem { Content = viaje };
                        ContextMenu menu = new ContextMenu();
                        MenuItem menuItem = new MenuItem { Header = "AGREGAR OBSERVACIONES", Tag = viaje }; ;
                        menu.Items.Add(menuItem);
                        menuItem.Click += MenuItem_Click;
                        lvl.ContextMenu = menu;
                        lvl.Selected += Lvl_Selected;

                        ToolTip too = new ToolTip();
                        StackPanel stpContentToolTip = new StackPanel { Orientation = Orientation.Vertical };
                        foreach (var comentario in viaje.listaComentarios)
                        {
                            stpContentToolTip.Children.Add(new controlComentario(comentario.usuario, comentario.fecha, comentario.comentario));
                        }
                        too.Content = stpContentToolTip;
                        lvl.ToolTip = too;

                        this.lvlCp.Items.Add(lvl);
                    }
                    llenarResumen();
                });
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                Dispatcher.Invoke(() =>
                {
                    popMensaje.IsOpen = false;
                });
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            MenuItem menuItem = sender as MenuItem;
            ReporteCartaPorteViajes viaje = menuItem.Tag as ReporteCartaPorteViajes;
            new addComentarios().addComentario(mainWindow.usuario.nombreUsuario, viaje.folio);
            _ = llenarLista();
        }

        private void llenarResumen()
        {
            txtNoViajes.Text = listaReporteViaje.Count().ToString("N0");
            txtKm.Text = listaReporteViaje.Sum(s => s.km).ToString("N4");
            txtToneladas.Text = listaReporteViaje.Sum(s => s.toneladas).ToString("N4");
            txtDias.Text = diasEvaluados.ToString("N0");
        }
        DateTime? fechaInicial => listaReporteViaje.Count == 0 ? null : listaReporteViaje.Min(s => s.fechaFin);
        DateTime? fechaFinal => listaReporteViaje.Count == 0 ? null : listaReporteViaje.Max(s => s.fechaFin);
        int diasEvaluados =>
             listaReporteViaje.Count == 0 ? 0 : Convert.ToInt32((this.fechaFinal.Value - this.fechaInicial.Value).TotalDays);
        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                ListViewItem lvl = sender as ListViewItem;
                ReporteCartaPorteViajes reporte = lvl.Content as ReporteCartaPorteViajes;
                mapDetalle(reporte);
            }
            else
            {
                lvlDetalles.ItemsSource = null;
            }
        }

        private void mapDetalle(ReporteCartaPorteViajes reporte)
        {
            lvlDetalles.ItemsSource = listaReporte.FindAll(s => s.folio == reporte.folio);
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                CierreProcesoViajes cierreProcesoViajes = mapForm();
                if (cierreProcesoViajes.listaDetalles.Count == 0) return new OperationResult(ResultTypes.warning, "SIN REGISTROS PARA GUARDAR");

                //return new OperationResult(ResultTypes.error, "");

                var resp = new CierreProcesoViajeSvc().saveCierreProcesoViaje(ref cierreProcesoViajes, ctrEmpresa.empresaSelected.clave,
                    listaViajesSeleccionados, listaCartaPortesSeleccionado);
                if (resp.typeResult == ResultTypes.success)
                {
                    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
                    mainWindow.habilitar = true;
                    limpiarListaAsync();
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
                listaCartaPortesSeleccionado = new List<ReporteCartaPorte>();
                listaViajesSeleccionados = new List<ReporteCartaPorteViajes>();
                llenarResumenSeleccionador();
                chcTodos.IsChecked = false;
            }
        }
        private CierreProcesoViajes mapForm()
        {
            try
            {
                CierreProcesoViajes cierreProcesoViajes = new CierreProcesoViajes()
                {
                    idCierreProcesoViajes = 0,
                    idEmpresa = ctrEmpresa.empresaSelected.clave,
                    idZonaOperativa = ctrEmpresa.empresaSelected.clave == 2 ? null : (int?)ctrZonaOperativa.zonaOperativa.idZonaOperativa,
                    idCliente = cbxOperacion.SelectedItem == null ? null : (int?)(cbxOperacion.SelectedItem as Cliente).clave,
                    fecha = DateTime.Now,
                    dias = diasEvaluados,
                    km = Convert.ToDecimal(txtKmCheck.Text),
                    noViaje = Convert.ToInt32(txtNoViajesCheck.Text.Replace(",", string.Empty)),
                    usuario = this.usuario.nombreUsuario,
                    toneladas = Convert.ToDecimal(txtToneladasCheck.Text)
                };
                cierreProcesoViajes.listaDetalles = new List<DetalleCierreProcesoViajes>();

               

                foreach (var cp in listaCartaPortesSeleccionado)
                {
                    cierreProcesoViajes.listaDetalles.Add(new DetalleCierreProcesoViajes
                    {
                        idDetalleCierreProcesoViajes = 0,
                        idCierreProcesoViajes = cierreProcesoViajes.idCierreProcesoViajes,
                        numGuiaId = cp.folio,
                        consecutivo = cp.cartaPorte,
                        idOrigen = cp.idOrigen,
                        idDestino = cp.idDestino,
                        idProducto = cp.idProducto,
                        descargado = cp.toneladas,
                        km = cp.km
                    });
                }

                if (ctrEmpresa.empresaSelected.clave == 5)
                {
                    listaViajesSeleccionados = (from v in this.listaCartaPortesSeleccionado
                                         group v by new { v.folio, v.idCliente } into grupoViaje
                                         select new ReporteCartaPorteViajes
                                         {
                                             lista = grupoViaje.ToList<ReporteCartaPorte>(),
                                             fechaInicioCarga = (from s in grupoViaje select s.fechaInicioCarga).First(),
                                             fechaInicio = (from s in grupoViaje select s.fechaInicio).First(),
                                             fechaFin = (from s in grupoViaje select s.fechaFin).First(),
                                             fechaPrograma = (from s in grupoViaje select s.fechaPrograma).First(),
                                             isReEnviar = (from s in grupoViaje select s.isReEnviar).First(),
                                             imputable = (from s in grupoViaje select s.imputable).First(),
                                             isSeleccionado = false
                                         }).ToList();
                }

                return cierreProcesoViajes;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void CbxOperacion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            limpiar();
        }
        List<ReporteCartaPorteViajes> listaViajesSeleccionados = new List<ReporteCartaPorteViajes>();
        List<ReporteCartaPorte> listaCartaPortesSeleccionado = new List<ReporteCartaPorte>();
        List<ListViewItem> lvlSeleccionados = new List<ListViewItem>();
        private void ChcSeleccionado_Checked(object sender, RoutedEventArgs e)
        {
            if (todos) return;

            llenarResumenSeleccionador();
            return;
            listaViajesSeleccionados = new List<ReporteCartaPorteViajes>();
            listaCartaPortesSeleccionado = new List<ReporteCartaPorte>();

            CheckBox chc = sender as CheckBox;
            int folio = (int)chc.Tag;

            return;
            foreach (var itemListViewItem in lvlCp.Items.Cast<ListViewItem>().ToList())
            {
                ReporteCartaPorteViajes itemViaje = itemListViewItem.Content as ReporteCartaPorteViajes;
                if (itemViaje.isSeleccionado)
                {
                    lvlSeleccionados.Add(itemListViewItem);
                    listaViajesSeleccionados.Add(itemViaje);
                    foreach (var itemCp in listaReporte.FindAll(s => s.folio == itemViaje.folio))
                    {
                        listaCartaPortesSeleccionado.Add(itemCp);
                    }
                }
            }

        }
        private void llenarResumenSeleccionador()
        {
            listaViajesSeleccionados = new List<ReporteCartaPorteViajes>();
            listaCartaPortesSeleccionado = new List<ReporteCartaPorte>();

            lvlCp.Items.Refresh();
            listaViajesSeleccionados = lvlCp.Items.Cast<ListViewItem>().ToList().Select(s => s.Content as ReporteCartaPorteViajes).ToList().FindAll(s => s.isSeleccionado);

            foreach (var viaje in listaViajesSeleccionados)
            {
                listaCartaPortesSeleccionado.AddRange(listaReporte.FindAll(s => s.folio == viaje.folio));
            }

            txtNoViajesCheck.Text = listaViajesSeleccionados.Count().ToString("N0");
            txtKmCheck.Text = listaViajesSeleccionados.Sum(s => s.km).ToString("N4");
            txtToneladasCheck.Text = listaViajesSeleccionados.Sum(s => s.toneladas).ToString("N4");
        }
        public void limpiarListaAsync()
        {
            foreach (var item in listaViajesSeleccionados)
            {
                listaReporteViaje.Remove(item);
            }
            foreach (var item in listaCartaPortesSeleccionado)
            {
                listaReporte.Remove(item);
            }
            llenarLista();
        }
        bool todos = false;
        private void chcTodos_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                todos = true;
                Cursor = Cursors.Wait;
                foreach (var itemListViewItem in lvlCp.Items.Cast<ListViewItem>().ToList())
                {
                    ReporteCartaPorteViajes itemViaje = itemListViewItem.Content as ReporteCartaPorteViajes;
                    itemViaje.isSeleccionado = chcTodos.IsChecked.Value;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
                todos = false;
                llenarResumenSeleccionador();
            }
        }
    }
}
