﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editSuedosFijos.xaml
    /// </summary>
    public partial class editSuedosFijos : ViewBase
    {
        public editSuedosFijos()
        {
            InitializeComponent();
        }
        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            nuevo();
            ctrOperadores.loaded(eTipoBusquedaPersonal.OPERADORES, mainWindow.empresa);
        }

        public override OperationResult guardar()
        {
            try
            {
                var val = validar();
                if (val.typeResult != ResultTypes.success)
                {
                    return val;
                }
                SueldoFijo sueldo = mapForm();
                var resp = new SueldoFijoSvc().saveSueldoFijo(ref sueldo);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(sueldo);
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public override OperationResult eliminar()
        {
            try
            {
                SueldoFijo sueldo = mapForm();
                sueldo.estatus = false;
                var resp = new SueldoFijoSvc().saveSueldoFijo(ref sueldo);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(null);
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public override void buscar()
        {
            selectSueldoFijo select = new selectSueldoFijo();
            SueldoFijo resp = select.getAllSueldosFijos();
            mapForm(resp);

        }

        private void mapForm(SueldoFijo sueldo)
        {
            if (sueldo != null)
            {
                txtClave.Tag = sueldo;
                txtClave.Text = sueldo.idSueldoFijo.ToString();
                ctrOperadores.personal = sueldo.operador;
                ctrImporte.valor = sueldo.importe;
                mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.editar | ToolbarCommands.eliminar | ToolbarCommands.cerrar);
            }
            else
            {
                txtClave.Tag = null;
                txtClave.Clear();
                ctrOperadores.limpiar();
                ctrImporte.valor = 0;
                mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.buscar | ToolbarCommands.cerrar);
            }
        }

        private SueldoFijo mapForm()
        {
            try
            {
                return new SueldoFijo
                {
                    idSueldoFijo = txtClave.Tag == null ? 0 : (txtClave.Tag as SueldoFijo).idSueldoFijo,
                    operador = ctrOperadores.personal,
                    importe = ctrImporte.valor,
                    autoriza = mainWindow.inicio._usuario.nombreUsuario,
                    estatus = true
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        OperationResult validar()
        {
            try
            {
                OperationResult val = new OperationResult { valor = 0, mensaje = "PARA CONTINUAR SE REQUIERE" + Environment.NewLine };
                if (ctrOperadores.personal == null)
                {
                    val.valor = 2;
                    val.mensaje += "    * Elegir un Operador";
                }
                if (ctrImporte.valor <= 0)
                {
                    val.valor = 2;
                    val.mensaje += "    * Un Sueldo mayor a 0";
                }
                return val;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
