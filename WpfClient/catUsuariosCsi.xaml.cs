﻿using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para catUsuariosCsi.xaml
    /// </summary>
    public partial class catUsuariosCsi : ViewBase
    {
        public catUsuariosCsi()
        {
            InitializeComponent();
        }
        public bool cargarUsuarios()
        {
            bool flag2;
            try
            {
                this.lvlUsuarios.Items.Clear();
                base.Cursor = Cursors.Wait;
                OperationResult resp = new UsuarioCSISvc().getAllUsuariosCSI();
                if (resp.typeResult == ResultTypes.success)
                {
                    foreach (UsuarioCSI ocsi in resp.result as List<UsuarioCSI>)
                    {
                        ListViewItem newItem = new ListViewItem
                        {
                            Content = ocsi
                        };
                        newItem.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                        newItem.Selected += new RoutedEventHandler(this.Lvl_Selected);
                        this.lvlUsuarios.Items.Add(newItem);
                    }
                    return true;
                }
                if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    return true;
                }
                ImprimirMensaje.imprimir(resp);
                flag2 = false;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                flag2 = false;
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return flag2;
        }

        public override OperationResult guardar()
        {
            OperationResult result3;
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult result = this.validar();
                if (result.typeResult > ResultTypes.success)
                {
                    return result;
                }
                UsuarioCSI usuarioCSI = this.mapForm();
                OperationResult result2 = new UsuarioCSISvc().saveUsuarioCSI(ref usuarioCSI);
                if (result2.typeResult == ResultTypes.success)
                {
                    base.mainWindow.habilitar = true;
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
                    this.mapForm(usuarioCSI);
                    this.cargarUsuarios();
                }
                result3 = result2;
            }
            catch (Exception exception)
            {
                result3 = new OperationResult(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return result3;
        }
        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
            {
                ListViewItem item = sender as ListViewItem;
                this.mapForm(item.Content as UsuarioCSI);
            }
        }

        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                ListViewItem item = sender as ListViewItem;
                this.mapForm(item.Content as UsuarioCSI);
            }
        }

        private UsuarioCSI mapForm()
        {
            try
            {
                return new UsuarioCSI
                {
                    idUsuario = (this.ctrClave.Tag == null) ? 0 : (this.ctrClave.Tag as UsuarioCSI).idUsuario,
                    alias = this.txtNombre.Text,
                    nombreUsuario = this.txtNombreUsuario.Text,
                    contraseña = this.txtContraseña.Text
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void mapForm(UsuarioCSI usuarioCSI)
        {
            try
            {
                if (usuarioCSI != null)
                {
                    this.ctrClave.Tag = usuarioCSI;
                    this.ctrClave.valor = usuarioCSI.idUsuario;
                    this.txtNombre.Text = usuarioCSI.alias;
                    this.txtNombreUsuario.Text = usuarioCSI.nombreUsuario;
                    this.txtContraseña.Text = usuarioCSI.contraseña;
                }
                else
                {
                    this.ctrClave.Tag = null;
                    this.ctrClave.valor = 0;
                    this.txtNombre.Clear();
                    this.txtNombreUsuario.Clear();
                    this.txtContraseña.Clear();
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
            this.mapForm(null);
        }
        public OperationResult validar()
        {
            try
            {
                OperationResult result = new OperationResult
                {
                    valor = 0,
                    mensaje = "PARA PROCEDER SE REQUIERE" + Environment.NewLine
                };
                if (string.IsNullOrEmpty(this.txtNombre.Text.Trim()))
                {
                    result.valor = 2;
                    result.mensaje = result.mensaje + "  * Proporcionar el Alias de la cuenta" + Environment.NewLine;
                }
                if (string.IsNullOrEmpty(this.txtNombreUsuario.Text.Trim()))
                {
                    result.valor = 2;
                    result.mensaje = result.mensaje + "  * Proporcionar el Nombre de la cuenta" + Environment.NewLine;
                }
                if (string.IsNullOrEmpty(this.txtContraseña.Text.Trim()))
                {
                    result.valor = 2;
                    result.mensaje = result.mensaje + "  * Proporcionar la contrase\x00f1a de la cuenta" + Environment.NewLine;
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            if (!this.cargarUsuarios())
            {
                base.IsEnabled = false;
            }
            else
            {
                this.nuevo();
            }
        }
    }
}
