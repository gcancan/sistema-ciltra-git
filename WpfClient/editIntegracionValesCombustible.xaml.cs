﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editIntegracionValesCombustible.xaml
    /// </summary>
    public partial class editIntegracionValesCombustible : ViewBase
    {
        public editIntegracionValesCombustible()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ctrVale.txtEntero.KeyDown += TxtEntero_KeyDown;
            
            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);
        }
        private void TxtEntero_KeyDown(object sender, KeyEventArgs e)
        {
            if (sender!= null)
            {
                if (e.Key == Key.Enter && ctrVale.valor > 0)
                {
                    buscarCargaCombustible(ctrVale.valor);
                }
            }
        }

        private void buscarCargaCombustible(int vale)
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult resp = new OperacionSvc().cargaCombustibleByIdVale(vale);
                if (resp.typeResult == ResultTypes.success)
                {
                    CargaCombustible cargaCombustible = (CargaCombustible)resp.result;
                    this.mapForm(cargaCombustible);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void mapForm(CargaCombustible cargaCombustible)
        {
            try
            {
                lvlCargasExtra.Items.Clear();
                lvlTickets.Items.Clear();
                if (cargaCombustible != null)
                {
                    ctrVale.Tag = cargaCombustible;
                    ctrVale.valor = cargaCombustible.idCargaCombustible;
                    ctrVale.IsEnabled = false;
                    ctrLitros.valor = cargaCombustible.ltCombustible;
                    txtFecha.Text = cargaCombustible.fechaCombustible.Value.ToString("dd/MM/yy HH:mm");
                    ctrUnidad.unidadTransporte = cargaCombustible.unidadTransporte;
                    ctrOperador.personal = cargaCombustible.masterOrdServ.chofer;
                    ctrDespachadora.personal = cargaCombustible.despachador;

                    var respIdExtra = new OperacionSvc().getCargasExtraByVale(cargaCombustible.idCargaCombustible);
                    if (respIdExtra.typeResult == ResultTypes.success)
                    {
                        foreach (var item in respIdExtra.result as List<int>)
                        {
                            OperationResult resp = new CargaCombustibleSvc().getCargaCombustibleExtra(item);
                            if (resp.typeResult == ResultTypes.success)
                            {
                                agregarCargaExtra(resp.result as CargaCombustibleExtra);
                            }
                        }
                    }

                    foreach (var tck in cargaCombustible.listaCargaTickets)
                    {
                        agregarTicket(tck);
                    }
                    this.IsEnabled = false;
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar | ToolbarCommands.editar);
                }
                else
                {
                    ctrVale.Tag = null;
                    ctrVale.valor = 0;
                    ctrVale.IsEnabled = true;
                    ctrLitros.valor = 0;
                    txtFecha.Clear();
                    ctrUnidad.unidadTransporte = null;
                    ctrOperador.personal = null;
                    ctrDespachadora.personal = null;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                calcularSumaLitros();
            }
        }

        private void agregarCargaExtra(CargaCombustibleExtra cargaCombustibleExtra)
        {
            ListViewItem lvl = new ListViewItem();
            lvl.Content = cargaCombustibleExtra;
            lvlCargasExtra.Items.Add(lvl);

            calcularSumaLitros();
        }

        private void BtnQuitarCargaExtra_Click(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                Button lvl = sender as Button;
                int idCargaExtra = (int)lvl.Tag;
                
                var resp = new CargaCombustibleSvc().desLigarCargasExtra(idCargaExtra);
                if (resp.typeResult != ResultTypes.success)
                {
                    ImprimirMensaje.imprimir(resp);
                }
                else
                {
                    lvlCargasExtra.Items.Remove(lvlCargasExtra.Items.Cast<ListViewItem>().ToList().Find(s => (s.Content as CargaCombustibleExtra).idCargaCombustibleExtra == idCargaExtra));
                }
                calcularSumaLitros();
            }
        }
        private void BtnQuitarTicket_Click(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                Button lvl = sender as Button;
                int idClave = (int)lvl.Tag;

                List<CargaTicketDiesel> listaCargas = lvlTickets.Items.Cast<ListViewItem>().ToList().Select(s => s.Content as CargaTicketDiesel).ToList().FindAll(s => s.clave != idClave);

                var resp = new CargaCombustibleSvc().desLigarCargasTicket((ctrVale.Tag as CargaCombustible).idCargaCombustible, idClave, listaCargas);
                if (resp.typeResult != ResultTypes.success)
                {
                    imprimir(resp);
                }
                else
                {
                    lvlTickets.Items.Remove(lvlTickets.Items.Cast<ListViewItem>().ToList().Find(s => (s.Content as CargaTicketDiesel).clave == idClave));
                }
                
                calcularSumaLitros();
            }
        }
        private void calcularSumaLitros()
        {
            decimal sumaLitros = 0m;
            decimal subTotalExtras = 0;
            foreach (var cargaExtra in lvlCargasExtra.Items.Cast<ListViewItem>().ToList().Select(s=> s.Content as CargaCombustibleExtra))
            {
                subTotalExtras += cargaExtra.tipoOrdenCombustible.operativo ? (decimal)cargaExtra.ltCombustible : 0;
            }
            decimal subTotalTickets = 0;
            foreach (var tickets in lvlTickets.Items.Cast<ListViewItem>().ToList().Select(s=> s.Content as CargaTicketDiesel))
            {
                subTotalTickets += tickets.litros;
            }

            ctrSubTotalExtra.valor = subTotalExtras;
            ctrSubTotalTickets.valor = subTotalTickets;
            sumaLitros += subTotalExtras + subTotalTickets;


            ctrTotalLitros.valor = ctrLitros.valor + sumaLitros;
        }
        private void BtnBuscarCargasExtra_Click(object sender, RoutedEventArgs e)
        {
            if (ctrUnidad.unidadTransporte != null)
            {

                List<int> listaId = lvlCargasExtra.Items.Cast<ListViewItem>().ToList().Select(s => (s.Content as CargaCombustibleExtra).idCargaCombustibleExtra).ToList();
                CargaCombustibleExtra combustibleExtra = new selectCargasExtra().getCargaCombustibleExtraLibreByUnidad(ctrUnidad.unidadTransporte.clave, listaId);
                if (combustibleExtra != null)
                {
                    agregarCargaExtra(combustibleExtra);
                }
            }
        }
        private void BtnBuscarTickets_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ctrUnidad.unidadTransporte != null)
                {
                    List<int> listaId = lvlTickets.Items.Cast<ListViewItem>().ToList().Select(s => (s.Content as CargaTicketDiesel).clave).ToList();
                    CargaTicketDiesel cargaTicket = new selectCargaTicketsDiesel().seleccionarCargaDiselByUnidad(ctrUnidad.unidadTransporte.clave, listaId);
                    if (cargaTicket != null)
                    {
                        agregarTicket(cargaTicket);
                    }
                }                    
            }
            catch (Exception ex)
            {

            }
        }
        private void agregarTicket(CargaTicketDiesel ticket)
        {
            ListViewItem lvl = new ListViewItem();
            lvl.Content = ticket;
            lvlTickets.Items.Add(lvl);

            calcularSumaLitros();
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;

                if (ctrVale.Tag == null)
                {
                    return new OperationResult(ResultTypes.warning, "SIN NADA PARA GUARDAR");
                }
                List<CargaCombustibleExtra> listaCargaExtra = new List<CargaCombustibleExtra>();
                List<CargaTicketDiesel> listaCargaTicket = new List<CargaTicketDiesel>();
                listaCargaExtra = lvlCargasExtra.Items.Cast<ListViewItem>().ToList().Select(s => s.Content as CargaCombustibleExtra).ToList();
                listaCargaTicket = lvlTickets.Items.Cast<ListViewItem>().ToList().Select(s => s.Content as CargaTicketDiesel).ToList();

                var resp = new CargaCombustibleSvc().integracionValesCombustible((ctrVale.Tag as CargaCombustible).idCargaCombustible, listaCargaExtra, listaCargaTicket);
                if (resp.typeResult == ResultTypes.success)
                {
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.editar| ToolbarCommands.cerrar);
                }
                
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public override void buscar()
        {
            ReporteCargasDiesel reporte = new selectCargaCombustible(mainWindow.usuario).getCargaCombustible();
            if (reporte != null)
            {
                buscarCargaCombustible(reporte.vale);
                
            }
        }
    }
}
