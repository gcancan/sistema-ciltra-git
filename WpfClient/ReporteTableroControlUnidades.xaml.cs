﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para ReporteTableroControlUnidades.xaml
    /// </summary>
    public partial class ReporteTableroControlUnidades : ViewBase
    {
        public ReporteTableroControlUnidades()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo|ToolbarCommands.cerrar);
            buscarTablero();
        }
        private void buscarTablero()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new TableroControlUnidadesSvc().getTableroControlUnidad();
                if (resp.typeResult == ResultTypes.success)
                {
                    List<TableroControlUnidades> listaTablero = resp.result as List<TableroControlUnidades>;
                    DateTime fechaActual = DateTime.Now;
                    foreach (var item in listaTablero)
                    {
                        item.fechaActual = fechaActual;
                    }

                    mapTableroTrayetoGranja(listaTablero.FindAll(s => s.ubicacion == UbicacionTablero.TRAYECTO_GRANJA));
                    mapTableroTrayectoPlanta(listaTablero.FindAll(s => s.ubicacion == UbicacionTablero.RETORNO_PLANTA));
                    mapTableroEnGranjas(listaTablero.FindAll(s => s.ubicacion == UbicacionTablero.GRANJA));
                    mapTableroEnPLANTA(listaTablero.FindAll(s => s.ubicacion == UbicacionTablero.PLANTA));

                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapTableroEnPLANTA(List<TableroControlUnidades> list)
        {
            lvlEnPLANTA.ItemsSource = list;
        }

        private void mapTableroEnGranjas(List<TableroControlUnidades> list)
        {
            lvlEnGranjas.ItemsSource = list;
        }

        private void mapTableroTrayectoPlanta(List<TableroControlUnidades> list)
        {
            lvlTrayectoPlanta.ItemsSource = list;
        }

        private void mapTableroTrayetoGranja(List<TableroControlUnidades> list)
        {
            lvlDestinos.ItemsSource = list;
        }
    }
}
