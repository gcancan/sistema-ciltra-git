﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Interfaces;
    using CoreFletera.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Forms;
    using System.Windows.Input;
    using System.Windows.Markup;
    using System.Windows.Media;

    public partial class reimprecionDocumentos : WpfClient.ViewBase
    {
        public reimprecionDocumentos()
        {
            this.InitializeComponent();
        }
        private List<List<CartaPorte>> agruparByZona(List<CartaPorte> listDetalles)
        {
            AgruparCartaPortes portes = new AgruparCartaPortes();
            portes.agruparByZona(listDetalles);
            return portes.superLista;
        }
        private void btnCambiarImpreso_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.PrintDialog dialog = new System.Windows.Forms.PrintDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                new Core.Utils.Util().setRutasImpresora("VIAJES_ATLANTE", dialog.PrinterSettings.PrinterName);
                this.establecerImpresora();
            }
        }

        private void btnImprimirCP_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int num = 0;
                if (string.IsNullOrEmpty(this.txtFolio.Text.Trim()))
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "No se proporciono Folio"
                    };
                    ImprimirMensaje.imprimir(resp);
                    this.txtFolio.Focus();
                }
                else if (!int.TryParse(this.txtFolio.Text.Trim(), out num))
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "El Folio no es valido para este documento, contiene caracteres no validos"
                    };
                    ImprimirMensaje.imprimir(resp);
                    this.txtFolio.Focus();
                }
                else
                {
                    OperationResult resp = new CartaPorteSvc().getCartaPorteById(num);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        Viaje viaje = ((List<Viaje>)resp.result)[0];
                        this.imprimirViajes(viaje);
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void btnImprimirValesCarga_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = System.Windows.Input.Cursors.Wait;
                int num = 0;
                if (string.IsNullOrEmpty(this.txtFolio.Text.Trim()))
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "No se proporciono Folio"
                    };
                    ImprimirMensaje.imprimir(resp);
                    this.txtFolio.Focus();
                }
                else if (!int.TryParse(this.txtFolio.Text.Trim(), out num))
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "El Folio no es valido para este documento, contiene caracteres no validos"
                    };
                    ImprimirMensaje.imprimir(resp);
                    this.txtFolio.Focus();
                }
                else
                {
                    OperationResult resp = new CartaPorteSvc().getCartaPorteById(num);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        Viaje viaje = ((List<Viaje>)resp.result)[0];
                        OperationResult result2 = new CartaPorteSvc().getValesCargaByIdViaje(viaje);
                        if (result2.typeResult == ResultTypes.success)
                        {
                            List<ValeCarga> result = (List<ValeCarga>)result2.result;
                            this.imprimirVales(result, viaje);
                        }
                        else
                        {
                            ImprimirMensaje.imprimir(result2);
                        }
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }

        private void btnValeCombustible_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = System.Windows.Input.Cursors.Wait;
                int num = 0;
                if (string.IsNullOrEmpty(this.txtFolio.Text.Trim()))
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "No se proporciono Orden de Servicio"
                    };
                    ImprimirMensaje.imprimir(resp);
                    this.txtFolio.Focus();
                }
                else if (!int.TryParse(this.txtFolio.Text.Trim(), out num))
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "El Folio no es valido para este documento, contiene caracteres no validos"
                    };
                    ImprimirMensaje.imprimir(resp);
                    this.txtFolio.Focus();
                }
                else
                {
                    OperationResult resp = new OperacionSvc().getSalidasEntradasByIdOrdenServ(num);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        //Dispatcher.BeginInvoke(() => this.gridProceso.Visibility = Visibility.Visible);
                        CargaCombustible result = (CargaCombustible)resp.result;
                        ReportView view = new ReportView();
                        if (result.masterOrdServ.tractor.empresa.clave == 1)
                        {
                            view.createValeGasolinaNuevo2(result, new Core.Utils.Util().getRutasImpresora("VIAJES_ATLANTE"));
                        }
                        else
                        {
                            view.createValeGasolinaNuevo(result, new Core.Utils.Util().getRutasImpresora("VIAJES_ATLANTE"));
                        }
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
                this.gridProceso.Visibility = Visibility.Hidden;
            }
        }

        private bool establecerImpresora()
        {
            try
            {
                string str = new Core.Utils.Util().getRutasImpresora("VIAJES_ATLANTE");
                if (string.IsNullOrEmpty(str))
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "Se requiere seleccionar la impresora"
                    };
                    ImprimirMensaje.imprimir(resp);
                    System.Windows.Forms.PrintDialog dialog = new System.Windows.Forms.PrintDialog();
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        new Core.Utils.Util().setRutasImpresora("VIAJES_ATLANTE", dialog.PrinterSettings.PrinterName);
                        return this.establecerImpresora();
                    }
                    return false;
                }
                this.txtNombreImpresora.Text = str;
                return true;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return false;
            }
        }

        private void imprimirReportes(ComercialCliente cli, ComercialEmpresa emp, DatosFacturaXML datosFactura, List<CartaPorte> listaAgrupada, Viaje viaje)
        {
            try
            {
                base.Cursor = System.Windows.Input.Cursors.Wait;
                new ReportView(base.mainWindow).cartaPorteFormatoNuevo(cli, emp, datosFactura, listaAgrupada, viaje, new Core.Utils.Util().getRutasImpresora("VIAJES_ATLANTE"));
            }
            catch (Exception exception)
            {
                System.Windows.MessageBox.Show(exception.Message);
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }

        private void imprimirReportesAtlante(ComercialCliente cli, ComercialEmpresa emp, DatosFacturaXML datosFactura, List<CartaPorte> listaAgrupada, Viaje viaje)
        {
            try
            {
                base.Cursor = System.Windows.Input.Cursors.Wait;
                new ReportView(base.mainWindow).cartaPorteFormatoNuevo(cli, emp, datosFactura, listaAgrupada, viaje, new Core.Utils.Util().getRutasImpresora("VIAJES_ATLANTE"));
            }
            catch (Exception exception)
            {
                System.Windows.MessageBox.Show(exception.Message);
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }

        private void imprimirVales(List<ValeCarga> listVales, Viaje viaje)
        {
            try
            {
                //base.get_Dispatcher().Invoke(() => this.gridProceso.Visibility = Visibility.Visible);
                base.Cursor = System.Windows.Input.Cursors.Wait;
                new ReportView().imprimirValesCarga(listVales, viaje, new Core.Utils.Util().getRutasImpresora("VIAJES_ATLANTE"));
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
                this.gridProceso.Visibility = Visibility.Hidden;
            }
        }

        private void imprimirViajes(Viaje viaje)
        {
            try
            {
                //base.get_Dispatcher().Invoke(() => this.gridProceso.Visibility = Visibility.Visible);
                base.Cursor = System.Windows.Input.Cursors.Wait;
                string str = new Core.Utils.Util().getRutaXML();
                if (string.IsNullOrEmpty(str))
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "Para proceder se requiere la ruta del archivo XML"
                    };
                    ImprimirMensaje.imprimir(resp);
                }
                else
                {
                    ComercialCliente cli = new ComercialCliente
                    {
                        nombre = viaje.cliente.nombre,
                        calle = viaje.cliente.domicilio,
                        rfc = viaje.cliente.rfc,
                        colonia = viaje.cliente.colonia,
                        estado = viaje.cliente.estado,
                        municipio = viaje.cliente.municipio,
                        pais = viaje.cliente.pais,
                        telefono = viaje.cliente.telefono,
                        cp = viaje.cliente.cp
                    };
                    Empresa empresa = viaje.cliente.empresa;
                    ComercialEmpresa emp = new ComercialEmpresa
                    {
                        nombre = empresa.nombre,
                        calle = empresa.direccion,
                        rfc = empresa.rfc,
                        colonia = empresa.colonia,
                        estado = empresa.estado,
                        municipio = base.mainWindow.inicio._usuario.personal.empresa.municipio,
                        pais = empresa.pais,
                        telefono = empresa.telefono,
                        cp = empresa.cp
                    };
                    DatosFacturaXML datosFactura = ObtenerDatosFactura.obtenerDatos(str);
                    if (datosFactura == null)
                    {
                        OperationResult resp = new OperationResult
                        {
                            valor = 1,
                            mensaje = "Ocurrio un error al intentar obtener datos del XML"
                        };
                        ImprimirMensaje.imprimir(resp);
                    }
                    else if (viaje.IdEmpresa == 1)
                    {
                        List<List<CartaPorte>> list = this.agruparByZona(viaje.listDetalles);
                        foreach (List<CartaPorte> list2 in list)
                        {
                            this.imprimirReportes(cli, emp, datosFactura, list2, viaje);
                        }
                    }
                    else
                    {
                        List<List<CartaPorte>> list3 = this.agruparByZona(viaje.listDetalles);
                        foreach (List<CartaPorte> list4 in list3)
                        {
                            this.imprimirReportesAtlante(cli, emp, datosFactura, list4, viaje);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
                this.gridProceso.Visibility = Visibility.Hidden;
            }
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ctrFolioPadre.txtEntero.KeyDown += TxtEntero_KeyDown;
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
            if (!this.establecerImpresora())
            {
                base.mainWindow.scrollContainer.IsEnabled = false;
            }
            else
            {
                this.establecerImpresora();
            }
        }

        private void TxtEntero_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            try
            {
                if (Key.Enter == e.Key)
                {
                    if (ctrFolioPadre.valor != 0)
                    {
                        buscarMasterOrdenServ(ctrFolioPadre.valor);
                    }
                }
            }
            catch (Exception ex)
            {
                //throw;
            }
        }
        private void buscarMasterOrdenServ(int valor)
        {
            try
            {
                stpOrdenesServ.Children.Clear();
                Cursor = System.Windows.Input.Cursors.Wait;
                var resp = new SolicitudOrdenServicioSvc().getMasterOrdenServicioByFolio(valor);
                if (resp.typeResult == ResultTypes.success)
                {
                    llenarControles(resp.result as MasterOrdServ);
                }
                else
                {
                   ImprimirMensaje.imprimir(resp);
                    nuevo();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }
        public override void nuevo()
        {
            base.nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
            ctrFolioPadre.Tag = null;
            ctrFolioPadre.valor = 0;
        }

        private void llenarControles(MasterOrdServ masterOrdServ)
        {
            chcTitulo.IsChecked = false;
            ctrFolioPadre.Tag = masterOrdServ;
            foreach (var ordenServ in masterOrdServ.listaCabOrdenServicio)
            {
                System.Windows.Controls.CheckBox chc = new System.Windows.Controls.CheckBox { Tag = ordenServ, IsChecked = false, Margin = new Thickness(1)};

                StackPanel stpContent = new StackPanel { Orientation = System.Windows.Controls.Orientation.Horizontal };

                System.Windows.Controls.Label lblUnidad = new System.Windows.Controls.Label
                {
                    Content = ordenServ.unidad.clave,
                    Foreground = Brushes.SteelBlue,
                    Width = 120, 
                    Margin = new Thickness(1)
                };
                System.Windows.Controls.Label lblNOOrden = new System.Windows.Controls.Label
                {
                    Content = ordenServ.idOrdenSer,
                    Foreground = Brushes.SteelBlue,
                    Width = 120,
                    Margin = new Thickness(1)
                };
                System.Windows.Controls.Label lblTipoOrden = new System.Windows.Controls.Label
                {
                    Content = ordenServ.enumTipoOrdServ,
                    Foreground = Brushes.SteelBlue,
                    Width = 120,
                    Margin = new Thickness(1)
                };
                System.Windows.Controls.Label lblTipoServicio = new System.Windows.Controls.Label
                {
                    Content = ordenServ.enumtipoServicio,
                    Foreground = Brushes.SteelBlue,
                    Width = 120,
                    Margin = new Thickness(1)
                };

                stpContent.Children.Add(lblUnidad);
                stpContent.Children.Add(lblNOOrden);
                stpContent.Children.Add(lblTipoOrden);
                stpContent.Children.Add(lblTipoServicio);

                chc.Content = stpContent;
                stpOrdenesServ.Children.Add(chc);

            }

            chcTitulo.IsChecked = true;
        }

        private void chcTitulo_Checked(object sender, RoutedEventArgs e)
        {
            marcarDesmarcarControles(true);
        }

        private void chcTitulo_Unchecked(object sender, RoutedEventArgs e)
        {
            marcarDesmarcarControles(false);
        }
        void marcarDesmarcarControles(bool check)
        {
            try
            {                
                foreach (System.Windows.Controls.Control ctr in stpOrdenesServ.Children)
                {
                    if (ctr is System.Windows.Controls.CheckBox)
                    {
                        System.Windows.Controls.CheckBox checkBox = ctr as System.Windows.Controls.CheckBox;
                        checkBox.IsChecked = check;
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }

        private void btnImprimirOrdenesServicio_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = System.Windows.Input.Cursors.Wait;
                MasterOrdServ masterOrdServ = ctrFolioPadre.Tag as MasterOrdServ;
                List<CabOrdenServicio> listCab = new List<CabOrdenServicio>();

                foreach (System.Windows.Controls.Control ctr in stpOrdenesServ.Children)
                {
                    if (ctr is System.Windows.Controls.CheckBox)
                    {
                        System.Windows.Controls.CheckBox checkBox = ctr as System.Windows.Controls.CheckBox;
                        if (checkBox.IsChecked.Value)
                        {
                            listCab.Add(checkBox.Tag as CabOrdenServicio);
                        }
                    }
                }

                Dispatcher.Invoke(() =>
                {
                    foreach (var item in listCab)
                    {
                        var rep = new ReportView();
                        rep.crearSolicitudOrdServ(masterOrdServ, item, this.txtNombreImpresora.Text);
                        rep.Show();
                    }
                });
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }
    }
}
