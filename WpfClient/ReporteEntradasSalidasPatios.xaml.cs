﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoreFletera.Models;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for ReporteEntradasSalidasPatios.xaml
    /// </summary>
    public partial class ReporteEntradasSalidasPatios : ViewBase
    {
        public ReporteEntradasSalidasPatios()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;                
                var v = mainWindow.inicio._usuario.personal.nombre;
                ctrEmpresas.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
                ctrEmpresas.loaded(mainWindow.empresa, true);
                
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void btnImprimir_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                var respReporte = new ReportView();
                OperationResult resp = new BitacoraEntradaSalidasSvc().getBitacoraEntradaSalidas(ctlFechas.fechaInicial, ctlFechas.fechaFinal, ctrEmpresas.empresaSelected.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    if (respReporte.ReporteBitacoraSalidasEntradas(ctlFechas.fechaInicial, ctlFechas.fechaFinal, (List<BitacoraEntradaSalidas>)resp.result, ctlFechas.isMes))
                    {
                        respReporte.ShowDialog();
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }

    }

