﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfClient
{
    public static class Validaciones
    {
        public static int validarDecimal(string text)
        {
            decimal d = 0;
            if (string.IsNullOrEmpty(text))
            {
                return 1;
            }
            if (!decimal.TryParse(text, out d))
            {
                return 2;
            }
            else
            {
                return 0;
            }

        }

        public static int validarInt(string text)
        {
            int d = 0;
            if (string.IsNullOrEmpty(text))
            {
                return 1;
            }
            if (!int.TryParse(text, out d))
            {
                return 2;
            }
            else
            {
                return 0;
            }

        }
    }
}
