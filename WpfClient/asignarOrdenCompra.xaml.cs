﻿using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for asignarOrdenCompra.xaml
    /// </summary>
    public partial class asignarOrdenCompra : ViewBase
    {
        public asignarOrdenCompra()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            cbxEmpresa.loaded(mainWindow.inicio._usuario.cliente.empresa, true);
            nuevo();
        }

        private void txtFolioCompra_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrEmpty(txtFolioCompra.Text.Trim()))
                {
                    buscarOrdenCompra();
                }
            }
        }
        public override void nuevo()
        {
            mapForm(null);
            base.nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
        }

        private void buscarOrdenCompra()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new OrdenCompraSvc().getOrdenCompraByFolio(cbxEmpresa.empresaSelected, txtFolioCompra.Text.Trim());
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm((OrdenCompra)resp.result);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapForm(OrdenCompra ordenCompra)
        {
            stpAsginarSeries.Children.Clear();
            if (ordenCompra != null)
            {
                txtFolioCompra.Tag = ordenCompra;
                txtFolioCompra.IsEnabled = false;
                txtFecha.Text = ordenCompra.fecha.ToString();
                txtProveedor.Text = ordenCompra.proveedor.razonSocial;
                txtTipoOrden.Text = ordenCompra.isLLanta ? "LLANTAS" : "INSUMOS";
                txtEstatus.Text = ordenCompra.estatus;
                mapDetalles(ordenCompra.listaDetalles);

                var resp = new LlantaSvc().getLlantasByOrdenCompra(ordenCompra);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapArticulosLlantas((List<Llanta>)resp.result);
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                }

                var respLL = new LlantaSvc().getLlantasPendientesByIdOrdenCompra(ordenCompra.idOrdenCompra);
                if (respLL.typeResult == ResultTypes.success)
                {
                    mapLlantas((List<Llanta>)respLL.result);
                }
            }
            else
            {
                txtFolioCompra.Clear();
                txtFolioCompra.Tag = null;
                txtFolioCompra.IsEnabled = true;
                txtFecha.Clear();
                txtProveedor.Clear();
                txtTipoOrden.Clear();
                txtEstatus.Clear();
                mapDetalles(new List<DetalleOrdenCompra>());
                mapArticulosLlantas(new List<Llanta>());
                
                chcTodos.IsChecked = false;
            }
            //stpAsginarSeries.IsEnabled = false;
        }

        private void mapLlantas(List<Llanta> listLlanta)
        {
            try
            {
                foreach (Llanta llanta in listLlanta)
                {
                    CheckBox chcPrincipal = new CheckBox { VerticalContentAlignment = VerticalAlignment.Center, Tag = llanta };
                    StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal };

                    chcPrincipal.Unchecked += ChcPrincipal_Unchecked;

                    Label lblFolio = new Label
                    {
                        Content = llanta.clave,
                        Width = 80,
                        Name = "lblFolio",
                        HorizontalContentAlignment = HorizontalAlignment.Left
                    };
                    stpContent.Children.Add(lblFolio);

                    Label lblDescripcion = new Label
                    {
                        Content = llanta.marca.descripcion + " " + llanta.diseño + " " + llanta.medida,
                        Width = 260,
                        Name = "lblDescripcion",
                        HorizontalContentAlignment = HorizontalAlignment.Center
                    };
                    stpContent.Children.Add(lblDescripcion);

                    Label lblFactura = new Label
                    {
                        Content = llanta.factura,
                        Width = 124,
                        Name = "lblFactura",
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        FontWeight = FontWeights.Bold
                    };
                    stpContent.Children.Add(lblFactura);

                    chcPrincipal.Content = stpContent;
                    stpAsginarSeries.Children.Add(chcPrincipal);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        bool omitir = false;
        private void ChcPrincipal_Unchecked(object sender, RoutedEventArgs e)
        {
            omitir = true;
            chcTodos.IsChecked = false;
            omitir = false;
        }

        private void mapArticulosLlantas(List<Llanta> listaLlantas)
        {
            stpArticulos.Children.Clear();
            foreach (var Llanta in listaLlantas)
            {
                StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal };
                stpContent.Tag = Llanta;

                Label lbl = new Label() { Width = 256, Tag = Llanta, Content = Llanta.marca.descripcion + " " + Llanta.diseño + " " + Llanta.medida };
                stpContent.Children.Add(lbl);

                Label lblClave = new Label { Width = 100, HorizontalContentAlignment = HorizontalAlignment.Center, Content = Llanta.clave };
                stpContent.Children.Add(lblClave);

                Label lblUbicacion = new Label { Width = 225, HorizontalContentAlignment = HorizontalAlignment.Center, Content = Llanta.getUbicacion() };
                stpContent.Children.Add(lblUbicacion);

                stpArticulos.Children.Add(stpContent);
            }
        }

        private void mapDetalles(List<DetalleOrdenCompra> listaDetalles)
        {
            stpDetalles.Children.Clear();
            foreach (DetalleOrdenCompra item in listaDetalles)
            {
                StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal };
                stpContent.Tag = item;

                Label lbl = new Label() { Width = 256, Tag = item, Content = item.llantaProducto.marca.descripcion + " " + item.llantaProducto.diseño.descripcion + " " + item.llantaProducto.medida };
                stpContent.Children.Add(lbl);

                Label ctrUnidad = new Label { Content = item.unidadTransporte.clave, Width = 130, HorizontalContentAlignment = HorizontalAlignment.Center };
                stpContent.Children.Add(ctrUnidad);

                Label lblCantidad = new Label { Width = 80, Content = Convert.ToInt32(item.cantidad).ToString(), HorizontalContentAlignment = HorizontalAlignment.Center };
                stpContent.Children.Add(lblCantidad);

                Label lblPendientes = new Label { Width = 80, Content = Convert.ToInt32(item.pendientes).ToString(), HorizontalContentAlignment = HorizontalAlignment.Center };
                stpContent.Children.Add(lblPendientes);

                stpDetalles.Children.Add(stpContent);
            }
            //llenarListaLlantas();
        }

        void llenarListaLlantas()
        {
            List<DetalleOrdenCompra> listaLLantas = new List<DetalleOrdenCompra>();
            foreach (StackPanel stPanel in stpDetalles.Children.Cast<StackPanel>().ToList())
            {
                DetalleOrdenCompra detalle = (DetalleOrdenCompra)stPanel.Tag;
                for (int i = 0; i < detalle.pendientes; i++)
                {
                    Llanta llanta = new Llanta
                    {
                        idEmpresa = (cbxEmpresa.empresaSelected.clave),
                        productoLLanta = detalle.llantaProducto,
                        noSerie = "",
                        factura = "",
                        medida = detalle.llantaProducto.medida,
                        profundidad = detalle.llantaProducto.profundidad,
                        diseño = detalle.llantaProducto.diseño.descripcion,
                        marca = detalle.llantaProducto.marca
                    };
                    listaLLantas.Add(detalle);
                }
            }
            if (listaLLantas.Count > 0)
            {
                añadirListaLlanta(listaLLantas);
            }
            else
            {
                stpAsginarSeries.Children.Clear();
            }

        }

        private void añadirListaLlanta(List<DetalleOrdenCompra> listaLLantas)
        {
            stpAsginarSeries.Children.Clear();
            foreach (var item in listaLLantas)
            {

                StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal };
                stpContent.Tag = item;

                Label lbl = new Label() { Width = 255, Tag = item, Content = item.llantaProducto.marca.descripcion + " " + item.llantaProducto.diseño.descripcion + " " + item.llantaProducto.medida };
                stpContent.Children.Add(lbl);

                Label lblFactura = new Label { Width = 124, HorizontalContentAlignment = HorizontalAlignment.Center, Name = "lblFactura" };
                stpContent.Children.Add(lblFactura);

                TextBox txt = new TextBox { Width = 150, Margin = new Thickness(2), HorizontalContentAlignment = HorizontalAlignment.Center };
                stpContent.Children.Add(txt);

                stpAsginarSeries.Children.Add(stpContent);
            }
        }

        private void BtnQuitar_Click(object sender, RoutedEventArgs e)
        {
        }

        private void btnAceptarFactura_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtFactura.Text.Trim()))
            {
                stpAsginarSeries.IsEnabled = true;
                foreach (CheckBox chcPrincipal in stpAsginarSeries.Children.Cast<CheckBox>().ToList())
                {
                    if (chcPrincipal.IsChecked.Value)
                    {
                        Llanta detalle = (Llanta)chcPrincipal.Tag;
                        foreach (Control item in ((StackPanel)chcPrincipal.Content).Children.Cast<Control>().ToList())
                        {
                            if (item is Label)
                            {
                                if (((Label)item).Name == "lblFactura")
                                {
                                    ((Label)item).Content = txtFactura.Text.Trim();

                                    detalle.factura = ((Label)item).Content.ToString();
                                    //detalle.fechaFactura = dtpFechaFactura.SelectedDate.Value;
                                }
                            }
                        }
                    }
                    chcPrincipal.IsChecked = false;
                }
            }
        }

        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                List<Llanta> listaLlanta = getListaLlantas();
                if (listaLlanta.Count > 0)
                {
                    var resp = new LlantaSvc().saveLlantasFromOrdenCompra(listaLlanta, listaRelacionFact, mainWindow.inicio._usuario.nombreUsuario, (OrdenCompra)txtFolioCompra.Tag);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        //mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
                        mainWindow.habilitar = true;
                        buscarOrdenCompra();
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult { valor = 3, mensaje = "NO HAY INFORMACIÓN PARA GUARDAR" };
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
            finally
            {
                Cursor = Cursors.Arrow;
                mainWindow.scrollContainer.IsEnabled = true;
            }
        }

        List<RelacionDetalleOrdenFactura> listaRelacionFact = new List<RelacionDetalleOrdenFactura>();
        private List<Llanta> getListaLlantas()
        {
            List<Llanta> listaLlanta = new List<Llanta>();
            listaRelacionFact = new List<RelacionDetalleOrdenFactura>();
            foreach (CheckBox chcPrincipal in stpAsginarSeries.Children.Cast<CheckBox>().ToList())
            {
                if (chcPrincipal.Tag is Llanta)
                {
                    Llanta llanta = (Llanta)chcPrincipal.Tag;
                    if (!string.IsNullOrEmpty(llanta.factura))
                    {
                        listaLlanta.Add(llanta);
                        sumarListaRelacioOrdenFact(new DetalleOrdenCompra { idDetalleCompra = llanta.idDetalleCompra });
                    }

                }
            }
            return listaLlanta;
        }

        private void sumarListaRelacioOrdenFact(DetalleOrdenCompra detalle)
        {

            bool encontro = false;
            foreach (RelacionDetalleOrdenFactura item in listaRelacionFact)
            {
                if (item.detalleCompra.idDetalleCompra == detalle.idDetalleCompra)
                {
                    item.cantidad += 1;
                    encontro = true;
                    break;
                }
            }
            if (!encontro)
            {
                listaRelacionFact.Add(new RelacionDetalleOrdenFactura
                {
                    ordenCompra = (OrdenCompra)txtFolioCompra.Tag,
                    detalleCompra = detalle,
                    cantidad = 1
                });
            }

        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            marcarDesmarcar(true);
        }
        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (omitir)
            {
                return;
            }
            marcarDesmarcar(false);
        }
        void marcarDesmarcar(bool chc)
        {
            foreach (CheckBox chcPrincipal in stpAsginarSeries.Children.Cast<CheckBox>().ToList())
            {
                chcPrincipal.IsChecked = chc;
            }
        }
    }
}