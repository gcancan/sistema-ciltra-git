﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para controlZonaOperativa.xaml
    /// </summary>
    public partial class controlZonaOperativa : UserControl
    {
        public controlZonaOperativa()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            cbxZonaOpetativa.Width = this.Width - 98;
        }
        public void cargarControl(Usuario usuario)
        {
            cargarControl(usuario.zonaOperativa, usuario.idUsuario);
        }
        public void cargarControl(ZonaOperativa zonaOpe, int idUsuario)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new ZonaOperativaSvc().getAllZonasOperativas();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxZonaOpetativa.ItemsSource = resp.result as List<ZonaOperativa>;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
                if (zonaOpe != null)
                {
                    int i = 0;
                    foreach (ZonaOperativa item in cbxZonaOpetativa.Items)
                    {
                        if (item.idZonaOperativa == zonaOpe.idZonaOperativa)
                        {
                            cbxZonaOpetativa.SelectedIndex = i;
                            break;
                        }
                        i++;
                    }
                }
                else
                {
                    cbxZonaOpetativa.SelectedItem = null;
                }
                var respPri = new PrivilegioSvc().consultarPrivilegio("MULTI_ZONA_OPETATIVA", idUsuario);
                cbxZonaOpetativa.IsEnabled = respPri.typeResult == ResultTypes.success;                
            }
        }
        public ZonaOperativa zonaOperativa
        {
            get
            {
                if (cbxZonaOpetativa.SelectedItem == null)
                {
                    return null;
                }
               return cbxZonaOpetativa.SelectedItem as ZonaOperativa;
            }
            set
            {
                ZonaOperativa zonaOpe = value;
                if (zonaOpe == null)
                {
                    cbxZonaOpetativa.SelectedItem = null;
                }
                else
                {
                    int i = 0;
                    foreach (ZonaOperativa item in cbxZonaOpetativa.ItemsSource)
                    {
                        if (item.idZonaOperativa == zonaOpe.idZonaOperativa)
                        {
                            cbxZonaOpetativa.SelectedIndex = i;
                            break;
                        }
                        i++;
                    }
                }
                
            }
        }
    }
}
