﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para reporteSuministrosCombustible.xaml
    /// </summary>
    public partial class reporteSuministrosCombustible : ViewBase
    {
        public reporteSuministrosCombustible()
        {
            InitializeComponent();
        }
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                this.lvlSuministros.ItemsSource = null;
                OperationResult resp = new SuministroCombustibleSvc().getDetalleSuministroCombustible(this.ctrFechas.fechaInicial, this.ctrFechas.fechaFinal, (this.txtTanque.Tag as TanqueCombustible).idTanqueCombustible, this.chcTodasEmpresa.IsChecked.Value ? 0 : this.ctrEmpresa.empresaSelected.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    this.lvlSuministros.ItemsSource = resp.result as List<ReporteSuministrosCombustible>;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void btnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                List<ReporteSuministrosCombustible> itemsSource = new List<ReporteSuministrosCombustible>();
                itemsSource = this.lvlSuministros.ItemsSource as List<ReporteSuministrosCombustible>;
                if (itemsSource != null)
                {
                    new ReportView().generarReporteSuministrosCombustible(itemsSource as List<ReporteSuministrosCombustible>);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.ctrZonaOperativa.zonaOperativa != null)
            {
                this.getTanquesDiesel(this.ctrZonaOperativa.zonaOperativa);
            }
        }

        private void getTanquesDiesel(ZonaOperativa zonaOperativa)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult resp = new TanqueCombustibleSvc().getTanqueByZonaOperativa(zonaOperativa);
                if (resp.typeResult == ResultTypes.success)
                {
                    TanqueCombustible combustible = resp.result as TanqueCombustible;
                    this.txtTanque.Tag = combustible;
                    this.txtTanque.Text = combustible.nombre;
                }
                else
                {
                    if (resp.typeResult != ResultTypes.recordNotFound)
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                    this.txtTanque.Tag = null;
                    this.txtTanque.Clear();
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += new SelectionChangedEventHandler(this.CbxZonaOpetativa_SelectionChanged);
            this.ctrZonaOperativa.cargarControl(base.mainWindow.zonaOperativa, base.mainWindow.usuario.idUsuario);
            bool habilitado = false;
            if (new PrivilegioSvc().consultarPrivilegio("isAdmin", base.mainWindow.usuario.idUsuario).typeResult == ResultTypes.success)
            {
                habilitado = true;
            }
            else
            {
                this.chcTodasEmpresa.IsEnabled = false;
            }
            this.ctrEmpresa.loaded(base.mainWindow.empresa, habilitado);
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
        }
    }
}
