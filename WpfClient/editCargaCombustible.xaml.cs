﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using Core.Interfaces;
using CoreFletera.Models;
using CoreFletera.Interfaces;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for editCargaCombustible.xaml
    /// </summary>
    public partial class editCargaCombustible : ViewBase
    {
        public editCargaCombustible()
        {
            InitializeComponent();
        }
        int claveEmpresa = 0;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {           

            OperationResult resp = new CargaCombustibleSvc().getProveedor();
            if (resp.typeResult == ResultTypes.success)
            {
                txtProveedor.ItemsSource = (List<ProveedorCombustible>)resp.result;
                txtProveedorCarga.ItemsSource = (List<ProveedorCombustible>)resp.result;
            }
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar);
            txtLitros.txtDecimal.TextChanged += TxtDecimal_TextChanged;
            //lvlVales.Visibility = Visibility.Collapsed;
            limpiarExtra(false);
            getTiposCombustible();
            nuevo();
            OperationResult respPrivilegio = new PrivilegioSvc().consultarPrivilegio("CAPTURA_CARGAS_EXTERNAS", mainWindow.inicio._usuario.idUsuario);
            if (respPrivilegio.typeResult == ResultTypes.success)
            {
                expExterno.IsEnabled = true;
            }

            ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
            ctrZonaOperativa.cargarControl(mainWindow.zonaOperativa, mainWindow.usuario.idUsuario);
        }
        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ctrZonaOperativa.zonaOperativa != null)
            {
                getNivelesByZonaOperativa(ctrZonaOperativa.zonaOperativa);
            }
        }

        private void getNivelesByZonaOperativa(ZonaOperativa zonaOperativa)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new NivelesTanqueSvc().getNivelTanqueByZonaOperativa(zonaOperativa);
                var nivelesTanque = resp.result as NivelesTanque;
                if (resp.typeResult == ResultTypes.success)
                {
                    if (nivelesTanque.fechaFinal != null)
                    {
                        imprimirMensaje(new OperationResult(ResultTypes.warning, "NO ESTAN ABIERTOS LOS NIVELES"));
                        //mainWindow.scrollContainer.IsEnabled = false;
                        mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                        return;
                    }
                    mapNivelTanque(nivelesTanque);
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo |ToolbarCommands.guardar| ToolbarCommands.cerrar);
                }
                else
                {
                    mapNivelTanque(null);
                    //mainWindow.scrollContainer.IsEnabled = false;
                    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapNivelTanque(NivelesTanque nivelesTanque)
        {
            if (nivelesTanque != null)
            {
                txtClave.Tag = nivelesTanque;
                txtClave.valor = nivelesTanque.idNivel;
                txtFechaInicio.Text = nivelesTanque.fechaInicio.ToString("dd/MM/yyyy HH:mm:ss");              
                
            }
            else
            {
                txtClave.Tag = null;
                txtClave.valor = 0;
                txtFechaInicio.Clear();              
            }
        }

        decimal valorDisel = 0m;
        private void getTiposCombustible()
        {
            try
            {
                OperationResult resp = new TipoCombustibleSvc().getTiposCombustibleAllOrById();
                if (resp.typeResult == ResultTypes.success)
                {
                    List<TipoCombustible> lista = (List<TipoCombustible>)resp.result;
                    TipoCombustible tipo = lista.Find(s => s.tipoCombustible.ToUpper() == "DIESEL");
                    valorDisel = tipo.ultimoPrecio;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void TxtDecimal_TextChanged(object sender, TextChangedEventArgs e)
        {
            actualizarLitrosCargados();
        }

        public override void nuevo()
        {
            txtPrecioCombustible.valor = valorDisel;
            expExterno.IsExpanded = false;
            expCargaExtra.IsExpanded = false;
            base.nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar);
            mapForm(null);
            txtFolio.Focus();
            mainWindow.scrollContainer.IsEnabled = true;
            txtFolio.Focus();
        }

        enum TipoUnidad
        {
            TRACTOR = 0,
            TOLVA = 1,
            DOLLY = 2,
            TOLVA_2 = 3
        }

        private void mapUnidades(TipoUnidad tipo, UnidadTransporte Unidad)
        {
            switch (tipo)
            {
                case TipoUnidad.TRACTOR:
                    if (Unidad != null)
                    {
                        txtTractor.Tag = Unidad;
                        txtTractor.Text = Unidad.clave;
                        txtTractor.IsEnabled = false;
                        txtOperador.Focus();
                    }
                    else
                    {
                        txtTractor.Tag = null;
                        txtTractor.Clear();
                        txtTractor.IsEnabled = true;
                    }
                    break;
                //case TipoUnidad.TOLVA:
                //    if (Unidad != null)
                //    {
                //        txtTolva1.Tag = Unidad;
                //        txtTolva1.Text = Unidad.clave;
                //    }
                //    else
                //    {
                //        txtTolva1.Tag = null;
                //        txtTolva1.Clear();
                //    }
                //    break;
                //case TipoUnidad.DOLLY:
                //    if (Unidad != null)
                //    {
                //        txtDolly.Tag = Unidad;
                //        txtDolly.Text = Unidad.clave;
                //    }
                //    else
                //    {
                //        txtDolly.Tag = null;
                //        txtDolly.Clear();
                //    }
                //    break;
                //case TipoUnidad.TOLVA_2:
                //    if (Unidad != null)
                //    {
                //        txtTolva2.Tag = Unidad;
                //        txtTolva2.Text = Unidad.clave;
                //    }
                //    else
                //    {
                //        txtTolva2.Tag = null;
                //        txtTolva2.Clear();
                //    }
                //    break;
                default:
                    break;
            }
        }

        private List<UnidadTransporte> buscarUnidades(string clave)
        {
            OperationResult resp = new UnidadTransporteSvc().getUnidadesALLorById(claveEmpresa, clave);
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    return (List<UnidadTransporte>)resp.result;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return null;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return null;
                case ResultTypes.recordNotFound:
                    return new List<UnidadTransporte>();

                default:
                    return null;
            }
        }

        private void completarClave(ref string clave)
        {
            if (clave.Length < 3)
            {
                for (int i = 0; i <= (3 - clave.Length); i++)
                {
                    clave = "0" + clave;
                }
            }
        }

        private void txtTractor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string clave = txtTractor.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                completarClave(ref clave);
                UnidadTransporte unidadTransporte = new UnidadTransporte();
                List<UnidadTransporte> ListUnidadT = buscarUnidades("TC" + clave);
                if (ListUnidadT.Count == 1)
                {
                    mapUnidades(TipoUnidad.TRACTOR, ListUnidadT[0]);
                    unidadTransporte = ListUnidadT[0];
                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("TC", claveEmpresa, clave);
                    var s = buscardor.buscarUnidades();
                    mapUnidades(TipoUnidad.TRACTOR, s);
                    unidadTransporte = s;
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    mapUnidades(TipoUnidad.TRACTOR, s);
                    unidadTransporte = s;
                }

                //if (unidadTransporte != null)
                //{
                //    var resp = new OperacionSvc().getSalidaEntradaByCamion(unidadTransporte.clave);
                //    if (((SalidaEntrada)resp.result).tipoMoviemiento == eTipoMovimiento.ENTRADA)
                //    {

                //        //    mapOperador(((SalidaEntrada)resp.result).chofer);
                //        ////guardar(eTipoMovimiento.SALIDA);
                //        //salidaEntrada = ((SalidaEntrada)resp.result);

                //    }
                //}
            }
        }

        private void txtOperador_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string clave = txtOperador.Text.Trim();
                //completarClave(ref clave);
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                List<Personal> listPersonal = buscarPersonal(clave);
                if (listPersonal.Count == 1)
                {
                    mapOperador(listPersonal[0]);
                }
                else if (listPersonal.Count == 0)
                {
                    selectPersonal buscardor = new selectPersonal(clave, ((UnidadTransporte)txtTractor.Tag).empresa.clave);
                    var per = buscardor.buscarPersonal();
                    mapOperador(per);
                }
                else
                {
                    selectPersonal buscardor = new selectPersonal(clave, ((UnidadTransporte)txtTractor.Tag).empresa.clave);
                    var per = buscardor.buscarPersonal();
                    mapOperador(per);
                }
            }
        }

        private void mapOperador(Personal operador)
        {
            if (operador != null)
            {
                txtOperador.Tag = operador;
                txtOperador.Text = operador.nombre;
                txtOperador.IsEnabled = false;
            }
            else
            {
                txtOperador.Tag = null;
                txtOperador.Text = "";
                txtOperador.IsEnabled = true;
            }
        }

        private List<Personal> buscarPersonal(string nombre)
        {
            OperationResult resp = new OperadorSvc().getOperadoresByNombre(nombre);
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    return (List<Personal>)resp.result;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return null;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return null;
                case ResultTypes.recordNotFound:
                    return new List<Personal>();

                default:
                    return null;
            }
        }

        private List<Personal> buscarDespachador(string nombre = "%")
        {
            OperationResult resp = new OperadorSvc().getDespachadoresByNombre(nombre);
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    return (List<Personal>)resp.result;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return null;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return null;
                case ResultTypes.recordNotFound:
                    return new List<Personal>();

                default:
                    return null;
            }
        }


        private OperationResult ultimoKm
        {
            get
            {
                return new RegistroKilometrajeSvc().getUltimoKM(((UnidadTransporte)txtTractor.Tag).clave);
            }
        }

        public OperationResult validar()
        {
            var resp = new OperationResult();
            try
            {
                if (unidad.tipoUnidad.clasificacion.ToUpper() == "TRACTOR")
                {
                    if (txtFolio.Tag == null)
                    {
                        return new OperationResult { valor = 3, mensaje = "Es necesario contar con una orden de servicio" };
                    }

                    decimal valor = 0m;
                    if (!string.IsNullOrEmpty(txtCombustibleUtilizado.Text.Trim()))
                    {
                        if (!decimal.TryParse(txtCombustibleUtilizado.Text.Trim(), out valor))
                        {
                            return new OperationResult { valor = 3, mensaje = "El valor proporcionado para el combustible utilizado no es valido" };
                        }
                    }
                    else
                    {
                        return new OperationResult { valor = 3, mensaje = "Todos los Valores son obligatorios" };
                    }

                    if (!string.IsNullOrEmpty(txtCombustiblePTO.Text.Trim()))
                    {
                        if (!decimal.TryParse(txtCombustiblePTO.Text.Trim(), out valor))
                        {
                            return new OperationResult { valor = 3, mensaje = "El valor proporcionado para el combustible PTO no es valido" };
                        }
                    }
                    else
                    {
                        return new OperationResult { valor = 3, mensaje = "Todos los Valores son obligatorios" };
                    }

                    if (!string.IsNullOrEmpty(txtCombustibleEST.Text.Trim()))
                    {
                        if (!decimal.TryParse(txtCombustibleEST.Text.Trim(), out valor))
                        {
                            return new OperationResult { valor = 3, mensaje = "El valor proporcionado para el combustible EST no es valido" };
                        }
                    }
                    else
                    {
                        return new OperationResult { valor = 3, mensaje = "Todos los Valores son obligatorios" };
                    }

                    if (!string.IsNullOrEmpty(txtCombustibleCargados.Text.Trim()))
                    {
                        if (!decimal.TryParse(txtCombustibleCargados.Text.Trim(), out valor))
                        {
                            return new OperationResult { valor = 3, mensaje = "El valor proporcionado para el combustible EST no es valido" };
                        }
                    }
                    else
                    {
                        return new OperationResult { valor = 3, mensaje = "Todos los Valores son obligatorios" };
                    }

                    //TimeSpan time = new TimeSpan( txtTiempoTotal.Text);
                    var split = txtTiempoTotal.Text.Split(':');
                    try
                    {
                        TimeSpan tiempo = new TimeSpan(Convert.ToInt32(split[0]), Convert.ToInt32(split[1]), Convert.ToInt32(split[2]));
                    }
                    catch (Exception e)
                    {
                        return new OperationResult { valor = 3, mensaje = "El valor del tiempo Total no es valido" };
                    }

                    split = txtTiempoPTO.Text.Split(':');
                    try
                    {
                        TimeSpan tiempo = new TimeSpan(Convert.ToInt32(split[0]), Convert.ToInt32(split[1]), Convert.ToInt32(split[2]));
                    }
                    catch (Exception e)
                    {
                        return new OperationResult { valor = 3, mensaje = "El valor del tiempo PTO no es valido" };
                    }
                    split = txtTiempoEst.Text.Split(':');
                    try
                    {
                        TimeSpan tiempo = new TimeSpan(Convert.ToInt32(split[0]), Convert.ToInt32(split[1]), Convert.ToInt32(split[2]));
                    }
                    catch (Exception e)
                    {
                        return new OperationResult { valor = 3, mensaje = "El valor del tiempo EST no es valido" };
                    }

                    if (!string.IsNullOrEmpty(txtKmRecorridos.Text.Trim()))
                    {
                        if (!decimal.TryParse(txtKmRecorridos.Text.Trim(), out valor))
                        {
                            return new OperationResult { valor = 3, mensaje = "El valor proporcionado para los kilometros recorridos no es valido" };
                        }
                    }
                    else
                    {
                        return new OperationResult { valor = 3, mensaje = "Todos los Valores son obligatorios" };
                    }

                    if (!string.IsNullOrEmpty(txtKm.Text.Trim()))
                    {
                        if (!decimal.TryParse(txtKm.Text.Trim(), out valor))
                        {
                            return new OperationResult { valor = 3, mensaje = "El valor proporcionado para los kilometros Totales no es valido" };
                        }

                        //var val = new RegistroKilometrajeSvc().getUltimoRegistroValido(((UnidadTransporte)txtTractor.Tag).clave);
                        //if (val.typeResult == ResultTypes.success)
                        //{
                        //    if ((Convert.ToDecimal(txtKm.Text.Trim()) - ((RegistroKilometraje)val.result).kmFinal) >= 10000)
                        //    {
                        //        return new OperationResult { valor = 3, mensaje = "El kilometraje Total no es Valido, ecxede el valor maximo permitido" };

                        //    }
                        //    if (((RegistroKilometraje)val.result).kmFinal > Convert.ToDecimal(txtKm.Text.Trim()))
                        //    {
                        //        return new OperationResult { valor = 3, mensaje = "El kilometraje Total no es Valido por que es menor al ultimo KM" };

                        //    }
                        //}
                        OperationResult respKm = ultimoKm;
                        if (respKm.typeResult == ResultTypes.success)
                        {
                            decimal km = (decimal)respKm.result;
                            if (valor < km)
                            {
                                return new OperationResult { valor = 3, mensaje = "El kilometraje Total no es Valido por que es menor al ultimo KM" };
                            }
                            if (valor > (km + 3000))
                            {
                                return new OperationResult { valor = 3, mensaje = "El kilometraje Total no es Valido, ecxede el valor maximo permitido" };
                            }
                        }
                    }
                    else
                    {
                        return new OperationResult { valor = 3, mensaje = "Todos los Valores son obligatorios" };
                    }

                    if (txtDespachador.Tag == null)
                    {
                        return new OperationResult { valor = 3, mensaje = "No se ha seleccionado el despachador" };
                    }

                    int a = 0;
                    if (!int.TryParse(txtTanqueIzq.Text.Trim(), out a))
                    {
                        return new OperationResult { valor = 3, mensaje = "El sello del tanque Izq no es valido" };
                    }

                    if (!int.TryParse(txtTanqueDer.Text.Trim(), out a))
                    {
                        return new OperationResult { valor = 3, mensaje = "El sello del tanque Der no es valido" };
                    }

                    if (!string.IsNullOrEmpty(txtTanqueRes.Text.Trim()))
                    {

                        if (!int.TryParse(txtTanqueRes.Text.Trim(), out a))
                        {
                            return new OperationResult { valor = 3, mensaje = "El sello del tanque de reserva no es valido" };
                        }
                    }
                }
                else
                {
                    if (txtDespachador.Tag == null)
                    {
                        return new OperationResult { valor = 3, mensaje = "No se ha seleccionado el despachador" };
                    }
                    decimal valor = 0m;
                    if (!string.IsNullOrEmpty(txtCombustibleCargados.Text.Trim()))
                    {
                        if (!decimal.TryParse(txtCombustibleCargados.Text.Trim(), out valor))
                        {
                            return new OperationResult { valor = 3, mensaje = "El valor proporcionado para el combustible EST no es valido" };
                        }
                    }
                    else
                    {
                        return new OperationResult { valor = 3, mensaje = "Todos los Valores son obligatorios" };
                    }

                }
                if (chcExterno.IsChecked.Value)
                {                    
                    if (string.IsNullOrEmpty(txtTiketCarga.Text.Trim()))
                    {
                        return new OperationResult { valor = 3, mensaje = "Se requiere proporcionar el ticket de la carga Externa" };
                    }
                    if (txtProveedorCarga.SelectedItem == null)
                    {
                        return new OperationResult { valor = 3, mensaje = "Se requiere seleccionar el proveedor de la carga Externa" };
                    }
                    if (txtPrecioCarga.valor < 1)
                    {
                        return new OperationResult { valor = 3, mensaje = "Se requiere Proporcionar el precio de la carga Externa" };
                    }
                    if (string.IsNullOrEmpty(txtFolioImpresoCarga.Text.Trim()))
                    {
                        return new OperationResult { valor = 3, mensaje = "Se requiere proporcionar el Folio Impreso de la carga Externa" };
                    }
                }

                //if (llenarLista)
                //{
                //    if (lvlCP.Items.Count<=0)
                //    {
                //        return new OperationResult { valor = 3, mensaje = "Para este caso se necesitan capturar los Folios de los viajes" };
                //    }
                //}

                return new OperationResult { valor = 0 };
            }
            catch (Exception E)
            {
                return new OperationResult { mensaje = E.Message, valor = 1 };
            }
        }

        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                var val = validar();
                if (val.typeResult != ResultTypes.success)
                {
                    return val;
                }
                CargaCombustible cargaGas = mapForm();
                if (cargaGas != null)
                {
                    confirmacionCargaCombustible confirmacion = new confirmacionCargaCombustible(cargaGas);
                    var res = confirmacion.ShowDialog();
                    if (res.Value)
                    {
                        KardexCombustible kardex = mapKardex();

                        if (cargaGas.isExterno && kardex == null)
                        {
                            return new OperationResult(ResultTypes.warning, "Ocurrio un error al leer información de la pantalla");
                        }

                        OperationResult resp = new OperacionSvc().saveCargaGasolina(ref cargaGas, ((UnidadTransporte)txtTractor.Tag).empresa.clave, kardex);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            txtFolio.Tag = (CargaCombustible)resp.result;
                            ReportView REPORT = new ReportView();
                            REPORT.createValeGasolinaNuevo((CargaCombustible)txtFolio.Tag);
                        }
                        return resp;
                    }
                    else
                    {
                        return new OperationResult { valor = 2, mensaje = "REVISAR LOS DATOS PROPORCIONADOS" };
                    }
                }
                else
                {
                    return new OperationResult { mensaje = "Error en la pantalla", valor = 1 };
                }
            }
            catch (Exception e)
            {
                return new OperationResult { mensaje = e.Message, valor = 1 };
            }
            finally
            {
                Cursor = Cursors.Arrow;
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        private KardexCombustible mapKardex()
        {
            try
            {
                KardexCombustible kardexCombustible = new KardexCombustible
                {
                    idInventarioDiesel = 0,
                    cantidad = Convert.ToDecimal(txtCombustibleCargados.Text),
                    empresa = (txtFolio.Tag as CargaCombustible).masterOrdServ.tractor.empresa,
                    idOperacion = 0,
                    tipoMovimiento = eTipoMovimiento.SALIDA,
                    tipoOperacion = TipoOperacionDiesel.CARGA,
                    unidadTransporte = (txtFolio.Tag as CargaCombustible).masterOrdServ.tractor,
                    tanque = (txtClave.Tag as NivelesTanque).tanqueCombustible,
                    personal = (txtDespachador.Tag as Personal)                    
                };
                return kardexCombustible;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private CargaCombustible mapForm()
        {
            try
            {
                CargaCombustible entSal = (CargaCombustible)txtFolio.Tag;

                entSal.ltCombustibleUtilizado = string.IsNullOrEmpty(txtCombustibleUtilizado.Text.Trim()) ? 0 : Convert.ToDecimal(txtCombustibleUtilizado.Text.Trim());
                entSal.ltCombustiblePTO = string.IsNullOrEmpty(txtCombustiblePTO.Text.Trim()) ? 0 : Convert.ToDecimal(txtCombustiblePTO.Text.Trim());
                entSal.ltCombustibleEST = string.IsNullOrEmpty(txtCombustibleEST.Text.Trim()) ? 0 : Convert.ToDecimal(txtCombustibleEST.Text.Trim());
                entSal.ltCombustible = Convert.ToDecimal(txtCombustibleCargados.Text.Trim());

                entSal.tiempoTotal = txtTiempoTotal.Text;
                entSal.tiempoPTO = txtTiempoPTO.Text;
                entSal.tiempoEST = txtTiempoEst.Text;


                entSal.kmDespachador = string.IsNullOrEmpty(txtKm.Text.Trim()) ? 0 : Convert.ToDecimal(txtKm.Text.Trim());
                entSal.fechaCombustible = DateTime.Now;
                entSal.kmRecorridos = string.IsNullOrEmpty(txtKmRecorridos.Text.Trim()) ? 0 : Convert.ToDecimal(txtKmRecorridos.Text.Trim());

                if (entSal.kmRecorridos <= 0)
                {
                    decimal num = 0;
                    if (decimal.TryParse(txtUltKM.Text, out num))
                    {
                        if (num > 0)
                        {
                            entSal.kmRecorridos = entSal.kmDespachador - num;
                        }
                    }
                }

                entSal.despachador = (Personal)txtDespachador.Tag;
                entSal.sello1 = string.IsNullOrEmpty(txtTanqueIzq.Text.Trim()) ? 0 : Convert.ToInt32(txtTanqueIzq.Text.Trim());
                entSal.sello2 = string.IsNullOrEmpty(txtTanqueDer.Text.Trim()) ? 0 : Convert.ToInt32(txtTanqueDer.Text.Trim());
                int? f = null;
                entSal.selloReserva = string.IsNullOrEmpty(txtTanqueRes.Text.Trim()) ? f : Convert.ToInt32(txtTanqueRes.Text.Trim());

                entSal.listCP = new List<CartaPorte>();
                foreach (ListViewItem item in lvlCP.Items)
                {
                    entSal.listCP.Add((CartaPorte)item.Content);
                }
                entSal.usuarioCrea = mainWindow.inicio._usuario.nombreUsuario;
                entSal.listCombustibleExtra = listCargasExtra;

                if (chcExtra.IsChecked.Value)
                {
                    entSal.ticketExtra = txtTicket.Text.Trim();
                    entSal.proveedorExtra = txtProveedor.SelectedItem as ProveedorCombustible;
                    entSal.proveedorExtra.precio = txtPrecio.valor;
                    entSal.ltsExtra = txtLitros.valor;
                    entSal.folioImpresoExtra = txtFolioImpreso.Text.Trim();
                }
                else
                {
                    entSal.ticketExtra = string.Empty;
                    entSal.proveedorExtra = null;
                    //entSal.proveedorExtra.precio = 0;
                    entSal.ltsExtra = 0;
                    entSal.folioImpresoExtra = string.Empty;
                }
                if (chcExterno.IsChecked.Value)
                {
                    entSal.isExterno = true;
                    entSal.ticket = txtTiketCarga.Text;
                    entSal.proveedor = (ProveedorCombustible)txtProveedorCarga.SelectedItem;
                    entSal.precioCombustible = txtPrecioCarga.valor;
                    entSal.folioImpreso = txtFolioImpresoCarga.Text;
                    entSal.observacioses = txtObservaciones.Text;
                }

                return entSal;
            }
            catch (Exception)
            {
                return null;
            }
        }
        private void imprimirMensaje(OperationResult resp)
        {
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    break;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
                case ResultTypes.recordNotFound:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    break;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    break;
                default:
                    break;
            }
        }
        private void txtFolio_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                int i = 0;
                if (int.TryParse(txtFolio.Text.Trim(), out i))
                {
                    var respOS = new OperacionSvc().getOrdenServById(i);
                    return;
                    var resp = new OperacionSvc().getSalidasEntradasByIdOrdenServ(Convert.ToInt32(txtFolio.Text.Trim()));
                    if (resp.typeResult == ResultTypes.success)
                    {
                        CargaCombustible carga = (CargaCombustible)resp.result;
                        mapForm(carga);
                        //if (!carga.isExterno)
                        //{
                        if (((CargaCombustible)resp.result).fechaCombustible != null)
                        {
                            mainWindow.scrollContainer.IsEnabled = false;
                            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo);
                        }
                        //}
                    }
                    else
                    {
                        imprimirMensaje(resp);
                    }
                }
                else
                {
                    MessageBox.Show("El folio de la orden de servicio no es valido", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
        bool llenarLista = false;
        UnidadTransporte unidad = new UnidadTransporte();
        private void mapForm(CargaCombustible result)
        {
            if (result != null)
            {
                txtFolio.Tag = result;
                txtFolio.IsEnabled = false;
                mapOperador(result.masterOrdServ.chofer);

                foreach (var item in result.masterOrdServ.listTiposOrdenServicio)
                {
                    if (item.idOrdenSer == result.idOrdenServ)
                    {
                        unidad = item.unidadTrans;
                    }
                }
                if (unidad.tipoUnidad.descripcion.Contains("TRACTO"))
                {
                    var resp = new CargaCombustibleSvc().findUltimaCargaByIdUnidad(unidad.clave);
                    if (resp.typeResult == ResultTypes.recordNotFound)
                    {
                        if (result.fechaCombustible == null)
                        {
                            cbxCP.IsEnabled = true;
                            llenarLista = true;
                        }
                        else
                        {
                            //cbxCP.IsEnabled = false;
                            llenarLista = false;
                        }
                    }
                }
                else
                {
                    //cbxCP.IsEnabled = false;
                    llenarLista = false;
                }

                mapList(result.listCP);
                abilitarControles(unidad.tipoUnidad.clasificacion.ToUpper() == "TRACTOR");
                mapUnidades(TipoUnidad.TRACTOR, unidad);

                txtCombustibleUtilizado.Text = result.ltCombustibleUtilizado <= 0 ? "" : result.ltCombustibleUtilizado.ToString();
                txtCombustiblePTO.Text = result.ltCombustiblePTO <= 0 ? "" : result.ltCombustiblePTO.ToString();
                txtCombustibleEST.Text = result.ltCombustibleEST <= 0 ? "" : result.ltCombustibleEST.ToString();
                txtCombustibleCargados.Text = result.ltCombustible <= 0 ? "" : result.ltCombustible.ToString();

                txtTiempoTotal.Text = result.tiempoTotal;
                txtTiempoPTO.Text = result.tiempoPTO;
                txtTiempoEst.Text = result.tiempoEST;

                txtKmRecorridos.Text = result.kmRecorridos <= 0 ? "0" : result.kmRecorridos.ToString("N2");
                txtKm.Text = result.kmDespachador <= 0 ? "0" : result.kmDespachador.ToString("N2");

                mapDespachador(result.despachador);
                txtTanqueIzq.Text = result.sello1 <= 0 ? "" : result.sello1.ToString();
                txtTanqueDer.Text = result.sello2 <= 0 ? "" : result.sello2.ToString();
                txtTanqueRes.Text = result.selloReserva <= 0 ? "" : result.selloReserva.ToString();
                txtFecha.Text = result.masterOrdServ.fechaCreacion.ToString();


                ListViewItem lvl = new ListViewItem();
                lvl.Foreground = Brushes.SteelBlue;
                lvl.Content = result;
                lvlCarga.Items.Add(lvl);

                if (result.fechaCombustible == null)
                {

                    if (!ReglasUbicacion.validar(EstatusUbicacion.GASOLINERA, unidad))
                    {
                        nuevo();
                        return;
                    }

                    if (ultimoKm.typeResult == ResultTypes.success || ultimoKm.typeResult == ResultTypes.recordNotFound)
                    {
                        txtUltKM.Text = ultimoKm.result.ToString();
                    }
                    else
                    {
                        txtUltKM.Text = "0";
                    }
                }
                else
                {
                    var resp = new RegistroKilometrajeSvc().getUltimoKM(unidad.clave, Convert.ToDateTime(result.fechaCombustible));
                    if (resp.typeResult == ResultTypes.success)
                    {
                        txtUltKM.Text = ((decimal)resp.result).ToString("N2");
                    }
                    else
                    {
                        txtUltKM.Text = "";
                    }
                }
                txtDespachador.Focus();

                if (result.proveedorExtra != null)
                {
                    expCargaExtra.IsExpanded = true;
                    chcExtra.IsChecked = true;
                    int i = 0;
                    foreach (ProveedorCombustible item in txtProveedor.Items)
                    {
                        if (item.idProveedor == result.proveedorExtra.idProveedor)
                        {
                            txtProveedor.SelectedIndex = i;
                            break;
                        }
                        i++;
                    }                    

                    
                    txtPrecio.valor = result.proveedorExtra.precio;
                    txtLitros.valor = result.ltsExtra;
                    txtTicket.Text = result.ticketExtra;
                    txtFolioImpreso.Text = result.folioImpresoExtra;

                }

                chcExterno.IsChecked = result.isExterno;
                if (result.isExterno)
                {
                    expExterno.IsExpanded = true;
                    int i = 0;
                    foreach (ProveedorCombustible item in txtProveedorCarga.Items)
                    {
                        if (item.idProveedor == result.proveedor.idProveedor)
                        {
                            txtProveedorCarga.SelectedIndex = i;
                            break;
                        }
                        i++;
                    }
                    txtTiketCarga.Text = result.ticket;
                    txtPrecioCarga.valor = result.precioCombustible;
                    txtFolioImpresoCarga.Text = result.folioImpreso;
                    txtObservaciones.Text = result.observacioses;
                }
                txtPrecioCombustible.valor = result.precioCombustible <= 0 ? valorDisel : result.precioCombustible;
            }
            else
            {
                txtFolio.IsEnabled = true;
                mapOperador(null);
                mapUnidades(TipoUnidad.TRACTOR, null);
                unidad = null;
                txtCombustibleUtilizado.Clear();
                txtCombustiblePTO.Clear();
                txtCombustibleEST.Clear();
                txtCombustibleCargados.Clear();
                abilitarControles(true);
                txtTiempoTotal.Clear();
                txtTiempoPTO.Clear();
                txtTiempoEst.Clear();

                txtKmRecorridos.Clear();
                txtKm.Clear();
                mapDespachador(null);
                txtTanqueIzq.Clear();
                txtTanqueDer.Clear();
                txtTanqueRes.Clear();
                txtFecha.Clear();
                txtFolio.Clear();
                txtFolio.Focus();
                //cbxCP.IsEnabled = false;
                lvlCP.Items.Clear();
                llenarLista = false;
                lvlValesExtra.Items.Clear();
                limpiarExtra(false);
                lvlCarga.Items.Clear();
                txtProveedorCarga.SelectedItem = null;
                txtFolioImpresoCarga.Clear();
                txtObservaciones.Clear();
                chcExterno.IsChecked = false;
                txtUltKM.Clear();
            }
        }

        private void abilitarControles(bool v)
        {
            cbxLts.IsEnabled = v;
            cbxSellos.IsEnabled = v;
            cbxTiempos.IsEnabled = v;
            cbxKMs.IsEnabled = v;
        }

        private void txtDespachador_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string clave = txtDespachador.Text.Trim();
                //completarClave(ref clave);
                List<Personal> listPersonal = new List<Personal>();
                if (string.IsNullOrEmpty(clave))
                {
                    listPersonal = buscarDespachador();
                }
                else
                {
                    listPersonal = buscarDespachador(clave);
                }

                if (listPersonal.Count == 1)
                {
                    mapDespachador(listPersonal[0]);
                }
                else if (listPersonal.Count == 0)
                {
                    selectPersonal buscardor = new selectPersonal(clave, 0);
                    var per = buscardor.buscarAllDespachador();
                    mapDespachador(per);
                }
                else
                {
                    selectPersonal buscardor = new selectPersonal(clave, 0);
                    var per = buscardor.buscarAllDespachador();
                    mapDespachador(per);
                }
            }
        }

        private void mapDespachador(Personal personal)
        {
            if (personal != null)
            {
                txtDespachador.Tag = personal;
                txtDespachador.Text = personal.nombre;
                txtDespachador.IsEnabled = false;

                if (unidad.tipoUnidad.clasificacion.ToUpper() == "TRACTOR")
                {
                    txtKm.Focus();
                }
                else
                {
                    txtCombustibleCargados.Focus();
                }
            }
            else
            {
                txtDespachador.Tag = null;
                txtDespachador.Text = "";
                txtDespachador.IsEnabled = true;
            }
        }

        private void txtKmRecorridos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrEmpty(txtKmRecorridos.Text.Trim()))
                {
                    txtCombustibleUtilizado.Focus();
                }
            }
        }

        private void txtCombustibleUtilizado_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrEmpty(txtCombustibleUtilizado.Text.Trim()))
                {
                    txtCombustiblePTO.Focus();
                }
            }
        }

        private void txtCombustiblePTO_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrEmpty(txtCombustiblePTO.Text.Trim()))
                {
                    txtCombustibleEST.Focus();
                }
            }
        }

        private void txtCombustibleEST_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrEmpty(txtCombustibleEST.Text.Trim()))
                {
                    txtCombustibleCargados.Focus();
                }
            }
        }

        private void txtCombustibleCargados_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrEmpty(txtCombustibleCargados.Text.Trim()))
                {
                    txtTiempoTotal.Focus();
                }
            }
        }

        void actualizarLitrosCargados()
        {
            decimal val = 0;
            decimal lts = 0;
            if (decimal.TryParse(txtCombustibleCargados.Text.Trim(), out lts))
            {
                txtSumaLitros.valor = (lts + txtLitros.valor);
            }
            else
            {
                txtSumaLitros.valor = (txtLitros.valor);
            }
            decimal.TryParse(txtCombustibleUtilizado.Text.Trim(), out val);
            txtRelleno.valor = txtSumaLitros.valor - val;

        }

        private void txtTiempoTotal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrEmpty(txtTiempoTotal.Text.Trim()))
                {
                    txtTiempoPTO.Focus();
                }
            }
        }

        private void txtTiempoPTO_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrEmpty(txtTiempoPTO.Text.Trim()))
                {
                    txtTiempoEst.Focus();
                }
            }
        }

        private void txtTiempoEst_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrEmpty(txtTiempoEst.Text.Trim()))
                {
                    txtTanqueIzq.Focus();
                }
            }
        }

        private void txtTanqueIzq_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrEmpty(txtTanqueIzq.Text.Trim()))
                {
                    txtTanqueDer.Focus();
                }
            }
        }

        private void txtTanqueDer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrEmpty(txtTanqueDer.Text.Trim()))
                {
                    txtTanqueRes.Focus();
                }
            }
        }




        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ReportView REPORT = new ReportView();
            REPORT.createValeGasolina((CargaCombustible)txtFolio.Tag);
            REPORT.ShowDialog();
        }

        private void txtKm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtKmRecorridos.Focus();
            }
        }

        private void txtFolioViaje_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    var resp = new CartaPorteSvc().getCartaPorteById(Convert.ToInt32(txtFolioViaje.Text.Trim()));
                    if (resp.typeResult == ResultTypes.success)
                    {
                        if (((List<Viaje>)resp.result)[0].tractor.clave == ((UnidadTransporte)txtTractor.Tag).clave)
                        {
                            mapList(((List<Viaje>)resp.result)[0].listDetalles);
                        }
                        else
                        {
                            imprimirMensaje(new OperationResult { valor = 2, mensaje = "Este viaje no corresponde al tractor seleccionado" });
                        }
                    }
                    else
                    {
                        imprimirMensaje(resp);
                    }

                    resp = new CartaPorteSvc().getCartaPorteById(Convert.ToInt32(txtFolioViaje.Text.Trim()));
                    if (resp.typeResult == ResultTypes.success)
                    {
                        resp = new CartaPorteSvc().getValesCargaByIdViaje(((List<Viaje>)resp.result)[0]);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            addValesCarga((List<ValeCarga>)resp.result);
                        }
                        else if (resp.typeResult != ResultTypes.recordNotFound)
                        {
                            imprimirMensaje(resp);
                        }
                    }


                    txtFolioViaje.Clear();
                }
            }
            catch (Exception ex)
            {
                imprimirMensaje(new OperationResult { valor = 1, mensaje = ex.Message });
            }
            finally
            {

            }
        }

        private void addValesCarga(List<ValeCarga> result)
        {
            try
            {
                //lvlValesExtra.Items.Clear();
                foreach (ValeCarga item in result)
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = item;
                    lvlVales.Items.Add(lvl);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void mapList(List<CartaPorte> listDetalles)
        {
            foreach (var item in listDetalles)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvlCP.Items.Add(lvl);
            }
        }

        private void txtValeExtra_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (e.Key == Key.Enter)
                {
                    int i = 0;
                    if (int.TryParse(txtValeExtra.Text.Trim(), out i))
                    {
                        OperationResult resp = new CargaCombustibleSvc().getCargaCombustibleExtra(i);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            llenarTabla((CargaCombustibleExtra)resp.result);
                        }
                        else
                        {
                            ImprimirMensaje.imprimir(resp);
                        }
                    }
                    txtValeExtra.Clear();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private List<CargaCombustibleExtra> listCargasExtra
        {
            get
            {
                List<CargaCombustibleExtra> l = new List<CargaCombustibleExtra>();
                foreach (ListViewItem item in lvlValesExtra.Items)
                {
                    l.Add((CargaCombustibleExtra)item.Content);
                }
                return l;
            }
        }

        private void llenarTabla(CargaCombustibleExtra result)
        {
            if (result.fechaCombustible == null)
            {
                ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "ESTE ACTICIPO NO ESTA FINALIZADO" });
                return;
            }

            MasterOrdServ master = ((CargaCombustible)txtFolio.Tag).masterOrdServ;
            if (result.tipoOrdenCombustible.tipoUnidad.ToUpper() == "TC")
            {
                if (master.tractor.clave != result.unidadTransporte.clave)
                {
                    ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "ESTE ACTICIPO NO CORRESPONDE PARA EL TRACTOR " + master.tractor.clave });
                    return;
                }
            }

            if (result.tipoOrdenCombustible.tipoUnidad.ToUpper() == "RE")
            {
                if (master.tolva2 == null)
                {
                    if (!(master.tolva1.clave == result.unidadTransporte.clave))
                    {
                        ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "ESTE ACTICIPO NO CORRESPONDE PARA EL REMOLQUE " + master.tolva1.clave });
                        return;
                    }
                }
                else
                {
                    if (!(master.tolva1.clave == result.unidadTransporte.clave || master.tolva2.clave == result.unidadTransporte.clave))
                    {
                        ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "ESTE ACTICIPO NO CORRESPONDE PARA NINGUNO DE LOS REMOLQUES " + master.tolva1.clave + "/" + master.tolva2.clave });
                        return;
                    }
                }
            }

            if (result.idCargaCombustible != null || result.idCargaCombustible > 0)
            {
                ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "ESTE ACTICIPO YA FUE UTILIZADO PARA EL VALE DE COMBUSTIBLE " + result.idCargaCombustible.ToString() });
                return;
            }

            if (listCargasExtra.Exists(element => element.idCargaCombustibleExtra == result.idCargaCombustibleExtra))
            {
                return;
            }

            ListViewItem lvl = new ListViewItem();
            lvl.Content = result;
            lvlValesExtra.Items.Add(lvl);
        }

        private void txtProveedor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (txtProveedor.SelectedItem != null)
            {
                txtPrecio.valor = ((ProveedorCombustible)txtProveedor.SelectedItem).precio;
            }
            else
            {
                txtPrecio.valor = 0;
            }
        }

        private void txtCombustibleCargados_TextChanged(object sender, TextChangedEventArgs e)
        {
            actualizarLitrosCargados();
        }

        private void chcExtra_Unchecked(object sender, RoutedEventArgs e)
        {
            limpiarExtra(false);
        }

        private void chcExtra_Checked(object sender, RoutedEventArgs e)
        {
            limpiarExtra(true);
        }
        void limpiarExtra(bool valor)
        {
            gboxExtra.IsEnabled = valor;
            if (valor)
            {

            }
            else
            {
                txtTicket.Clear();
                txtProveedor.SelectedItem = null;
                txtLitros.valor = 0;
                txtPrecio.valor = 0;
                txtFolioImpreso.Clear();
            }
        }

        private void txtProveedorCarga_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (txtProveedorCarga.SelectedItem != null)
            {
                txtPrecioCarga.valor = ((ProveedorCombustible)txtProveedorCarga.SelectedItem).precio;
            }
            else
            {
                txtPrecioCarga.valor = 0;
            }
        }
    }
}
