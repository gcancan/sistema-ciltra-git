﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using CoreFletera.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Markup;

    public partial class editPreOCDirectas : WpfClient.ViewBase, IComponentConnector
    {
        public editPreOCDirectas()
        {
            this.InitializeComponent();
        }      

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            this.buscarProducto();
        }

        private void buscarProducto()
        {
            List<ProductosCOM> list = new selectProductosCOM(this.txtInsumo.Text.Trim()).consultasTallerSinExistencias(AreaTrabajo.TOTAS);
            foreach (ProductosCOM scom in list)
            {
                decimal cantidad = scom.cantidad;
                scom.existencia = decimal.Zero;
                if (!this.existeLista(scom))
                {
                    this.mapListaProducto(scom);
                }
            }
            this.txtInsumo.Clear();
        }

        private bool existeLista(ProductosCOM item) =>
            (this.listaProductos.Find(s => s.idProducto == item.idProducto) != null);

        public override OperationResult guardar()
        {
            OperationResult result2;
            try
            {
                base.Cursor = Cursors.Wait;
                if (this.listaProductos.Count <= 0)
                {
                    return new OperationResult(ResultTypes.error, "No hay datos para guardar", null);
                }
                PreOrdenCompra preOrdenCompra = this.mapForm();
                OperationResult result = new OperacionSvc().savePreOrdenCompraDirecta(ref preOrdenCompra);
                if (result.typeResult == ResultTypes.success)
                {
                    this.txtFolio.valor = preOrdenCompra.idPreOrdenCompra;
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                }
                result2 = result;
            }
            catch (Exception exception)
            {
                result2 = new OperationResult(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return result2;
        }
        private PreOrdenCompra mapForm()
        {
            try
            {
                PreOrdenCompra compra = new PreOrdenCompra
                {
                    estatus = "PRE",
                    fechaPreOrden = DateTime.Now,
                    usuario = base.mainWindow.usuario.nombreUsuario
                };
                compra.listaDetalles = new List<DetallePreOrdenCompra>();
                foreach (ProductosCOM scom in this.listaProductos)
                {
                    DetallePreOrdenCompra item = new DetallePreOrdenCompra
                    {
                        producto = scom,
                        cantidad = scom.cantPreOC,
                        cantidadSolicitada = scom.cantPreOC
                    };
                    compra.listaDetalles.Add(item);
                }
                return compra;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void mapListaProducto(ProductosCOM producto)
        {
            try
            {
                Button element = new Button
                {
                    HorizontalContentAlignment = HorizontalAlignment.Left,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(1.0),
                    Background = null,
                    Tag = producto
                };
                StackPanel panel = new StackPanel
                {
                    Orientation = Orientation.Horizontal,
                    Tag = producto
                };
                TextBox box = new TextBox
                {
                    Text = producto.nombre,
                    Width = 250.0,
                    TextWrapping = TextWrapping.Wrap,
                    IsReadOnly = true,
                    BorderBrush = null,
                    Background = null,
                    Focusable = false,
                    IsEnabled = false,
                    Margin = new Thickness(1.0)
                };
                panel.Children.Add(box);
                controlDecimal num = new controlDecimal
                {
                    valor = producto.cantidad,
                    Width = 100.0,
                    Margin = new Thickness(1.0),
                    Tag = producto
                };
                num.txtDecimal.Tag = num;
                num.txtDecimal.TextChanged += new TextChangedEventHandler(this.TxtDecimal_CantidadProducto);
                panel.Children.Add(num);
                element.Content = panel;
                this.stpInsumos.Children.Add(element);
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
            this.stpInsumos.Children.Clear();
            this.txtFolio.limpiar();
        }
        private void TxtDecimal_CantidadProducto(object sender, TextChangedEventArgs e)
        {
            controlDecimal tag = (sender as TextBox).Tag as controlDecimal;
            ProductosCOM scom = tag.Tag as ProductosCOM;
            scom.existencia = decimal.Zero;
            scom.cantidad = tag.valor;
        }

        private void txtInsumo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.buscarProducto();
            }
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.nuevo();
        }

        private List<ProductosCOM> listaProductos
        {
            get
            {
                List<ProductosCOM> list = new List<ProductosCOM>();
                List<Button> list2 = this.stpInsumos.Children.Cast<Button>().ToList<Button>();
                foreach (Button button in list2)
                {
                    list.Add(button.Tag as ProductosCOM);
                }
                return list;
            }
        }
    }
}
