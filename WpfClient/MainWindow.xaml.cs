﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using Core.Utils;
using Core.Interfaces;
//using System.Net.Http;
//using System.Net.Http.Headers;
using System.Web.Script.Serialization;
//using menu = Fletera.Com;
using time = System.Timers;
using MahApps.Metro.Controls;
using CoreFletera.Models;
using CoreFletera.Interfaces;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        private IView pantalla { get; set; }
        public IView pant { get; set; }
        private bool isNewWindow { get; set; }
        Dictionary<string, Window> _childs;
        private bool isDialog { get; set; }
        int _winId;
        public Inicio inicio;
        public Empresa empresa;
        public Personal personal;
        public Cliente cliente;
        public Usuario usuario;
        public ZonaOperativa zonaOperativa;
        public string nombreEquipo = string.Empty;
        public MainWindow()
        {
            InitializeComponent();
        }
        public MainWindow(MainWindow mainWindow)
        {
            
            this.mainWindow = mainWindow;
            this.inicio = mainWindow.inicio;
            this.empresa = this.inicio._usuario.empresa;
            this.personal = this.inicio._usuario.personal;
            this.cliente = this.inicio._usuario.cliente;
            this.zonaOperativa = this.inicio._usuario.zonaOperativa;
            this.usuario = this.inicio._usuario;
            this.nombreEquipo = inicio.nombreEquipo;
            InitializeComponent();

        }
        public MainWindow(Inicio inicio)
        {
            
            this.inicio = inicio;
            this.empresa = inicio._usuario.empresa;
            this.personal = inicio._usuario.personal;
            this.cliente = inicio._usuario.cliente;
            this.zonaOperativa = inicio._usuario.zonaOperativa;
            this.usuario = inicio._usuario;
            this.nombreEquipo = inicio.nombreEquipo;
            InitializeComponent();
        }
        public OperationResult abrirVentanaCargaExterno(int idOrden, Viaje viaje)
        {
            try
            {
                mainMenu.Visibility = Visibility.Collapsed;
                var ventana = new editCombustibleExterto(idOrden, mainWindow, viaje);
                pantalla = ventana;
                var c = empresa;
                scrollContainer.Content = pantalla;
                var dResult = this.ShowDialog();

                if (dResult.Value)
                {
                    return ventana.result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        public OperationResult abrirVentanaLiquidacion(LiquidacionPegaso liquidacion)
        {
            try
            {
                mainMenu.Visibility = Visibility.Collapsed;
                var ventana = new editLiquidacionPegaso(liquidacion, mainWindow);
                pantalla = ventana;
                var c = empresa;
                scrollContainer.Content = pantalla;
                var dResult = this.ShowDialog();

                if (dResult.Value)
                {
                    return ventana.result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        private void mainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            txtFolioViaje.txtEntero.KeyDown += TxtEntero_KeyDown;
            var path = System.Windows.Forms.Application.StartupPath;

            string archivo = path + @"\config.ini";
            txtNomBD.Content = new Util().Read("CONECTION_STRING", "DB_NAME", archivo);
            //txtNomBDComercial.Text = new Util().Read("CONECTION_STRING_COMERCIAL", "DB_NAME", archivo);
            //txtCuentaCorreo.Text = new Util().Read("CONFIGURACION_CORREO", "USUARIO", archivo);
            enableToolBarCommands(ToolbarCommands.none);
            this.Title = inicio._usuario.personal.empresa.nombre.ToUpper();
            var mi1 = new MenuItem { Header = "Sistema" };
            mi1.FontWeight = FontWeights.Medium;
            iniciar();
            mainMenu.Items.Add(mi1);

            var mi2 = new MenuItem();

            mi2 = new MenuItem { Header = "Cerrar sistema" };
            mi2.FontWeight = FontWeights.Medium;
            mi2.Click += miCerrarSistema_Click;

            mi1.Items.Add(mi2);

            //var cat = new MenuItem { Header = "Nuevo Cat" };
            //cat.Click += Cat_Click;
            //mi1.Items.Add(cat);
            List<MenusAplicacion> menus = new List<MenusAplicacion>();
            //using (var client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri("http://localhost:56543/");
            //    client.DefaultRequestHeaders.Accept.Clear();
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //    // New code:

            //    HttpResponseMessage response = await client.GetAsync("api/UnidadTransporte/buscarMen/");
            //    if (response.IsSuccessStatusCode)
            //    {
            //        string product = await response.Content.ReadAsStringAsync();
            //        //var s = new Newtonsoft.Json.JsonConverterJsonConvert();
            //        OperationResult resp = Newtonsoft.Json.JsonConvert.DeserializeObject<OperationResult>(product);
            //        menus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MenusAplicacion>>(resp.result.ToString());

            //        if (resp.typeResult == ResultTypes.error)
            //        {
            //            MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //            return;
            //        }
            //        if (resp.typeResult == ResultTypes.recordNotFound)
            //        {
            //            MessageBox.Show(string.Format(resp.mensaje), "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
            //            return;
            //        }
            //        if (resp.typeResult == ResultTypes.success)
            //        {
            //            foreach (MenusAplicacion ma in menus)
            //            {
            //                if (ma.padre == null)
            //                {
            //                    MenuItem mi = new MenuItem();

            //                    mi.Header = ma.nombre;
            //                    mi.Name = ma.clave;
            //                    mi.Tag = ma;

            //                    generateMenuRaiz(mi, menus);

            //                    mainMenu.Items.Add(mi);
            //                }

            //            }
            //        }


            //    }
            //}


            OperationResult resp = new MenusAplicacionSvc().getAllMenusAplicacionByIdUser(inicio._usuario.idUsuario);
            //OperationResult resp = Newtonsoft.Json.JsonConvert.DeserializeObject<OperationResult>(product);
            //menus = Newtonsoft.Json.JsonConvert.DeserializeObject<List<MenusAplicacion>>(resp.result.ToString());

            if (resp.typeResult == ResultTypes.error)
            {
                MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (resp.typeResult == ResultTypes.recordNotFound)
            {
                MessageBox.Show(string.Format(resp.mensaje), "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (resp.typeResult == ResultTypes.success)
            {
                menus = (List<MenusAplicacion>)resp.result;
                foreach (MenusAplicacion ma in menus)
                {
                    if (ma.padre == null)
                    {
                        MenuItem mi = new MenuItem();
                        mi.Header = ma.nombre;
                        mi.Name = ma.clave;
                        mi.Tag = ma;
                        mi.FontWeight = FontWeights.Medium;
                        generateMenuRaiz(mi, menus);
                        mainMenu.Items.Add(mi);

                        TabItem tab = new TabItem { Name = ma.clave, Header = ma.nombre, Tag = ma };
                        crearBotonesMenu(tab, menus);
                        tblControl.Items.Add(tab);
                    }
                }
            }
            lblUsuario.Content = inicio._usuario.nombreUsuario.ToUpper();
            lblEmpresa.Content = empresa.nombre.ToUpper();
            txtZonaOpetativa.Content = zonaOperativa == null ? string.Empty : zonaOperativa.nombre;
            OperationResult respPrivilegio = new PrivilegioSvc().consultarPrivilegio("BUSCADOR_RAPIDO_VIAJES", inicio._usuario.idUsuario);            
            if (respPrivilegio.typeResult == ResultTypes.success)
            {
                stpBuscadorViajes.Visibility = Visibility.Visible;
            }

            stpBaseDatos.Visibility = new PrivilegioSvc().consultarPrivilegio("VER_LEYENDA_BASE_DATOS", inicio._usuario.idUsuario).typeResult == ResultTypes.success ? Visibility.Visible : Visibility.Collapsed;
        }

        private void TxtEntero_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {                
                ventanaGeneral ventana = new ventanaGeneral(this);
                ventana.abrirInfoViajes(txtFolioViaje.valor);
                txtFolioViaje.limpiar();
            }
            
        }

        //public time.Timer timer = new time.Timer();
        int contador = 0;
        private void iniciar()
        {
            Dispatcher.Invoke((Action)(() =>
            {
                //timer = new time.Timer { Interval = 1000 };
                //timer.Elapsed += Timer_Elapsed;
                //timer.Start();
            }));
        }

        //private void Timer_Elapsed(object sender, time.ElapsedEventArgs e)
        //{
        //    Dispatcher.Invoke((Action)(() =>
        //    {
        //        if (contador >= 1)
        //        {
        //            timer.Stop();
        //            try
        //            {
        //                var conn = new Util().getConectionString();
        //                conn.ConnectTimeout = 15;
        //                System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(conn.ToString());

        //                con.Open();
        //                stpConectado.Visibility = Visibility.Visible;
        //                stpDesconectado.Visibility = Visibility.Collapsed;
        //            }
        //            catch (Exception ex)
        //            {
        //                stpConectado.Visibility = Visibility.Collapsed;
        //                stpDesconectado.Visibility = Visibility.Visible;
        //            }
        //            contador = 0;
        //            timer.Start();
        //        }
        //        else
        //        {
        //            contador++;
        //        }
        //    }));
        //}

        //private void Cat_Click(object sender, RoutedEventArgs e)
        //{
        //    var con = Core.Utils.BoundedContextFactory.ConnectionFactory().ConnectionString;
        //    var conComercial = Core.Utils.BoundedContextFactory.ConnectionFactory(true).ConnectionString;
        //    var conConfigl = Core.Utils.BoundedContextFactory.ConnectionFactoryCompacWAdmin().ConnectionString;
        //    var fac = new menu.fFacturaViajes(con, conComercial, conConfigl);
        //    fac.ShowDialog();
        //}

        private void miCerrarSistema_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void generateMenuRaiz(MenuItem raiz, List<MenusAplicacion> list)
        {
            foreach (MenusAplicacion ma in list)
            {
                if (ma.idPadre == ((MenusAplicacion)raiz.Tag).idMenuAplicacion)
                {
                    MenuItem mi = new MenuItem();

                    mi.Name = ma.clave;
                    mi.Header = ma.nombre;
                    mi.FontWeight = FontWeights.Medium;
                    mi.Tag = ma;

                    generateMenuRaiz(mi, list);

                    if (mi.Items.Count == 0)
                        mi.Click += menu_Click;

                    raiz.Items.Add(mi);
                }
            }
        }
        private void crearBotonesMenu(TabItem tab, List<MenusAplicacion> list)
        {
            MenusAplicacion menu = tab.Tag as MenusAplicacion;
            ScrollViewer scroll = new ScrollViewer();
            WrapPanel stack = new WrapPanel() { Orientation = Orientation.Horizontal };
            foreach (var item in list.FindAll(s=>s.idPadre == menu.idMenuAplicacion))
            {
                Button btnMenu = new Button { Content = item.nombre, Tag = item, Margin= new Thickness(2), Name = item.clave };
                btnMenu.Click += BtnMenu_Click;
                stack.Children.Add(btnMenu);
            }
            scroll.Content = stack;
            tab.Content = scroll;
        }

        private void BtnMenu_Click(object sender, RoutedEventArgs e)
        {
            if(pant != null)
            {
                if (Keyboard.IsKeyDown(Key.LeftShift) && pant.bandeja == false)
                {
                    try
                    {
                        MainWindow mw = new MainWindow(inicio);
                        mw.isNewWindow = true;
                        mw.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                        //mw.SizeToContent = SizeToContent.WidthAndHeight;
                        //mw.Owner = this;
                        //mw.datosSesion = datosSesion;
                        mw.createView(((Button)sender).Name);
                        if (mw.isWPF)
                        {
                            mw.Show();
                        }
                    }
                    catch (Exception)
                    {
                        createView(((Button)sender).Name);
                    }
                }
                else
                {

                    //if (isEditable)
                    //{
                    //    if (MessageBox.Show("¿Desea salir de esta pantalla sin guardar los cambios?", Title, MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                    //    {
                    //        return;
                    //    }
                    //}

                    //if (MessageBox.Show("¿Desea salir de esta pantalla sin guardar los cambios?", Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    //{
                    if (!Keyboard.IsKeyDown(Key.LeftShift))
                        createView(((Button)sender).Name);
                    else
                    {
                        try
                        {
                            MainWindow mw = new MainWindow(inicio);
                            mw.isNewWindow = true;
                            mw.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                            //mw.SizeToContent = SizeToContent.WidthAndHeight;

                            //mw.datosSesion = datosSesion;
                            mw.createView(((Button)sender).Name);
                            if (mw.isWPF)
                            {
                                mw.Show();
                            }
                        }
                        catch (Exception)
                        {
                            createView(((Button)sender).Name);
                        }
                    }
                    pant = null;
                    //} //fin del if keyboard.iskeydown
                }
            }
            else
            {
                if (!Keyboard.IsKeyDown(Key.LeftShift))
                    createView(((Button)sender).Name);
                else
                {
                    try
                    {
                        var mw = new MainWindow(inicio)
                        {
                            isNewWindow = true,
                            WindowStartupLocation = WindowStartupLocation.CenterScreen,
                            //SizeToContent = SizeToContent.WidthAndHeight,
                            //datosSesion = datosSesion
                        };
                        //mw.Owner = this;
                        mw.createView(((Button)sender).Name);
                        if (mw.isWPF)
                        {
                            mw.Show();
                        }

                    }
                    catch (Exception)
                    {
                        createView(((Button)sender).Name);
                    }
                }
            }
        }

        private void menu_Click(object sender, RoutedEventArgs e)
        {
            if (pant != null)
            {
                if (Keyboard.IsKeyDown(Key.LeftShift) && pant.bandeja == false)
                {
                    try
                    {
                        MainWindow mw = new MainWindow(inicio);
                        mw.isNewWindow = true;
                        mw.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                        //mw.SizeToContent = SizeToContent.WidthAndHeight;
                        //mw.Owner = this;
                        //mw.datosSesion = datosSesion;
                        mw.createView(((MenuItem)sender).Name);
                        if (mw.isWPF)
                        {
                            mw.Show();
                        }
                    }
                    catch (Exception)
                    {
                        createView(((MenuItem)sender).Name);
                    }
                }
                else
                {

                    //if (isEditable)
                    //{
                    //    if (MessageBox.Show("¿Desea salir de esta pantalla sin guardar los cambios?", Title, MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                    //    {
                    //        return;
                    //    }
                    //}

                    //if (MessageBox.Show("¿Desea salir de esta pantalla sin guardar los cambios?", Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                    //{
                    if (!Keyboard.IsKeyDown(Key.LeftShift))
                        createView(((MenuItem)sender).Name);
                    else
                    {
                        try
                        {
                            MainWindow mw = new MainWindow(inicio);
                            mw.isNewWindow = true;
                            mw.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                            //mw.SizeToContent = SizeToContent.WidthAndHeight;

                            //mw.datosSesion = datosSesion;
                            mw.createView(((MenuItem)sender).Name);
                            if (mw.isWPF)
                            {
                                mw.Show();
                            }
                        }
                        catch (Exception)
                        {
                            createView(((MenuItem)sender).Name);
                        }
                    }
                    pant = null;
                    //} //fin del if keyboard.iskeydown
                }
            }
            else
            {
                if (!Keyboard.IsKeyDown(Key.LeftShift))
                    createView(((MenuItem)sender).Name);
                else
                {
                    try
                    {
                        var mw = new MainWindow(inicio)
                        {
                            isNewWindow = true,
                            WindowStartupLocation = WindowStartupLocation.CenterScreen,
                            //SizeToContent = SizeToContent.WidthAndHeight,
                            //datosSesion = datosSesion
                        };
                        //mw.Owner = this;
                        mw.createView(((MenuItem)sender).Name);
                        if (mw.isWPF)
                        {
                            mw.Show();
                        }

                    }
                    catch (Exception)
                    {
                        createView(((MenuItem)sender).Name);
                    }
                }
            }
        }

        bool isWPF = false;
        public void createView(string clave)
        {
            Cursor = Cursors.Wait;
            scrollContainer.IsEnabled = true;
            IView view = ViewFactory.createView(clave, this);

            if (view == null)
            {
                Cursor = Cursors.Arrow;
                isWPF = false;
                return;
            }
            isWPF = true;
            view.mainWindow = this;
            //view.datosSesion = datosSesion;
            if (view.bandeja == false)
            {
                viewContainer vc = new viewContainer(view);
                scrollContainer.Content = vc;
                pantalla = view;
                pant = view;
                //
                //viewContainer.Children.Clear();
                //viewContainer.Children.Add((ViewBase)view);

                ((ViewBase)view).SetResourceReference(StyleProperty, "EditView");

                //enableToolBarCommands(ToolbarCommands.cerrar);
            }
            else
            {
                var vf = new viewFlotantes(view)
                {
                    Name = "winfloat" + _winId
                };
                _childs.Add(vf.Name, vf);
                vf.Closing += Vf_Closing; ;
                //vf.Owner = this;
                Cursor = Cursors.Arrow;
                vf.Show();
                _winId += 1;
            }
            Cursor = Cursors.Arrow;
        }

        private void Vf_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _childs.Remove(((Window)sender).Name);           
        }

        //public IView activeView
        //{
        //    get
        //    {
        //        if (viewContainer.Children.Count > 0)
        //            return (IView)viewContainer.Children[0];
        //        return null;
        //    }
        //}

        public void enableToolBarCommands(ToolbarCommands commands)
        {
            btnNuevo.Visibility = (commands & ToolbarCommands.nuevo) == ToolbarCommands.nuevo ? Visibility.Visible : Visibility.Collapsed;
            btnGuardar.Visibility = (commands & ToolbarCommands.guardar) == ToolbarCommands.guardar ? Visibility.Visible : Visibility.Collapsed;
            btnEliminar.Visibility = (commands & ToolbarCommands.eliminar) == ToolbarCommands.eliminar ? Visibility.Visible : Visibility.Collapsed;
            btnBuscar.Visibility = (commands & ToolbarCommands.buscar) == ToolbarCommands.buscar ? Visibility.Visible : Visibility.Collapsed;
            btnEditar.Visibility = (commands & ToolbarCommands.editar) == ToolbarCommands.editar ? Visibility.Visible : Visibility.Collapsed;
            cerrarButton.Visibility = (commands & ToolbarCommands.cerrar) == ToolbarCommands.cerrar ? Visibility.Visible : Visibility.Collapsed;

            //if (activeView != null)
            //cerrarButton.Visibility = Visibility.Visible;

            //if (activeView != null && isDialog)
            //{
            //    guardarButton.Visibility = Visibility.Visible;
            //    cerrarButton.Visibility = Visibility.Visible;
            //    nuevoButton.Visibility = Visibility.Collapsed;
            //    eliminarButton.Visibility = Visibility.Collapsed;
            //    copiarButton.Visibility = Visibility.Collapsed;
            //    buscarButton.Visibility = Visibility.Collapsed;
            //    editarButton.Visibility = Visibility.Collapsed;
            //}
        }
        //public IView activeView
        //{
        //    get
        //    {
        //        if (viewContainer.Children.Count > 0)
        //            return (IView)viewContainer.Children[0];
        //        return null;
        //    }
        //}

        //public void visualizarToolBar(Visibility x)
        //{
        //    toolBar1.Visibility = x;
        //}

        private void cerrarButton_Click(object sender, RoutedEventArgs e)
        {
            _cerrar();
            //visualizarToolBar(Visibility.Collapsed);          
        }

        private void _cerrar()
        {
            //if (MessageBox.Show("¿Desea salir de esta pantalla sin guardar los cambios?", Title, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            //{
            IView vw = pantalla;
            vw.cerrar();
            //pant = null;
            Cursor = Cursors.Wait;
            //IResult result = validarEjecucionCmd();            

            //pantalla.cerrar();

            viewContainer vc = new viewContainer(null);
            scrollContainer.Content = vc;
            pantalla = null;
            //if (viewContainer.Children != null)
            //    if (viewContainer.Children.Count > 0)
            //        viewContainer.Children.Clear();
            enableToolBarCommands(ToolbarCommands.none);
            Cursor = Cursors.Arrow;

            //}
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            _guardar();
        }

        public void guardar()
        {
            _guardar();
        }
        public bool habilitar = false;
        private void _guardar()
        {
            try
            {
                IView vw = pantalla;
                var result = vw.guardar();
                if (result.valor != null)
                {
                    if (result.typeResult == ResultTypes.success)
                    {
                        MessageBox.Show(string.Format(result.mensaje), "Registro Guardado", MessageBoxButton.OK, MessageBoxImage.Information);
                        scrollContainer.IsEnabled = habilitar;
                        //mainWindow.enableToolBarCommands(
                        //ToolbarCommands.eliminar |
                        //ToolbarCommands.editar |
                        //ToolbarCommands.nuevo |
                        //ToolbarCommands.cerrar
                        //);
                    }
                    else if (result.typeResult == ResultTypes.warning)
                    {
                        MessageBox.Show(string.Format(result.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else if (result.typeResult == ResultTypes.recordNotFound)
                    {
                        MessageBox.Show(string.Format(result.mensaje), "No se encontraron registros", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else
                    {
                        MessageBox.Show(string.Format(result.mensaje), "Ocurrio un Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show(string.Format("Se implemento codigo de guardado"), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (ArithmeticException ex)
            {
                MessageBox.Show(string.Format(ex.Message), "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (Exception e)
            {
                MessageBox.Show(string.Format(e.Message), "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                habilitar = false;
            }



        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            _eliminar();
        }

        private void _eliminar()
        {
            IView vw = pantalla;
            var result = vw.eliminar();

            if (result.typeResult == ResultTypes.success)
            {
                MessageBox.Show(string.Format(result.mensaje), "Registro Borrado", MessageBoxButton.OK, MessageBoxImage.Information);
                scrollContainer.IsEnabled = true;
            }
            else if (result.typeResult == ResultTypes.warning)
            {
                MessageBox.Show(string.Format(result.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else if (result.typeResult == ResultTypes.recordNotFound)
            {
                MessageBox.Show(string.Format(result.mensaje), "No se encontraron registros", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            else
            {
                MessageBox.Show(string.Format(result.mensaje), "Ocurrio un Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            _nuevo();
        }

        private void _nuevo()
        {

            IView vw = pantalla;
            // mainWindow.enableToolBarCommands(
            //    ToolbarCommands.buscar |
            //    ToolbarCommands.guardar |
            //    ToolbarCommands.nuevo |
            //    ToolbarCommands.cerrar
            //);
            vw.nuevo();
            scrollContainer.IsEnabled = true;

        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            _editar();
        }

        private void _editar()
        {
            IView vw = pantalla;
            vw.editar();

        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            _buscar();
        }

        private void _buscar()
        {
            IView vw = pantalla;
            vw.buscar();
            //scrollContainer.IsEnabled = false;
        }

        private void mainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //this.Close();
            //timer.Stop();
            //timer.Dispose();
            BitacoraAcceso bitacoraAcceso = new BitacoraAcceso
            {
                idBitacoraAcceso = 0,
                acceso = "FIN_SESION",
                fecha = DateTime.Now,
                usuario = usuario.nombreUsuario,
                contraseña = usuario.contrasena,
                nombreEquipo = this.nombreEquipo
            };

            var resp = new BitacoraAccesoSvc().saveBitacoraAcceso(ref bitacoraAcceso);
            if (resp.typeResult != ResultTypes.success)
            {
                ImprimirMensaje.imprimir(resp);
            }
        }

        private void mainWindow_KeyDown(object sender, KeyEventArgs e)
        {

            switch (e.SystemKey)
            {
                case Key.F10:
                    if (btnGuardar.Visibility == Visibility.Visible)
                        _guardar();
                    break;
                default:
                    break;
            }
            switch (e.Key)
            {
                case Key.F3:
                    if (cerrarButton.Visibility == Visibility.Visible)
                        _cerrar();
                    break;
                case Key.F11:
                    if (btnEditar.Visibility == Visibility.Visible)
                        _editar();
                    break;
                case Key.F9:
                    if (btnNuevo.Visibility == Visibility.Visible)
                        _nuevo();
                    break;
                case Key.F2:
                    if (btnBuscar.Visibility == Visibility.Visible)
                        _buscar();
                    break;
                case Key.F6:
                    if (btnEliminar.Visibility == Visibility.Visible)
                        _eliminar();
                    break;
                case Key.F10:
                    if (btnGuardar.Visibility == Visibility.Visible)
                        _guardar();
                    break;
                case Key.F4:
                    funcionF4();
                    break;
                case Key.F5:
                    funcionF5();
                    break;
                case Key.F7:
                    funcionF7();
                    break;
                default:
                    break;
            }
        }

        private void funcionF4()
        {
            try
            {
                if (scrollContainer.Content != null)
                {
                    if (scrollContainer.Content is viewContainer)
                    {
                        viewContainer view = scrollContainer.Content as viewContainer;
                        var pantalla = (view.Content as ContentControl).Content;
                        if (pantalla is editLiquidaciones)
                        {
                            editLiquidaciones liquidaciones = pantalla as editLiquidaciones;
                            if (liquidaciones.stpDescuentos.IsEnabled)
                            {
                                liquidaciones.añadirDescuento();
                            }
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private void funcionF5()
        {
            try
            {
                if (scrollContainer.Content != null)
                {
                    if (scrollContainer.Content is viewContainer)
                    {
                        viewContainer view = scrollContainer.Content as viewContainer;
                        var pantalla = (view.Content as ContentControl).Content;
                        if (pantalla is editLiquidaciones)
                        {
                            editLiquidaciones liquidaciones = pantalla as editLiquidaciones;
                            if (liquidaciones.stpExtras.IsEnabled)
                            {
                                liquidaciones.añadirExtra();
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private void funcionF7()
        {
            try
            {
                if (scrollContainer.Content != null)
                {
                    if (scrollContainer.Content is viewContainer)
                    {
                        viewContainer view = scrollContainer.Content as viewContainer;
                        var pantalla = (view.Content as ContentControl).Content;
                        if (pantalla is editLiquidaciones)
                        {
                            editLiquidaciones liquidaciones = pantalla as editLiquidaciones;
                            if (liquidaciones.stpHorasExtras.IsEnabled)
                            {
                                liquidaciones.añadirHorasExtra();
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
    }
}