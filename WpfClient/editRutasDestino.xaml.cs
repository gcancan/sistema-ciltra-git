﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using Core.Utils;
    using CoreFletera.Interfaces;
    using CoreFletera.Models;
    using Microsoft.Win32;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Drawing;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Forms;
    using System.Windows.Input;
    using System.Windows.Markup;
    using System.Windows.Media;

    public partial class editRutasDestino : WpfClient.ViewBase
    {
        private eAction action = eAction.NUEVO;
        private int lResult = 0;

        public editRutasDestino()
        {
            this.InitializeComponent();
        }
        private bool abrirEmpresa()
        {
            string ruta = System.Windows.Forms.Application.StartupPath + @"\config.ini";
            this.lResult = Declaraciones.fAbreEmpresa(@"C:\Compac\Empresas\" + new Core.Utils.Util().Read("CONECTION_STRING_COMERCIAL", "DB_NAME", ruta));
            if (this.lResult == 0)
            {
                return true;
            }
            Declaraciones.MuestraError(this.lResult);
            return false;
        }

        private bool BorrarSDK(Zona zona)
        {
            try
            {
                return GuardarServicio.eliminarServicio(zona);
            }
            catch (Exception exception)
            {
                System.Windows.MessageBox.Show(exception.Message);
                return false;
            }
        }

        private void btnImprimir_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = System.Windows.Input.Cursors.Wait;
                ReportView view = new ReportView();
                if (view.imprimirCatalogoRutas((Cliente)this.cbxClientes.SelectedItem, (cbxEmpresa.SelectedItem as Empresa).clave));
                {
                    //view.Show();
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }

        public override void buscar()
        {
            if (cbxEmpresa.SelectedItem == null || cbxClientes.SelectedItem == null) return;

            Zona resultado = new selectZonas("").findZonasByEmpresaCliente((cbxEmpresa.SelectedItem as Empresa).clave, (cbxClientes.SelectedItem as Cliente).clave);
            if (resultado != null)
            {
                this.mapForm(resultado);
                base.buscar();
            }
        }

        private void buscarOrigenes(int clave)
        {
            try
            {
                base.Cursor = System.Windows.Input.Cursors.Wait;
                OperationResult resp = new ZonasSvc().getOrigenByIdCliente(clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<OrigenPegaso> list = (List<OrigenPegaso>)resp.result;
                    this.cbxOrigen.ItemsSource = list;
                    if (list.Count == 1)
                    {
                        this.cbxOrigen.SelectedIndex = 0;
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }

        private void cbxClientes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.cbxClientes.SelectedItem == null)
            {
                this.btnImprimir.IsEnabled = false;
            }
            else
            {
                this.buscarOrigenes((this.cbxClientes.SelectedItem as Cliente).clave);
                this.btnImprimir.IsEnabled = true;
            }
        }

        private void cbxOrigen_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        public override void cerrar()
        {
            try
            {
                Declaraciones.fCierraEmpresa();
            }
            catch (Exception)
            {
            }
            try
            {
                Declaraciones.fTerminaSDK();
            }
            catch (Exception)
            {
            }
            base.cerrar();
        }

        public override void editar()
        {
            this.action = eAction.EDITAR;
            base.editar();
        }

        public override OperationResult eliminar()
        {
            OperationResult result3;
            try
            {
                base.Cursor = System.Windows.Input.Cursors.Wait;
                OperationResult result = this.validarForm();
                if (result.typeResult == ResultTypes.success)
                {
                    Zona zona = this.mapForm();
                    if (zona != null)
                    {
                        OperationResult result2 = new ZonasSvc().deleteRutasDestino(zona);
                        if (result2.typeResult == ResultTypes.success)
                        {
                            this.nuevo();
                        }
                        return result2;
                    }
                    return new OperationResult
                    {
                        mensaje = "Ocurrio un error al querer leer la informaci\x00f3n del formulario",
                        valor = 1,
                        result = null
                    };
                }
                result3 = result;
            }
            catch (Exception exception)
            {
                result3 = new OperationResult
                {
                    mensaje = exception.Message,
                    valor = 1,
                    result = null
                };
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
            return result3;
        }

        public override OperationResult guardar()
        {
            OperationResult result3;
            try
            {
                base.Cursor = System.Windows.Input.Cursors.Wait;
                OperationResult result = this.validarForm();
                if (result.typeResult == ResultTypes.success)
                {
                    Zona zona = this.mapForm();
                    if (zona != null)
                    {
                        OperationResult result2 = new ZonasSvc().saveRutasDestino(ref zona);
                        if (result2.typeResult == ResultTypes.success)
                        {
                            this.mapForm(zona);
                        }
                        return result2;
                    }
                    return new OperationResult
                    {
                        mensaje = "Ocurrio un error al querer leer la informaci\x00f3n del formulario",
                        valor = 1,
                        result = null
                    };
                }
                result3 = result;
            }
            catch (Exception exception)
            {
                result3 = new OperationResult
                {
                    mensaje = exception.Message,
                    valor = 1,
                    result = null
                };
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
            return result3;
        }

        private bool guardarSDK(Zona zona)
        {
            try
            {
                return GuardarServicio.guardarSDK(zona, this.action);
            }
            catch (Exception exception)
            {
                System.Windows.MessageBox.Show(exception.Message);
                return false;
            }
        }

        private bool hayError(int error)
        {
            if (error != 0)
            {
                Declaraciones.MuestraError(error);
                return true;
            }
            return false;
        }

        private void iniciaSDK()
        {
            try
            {
                string name = "SOFTWARE\\Wow6432Node\\Computaci\x00f3n en Acci\x00f3n, SA CV\\CONTPAQ I COMERCIAL";
                this.lResult = Declaraciones.SetCurrentDirectory(Registry.LocalMachine.OpenSubKey(name).GetValue("DirectorioBase").ToString());
                if (this.lResult != 0)
                {
                    Declaraciones.fInicializaSDK();
                    Declaraciones.fSetNombrePAQ("CONTPAQ I COMERCIAL");
                }
                else
                {
                    Declaraciones.MuestraError(this.lResult);
                }
            }
            catch (Exception exception)
            {
                System.Windows.MessageBox.Show("Ocurrio un error al tratar de iniciar el SDK." + Environment.NewLine + exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
                base.mainWindow.scrollContainer.IsEnabled = false;
            }
        }
        private void mapComboEmpresa(List<Empresa> listEmpresa)
        {
            foreach (Empresa empresa in listEmpresa)
            {
                this.cbxEmpresa.Items.Add(empresa);
            }
        }

        private Zona mapForm()
        {
            try
            {
                return new Zona
                {
                    clave = (this.txtClave.Tag == null) ? 0 : ((Zona)this.txtClave.Tag).clave,
                    descripcion = this.txtDescripcion.Text,
                    clave_empresa = ((Empresa)this.cbxEmpresa.SelectedItem).clave,
                    costoSencillo = Convert.ToDecimal(this.txtPrecioSencillo.valor),
                    costoSencillo35 = txtPrecioSencillo35.valor,// string.IsNullOrEmpty(this.txtPrecioSencillo35.Text.Trim()) ? decimal.Zero : Convert.ToDecimal(this.txtPrecioSencillo35.Text.Trim()),
                    costoFull = txtPrecioFull.valor,// Convert.ToDecimal(this.txtPrecioFull.Text.Trim()),
                    km = Convert.ToDecimal(this.txtKM.Text.Trim()),
                    claveCliente = ((Cliente)this.cbxClientes.SelectedItem).clave,
                    claveExtra = string.IsNullOrEmpty(this.txtClaveExtra.Text.Trim()) ? "0" : this.txtClaveExtra.Text.Trim(),
                    correo = this.txtCorreo.Text.Trim(),
                    precioFijoSencillo = this.ctrPrecioFijoSencillo.valor,
                    precioFijoSencillo35 = this.ctrPrecioFijoSencillo35.valor,
                    precioFijoFull = this.ctrPrecioFijoFull.valor,
                    estado = (Estado)this.cbxEstado.SelectedItem,
                    sencillo_24 = this.ctr24.valor,
                    sencillo_30 = this.ctr30.valor,
                    sencillo_35 = this.ctr35.valor,
                    full = this.ctrFull.valor,
                    origenPegaso = this.cbxOrigen.SelectedItem as OrigenPegaso,
                    tiempoIda = txtTiempoIda.Text,
                    tiempoGranja = txtTiempoGranja.Text,
                    tiempoRetorno = txtTiempoRetorno.Text,
                    viaje = chcViaje.IsChecked.Value,
                    claveZona = txtZona.Text.Trim(),
                    categoria = cbxCategoria1.SelectedItem == null ? null : (cbxCategoria1.SelectedItem as TipoGranja),
                    categoria2 = cbxCategoria2.SelectedItem == null ? null : (cbxCategoria2.SelectedItem as TipoGranja)
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void mapForm(Zona resultado)
        {
            if (resultado == null)
            {
                this.cbxEmpresa.SelectedItem = null;
                this.cbxClientes.SelectedItem = null;
                this.txtClave.Clear();
                this.txtClave.Tag = null;
                this.txtDescripcion.Clear();
                this.txtPrecioFull.Clear();
                this.txtPrecioSencillo.Clear();
                this.txtKM.Clear();
                this.txtPrecioSencillo35.Clear();
                this.txtZona.Clear();
                this.txtClaveExtra.Text = "";
                this.txtCorreo.Text = "";
                this.cbxEstado.SelectedItem = null;
                this.ctr24.valor = decimal.Zero;
                this.ctr30.valor = decimal.Zero;
                this.ctr35.valor = decimal.Zero;
                this.ctrFull.valor = decimal.Zero;
                this.ctrPrecioFijoSencillo.valor = decimal.Zero;
                this.ctrPrecioFijoSencillo35.valor = decimal.Zero;
                this.ctrPrecioFijoFull.valor = decimal.Zero;
                this.txtTiempoIda.Text = "00:00:00";
                txtTiempoGranja.Text = "00:00:00";
                txtTiempoRetorno.Text = "00:00:00";
                chcViaje.IsChecked = false;
                cbxCategoria1.SelectedItem = null;
                cbxCategoria2.SelectedItem = null;
            }
            else
            {
                int num = 0;
                foreach (Empresa empresa in (IEnumerable)this.cbxEmpresa.Items)
                {
                    if (empresa.clave == resultado.clave_empresa)
                    {
                        this.cbxEmpresa.SelectedIndex = num;
                        break;
                    }
                    num++;
                }
                num = 0;
                foreach (Cliente cliente in (IEnumerable)this.cbxClientes.Items)
                {
                    if (cliente.clave == resultado.claveCliente)
                    {
                        this.cbxClientes.SelectedIndex = num;
                        break;
                    }
                    num++;
                }
                num = 0;
                foreach (Estado estado in (IEnumerable)this.cbxEstado.Items)
                {
                    if (estado.idEstado == resultado.estado.idEstado)
                    {
                        this.cbxEstado.SelectedIndex = num;
                        break;
                    }
                    num++;
                }
                this.txtClave.Text = resultado.clave.ToString();
                this.txtClave.Tag = resultado;
                this.txtDescripcion.Text = resultado.descripcion;
                this.txtPrecioFull.valor = resultado.costoFull; //.ToString("#.##");
                this.txtPrecioSencillo.valor = resultado.costoSencillo; //.ToString("#.##");
                this.txtKM.Text = resultado.km.ToString("#.##");
                this.txtPrecioSencillo35.valor = resultado.costoSencillo35;//.ToString("#.##");
                this.txtZona.Text = resultado.claveZona;
                this.txtClaveExtra.Text = resultado.claveExtra.ToString();
                this.txtCorreo.Text = resultado.correo;
                this.ctr24.valor = resultado.sencillo_24;
                this.ctr30.valor = resultado.sencillo_30;
                this.ctr35.valor = resultado.sencillo_35;
                this.ctrFull.valor = resultado.full;
                this.ctrPrecioFijoFull.valor = resultado.precioFijoFull;
                this.ctrPrecioFijoSencillo.valor = resultado.precioFijoSencillo;
                this.ctrPrecioFijoSencillo35.valor = resultado.precioFijoSencillo35;
                chcViaje.IsChecked = resultado.viaje;
                txtTiempoIda.Text = string.IsNullOrEmpty(resultado.tiempoIda) ? "00:00:00" : resultado.tiempoIda;
                txtTiempoGranja.Text = string.IsNullOrEmpty(resultado.tiempoGranja) ? "00:00:00" : resultado.tiempoGranja;
                txtTiempoRetorno.Text = string.IsNullOrEmpty(resultado.tiempoRetorno) ? "00:00:00" : resultado.tiempoRetorno;
                if (resultado.origenPegaso != null)
                {
                    this.cbxOrigen.SelectedItem = this.cbxOrigen.Items.Cast<OrigenPegaso>().ToList<OrigenPegaso>().Find(s => s.idOrigen == resultado.origenPegaso.idOrigen);
                }

                if (resultado.categoria == null)
                {
                    cbxCategoria1.SelectedItem = null;
                }
                else
                {
                    cbxCategoria1.SelectedItem = cbxCategoria1.ItemsSource.Cast<TipoGranja>().ToList().Find(s => s.idTipoGranja == resultado.categoria.idTipoGranja);
                }

                if (resultado.categoria2 == null)
                {
                    cbxCategoria2.SelectedItem = null;
                }
                else
                {
                    cbxCategoria2.SelectedItem = cbxCategoria2.ItemsSource.Cast<TipoGranja>().ToList().Find(s => s.idTipoGranja == resultado.categoria2.idTipoGranja);
                }

            }
        }

        public override void nuevo()
        {
            this.mapForm(null);
            this.action = eAction.NUEVO;
            base.nuevo();
        }
        private OperationResult validarForm()
        {
            bool flag = true;
            string str = "Para proceder se requiere corregir lo siguiente:" + Environment.NewLine;
            if (this.cbxEmpresa.SelectedItem == null)
            {
                str = str + "* Seleccionar una empresa." + Environment.NewLine;
                flag = false;
            }
            if (!string.IsNullOrEmpty(this.txtClaveExtra.Text.Trim()))
            {
            }
            if (this.cbxEstado.SelectedItem == null)
            {
                flag = false;
                str = str + "* Seleccionar un Estado." + Environment.NewLine;
            }
            if (string.IsNullOrEmpty(this.txtDescripcion.Text.Trim()))
            {
                str = str + "* Escribir una descripci\x00f3n." + Environment.NewLine;
                flag = false;
            }
            if (string.IsNullOrEmpty(this.txtKM.Text.Trim()))
            {
                str = str + "* Proporcionar un kilometraje." + Environment.NewLine;
                flag = false;
            }
            else if (!decimal.TryParse(this.txtKM.Text.Trim(), out _))
            {
                str = str + "* Escribir un kilometraje valido." + Environment.NewLine;
            }
            if (!string.IsNullOrEmpty(this.txtPrecioSencillo35.txtDecimal.Text.Trim()) && !decimal.TryParse(this.txtPrecioSencillo35.txtDecimal.Text.Trim(), out _))
            {
                str = str + "* Escribir un precio de flete Sencillo de 35 toneladas valido." + Environment.NewLine;
            }
            if (string.IsNullOrEmpty(this.txtPrecioSencillo.txtDecimal.Text.Trim()))
            {
                str = str + "* Proporcionar el precio del flete sencillo." + Environment.NewLine;
                flag = false;
            }
            else if (!decimal.TryParse(this.txtPrecioSencillo.txtDecimal.Text.Trim(), out _))
            {
                str = str + "* Escribir un precio de flete Sencillo valido." + Environment.NewLine;
            }
            if (string.IsNullOrEmpty(this.txtPrecioFull.txtDecimal.Text.Trim()))
            {
                str = str + "* Proporcionar el precio del flete Full." + Environment.NewLine;
                flag = false;
            }
            else if (!decimal.TryParse(this.txtPrecioFull.txtDecimal.Text.Trim(), out _))
            {
                str = str + "* Escribir un precio de flete Full valido." + Environment.NewLine;
            }
            if (this.cbxClientes.SelectedItem == null)
            {
                str = str + "* Seleccionar un Cliente." + Environment.NewLine;
                flag = false;
            }
            if (flag)
            {
                return new OperationResult
                {
                    mensaje = "Correcto",
                    valor = 0
                };
            }
            return new OperationResult
            {
                mensaje = str,
                valor = 2
            };
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            OperationResult result = new EmpresaSvc().getEmpresasALLorById(0);
            if (result.typeResult == ResultTypes.success)
            {
                List<Empresa> listEmpresa = (List<Empresa>)result.result;
                this.mapComboEmpresa(listEmpresa);
            }
            else
            {
                ImprimirMensaje.imprimir(result);
            }

            result = new EstadoSvc().getEstadosAllorById(0);
            if (result.typeResult == ResultTypes.success)
            {
                this.cbxEstado.ItemsSource = (List<Estado>)result.result;
            }

            txtTiempoIda.TextChanged += actualizarTiempos;
            txtTiempoGranja.TextChanged += actualizarTiempos;
            txtTiempoRetorno.TextChanged += actualizarTiempos;

            var resp = new TipoGranjaSvc().getAllTiposGranjas();
            if (resp.typeResult == ResultTypes.success)
            {
                List<TipoGranja> listaTipos = (resp.result as List<TipoGranja>).FindAll(s => s.activo).ToList();
                cbxCategoria1.ItemsSource = listaTipos;
                cbxCategoria2.ItemsSource = listaTipos;
            }

            this.nuevo();
        }
        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cbxEmpresa.SelectedItem != null)
                {
                    Empresa empresa = cbxEmpresa.SelectedItem as Empresa;
                    var result = new ClienteSvc().getClientesALLorById(empresa.clave, 0, true);
                    if (result.typeResult == ResultTypes.success)
                    {
                        this.cbxClientes.ItemsSource = (List<Cliente>)result.result;
                    }
                    else
                    {
                        cbxClientes.ItemsSource = null;
                    }
                    
                }
                else
                {
                    cbxClientes.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {
                
            }
        }
        private void actualizarTiempos(object sender, TextChangedEventArgs e)
        {
            TimeSpan tiempoIda;
            if (TimeSpan.TryParse(txtTiempoIda.Text, out tiempoIda))
            {
                txtTiempoIda.BorderBrush = new SolidColorBrush(Colors.Black);
            }
            else
            {
                txtTiempoIda.BorderBrush = new SolidColorBrush(Colors.Red);
                tiempoIda = new TimeSpan(0, 0, 0);
            }

            TimeSpan tiempoGranja;
            if (TimeSpan.TryParse(txtTiempoGranja.Text, out tiempoGranja))
            {
                txtTiempoGranja.BorderBrush = new SolidColorBrush(Colors.Black);
            }
            else
            {
                txtTiempoGranja.BorderBrush = new SolidColorBrush(Colors.Red);
                tiempoGranja = new TimeSpan(0, 0, 0);
            }

            TimeSpan tiempoRetorno;
            if (TimeSpan.TryParse(txtTiempoRetorno.Text, out tiempoRetorno))
            {
                txtTiempoRetorno.BorderBrush = new SolidColorBrush(Colors.Black);
            }
            else
            {
                txtTiempoRetorno.BorderBrush = new SolidColorBrush(Colors.Red);
                tiempoRetorno = new TimeSpan(0, 0, 0);
            }

            var total = (tiempoIda + tiempoGranja + tiempoRetorno);

            string[] ar = total.ToString().Split(':');
            if (((int)total.TotalHours).ToString().Length == 3)
            {
                txtTiempoTotal.Text = string.Format("{0}:{1}:{2}", ((int)total.TotalHours).ToString(), ar[1], ar[2]);
            }
            else if (((int)total.TotalHours).ToString().Length == 2)
            {
                txtTiempoTotal.Text = string.Format("0{0}:{1}:{2}", ((int)total.TotalHours).ToString(), ar[1], ar[2]);
            }
            else if (((int)total.TotalHours).ToString().Length == 1)
            {
                txtTiempoTotal.Text = string.Format("00{0}:{1}:{2}", ((int)total.TotalHours).ToString(), ar[1], ar[2]);
            }
        }

        
    }
}
