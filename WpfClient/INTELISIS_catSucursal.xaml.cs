﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using CoreINTELISIS.INTERFACES;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para INTELISIS_catSucursal.xaml
    /// </summary>
    public partial class INTELISIS_catSucursal : UserControl
    {
        SincronizadorINTELISIS pantalla = null;
        public INTELISIS_catSucursal(SincronizadorINTELISIS pantalla)
        {
            InitializeComponent();
            this.pantalla = pantalla;
        }
        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            pantalla.stpContenedor.Children.Clear();
        }
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lvlSucursales.ItemsSource = null;
                lblContador.Content = 0;
                lblActualizados.Content = 0;
                lblNuevos.Content = 0;
                Cursor = Cursors.Wait;
                var resp = new SucursalINTELISISSvc().getAllSucursales();
                if (resp.typeResult == ResultTypes.success)
                {
                    List<ZonaOperativa> listaZonas = resp.result as List<ZonaOperativa>;
                    lvlSucursales.ItemsSource = listaZonas;
                    lblContador.Content = listaZonas.Count;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                lblActualizados.Content = 0;
                lblNuevos.Content = 0;
                if (lvlSucursales.ItemsSource != null)
                {
                    List<ZonaOperativa> listaZonas = lvlSucursales.ItemsSource as List<ZonaOperativa>;
                    var resp = new ZonaOperativaSvc().sincronizarZonasOperativas(listaZonas);
                    lblActualizados.Content = resp.noActualizados;
                    lblNuevos.Content = resp.noNuevos;
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }       
    }
}
