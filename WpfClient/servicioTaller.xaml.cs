﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using CoreFletera.Interfaces;
    using CoreFletera.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Markup;

    public partial class servicioTaller : ViewBase
    {
        public servicioTaller()
        {
            this.InitializeComponent();
        }       

        private void btnAgregarOrden_Click(object sender, RoutedEventArgs e)
        {
            List<OrdenTaller> listaOrdenes = new List<OrdenTaller>();
            foreach (ListViewItem item in (IEnumerable)this.lvlActividades.Items)
            {
                listaOrdenes.Add((OrdenTaller)item.Content);
            }
            agregarOrdenLlantera llantera = new agregarOrdenLlantera(this.txtFolio.Tag as TipoOrdenServicio, (this.txtFolio.Tag as TipoOrdenServicio).TipoServicio == "LLANTAS", listaOrdenes, false, false);
            if (llantera.ShowDialog().Value)
            {
                this.buscarOrdenTraabajo(this.txtFolio.valor);
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            string str = new ventanaInputTexto("Motivo De Cancelacion", "Proporcionar el motivo de la cancelaci\x00f3n").obtenerRespuesta();
            if (!string.IsNullOrEmpty(str))
            {
                OperationResult resp = new MasterOrdServSvc().cancelarOrdenServicio(this.txtFolio.Tag as TipoOrdenServicio, str, base.mainWindow.inicio._usuario.nombreUsuario);
                if (resp.typeResult == ResultTypes.success)
                {
                    ImprimirMensaje.imprimir(resp);
                    this.buscarOrdenTraabajo(this.txtFolio.valor);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
        }

        private void btnTallerExterno_Click(object sender, RoutedEventArgs e)
        {
            OperationResult resp = new OrdenesTrabajoSvc().enviarTallerExterno(this.txtFolio.valor);
            if (resp.typeResult == ResultTypes.success)
            {
                this.buscarOrdenTraabajo(this.txtFolio.valor);
            }
            ImprimirMensaje.imprimir(resp);
        }

        private void btnVerPanel_Click(object sender, RoutedEventArgs e)
        {
            new viewPanelOSTransito(2).abrirPanel();
        }

        public override void buscar()
        {
            TipoOrdenServicio servicio2 = new selectOrdenServicio().getOrdenServicio(0);
            if (servicio2 != null)
            {
                this.buscarOrdenTraabajo(servicio2.idOrdenSer);
            }
        }

        private void buscarOrdenTraabajo(int valor)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult resp = new OperacionSvc().getOrdenServById(valor);
                if (resp.typeResult == ResultTypes.success)
                {
                    TipoOrdenServicio orden = resp.result as TipoOrdenServicio;
                    this.mapOrdenServicio(orden);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void mapOrdenServicio(TipoOrdenServicio orden)
        {
            if (orden != null)
            {
                this.txtFolio.valor = orden.idOrdenSer;
                this.txtFolio.Tag = orden;
                this.txtFolio.IsEnabled = false;
                this.txtUnidad.Text = orden.unidadTrans.clave;
                this.txtEmpresa.Text = orden.unidadTrans.empresa.nombre;
                this.txtTipoOrden.Text = orden.TipoServicio;
                this.txtEstatus.Text = orden.estatus;
                this.txtChofer.Text = orden.chofer;
                this.txtTipoServicio.Text = orden.tipoOrden.nombre;
                this.lvlActividades.Items.Clear();
                OperationResult resp = new MasterOrdServSvc().getTrabajosByOrdenSer(orden.idOrdenSer);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<OrdenTaller> result = resp.result as List<OrdenTaller>;
                    bool flag3 = true;
                    foreach (OrdenTaller taller in result)
                    {
                        ListViewItem newItem = new ListViewItem
                        {
                            Content = taller
                        };
                        if ((((taller.estatus.ToUpper() == "PRE") || (taller.estatus.ToUpper() == "REC")) || ((taller.estatus.ToUpper() == "ASIG") || (taller.estatus.ToUpper() == "ENT"))) || (taller.estatus.ToUpper() == "DIAG"))
                        {
                            ContextMenu menu = new ContextMenu();
                            MenuItem item2 = new MenuItem
                            {
                                Header = "Cancelar"
                            };
                            item2.Tag = taller;
                            item2.Click += new RoutedEventHandler(this.MenuCandelar_Click);
                            menu.Items.Add(item2);
                            MenuItem item3 = new MenuItem
                            {
                                Tag = taller,
                                Header = "Cambiar"
                            };
                            item3.Click += new RoutedEventHandler(this.MenuCambiar_Click);
                            menu.Items.Add(item3);
                            newItem.ContextMenu = menu;
                        }
                        else
                        {
                            flag3 = false;
                        }
                        this.lvlActividades.Items.Add(newItem);
                    }
                    this.btnCancelar.IsEnabled = flag3;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
                if ((((orden.estatus == "PRE") || (orden.estatus == "REC")) || (orden.estatus == "DIAG")) || (orden.estatus == "ASIG"))
                {
                    base.IsEnabled = true;
                }
                else
                {
                    base.IsEnabled = false;
                }
                if (orden.externo)
                {
                    base.IsEnabled = false;
                    this.btnTallerExterno.Visibility = Visibility.Collapsed;
                    this.lblExterno.Visibility = Visibility.Visible;
                }
                this.btnTallerExterno.IsEnabled = !orden.externo;
            }
            else
            {
                this.txtFolio.limpiar();
                this.txtFolio.Tag = null;
                this.txtFolio.IsEnabled = true;
                this.txtUnidad.Clear();
                this.txtTipoOrden.Clear();
                this.txtEstatus.Clear();
                this.lvlActividades.Items.Clear();
                base.IsEnabled = true;
                this.btnCancelar.IsEnabled = false;
                this.btnTallerExterno.IsEnabled = false;
                this.btnTallerExterno.Visibility = Visibility.Visible;
                this.lblExterno.Visibility = Visibility.Collapsed;
                this.txtTipoServicio.Clear();
                this.txtEmpresa.Clear();
                this.txtChofer.Clear();
            }
        }

        private void MenuCambiar_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            OrdenTaller tag = item.Tag as OrdenTaller;
            List<OrdenTaller> listaOrdenes = new List<OrdenTaller>();
            foreach (ListViewItem item2 in (IEnumerable)this.lvlActividades.Items)
            {
                listaOrdenes.Add((OrdenTaller)item2.Content);
            }
            agregarOrdenLlantera llantera = new agregarOrdenLlantera(tag, this.txtFolio.Tag as TipoOrdenServicio, (this.txtFolio.Tag as TipoOrdenServicio).TipoServicio == "LLANTAS", listaOrdenes, false);
            if (llantera.ShowDialog().Value)
            {
                this.buscarOrdenTraabajo(this.txtFolio.valor);
            }
        }

        private void MenuCandelar_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            OrdenTaller tag = item.Tag as OrdenTaller;
            if (new cancelarOrden(tag, base.mainWindow.inicio._usuario.nombreUsuario).cancelarOrdenTrabajo().Value)
            {
                this.buscarOrdenTraabajo(this.txtFolio.valor);
            }
        }

        public override void nuevo()
        {
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.buscar | ToolbarCommands.nuevo);
            this.mapOrdenServicio(null);
        }

        

        private void TxtEntero_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && !string.IsNullOrEmpty(this.txtFolio.txtEntero.Text.Trim()))
            {
                this.buscarOrdenTraabajo(this.txtFolio.valor);
            }
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.nuevo();
            this.txtFolio.txtEntero.KeyDown += new KeyEventHandler(this.TxtEntero_KeyDown);
            this.btnTallerExterno.Visibility = (new PrivilegioSvc().consultarPrivilegio("MOSTRAR_BOTON_ENVIO_EXTERNO", base.mainWindow.inicio._usuario.idUsuario).typeResult == ResultTypes.success) ? Visibility.Visible : Visibility.Collapsed;
        }
    }
}
