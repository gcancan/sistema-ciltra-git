﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using f = System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using Core.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for salidasProgramadas.xaml
    /// </summary>
    public partial class salidasProgramadas : ViewBase
    {
        public salidasProgramadas()
        {
            InitializeComponent();
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            nuevo();
            var respCliente = new ClienteSvc().getClientesALLorById(mainWindow.inicio._usuario.personal.empresa.clave);
            List<Cliente> listCliente = (List<Cliente>)respCliente.result;
            mapComboClientes(listCliente);

            int i = 0;
            foreach (Cliente item in cbxClientes.Items)
            {
                if (item.clave == mainWindow.inicio._usuario.cliente.clave)
                {
                    cbxClientes.SelectedIndex = i;
                    //cbxClientes.IsEnabled = false;
                    break;
                }
                i++;
            }
        }

        private void mapComboClientes(List<Cliente> listCliente)
        {
            foreach (Cliente item in listCliente)
            {
                cbxClientes.Items.Add(item);
            }
        }
        public override void nuevo()
        {            
            mainWindow.enableToolBarCommands(
                ToolbarCommands.guardar |
                ToolbarCommands.nuevo |
                ToolbarCommands.cerrar);
            lvlTareas.Items.Clear();
        }
        private void CargarArchivo_Click(object sender, RoutedEventArgs e)
        {
            f.OpenFileDialog dialog = new f.OpenFileDialog();
            //dialog
            dialog.Filter = "|*.xlsx";
            dialog.Title = "Elige el archivo para subir";
            if (dialog.ShowDialog() == f.DialogResult.OK)
            {
                OperationResult readExel = new LeerExcel().leerDocumento(dialog.FileName);
                if (readExel.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(readExel.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (readExel.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(readExel.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                if (readExel.typeResult == ResultTypes.success)
                {
                    mapLista((List<TareaProgramada>)readExel.result);
                }
            }
        }

        private void mapLista(List<TareaProgramada> result)
        {
            lvlTareas.Items.Clear();
            foreach (var item in result)
            {
                if (item.remision == 0)
                {
                    continue;
                }
                item.idCliente = ((Cliente)cbxClientes.SelectedItem).clave;
                ListViewItem lvl = new ListViewItem();
                item.estatus = true;
                lvl.Content = item;
                //lvl.Foreground = Brushes.White;
                lvl.FontWeight = FontWeights.Bold;
                lvlTareas.Items.Add(lvl);
            }
        }

        public override OperationResult guardar()
        {
            List<TareaProgramada> listTarea= mapForm();
            if (listTarea != null)
            {
                var resp = new TareasProgramadasSvc().saveTareasProgramadas(listTarea);
                if (resp.typeResult == ResultTypes.success)
                {
                    nuevo();
                    return resp;
                }
                else
                {
                    return new OperationResult { valor = resp.valor, mensaje = resp.mensaje, result = null };
                }                
            }
            else
            {
                return new OperationResult { valor = 2, mensaje = "La lista esta vácia", result = null };
            }
           
        }

        private List<TareaProgramada> mapForm()
        {
            try
            {
                List<TareaProgramada> listTarea = new List<TareaProgramada>();
                if (lvlTareas.Items.Count > 0)
                {
                    foreach (ListViewItem item in lvlTareas.Items)
                    {
                        listTarea.Add((TareaProgramada)item.Content);
                    }
                    return listTarea;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
        
    }
}
