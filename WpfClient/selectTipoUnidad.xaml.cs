﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectTipoUnidad.xaml
    /// </summary>
    public partial class selectTipoUnidad : Window
    {
        public selectTipoUnidad()
        {
            InitializeComponent();
        }
        string parametro = string.Empty;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtBuscador.Text = parametro;
        }
        public TipoUnidad getAllTipoUnidad(string parametro)
        {
            try
            {
                this.parametro = parametro;
                buscarTodosTiposUnidades();
                bool? resp = ShowDialog();
                if (resp.Value && lvlTipoUnidad.SelectedItem != null)
                {
                    return (lvlTipoUnidad.SelectedItem as ListViewItem).Content as TipoUnidad;
                }
                return null;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }

        void buscarTodosTiposUnidades()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new TipoUnidadSvc().getAllTipoUnidades();
                if (resp.typeResult == ResultTypes.success)
                {
                    mapTipoUnidad(resp.result as List<TipoUnidad>);
                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void mapTipoUnidad(List<TipoUnidad> listTipoUnidad)
        {
            try
            {
                lvlTipoUnidad.ItemsSource = null;
                List<ListViewItem> list = new List<ListViewItem>();
                foreach (TipoUnidad col in listTipoUnidad)
                {
                    ListViewItem item = new ListViewItem
                    {
                        Content = col
                    };
                    item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                    list.Add(item);
                }
                this.lvlTipoUnidad.ItemsSource = list;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlTipoUnidad.ItemsSource);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtBuscador.Text) ||
            ((((TipoUnidad)(item as ListViewItem).Content).descripcion.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(this.lvlTipoUnidad.ItemsSource).Refresh();
        }
    }
}
