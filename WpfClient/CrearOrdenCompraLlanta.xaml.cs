﻿using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for CrearOrdenCompraLlanta.xaml
    /// </summary>
    public partial class CrearOrdenCompraLlanta : ViewBase
    {
        crearOrdenCompra classOrden = null;
        public CrearOrdenCompraLlanta()
        {
            InitializeComponent();
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {            
            nuevo();
            if (!buscarLlantasProducto())
            {
                mainWindow.scrollContainer.IsEnabled = false;
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                return;
            }
            if (!buscarProveedores())
            {
                mainWindow.scrollContainer.IsEnabled = false;
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                return;
            }
            
            cbxEmpresas.loaded(null, true);              
        }

        private bool buscarProveedores()
        {
            try
            {
                OperationResult resp = new ProveedorSvc().getAllProveedores();
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Proveedor> lista = (List<Proveedor>)resp.result;
                    cbxProveedor.ItemsSource = lista;
                    if (lista.Count == 1)
                    {
                        cbxProveedor.SelectedIndex = 0;
                    }
                    return true;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    return false;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return false;
            }
        }

        private bool buscarLlantasProducto()
        {
            try
            {
                OperationResult resp = new LlantaSvc().getAllLLantasProducto();
                if (resp.typeResult == ResultTypes.success)
                {
                    return llenatPanel((List<LLantasProducto>)resp.result);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    return false;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return false;
            }
        }

        private bool llenatPanel(List<LLantasProducto> Lista)
        {
            try
            {
                foreach (LLantasProducto llProducto in Lista)
                {
                    CheckBox chc = new CheckBox() { VerticalContentAlignment = VerticalAlignment.Center, Tag = llProducto };
                    StackPanel stpChc = new StackPanel { Orientation = Orientation.Horizontal };
                    chc.Unchecked += Chc_Unchecked;
                    Label lblMarca = new Label() { Content = llProducto.marca.descripcion, Margin = new Thickness(2), Width = 200 };
                    stpChc.Children.Add(lblMarca);

                    Label lblDiseño = new Label() { Content = llProducto.diseño.descripcion, Margin = new Thickness(2), Width = 120, HorizontalContentAlignment = HorizontalAlignment.Center };
                    stpChc.Children.Add(lblDiseño);

                    Label lblMedida = new Label() { Content = llProducto.medida, Margin = new Thickness(2), Width = 120, HorizontalContentAlignment = HorizontalAlignment.Center };
                    stpChc.Children.Add(lblMedida);

                    Label lblProfundidad = new Label() { Content = llProducto.profundidad, Margin = new Thickness(2), Width = 120, HorizontalContentAlignment = HorizontalAlignment.Center };
                    stpChc.Children.Add(lblProfundidad);
                    chc.Content = stpChc;
                    stpListTiposLlantas.Children.Add(chc);
                }
                return true;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return false;
            }
        }
        private bool omitir = false;
        private void Chc_Unchecked(object sender, RoutedEventArgs e)
        {
            omitir = true;
            chcTodos.IsChecked = false;
            omitir = false;
        }

        public override void nuevo()
        {
            base.nuevo();
            txtFolioCompra.Tag = null;
            txtFolioCompra.Clear();
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            stpDetalles.Children.Clear();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (stpListTiposLlantas.Children.Count > 0)
            {
                marcarDesmarcar(true);
            }
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            if (omitir)
            {
                return;
            }
            if (stpListTiposLlantas.Children.Count > 0)
            {
                marcarDesmarcar(false);
            }
        }

        public void marcarDesmarcar(bool check)
        {
            List<CheckBox> listaChecBox = stpListTiposLlantas.Children.Cast<CheckBox>().ToList();
            foreach (CheckBox chcBox in listaChecBox)
            {
                chcBox.IsChecked = check;
            }
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {

            if (cbxEmpresas.empresaSelected == null)
            {
                ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "Se tiene que elegir una empresa" });
                return;
            }
            List<LLantasProducto> listaProductos = new List<LLantasProducto>();
            List<CheckBox> listaChecBox = stpListTiposLlantas.Children.Cast<CheckBox>().ToList();
            foreach (CheckBox chcBox in listaChecBox)
            {
                if (chcBox.IsChecked.Value)
                {
                    listaProductos.Add((LLantasProducto)chcBox.Tag);
                }
            }

            foreach (var item in listaProductos)
            {
                //if (!existeProductoLista(item))
                //{
                StackPanel stp = new StackPanel() { Orientation = Orientation.Horizontal, Margin = new Thickness(2) };
                stp.Tag = item;
                Label lbl = new Label() { Width = 270, Tag = item, Content = item.marca.descripcion + " " + item.diseño.descripcion + " " + item.medida };
                stp.Children.Add(lbl);

                controlDecimal ctrDecimal = new controlDecimal();
                stp.Children.Add(ctrDecimal);

                Label lbluni = new Label() { Content = "Para:", FontWeight = FontWeights.Medium };
                stp.Children.Add(lbluni);

                controlUnidadesTransporte ctrUnidades = new controlUnidadesTransporte();
                ctrUnidades.loaded(etipoUniadBusqueda.TODAS, cbxEmpresas.empresaSelected);
                stp.Children.Add(ctrUnidades);

                Button btnCambiar = new Button() { Tag = item, Content = "CAMBIAR", Foreground = Brushes.White, Background = Brushes.SteelBlue, Width = 100, Margin = new Thickness(2) };
                btnCambiar.Click += BtnCambiar_Click;
                stp.Children.Add(btnCambiar);

                Button btnQuitar = new Button() { Tag = item, Content = "QUITAR", Foreground = Brushes.White, Background = Brushes.Red, Width = 100, Margin = new Thickness(2) };
                btnQuitar.Click += Btn_Click;
                stp.Children.Add(btnQuitar);

                stpDetalles.Children.Add(stp);
                //}                
            }

            //OrdenCompraLLantas orden = new OrdenCompraLLantas()
            //{
            //    empresa = new Empresa { clave = 1, nombre = "pegaso" },
            //    estatus = "Act",
            //    fecha = DateTime.Now,
            //    folioComercial = "0",
            //    idOrdenCompra = 0,
            //    folioFactura = "",
            //    listaDetalles = new List<DetalleCompraLlanta>()
            //    {
            //        new DetalleCompraLlanta
            //        {
            //            llantaProducto = new LLantasProducto
            //            {
            //                diseño = "asd",
            //                idLLantasProducto = 12354,
            //                marca = new Marca
            //                {
            //                    descripcion = "marcaFalsa"
            //                },
            //                medida = "124s4"
            //            },
            //            cantidad = 10,
            //            unidadTransporte = null
            //        }

            //    }
            //};
            //var s = crearOrdenCompra.generarOrdenCompra("adPegaso", orden);
        }

        private void BtnCambiar_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            LLantasProducto llPro = (LLantasProducto)btn.Tag;
            List<StackPanel> listaSp = stpDetalles.Children.Cast<StackPanel>().ToList();
            foreach (StackPanel stp in listaSp)
            {
                if (((LLantasProducto)stp.Tag).idLLantasProducto == llPro.idLLantasProducto)
                {
                    List<object> listaObj = stp.Children.Cast<object>().ToList();
                    foreach (object item in listaObj)
                    {
                        if (item is controlUnidadesTransporte)
                        {
                            controlUnidadesTransporte ctr = (controlUnidadesTransporte)item;
                            ctr.limpiar();
                        }
                    }
                }
            }
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            int i = 0;
            Button btn = (Button)sender;
            LLantasProducto llPro = (LLantasProducto)btn.Tag;
            List<StackPanel> listaSp = stpDetalles.Children.Cast<StackPanel>().ToList();
            foreach (StackPanel stp in listaSp)
            {
                if (((LLantasProducto)stp.Tag).idLLantasProducto == llPro.idLLantasProducto)
                {
                    stpDetalles.Children.Remove(stp);
                }
            }
        }

        private bool existeProductoLista(LLantasProducto item)
        {
            List<StackPanel> listaChecBox = stpDetalles.Children.Cast<StackPanel>().ToList();
            foreach (StackPanel stp in listaChecBox)
            {
                if (((LLantasProducto)stp.Tag).idLLantasProducto == item.idLLantasProducto)
                {
                    return true;
                }
            }
            return false;
        }
        
        public override void cerrar()
        {
            if (classOrden != null)
            {
                classOrden.cerrar();
            }
            
            base.cerrar();
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                if (classOrden == null)
                {
                    bool resp = false;
                    classOrden = new crearOrdenCompra(ref resp);
                    if (!resp)
                    {
                        return new OperationResult { valor = 2, mensaje = "ERROR AL INICIAR LA INSTANCIA DEL COMERCIAL" };
                    }
                }

                OrdenCompra orden = new OrdenCompra();
                orden = mapform();
                var guardo = classOrden.generarOrdenCompra(cbxEmpresas.empresaSelected.nombreComercial, orden);
                if (guardo)
                {
                    //MessageBox.Show(classOrden.folioOrdenCompra.ToString()+ " - " + classOrden.folio.ToString());
                    orden.CIDDOCUMENTO = classOrden.folioOrdenCompra;
                    orden.CFOLIO = classOrden.folio;
                    orden.CSERIEDOCUMENTO = classOrden.serie;
                    var resp = new OrdenCompraSvc().saveOrdenCompraLlantas(ref orden);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        txtFolioCompra.Tag = orden;
                        txtFolioCompra.Text = orden.CSERIEDOCUMENTO.ToString() + orden.CFOLIO.ToString();
                        mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult { valor = 1, mensaje = "ERROR AL GUARDAR LA OPERACIÓN EN EL COMERCIAL" };
                }                
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
            finally
            {
                Cursor = Cursors.Arrow;
                classOrden = null;
            }
        }

        private OrdenCompra mapform()
        {
            try
            {
                OrdenCompra orden = new OrdenCompra()
                {
                    idOrdenCompra = 0,
                    empresa = cbxEmpresas.empresaSelected,
                    estatus = "ACT",
                    fecha = DateTime.Now,
                    CIDDOCUMENTO = 0,
                    folioFactura = "",
                    proveedor = (Proveedor)cbxProveedor.SelectedItem
                };
                orden.listaDetalles = new List<DetalleOrdenCompra>();
                orden.listaDetalles = getDetalles();
                return orden;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }

        private List<DetalleOrdenCompra> getDetalles()
        {
            try
            {
                List<DetalleOrdenCompra> lista = new List<DetalleOrdenCompra>();
                List<StackPanel> listaSp = stpDetalles.Children.Cast<StackPanel>().ToList();
                foreach (StackPanel stp in listaSp)
                {
                    DetalleOrdenCompra detalle = new DetalleOrdenCompra();
                    detalle.idDetalleCompra = 0;
                    detalle.llantaProducto = (LLantasProducto)stp.Tag;                    
                    foreach (object item in stp.Children.Cast<object>().ToList())
                    {
                        if (item is controlDecimal)
                        {
                            detalle.cantidad = ((controlDecimal)item).valor;
                            detalle.pendientes = ((controlDecimal)item).valor;
                        }
                        if (item is controlUnidadesTransporte)
                        {
                            detalle.unidadTransporte = ((controlUnidadesTransporte)item).unidadTransporte;
                        }
                    }
                    lista.Add(detalle);
                }
                return lista;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }
    }
}
