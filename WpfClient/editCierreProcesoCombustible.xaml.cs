﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editConciliacionDiesel.xaml
    /// </summary>
    public partial class editCierreProcesoCombustible : ViewBase
    {
        public editCierreProcesoCombustible()
        {
            InitializeComponent();
        }
        Usuario usuario = null;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                nuevo();
                Cursor = Cursors.Wait;
                usuario = mainWindow.usuario;
                ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
                ctrZonaOperativa.cargarControl(usuario);
                txtFechaFin.Value = DateTime.Now;

            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public override void nuevo()
        {
            base.nuevo();
            limpiar();
            //txtFechaFin.Value = DateTime.Now;
            if (txtNombreTanque.Tag != null)
            {
                buscarFechaInicio((txtNombreTanque.Tag as TanqueCombustible).idTanqueCombustible);
            }
            else
            {
                txtFechaInicio.Clear();
                txtFechaInicio.Tag = null;
            }
            txtFechaFin.Tag = null;
            
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
        }
        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                limpiar();
                Cursor = Cursors.Wait;
                txtFechaInicio.Clear();
                //txtFechaFin.Value = DateTime.Now;
                if (ctrZonaOperativa.zonaOperativa != null)
                {
                    OperationResult result = new TanqueCombustibleSvc().getTanqueByZonaOperativa(this.ctrZonaOperativa.zonaOperativa);
                    if (result.typeResult == ResultTypes.success)
                    {
                        TanqueCombustible tanque = result.result as TanqueCombustible;
                        this.txtNombreTanque.Tag = tanque;
                        this.txtNombreTanque.Text = tanque.nombre;
                        buscarFechaInicio(tanque.idTanqueCombustible);
                    }
                    else
                    {
                        this.txtNombreTanque.Tag = null;
                        this.txtNombreTanque.Clear();
                        btnBuscar.IsEnabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

       

        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                
                limpiar();
                listaNivelesInicales = listaNivIni.FindAll(s=> s.idTanque != 0).ToList();
                Cursor = Cursors.Wait;
                if (txtNombreTanque.Tag != null)
                {
                    TanqueCombustible tanque = txtNombreTanque.Tag as TanqueCombustible;
                    buscarNivelesIniciales(tanque.idTanqueCombustible);
                }

            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }

        }
        void limpiar()
        {

            lvlResulFinal.ItemsSource = null;
            lvlResumen.ItemsSource = null;
            lvlDespachos.ItemsSource = null;
            lvlSuministros.ItemsSource = null;
            listaGeneralDespachos = new List<Despacho>();
            listaGeneralNewDespachos = new List<Despacho>();
            listaNivelesInicales = new List<NivelInicial>();
            lblSumaFinales.Content = "0.00";
            lblSumaLitros.Content = "0.00";
            lblTotal.Content = "0.00";
            lblSumaSuministros.Content = "0.00";

        }
        List<NivelInicial> listaNivelesInicales = new List<NivelInicial>();
        DateTime fechaInicial;
        DateTime fechaActual;
        private void buscarNivelesIniciales(int idTanque)
        {
            if (txtFechaFin.Value == null) return;


            //var resp = new SuministroCombustibleSvc().getNivelesIniciales(idTanque);
            //if (resp.typeResult == ResultTypes.success)
            //{
            //    List<NivelInicial> lista = resp.result as List<NivelInicial>;

            //    foreach (var item in lista)
            //    {
            //        listaNivelesInicales.Add(item);
            //    }
            //    lvlSuministros.ItemsSource = listaNivelesInicales;
            //    fechaInicial = listaNivelesInicales[0].fecha;

            //    txtFechaInicio.Text = fechaInicial.ToString("dd/MM/yy HH:ss");
            //    txtFechaInicio.Tag = fechaInicial;

            fechaActual = txtFechaFin.Value.Value;
            txtFechaFin.Tag = fechaActual;
            if (fechaActual < fechaInicial)
            {
                ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "LA FECHA FINAL TIENE QUE SER MAYOR A LA INICIAL"));
                return;
            }           
            buscarSuministros();
            //}
            //else
            //{
            //    ImprimirMensaje.imprimir(resp);
            //    return;
            //}
        }
        List<NivelInicial> listaNivIni = new List<NivelInicial>();
        void buscarFechaInicio(int idTanque)
        {
            try
            {
                Cursor = Cursors.Wait;
                listaNivIni = new List<NivelInicial>();
                var resp = new SuministroCombustibleSvc().getNivelesIniciales(idTanque);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<NivelInicial> lista = resp.result as List<NivelInicial>;

                    foreach (var item in lista)
                    {
                        listaNivelesInicales.Add(item);
                        listaNivIni.Add(item);
                    }
                    lvlSuministros.ItemsSource = listaNivelesInicales;
                    fechaInicial = listaNivelesInicales[0].fecha;

                    txtFechaInicio.Text = fechaInicial.ToString("dd/MM/yy HH:mm");
                    txtFechaInicio.Tag = fechaInicial;

                    btnBuscar.IsEnabled = true;
                }
                else
                {
                    txtFechaInicio.Clear();
                    txtFechaInicio.Tag = null;
                    btnBuscar.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void buscarSuministros()
        {
            List<NivelInicial> listaSumunistros = new List<NivelInicial>();
            OperationResult result2 = new SuministroCombustibleSvc().getDetalleSuministroCombustible(fechaInicial, fechaActual, (this.txtNombreTanque.Tag as TanqueCombustible).idTanqueCombustible, 0);
            this.lvlSuministros.ItemsSource = null;
            if (result2.typeResult == ResultTypes.success)
            {
                List<ReporteSuministrosCombustible> list3 = result2.result as List<ReporteSuministrosCombustible>;
                foreach (ReporteSuministrosCombustible combustible in list3)
                {
                    NivelInicial item = new NivelInicial
                    {
                        fecha = combustible.fecha,
                        cantidad = combustible.ltsTotales,
                        precio = combustible.precio,
                        factura = combustible.factura
                    };
                    listaNivelesInicales.Add(item);
                }
            }
            else if (result2.typeResult != ResultTypes.recordNotFound)
            {
                ImprimirMensaje.imprimir(result2);
                return;
            }
            

            if (listaNivelesInicales.Exists(s=> string.IsNullOrEmpty(s.factura)))
            {
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                imprimir(new OperationResult(ResultTypes.warning, "Se encontraron suministros sin facturas."));
            }
            else
            {
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo |ToolbarCommands.guardar);
            }

            listaSumunistros = (from g in listaNivelesInicales
                                orderby g.fecha
                                group g by new { g.fecha, g.precio, g.factura } into gr
                                select new NivelInicial
                                {
                                    fecha = (from s in gr select s.fecha).First<DateTime>(),
                                    cantidad = gr.Sum<NivelInicial>(s => s.cantidad),
                                    precio = (from s in gr select s.precio).First<decimal>(),
                                    factura = (from s in gr select s.factura).First<string>()
                                }).ToList<NivelInicial>();
            List<ListViewItem> listalvl = new List<ListViewItem>();
            foreach (var item in listaSumunistros)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.Selected += Lvl_Selected;
                ContextMenu menus = new ContextMenu();

                MenuItem menuItem = new MenuItem { Header = "EXPORTAR.", Tag = item };
                menus.Items.Add(menuItem);
                lvl.ContextMenu = menus;
                menuItem.Click += MenuItem_Click;
                listalvl.Add(lvl);
            }
            this.lvlSuministros.ItemsSource = listalvl;
            lblSumaSuministros.Content = listaSumunistros.Sum(s => s.cantidad).ToString("N4");
            buscarDespachos(listaSumunistros);
            //listaNivelesInicales = new List<NivelInicial>();
        }
        List<Despacho> listaGeneralDespachos = new List<Despacho>();
        List<Despacho> listaGeneralNewDespachos = new List<Despacho>();
        private void buscarDespachos(List<NivelInicial> listaSumunistros)
        {
            bool suministroAgotado = false;
            OperationResult result3 = new CargaCombustibleSvc().getDespachosCombustibleByFiltros(fechaInicial, fechaActual, (this.txtNombreTanque.Tag as TanqueCombustible).idTanqueCombustible, "%");
            if (result3.typeResult == ResultTypes.success)
            {
                List<Despacho> listaDespachos = result3.result as List<Despacho>;
                int i = 0;
                //decimal sumaDespachos = 0M;
                List<Despacho> listaNewDespachos = new List<Despacho>();
                int indiceDespachos = 0;
                foreach (Despacho despacho in listaDespachos)
                {
                    if (listaSumunistros.Count == 0)
                    {
                        suministroAgotado = true;
                    }
                    if (despacho.noVale == 40187)
                    {

                    }
                    if (!suministroAgotado)
                    {
                        listaSumunistros[i].cantidadFinal -= despacho.despachado;
                    }
                    else
                    {
                        listaNewDespachos.Add(new Despacho(despacho) { precio = 0, factura = string.Empty });
                        continue;
                    }
                    
                    if (listaSumunistros[i].cantidadFinal > 0)
                    {
                        if (i > (listaSumunistros.Count))
                        {
                            suministroAgotado = true;
                        }
                        despacho.precio = suministroAgotado ? 0 : listaSumunistros[i].precio;
                        despacho.factura = suministroAgotado ? string.Empty : listaSumunistros[i].factura;
                        listaNewDespachos.Add(despacho);
                    }
                    else
                    {
                        if (listaSumunistros.Count == 1)
                        {
                            suministroAgotado = true;
                        }
                        
                        //if ((i+1) >= (listaSumunistros.Count))
                        //{
                        //    suministroAgotado = true;
                        //}
                        try
                        {
                            var valor = listaSumunistros[i+1].cantidad;
                        }
                        catch (Exception ex)
                        {
                            suministroAgotado = true;
                        }
                        decimal diferencia = Math.Abs(listaSumunistros[i].cantidadFinal);
                        decimal _despacho = despacho.despachado - diferencia;

                        decimal importe1 = diferencia * listaSumunistros[i].precio;
                        decimal importe2 = suministroAgotado ? 0 : _despacho * listaSumunistros[i + 1].precio;

                        //if (suministroAgotado)
                        //{
                        //    listaNewDespachos.Add(new Despacho(despacho) { precio = 0, despachado = _despacho, factura = string.Empty });
                        //}
                        //else
                        //{
                            listaNewDespachos.Add(new Despacho(despacho) { precio = listaSumunistros[i].precio, despachado = _despacho, factura = listaSumunistros[i].factura });
                            listaNewDespachos.Add(new Despacho(despacho) { precio = (suministroAgotado ? 0 : listaSumunistros[i + 1].precio), despachado = diferencia, factura = (suministroAgotado ? string.Empty : listaSumunistros[i + 1].factura) });
                        //}
                        

                        despacho.precio = (importe1 + importe2) / despacho.despachado;
                        despacho.factura = string.Format("{0},{1}", listaSumunistros[i].factura,  (suministroAgotado ? string.Empty : listaSumunistros[i + 1].factura));

                      
                        i++;
                        if (suministroAgotado)
                        {
                            i--;
                        }
                        if (!suministroAgotado)
                        {
                            listaSumunistros[i].cantidadFinal -= diferencia;
                        }
                        
                    }
                    indiceDespachos++;
                }
                listaGeneralDespachos = listaNewDespachos;
                lblContador.Content = listaGeneralDespachos.Count.ToString("N0");
                listaGeneralNewDespachos = listaDespachos;
                this.lvlDespachos.ItemsSource = listaNewDespachos;
                List<GroupDespachos> list5 = (from p in listaDespachos
                                              group p by p.cliente into g
                                              select new GroupDespachos
                                              {
                                                  cliente = (from s in g select s.cliente).First<string>(),
                                                  lts = g.Sum<Despacho>(s => s.despachado)
                                              }).ToList<GroupDespachos>();
                this.lvlResumen.ItemsSource = list5;
                this.lblTotal.Content = listaDespachos.Sum<Despacho>(s => s.despachado).ToString("N4");

                lvlResulFinal.ItemsSource = listaSumunistros.FindAll(s => s.cantidadFinal > 0);
                lblSumaFinales.Content = listaSumunistros.FindAll(s => s.cantidadFinal > 0).Sum(s => s.cantidadFinal).ToString("N4");
                lblSumaLitros.Content = listaGeneralDespachos.Sum(s => s.despachado).ToString("N4");

                if (suministroAgotado)
                {
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                    imprimir(new OperationResult(ResultTypes.warning, "SALDO DE INVENTARIO INSUFICIENTE"));
                }
            }
            else if (result3.typeResult != ResultTypes.recordNotFound)
            {
                ImprimirMensaje.imprimir(result3);
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    MenuItem menuItem = sender as MenuItem;
                    NivelInicial nivelInicial = menuItem.Tag as NivelInicial;
                    List<Despacho> listaDespacho = listaGeneralDespachos.FindAll(s => s.factura == nivelInicial.factura);

                    List<NivelInicial> listaNiveles = new List<NivelInicial> { nivelInicial };
                    new ReportView().exportarDespachosLitos(listaDespacho, listaNiveles);

                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }

        }

        private void LvlSuministros_GotFocus(object sender, RoutedEventArgs e)
        {

        }

        private void LvlSuministros_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                lvlSuministros.SelectedItems.Clear();
                lvlDespachos.ItemsSource = listaGeneralDespachos;
                decimal sumaLitros = listaGeneralDespachos.Sum(s => s.despachado);
                lblSumaLitros.Content = sumaLitros.ToString("N4");
                lblContador.Content = listaGeneralDespachos.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;

                DateTime fecha = fechaActual;
                List<NivelInicial> lista = lvlResulFinal.ItemsSource as List<NivelInicial>;

                CierreProcesoCombustible cierreProceso = new CierreProcesoCombustible
                {
                    idCierreProcesosCombustible = 0,
                    zonaOperativa = ctrZonaOperativa.zonaOperativa,
                    tanque = txtNombreTanque.Tag as TanqueCombustible,
                    usuario = usuario.nombreUsuario,
                    fechaInicio = fechaInicial,
                    fechaFin = fechaActual
                };

                var resp = new SuministroCombustibleSvc().saveNivelesIniciales(fecha, lista, (txtNombreTanque.Tag as TanqueCombustible).idTanqueCombustible, listaGeneralNewDespachos, cierreProceso);
                if (resp.typeResult == ResultTypes.success)
                {
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    ListViewItem lvl = sender as ListViewItem;
                    NivelInicial nivelInicial = lvl.Content as NivelInicial;
                    var listaDespacho = listaGeneralDespachos.FindAll(s => s.factura.Contains(nivelInicial.factura));
                    lvlDespachos.ItemsSource = listaDespacho;
                    decimal sumaLitros = listaDespacho.Sum(s => s.despachado);
                    lblSumaLitros.Content = sumaLitros.ToString("N4");
                    lblContador.Content = listaDespacho.Count.ToString("N0");
                }
                else
                {
                    lvlDespachos.ItemsSource = listaGeneralDespachos;
                    lblContador.Content = listaGeneralDespachos.Count.ToString("N0");
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void Exportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.lvlDespachos.ItemsSource != null)
                {
                    base.Cursor = Cursors.Wait;
                    List<NivelInicial> listaNiveles = (this.lvlSuministros.ItemsSource as List<ListViewItem>).Select(s => s.Content as NivelInicial).ToList();
                    new ReportView().exportarDespachosLitos(listaGeneralDespachos, listaNiveles);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
    }
}
