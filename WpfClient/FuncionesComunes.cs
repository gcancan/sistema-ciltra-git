﻿using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    public static class FuncionesComunes
    {
        public static List<ProductoInsumo> convertirProductos(List<ProductosCOM> listaProductosCom)
        {
            try
            {
                List<ProductoInsumo> listaNew = new List<ProductoInsumo>();
                foreach (ProductosCOM producto in listaProductosCom)
                {
                    listaNew.Add(new ProductoInsumo(producto));
                }
                return listaNew;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }
    }
}
