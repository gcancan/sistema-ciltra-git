﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para reporteUltimosKm.xaml
    /// </summary>
    public partial class reporteUltimosKm : ViewBase
    {
        public reporteUltimosKm()
        {
            InitializeComponent();
        }
        Usuario usuario = null;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            usuario = mainWindow.usuario;
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresa.loaded(usuario);

            cbxZonasOperativas.SelectedItem = cbxZonasOperativas.Items.Cast<ZonaOperativa>().ToList().Find(s => s.idZonaOperativa == usuario.zonaOperativa.idZonaOperativa);
            cbxOperacion.SelectedItem = cbxOperacion.Items.Cast<OperacionFletera>().ToList().Find(s => s.cliente.clave == usuario.cliente.clave);

        }
        Empresa empresa = null;
        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            empresa = ctrEmpresa.empresaSelected;
            chcTodosZonaOperativa.IsChecked = false;
            chcTodosOperacion.IsChecked = false;
            cargarZonasOperativas();
        }
        List<ZonaOperativa> listaZonaOpertiva = new List<ZonaOperativa>();
        void cargarZonasOperativas()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new ZonaOperativaSvc().getAllZonasOperativas();
                if (resp.typeResult == ResultTypes.success)
                {
                    listaZonaOpertiva = resp.result as List<ZonaOperativa>;
                    cbxZonasOperativas.ItemsSource = listaZonaOpertiva;
                }
                else
                {
                    cbxZonasOperativas.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {                
                Dispatcher.Invoke(() =>
                {
                    
                });
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        List<OperacionFletera> listaOperaciones = new List<OperacionFletera>();
        private void CbxZonasOperativas_ItemSelectionChanged(object sender, Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventArgs e)
        {
            try
            {
                cbxOperacion.ItemsSource = null;
                chcTodosOperacion.IsChecked = false;
                var listaId = (from s in this.cbxZonasOperativas.SelectedItems.Cast<ZonaOperativa>().ToList<ZonaOperativa>() select s.idZonaOperativa.ToString()).ToList<string>();
                if (listaId != null)
                {
                    var resp = new ClienteSvc().getClientesByZonasOperativas(listaId, empresa.clave);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listaOperaciones = resp.result as List<OperacionFletera>;
                        cbxOperacion.ItemsSource = listaOperaciones;
                    }
                    else if (resp.typeResult != ResultTypes.recordNotFound)
                    {
                        //ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception ex)
            {
                //ImprimirMensaje.imprimir(ex);
                Dispatcher.Invoke(() =>
                {
            
                });
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                CheckBox chc = sender as CheckBox;
                switch (chc.Name)
                {
                    case "chcTodosZonaOperativa":
                        cbxZonasOperativas.SelectedItems.Clear();
                        if (chc.IsChecked.Value)
                        {
                            cbxZonasOperativas.SelectAll();
                        }
                        break;
                    case "chcTodosOperacion":
                        cbxOperacion.SelectedItems.Clear();
                        if (chc.IsChecked.Value)
                        {
                           cbxOperacion.SelectAll();
                        }
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Dispatcher.Invoke(() =>
                {
                    
                });
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CbxOperacion_ItemSelectionChanged(object sender, Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventArgs e)
        {

        }

        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                int idEmpresa = ctrEmpresa.empresaSelected.clave;
                List<string> listaZonasStr = cbxZonasOperativas.SelectedItems.Cast<ZonaOperativa>().ToList().Select(s => s.idZonaOperativa.ToString()).ToList();
                List<string> listaOperacionesStr = cbxOperacion.SelectedItems.Cast<OperacionFletera>().ToList().Select(s => s.cliente.clave.ToString()).ToList();
                var resp = new UltimosKmUnidadesSvc().getUltimosKmUnidades(idEmpresa, listaZonasStr, listaOperacionesStr);
                if (resp.typeResult == ResultTypes.success)
                {
                    lvlResultados.ItemsSource = resp.result as List<UltimosKmUnidades>;
                }
                else
                {
                    lvlResultados.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                List<UltimosKmUnidades> listaKm = new List<UltimosKmUnidades>();
                listaKm = lvlResultados.ItemsSource.Cast<UltimosKmUnidades>().ToList();
                new ReportView().exportarListaUltimosKM(listaKm);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
