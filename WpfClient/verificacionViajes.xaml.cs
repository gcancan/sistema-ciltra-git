﻿using Core.Models;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para verificacionViajes.xaml
    /// </summary>
    public partial class verificacionViajes : Window
    {
        private Viaje viaje;
        private bool mostrarOtrosDatos;
        public verificacionViajes()
        {
            this.viaje = new Viaje();
            this.InitializeComponent();
        }
        public verificacionViajes(Viaje viaje, bool mostrarOtrosDatos = false)
        {
            this.viaje = new Viaje();
            this.InitializeComponent();
            this.viaje = viaje;
            this.mostrarOtrosDatos = mostrarOtrosDatos;
        }
        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            base.DialogResult = true;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                this.lblCliente.Content = this.viaje.cliente.nombre;
                this.lblChofer.Content = this.viaje.operador.nombre;
                this.lblTractor.Content = this.viaje.tractor.clave;
                this.lblRemolque1.Content = this.viaje.remolque?.clave;
                this.lblDolly.Content = (this.viaje.dolly == null) ? string.Empty : this.viaje.dolly.clave;
                this.lblRemolque2.Content = (this.viaje.remolque2 == null) ? string.Empty : this.viaje.remolque2.clave;
                this.lblFecha.Content = this.viaje.FechaHoraViaje.ToString("dd/MM/yyyy HH:mm");
                this.lblFechaPrograma.Content = this.viaje.FechaPrograma.Value.ToString("dd/MM/yyyy");
                GridView view = (GridView)this.lvlDetalles.View;
                GridViewColumnCollection columns = view.Columns;
                if (this.mostrarOtrosDatos)
                {
                    GridViewColumn column1 = new GridViewColumn
                    {
                        Header = "Remisión",
                        Width = 100.0,
                        DisplayMemberBinding = new Binding("remision")
                    };
                    view.Columns.Add(column1);
                }
                GridViewColumn item = new GridViewColumn
                {
                    Header = "Destino",
                    Width = 180.0,
                    DisplayMemberBinding = new Binding("zonaSelect.descripcion")
                };
                view.Columns.Add(item);
                GridViewColumn column3 = new GridViewColumn
                {
                    Header = "Producto",
                    Width = 180.0,
                    DisplayMemberBinding = new Binding("producto.descripcion")
                };
                view.Columns.Add(column3);
                GridViewColumn column4 = new GridViewColumn
                {
                    Header = "Vol Prog",
                    Width = 80.0
                };
                Binding binding1 = new Binding("volumenCarga")
                {
                    StringFormat = "N4"
                };
                column4.DisplayMemberBinding = binding1;
                view.Columns.Add(column4);
                if (this.mostrarOtrosDatos)
                {
                    GridViewColumn column5 = new GridViewColumn
                    {
                        Header = "Vol Descarga",
                        Width = 80.0
                    };
                    Binding binding2 = new Binding("volumenDescarga")
                    {
                        StringFormat = "N4"
                    };
                    column5.DisplayMemberBinding = binding2;
                    view.Columns.Add(column5);
                }
                this.lvlDetalles.ItemsSource = this.viaje.listDetalles;
                lblCont.Content = this.viaje.noViaje.ToString();
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }
    }
}
