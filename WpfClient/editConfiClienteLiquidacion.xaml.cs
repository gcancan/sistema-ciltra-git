﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Interfaces;
using CoreFletera.Interfaces;
using Core.Models;
using CoreFletera.Models;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editConfiClienteLiquidacion.xaml
    /// </summary>
    public partial class editConfiClienteLiquidacion : ViewBase
    {
        public editConfiClienteLiquidacion()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            var resCliente = new ClienteSvc().getClientesALLorById(mainWindow.empresa.clave, 0, true);
            if (resCliente.typeResult == ResultTypes.success)
            {
                cbxCliente.ItemsSource = (List<Cliente>)resCliente.result;
            }
            var lisDias = Enum.GetValues(typeof(eDiasSemana));
            cbxDiaInicial.ItemsSource = lisDias;
            cbxDiaFinal.ItemsSource = lisDias;

            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);
            evento = true;
        }
        public override OperationResult guardar()
        {
            var config = mapForm();
            if (config == null)
            {
                return new OperationResult { valor = 2, mensaje = "Ocurrio un error al leer los datos de la pantalla"};
            }

            var resp = new LiquidacionPegasoSvc().saveConfiguracionCliente(ref config);
            if (resp.typeResult == ResultTypes.success)
            {
                mainWindow.habilitar = true;
                mapForm(config);
            }
            return resp;            
        }

        ConfiguracionClienteLiquidacion mapForm()
        {
            try
            {
                ConfiguracionClienteLiquidacion config = new ConfiguracionClienteLiquidacion
                {
                    idConfig = 0,
                    cliente = (Cliente)cbxCliente.SelectedItem,
                    diaInicio = (int)((eDiasSemana)cbxDiaInicial.SelectedItem),  
                    horaInicio = txtHoraInicial.Text,                  
                    diaFin = (int)((eDiasSemana)cbxDiaFinal.SelectedItem),
                    horaFin = txtHoraFinal.Text,
                    noViajes = txtViajes.valor
                };               

                return config;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        bool evento = true;
        private void cbxCliente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxCliente.SelectedItem != null)
            {
                if (evento)
                {
                    var res = new LiquidacionPegasoSvc().getCongByIdCliente(((Cliente)cbxCliente.SelectedItem).clave);
                    if (res.typeResult == ResultTypes.success)
                    {
                        mapForm(res.result as ConfiguracionClienteLiquidacion);
                    }
                    else if (res.typeResult == ResultTypes.recordNotFound)
                    {
                        mapForm(null);
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(res);
                    }
                }                
            }
            else
            {
                mapForm(null);
            }
            evento = true;
        }

        private void mapForm(ConfiguracionClienteLiquidacion config)
        {
            if (config != null)
            {
                evento = false;
                int i = 0;
                foreach (Cliente item in cbxCliente.Items)
                {
                    if (config.cliente.clave == item.clave)
                    {
                        cbxCliente.SelectedIndex = i;
                        break;
                    }i++;
                }
                selectCombos(config);
                txtHoraInicial.Text = config.horaInicio.ToString();
                txtHoraFinal.Text = config.horaFin.ToString();
                txtViajes.valor = config.noViajes;
                mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            }
            else
            {
                evento = false;
                //cbxCliente.SelectedItem = null;
                selectCombos(null);
                txtHoraInicial.Clear();
                txtHoraFinal.Clear();
                txtViajes.valor = 0;
                mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            }
            evento = true;
        }

        private void selectCombos(ConfiguracionClienteLiquidacion config)
        {
            if (config != null)
            {
                int i = 0;
                foreach (eDiasSemana item in cbxDiaInicial.Items)
                {
                    if (config.diaInicio == (int)item)
                    {
                        cbxDiaInicial.SelectedIndex = i;
                        break;
                    }
                    i++;
                }
                i = 0;
                foreach (eDiasSemana item in cbxDiaFinal.Items)
                {
                    if (config.diaFin == (int)item)
                    {
                        cbxDiaFinal.SelectedIndex = i;
                        break;
                    }
                    i++;
                }
            }
            else
            {
                cbxDiaInicial.SelectedItem = null;
                cbxDiaFinal.SelectedItem = null;
            }
        }
    }
}
