﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para controlMotivosCancelacion.xaml
    /// </summary>
    public partial class controlMotivosCancelacion : UserControl
    {
        enumProcesos tipoProceso;
        public controlMotivosCancelacion()
        {
            InitializeComponent();
        }
        public controlMotivosCancelacion(enumProcesos tipoProceso)
        {
            InitializeComponent();
            this.tipoProceso = tipoProceso;
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                cbxMotivos.SelectionChanged += ComboBox_SelectionChanged;
                buscarMotivosCancelacion();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public MotivoCancelacion motivoCancelacion
        {
            get
            {
                if (cbxMotivos.SelectedItem != null)
                {
                    return cbxMotivos.SelectedItem as MotivoCancelacion;
                }
                return null;
            }
            set
            {
                MotivoCancelacion motivo = value;
                if (motivo == null)
                {
                    cbxMotivos.SelectedItem = null;
                }
                else
                {
                    cbxMotivos.SelectedItem = cbxMotivos.ItemsSource.Cast<MotivoCancelacion>().ToList().Find(s => s.idMotivoCancelacion == motivo.idMotivoCancelacion);
                }

            }
        }
        public string obsMotivo
        {
            get =>
                txtObservacion.Text.Trim();
            set
            {
                string motivo = value;
                txtObservacion.Text = motivo;
            }
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender != null)
            {
                ComboBox comboBox = sender as ComboBox;
                if (comboBox.SelectedItem != null)
                {
                    MotivoCancelacion motivo = comboBox.SelectedItem as MotivoCancelacion;
                    if (motivo.requiereObservacion)
                    {
                        txtObservacion.IsEnabled = true;
                        motivoCancelacion = motivo;
                    }
                    else
                    {
                        txtObservacion.IsEnabled = false;
                        motivoCancelacion = motivo;
                        txtObservacion.Clear();
                    }
                }
                else
                {
                    txtObservacion.IsEnabled = false;
                    motivoCancelacion = null;
                    txtObservacion.Clear();
                }

            }
        }
        void buscarMotivosCancelacion()
        {
            try
            {
                cbxMotivos.ItemsSource = null;
                var resp = new MotivoCancelacionSvc().getAllMotivosByProceso(tipoProceso);
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxMotivos.ItemsSource = resp.result as List<MotivoCancelacion>;
                }
                else
                {
                    this.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                this.IsEnabled = false;
            }
        }
        public void cargarControl()
        {
            buscarMotivosCancelacion();
        }
    }
}
