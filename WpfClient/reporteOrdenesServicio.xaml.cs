﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para reporteOrdenesServicio.xaml
    /// </summary>
    public partial class reporteOrdenesServicio : ViewBase
    {
        public reporteOrdenesServicio()
        {
            InitializeComponent();
        }
        Usuario usuario = null;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                this.usuario = mainWindow.usuario;
                ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
                ctrEmpresa.loaded(mainWindow.empresa, (new PrivilegioSvc().consultarPrivilegio("isAdmin", usuario.idUsuario).typeResult == ResultTypes.success));

                ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
                ctrZonaOperativa.cargarControl(usuario.zonaOperativa, usuario.idUsuario);

                cbxTipoServicio.ItemsSource = Enum.GetValues(typeof(enumTipoOrden));
                cbxTipoOrdenServicio.ItemsSource = Enum.GetValues(typeof(enumTipoOrdServ));

                List<EstatusOrdenesServicio> listaEstatus = new List<EstatusOrdenesServicio>
                {
                    new EstatusOrdenesServicio { descripcion = "CAPTURA", clave = "CAP"},
                    new EstatusOrdenesServicio { descripcion = "RECEPCIÓN", clave = "REC" },
                    new EstatusOrdenesServicio { descripcion = "TERMINADO", clave = "TER" },
                    new EstatusOrdenesServicio { descripcion = "CANCELADO", clave = "CAN" }
                };

                cbxEstatus.ItemsSource = listaEstatus;
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);

                stpOperacion.IsEnabled = new PrivilegioSvc().consultarPrivilegio("ADMIN_COMBO_CLIENTES", usuario.idUsuario).typeResult == ResultTypes.success;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                this.IsEnabled = false;
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                cbxUnidades.ItemsSource = null;
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    ComboBox combo = sender as ComboBox;
                    if (combo.SelectedItem != null)
                    {
                        //var resp = new UnidadTransporteSvc().getUnidadesALLorById(ctrEmpresa.empresaSelected.clave);
                        //if (resp.typeResult == ResultTypes.success)
                        //{
                        //    cbxUnidades.ItemsSource = resp.result as List<UnidadTransporte>;
                        //}
                        cargarOperacion();
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cargarOperacion();
            if (ctrEmpresa.empresaSelected.clave != 1)
            {
                cargarUnidades();
            }

        }
        void cargarOperacion()
        {
            try
            {
                Cursor = Cursors.Wait;
                cbxOperacion.ItemsSource = null;

                if (ctrEmpresa.empresaSelected.clave == 1)
                {
                    stpOperacion.Visibility = Visibility.Visible;
                    if (ctrZonaOperativa.zonaOperativa != null)
                    {
                        var resp = new ClienteSvc().getClientesByZonasOperativas(new List<string> { (ctrZonaOperativa.zonaOperativa.idZonaOperativa.ToString()) }, ctrEmpresa.empresaSelected.clave);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            var listaOperaciones = resp.result as List<OperacionFletera>;
                            cbxOperacion.ItemsSource = listaOperaciones;
                            var sel = cbxOperacion.ItemsSource.Cast<OperacionFletera>().ToList().Find(s => s.cliente.clave == usuario.cliente.clave);
                            if (sel != null)
                            {
                                cbxOperacion.SelectedItems.Add(sel);
                            }

                        }
                        else if (resp.typeResult != ResultTypes.recordNotFound)
                        {
                            imprimir(resp);
                        }
                    }
                }
                else
                {
                    stpOperacion.Visibility = Visibility.Hidden;
                    cargarUnidades();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CbxOperacion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void ChcTodosUnidades_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    CheckBox check = sender as CheckBox;
                    if (check.IsChecked.Value)
                    {
                        cbxUnidades.SelectAll();
                    }
                    else
                    {
                        cbxUnidades.SelectedItems.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void ChcTodosTipoServicio_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    CheckBox check = sender as CheckBox;
                    if (check.IsChecked.Value)
                    {
                        foreach (var item in cbxTipoServicio.ItemsSource)
                        {
                            cbxTipoServicio.SelectedItems.Add(item);
                        }
                    }
                    else
                    {
                        cbxTipoServicio.SelectedItems.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void ChcTodosTipoOrdServ_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    CheckBox check = sender as CheckBox;
                    if (check.IsChecked.Value)
                    {
                        foreach (var item in cbxTipoOrdenServicio.ItemsSource)
                        {
                            cbxTipoOrdenServicio.SelectedItems.Add(item);
                        }
                    }
                    else
                    {
                        cbxTipoOrdenServicio.SelectedItems.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public class EstatusOrdenesServicio
        {
            public string descripcion { get; set; }
            public string clave { get; set; }
        }

        private void ChcTodosEstatus_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    CheckBox check = sender as CheckBox;
                    if (check.IsChecked.Value)
                    {
                        foreach (var item in cbxEstatus.ItemsSource)
                        {
                            cbxEstatus.SelectedItems.Add(item);
                        }
                    }
                    else
                    {
                        cbxEstatus.SelectedItems.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }

        }

        private void ChcTodosOperaciones_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    CheckBox check = sender as CheckBox;
                    if (check.IsChecked.Value)
                    {
                        cbxOperacion.SelectAll();
                    }
                    else
                    {
                        cbxOperacion.SelectedItems.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lvlOs.ItemsSource = null;
                Cursor = Cursors.Wait;

                List<string> listaClientes = cbxOperacion.SelectedItems.Cast<OperacionFletera>().ToList().Select(s => s.cliente.clave.ToString()).ToList();
                List<string> listaUnidades = cbxUnidades.SelectedItems.Cast<UnidadTransporte>().ToList().Select(s => s.clave).ToList();
                List<string> listaTipoOrden = cbxTipoServicio.SelectedItems.Cast<enumTipoOrden>().ToList().Select(s => ((int)s).ToString()).ToList();
                List<string> listaTipoOrdenServicio = cbxTipoOrdenServicio.SelectedItems.Cast<enumTipoOrdServ>().ToList().Select(s => ((int)s).ToString()).ToList();
                List<string> listaEstatus = cbxEstatus.SelectedItems.Cast<EstatusOrdenesServicio>().ToList().Select(s => s.clave).ToList();

                var resp = new SolicitudOrdenServicioSvc().getReporteOrdenesServicio(ctrEmpresa.empresaSelected.clave, ctrFechas.fechaInicial,
                    ctrFechas.fechaFinal, listaClientes, listaUnidades, listaTipoOrden, listaTipoOrdenServicio, listaEstatus);
                if (resp.typeResult == ResultTypes.success)
                {
                    lvlOs.ItemsSource = resp.result as List<ReporteOrdenesServicio>;
                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                Cursor = Cursors.Wait;

                if (lvlOs.ItemsSource != null)
                {
                    new ReportView().exportarReporteOrdenesServicios(lvlOs.ItemsSource as List<ReporteOrdenesServicio>);
                }

            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CbxOperacion_ItemSelectionChanged(object sender, Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventArgs e)
        {
            if (ctrEmpresa.empresaSelected.clave == 1)
            {
                if (cbxOperacion.SelectedItems != null)
                {
                    cargarUnidades();
                }

            }
        }
        void cargarUnidades()
        {
            try
            {
                List<UnidadTransporte> listaUnidades = new List<UnidadTransporte>();
                chcTodosUnidades.IsChecked = false;
                cbxUnidades.SelectedItems.Clear();
                Cursor = Cursors.Wait;

                if (ctrEmpresa.empresaSelected.clave == 1)
                {
                    if (cbxOperacion.SelectedItems.Count >= 0)
                    {
                        foreach (OperacionFletera operacion in cbxOperacion.SelectedItems)
                        {
                            var resp = new FlotaSvc().getFlota(operacion);
                            if (resp.typeResult == ResultTypes.success)
                            {
                                foreach (Flota flota in resp.result as List<Flota>)
                                {
                                    if (flota.tractor != null)
                                    {
                                        listaUnidades.Add(flota.tractor);
                                    }
                                    if (flota.remolque1 != null)
                                    {
                                        listaUnidades.Add(flota.remolque1);
                                    }
                                    if (flota.dolly != null)
                                    {
                                        listaUnidades.Add(flota.dolly);
                                    }
                                    if (flota.remolque2 != null)
                                    {
                                        listaUnidades.Add(flota.remolque2);
                                    }
                                }
                            }
                        }
                    }

                }
                else
                {
                    if (ctrZonaOperativa.zonaOperativa != null)
                    {
                        OperacionFletera operacion = new OperacionFletera
                        {
                            empresa = ctrEmpresa.empresaSelected,
                            cliente = null,
                            zonaOperativa = ctrZonaOperativa.zonaOperativa
                        };
                        var resp = new FlotaSvc().getFlota(operacion);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            foreach (Flota flota in resp.result as List<Flota>)
                            {
                                if (flota.tractor != null)
                                {
                                    listaUnidades.Add(flota.tractor);
                                }
                                if (flota.remolque1 != null)
                                {
                                    listaUnidades.Add(flota.remolque1);
                                }
                                if (flota.dolly != null)
                                {
                                    listaUnidades.Add(flota.dolly);
                                }
                                if (flota.remolque2 != null)
                                {
                                    listaUnidades.Add(flota.remolque2);
                                }
                            }
                        }
                    }
                }
                cbxUnidades.ItemsSource = listaUnidades.OrderBy(s => s.clave);


            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
