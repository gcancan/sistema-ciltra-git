﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para vistaFacturaProveedorDiesel.xaml
    /// </summary>
    public partial class vistaFacturaProveedorDiesel : Window
    {
        public vistaFacturaProveedorDiesel()
        {
            InitializeComponent();
        }
        FacturaProveedorCombustible factura; 
        public void cargarControles(FacturaProveedorCombustible factura)
        {
            try
            {
                //Cursor = Cursors.Wait;
                this.factura = factura;
                cargar();
                ShowDialog();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void cargar()
        {
            txtFactura.Text = factura.factura;
            txtFolioFiscal.Text = factura.folioFiscal;
            txtProveedor.Text = factura.proveedor.nombre;
            dtpFechaFactura.Text = factura.fechaFactura.ToString("dd/MM/yy");
            txtSumaLitros.Text = factura.litros.ToString("N4");
            txtSubTotalIEPS.Text = factura.subTotalIEPS.ToString("C4");
            txtSubTotal.Text = factura.subTotal.ToString("C4");
            txtIVA.Text = factura.iva.ToString("C4");
            txtSumaIEPS.Text = factura.IEPS.ToString("C4");
            txtSumaImportes.Text = factura.importe.ToString("C4");
            lvlResultados.ItemsSource = factura.listaDetalles;
            lblContador.Content = factura.listaDetalles.Count.ToString("N0");
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                new ReportView().exportarFacturaProveedorDiesel(factura);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
