﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para catUnidadesTransporte.xaml
    /// </summary>
    public partial class catUnidadesTransporte : ViewBase
    {
        public catUnidadesTransporte()
        {
            InitializeComponent();
        }
        TipoAccionUnidad tipoAccion = TipoAccionUnidad.NUEVO;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;

                ctrEmpresa.loaded(mainWindow.usuario.empresa, (new PrivilegioSvc().consultarPrivilegio("isAdmin", mainWindow.usuario.idUsuario).typeResult == ResultTypes.success));

                cbxTipoAdquisicion.ItemsSource = Enum.GetValues(typeof(enumTipoAdquisicion));

                var respTipoUnidad = new TipoUnidadSvc().getAllTipoUnidades();
                if (respTipoUnidad.typeResult == ResultTypes.success)
                {
                    cbxTipoUnidad.ItemsSource = (respTipoUnidad.result as List<TipoUnidad>).FindAll(S => S.activo);
                }
                else
                {
                    imprimir(respTipoUnidad);
                    this.IsEnabled = false;
                    return;
                }

                var respMarca = new MarcasSvc().getMarcasUnidades();
                if (respMarca.typeResult == ResultTypes.success)
                {
                    cbxMarca.ItemsSource = respMarca.result as List<Marca>;
                }
                else
                {
                    imprimir(respMarca);
                    this.IsEnabled = false;
                    return;
                }

                var respProveedores = new ProveedorSvc().getAllProveedoresUnidades();
                if (respProveedores.typeResult == ResultTypes.success)
                {
                    cbxProveedor.ItemsSource = respProveedores.result as List<ProveedorCombustible>;
                }
                else
                {
                    imprimir(respMarca);
                    //this.IsEnabled = false;
                    return;
                }

                cbxEstatus.ItemsSource = Enum.GetValues(typeof(enumEstatus));
                cbxEstatus.SelectedItem = enumEstatus.ACTIVO;

                nuevo();
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public override void nuevo()
        {
            base.nuevo();
            tipoAccion = TipoAccionUnidad.NUEVO;
            mapForm(null);
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                var val = validar();
                if (val.typeResult != ResultTypes.success)
                {
                    return val;
                }
                UnidadTransporte unidad = mapForm();
                if (unidad == null)
                {
                    return new OperationResult(ResultTypes.success, "ERROR AL LEER LA INFORMACION DE LA PANTALLA.");
                }
                var resp = new UnidadTransporteSvc().saveUnidadTransporte(ref unidad);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(unidad);
                    base.guardar();
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void mapForm(UnidadTransporte unidad)
        {
            try
            {
                if (unidad != null)
                {
                    tipoAccion = TipoAccionUnidad.EDITAR;
                    ctrClave.Text = unidad.clave;
                    ctrClave.Tag = unidad;
                    ctrClave.IsEnabled = false;
                    ctrEmpresa.seleccionarEmpresa(unidad.empresa);
                    cbxEstatus.SelectedItem = unidad.estatus == "ACT" ? enumEstatus.ACTIVO : enumEstatus.BAJA;
                    dtpFechaAdquisicion.Value = unidad.fechaAdquisicion;
                    cbxTipoAdquisicion.SelectedItem = unidad.tipoAdquisicion;
                    ctrOdometro.valor = unidad.odometro;
                    txtPlacas.Text = unidad.placas;
                    ctrAño.valor = unidad.anio;
                    cbxProveedor.SelectedItem = unidad.proveedor == null ? null :
                        cbxProveedor.ItemsSource.Cast<ProveedorCombustible>().ToList().Find(s => s.idProveedor == unidad.proveedor.idProveedor);
                    cbxTipoUnidad.SelectedItem = cbxTipoUnidad.ItemsSource.Cast<TipoUnidad>().ToList().Find(s => s.clave == unidad.tipoUnidad.clave);
                    ctrCapacidadTanque.valor = unidad.capacidadTanque;
                    cbxMarca.SelectedItem = unidad.marca == null ? null :
                        cbxMarca.ItemsSource.Cast<Marca>().ToList().Find(s => s.clave == unidad.marca.clave);
                    txtDocumento.Text = unidad.documento;
                    cbxModelos.SelectedItem = unidad.modelo == null ? null : ((cbxModelos.ItemsSource as List<Modelo>).Find(s => s.idModelo == unidad.modelo.idModelo));
                    ctrMttoProv.valor = unidad.mttoPrev;
                    txtVIN.Text = unidad.VIN;
                }
                else
                {
                    tipoAccion = TipoAccionUnidad.NUEVO;
                    ctrClave.Clear();
                    ctrClave.Tag = null;
                    ctrClave.IsEnabled = true;
                    cbxEstatus.SelectedItem = enumEstatus.ACTIVO;
                    dtpFechaAdquisicion.Value = DateTime.Now;
                    cbxTipoAdquisicion.SelectedItem = enumTipoAdquisicion.COMPRA;
                    ctrOdometro.valor = 0;
                    txtPlacas.Clear();
                    cbxProveedor.SelectedItem = null;
                    cbxTipoUnidad.SelectedItem = null;
                    ctrCapacidadTanque.valor = 0;
                    cbxMarca.SelectedItem = null;
                    cbxModelos.SelectedItem = null;
                    txtDocumento.Clear();
                    cbxProveedor.SelectedItem = null;
                    cbxMarca.SelectedItem = null;
                    ctrAño.valor = 0;
                    ctrMttoProv.valor = 0;
                    txtVIN.Clear();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        UnidadTransporte mapForm()
        {
            try
            {
                UnidadTransporte unidadAnterior = null;
                if (tipoAccion == TipoAccionUnidad.EDITAR)
                {
                    unidadAnterior = ctrClave.Tag as UnidadTransporte;
                }
                UnidadTransporte unidad = new UnidadTransporte
                {
                    empresa = ctrEmpresa.empresaSelected,
                    clave = ctrClave.Text.Trim(),
                    estatus = (enumEstatus)cbxEstatus.SelectedItem == enumEstatus.ACTIVO ? "ACT" : "BAJ",
                    fechaAdquisicion = dtpFechaAdquisicion.Value,
                    tipoAdquisicion = (enumTipoAdquisicion)cbxTipoAdquisicion.SelectedItem,
                    odometro = ctrOdometro.valor,
                    placas = txtPlacas.Text.Trim(),
                    proveedor = cbxProveedor.SelectedItem == null ? null : cbxProveedor.SelectedItem as ProveedorCombustible,
                    tipoUnidad = cbxTipoUnidad.SelectedItem as TipoUnidad,
                    capacidadTanque = ctrCapacidadTanque.valor,
                    marca = cbxMarca.SelectedItem as Marca,
                    modelo = cbxModelos.SelectedItem == null ? null : cbxModelos.SelectedItem as Modelo,
                    documento = txtDocumento.Text.Trim(),
                    tipoAccion = this.tipoAccion,
                    listaAjustes = new List<RegistroAjustes>(),
                    anio = ctrAño.valor,
                    mttoPrev = ctrMttoProv.valor,
                    VIN = txtVIN.Text.Trim()
                };
                unidad.listaAjustes = new List<RegistroAjustes>();
                if (tipoAccion == TipoAccionUnidad.EDITAR)
                {
                    unidad.listaAjustes = LLenarRegistroAjustes.getAjustesUnidadTransporte(unidadAnterior, unidad, mainWindow.usuario.nombreUsuario);
                }

                return unidad;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        OperationResult validar()
        {
            try
            {
                OperationResult result = new OperationResult() { valor = 0, mensaje = ("PARA CONTINUAR SE REQUIERE:" + Environment.NewLine) };

                if (string.IsNullOrEmpty(ctrClave.Text.Trim()))
                {
                    result.valor = 3;
                    result.mensaje += " * Proporcionar una clave para la unidad." + Environment.NewLine;
                }

                if (ctrOdometro.valor < 0)
                {
                    result.valor = 3;
                    result.mensaje += " * Proporcionar el odómetro." + Environment.NewLine;
                }

                //if (string.IsNullOrEmpty(txtPlacas.Text.Trim()))
                //{
                //    result.valor = 3;
                //    result.mensaje += " * Proporcionar las placas para la unidad." + Environment.NewLine;
                //}

                //if (cbxProveedor.SelectedItem == null)
                //{
                //    result.valor = 3;
                //    result.mensaje += " * Seleccionar un proveedor." + Environment.NewLine;
                //}

                if (cbxTipoUnidad.SelectedItem == null)
                {
                    result.valor = 3;
                    result.mensaje += " * Seleccionar un tipo de unidad." + Environment.NewLine;
                }

                if (cbxMarca.SelectedItem == null)
                {
                    result.valor = 3;
                    result.mensaje += " * Seleccionar una marca." + Environment.NewLine;
                }

                return result;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public override void buscar()
        {
            try
            {
                UnidadTransporte unidad = new selectUnidadTransporteV2().getUnidadAll();
                if (unidad != null)
                {
                    tipoAccion = TipoAccionUnidad.EDITAR;
                    mapForm(unidad);
                    base.buscar();
                    if (unidad.strEstatus == "BAJA")
                    {
                        mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.editar | ToolbarCommands.cerrar);
                    }
                }
                else
                {
                    nuevo();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        public override OperationResult eliminar()
        {
            try
            {
                Cursor = Cursors.Wait;

                UnidadTransporte unidad = ctrClave.Tag as UnidadTransporte;
                unidad.estatus = "BAJ";
                unidad.listaAjustes = new List<RegistroAjustes>();
                unidad.listaAjustes.Add(new RegistroAjustes
                {
                    idTabla = unidad.clave,
                    nombreColumna = "Estatus",
                    nombreTabla = "CatUnidadTrans",
                    valorAnterior = "ACT",
                    valorNuevo = "BAJ",
                    usuario = mainWindow.usuario.nombreUsuario
                });
                unidad.tipoAccion = TipoAccionUnidad.EDITAR;
                var resp = new UnidadTransporteSvc().saveUnidadTransporte(ref unidad);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(null);
                    base.eliminar();
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new UnidadTransporteSvc().getAllUnidadesTransV2();
                if (resp.typeResult == ResultTypes.success)
                {
                    new ReportView().exportarListaUnidadesTrans(resp.result as List<UnidadTransporte>);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CbxMarca_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                cbxModelos.ItemsSource = null;
                if (cbxMarca.SelectedItem == null)
                {
                    cbxModelos.ItemsSource = null;
                }
                else
                {
                    Marca marca = cbxMarca.SelectedItem as Marca;
                    var resp = new ModeloSvc().getAllModelosByMarca(marca.clave);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        cbxModelos.ItemsSource = resp.result as List<Modelo>;
                    }
                    else if (resp.typeResult != ResultTypes.recordNotFound)
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
