﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
using System.Globalization;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editLiquidaciones.xaml
    /// </summary>
    public partial class editLiquidaciones : ViewBase
    {
        public editLiquidaciones()
        {
            InitializeComponent();
        }
        bool privGuardar = false;
        bool privEditar = false;
        bool privConsultarDtpos = false;
        Usuario usuario = null;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                this.usuario = mainWindow.usuario;

                ctrEmpresa.loaded(usuario);
                //var pers = new OperadorSvc().getPersonal_v2()

                this.privGuardar = new PrivilegioSvc().consultarPrivilegio("CREAR_LIQUIDACIONES", usuario.idUsuario).typeResult == ResultTypes.success;
                this.privEditar = new PrivilegioSvc().consultarPrivilegio("EDITAR_LIQUIDACIONES", usuario.idUsuario).typeResult == ResultTypes.success;
                this.privConsultarDtpos = new PrivilegioSvc().consultarPrivilegio("CONSULTAR_TODOS_DETOS_LIQUIDACIONES", usuario.idUsuario).typeResult == ResultTypes.success;

                cargarDepartamentos();
                cargarPuestos();
                var respdias = new LiquidacionSvc().getInhabiles();
                if (respdias.typeResult == ResultTypes.error)
                {
                    imprimir(respdias);
                    this.IsEnabled = false;
                    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    return;
                }
                listaDiasInHabil = respdias.result as List<DateTime>;
                //listaDiasInHabil.Add(new DateTime(2019, 10, 28));
                rbnResumen.IsChecked = true;
                nuevo();
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void cargarPuestos()
        {
            try
            {
                var resp = new PuestoSvc().getALLPuestosById();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxPuestos.ItemsSource = (resp.result as List<Puesto>).FindAll(s => s.activo).OrderBy(s => s.nombrePuesto).ToList();
                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            cbxDepartamentos.SelectedItems.Clear();
            cbxPuestos.SelectedItems.Clear();
            mapForm(null);
        }
        private void cargarDepartamentos()
        {
            try
            {
                var resp = new DepartamentoSvc().getALLDepartamentosById();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxDepartamentos.ItemsSource = (resp.result as List<Departamento>).FindAll(s => s.activo).OrderBy(s => s.descripcion).ToList();
                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private DateTime fechaInicial { get; set; }
        private DateTime fechaFinal { get; set; }
        private List<Departamento> listaDepartamento { get; set; }
        private List<Puesto> listaPuestos { get; set; }
        private void llenarDetalles(bool borrar = false)
        {
            lBoxDeptos.Items.Clear();
            foreach (var depto in cbxDepartamentos.SelectedItems.Cast<Departamento>().ToList())
            {
                lBoxDeptos.Items.Add(depto.descripcion);
            }

            lBoxPuestos.Items.Clear();
            foreach (var puesto in cbxPuestos.SelectedItems.Cast<Puesto>().ToList())
            {
                lBoxPuestos.Items.Add(puesto.nombrePuesto);
            }
        }

        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                mapForm(null);
                Cursor = Cursors.Wait;

                //lvlResumen.ItemsSource = null;
                listaViewItemGeneral = new List<ListViewItem>();
                this.listaReporteCartaPortes = new List<ReporteCartaPorte>();
                this.listaReporteViaje = new List<ReporteCartaPorteViajes>();

                if (cbxDepartamentos.SelectedItems.Count == 0 || cbxPuestos.SelectedItems.Count == 0) return;



                this.fechaInicial = ctrFecha.fechaInicial;
                this.fechaFinal = ctrFecha.fechaFinal;
                this.listaDepartamento = cbxDepartamentos.SelectedItems.Cast<Departamento>().ToList();
                this.listaPuestos = cbxPuestos.SelectedItems.Cast<Puesto>().ToList();

                List<string> listaId = listaDepartamento.Select(s => s.clave.ToString()).ToList();
                List<string> listaIdPuestos = listaPuestos.Select(s => s.idPuesto.ToString()).ToList();

                var respValidar = new LiquidacionSvc().validarDepartamentoEnLiquidacion(listaId);

                List<classCadenas> listaDic = llenarDicionario(listaId);

                //var respValFechas = new LiquidacionSvc().validarDepartamentoFechasEnLiquidacion(listaDic);

                //if (respValFechas.typeResult != ResultTypes.success)
                //{
                //    imprimir(respValFechas);
                //    return;
                //}

                if (respValidar.typeResult != ResultTypes.success && respValidar.typeResult != ResultTypes.recordNotFound)
                {
                    imprimir(respValidar);
                    return;
                }

                var resp = new OperadorSvc().getPersonalByDepartamentosAndPuesto(listaId, listaIdPuestos);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Personal> listaPersonal = resp.result as List<Personal>;
                    getViajes(listaPersonal.Select(s => s.clave.ToString()).ToList());
                    foreach (var personal in listaPersonal.OrderBy(s => s.nombre).ToList())
                    {
                        DetallePersonaLiquidacion detallePersonaLiquidacion = new DetallePersonaLiquidacion
                        {
                            idDetallePersonaLiquidacion = 0,
                            idLiquidacion = 0,
                            personal = personal,
                            listaConceptoLiquidacion = new List<ConceptoLiquidacion>(),

                        };
                        agregarDetallePersonalLiquidacion(detallePersonaLiquidacion);
                    }

                    lvlResumen.ItemsSource = listaViewItemGeneral;
                    CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlResumen.ItemsSource);
                    //PropertyGroupDescription groupDescriptionDet = new PropertyGroupDescription("mombreDpto"){};
                    //defaultView.GroupDescriptions.Add(groupDescriptionDet);
                    defaultView.Filter = new Predicate<object>(this.UserFilter);
                    actualizarTotal();
                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
                llenarDetalles();
            }
        }
        private List<classCadenas> llenarDicionario(List<string> listaId)
        {
            try
            {
                List<classCadenas> listaDic = new List<classCadenas>();

                foreach (var item in listaId)
                {
                    int x = 0;
                    TimeSpan tiempo = ctrFecha.fechaFinal - ctrFecha.fechaInicial;
                    for (int i = 0; i < tiempo.Days; i++)
                    {
                        listaDic.Add(new classCadenas { fecha = ctrFecha.fechaInicial.AddDays(i), id = item });
                    }
                }

                return listaDic;
            }
            catch (Exception ex)
            {
                return new List<classCadenas>();
            }
        }

        private void DetallePersonaLiquidacion_ValueChanged(object sender, EventArgs e)
        {

        }

        private void txtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (lvlResumen.ItemsSource == null) return;

            CollectionViewSource.GetDefaultView(this.lvlResumen.ItemsSource).Refresh();
        }
        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtBuscador.Text) ||
            ((((DetallePersonaLiquidacion)(item as ListViewItem).Content).personal.nombre.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        List<DateTime> listaDiasInHabil = new List<DateTime>();
        private void agregarDetallePersonalLiquidacion(DetallePersonaLiquidacion detallePersonaLiquidacion)
        {
            if (detallePersonaLiquidacion.personal.clave == 22179)
            {

            }
            int i = 0;

            if (ctrEmpresa.empresaSelected.clave != 2)
            {
                foreach (var viaje in listaReporteViaje.FindAll(s => (s.idOperadorFin == 0 ? s.idOperador : s.idOperadorFin) == detallePersonaLiquidacion.personal.clave))
                {
                    i++;
                    ConceptoLiquidacion conceptoViaje = new ConceptoLiquidacion();
                    if (viaje.idEmpresa == 1)
                    {
                        conceptoViaje = getConceptoViajeAlimentoBalanceado(detallePersonaLiquidacion, i, viaje);
                    }
                    else if (viaje.idEmpresa == 5)
                    {
                        conceptoViaje = getConceptoViajeSecosRefrigerados(detallePersonaLiquidacion, i, viaje);
                    }                   
                    detallePersonaLiquidacion.listaConceptoLiquidacion.Add(conceptoViaje);
                }
            }
            else
            {
                foreach (var cp in listaReporteCartaPortes.FindAll(s=> s.idOperador == detallePersonaLiquidacion.personal.clave))
                {
                    i++;
                    ConceptoLiquidacion conceptoViaje = new ConceptoLiquidacion();
                    if (cp.idEmpresa == 2)
                    {
                        conceptoViaje = getConceptoViajeAtlante(detallePersonaLiquidacion, i, cp);
                    }
                    detallePersonaLiquidacion.listaConceptoLiquidacion.Add(conceptoViaje);
                }
            }

            DateTime fechaIngreso = detallePersonaLiquidacion.personal.fechaIngreso ?? new DateTime(2019, 01, 01);
            if (ctrEmpresa.empresaSelected.clave != 2)
            {
                GregorianCalendar cal = new GregorianCalendar(GregorianCalendarTypes.Localized);                

                int semanaActual = cal.GetWeekOfYear(ctrFecha.fechaInicial, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
                int semanaIngreso = cal.GetWeekOfYear(fechaIngreso, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

                //if ((semanaActual - semanaIngreso) <= 4)
                if (fechaIngreso.AddDays(31) >= ctrFecha.fechaInicial)
                {
                    DateTime fechaIni = ctrFecha.fechaInicial;
                    DateTime fechaInicio;

                    if (fechaIngreso > ctrFecha.fechaInicial)
                    {
                        fechaInicio = new DateTime(fechaIngreso.Year, fechaIngreso.Month, fechaIngreso.Day, 0, 0, 0);
                    }
                    else
                    {
                        fechaInicio = new DateTime(ctrFecha.fechaInicial.Year, ctrFecha.fechaInicial.Month, ctrFecha.fechaInicial.Day, 0, 0, 0);
                    }

                    TimeSpan tiempo = ctrFecha.fechaFinal.AddDays(1) - fechaInicio;

                    decimal pagoFijo = detallePersonaLiquidacion.personal.pagoFijo == 0 ? 900 : detallePersonaLiquidacion.personal.pagoFijo;
                    int diasdivicion = (ctrFecha.fechaFinal.AddDays(1) - ctrFecha.fechaInicial).Days;
                    for (int ix = 0; ix < tiempo.Days; ix++)
                    {
                        DateTime fecha = fechaInicio.AddDays(ix);
                        ConceptoLiquidacion conceptoLiquidacion = new ConceptoLiquidacion
                        {
                            tipoConceptoLiquidacion = TipoConceptoLiquidacion.PAGO_FIJO,
                            fecha = fecha,
                            cantidad = (pagoFijo / (tiempo.Days <= 0 ? 1 : diasdivicion)),
                            listaTiposIndicencias = new List<TipoIncidencia>
                        {
                            TipoIncidencia.ASISTENCIA,
                            TipoIncidencia.FALTA
                        },
                            tipoIncidencia = TipoIncidencia.ASISTENCIA,
                            detalleHeader = detallePersonaLiquidacion,
                            usuario = mainWindow.usuario.nombreUsuario
                        };
                        detallePersonaLiquidacion.listaConceptoLiquidacion.Add(conceptoLiquidacion);
                    }
                }
            }
            

            if (detallePersonaLiquidacion.personal.apoyoPersonal != null)
            {
                if (detallePersonaLiquidacion.personal.apoyoPersonal.activo)
                {
                    ApoyoPersonal apoyoPersonal = detallePersonaLiquidacion.personal.apoyoPersonal;
                    decimal pagoFijo = apoyoPersonal.pagoApoyo;

                    int noViajes = detallePersonaLiquidacion.listaConceptoLiquidacion.FindAll(
                        s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.VIAJES_ALIMENTOS_BALANCEADOS ||
                        s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.VIAJES_SECOS_REFRIGERADOS
                        ).ToList().Count();

                    DateTime fechaIni = ctrFecha.fechaInicial;
                    DateTime fechaInicio;

                    if (fechaIngreso > ctrFecha.fechaInicial)
                    {
                        fechaInicio = new DateTime(fechaIngreso.Year, fechaIngreso.Month, fechaIngreso.Day, 0, 0, 0);
                    }
                    else
                    {
                        fechaInicio = new DateTime(ctrFecha.fechaInicial.Year, ctrFecha.fechaInicial.Month, ctrFecha.fechaInicial.Day, 0, 0, 0);
                    }

                    TimeSpan tiempo = ctrFecha.fechaFinal.AddDays(1) - fechaInicio;
                    //DateTime fechaInicio = new DateTime(ctrFecha.fechaInicial.Year, ctrFecha.fechaInicial.Month, ctrFecha.fechaInicial.Day, 0, 0, 0);
                    int diasdivicion = (ctrFecha.fechaFinal.AddDays(1) - ctrFecha.fechaInicial).Days;
                    if (noViajes >= apoyoPersonal.metaViaje)
                    {
                        for (int ix = 0; ix < tiempo.Days; ix++)
                        {
                            DateTime fecha = fechaInicio.AddDays(ix);
                            ConceptoLiquidacion conceptoLiquidacion = new ConceptoLiquidacion
                            {
                                tipoConceptoLiquidacion = TipoConceptoLiquidacion.PAGO_APOYO,
                                fecha = fecha,
                                cantidad = (pagoFijo / (tiempo.Days <= 0 ? 1 : diasdivicion)),
                                listaTiposIndicencias = new List<TipoIncidencia>
                        {
                            TipoIncidencia.ASISTENCIA,
                            TipoIncidencia.FALTA
                        },
                                tipoIncidencia = TipoIncidencia.ASISTENCIA,
                                detalleHeader = detallePersonaLiquidacion,
                                usuario = mainWindow.usuario.nombreUsuario
                            };
                            detallePersonaLiquidacion.listaConceptoLiquidacion.Add(conceptoLiquidacion);
                        }
                    }
                }
            }

            ListViewItem lvl = new ListViewItem { Content = detallePersonaLiquidacion };
            if (detallePersonaLiquidacion.totalPagoFijo > 0)
            {
                lvl.FontWeight = FontWeights.Bold;
                lvl.Foreground = Brushes.Blue;
            }
            lvl.Selected += Lvl_Selected;
            listaViewItemGeneral.Add(lvl);
        }

        private ConceptoLiquidacion getConceptoViajeAlimentoBalanceado(DetallePersonaLiquidacion detallePersonaLiquidacion, int i, ReporteCartaPorteViajes viaje)
        {
            return new ConceptoLiquidacion
            {
                tipoConceptoLiquidacion = TipoConceptoLiquidacion.VIAJES_ALIMENTOS_BALANCEADOS,
                folioViaje = viaje.folio,
                modalidad = viaje.modalidad,
                tractor = viaje.tc,
                noViaje = i,
                destinos = viaje.destino,
                noDestinos = viaje.noDestinos,
                noViajeOperador = viaje.noViajesOperadores,
                fechaInicial = viaje.fechaSalidaPlantaCSI == null ? viaje.fechaInicio : viaje.fechaSalidaPlantaCSI,
                fechaFinal = viaje.fechaFin,
                cantidad = viaje.pagoOperador,
                idCliente = viaje.idCliente,
                listaDiasInhabil = this.listaDiasInHabil,
                detalleHeader = detallePersonaLiquidacion,
                usuario = mainWindow.usuario.nombreUsuario
                //motivosDoble = MotivosDoble.CINCO_VIAJES
            };
        }
        private ConceptoLiquidacion getConceptoViajeSecosRefrigerados(DetallePersonaLiquidacion detallePersonaLiquidacion, int i, ReporteCartaPorteViajes viaje)
        {
            return new ConceptoLiquidacion
            {
                tipoConceptoLiquidacion = TipoConceptoLiquidacion.VIAJES_SECOS_REFRIGERADOS,
                folioViaje = viaje.folio,
                modalidad = viaje.modalidad,
                noViaje = i,
                tractor = viaje.tc,
                destinos = viaje.destino,
                noDestinos = viaje.noDestinos,
                noViajeOperador = viaje.noViajesOperadores,
                fechaInicial = viaje.fechaSalidaPlantaCSI == null ? viaje.fechaInicio : viaje.fechaSalidaPlantaCSI,
                fechaFinal = viaje.fechaFin,
                cantidad = viaje.pagoOperador,
                idCliente = viaje.idCliente,
                listaDiasInhabil = this.listaDiasInHabil,
                detalleHeader = detallePersonaLiquidacion,
                usuario = mainWindow.usuario.nombreUsuario
            };
        }

        private ConceptoLiquidacion getConceptoViajeAtlante(DetallePersonaLiquidacion detallePersonaLiquidacion, int i, ReporteCartaPorte cp)
        {
            return new ConceptoLiquidacion
            {
                tipoConceptoLiquidacion = TipoConceptoLiquidacion.VIAJES_ATLANTE,
                folioViaje = cp.folio,
                modalidad = cp.modalidad,
                tractor = cp.tc,
                cartaPorte = cp.cartaPorte,
                origenes = cp.origen,
                remision = cp.orden,
                noViaje = i,
                destinos = cp.destino,
                noDestinos = cp.noDestinos,
                noViajeOperador = cp.noViajesOperadores,
                fechaInicial = cp.fechaSalidaPlantaCSI == null ? cp.fechaInicio : cp.fechaSalidaPlantaCSI,
                fechaFinal = cp.fechaFin,
                cantidad = cp.pagoOperador,
                idCliente = cp.idCliente,
                listaDiasInhabil = this.listaDiasInHabil,
                detalleHeader = detallePersonaLiquidacion,
                usuario = mainWindow.usuario.nombreUsuario,
                fechaCarga = cp.fechaCarga,
                fechaDescarga = cp.fechaDescarga
                
            };
        }

        List<ListViewItem> listaViewItemGeneral = new List<ListViewItem>();
        DetallePersonaLiquidacion _detalle = null;
        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                ListViewItem lvl = sender as ListViewItem;
                DetallePersonaLiquidacion detallePersonaLiquidacion = lvl.Content as DetallePersonaLiquidacion;
                _detalle = detallePersonaLiquidacion;
                llenarListas(detallePersonaLiquidacion);
            }
        }

        private void llenarListas(DetallePersonaLiquidacion detallePersonaLiquidacion)
        {
            lvlViajesAlimentosBalanceados.ItemsSource = null;
            lvlViajesSecos.ItemsSource = null;
            lvlViajesAtlantes.ItemsSource = null;
            lvlPagosFijos.ItemsSource = null;
            lvlPagosApoyos.ItemsSource = null;
            lvlExtras.ItemsSource = null;
            lvlHoraExtras.ItemsSource = null;

            detallePersonaLiquidacion = detallePersonaLiquidacion == null ? new DetallePersonaLiquidacion { listaConceptoLiquidacion = new List<ConceptoLiquidacion>() } : detallePersonaLiquidacion;

            lvlViajesAlimentosBalanceados.ItemsSource = detallePersonaLiquidacion.listaConceptoLiquidacion.FindAll(s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.VIAJES_ALIMENTOS_BALANCEADOS);
            lblSumaViajesBalanceador.Content = detallePersonaLiquidacion.totalViajesBalanceados.ToString("C2");
            lblSumaViajesBalanceador.Foreground = (detallePersonaLiquidacion.sePagaViaje && detallePersonaLiquidacion.totalViajesBalanceados > 0) ? Brushes.DarkGreen : Brushes.White;

            lvlViajesSecos.ItemsSource = detallePersonaLiquidacion.listaConceptoLiquidacion.FindAll(s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.VIAJES_SECOS_REFRIGERADOS);
            lblSumaViajesSecos.Content = detallePersonaLiquidacion.totalViajesSecos.ToString("C2");
            lblSumaViajesSecos.Foreground = (detallePersonaLiquidacion.sePagaViaje && detallePersonaLiquidacion.totalViajesSecos > 0) ? Brushes.DarkGreen : Brushes.White;

            lvlViajesAtlantes.ItemsSource = detallePersonaLiquidacion.listaConceptoLiquidacion.FindAll(s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.VIAJES_ATLANTE);
            lblSumaViajesAtlantes.Content = detallePersonaLiquidacion.totalViajesAtlante.ToString("C2");
            lblSumaViajesAtlantes.Foreground = (detallePersonaLiquidacion.sePagaViaje && detallePersonaLiquidacion.totalViajesAtlante > 0) ? Brushes.DarkGreen : Brushes.White;

            decimal sumaPagoFijo = 0m;
            decimal sumaPagoApoyo = 0m;

            sumaPagoFijo = detallePersonaLiquidacion.totalPagoFijo;
            sumaPagoApoyo = detallePersonaLiquidacion.totalPagoApoyo;

            lvlPagosFijos.ItemsSource = detallePersonaLiquidacion.listaConceptoLiquidacion.FindAll(s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.PAGO_FIJO);
            lblSumaSueldoFijos.Content = sumaPagoFijo.ToString("C2");
            lblSumaSueldoFijos.Foreground = (!detallePersonaLiquidacion.sePagaViaje && (sumaPagoFijo > sumaPagoApoyo)) ? Brushes.DarkGreen : Brushes.White;

            lvlPagosApoyos.ItemsSource = detallePersonaLiquidacion.listaConceptoLiquidacion.FindAll(s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.PAGO_APOYO);
            lblSumaApoyo.Content = sumaPagoApoyo.ToString("C2");
            lblSumaApoyo.Foreground = (!detallePersonaLiquidacion.sePagaViaje && (sumaPagoFijo < sumaPagoApoyo)) ? Brushes.DarkGreen : Brushes.White;

            lvlDescuentos.ItemsSource = detallePersonaLiquidacion.listaConceptoLiquidacion.FindAll(s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.DESCUENTO);
            lblSumaDescuentos.Content = detallePersonaLiquidacion.totalDescuento.ToString("C2");
            lblSumaDescuentos.Foreground = detallePersonaLiquidacion.listaConceptoLiquidacion.FindAll(s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.DESCUENTO).Count > 0 ? Brushes.DarkRed : Brushes.White;
            //var sss = detallePersonaLiquidacion.totalPago;
            lvlExtras.ItemsSource = detallePersonaLiquidacion.listaConceptoLiquidacion.FindAll(s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.EXTRAS);
            lblSumaExtras.Content = detallePersonaLiquidacion.totalExtras.ToString("C2");
            lblSumaExtras.Foreground = detallePersonaLiquidacion.listaConceptoLiquidacion.FindAll(s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.EXTRAS).Count > 0 ? Brushes.DarkGreen : Brushes.White;

            lvlHoraExtras.ItemsSource = detallePersonaLiquidacion.listaConceptoLiquidacion.FindAll(s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.HORAS_EXTRAS);
            lblSumaHorasExtras.Content = detallePersonaLiquidacion.totalHorasExtras.ToString("C2");
            lblSumaHorasExtras.Foreground = detallePersonaLiquidacion.listaConceptoLiquidacion.FindAll(s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.HORAS_EXTRAS).Count > 0 ? Brushes.DarkGreen : Brushes.White;

            lblBalance.Content = detallePersonaLiquidacion.totalPago.ToString("C2");
            lblBalanceHeader.Content = detallePersonaLiquidacion.totalPago.ToString("C2");
            lblNombrePersonal.Content = detallePersonaLiquidacion.personal == null ? string.Empty : detallePersonaLiquidacion.personal.nombre;
            actualizarTotal();
        }

        private List<ReporteCartaPorte> listaReporteCartaPortes = new List<ReporteCartaPorte>();
        private List<ReporteCartaPorteViajes> listaReporteViaje = new List<ReporteCartaPorteViajes>();
        public void getViajes(List<string> listaIdOperadores)
        {
            OperationResult resp = new OperationResult();

            if (ctrEmpresa.empresaSelected.clave == 2)
            {
                resp = new ReporteCartaPorteSvc().getReporteCartaPorteByOperadoresAtlante(ctrEmpresa.empresaSelected.clave, ctrFecha.fechaInicial,
               ctrFecha.fechaFinal, listaIdOperadores);
            }
            else
            {
                resp = new ReporteCartaPorteSvc().getReporteCartaPorteByOperadores(ctrEmpresa.empresaSelected.clave, ctrFecha.fechaInicial,
                ctrFecha.fechaFinal, listaIdOperadores);
            }            


            if (resp.typeResult == ResultTypes.success)
            {
                this.listaReporteCartaPortes = resp.result as List<ReporteCartaPorte>;

                this.listaReporteCartaPortes = listaReporteCartaPortes.OrderBy(s => s.fechaInicio).ToList();

                List<GrupoRuta> listaGrupos = (from v in listaReporteCartaPortes
                                               group v by v.cartaPorte into grupo
                                               select new GrupoRuta
                                               {
                                                   folio = (from s in grupo select s.folio).First(),
                                                   cartaPorte = (from s in grupo select s.cartaPorte).First(),
                                                   idOrigen = (from s in grupo select s.idOrigen).First(),
                                                   idDestino = (from s in grupo select s.idDestino).First(),
                                                   kmOrigen = (from s in grupo select s.kmOrigen).First(),
                                                   kmOrigenDestino = (from s in grupo select s.kmOrigenDestino).First(),
                                                   kmDestino = (from s in grupo select s.kmDestino).First(),
                                                   //km = (from s in grupo select s.km).First()
                                               }).ToList();



                foreach (ReporteCartaPorte reporteCp in listaReporteCartaPortes)
                {
                    reporteCp.listaGrupo = new List<GrupoRuta>();
                    reporteCp.listaGrupo = listaGrupos.FindAll(s => s.folio == reporteCp.folio);
                }

                this.listaReporteViaje = (from v in this.listaReporteCartaPortes
                                          group v by v.folio into grupoViaje
                                          select new ReporteCartaPorteViajes
                                          {
                                              lista = grupoViaje.ToList<ReporteCartaPorte>(),
                                              fechaInicioCarga = (from s in grupoViaje select s.fechaInicioCarga).First(),
                                              fechaInicio = (from s in grupoViaje select s.fechaInicio).First(),
                                              fechaFin = (from s in grupoViaje select s.fechaFin).First(),
                                              fechaPrograma = (from s in grupoViaje select s.fechaPrograma).First(),
                                              isReEnviar = (from s in grupoViaje select s.isReEnviar).First(),
                                              imputable = (from s in grupoViaje select s.imputable).First()
                                          }).ToList();
            }
            else if (resp.typeResult != ResultTypes.recordNotFound)
            {
                imprimir(resp);
            }
        }

        private void CbxCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvlResumen.SelectedItem != null)
            {
                var i = _detalle;
                llenarListas(_detalle);
            }
            actualizarTotal();
        }
        void actualizarTotal()
        {
            if (lvlResumen.ItemsSource == null)
            {
                txtTotal.Text = decimal.Zero.ToString("C2");
            }
            else
            {
                txtTotal.Text = lvlResumen.ItemsSource.Cast<ListViewItem>().ToList().Sum(s => (s.Content as DetallePersonaLiquidacion).totalPago).ToString("C2");
            }
        }
        private void BtnAddDescuento_Click(object sender, RoutedEventArgs e)
        {
            if (lvlResumen.SelectedItem != null)
                añadirDescuento();
        }
        public void añadirDescuento()
        {
            if (lvlResumen.SelectedItem != null)
            {
                expDescuentos.IsExpanded = true;
                List<ViajesOperador> listaViajesOperador = _detalle.listaConceptoLiquidacion.Where(
                    s => s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.VIAJES_ALIMENTOS_BALANCEADOS
                    || s.tipoConceptoLiquidacion == TipoConceptoLiquidacion.VIAJES_SECOS_REFRIGERADOS).
                    Select(s => new ViajesOperador { folioViaje = s.folioViaje.Value, pagoViaje = s.total }).ToList();

                _detalle.listaConceptoLiquidacion.Add(new ConceptoLiquidacion
                {
                    tipoConceptoLiquidacion = TipoConceptoLiquidacion.DESCUENTO,
                    detalleHeader = _detalle,
                    listaViajesOperador = listaViajesOperador,
                    viajesOperadorDecuento = null,

                    listaMotivosDescuento = new List<MotivoDescuentos>
                    {
                        MotivoDescuentos.OTRO,
                        MotivoDescuentos.COMBUSTIBLE,
                        MotivoDescuentos.DAÑO_O_PERDIDA,
                        MotivoDescuentos.ZONA_DE_ROBO
                    },
                    cantidad = 0,
                    motivoDescuentos = MotivoDescuentos.OTRO,
                    observaciones = string.Empty,
                    usuario = mainWindow.usuario.nombreUsuario
                });

                actulizarResumen();
            }
        }
        private void BtnDeleteDescuento_Click(object sender, RoutedEventArgs e)
        {
            if (lvlDescuentos.SelectedItem != null)
            {
                ConceptoLiquidacion concepto = lvlDescuentos.SelectedItem as ConceptoLiquidacion;
                if (concepto.idConceptoLiquidacion != 0)
                {
                    var resp = new LiquidacionSvc().bajarConceptoLiquidacion(concepto.idConceptoLiquidacion);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        _detalle.listaConceptoLiquidacion.Remove(concepto);
                    }
                    else
                    {
                        imprimir(resp);
                    }
                }
                else
                {
                    _detalle.listaConceptoLiquidacion.Remove(concepto);
                }

                //llenarListas(_detalle);
                actulizarResumen();
            }
        }
        void actulizarResumen()
        {
            int i = lvlResumen.SelectedIndex;
            List<DetallePersonaLiquidacion> lista = lvlResumen.ItemsSource.Cast<ListViewItem>().ToList().Select(s => s.Content as DetallePersonaLiquidacion).ToList();
            lvlResumen.ItemsSource = null;
            actulizarResumen2(lista, i);
        }
        void actulizarResumen2(List<DetallePersonaLiquidacion> lista, int i)
        {

            try
            {
                listaViewItemGeneral = new List<ListViewItem>();
                foreach (var detallePersonaLiquidacion in lista)
                {
                    ListViewItem lvl = new ListViewItem { Content = detallePersonaLiquidacion };

                    lvl.Selected += Lvl_Selected;
                    listaViewItemGeneral.Add(lvl);
                }

                lvlResumen.ItemsSource = listaViewItemGeneral;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlResumen.ItemsSource);
                //PropertyGroupDescription groupDescriptionDet = new PropertyGroupDescription("personal.departamento.descripcion");
                //defaultView.GroupDescriptions.Add(groupDescriptionDet);
                defaultView.Filter = new Predicate<object>(this.UserFilter);

                lvlResumen.SelectedIndex = i;
                actualizarTotal();
            }
            catch (Exception ex)
            {

            }
        }
        private void BtnAddExtra_Click(object sender, RoutedEventArgs e)
        {
            if (lvlResumen.SelectedItem != null)
                añadirExtra();
        }
        public void añadirExtra()
        {
            if (lvlResumen.SelectedItem != null)
            {
                expExtras.IsExpanded = true;
                _detalle.listaConceptoLiquidacion.Add(new ConceptoLiquidacion
                {
                    tipoConceptoLiquidacion = TipoConceptoLiquidacion.EXTRAS,
                    detalleHeader = _detalle,
                    cantidad = 0,
                    observaciones = string.Empty,
                    usuario = mainWindow.usuario.nombreUsuario
                });

                actulizarResumen();
            }
        }
        private void BtnDeleteExtra_Click(object sender, RoutedEventArgs e)
        {
            if (lvlExtras.SelectedItem != null)
            {
                ConceptoLiquidacion concepto = lvlExtras.SelectedItem as ConceptoLiquidacion;
                if (concepto.idConceptoLiquidacion != 0)
                {
                    var resp = new LiquidacionSvc().bajarConceptoLiquidacion(concepto.idConceptoLiquidacion);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        _detalle.listaConceptoLiquidacion.Remove(concepto);
                    }
                    else
                    {
                        imprimir(resp);
                    }
                }
                else
                {
                    _detalle.listaConceptoLiquidacion.Remove(concepto);
                }
                actulizarResumen();
            }
        }

        private void BtnAddHoraExtra_Click(object sender, RoutedEventArgs e)
        {
            if (lvlResumen.SelectedItem != null)
                añadirHorasExtra();
        }
        public void añadirHorasExtra()
        {
            if (lvlResumen.SelectedItem != null)
            {
                expHorasExtra.IsExpanded = true;
                _detalle.listaConceptoLiquidacion.Add(new ConceptoLiquidacion
                {
                    tipoConceptoLiquidacion = TipoConceptoLiquidacion.HORAS_EXTRAS,
                    fecha = DateTime.Now,
                    detalleHeader = _detalle,
                    cantidad = 0,
                    noHoras = 0,
                    observaciones = string.Empty,
                    usuario = mainWindow.usuario.nombreUsuario
                });

                actulizarResumen();
            }

        }

        private void BtnDeleteHoraExtra_Click(object sender, RoutedEventArgs e)
        {
            if (lvlHoraExtras.SelectedItem != null)
            {
                ConceptoLiquidacion concepto = lvlHoraExtras.SelectedItem as ConceptoLiquidacion;
                if (concepto.idConceptoLiquidacion != 0)
                {
                    var resp = new LiquidacionSvc().bajarConceptoLiquidacion(concepto.idConceptoLiquidacion);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        _detalle.listaConceptoLiquidacion.Remove(concepto);
                    }
                    else
                    {
                        imprimir(resp);
                    }
                }
                else
                {
                    _detalle.listaConceptoLiquidacion.Remove(concepto);
                }
                actulizarResumen();
            }
        }

        private void TxtCantidadDescuento_LostFocus(object sender, RoutedEventArgs e)
        {
            actulizarResumen();
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            actulizarResumen();
        }
        public override OperationResult guardar()
        {
            Cursor = Cursors.Wait;
            try
            {

                Liquidacion liquidacion = mapForm();

                if (liquidacion != null)
                {

                    if (liquidar)
                    {
                        liquidacion.liquidado = true;
                        liquidacion.fechaFin = DateTime.Now;
                        liquidacion.usuarioFin = mainWindow.usuario.nombreUsuario;
                    }

                    if (liquidacion.idLiquidacion == 0)
                    {
                        foreach (var item in liquidacion.listaDetalleLiquidacion)
                        {
                            item.idLiquidacion = 0;
                            item.idDetalleLiquidacion = 0;
                        }

                        foreach (var item in liquidacion.listaDetallePersonalLiquidacion)
                        {
                            item.idLiquidacion = 0;
                            item.idDetallePersonaLiquidacion = 0;
                            foreach (var con in item.listaConceptoLiquidacion)
                            {
                                con.idLiquidacion = 0;
                                con.idDetallePersonaLiquidacion = 0;
                                con.idConceptoLiquidacion = 0;
                            }
                        }
                    }

                    var resp = new LiquidacionSvc().saveLiquidacion(ref liquidacion);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mapForm(liquidacion);
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LA INFORAMCIÓN DE LA PANTALLA");
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
                liquidar = false;
            }
        }
        private class grupoPuestos
        {
            public List<Puesto> listaPuesto { get; set; }
        }
        private void mapForm(Liquidacion liquidacion)
        {
            try
            {

                if (liquidacion != null)
                {
                    cbxDepartamentos.SelectedItems.Clear();
                    cbxPuestos.SelectedItems.Clear();

                    ctrFecha.IsEnabled = false;
                    btnBuscar.IsEnabled = false;
                    ctrClave.valor = liquidacion.idLiquidacion;
                    ctrClave.Tag = liquidacion;
                    ctrClave.IsEnabled = false;

                    List<Puesto> listaPuestos = liquidacion.listaDetallePersonalLiquidacion.Select(s => s.personal.puesto).ToList();
                    List<Puesto> listaPuestosNuevo = new List<Puesto>();
                    foreach (var item in listaPuestos)
                    {
                        if (!listaPuestosNuevo.Exists(s => s.idPuesto == item.idPuesto))
                        {
                            listaPuestosNuevo.Add(item);
                        }
                    }

                    listaViewItemGeneral = new List<ListViewItem>();
                    if (privConsultarDtpos)
                    {
                        foreach (var detallePersonaLiquidacion in liquidacion.listaDetallePersonalLiquidacion)
                        {
                            ListViewItem lvl = new ListViewItem { Content = detallePersonaLiquidacion };
                            lvl.Selected += Lvl_Selected;
                            if (detallePersonaLiquidacion.totalPagoFijo > 0)
                            {
                                lvl.FontWeight = FontWeights.Bold;
                                lvl.Foreground = Brushes.Blue;
                            }
                            listaViewItemGeneral.Add(lvl);
                        }
                    }
                    else
                    {
                        foreach (var detallePersonaLiquidacion in liquidacion.listaDetallePersonalLiquidacion.FindAll(s => s.personal.departamento.clave == usuario.personal.departamento.clave).ToList())
                        {
                            ListViewItem lvl = new ListViewItem { Content = detallePersonaLiquidacion };
                            lvl.Selected += Lvl_Selected;
                            if (detallePersonaLiquidacion.totalPagoFijo > 0)
                            {
                                lvl.FontWeight = FontWeights.Bold;
                                lvl.Foreground = Brushes.Blue;
                            }
                            listaViewItemGeneral.Add(lvl);
                        }
                    }

                    lvlResumen.ItemsSource = listaViewItemGeneral;
                    CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlResumen.ItemsSource);
                    defaultView.Filter = new Predicate<object>(this.UserFilter);
                    llenarListas(null);


                    lvlViajesAlimentosBalanceados.IsEnabled = false;
                    lvlViajesSecos.IsEnabled = false;
                    lvlPagosFijos.IsEnabled = false;
                    lvlPagosApoyos.IsEnabled = false;
                    stpDescuentos.IsEnabled = false;
                    stpExtras.IsEnabled = false;
                    stpHorasExtras.IsEnabled = false;

                    List<Departamento> listaDtp = new List<Departamento>();
                    if (privConsultarDtpos)
                    {
                        listaDtp = liquidacion.listaDetalleLiquidacion.Select(s => s.departamento).ToList();
                    }
                    else
                    {
                        listaDtp = liquidacion.listaDetalleLiquidacion.FindAll(s => s.departamento.clave == usuario.personal.departamento.clave).ToList().Select(s => s.departamento).ToList();
                    }

                    foreach (var item in listaDtp)
                    {
                        cbxDepartamentos.SelectedItems.Add(cbxDepartamentos.ItemsSource.Cast<Departamento>().ToList().Find(s => s.clave == item.clave));
                    }
                    cbxDepartamentos.IsEnabled = false;

                    foreach (var item in listaPuestosNuevo)
                    {
                        cbxPuestos.SelectedItems.Add(cbxPuestos.ItemsSource.Cast<Puesto>().ToList().Find(s => s.idPuesto == item.idPuesto));
                    }
                    cbxPuestos.IsEnabled = false;

                    ctrFecha.dtpFechaInicio.SelectedDate = liquidacion.fechaInicio;
                    ctrFecha.dtpFechaFin.SelectedDate = liquidacion.fechaFin;

                    if (liquidacion.liquidado)
                    {
                        mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                        btnLiquidar.IsEnabled = false;
                        brdExportar.IsEnabled = true;
                    }
                    else
                    {
                        if (privEditar)
                        {
                            mainWindow.enableToolBarCommands(ToolbarCommands.editar | ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                        }
                        else
                        {
                            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                        }

                        btnLiquidar.IsEnabled = true;
                        brdExportar.IsEnabled = false;
                    }
                }
                else
                {
                    btnBuscar.IsEnabled = true;
                    ctrFecha.IsEnabled = true;
                    ctrClave.valor = 0;
                    ctrClave.Tag = null;
                    ctrClave.IsEnabled = true;

                    lvlResumen.ItemsSource = null;
                    llenarListas(null);

                    if (privGuardar)
                    {
                        mainWindow.enableToolBarCommands(ToolbarCommands.guardar | ToolbarCommands.nuevo | ToolbarCommands.cerrar | ToolbarCommands.buscar);
                    }
                    else
                    {
                        mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar | ToolbarCommands.buscar);
                        btnBuscar.Visibility = Visibility.Collapsed;
                    }

                    lvlViajesAlimentosBalanceados.IsEnabled = true;
                    lvlViajesSecos.IsEnabled = true;
                    lvlPagosFijos.IsEnabled = true;
                    lvlPagosApoyos.IsEnabled = true;
                    stpDescuentos.IsEnabled = true;
                    stpExtras.IsEnabled = true;
                    stpHorasExtras.IsEnabled = true;
                    //cbxDepartamentos.SelectedItems.Clear();
                    cbxDepartamentos.IsEnabled = true;
                    cbxPuestos.IsEnabled = true;
                    btnLiquidar.IsEnabled = false;
                    brdExportar.IsEnabled = false;
                }
                actualizarTotal();
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                llenarDetalles();
            }
        }
        public override void editar()
        {
            base.editar();
            lvlViajesAlimentosBalanceados.IsEnabled = true;
            lvlViajesSecos.IsEnabled = true;
            lvlPagosFijos.IsEnabled = true;
            lvlPagosApoyos.IsEnabled = true;
            stpDescuentos.IsEnabled = true;
            stpExtras.IsEnabled = true;
            stpHorasExtras.IsEnabled = true;
            mainWindow.enableToolBarCommands(ToolbarCommands.guardar | ToolbarCommands.nuevo | ToolbarCommands.cerrar | ToolbarCommands.buscar);
        }
        private Liquidacion mapForm()
        {
            try
            {
                Liquidacion liq = ctrClave.Tag == null ? null : (ctrClave.Tag as Liquidacion);
                Liquidacion liquidacion = new Liquidacion()
                {
                    idLiquidacion = liq == null ? 0 : liq.idLiquidacion,
                    activo = true,
                    fechaInicio = liq == null ? this.fechaInicial : liq.fechaInicio,
                    fechaFin = liq == null ? this.fechaFinal.AddSeconds(-1) : liq.fechaFin,
                    liquidado = false,
                    usuarioInicio = liq == null ? mainWindow.usuario.nombreUsuario : liq.usuarioInicio,
                    usuarioFin = string.Empty
                };

                if (liq == null)
                {
                    liquidacion.listaDetalleLiquidacion = new List<DetalleLiquidacion>();
                    foreach (var dto in this.listaDepartamento)
                    {
                        liquidacion.listaDetalleLiquidacion.Add(new DetalleLiquidacion
                        {
                            idLiquidacion = 0,
                            idDetalleLiquidacion = 0,
                            departamento = dto
                        });
                    }
                }
                else
                {
                    liquidacion.listaDetalleLiquidacion = liq.listaDetalleLiquidacion;
                }
                liquidacion.listaDetallePersonalLiquidacion = lvlResumen.ItemsSource.Cast<ListViewItem>().ToList().Select(s => s.Content as DetallePersonaLiquidacion).ToList();
                return liquidacion;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void CtrClave_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (ctrClave.valor > 0)
                {
                    buscarLiquidacionByFolio(ctrClave.valor);
                }
                else
                {
                    buscar();
                }

            }
        }
        void buscarLiquidacionByFolio(int folio)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new LiquidacionSvc().getLiquidacionesByFolio(folio);
                if (resp.typeResult == ResultTypes.success)
                {
                    Liquidacion liquidacion = resp.result as Liquidacion;
                    mapForm(liquidacion);
                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        bool liquidar = false;
        private void BtnLiquidar_Click(object sender, RoutedEventArgs e)
        {
            liquidar = true;
            guardar();
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            if (rbnResumen.IsChecked.Value)
            {
                exportarResumen();
            }
            if (rbnDetalle.IsChecked.Value)
            {
                exportarDetalle();
            }
        }

        private void exportarResumen()
        {
            try
            {
                Cursor = Cursors.Wait;
                Liquidacion liquidacion = ctrClave.Tag as Liquidacion;
                var rr = new ReportView();
                rr.Title = "RESUMEN DE LIQUIDACIÓN.";
                rr.btnEXCEL.Visibility = Visibility.Visible;
                //rr.exportarResumenLiquidaciones(liquidacion);
                rr.exportarResumenLiquidaciones(liquidacion, ctrEmpresa.empresaSelected.clave);
                rr.Show();
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void exportarDetalle()
        {
            try
            {
                Cursor = Cursors.Wait;
                Liquidacion liquidacion = ctrClave.Tag as Liquidacion;
                var rr = new ReportView();
                rr.Title = "DETALLE DE LIQUIDACIÓN.";
                rr.btnEXCEL.Visibility = Visibility.Visible;
                rr.exportarDetalleLiquidaciones(liquidacion, _detalle);
                rr.Show();
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public override void buscar()
        {
            Liquidacion liquidacion = null;
            if (privConsultarDtpos)
            {
                liquidacion = new selectLiquidaciones().obtenerLiquidacion();
            }
            else
            {
                liquidacion = new selectLiquidaciones().obtenerLiquidacion(usuario.personal.departamento.clave);
            }
            if (liquidacion != null)
            {
                buscarLiquidacionByFolio(liquidacion.idLiquidacion);
            }
        }
    }
}
