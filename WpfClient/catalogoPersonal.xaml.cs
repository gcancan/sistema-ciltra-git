﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using Core.Interfaces;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para catalogoPersonal.xaml
    /// </summary>
    public partial class catalogoPersonal : ViewBase
    {
        public catalogoPersonal()
        {
            InitializeComponent();
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ctrEmpresa.loaded(mainWindow.empresa, (new PrivilegioSvc().consultarPrivilegio("isAdmin", mainWindow.usuario.idUsuario).typeResult == ResultTypes.success));
            cargarDepartamentos();
            cargarPuestos();
            ctrFolioPersonal.txtEntero.IsReadOnly = true;
            ctrFolioPersonal.txtEntero.Cursor = Cursors.No;
            cbxTiposSandre.ItemsSource = DatosPersonales.getTiposSangre();
            cbxGenero.ItemsSource = DatosPersonales.getListaGeneros();
            cbxEstadoCivil.ItemsSource = DatosPersonales.getListaEstadoCivil();
            //cbxTallaCamisa.ItemsSource = DatosPersonales.getListaTallaCamisa();
            cbxTallaZapato.ItemsSource = DatosPersonales.getListaTallaZapatos();
            cbxPatentezco.ItemsSource = DatosPersonales.getListaParentezcos();

            cbxMotivoBajaIngreso.ItemsSource = DatosPersonales.getListaMotivosBaja();
            cbxMotivoBajaReIngreso.ItemsSource = DatosPersonales.getListaMotivosBaja();
            cbxMotivoBajaCambio.ItemsSource = DatosPersonales.getListaMotivosBaja();



            cbxTipoEstatal.ItemsSource = DatosPersonales.getListaLicenciaEstatal();
            cbxTipoFederal.ItemsSource = DatosPersonales.getListaLicenciaFederal();

            cbxBanco.ItemsSource = DatosPersonales.getListaBancos();

            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);
        }
        void cargarDepartamentos()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new DepartamentoSvc().getALLDepartamentosById();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxDepartamento.ItemsSource = (resp.result as List<Departamento>).FindAll(s => s.activo);
                    cbxDepartamentoIngreso.ItemsSource = (resp.result as List<Departamento>).FindAll(s => s.activo);
                    cbxDepartamentoReIngreso.ItemsSource = (resp.result as List<Departamento>).FindAll(s => s.activo);
                    cbxDepartamentoCambio.ItemsSource = (resp.result as List<Departamento>).FindAll(s => s.activo);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        void cargarPuestos()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new PuestoSvc().getALLPuestosById();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxPuesto.ItemsSource = (resp.result as List<Puesto>).FindAll(s => s.activo);
                    cbxPuestoIngreso.ItemsSource = (resp.result as List<Puesto>).FindAll(s => s.activo);
                    cbxPuestoReIngreso.ItemsSource = (resp.result as List<Puesto>).FindAll(s => s.activo);
                    cbxPuestoCambio.ItemsSource = (resp.result as List<Puesto>).FindAll(s => s.activo);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void txtNombres_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtApellidoMaterno.Text.Trim()))
            {
                txtNombreCompleto.Text = string.Format("{0} {1}", txtApellidoPaterno.Text.Trim(), txtNombres.Text.Trim()).Trim();
            }
            else
            {
                txtNombreCompleto.Text = string.Format("{0} {1} {2}", txtApellidoPaterno.Text.Trim(), txtApellidoMaterno.Text.Trim(), txtNombres.Text.Trim()).Trim();
            }

        }
        private void txtColonia_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (e.Key == Key.Enter)
                {
                    if (string.IsNullOrEmpty(txtColonia.Text.Trim()))
                    {
                        var colonia = new selectColonias().getAllColoniaByNombre("");
                        if (colonia != null)
                        {
                            mapColonia(colonia);
                        }
                    }

                    List<Colonia> listaColonias = new List<Colonia>();
                    var resp = new DireccionSvc().getALLColoniasByNombre(txtColonia.Text.Trim());
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listaColonias = resp.result as List<Colonia>;
                        if (listaColonias.Count == 1)
                        {
                            mapColonia(listaColonias[0]);
                        }
                        else
                        {
                            var colonia = new selectColonias().getAllColoniaByNombre(txtColonia.Text.Trim());
                            if (colonia != null)
                            {
                                mapColonia(colonia);
                            }
                        }
                    }
                    else if (resp.typeResult == ResultTypes.recordNotFound)
                    {
                        var colonia = new selectColonias().getAllColoniaByNombre("");
                        if (colonia != null)
                        {
                            mapColonia(colonia);
                        }
                    }
                    else
                    {
                        imprimir(resp);
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapColonia(Colonia colonia)
        {
            try
            {
                if (colonia != null)
                {
                    txtColonia.Text = colonia.nombre;
                    txtColonia.IsEnabled = false;
                    txtColonia.Tag = colonia;
                    //txtCP.valor = ;
                    txtCiudad.Text = colonia.ciudad.nombre;
                    txtEstado.Text = colonia.ciudad.estado.nombre;
                    txtPais.Text = colonia.ciudad.estado.pais.nombrePais;
                }
                else
                {
                    txtColonia.Clear();
                    txtColonia.IsEnabled = true;
                    txtColonia.Tag = null;
                    //txtCP.Clear();
                    txtCiudad.Clear();
                    txtEstado.Clear();
                    txtPais.Clear();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                Personal personal = mapForm();
                //return new OperationResult();
                if (personal == null)
                {
                    return new OperationResult(ResultTypes.error, "ERROR AL LEER LA INFORMACIÓN DE LA PANTALLA.");
                }

                var resp = new OperadorSvc().savePersonal(ref personal);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(personal);
                    base.guardar();
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }

        }
        //OperationResult validarForm()
        //{
        //    try
        //    {
        //        OperationResult validacion = new OperationResult() { valor = 0, mensaje = "PARA CONTINUAR SE REQUIERE" + Environment.NewLine};
        //    }
        //    catch (Exception ex)
        //    {
        //        return new OperationResult(ex);
        //    }
        //}
        public override OperationResult eliminar()
        {
            try
            {
                Cursor = Cursors.Wait;
                Personal personal = ctrFolioPersonal.Tag as Personal;
                personal.estatus = "BAJA";
                personal.usuario = mainWindow.usuario.nombreUsuario;

                personal.listaAjustes = new List<RegistroAjustes>();
                personal.listaAjustes.Add(new RegistroAjustes
                {
                    idTabla = personal.clave.ToString(),
                    nombreColumna = "Estatus",
                    nombreTabla = "CatPersonal",
                    valorAnterior = "ACT",
                    valorNuevo = "BAJ",
                    usuario = mainWindow.usuario.nombreUsuario
                });

                var resp = new OperadorSvc().savePersonal(ref personal);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(null);
                    base.nuevo();
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }

        }
        public Personal mapForm()
        {
            try
            {
                Personal personalAnterior = ctrFolioPersonal.Tag == null ? null : (ctrFolioPersonal.Tag as Personal);
                Personal personalNuevo = new Personal
                {
                    clave = ctrFolioPersonal.Tag == null ? 0 : (ctrFolioPersonal.Tag as Personal).clave,
                    estatus = chcActivo.IsChecked.Value ? "ACTIVO" : "BAJA",// txtEstatus.Text,
                    empresa = ctrEmpresa.empresaSelected,
                    nombres = txtNombres.Text.Trim(),
                    apellidoPaterno = txtApellidoPaterno.Text.Trim(),
                    apellidoMaterno = txtApellidoMaterno.Text.Trim(),
                    fechaNacimiento = dtpFechaNacimiento.SelectedDate,
                    tipoSangre = cbxTiposSandre.SelectedItem == null ? string.Empty : cbxTiposSandre.SelectedItem.ToString(),
                    calle = txtCalle.Text.Trim(),
                    numero = txtNo.Text.Trim(),
                    cruce1 = txtCruce1.Text.Trim(),
                    cruce2 = txtCruce2.Text.Trim(),
                    ciudad = txtCiudad.Text.Trim(),
                    coloniaStr = txtColoniaLibre.Text.Trim(),
                    cp = txtCP.valor,
                    fechaIngreso = stpFechaIngreso.SelectedDate,
                    departamento = cbxDepartamento.SelectedItem as Departamento,
                    puesto = cbxPuesto.SelectedItem as Puesto,
                    direccion = txtDireccion.Text.Trim(),
                    colonia = txtColonia.Tag as Colonia,
                    telefono = txtTelefono.Text.Trim(),
                    telefonoCelular = txtTelefonoCelular.Text.Trim(),
                    correoElectronico = txtCorreoElectronico.Text.Trim(),
                    genero = cbxGenero.SelectedItem == null ? string.Empty : cbxGenero.SelectedItem.ToString(),
                    estadoCivil = cbxEstadoCivil.SelectedItem == null ? string.Empty : cbxEstadoCivil.SelectedItem.ToString(),
                    tallaCamisa = cbxTallaCamisa.SelectedItem == null ? string.Empty : cbxTallaCamisa.SelectedItem.ToString(),
                    tallaZapatos = cbxTallaZapato.SelectedItem == null ? string.Empty : cbxTallaZapato.SelectedItem.ToString(),
                    usuario = mainWindow.usuario.nombreUsuario,
                };

                if (cbxPatentezco.SelectedItem != null)
                {
                    ContactoEmergencia contactoEmergencia = new ContactoEmergencia
                    {
                        parentezco = cbxPatentezco.SelectedItem.ToString(),
                        nombres = txtNombresContacto.Text.Trim(),
                        apellidoPaterno = txtApellidoPaternoContacto.Text.Trim(),
                        apellidoMaterno = txtApellidoMaternoContacto.Text.Trim(),
                        telefonoCelular = txtTelefonoCelularContacto.Text.Trim(),
                        telefonoDomicilio = txtTelefonoDomContacto.Text.Trim(),
                        idPersonal = personalNuevo.clave,
                        usuario = mainWindow.usuario.nombreUsuario
                    };
                    if (personalAnterior == null)
                    {
                        contactoEmergencia.idContactoEmergencia = 0;
                    }
                    else
                    {
                        if (personalAnterior.contactoEmergencia == null)
                        {
                            contactoEmergencia.idContactoEmergencia = 0;
                        }
                        else
                        {

                            contactoEmergencia.idContactoEmergencia = personalAnterior.contactoEmergencia.idContactoEmergencia;
                        }
                    }
                    personalNuevo.contactoEmergencia = contactoEmergencia;
                }
                DocumentacionPersonal documentacionPersonal = new DocumentacionPersonal
                {
                    numeroSeguroSocial = txtNSS.Text.Trim(),
                    CURP = txtCURP.Text.Trim(),
                    INE = txtINE.Text.Trim(),
                    RFC = txtRFC.Text.Trim(),
                    noExamenPsicofisico = txtNoExamenPsicofisico.Text.Trim(),
                    fechaVencimientoExamen = dtpFechaVigenciaExamen.SelectedDate,
                    banco = cbxBanco.SelectedItem == null ? string.Empty : cbxBanco.SelectedItem.ToString(),
                    cuenta = txtCuentaBanco.Text.Trim(),
                    clabe = txtCLABE.Text.Trim(),
                    observaciones = txtObservacionesDocumentacion.Text.Trim(),
                    usuario = mainWindow.usuario.nombreUsuario
                };

                LicenciaConducir licenciaEstatal = null;
                if (!string.IsNullOrEmpty(txtNoLicenciaEstatal.Text.Trim()))
                {
                    licenciaEstatal = new LicenciaConducir
                    {
                        idLicencia = txtNoLicenciaEstatal.Tag == null ? 0 : (txtNoLicenciaEstatal.Tag as LicenciaConducir).idLicencia,
                        noLicencia = txtNoLicenciaEstatal.Text.Trim(),
                        clasificacionLicencia = ClasificacionLicencia.ESTATAL,
                        tipoLicencia = cbxTipoEstatal.SelectedItem.ToString(),
                        usuario = mainWindow.usuario.nombreUsuario,
                        vencimiento = dtpFechaVigenciaEstatal.SelectedDate.Value
                    };
                }

                LicenciaConducir licenciaFederal = null;
                if (!string.IsNullOrEmpty(txtNoLicenciaFederal.Text.Trim()))
                {
                    licenciaFederal = new LicenciaConducir
                    {
                        idLicencia = txtNoLicenciaFederal.Tag == null ? 0 : (txtNoLicenciaFederal.Tag as LicenciaConducir).idLicencia,
                        noLicencia = txtNoLicenciaFederal.Text.Trim(),
                        clasificacionLicencia = ClasificacionLicencia.FEDERAL,
                        tipoLicencia = cbxTipoFederal.SelectedItem.ToString(),
                        usuario = mainWindow.usuario.nombreUsuario,
                        vencimiento = dtpFechaVigenciaFederal.SelectedDate.Value
                    };
                }

                documentacionPersonal.licenciaEstatal = licenciaEstatal;
                documentacionPersonal.licenciaFederal = licenciaFederal;

                if (personalAnterior == null)
                {
                    documentacionPersonal.idDocumentacionPersonal = 0;
                }
                else
                {
                    if (personalAnterior.documentacionPersonal == null)
                    {
                        documentacionPersonal.idDocumentacionPersonal = 0;
                    }
                    else
                    {

                        documentacionPersonal.idDocumentacionPersonal = personalAnterior.documentacionPersonal.idDocumentacionPersonal;
                    }
                }
                personalNuevo.documentacionPersonal = documentacionPersonal;

                if (dtpFechaContratacionIngreso.SelectedDate != null)
                {
                    DatosIngresoPersonal datosIngresoPersonalIngreso = new DatosIngresoPersonal()
                    {
                        fechaInicio = dtpFechaContratacionIngreso.SelectedDate.Value,
                        fechaFinCapacitacion = dtpFechaFinCapacitacionIngreso.SelectedDate.Value,
                        fechaBaja = dtpFechaBajaIngreso.SelectedDate,
                        puesto = cbxPuestoIngreso.SelectedItem as Puesto,
                        departamento = cbxDepartamentoIngreso.SelectedItem as Departamento,
                        motivoBaja = cbxMotivoBajaIngreso.SelectedItem == null ? string.Empty : cbxMotivoBajaIngreso.SelectedItem.ToString(),
                        salarioDiario = ctrSalarioDiarioIngreso.valor,
                        salarioHora = ctrSalarioHoraIngreso.valor,
                        maximoHorasExtra = ctrMaximoHorasIngreso.valor,
                        observaciones = txtObservacionesIngreso.Text.Trim(),
                        tipoIngreso = TipoIngreso.INGRESO,
                        usuarioRegistra = mainWindow.usuario.nombreUsuario,
                        idPersonal = personalNuevo.clave
                    };
                    if (dtpFechaBajaIngreso.SelectedDate != null)
                    {
                        datosIngresoPersonalIngreso.usuarioBaja = mainWindow.usuario.nombreUsuario;
                    }
                    if (personalAnterior == null)
                    {
                        datosIngresoPersonalIngreso.idDatoIngreso = 0;
                    }
                    else
                    {
                        if (personalAnterior.ingreso == null)
                        {
                            datosIngresoPersonalIngreso.idDatoIngreso = 0;
                        }
                        else
                        {

                            datosIngresoPersonalIngreso.idDatoIngreso = personalAnterior.ingreso.idDatoIngreso;
                        }
                    }
                    personalNuevo.ingreso = datosIngresoPersonalIngreso; ;
                }

                if (dtpFechaContratacionReIngreso.SelectedDate != null)
                {
                    DatosIngresoPersonal datosIngresoPersonalReingreso = new DatosIngresoPersonal()
                    {
                        fechaInicio = dtpFechaContratacionReIngreso.SelectedDate.Value,
                        fechaFinCapacitacion = dtpFechaFinCapacitacionReIngreso.SelectedDate.Value,
                        fechaBaja = dtpFechaBajaReIngreso.SelectedDate,
                        puesto = cbxPuestoReIngreso.SelectedItem as Puesto,
                        departamento = cbxDepartamentoReIngreso.SelectedItem as Departamento,
                        motivoBaja = cbxMotivoBajaReIngreso.SelectedItem == null ? string.Empty : cbxMotivoBajaReIngreso.SelectedItem.ToString(),
                        salarioDiario = ctrSalarioDiarioReIngreso.valor,
                        salarioHora = ctrSalarioHoraReIngreso.valor,
                        maximoHorasExtra = ctrMaximoHorasReIngreso.valor,
                        observaciones = txtObservacionesReIngreso.Text.Trim(),
                        tipoIngreso = TipoIngreso.REINGRESO,
                        usuarioRegistra = mainWindow.usuario.nombreUsuario,
                        idPersonal = personalNuevo.clave
                    };
                    if (dtpFechaBajaReIngreso.SelectedDate != null)
                    {
                        datosIngresoPersonalReingreso.usuarioBaja = mainWindow.usuario.nombreUsuario;
                    }
                    if (personalAnterior == null)
                    {
                        datosIngresoPersonalReingreso.idDatoIngreso = 0;
                    }
                    else
                    {
                        if (personalAnterior.reingreso == null)
                        {
                            datosIngresoPersonalReingreso.idDatoIngreso = 0;
                        }
                        else
                        {

                            datosIngresoPersonalReingreso.idDatoIngreso = personalAnterior.reingreso.idDatoIngreso;
                        }
                    }
                    personalNuevo.reingreso = datosIngresoPersonalReingreso; ;
                }

                if (dtpFechaContratacionCambio.SelectedDate != null)
                {
                    DatosIngresoPersonal datosIngresoPersonalCambio = new DatosIngresoPersonal()
                    {
                        fechaInicio = dtpFechaContratacionCambio.SelectedDate.Value,
                        fechaFinCapacitacion = dtpFechaFinCapacitacionCambio.SelectedDate.Value,
                        fechaBaja = dtpFechaBajaCambio.SelectedDate,
                        puesto = cbxPuestoCambio.SelectedItem as Puesto,
                        departamento = cbxDepartamentoCambio.SelectedItem as Departamento,
                        motivoBaja = cbxMotivoBajaCambio.SelectedItem == null ? string.Empty : cbxMotivoBajaCambio.SelectedItem.ToString(),
                        salarioDiario = ctrSalarioDiarioCambio.valor,
                        salarioHora = ctrSalarioHoraCambio.valor,
                        maximoHorasExtra = ctrMaximoHorasCambio.valor,
                        observaciones = txtObservacionesCambio.Text.Trim(),
                        tipoIngreso = TipoIngreso.CAMBIO_PUESTO,
                        usuarioRegistra = mainWindow.usuario.nombreUsuario,
                        idPersonal = personalNuevo.clave
                    };
                    if (dtpFechaBajaCambio.SelectedDate != null)
                    {
                        datosIngresoPersonalCambio.usuarioBaja = mainWindow.usuario.nombreUsuario;
                    }
                    if (personalAnterior == null)
                    {
                        datosIngresoPersonalCambio.idDatoIngreso = 0;
                    }
                    else
                    {
                        if (personalAnterior.cambioPuesto == null)
                        {
                            datosIngresoPersonalCambio.idDatoIngreso = 0;
                        }
                        else
                        {

                            datosIngresoPersonalCambio.idDatoIngreso = personalAnterior.cambioPuesto.idDatoIngreso;
                            if (datosIngresoPersonalCambio.fechaBaja != null)
                            {
                                if (personalNuevo.reingreso != null)
                                {
                                    if (personalNuevo.reingreso.fechaBaja == null)
                                    {
                                        datosIngresoPersonalCambio.puesto = personalNuevo.reingreso.puesto;
                                        datosIngresoPersonalCambio.departamento = personalNuevo.reingreso.departamento;
                                        datosIngresoPersonalCambio.fechaInicio = personalNuevo.reingreso.fechaInicio;
                                        datosIngresoPersonalCambio.fechaFinCapacitacion = personalNuevo.reingreso.fechaFinCapacitacion;
                                        datosIngresoPersonalCambio.salarioDiario = personalNuevo.reingreso.salarioDiario;
                                        datosIngresoPersonalCambio.salarioHora = personalNuevo.reingreso.salarioHora;
                                        datosIngresoPersonalCambio.maximoHorasExtra = personalNuevo.reingreso.maximoHorasExtra;
                                        datosIngresoPersonalCambio.motivoBaja = string.Empty;
                                    }
                                }
                            }
                        }
                    }
                    personalNuevo.cambioPuesto = datosIngresoPersonalCambio;

                    if (personalNuevo.ingreso != null)
                    {
                        if (personalNuevo.ingreso.fechaBaja != null)
                        {
                            personalNuevo.cambioPuesto.fechaBaja = personalNuevo.ingreso.fechaBaja;
                            personalNuevo.cambioPuesto.motivoBaja = personalNuevo.ingreso.motivoBaja;
                        }
                        else
                        {
                            personalNuevo.cambioPuesto.fechaBaja = null;
                            personalNuevo.cambioPuesto.motivoBaja = string.Empty;
                        }
                    }
                    if (personalNuevo.reingreso != null)
                    {
                        if (personalNuevo.reingreso.fechaBaja != null)
                        {
                            personalNuevo.cambioPuesto.fechaBaja = personalNuevo.reingreso.fechaBaja;
                            personalNuevo.cambioPuesto.motivoBaja = personalNuevo.reingreso.motivoBaja;
                            personalNuevo.cambioPuesto.observaciones += " " + personalNuevo.reingreso.observaciones;
                        }
                        else
                        {
                            personalNuevo.cambioPuesto.fechaBaja = null;
                            personalNuevo.cambioPuesto.motivoBaja = string.Empty;
                        }
                    }
                }
                else
                {
                    if (personalNuevo.reingreso != null)
                    {
                        if (personalNuevo.reingreso.fechaBaja == null)
                        {
                            personalNuevo.cambioPuesto = new DatosIngresoPersonal
                            {
                                idDatoIngreso = 0,
                                fechaInicio = personalNuevo.reingreso.fechaInicio,
                                departamento = personalNuevo.reingreso.departamento,
                                puesto = personalNuevo.reingreso.puesto,
                                fechaBaja = personalNuevo.reingreso.fechaBaja,
                                fechaFinCapacitacion = personalNuevo.reingreso.fechaFinCapacitacion,
                                maximoHorasExtra = personalNuevo.reingreso.maximoHorasExtra,
                                motivoBaja = personalNuevo.reingreso.motivoBaja,
                                observaciones = personalNuevo.reingreso.observaciones,
                                salarioDiario = personalNuevo.reingreso.salarioDiario,
                                salarioHora = personalNuevo.reingreso.salarioHora,
                                usuarioRegistra = mainWindow.usuario.nombreUsuario,
                                usuarioBaja = personalNuevo.reingreso.usuarioBaja,
                                tipoIngreso = TipoIngreso.CAMBIO_PUESTO
                            };
                        }
                    }
                    if (personalNuevo.ingreso != null)
                    {
                        //if (personalNuevo.ingreso.fechaBaja == null)
                        //{
                            personalNuevo.cambioPuesto = new DatosIngresoPersonal
                            {
                                idDatoIngreso = 0,
                                fechaInicio = personalNuevo.ingreso.fechaInicio,
                                departamento = personalNuevo.ingreso.departamento,
                                puesto = personalNuevo.ingreso.puesto,
                                fechaBaja = personalNuevo.ingreso.fechaBaja,
                                fechaFinCapacitacion = personalNuevo.ingreso.fechaFinCapacitacion,
                                maximoHorasExtra = personalNuevo.ingreso.maximoHorasExtra,
                                motivoBaja = personalNuevo.ingreso.motivoBaja,
                                observaciones = personalNuevo.ingreso.observaciones,
                                salarioDiario = personalNuevo.ingreso.salarioDiario,
                                salarioHora = personalNuevo.ingreso.salarioHora,
                                usuarioRegistra = mainWindow.usuario.nombreUsuario,
                                usuarioBaja = personalNuevo.ingreso.usuarioBaja,
                                tipoIngreso = TipoIngreso.CAMBIO_PUESTO,
                                idPersonal = mainWindow.usuario.personal.clave
                            };
                        //}
                    }
                    
                }

                //if (personalNuevo.cambioPuesto != null)
                //{
                //    personalNuevo.puesto = personalNuevo.cambioPuesto.puesto;
                //    personalNuevo.departamento = personalNuevo.cambioPuesto.departamento;
                //    personalNuevo.fechaIngreso = personalNuevo.cambioPuesto.fechaInicio;
                //}


                if (personalNuevo.cambioPuesto != null)
                {
                    if (personalNuevo.cambioPuesto.fechaBaja == null)
                    {
                        personalNuevo.estatus = "ACTIVO";
                    }
                    else
                    {
                        personalNuevo.estatus = "BAJA";
                    }
                }

                personalNuevo.listaAjustes = new List<RegistroAjustes>();
                if (personalAnterior != null)
                {
                    personalNuevo.listaAjustes = LLenarRegistroAjustes.getAjustesPersonal(personalAnterior, personalNuevo, mainWindow.usuario.nombreUsuario);
                }

                

                if (personalNuevo.ingreso != null)
                {
                    if (personalNuevo.ingreso.fechaBaja != null)
                    {
                        personalNuevo.estatus = "BAJA";
                    }
                    else
                    {
                        personalNuevo.estatus = "ACTIVO";                        
                    }
                    if (personalNuevo.cambioPuesto == null)
                    {
                        
                    }
                }
                if (personalNuevo.reingreso != null)
                {
                    if (personalNuevo.reingreso.fechaBaja != null)
                    {
                        personalNuevo.estatus = "BAJA";
                    }
                    else
                    {
                        personalNuevo.estatus = "ACTIVO";
                    }
                }
                if (personalNuevo.cambioPuesto != null)
                {
                    if (personalNuevo.cambioPuesto.fechaBaja != null)
                    {
                        personalNuevo.estatus = "BAJA";
                    }
                    else
                    {
                        personalNuevo.estatus = "ACTIVO";
                    }
                }
                personalNuevo.puesto = personalNuevo.cambioPuesto.puesto;
                personalNuevo.departamento = personalNuevo.cambioPuesto.departamento;
                personalNuevo.fechaIngreso = personalNuevo.cambioPuesto.fechaInicio;
                return personalNuevo;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        void mapForm(Personal personal)
        {
            try
            {
                if (personal != null)
                {
                    ctrFolioPersonal.Tag = personal;
                    ctrFolioPersonal.valor = personal.clave;
                    txtNombres.Text = personal.nombres;
                    txtApellidoPaterno.Text = personal.apellidoPaterno;
                    txtApellidoMaterno.Text = personal.apellidoMaterno;
                    mapColonia(personal.colonia);
                    txtEstatus.Text = personal.estatus;
                    chcActivo.IsChecked = true;
                    chcActivo.IsChecked = personal.estatusBD == "ACT";
                    dtpFechaNacimiento.SelectedDate = personal.fechaNacimiento;
                    stpFechaIngreso.SelectedDate = personal.fechaIngreso;
                    txtDireccion.Text = personal.direccion;
                    txtTelefono.Text = personal.telefono;
                    cbxDepartamento.SelectedItem = cbxDepartamento.ItemsSource.Cast<Departamento>().ToList().Find
                        (S => S.clave == (personal.departamento == null ? 0 : personal.departamento.clave));
                    cbxPuesto.SelectedItem = cbxPuesto.ItemsSource.Cast<Puesto>().ToList().Find
                        (s => s.idPuesto == (personal.puesto == null ? 0 : personal.puesto.idPuesto));

                    if (personal.estatusBD == "BAJ")
                    {
                        mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.editar | ToolbarCommands.nuevo);
                    }

                    cbxTiposSandre.SelectedItem = personal.tipoSangre;
                    txtCalle.Text = personal.calle;
                    txtNo.Text = personal.numero;
                    txtCruce1.Text = personal.cruce1;
                    txtCruce2.Text = personal.cruce2;
                    txtCiudad.Text = personal.ciudad;
                    txtColoniaLibre.Text = personal.coloniaStr;
                    txtCP.valor = personal.cp;
                    txtTelefonoCelular.Text = personal.telefonoCelular;
                    txtCorreoElectronico.Text = personal.correoElectronico;
                    cbxGenero.SelectedItem = personal.genero;
                    cbxEstadoCivil.SelectedItem = personal.estadoCivil;
                    cbxTallaCamisa.SelectedItem = personal.tallaCamisa;
                    cbxTallaZapato.SelectedItem = personal.tallaZapatos;

                    if (personal.contactoEmergencia != null)
                    {
                        cbxPatentezco.SelectedItem = personal.contactoEmergencia.parentezco;
                        txtNombresContacto.Text = personal.contactoEmergencia.nombres;
                        txtApellidoPaternoContacto.Text = personal.contactoEmergencia.apellidoPaterno;
                        txtApellidoMaternoContacto.Text = personal.contactoEmergencia.apellidoMaterno;
                        txtTelefonoCelularContacto.Text = personal.contactoEmergencia.telefonoCelular;
                        txtTelefonoDomContacto.Text = personal.contactoEmergencia.telefonoDomicilio;
                    }

                   
                    if (personal.ingreso == null)
                    {
                        cbxPuestoIngreso.SelectedItem = cbxPuestoIngreso.ItemsSource.Cast<Puesto>().ToList().Find(s => s.idPuesto == personal.puesto.idPuesto);
                        cbxDepartamentoIngreso.SelectedItem = cbxDepartamentoIngreso.ItemsSource.Cast<Departamento>().ToList().Find(s => s.clave == personal.departamento.clave);
                    }
                    else
                    {
                        mapCamposIngreso(personal.ingreso);
                    }
                    
                    mapCamposReIngreso(personal.reingreso);
                    mapCamposCambio(personal.cambioPuesto);
                    mapDocumentosPersonal(personal.documentacionPersonal);

                    if (personal.ingreso != null)
                    {
                        if (personal.ingreso.fechaBaja != null && personal.reingreso == null)
                        {
                            stpReIngreso.IsEnabled = true;
                            stpReingresoDatos1.IsEnabled = true;
                            stpReingresoDatos2.IsEnabled = true;
                            stpIngresoBaja.IsEnabled = true;
                            stpReingresoObservaciones.IsEnabled = true;
                        }
                    }

                }
                else
                {
                    ctrFolioPersonal.Tag = null;
                    ctrFolioPersonal.valor = 0;
                    txtNombres.Clear();
                    txtApellidoPaterno.Clear();
                    txtApellidoMaterno.Clear();
                    mapColonia(null);
                    txtEstatus.Text = "ACTIVO";
                    dtpFechaNacimiento.SelectedDate = DateTime.Now.AddYears(-18);
                    stpFechaIngreso.SelectedDate = DateTime.Now;
                    txtDireccion.Clear();
                    txtTelefono.Clear();
                    cbxDepartamento.SelectedItem = null;
                    cbxPuesto.SelectedItem = null;
                    chcActivo.IsChecked = true;
                    cbxTiposSandre.SelectedItem = null;
                    txtCalle.Clear();
                    txtNo.Clear();
                    txtCruce1.Clear();
                    txtCruce2.Clear();
                    txtCiudad.Clear();
                    txtColoniaLibre.Clear();
                    txtCP.valor = 0;
                    txtTelefonoCelular.Clear();
                    txtCorreoElectronico.Clear();
                    cbxGenero.SelectedItem = null;
                    cbxEstadoCivil.SelectedItem = null;
                    cbxTallaCamisa.SelectedItem = null;
                    cbxTallaZapato.SelectedItem = null;

                    cbxPatentezco.SelectedItem = null;
                    txtNombresContacto.Clear();
                    txtApellidoPaternoContacto.Clear();
                    txtApellidoMaternoContacto.Clear();
                    txtTelefonoCelularContacto.Clear();
                    txtTelefonoDomContacto.Clear();

                    mapCamposIngreso(null);
                    mapCamposReIngreso(null);
                    mapCamposCambio(null);
                    mapDocumentosPersonal(null);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void mapDocumentosPersonal(DocumentacionPersonal documento)
        {
            if (documento != null)
            {
                txtNSS.Text = documento.numeroSeguroSocial;
                txtCURP.Text = documento.CURP;
                txtRFC.Text = documento.RFC;
                txtINE.Text = documento.INE;
                txtNoExamenPsicofisico.Text = documento.noExamenPsicofisico;
                dtpFechaVigenciaExamen.SelectedDate = documento.fechaVencimientoExamen;
                cbxBanco.SelectedItem = documento.banco;
                txtCuentaBanco.Text = documento.cuenta;
                txtCLABE.Text = documento.clabe;
                txtObservacionesDocumentacion.Text = documento.observaciones;
                mapLicenciaEstatal(documento.licenciaEstatal);
                maplicenciaFederal(documento.licenciaFederal);
            }
            else
            {
                txtNSS.Clear(); ;
                txtCURP.Clear();
                txtRFC.Clear();
                txtINE.Clear();
                txtNoExamenPsicofisico.Clear();
                dtpFechaVigenciaExamen.SelectedDate = null;
                cbxBanco.SelectedItem = null;
                txtCuentaBanco.Clear();
                txtCLABE.Clear();
                txtObservacionesDocumentacion.Clear();
                mapLicenciaEstatal(null);
                maplicenciaFederal(null);
            }
        }

        private void maplicenciaFederal(LicenciaConducir licenciaFederal)
        {
            if (licenciaFederal != null)
            {
                txtNoLicenciaFederal.Tag = licenciaFederal;
                txtNoLicenciaFederal.Text = licenciaFederal.noLicencia;
                cbxTipoFederal.SelectedItem = licenciaFederal.tipoLicencia;
                dtpFechaVigenciaFederal.SelectedDate = licenciaFederal.vencimiento;
            }
            else
            {
                txtNoLicenciaFederal.Tag = null;
                txtNoLicenciaFederal.Clear();
                cbxTipoFederal.SelectedItem = null;
                dtpFechaVigenciaFederal.SelectedDate = null;
            }
        }

        private void mapLicenciaEstatal(LicenciaConducir licenciaEstatal)
        {
            if (licenciaEstatal != null)
            {
                txtNoLicenciaEstatal.Tag = licenciaEstatal;
                txtNoLicenciaEstatal.Text = licenciaEstatal.noLicencia;
                cbxTipoEstatal.SelectedItem = licenciaEstatal.tipoLicencia;
                dtpFechaVigenciaEstatal.SelectedDate = licenciaEstatal.vencimiento;
            }
            else
            {
                txtNoLicenciaEstatal.Tag = null;
                txtNoLicenciaEstatal.Clear();
                cbxTipoEstatal.SelectedItem = null;
                dtpFechaVigenciaEstatal.SelectedDate = null;
            }
        }

        private void mapCamposIngreso(DatosIngresoPersonal ingreso)
        {
            if (ingreso != null)
            {
                dtpFechaContratacionIngreso.SelectedDate = ingreso.fechaInicio;
                dtpFechaFinCapacitacionIngreso.SelectedDate = ingreso.fechaFinCapacitacion;
                dtpFechaBajaIngreso.SelectedDate = ingreso.fechaBaja;
                cbxPuestoIngreso.SelectedItem = cbxPuestoIngreso.ItemsSource.Cast<Puesto>().ToList().Find(s => s.idPuesto == ingreso.puesto.idPuesto);
                cbxDepartamentoIngreso.SelectedItem = cbxDepartamentoIngreso.ItemsSource.Cast<Departamento>().ToList().Find(s => s.clave == ingreso.departamento.clave);
                cbxMotivoBajaIngreso.SelectedItem = ingreso.motivoBaja;
                ctrSalarioDiarioIngreso.valor = ingreso.salarioDiario;
                ctrSalarioHoraIngreso.valor = ingreso.salarioHora;
                ctrMaximoHorasIngreso.valor = ingreso.maximoHorasExtra;
                txtObservacionesIngreso.Text = ingreso.observaciones;

                stpIngresoDatos1.IsEnabled = false;
                stpIngresoDatos2.IsEnabled = false;
                stpIngresoObservaciones.IsEnabled = false;

                if (ingreso.fechaBaja != null)
                {
                    stpIngreso.IsEnabled = false;
                    stpReIngreso.IsEnabled = true;
                    stpIngresoBaja.IsEnabled = false;

                }
                else
                {
                    stpIngreso.IsEnabled = true;
                    stpReIngreso.IsEnabled = false;
                    cbxMotivoBajaIngreso.SelectedItem = null;
                }

            }
            else
            {
                dtpFechaContratacionIngreso.SelectedDate = null;
                dtpFechaFinCapacitacionIngreso.SelectedDate = null;
                dtpFechaBajaIngreso.SelectedDate = null; ;
                cbxPuestoIngreso.SelectedItem = null;
                cbxDepartamentoIngreso.SelectedItem = null;
                cbxMotivoBajaIngreso.SelectedItem = null;
                ctrSalarioDiarioIngreso.valor = 0;
                ctrSalarioHoraIngreso.valor = 0;
                ctrMaximoHorasIngreso.valor = 0;
                txtObservacionesIngreso.Clear();
                stpIngreso.IsEnabled = true;
                stpReIngreso.IsEnabled = false;

                stpIngresoDatos1.IsEnabled = true;
                stpIngresoDatos2.IsEnabled = true;
                stpIngresoObservaciones.IsEnabled = true;
                stpIngresoBaja.IsEnabled = true;
            }
        }
        private void mapCamposReIngreso(DatosIngresoPersonal ingreso)
        {
            if (ingreso != null)
            {
                stpReIngreso.IsEnabled = true;
                dtpFechaContratacionReIngreso.SelectedDate = ingreso.fechaInicio;
                dtpFechaFinCapacitacionReIngreso.SelectedDate = ingreso.fechaFinCapacitacion;
                dtpFechaBajaReIngreso.SelectedDate = ingreso.fechaBaja;
                cbxPuestoReIngreso.SelectedItem = cbxPuestoIngreso.ItemsSource.Cast<Puesto>().ToList().Find(s => s.idPuesto == ingreso.puesto.idPuesto);
                cbxDepartamentoReIngreso.SelectedItem = cbxDepartamentoIngreso.ItemsSource.Cast<Departamento>().ToList().Find(s => s.clave == ingreso.departamento.clave);
                cbxMotivoBajaReIngreso.SelectedItem = ingreso.motivoBaja;
                ctrSalarioDiarioReIngreso.valor = ingreso.salarioDiario;
                ctrSalarioHoraReIngreso.valor = ingreso.salarioHora;
                ctrMaximoHorasReIngreso.valor = ingreso.maximoHorasExtra;
                txtObservacionesReIngreso.Text = ingreso.observaciones;

                stpReingresoDatos1.IsEnabled = false;
                stpReingresoDatos2.IsEnabled = false;
                stpReingresoObservaciones.IsEnabled = false;

                if (ingreso.fechaBaja != null)
                {
                    stpReingresoBaja.IsEnabled = false;
                }
                else
                {
                    stpReingresoBaja.IsEnabled = true;
                }

            }
            else
            {
                dtpFechaContratacionReIngreso.SelectedDate = null;
                dtpFechaFinCapacitacionReIngreso.SelectedDate = null;
                dtpFechaBajaReIngreso.SelectedDate = null; ;
                cbxPuestoReIngreso.SelectedItem = null;
                cbxDepartamentoReIngreso.SelectedItem = null;
                cbxMotivoBajaReIngreso.SelectedItem = null;
                ctrSalarioDiarioReIngreso.valor = 0;
                ctrSalarioHoraReIngreso.valor = 0;
                ctrMaximoHorasReIngreso.valor = 0;
                txtObservacionesReIngreso.Clear();

                stpReingresoDatos1.IsEnabled = true;
                stpReingresoDatos2.IsEnabled = true;
                stpReingresoObservaciones.IsEnabled = true;
                stpReingresoBaja.IsEnabled = true;

                stpReIngreso.IsEnabled = false;
                //stpIngreso.IsEnabled = true;
                //stpReIngreso.IsEnabled = false;
                //stpIngresoBaja.IsEnabled = true;
            }
        }
        private void mapCamposCambio(DatosIngresoPersonal ingreso)
        {
            if (ingreso != null)
            {
                stpCambio.IsEnabled = true;
                dtpFechaContratacionCambio.SelectedDate = ingreso.fechaInicio;
                dtpFechaFinCapacitacionCambio.SelectedDate = ingreso.fechaFinCapacitacion;
                dtpFechaBajaCambio.SelectedDate = ingreso.fechaBaja;
                cbxPuestoCambio.SelectedItem = cbxPuestoIngreso.ItemsSource.Cast<Puesto>().ToList().Find(s => s.idPuesto == ingreso.puesto.idPuesto);
                cbxDepartamentoCambio.SelectedItem = cbxDepartamentoIngreso.ItemsSource.Cast<Departamento>().ToList().Find(s => s.clave == ingreso.departamento.clave);
                cbxMotivoBajaCambio.SelectedItem = ingreso.motivoBaja;
                ctrSalarioDiarioCambio.valor = ingreso.salarioDiario;
                ctrSalarioHoraCambio.valor = ingreso.salarioHora;
                ctrMaximoHorasCambio.valor = ingreso.maximoHorasExtra;
                txtObservacionesCambio.Text = ingreso.observaciones;

                //stpReingresoDatos1.IsEnabled = false;
                //stpReingresoDatos2.IsEnabled = false;
                //stpReingresoObservaciones.IsEnabled = false;

                if (ingreso.fechaBaja != null)
                {
                    stpCambio.IsEnabled = false;
                }
                else
                {
                    stpCambio.IsEnabled = true;
                }

            }
            else
            {
                dtpFechaContratacionCambio.SelectedDate = null;
                dtpFechaFinCapacitacionCambio.SelectedDate = null;
                dtpFechaBajaCambio.SelectedDate = null; ;
                cbxPuestoCambio.SelectedItem = null;
                cbxDepartamentoCambio.SelectedItem = null;
                cbxMotivoBajaCambio.SelectedItem = null;
                ctrSalarioDiarioCambio.valor = 0;
                ctrSalarioHoraCambio.valor = 0;
                ctrMaximoHorasCambio.valor = 0;
                txtObservacionesCambio.Clear();

                //stpReingresoDatos1.IsEnabled = true;
                //stpReingresoDatos2.IsEnabled = true;
                //stpReingresoObservaciones.IsEnabled = true;
                //stpReingresoBaja.IsEnabled = true;

                stpCambio.IsEnabled = false;
            }
        }

        public override void buscar()
        {
            Personal personal = new selectPersonalV2().getPersonalByEmpresa(ctrEmpresa.empresaSelected);
            if (personal != null)
            {
                var resp = new OperadorSvc().getListaDatosIngreso(personal.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    personal.listaIngresos = resp.result as List<DatosIngresoPersonal>;
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    personal.listaIngresos = new List<DatosIngresoPersonal>();
                }
                else
                {
                    imprimir(resp);
                    return;
                }

                var respEmergencia = new OperadorSvc().getContactoEmergencia(personal.clave);
                if (respEmergencia.typeResult == ResultTypes.success)
                {
                    personal.contactoEmergencia = (respEmergencia.result as List<ContactoEmergencia>)[0];
                }
                else if (respEmergencia.typeResult == ResultTypes.recordNotFound)
                {
                    personal.contactoEmergencia = null;
                }
                else
                {
                    imprimir(respEmergencia);
                    return;
                }

                var respDoc = new OperadorSvc().getDocumentosPersonalByIdPersonal(personal.clave);
                if (respDoc.typeResult == ResultTypes.success)
                {
                    personal.documentacionPersonal = (respDoc.result as List<DocumentacionPersonal>)[0];

                    var respLic = new OperadorSvc().getLicenciasConducirByIdPersonal(personal.clave);
                    if (respLic.typeResult == ResultTypes.success)
                    {
                        personal.documentacionPersonal.listaLicencias = respLic.result as List<LicenciaConducir>;
                    }
                    else if (respLic.typeResult == ResultTypes.recordNotFound)
                    {
                        personal.documentacionPersonal.listaLicencias = null;
                    }
                    else
                    {
                        imprimir(respLic);
                        return;
                    }
                }
                else if (respDoc.typeResult == ResultTypes.recordNotFound)
                {
                    personal.documentacionPersonal = null;
                }
                else
                {
                    imprimir(respDoc);
                    return;
                }

                base.buscar();
                mapForm(personal);
            }
            else
            {
                base.nuevo();
            }
        }

        private void btnCambiarColonia_Click(object sender, RoutedEventArgs e)
        {
            mapColonia(null);
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new OperadorSvc().getPersonal_v2ByIdEmpresa(ctrEmpresa.empresaSelected.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    new ReportView().exportarListaPersonal(resp.result as List<Personal>);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void cbxGenero_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxGenero.SelectedItem == null)
            {
                cbxTallaCamisa.ItemsSource = null;
            }
            else if (cbxGenero.SelectedItem.ToString() == "MASCULINO")
            {
                cbxTallaCamisa.ItemsSource = DatosPersonales.getListaTallaCamisaHombres();
            }
            else if (cbxGenero.SelectedItem.ToString() == "FEMENINO")
            {
                cbxTallaCamisa.ItemsSource = DatosPersonales.getListaTallaCamisaMujer();
            }

        }

        private void dtpFechaContratacionIngreso_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dtpFechaContratacionIngreso.SelectedDate != null)
            {
                dtpFechaFinCapacitacionIngreso.SelectedDate = dtpFechaContratacionIngreso.SelectedDate.Value.AddDays(31);
            }
            else
            {
                dtpFechaFinCapacitacionIngreso.SelectedDate = null;
            }
        }

        private void dtpFechaContratacionReIngreso_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dtpFechaContratacionReIngreso.SelectedDate != null)
            {
                dtpFechaFinCapacitacionReIngreso.SelectedDate = dtpFechaContratacionReIngreso.SelectedDate.Value.AddDays(31);
            }
            else
            {
                dtpFechaFinCapacitacionReIngreso.SelectedDate = null;
            }
        }

        private void dtpFechaContratacionCambio_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dtpFechaContratacionCambio.SelectedDate != null)
            {
                dtpFechaFinCapacitacionCambio.SelectedDate = dtpFechaContratacionCambio.SelectedDate.Value.AddDays(31);
            }
            else
            {
                dtpFechaFinCapacitacionCambio.SelectedDate = null;
            }
        }

        private void dtpFechaBajaIngreso_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            //stpReIngreso.IsEnabled = dtpFechaBajaIngreso.SelectedDate != null;

            if (dtpFechaBajaIngreso.SelectedDate == null)
            {
                cbxMotivoBajaIngreso.SelectedItem = null;
                cbxMotivoBajaIngreso.IsEnabled = false;
                mapCamposReIngreso(null);
            }
            else
            {
                cbxMotivoBajaIngreso.IsEnabled = true;
            }
        }

        private void dtpFechaBajaReIngreso_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            //stpCambio.IsEnabled = dtpFechaBajaReIngreso.SelectedDate != null;
            if (dtpFechaBajaIngreso.SelectedDate == null)
            {
                cbxMotivoBajaReIngreso.SelectedItem = null;
                cbxMotivoBajaReIngreso.IsEnabled = false;
                mapCamposReIngreso(null);
            }
            else
            {
                cbxMotivoBajaReIngreso.IsEnabled = true;
            }
        }

        private void dtpFechaVigenciaEstatal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dtpFechaVigenciaEstatal.SelectedDate != null)
            {
                int dias = (dtpFechaVigenciaEstatal.SelectedDate.Value.AddDays(1) - DateTime.Now).Days;
                ctrDiasEstatal.valor = dias <= 0 ? 0 : dias;
            }
            else
            {
                ctrDiasEstatal.valor = 0;
            }
        }

        private void dtpFechaVigenciaFederal_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dtpFechaVigenciaFederal.SelectedDate != null)
            {
                int dias = (dtpFechaVigenciaFederal.SelectedDate.Value.AddDays(1) - DateTime.Now).Days;
                ctrDiasFederal.valor = dias <= 0 ? 0 : dias;
            }
            else
            {
                ctrDiasFederal.valor = 0;
            }
        }

        private void dtpFechaVigenciaExamen_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dtpFechaVigenciaExamen.SelectedDate != null)
            {
                int dias = (dtpFechaVigenciaExamen.SelectedDate.Value.AddDays(1) - DateTime.Now).Days;
                ctrDiasExamen.valor = dias <= 0 ? 0 : dias;
            }
            else
            {
                ctrDiasExamen.valor = 0;
            }
        }
    }
}
