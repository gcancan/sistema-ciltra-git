﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Interfaces;
using Core.Models;
using CoreFletera.Models;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editPagosApoyo.xaml
    /// </summary>
    public partial class editPagosApoyo : ViewBase
    {
        public editPagosApoyo()
        {
            InitializeComponent();
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ctrEmpresa.loaded(mainWindow.usuario);
            ctrPersonal.loaded(eTipoBusquedaPersonal.TODOS, ctrEmpresa.empresaSelected);
            ctrPersonal.txtPersonal.TextChanged += TxtPersonal_TextChanged;
            nuevo();
        }

        private void TxtPersonal_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (lvlPagosApoyo.Items.Count == 0) return;

            CollectionViewSource.GetDefaultView(this.lvlPagosApoyo.ItemsSource).Refresh();
        }

        public override void nuevo()
        {
            base.nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo | ToolbarCommands.guardar);
            ctrPersonal.personal = null;
            cargarPersonalApoyo();
        }
        void cargarPersonalApoyo()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new OperadorSvc().getPersonalConApoyo();
                if (resp.typeResult == ResultTypes.success)
                {
                    llenarLista(resp.result as List<Personal>);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void llenarLista(List<Personal> listaPersonalApoyo)
        {
            lvlPagosApoyo.ItemsSource = listaPersonalApoyo;
            CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlPagosApoyo.ItemsSource);
            defaultView.Filter = new Predicate<object>(this.UserFilter);
        }
        private bool UserFilter(object item) =>
           (string.IsNullOrEmpty(this.ctrPersonal.txtPersonal.Text) ||
           (((item as Personal).nombre.IndexOf(this.ctrPersonal.txtPersonal.Text, StringComparison.OrdinalIgnoreCase) >= 0)));
        private void BtnAgregar_Click(object sender, RoutedEventArgs e)
        {
            List<Personal> listaPersonalApoyo = new List<Personal>();
            listaPersonalApoyo = lvlPagosApoyo.ItemsSource.Cast<Personal>().ToList();
            if (ctrPersonal.personal != null)
            {
                Personal personal = ctrPersonal.personal;

                var resp = new OperadorSvc().getPersonal_v2ByClave(personal.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    personal = resp.result as Personal;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    return;
                }

                if (!listaPersonalApoyo.Exists(s=> s.clave == personal.clave))
                {
                    personal.apoyoPersonal = new ApoyoPersonal();
                    listaPersonalApoyo.Insert (0,personal);
                    lvlPagosApoyo.ItemsSource = listaPersonalApoyo;
                    ctrPersonal.personal = null;
                }
            }            
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                List<Personal> listaPersonalApoyo = new List<Personal>();
                listaPersonalApoyo = lvlPagosApoyo.ItemsSource.Cast<Personal>().ToList();
                var resp = new OperadorSvc().savePagosApoyos(listaPersonalApoyo);
                if (resp.typeResult == ResultTypes.success)
                {
                    //mainWindow.habilitar = true;
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ctrPersonal.personal = null;
        }
    }
}
