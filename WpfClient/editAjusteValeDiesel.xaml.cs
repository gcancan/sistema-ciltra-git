﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using CoreFletera.Interfaces;
    using CoreFletera.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Markup;
    using Xceed.Wpf.Toolkit;

    public partial class editAjusteValeDiesel : WpfClient.ViewBase
    {
        public editAjusteValeDiesel()
        {
            this.InitializeComponent();
        }

        private void cbxProveedor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.cbxProveedor.SelectedItem != null)
            {
                this.txtPrecioExtra.valor = (this.cbxProveedor.SelectedItem as ProveedorCombustible).precio;
            }
        }

        private void chcExtra_Checked(object sender, RoutedEventArgs e)
        {
            this.gboxExtra.IsEnabled = true;
        }

        private void chcExtra_Unchecked(object sender, RoutedEventArgs e)
        {
            this.gboxExtra.IsEnabled = false;
            this.txtTicket.Clear();
            this.txtLitrosExtra.valor = decimal.Zero;
            this.cbxProveedor.SelectedItem = null;
            this.txtPrecioExtra.valor = decimal.Zero;
            this.txtFolioImpresoExtra.Clear();
        }

        private void CtrOrdenServicio_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    base.Cursor = Cursors.Wait;
                    if (this.ctrOrdenServicio.valor != 0)
                    {
                        OperationResult resp = new OperacionSvc().getSalidasEntradasByIdOrdenServ(this.ctrOrdenServicio.valor);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            CargaCombustible result = (CargaCombustible)resp.result;
                            this.mapForm(result);
                        }
                        else
                        {
                            ImprimirMensaje.imprimir(resp);
                        }
                    }
                }
                catch (Exception exception)
                {
                    ImprimirMensaje.imprimir(exception);
                }
                finally
                {
                    base.Cursor = Cursors.Arrow;
                }
            }
        }

        private void CtrVale_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    base.Cursor = Cursors.Wait;
                    if (this.ctrVale.valor != 0)
                    {
                        OperationResult resp = new OperacionSvc().cargaCombustibleByIdVale(this.ctrVale.valor);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            CargaCombustible result = (CargaCombustible)resp.result;
                            this.mapForm(result);
                        }
                        else
                        {
                            ImprimirMensaje.imprimir(resp);
                        }
                    }
                }
                catch (Exception exception)
                {
                    ImprimirMensaje.imprimir(exception);
                }
                finally
                {
                    base.Cursor = Cursors.Arrow;
                }
            }
        }

        public override OperationResult guardar()
        {
            OperationResult result2;
            try
            {
                base.Cursor = Cursors.Wait;
                CargaCombustible cargaGas = this.mapForm();
                if (fechaMinima != null)
                {
                    if (fechaCarga != cargaGas.fechaCombustible)
                    {
                        if (cargaGas.fechaCombustible < fechaMinima)
                        {
                            return new OperationResult(ResultTypes.warning, "NO SE PUEDE AJUSTAR LA FECHA POR QUE ES MENOR AL ULTIMO CIERRE DE PROCESO DE COMBUSTIBLE");
                        }
                    }
                }
                OperationResult result = new OperacionSvc().saveCargaGasolina(ref cargaGas, cargaGas.empresa.clave, null, true);
                if (result.typeResult == ResultTypes.success)
                {
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                }
                result2 = result;
            }
            catch (Exception exception)
            {
                result2 = new OperationResult(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return result2;
        }
        private CargaCombustible mapForm()
        {
            try
            {
                CargaCombustible cargaAnterior = this.ctrVale.Tag as CargaCombustible;


                CargaCombustible cargaNuevo = new CargaCombustible
                {
                    fechaCombustible = new DateTime?(this.dtpFecha.Value.Value),
                    kmFinal = this.ctrKmFinal.valor,
                    kmDespachador = this.ctrKmFinal.valor,
                    ltCombustible = this.ctrLtsDespachados.valor,
                    sello1 = new int?((int)this.ctrSelloIzq.valor),
                    sello2 = new int?((int)this.ctrSelloDer.valor),
                    selloReserva = new int?((int)this.ctrSelloReserva.valor),
                    kmRecorridos = this.ctrKmRecorridos.valor,
                    ltCombustibleUtilizado = this.ctrLtsTotales.valor,
                    ltCombustiblePTO = this.ctrLtsPTO.valor,
                    ltCombustibleEST = this.ctrLtsEST.valor,
                    tiempoTotal = this.txtTiempoTotal.Text,
                    tiempoPTO = this.txtTiempoPTO.Text,
                    tiempoEST = this.txtTiempoEst.Text,
                    zonaOperativa = ctrZonaOperativa.zonaOperativa,
                    empresa = ctrEmpresa.empresaSelected,
                    despachador = cargaAnterior.despachador,
                    ecm = cargaAnterior.ecm,
                    fechaEntrada = cargaAnterior.fechaEntrada,
                    folioImpreso = cargaAnterior.folioImpreso,
                    idCargaCombustible = cargaAnterior.idCargaCombustible,
                    idChofer = cargaAnterior.idChofer,
                    idCliente = cargaAnterior.idCliente,
                    idOrdenServ = cargaAnterior.idOrdenServ,
                    idSalidaEntrada = cargaAnterior.idSalidaEntrada,
                    isExterno = cargaAnterior.isExterno,
                    listCombustibleExtra = cargaAnterior.listCombustibleExtra,
                    kmInicial = cargaAnterior.kmInicial,
                    listCP = cargaAnterior.listCP,
                    ltsExtra = cargaAnterior.ltsExtra,
                    listViajes = cargaAnterior.listViajes,
                    masterOrdServ = cargaAnterior.masterOrdServ,
                    folioImpresoExtra = cargaAnterior.folioImpresoExtra,
                    observacioses = cargaAnterior.observacioses,
                    precioCombustible = cargaAnterior.precioCombustible,
                    proveedor = cargaAnterior.proveedor,
                    proveedorExtra = cargaAnterior.proveedorExtra,
                    sumaLitrosExtra = cargaAnterior.sumaLitrosExtra,
                    tanqueCombustible = cargaAnterior.tanqueCombustible,
                    ticket = cargaAnterior.ticket,
                    ticketExtra = cargaAnterior.ticketExtra,
                    unidadTransporte = cargaAnterior.unidadTransporte,
                    usuarioCrea = cargaAnterior.usuarioCrea
                    
                };

                if (this.chcExtra.IsChecked.Value)
                {
                    cargaNuevo.ticketExtra = this.txtTicket.Text.Trim();
                    cargaNuevo.ltsExtra = this.txtLitrosExtra.valor;
                    cargaNuevo.proveedorExtra = this.cbxProveedor.SelectedItem as ProveedorCombustible;
                    cargaNuevo.proveedorExtra.precio = this.txtPrecioExtra.valor;
                    cargaNuevo.folioImpresoExtra = this.txtFolioImpresoExtra.Text.Trim();
                }
                else
                {
                    cargaNuevo.ticketExtra = string.Empty;
                    cargaNuevo.ltsExtra = decimal.Zero;
                    cargaNuevo.proveedorExtra = null;
                    cargaNuevo.folioImpresoExtra = string.Empty;
                }

                //CargaCombustible tag = this.ctrVale.Tag as CargaCombustible;

                //tag.fechaCombustible = new DateTime?(this.dtpFecha.Value.Value);
                //tag.kmFinal = this.ctrKmFinal.valor;
                //tag.kmDespachador = this.ctrKmFinal.valor;
                //tag.ltCombustible = this.ctrLtsDespachados.valor;
                //tag.sello1 = new int?((int)this.ctrSelloIzq.valor);
                //tag.sello2 = new int?((int)this.ctrSelloDer.valor);
                //tag.selloReserva = new int?((int)this.ctrSelloReserva.valor);
                //tag.kmRecorridos = this.ctrKmRecorridos.valor;
                //tag.ltCombustibleUtilizado = this.ctrLtsTotales.valor;
                //tag.ltCombustiblePTO = this.ctrLtsPTO.valor;
                //tag.ltCombustibleEST = this.ctrLtsEST.valor;
                //tag.tiempoTotal = this.txtTiempoTotal.Text;
                //tag.tiempoPTO = this.txtTiempoPTO.Text;
                //tag.tiempoEST = this.txtTiempoEst.Text;
                //tag.zonaOperativa = ctrZonaOperativa.zonaOperativa;
                //tag.empresa = ctrEmpresa.empresaSelected;
                //if (this.chcExtra.IsChecked.Value)
                //{
                //    tag.ticketExtra = this.txtTicket.Text.Trim();
                //    tag.ltsExtra = this.txtLitrosExtra.valor;
                //    tag.proveedorExtra = this.cbxProveedor.SelectedItem as ProveedorCombustible;
                //    tag.proveedorExtra.precio = this.txtPrecioExtra.valor;
                //    tag.folioImpresoExtra = this.txtFolioImpresoExtra.Text.Trim();
                //}
                //else
                //{
                //    tag.ticketExtra = string.Empty;
                //    tag.ltsExtra = decimal.Zero;
                //    tag.proveedorExtra = null;
                //    tag.folioImpresoExtra = string.Empty;
                //}

                //CargaCombustible cargaNuevo = tag;

                var s = LLenarRegistroAjustes.getAjustesCargaCombustible(cargaAnterior, cargaNuevo, mainWindow.usuario.nombreUsuario);
                cargaNuevo.listaAjustes = s;
                return cargaNuevo;
            }
            catch (Exception)
            {
                return null;
            }
        }

        DateTime? fechaMinima = null;
        DateTime fechaCarga = DateTime.Now;
        private void mapForm(CargaCombustible carga)
        {
            try
            {
                if (carga != null)
                {
                    this.ctrVale.valor = carga.idCargaCombustible;
                    this.ctrVale.Tag = carga;
                    this.ctrTractor.unidadTransporte = carga.masterOrdServ.tractor;
                    this.ctrOrdenServicio.valor = carga.idOrdenServ;
                    this.txtTickets.Text = carga.ticket;
                    this.dtpFecha.Value = carga.fechaCombustible;
                    this.fechaCarga = carga.fechaCombustible.Value;
                    this.gBoxFolios.IsEnabled = false;
                    this.gBoxGenerales.IsEnabled = true;
                    this.ctrKmFinal.valor = carga.kmFinal;
                    this.ctrLtsDespachados.valor = carga.ltCombustible;
                    this.ctrSelloIzq.valor = carga.sello1.Value;
                    this.ctrSelloDer.valor = carga.sello2.Value;
                    this.ctrSelloReserva.valor = !carga.selloReserva.HasValue ? 0 : ((decimal)carga.selloReserva.Value);
                    this.ctrKmRecorridos.valor = carga.kmRecorridos;
                    this.ctrLtsTotales.valor = carga.ltCombustibleUtilizado;
                    this.ctrLtsPTO.valor = carga.ltCombustiblePTO;
                    this.ctrLtsEST.valor = carga.ltCombustibleEST;
                    if ((carga.tiempoTotal == "00:00:00") || (carga.tiempoTotal == "000:00:00"))
                    {
                        this.txtTiempoTotal.Text = "000:00:00";
                    }
                    else
                    {
                        var ar = carga.tiempoTotal.Split(':');
                        if (ar[0].Length == 2)
                        {
                            ar[0] = string.Format("0{0}", ar[0]);
                        }

                        this.txtTiempoTotal.Text = string.Format("{0}:{1}:{2}", ar[0], ar[1], ar[2]);
                    }
                    this.txtTiempoPTO.Text = carga.tiempoPTO;
                    this.txtTiempoEst.Text = carga.tiempoEST;
                    if (!string.IsNullOrEmpty(carga.ticketExtra))
                    {
                        this.expCargaExtra.IsExpanded = true;
                        this.chcExtra.IsChecked = true;
                        this.txtTicket.Text = carga.ticketExtra;
                        this.txtLitrosExtra.valor = carga.ltsExtra;
                        this.cbxProveedor.SelectedItem = (this.cbxProveedor.ItemsSource as List<ProveedorCombustible>).Find(s => s.idProveedor == carga.proveedorExtra.idProveedor);
                        this.txtPrecioExtra.valor = carga.proveedorExtra.precio;
                        this.txtFolioImpresoExtra.Text = carga.folioImpresoExtra;
                    }
                    else
                    {
                        this.expCargaExtra.IsExpanded = false;
                        this.chcExtra.IsChecked = false;
                        this.txtTicket.Clear();
                        this.txtLitrosExtra.valor = decimal.Zero;
                        this.cbxProveedor.SelectedItem = null;
                        this.txtPrecioExtra.valor = decimal.Zero;
                        this.txtFolioImpresoExtra.Clear();
                    }
                    ctrEmpresa.loaded(carga.empresa, false);// = carga.empresa;
                    ctrZonaOperativa.cargarControl(carga.zonaOperativa, 0);

                    if (!string.IsNullOrEmpty(carga.facturaPemex))
                    {
                        dtpFecha.IsEnabled = false;
                        ctrLtsDespachados.IsEnabled = false;
                        btnCancelar.IsEnabled = false;
                    }
                    if (!string.IsNullOrEmpty(carga.facturaProveedor))
                    {
                        txtLitrosExtra.IsEnabled = false;
                        txtLitrosExtra.IsEnabled = false;
                        txtPrecioExtra.IsEnabled = false;
                        btnCancelar.IsEnabled = false;
                        chcExtra.IsEnabled = false;
                        cbxProveedor.IsEnabled = false;
                    }
                    fechaMinima = null;
                    if (carga.zonaOperativa != null)
                    {
                        if (!carga.isExterno)
                        {
                            OperationResult result = new TanqueCombustibleSvc().getTanqueByZonaOperativa(carga.zonaOperativa);
                            if (result.typeResult == ResultTypes.success)
                            {
                                TanqueCombustible tanque = result.result as TanqueCombustible;
                                buscarFechaInicio(tanque.idTanqueCombustible);
                            }                            
                        }                       
                    }
                    dtpFechaMinima.Value = fechaMinima;
                }
                else
                {
                    fechaMinima = null;
                    dtpFechaMinima.Value = fechaMinima;
                    this.fechaCarga = DateTime.Now;
                    this.ctrVale.valor = 0;
                    this.ctrVale.Tag = null;
                    this.ctrTractor.limpiar();
                    this.ctrOrdenServicio.valor = 0;
                    this.txtTickets.Clear();
                    this.dtpFecha.Value = null;
                    this.gBoxFolios.IsEnabled = true;
                    this.gBoxGenerales.IsEnabled = false;
                    this.ctrKmFinal.valor = decimal.Zero;
                    this.ctrLtsDespachados.valor = decimal.Zero;
                    this.ctrSelloIzq.valor = decimal.Zero;
                    this.ctrSelloDer.valor = decimal.Zero;
                    this.ctrSelloReserva.valor = decimal.Zero;
                    this.ctrKmRecorridos.valor = decimal.Zero;
                    this.ctrLtsTotales.valor = decimal.Zero;
                    this.ctrLtsPTO.valor = decimal.Zero;
                    this.ctrLtsEST.valor = decimal.Zero;
                    this.txtTiempoTotal.Text = "000:00:00";
                    this.txtTiempoPTO.Text = "00:00:00";
                    this.txtTiempoEst.Text = "00:00:00";
                    this.chcExtra.IsChecked = false;
                    this.txtTicket.Clear();
                    this.txtLitrosExtra.valor = decimal.Zero;
                    this.cbxProveedor.SelectedItem = null;
                    this.txtPrecioExtra.valor = decimal.Zero;
                    this.txtFolioImpresoExtra.Clear();
                    this.chcExtra.IsChecked = false;
                    ctrEmpresa.loaded(null, false);// = null;
                    ctrZonaOperativa.cargarControl(null, 0);
                    dtpFecha.IsEnabled = true;
                    ctrLtsDespachados.IsEnabled = true;
                    btnCancelar.IsEnabled = true;
                    txtLitrosExtra.IsEnabled = true;
                    txtLitrosExtra.IsEnabled = true;
                    txtPrecioExtra.IsEnabled = true;
                    chcExtra.IsEnabled = true;
                    cbxProveedor.IsEnabled = true;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }
        void buscarFechaInicio(int idTanque)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new SuministroCombustibleSvc().getNivelesIniciales(idTanque);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<NivelInicial> lista = resp.result as List<NivelInicial>;
                    fechaMinima = lista[0].fecha;                    
                }
                else
                {
                    fechaMinima = null;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public override void nuevo()
        {
            base.nuevo();
            this.gboxExtra.IsEnabled = false;
            this.chcExtra.IsChecked = false;
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
            this.mapForm(null);
        }

        private void TxtDecimal_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                this.ctrSumaLitros.valor = this.ctrLtsDespachados.valor + this.txtLitrosExtra.valor;
                this.ctrRelleno.valor = this.ctrSumaLitros.valor - this.ctrLtsTotales.valor;
                if (this.ctrSumaLitros.valor != decimal.Zero)
                {
                    this.ctrRendimiento.valor = Convert.ToDecimal((this.ctrKmRecorridos.valor / (this.ctrLtsDespachados.valor + this.txtLitrosExtra.valor)).ToString("N4"));
                }
                else
                {
                    this.ctrRendimiento.valor = decimal.Zero;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void TxtTickets_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    base.Cursor = Cursors.Wait;
                    if (!string.IsNullOrEmpty(this.txtTickets.Text.Trim()))
                    {
                        OperationResult resp = new CargaCombustibleSvc().getCargaCombustibleExternoByTicket(this.txtTickets.Text.Trim());
                        if (resp.typeResult == ResultTypes.success)
                        {
                            CargaCombustible result = (CargaCombustible)resp.result;
                            this.mapForm(result);
                        }
                        else
                        {
                            ImprimirMensaje.imprimir(resp);
                        }
                    }
                }
                catch (Exception exception)
                {
                    ImprimirMensaje.imprimir(exception);
                }
                finally
                {
                    base.Cursor = Cursors.Arrow;
                }
            }
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.ctrVale.KeyDown += new KeyEventHandler(this.CtrVale_KeyDown);
            this.ctrOrdenServicio.KeyDown += new KeyEventHandler(this.CtrOrdenServicio_KeyDown);
            this.txtTickets.KeyDown += new KeyEventHandler(this.TxtTickets_KeyDown);
            OperationResult result = new CargaCombustibleSvc().getProveedor(0);
            if (result.typeResult == ResultTypes.success)
            {
                this.cbxProveedor.ItemsSource = (List<ProveedorCombustible>)result.result;
            }
            this.ctrKmRecorridos.txtDecimal.TextChanged += new TextChangedEventHandler(this.TxtDecimal_TextChanged);
            this.ctrLtsDespachados.txtDecimal.TextChanged += new TextChangedEventHandler(this.TxtDecimal_TextChanged);
            this.ctrLtsTotales.txtDecimal.TextChanged += new TextChangedEventHandler(this.TxtDecimal_TextChanged);
            this.txtLitrosExtra.txtDecimal.TextChanged += new TextChangedEventHandler(this.TxtDecimal_TextChanged);
            this.nuevo();
        }

        private void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (ctrVale.Tag != null)
                {
                    CargaCombustible carga = this.ctrVale.Tag as CargaCombustible;
                    var resp = new OperacionSvc().cancelaValeCombustible(carga.idCargaCombustible);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        nuevo();
                    }
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
