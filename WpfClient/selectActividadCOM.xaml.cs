﻿using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectActividadCOM.xaml
    /// </summary>
    public partial class selectActividadCOM : Window
    {
        private string parametro = string.Empty;

        public selectActividadCOM()
        {
            InitializeComponent();
        }
        public selectActividadCOM(string parametro)
        {
            this.parametro = parametro;
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtFind.Text = parametro;
            lvlActividades.Focus();
        }
        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlActividades.ItemsSource).Refresh();
        }
        public Actividad getActividadCOM()
        {
            try
            {
                buscarAllActividadesCOM();
                bool? resp = ShowDialog();
                if (resp.Value && lvlActividades.SelectedItem != null)
                {
                    return ((Actividad)((ListViewItem)lvlActividades.SelectedItem).Content);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }
        public Actividad getActividad(List<Actividad> listaActividad)
        {
            try
            {
                mapLista(listaActividad);
                bool? resp = ShowDialog();
                if (resp.Value && lvlActividades.SelectedItem != null)
                {
                    return ((Actividad)((ListViewItem)lvlActividades.SelectedItem).Content);
                }
                else
                {
                    return null;
                }

            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }

        void buscarAllActividadesCOM()
        {
            var resp = new ConsultasTallerSvc().getActividadesCOM(AreaTrabajo.TOTAS);
            if (resp.typeResult == ResultTypes.success)
            {
                mapLista(resp.result as List<Actividad>);
            }
            else if (resp.typeResult != ResultTypes.recordNotFound)
            {
                imprimir(resp);
                IsEnabled = false;
            }
        }

        private void mapLista(List<Actividad> list)
        {
            lvlActividades.ItemsSource = null;
            try
            {
                List<ListViewItem> listLvl = new List<ListViewItem>();
                foreach (var item in list)
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = item;
                    lvl.MouseDoubleClick += Lvl_MouseDoubleClick; ;
                    listLvl.Add(lvl);
                }
                lvlActividades.ItemsSource = listLvl;
                CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlActividades.ItemsSource);
                view.Filter = UserFilter;
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((Actividad)(item as ListViewItem).Content).CCODIGOPRODUCTO.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Actividad)(item as ListViewItem).Content).nombre.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void lvlActividades_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }
    }
}
