﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Interfaces;
using Core.Models;
using Core.Utils;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for editCartaPortesV2.xaml
    /// </summary>
    public partial class editCartaPortesV2 : ViewBase
    {
        public DateTime MyTime { get; set; }
        public editCartaPortesV2()
        {
            InitializeComponent();
            MyTime = DateTime.Now;
            DataContext = this;
        }

        private void btnCargaArchivoXML_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();
            //dialog
            dialog.Filter = "|*.XML";
            dialog.Title = "Elige el archivo XML para subir";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var RUTA = new Util().setRutaXML(dialog.FileName);
                txtRutaXML.Text = RUTA;
            }
        }

        private void establecerRutaXML()
        {
            var RUTA = new Util().getRutaXML();
            if (System.IO.File.Exists(RUTA))
            {
                txtRutaXML.Text = RUTA;
            }
            else
            {
                txtRutaXML.Text = new Util().setRutaXML("");
            }
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                establecerRutaXML();
                var respPrivilegio = new PrivilegioSvc().consultarPrivilegio("APLICAR_FECHA_VIAJE", mainWindow.inicio._usuario.idUsuario);
                if (respPrivilegio.typeResult == ResultTypes.success)
                {
                    txtFecha2.IsEnabled = true;
                }
                else
                {
                    txtFecha2.IsEnabled = false;
                }

                txtTractor.loaded(etipoUniadBusqueda.TRACTOR, mainWindow.empresa);
                txtTolva1.loaded(etipoUniadBusqueda.TOLVA, mainWindow.empresa);
                txtDolly.loaded(etipoUniadBusqueda.DOLLY, mainWindow.empresa);
                txtTolva2.loaded(etipoUniadBusqueda.TOLVA, mainWindow.empresa);
                txtOperador.loaded(eTipoBusquedaPersonal.OPERADORES, mainWindow.empresa);
                txtOperadorAuxiliar.loaded(eTipoBusquedaPersonal.OPERADORES, mainWindow.empresa);
                var v = mapComboClientes();
                if (!v)
                {
                    mainWindow.scrollContainer.IsEnabled = v;
                    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    return;
                }

                txtTractor.DataContextChanged += TxtTractor_DataContextChanged;
                txtTolva1.DataContextChanged += TxtTolva1_DataContextChanged;
                txtDolly.DataContextChanged += TxtDolly_DataContextChanged;
                txtTolva2.DataContextChanged += TxtTolva2_DataContextChanged;
                txtOperador.DataContextChanged += TxtOperador_DataContextChanged;
                txtOperadorAuxiliar.DataContextChanged += TxtOperadorAuxiliar_DataContextChanged;
                selectCliente(mainWindow.inicio._usuario.cliente);
                nuevo();
                chcBoxImprimir.IsChecked = new Util().isActivoImpresion();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void TxtOperadorAuxiliar_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (txtOperadorAuxiliar.DataContext != null)
            {
                txtTara1.Focus();
            }
        }

        private void TxtOperador_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (txtOperador.DataContext != null)
            {
                txtOperador.txtPersonal.Focus();
            }
        }

        private void TxtTolva2_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (txtTolva2.DataContext != null)
            {
                rbnTolva2.IsEnabled = true;
                txtTara2.IsEnabled = true;
                txtOperador.txtPersonal.Focus();
            }
            else
            {
                rbnTolva2.IsEnabled = false;
                txtTara2.IsEnabled = false;
            }
        }

        private void TxtDolly_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (txtDolly.DataContext != null)
            {
                txtTolva2.txtUnidadTrans.Focus();
            }
        }

        private void TxtTolva1_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (txtTolva1.DataContext != null)
            {
                txtDolly.txtUnidadTrans.Focus();
            }
        }

        private void TxtTractor_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (txtTractor.DataContext != null)
            {
                OperationResult resp = new CartaPorteSvc().getCartaPorteByIdCamion(txtTractor.unidadTransporte.clave);
                switch (resp.typeResult)
                {
                    case ResultTypes.success:
                        mapForm(((List<Viaje>)resp.result)[0]);
                        txtProducto.Focus();
                        break;
                    case ResultTypes.error:
                        MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    case ResultTypes.warning:
                        MessageBox.Show(resp.mensaje, "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
                        break;
                    case ResultTypes.recordNotFound:
                        break;
                }
                txtTolva1.txtUnidadTrans.Focus();
            }
        }

        public override void nuevo()
        {
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            mapForm(null);
            txtFecha2.Value = DateTime.Now;
            limpiarDetalles(); mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
            controles.IsEnabled = true;
            rbnTolva1.IsChecked = true;
            rbnTolva2.IsEnabled = false;
            txtTractor.txtUnidadTrans.Focus();
            MyTime = DateTime.Now;
        }

        private void mapForm(Viaje viaje)
        {
            if (viaje != null)
            {
                txtFolio.Tag = viaje;
                txtFolio.Text = viaje.NumGuiaId.ToString();
                selectCliente(viaje.cliente);
                //txtTractor.unidadTransporte = viaje.tractor;
                txtTolva1.unidadTransporte = viaje.remolque;
                txtDolly.unidadTransporte = viaje.dolly;
                txtTolva2.unidadTransporte = viaje.remolque2;
                txtOperador.personal = viaje.operador;
                txtOperadorAuxiliar.personal = viaje.operadorCapacitacion;
                txtTara1.Text = viaje.tara.ToString();
                txtTara2.Text = viaje.tara2.ToString();
                txtTara1.IsEnabled = false;
                txtTara2.IsEnabled = false;
                mapListaNew(viaje.listDetalles);
                lvlDetalles.Items.Clear();
            }
            else
            {
                txtFolio.Tag = viaje;
                txtFolio.Clear();
                selectCliente(mainWindow.inicio._usuario.cliente);
                txtTractor.limpiar();
                txtTolva1.limpiar();
                txtDolly.limpiar();
                txtTolva2.limpiar();
                txtOperador.limpiar();
                txtOperadorAuxiliar.limpiar();
                txtTara1.Clear();
                txtTara2.Clear();
                txtTara1.IsEnabled = true;
                lvlDetallesNew.Items.Clear();
                lvlDetalles.Items.Clear();                
            }
        }
        private void mapListaNew(List<CartaPorte> listaCartaPortes)
        {
            lvlDetallesNew.Items.Clear();
            foreach (var item in listaCartaPortes)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvlDetallesNew.Items.Add(lvl);
            }
        }

        private void selectCliente(Cliente cliente)
        {
            int i = 0;
            foreach (Cliente item in cbxClientes.Items)
            {
                if (item.clave == cliente.clave)
                {
                    cbxClientes.SelectedIndex = i;
                    return;
                }
                i++;
            }
        }

        private bool mapComboClientes()
        {
            try
            {
                OperationResult resp = new ClienteSvc().getClientesALLorById(mainWindow.empresa.clave,0 , true);
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxClientes.ItemsSource = (List<Cliente>)resp.result;
                    return true;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    return false;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return false;
            }
        }

        private void txtProducto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                List<Producto> listProducto = new List<Producto>();
                var mySearch = this.listaProductos.FindAll(S => S.descripcion.IndexOf(txtProducto.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                                           S.clave_externo.IndexOf(txtProducto.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                   
                foreach (var item in mySearch)
                {
                    listProducto.Add(item);
                }
                selectProducto buscador = new selectProducto(txtProducto.Text);
                switch (listProducto.Count)
                {
                    //case 0:
                    //    var resp = buscador.buscarZona(_listZonas);
                    //    mapDestinos(resp);
                    //    break;
                    case 1:
                        mapProductos(listProducto[0]);
                        break;
                    default:
                        var resp = buscador.buscarProductos(this.listaProductos);
                        mapProductos(resp);
                        break;
                }
            }
        }

        private void mapProductos(Producto producto)
        {
            if (producto != null)
            {
                txtProducto.Tag = producto;
                txtProducto.Text = producto.descripcion;
                txtProducto.IsEnabled = false;
                txtDestino.Focus();
            }
            else
            {
                txtProducto.Tag = null;
                txtProducto.Clear();
                txtProducto.IsEnabled = true;
            }
        }
        private List<Zona> _listZonas = new List<Zona>();
        private void txtDestino_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (_listZonas.Count == 0)
                {
                    return;
                }
                List<Zona> listZonas = new List<Zona>();
                var mySearch = _listZonas.FindAll(S => S.descripcion.IndexOf(txtDestino.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                S.claveExtra.ToString().IndexOf(txtDestino.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (var item in mySearch)
                {
                    listZonas.Add(item);
                }

                selectDestinos buscador = new selectDestinos(txtDestino.Text);
                switch (listZonas.Count)
                {
                    //case 0:
                    //    var resp = buscador.buscarZona(_listZonas);
                    //    mapDestinos(resp);
                    //    break;
                    case 1:
                        mapDestinos(listZonas[0]);
                        break;
                    default:
                        var resp = buscador.buscarZona(_listZonas);
                        mapDestinos(resp);
                        break;
                }
            }
        }

        private void mapDestinos(Zona zona)
        {
            if (zona != null)
            {
                txtDestino.Tag = zona;
                txtDestino.Text = zona.descripcion;
                txtDestino.IsEnabled = false;
                txtPesoBruto.Focus();
            }
            else
            {
                txtDestino.Tag = null;
                txtDestino.Clear();
                txtDestino.IsEnabled = true;
            }
        }
        List<Producto> listaProductos = new List<Producto>();
        private void cbxClientes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender != null)
            {
                if (cbxClientes.SelectedItem != null)
                {
                    Cliente cliente = (Cliente)cbxClientes.SelectedItem;
                    var respZonas = new ZonasSvc().getZonasALLorById(0, ((Cliente)cbxClientes.SelectedItem).clave.ToString());
                    _listZonas = (List<Zona>)respZonas.result;

                    var resp = new ProductoSvc().getALLProductosByIdORidFraccion(cliente.clave);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listaProductos = (List<Producto>)resp.result;
                    }
                    else
                    {
                        listaProductos = new List<Producto>();
                    }

                    lvlDetalles.Items.Clear();
                    lvlDetallesNew.Items.Clear();
                }
            }
        }

        private void txtPesoBruto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (string.IsNullOrEmpty(txtPesoBruto.Text.Trim()))
                {
                    MessageBox.Show("Se requiere proporcionar el peso bruto", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                decimal v = 0;
                if (!decimal.TryParse(txtPesoBruto.Text.Trim(), out v))
                {
                    MessageBox.Show("El valor proporcionado para el peso bruto no es valido", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //txtTara.Clear();
                    return;
                }
                if (Convert.ToDecimal(txtPesoBruto.Text.Trim()) < 100)
                {
                    MessageBox.Show("La captura del peso bruto es en Kms", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                txtRemision.Focus();
            }
        }

        private void txtRemision_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtHoraImp.Focus();
            }
        }

        private void txtHoraImp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtObservaciones.Focus();
            }
        }

        private void txtObservaciones_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                addDetalle();
            }
        }
        private void addNewDetalle_Click(object sender, RoutedEventArgs e)
        {
            addDetalle();
        }

        private void addDetalle()
        {
            #region
            if (rbnTolva1.IsChecked.Value)
            {
                if (!string.IsNullOrEmpty(txtTara1.Text.Trim()))
                {
                    decimal dec = 0m;
                    if (!decimal.TryParse(txtTara1.Text.Trim(), out dec))
                    {
                        MessageBox.Show("El valor de la tara 1 no es valido", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("No se ha proporcionado la tara 1 del viaje", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtTara1.Focus();
                    return;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(txtTara2.Text.Trim()))
                {
                    decimal dec = 0m;
                    if (!decimal.TryParse(txtTara2.Text.Trim(), out dec))
                    {
                        MessageBox.Show("El valor de la tara 2 no es valido", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("No se ha proporcionado la tara 2 del viaje", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtTara2.Focus();
                    return;
                }
            }
            #endregion
            if (!validarCampos())
            {
                return;
            }
            string fecha = DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() +
                " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":00";

            try
            {
                CartaPorte cP = new CartaPorte();
                cP.zonaSelect = (Zona)txtDestino.Tag;
                cP.producto = (Producto)txtProducto.Tag;
                cP.PorcIVA = ((Cliente)cbxClientes.SelectedItem).impuesto.valor;
                cP.PorcRetencion = ((Cliente)cbxClientes.SelectedItem).impuestoFlete.valor;
                cP.tara = obtenerPesoBruto();

                if ((Convert.ToDecimal(txtPesoBruto.Text.Trim()) / 1000) < cP.tara)
                {
                    MessageBox.Show("El peso bruto no puede ser menor a la ultima tara", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtPesoBruto.Focus();
                    return;
                }
                cP.pesoBruto = Convert.ToDecimal(txtPesoBruto.Text.Trim()) / 1000;
                cP.tipoPrecio = EstablecerPrecios.establecerPrecio(
                                                                        txtTolva1.unidadTransporte,
                                                                        txtDolly.unidadTransporte,
                                                                        txtTolva2.unidadTransporte, mainWindow.empresa,
                                                                        (Cliente)cbxClientes.SelectedItem
                                                                     );
                cP.tipoPrecioChofer = EstablecerPrecios.establecerPrecioChofer(
                                                                                txtTolva1.unidadTransporte,
                                                                                txtDolly.unidadTransporte,
                                                                                txtTolva2.unidadTransporte, mainWindow.empresa
                                                                                );
                cP.horaImpresion = MyTime;
                cP.horaCaptura = DateTime.Now;
                cP.horaLlegada = Convert.ToDateTime(txtFecha2.Value);
                cP.remision = txtRemision.Text.Trim();
                cP.observaciones = txtObservaciones.Text.Trim();
                cP.remolque = rbnTolva1.IsChecked.Value ? 1 : 2;
                ListViewItem lvl = new ListViewItem();
                lvl.Content = cP;
                lvlDetalles.Items.Add(lvl);
                limpiarDetalles();
                txtProducto.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void limpiarDetalles()
        {
            mapDestinos(null);
            mapProductos(null);
            txtRemision.Clear();
            MyTime = DateTime.Now;
            txtPesoBruto.Clear();
            txtObservaciones.Clear();
        }

        private bool validarCampos()
        {
            if (txtProducto.Tag == null)
            {
                txtProducto.Focus(); return false;
            }
            if (txtDestino.Tag == null)
            {
                txtDestino.Focus(); return false;
            }
            decimal dec = 0m;
            if (!decimal.TryParse(txtPesoBruto.Text.Trim(), out dec))
            {
                txtPesoBruto.Clear();
                txtPesoBruto.Focus();
            }
            int i = 0;
            if (!int.TryParse(txtRemision.Text.Trim(), out i))
            {
                txtRemision.Clear();
                txtRemision.Focus();
            }
            return true;
        }
        private TipoPrecio establecerPrecio()
        {
            if (txtDolly.unidadTransporte != null || txtTolva2.unidadTransporte != null)
            {
                return TipoPrecio.FULL;
            }
            else if (txtTolva1.unidadTransporte.tipoUnidad.descripcion == "TOLVA 35 TON")
            {
                return TipoPrecio.SENCILLO35;
            }
            else
            {
                return TipoPrecio.SENCILLO;
            }
        }
        private decimal obtenerPesoBruto()
        {
            if (rbnTolva1.IsChecked.Value)
            {
                var list = getPesoBruto(1);
                if (list.Count > 0)
                {
                    CartaPorte c = list[list.Count - 1];
                    return c.pesoBruto;
                }
                list = getPesoBrutoNew(1);
                if (list.Count > 0)
                {
                    CartaPorte c = list[list.Count - 1];
                    return c.pesoBruto;
                }
                return Convert.ToDecimal(txtTara1.Text.Trim()) / 1000;
            }
            else if (rbnTolva2.IsChecked.Value)
            {
                var list = getPesoBruto(2);
                if (list.Count > 0)
                {
                    CartaPorte c = list[list.Count - 1];
                    return c.pesoBruto;
                }
                list = getPesoBrutoNew(2);
                if (list.Count > 0)
                {
                    CartaPorte c = list[list.Count - 1];
                    return c.pesoBruto;
                }
                return Convert.ToDecimal(txtTara2.Text.Trim()) / 1000;
            }
            else
                return 0;
        }

        private List<CartaPorte> getPesoBruto(int tolva)
        {
            List<CartaPorte> listCp = new List<CartaPorte>();
            foreach (ListViewItem item in lvlDetalles.Items)
            {
                if (((CartaPorte)item.Content).remolque == tolva)
                {
                    listCp.Add((CartaPorte)item.Content);
                }
            }
            return listCp;
        }

        private List<CartaPorte> getPesoBrutoNew(int tolva)
        {
            List<CartaPorte> listCp = new List<CartaPorte>();
            foreach (ListViewItem item in lvlDetallesNew.Items)
            {
                if (((CartaPorte)item.Content).remolque == tolva)
                {
                    listCp.Add((CartaPorte)item.Content);
                }
            }
            return listCp;
        }

        private void txtTara1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (string.IsNullOrEmpty(txtTara1.Text.Trim()))
                {
                    MessageBox.Show("Se requiere proporcionar la tara", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                decimal v = 0;
                if (!decimal.TryParse(txtTara1.Text.Trim(), out v))
                {
                    MessageBox.Show("El valor proporcionado para la tara no es valido", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtTara1.Clear();
                    return;
                }
                if (Convert.ToDecimal(txtTara1.Text.Trim()) < 100)
                {
                    MessageBox.Show("La captura de la tara es en Kms", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                if (txtTolva2.unidadTransporte != null)
                {
                    txtTara2.Focus();
                }
                else
                {
                    txtProducto.Focus();
                }
            }
        }

        private void txtTara2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (string.IsNullOrEmpty(txtTara2.Text.Trim()))
                {
                    MessageBox.Show("Se requiere proporcionar la tara", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                decimal v = 0;
                if (!decimal.TryParse(txtTara2.Text.Trim(), out v))
                {
                    MessageBox.Show("El valor proporcionado para la tara no es valido", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtTara2.Clear();
                    return;
                }
                if (Convert.ToDecimal(txtTara2.Text.Trim()) < 100)
                {
                    MessageBox.Show("La captura de la tara es en Kms", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                txtProducto.Focus();
            }
        }

        public override OperationResult guardar()
        {
            bool isSave = false;
            try
            {
                Cursor = Cursors.Wait;
                lvlDetallesNew.Focus();
                var resp = validarForm();
                if (resp.typeResult != ResultTypes.success)
                {
                    return resp;
                }
                Viaje viajeBachoco = mapForm();
                if (viajeBachoco != null)
                {
                    int ordenServicio = 0;
                    OperationResult save = new CartaPorteSvc().saveCartaPorte_V2(ref viajeBachoco, ref ordenServicio);
                    if (save.typeResult == ResultTypes.success)
                    {
                        buscarViaje(viajeBachoco.NumGuiaId);
                        isSave = true;
                        //mainWindow.habilitar = true;
                        return save;
                    }
                    else
                    {
                        return save;
                    }
                }
                else
                {
                    return new OperationResult { valor = 2, mensaje = "Objeto no valido" };
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
            finally
            {
                Cursor = Cursors.Arrow;
                if (isSave)
                {
                    mainWindow.habilitar = true;
                    mainWindow.scrollContainer.IsEnabled = true;
                    controles.IsEnabled = false;
                }
            }
        }

        private void buscarViaje(int numGuiaId)
        {
            try
            {
                var resp = new CartaPorteSvc().getCartaPorteById(numGuiaId);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(((List<Viaje>)resp.result)[0]);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private Viaje mapForm()
        {
            try
            {
                Viaje viaje = new Viaje();
                viaje.cliente = ((Cliente)cbxClientes.SelectedItem);
                viaje.NumGuiaId = txtFolio.Tag == null ? 0 : ((Viaje)txtFolio.Tag).NumGuiaId;
                viaje.SerieGuia = "B";
                viaje.IdEmpresa = mainWindow.inicio._usuario.personal.empresa.clave;

                //DateTime fc = (DateTime)txtFecha2.Value;
                //if (fc.Hour < 7)
                //{
                //    DateTime newfc = fc.AddDays(-1);
                //    viaje.fechaBachoco = newfc;
                //}
                //else
                //{
                //    viaje.fechaBachoco = fc;
                //}
                viaje.FechaHoraViaje = Convert.ToDateTime(txtFecha2.Text);
                viaje.tractor = txtTractor.unidadTransporte;
                viaje.remolque = txtTolva1.unidadTransporte;
                viaje.dolly = txtDolly.unidadTransporte == null ? null : txtDolly.unidadTransporte;
                viaje.remolque2 = txtTolva2.unidadTransporte == null ? null : txtTolva2.unidadTransporte;
                viaje.tara = Convert.ToDecimal(txtTara1.Text);
                viaje.tara2 = txtTolva2.unidadTransporte == null ? 0 : Convert.ToDecimal(txtTara2.Text);
                viaje.operador = txtOperador.personal;
                viaje.operadorCapacitacion = txtOperadorAuxiliar.personal;
                viaje.UserViaje = mainWindow.inicio._usuario.idUsuario;
                viaje.EstatusGuia = eEstatus.INICIADO.ToString();
                viaje.listDetalles = new List<CartaPorte>();
                viaje.tipoViaje = establecerTipoViaje();
                //viaje.km = Convert.ToDecimal(txtKm.Text.Trim());
                foreach (ListViewItem item in lvlDetalles.Items)
                {
                    viaje.listDetalles.Add((CartaPorte)item.Content);
                }

                return viaje;
            }

            catch (Exception ex)
            {
                return null;
            }
        }
        private char establecerTipoViaje()
        {

            if (txtTolva1.unidadTransporte != null || txtDolly.unidadTransporte != null)
            {
                return 'F';
            }
            else if (txtTolva1.unidadTransporte.tipoUnidad.descripcion == "TOLVA 35 TON")
            {
                return 'F';
            }
            return 'S';
        }

        private OperationResult validarForm()
        {
            string cadena = "Para Proceder el guardado es necesario" + Environment.NewLine;
            bool error = false;
            if (cbxClientes.SelectedItem == null)
            {
                error = true;
                cadena += "* Elegir un cliente" + Environment.NewLine;
            }
            if (txtTractor.unidadTransporte == null)
            {
                error = true;
                cadena += "* Prorcionar la clave del tractor" + Environment.NewLine;
            }
            if (txtTolva1.unidadTransporte == null)
            {
                error = true;
                cadena += "* Prorcionar la clave del remolque 1" + Environment.NewLine;
            }

            if (txtTolva2.unidadTransporte != null || txtDolly.unidadTransporte != null)
            {
                if (txtDolly.unidadTransporte == null)
                {
                    error = true;
                    cadena += "* Prorcionar la clave del Dolly" + Environment.NewLine;
                }
                if (txtTolva2.unidadTransporte == null)
                {
                    error = true;
                    cadena += "* Prorcionar la clave de la tolva 2" + Environment.NewLine;
                }
                if (!string.IsNullOrEmpty(txtTara2.Text.Trim()))
                {
                    decimal dec = 0m;
                    if (!decimal.TryParse(txtTara2.Text.Trim(), out dec))
                    {
                        error = true;
                        cadena += "* Prorcionar un valor valido para la tara 2 del viaje" + Environment.NewLine;
                    }
                }
                else
                {
                    error = true;
                    cadena += "* Prorcionar la tara 2 del viaje" + Environment.NewLine;
                }
            }
            if (!string.IsNullOrEmpty(txtTara1.Text.Trim()))
            {
                decimal dec = 0m;
                if (!decimal.TryParse(txtTara1.Text.Trim(), out dec))
                {
                    error = true;
                    cadena += "* Prorcionar un valor valido para la tara del viaje" + Environment.NewLine;
                }
            }
            else
            {
                error = true;
                cadena += "* Prorcionar la tara del viaje" + Environment.NewLine;
            }

            if (txtOperador.personal == null)
            {
                error = true;
                cadena += "* Elegir un chofer" + Environment.NewLine;
            }

            if (validarLista())
            {
                error = true;
                cadena += "* Existen datos incorrectos en la lista o estan incompletos" + Environment.NewLine;
            }
            return new OperationResult { valor = error ? 2 : 0, mensaje = cadena };
        }

        private bool validarLista()
        {
            bool error = false;
            foreach (ListViewItem item in lvlDetalles.Items)
            {
                CartaPorte c = (CartaPorte)item.Content;
                if (c.zonaSelect == null)
                {
                    var s = c.horaImpresion;
                    error = true;
                    break;
                }
                if (c.producto == null)
                {
                    error = true;
                    break;
                }
                if (c.remision == "0" || string.IsNullOrEmpty(c.remision))
                {
                    error = true;
                    break;
                }
                if (c.pesoBruto == 0)
                {
                    error = true;
                    break;
                }
            }
            return error;
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            if (txtFolio.Tag != null)
            {
                if (((Viaje)txtFolio.Tag).listDetalles.Count == 0)
                {
                    MessageBox.Show("Se necesita tener al menos una carga para cerrar el viaje", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning); return;
                }
                var ruta = new Util().getRutaXML();
                if (string.IsNullOrEmpty(ruta))
                {
                    MessageBox.Show("Para proceder se requiere la ruta del archivo XML", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                //var respCliente = new SDKconsultasSvc().buscarDatosCliente(viajeBachoco.cliente.clave);
                //if (respCliente.typeResult != ResultTypes.success)
                //{
                //    return respCliente;
                //}
                ComercialCliente cli = new ComercialCliente
                {
                    nombre = ((Cliente)cbxClientes.SelectedItem).nombre,
                    calle = ((Cliente)cbxClientes.SelectedItem).domicilio,
                    rfc = ((Cliente)cbxClientes.SelectedItem).rfc,
                    colonia = ((Cliente)cbxClientes.SelectedItem).colonia,
                    estado = ((Cliente)cbxClientes.SelectedItem).estado,
                    municipio = ((Cliente)cbxClientes.SelectedItem).municipio,
                    pais = ((Cliente)cbxClientes.SelectedItem).pais,
                    telefono = ((Cliente)cbxClientes.SelectedItem).telefono,
                    cp = ((Cliente)cbxClientes.SelectedItem).cp
                };
                ComercialEmpresa emp = new ComercialEmpresa
                {
                    nombre = ((Empresa)mainWindow.inicio._usuario.personal.empresa).nombre,
                    calle = (mainWindow.inicio._usuario.personal.empresa).direccion,
                    rfc = (mainWindow.inicio._usuario.personal.empresa).rfc,
                    colonia = (mainWindow.inicio._usuario.personal.empresa).colonia,
                    estado = (mainWindow.inicio._usuario.personal.empresa).estado,
                    municipio = (mainWindow.inicio._usuario.personal.empresa).municipio,
                    pais = (mainWindow.inicio._usuario.personal.empresa).pais,
                    telefono = (mainWindow.inicio._usuario.personal.empresa).telefono,
                    cp = (mainWindow.inicio._usuario.personal.empresa).cp
                };

                var datosFactura = ObtenerDatosFactura.obtenerDatos(ruta);
                if (datosFactura == null)
                {
                    MessageBox.Show("Ocurrio un error al intentar obtener datos del XML", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                OperationResult resp = new CartaPorteSvc().activarViajeBachoco(((Viaje)txtFolio.Tag).NumGuiaId, eEstatus.ACTIVO);
                if (resp.typeResult == ResultTypes.success)
                {

                    if (new Util().isActivoImpresion())
                    {
                        var listaAgrupada = agruparByZona(((List<Viaje>)resp.result)[0].listDetalles);
                        foreach (var item in listaAgrupada)
                        {
                            imprimirReportes(cli, emp, datosFactura, item, ((List<Viaje>)resp.result)[0]);
                        }
                    }
                    MessageBox.Show("Operación Guadada con exito", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    nuevo();
                }
                else
                {
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
                }

            }
        }
        private List<List<CartaPorte>> agruparByZona(List<CartaPorte> listDetalles)
        {
            var grupo = new AgruparCartaPortes();
            grupo.agruparByZona(listDetalles);
            return grupo.superLista;
        }

        private void imprimirReportes(ComercialCliente respCliente, ComercialEmpresa respEmpresa, DatosFacturaXML datosFactura, List<CartaPorte> listaAgrupada, Viaje viaje)
        {
            try
            {
                Cursor = Cursors.Wait;
                ReportView reporte = new ReportView(mainWindow);
                bool resp = reporte.cartaPorteFalso(respCliente, respEmpresa, datosFactura, listaAgrupada, viaje);
                if (resp)
                {
                    //reporte.Show();
                    //MessageBox.Show("Imprimiendo carta porte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Ocurrio un error al intentar imprimir la Carta Porte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            if (txtFolio.Tag != null)
            {
                string motivo = Microsoft.VisualBasic.Interaction.InputBox("Escribir el motivo de la cancelación", "Motivo de Cancelación");
                if (!string.IsNullOrEmpty(motivo.Trim()))
                {
                    OperationResult resp = new CartaPorteSvc().cancelarViajeBachoco(((Viaje)txtFolio.Tag).NumGuiaId, eEstatus.CANCELADO, motivo.ToUpper());
                    if (resp.typeResult == ResultTypes.success)
                    {
                        MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                        mapForm((Viaje)resp.result);
                    }
                    else
                    {
                        MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Se necesita especificar el motivo de la cancelación", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }
        }
        string archivo = System.Windows.Forms.Application.StartupPath + @"\config.ini";
        private void chcBoxImprimir_Checked(object sender, RoutedEventArgs e)
        {
            new Util().Write("IMPRIMIR", "ACTIVO", "true", archivo);
        }

        private void chcBoxImprimir_Unchecked(object sender, RoutedEventArgs e)
        {
            new Util().Write("IMPRIMIR", "ACTIVO", "false", archivo);
        }
    }
}
