﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for selectZonas.xaml
    /// </summary>
    public partial class selectZonas : Window
    {
        public string parametro = "";
        public string idCliente = "";
        public selectZonas()
        {
            InitializeComponent();
        }

        public selectZonas(string parametro)
        {
            InitializeComponent();
            this.parametro = parametro;
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            txtFind.Text = parametro;

        }
        public Zona findZonas(string idCliente)
        {
            this.idCliente = idCliente.ToString();
            findAllZonas();
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlZonasNew != null && lvlZonasNew.SelectedItem != null)
            {
                return (Zona)((ListViewItem)lvlZonasNew.SelectedItem).Content;
            }
            else
            {
                return null;
            }
        }
        public Zona findZonasByEmpresaCliente(int idEmpresa, int idCliente)
        {
            
            findAllZonaByEmpresaCliente(idEmpresa, idCliente);
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlZonasNew != null && lvlZonasNew.SelectedItem != null)
            {
                return (Zona)((ListViewItem)lvlZonasNew.SelectedItem).Content;
            }
            else
            {
                return null;
            }
        }
        public Zona findZonasIncompletas(int idCliente)
        {
            this.idCliente = idCliente.ToString();
            findAllZonasIncompletas();
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlZonasNew != null && lvlZonasNew.SelectedItem != null)
            {
                return (Zona)((ListViewItem)lvlZonasNew.SelectedItem).Content;
            }
            else
            {
                return null;
            }
        }

        private void mapList(List<Zona> list)
        {
            lvlZonasNew.Items.Clear();
            List<ListViewItem> lvlList = new List<ListViewItem>();
            foreach (Zona item in list)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                lvlList.Add(lvl);
                //lvlUnidades.Items.Add(lvl);
            }
            lvlZonasNew.ItemsSource = lvlList;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlZonasNew.ItemsSource);
            view.Filter = UserFilter;
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return
                        //((Zona)(item as ListViewItem).Content).clave.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Zona)(item as ListViewItem).Content).descripcion.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;// ||
                        //((Zona)(item as ListViewItem).Content).clave_empresa.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;

        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlZonasNew.ItemsSource).Refresh();
        }

        private void findAllZonas()
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult resp = new ZonasSvc().getZonasALLorById(0, idCliente);
                if (resp.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.success)
                {
                    //lvlList = ((List<Personal>)resp.result);
                    mapList(((List<Zona>)resp.result));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format(ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                controles.IsEnabled = false;
            }
            finally { Cursor = Cursors.Arrow; }

        }

        private void findAllZonaByEmpresaCliente(int idEmpresa, int idCliente)
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult resp = new ZonasSvc().getZonasByEmpresaCliente(idEmpresa, idCliente);
                if (resp.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.success)
                {
                    //lvlList = ((List<Personal>)resp.result);
                    mapList(((List<Zona>)resp.result));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format(ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                controles.IsEnabled = false;
            }
            finally { Cursor = Cursors.Arrow; }

        }
        private void findAllZonasIncompletas()
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult resp = new ZonasSvc().getDestinosIncompletosAllById(0, idCliente);
                if (resp.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(resp.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                if (resp.typeResult == ResultTypes.success)
                {
                    //lvlList = ((List<Personal>)resp.result);
                    mapList(((List<Zona>)resp.result));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format(ex.Message), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                controles.IsEnabled = false;
            }
            finally { Cursor = Cursors.Arrow; }

        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                DialogResult = false;
            }
        }

        private void txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                lvlZonasNew.Focus();
            }
        }
    }
}
