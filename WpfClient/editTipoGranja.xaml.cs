﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using Core.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editTipoGranja.xaml
    /// </summary>
    public partial class editTipoGranja : ViewBase
    {
        public editTipoGranja()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);
        }
        void mapForm(TipoGranja tipo)
        {
            try
            {
                if (tipo != null)
                {
                    ctrClave.Tag = tipo;
                    ctrClave.valor = tipo.idTipoGranja;
                    txtTipoGranja.Text = tipo.tipoGranja;
                    chcActivo.IsChecked = tipo.activo;
                }
                else
                {
                    ctrClave.Tag = null;
                    ctrClave.valor = 0;
                    txtTipoGranja.Clear();
                    chcActivo.IsChecked = true;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        public override OperationResult guardar()
        {
            try
            {
                if (string.IsNullOrEmpty(txtTipoGranja.Text.Trim())) return new OperationResult(ResultTypes.warning, "SE REQUIERE ESPECIFICAR EL TIPO DE GRANJA");

                TipoGranja tipoGranja = mapForm();
                if (tipoGranja == null) return new OperationResult(ResultTypes.warning, "OCURRIO UN ERROR AL LEER LOS DATOS DEL FORMULARIO");

                var resp = new TipoGranjaSvc().saveTipoGranja(ref tipoGranja, mainWindow.usuario.nombreUsuario);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(tipoGranja);
                    base.guardar();
                }
                return resp;

            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public override OperationResult eliminar()
        {
            try
            {
                if (string.IsNullOrEmpty(txtTipoGranja.Text.Trim())) return new OperationResult(ResultTypes.warning, "SE REQUIERE ESPECIFICAR EL TIPO DE GRANJA");

                TipoGranja tipoGranja = mapForm();
                tipoGranja.activo = false;
                if (tipoGranja == null) return new OperationResult(ResultTypes.warning, "OCURRIO UN ERROR AL LEER LOS DATOS DEL FORMULARIO");

                var resp = new TipoGranjaSvc().saveTipoGranja(ref tipoGranja, mainWindow.usuario.nombreUsuario);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(tipoGranja);
                    base.eliminar();
                }
                return resp;

            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        private TipoGranja mapForm()
        {
            try
            {
                return new TipoGranja
                {
                    idTipoGranja = ctrClave.Tag == null ? 0 : (ctrClave.Tag as TipoGranja).idTipoGranja,
                    tipoGranja = txtTipoGranja.Text.Trim(),
                    activo = chcActivo.IsChecked.Value
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public override void buscar()
        {
            TipoGranja tipoGranja = new selectTipoGranja().getTipoGranja();
            if (tipoGranja != null)
            {
                mapForm(tipoGranja);
                base.buscar();
            }
            
        }
    }
}
