﻿using Core.Interfaces;
using Core.Models;
using CoreINTELISIS.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para INTELISIS_catClientes.xaml
    /// </summary>
    public partial class INTELISIS_catClientes : UserControl
    {
        SincronizadorINTELISIS pantalla = null;
        public INTELISIS_catClientes(SincronizadorINTELISIS pantalla)
        {
            InitializeComponent();
            this.pantalla = pantalla;
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ctrEmpresa.loaded(pantalla.mainWindow.usuario.empresa, true);
        }
        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            pantalla.stpContenedor.Children.Clear();
        }
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lvlClientes.ItemsSource = null;
                lblContador.Content = 0;
                lblActualizados.Content = 0;
                lblNuevos.Content = 0;
                lblActualizados.Content = 0;
                Cursor = Cursors.Wait;
                var resp = new ClienteINTELISISSvc().getAllClientes();
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Cliente> listaClientes = resp.result as List<Cliente>;
                    llenarLista(listaClientes);
                    lblContador.Content = listaClientes.Count;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void llenarLista(List<Cliente> listaCliente)
        {
            try
            {
                lvlClientes.ItemsSource = null;
                
                this.lvlClientes.ItemsSource = listaCliente;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlClientes.ItemsSource);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtBuscador.Text) ||
            ((((Cliente)item).nombre.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void TxtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (lvlClientes.ItemsSource == null) return;

            CollectionViewSource.GetDefaultView(this.lvlClientes.ItemsSource).Refresh();
        }
        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                
                if (lvlClientes.ItemsSource != null)
                {
                    List<Cliente> listaClientes = (lvlClientes.ItemsSource as List<Cliente>).FindAll(s=>s.isSeleccionado);

                    if (listaClientes.Count>0)
                    {
                        if (new ConfirmarSincronizacionClientes(ctrEmpresa.empresaSelected, listaClientes).confirmarSincronizacion().Value)
                        {
                            sincronizarCliente(ctrEmpresa.empresaSelected, listaClientes);
                        }
                    }                   
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
           
        }
        public void sincronizarCliente(Empresa empresa, List<Cliente> listaCliente)
        {
            try
            {
                Cursor = Cursors.Wait;
                lblActualizados.Content = 0;
                lblNuevos.Content = 0;

                var resp = new ClienteSvc().sincronizarClientes(empresa.clave, listaCliente);
                lblActualizados.Content = resp.noActualizados;
                lblNuevos.Content = resp.noNuevos;
                ImprimirMensaje.imprimir(resp);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void chcSeleccionado_Checked(object sender, RoutedEventArgs e)
        {
            if (lvlClientes.ItemsSource != null)
            {
                lblSeleccionados.Content = lvlClientes.ItemsSource.Cast<Cliente>().ToList().Count(S => S.isSeleccionado);
            }
            else
            {
                lblActualizados.Content = 0;
            }
        }        
    }
}
