﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using Core.Interfaces;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editPresupuestos.xaml
    /// </summary>
    public partial class editPresupuestos : ViewBase
    {
        public editPresupuestos()
        {
            InitializeComponent();
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ctrFolio.txtEntero.KeyDown += TxtEntero_KeyDown;
            Usuario usuario = mainWindow.usuario;
            ctrEmpresas.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrSucusal.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
            ctrEmpresas.loaded(usuario);
            ctrSucusal.cargarControl(usuario);

            cargarModalidades();

            for (int i = (DateTime.Now.Year + 1); i > 2016; i--)
            {
                cbxAño.Items.Add(i);
            }



            ctrDiasNoOperEne.txtEntero.TextChanged += TxtEntero_TextChanged1;
            ctrDiasNoOperFeb.txtEntero.TextChanged += TxtEntero_TextChanged1;
            ctrDiasNoOperMar.txtEntero.TextChanged += TxtEntero_TextChanged1;
            ctrDiasNoOperAbr.txtEntero.TextChanged += TxtEntero_TextChanged1;
            ctrDiasNoOperMay.txtEntero.TextChanged += TxtEntero_TextChanged1;
            ctrDiasNoOperJun.txtEntero.TextChanged += TxtEntero_TextChanged1;
            ctrDiasNoOperJul.txtEntero.TextChanged += TxtEntero_TextChanged1;
            ctrDiasNoOperAgo.txtEntero.TextChanged += TxtEntero_TextChanged1;
            ctrDiasNoOperSep.txtEntero.TextChanged += TxtEntero_TextChanged1;
            ctrDiasNoOperOct.txtEntero.TextChanged += TxtEntero_TextChanged1;
            ctrDiasNoOperNov.txtEntero.TextChanged += TxtEntero_TextChanged1;
            ctrDiasNoOperDic.txtEntero.TextChanged += TxtEntero_TextChanged1;
         

            this.cbxClientes.SelectedItem = this.cbxClientes.ItemsSource.Cast<Cliente>().ToList<Cliente>().Find(s => s.clave == base.mainWindow.usuario.cliente.clave);
            if (this.cbxClientes.SelectedItem != null)
            {
                OperationResult result4 = new PrivilegioSvc().consultarPrivilegio("ADMIN_COMBO_CLIENTES", base.mainWindow.usuario.idUsuario);
                this.cbxClientes.IsEnabled = result4.typeResult == ResultTypes.success;
            }
            cbxClientes.SelectionChanged += CbxClientes_SelectionChanged;

            cbxAño.SelectedItem = cbxAño.Items.Cast<int>().ToList().Find(s => s == DateTime.Now.Year);
            nuevo();
            actualizarDiasNoOperativos();
        }

        private void CbxClientes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            llenarHeader();
        }

        private void llenarHeader()
        {
            if (cbxAño.SelectedItem != null && cbxClientes.SelectedItem != null)
            {
                gboxDetalles.Header = string.Format("Presupuesto {0} para {1} {2}", ctrEmpresas.empresaSelected.nombre, (cbxClientes.SelectedItem as Cliente).nombre, cbxAño.SelectedItem.ToString());
            }

        }

        private void TxtEntero_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (ctrFolio.valor > 0)
                {
                    buscarPresupuestoByClave(ctrFolio.valor);
                }
            }
        }

        private void buscarPresupuestoByClave(int valor)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new PresupuestoSvc().getAllPresupuestosByClave(valor);
                if (resp.typeResult == ResultTypes.success)
                {
                    Presupuesto presupuesto = resp.result as Presupuesto;
                    var resp2 = new PresupuestoSvc().getDetallePresupuestosByClave(presupuesto.idPresupuesto);
                    if (resp2.typeResult == ResultTypes.success)
                    {
                        presupuesto.listaDetalles = resp2.result as List<DetallePresupuesto>;
                        mapForm(presupuesto);
                        base.buscar();
                        mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.editar | ToolbarCommands.cerrar);
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cargarClientesByZonaOperativa();
            llenarHeader();
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cargarClientesByZonaOperativa();
            llenarHeader();
        }

        void cargarClientesByZonaOperativa()
        {
            try
            {
                Cursor = System.Windows.Input.Cursors.Wait;
                cbxClientes.ItemsSource = null;
                Empresa empresa = ctrEmpresas.empresaSelected;
                if (empresa.clave == 1)
                {
                    if (ctrSucusal.zonaOperativa == null) return;

                    var resp = new ClienteSvc().getClientesByZonasOperativas(new List<string> { ctrSucusal.zonaOperativa.idZonaOperativa.ToString() }, empresa.clave);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        var listaOperaciones = resp.result as List<OperacionFletera>;
                        List<Cliente> listaCliente = listaOperaciones.Select(s => s.cliente).ToList();
                        cbxClientes.ItemsSource = listaCliente;
                        if (listaOperaciones.Count == 1)
                        {
                            cbxClientes.SelectedIndex = 0;
                        }
                    }
                    else if (resp.typeResult != ResultTypes.recordNotFound)
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
                else
                {
                    var resp = new ClienteSvc().getClientesALLorById(empresa.clave, 0, true);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        var listaClientes = resp.result as List<Cliente>;
                        cbxClientes.ItemsSource = listaClientes;
                        if (listaClientes.Count == 1)
                        {
                            cbxClientes.SelectedIndex = 0;
                        }
                    }
                    else if (resp.typeResult != ResultTypes.recordNotFound)
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }

            }
            catch (Exception ex)
            {

            }
            finally
            {
                Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }
        private void TxtEntero_TextChanged1(object sender, TextChangedEventArgs e)
        {
            actualizarDiasNoOperativos();
        }
        void actualizarDiasNoOperativos()
        {
            int año = (int)cbxAño.SelectedItem;
            lblSumaDiasOperativosEne.Content = DateTime.DaysInMonth(año, 1) - ctrDiasNoOperEne.valor;
            lblSumaDiasOperativosFeb.Content = DateTime.DaysInMonth(año, 2) - ctrDiasNoOperFeb.valor;
            lblSumaDiasOperativosMar.Content = DateTime.DaysInMonth(año, 3) - ctrDiasNoOperMar.valor;
            lblSumaDiasOperativosAbril.Content = DateTime.DaysInMonth(año, 4) - ctrDiasNoOperAbr.valor;
            lblSumaDiasOperativosMay.Content = DateTime.DaysInMonth(año, 5) - ctrDiasNoOperMay.valor;
            lblSumaDiasOperativosJunio.Content = DateTime.DaysInMonth(año, 6) - ctrDiasNoOperJun.valor;
            lblSumaDiasOperativosJulio.Content = DateTime.DaysInMonth(año, 7) - ctrDiasNoOperJul.valor;
            lblSumaDiasOperativosAgo.Content = DateTime.DaysInMonth(año, 8) - ctrDiasNoOperAgo.valor;
            lblSumaDiasOperativosSep.Content = DateTime.DaysInMonth(año, 9) - ctrDiasNoOperSep.valor;
            lblSumaDiasOperativosOct.Content = DateTime.DaysInMonth(año, 10) - ctrDiasNoOperOct.valor;
            lblSumaDiasOperativosNov.Content = DateTime.DaysInMonth(año, 11) - ctrDiasNoOperNov.valor;
            lblSumaDiasOperativosDic.Content = DateTime.DaysInMonth(año, 12) - ctrDiasNoOperDic.valor;

            lblSumaDiasOperativosSuma.Content = ((int)lblSumaDiasOperativosEne.Content + (int)lblSumaDiasOperativosFeb.Content + (int)lblSumaDiasOperativosMar.Content + (int)lblSumaDiasOperativosAbril.Content + (int)lblSumaDiasOperativosJunio.Content + (int)lblSumaDiasOperativosJulio.Content +
                (int)lblSumaDiasOperativosAgo.Content + (int)lblSumaDiasOperativosSep.Content + (int)lblSumaDiasOperativosOct.Content + (int)lblSumaDiasOperativosNov.Content + (int)lblSumaDiasOperativosDic.Content + (int)lblSumaDiasOperativosEne.Content);
        }

        void cargarModalidades()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new ModalidadSvc().getAllModalidades();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxModalidad.ItemsSource = resp.result as List<Modalidades>;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            if (cbxModalidad.SelectedItem == null) return;

            Modalidades modalidad = cbxModalidad.SelectedItem as Modalidades;
            if (listaPreToneladas.Exists(s => s.modalidad.idModalidad == modalidad.idModalidad)) return;

            if (cbxModalidad.SelectedItem != null)
            {
                addDetallePresupuestoEntero(
                    new DetallePresupuesto
                    {
                        modalidad = modalidad,
                        indicador = Indicador.TONELADAS
                    });
                actualizarViajes();
                addDetallePresupuestoEntero(
                    new DetallePresupuesto
                    {
                        modalidad = modalidad,
                        indicador = Indicador.KILOMETROS
                    });
                addDetallePresupuestoEntero(
                    new DetallePresupuesto
                    {
                        modalidad = modalidad,
                        indicador = Indicador.LITROS
                    });
                addDetallePresupuestoEntero(
                   new DetallePresupuesto
                   {
                       modalidad = modalidad,
                       indicador = Indicador.VENTAS
                   });
            }
        }
        void addDetallePresupuestoDecimal(DetallePresupuesto detalle)
        {
            try
            {
                StackPanel stpPrincipal = new StackPanel { Orientation = Orientation.Horizontal, Tag = detalle };

                Label lblModalidad = new Label
                {
                    Content = detalle.modalidad.modalidad,
                    Width = 100,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    FontWeight = FontWeights.Medium,
                    HorizontalContentAlignment = HorizontalAlignment.Center
                };

                Label lblSubModalidad = new Label
                {
                    Content = detalle.modalidad.descripcion,
                    Width = 100,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    FontWeight = FontWeights.Medium,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    BorderThickness = new Thickness(0, 0, 1, 0),
                    BorderBrush = Brushes.Black
                };

                controlDecimal ctrDecimalEnero = new controlDecimal
                {
                    valor = detalle.enero,
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalEnero",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalEnero.txtDecimal.Tag = ctrDecimalEnero;
                ctrDecimalEnero.txtDecimal.TextChanged += TxtDecimalToneladas_TextChanged;

                controlDecimal ctrDecimalFebrero = new controlDecimal
                {
                    valor = detalle.febrero,
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalFebrero",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalFebrero.txtDecimal.Tag = ctrDecimalFebrero;
                ctrDecimalFebrero.txtDecimal.TextChanged += TxtDecimalToneladas_TextChanged;

                controlDecimal ctrDecimalMarzo = new controlDecimal
                {
                    valor = detalle.marzo,
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalMarzo",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalMarzo.txtDecimal.Tag = ctrDecimalMarzo;
                ctrDecimalMarzo.txtDecimal.TextChanged += TxtDecimalToneladas_TextChanged;

                controlDecimal ctrDecimalAbril = new controlDecimal
                {
                    valor = detalle.abril,
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalAbril",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalAbril.txtDecimal.Tag = ctrDecimalAbril;
                ctrDecimalAbril.txtDecimal.TextChanged += TxtDecimalToneladas_TextChanged;

                controlDecimal ctrDecimalMayo = new controlDecimal
                {
                    valor = detalle.mayo,
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalMayo",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalMayo.txtDecimal.Tag = ctrDecimalMayo;
                ctrDecimalMayo.txtDecimal.TextChanged += TxtDecimalToneladas_TextChanged;

                controlDecimal ctrDecimalJunio = new controlDecimal
                {
                    valor = detalle.junio,
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalJunio",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalJunio.txtDecimal.Tag = ctrDecimalJunio;
                ctrDecimalJunio.txtDecimal.TextChanged += TxtDecimalToneladas_TextChanged;

                controlDecimal ctrDecimalJulio = new controlDecimal
                {
                    valor = detalle.julio,
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalJulio",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalJulio.txtDecimal.Tag = ctrDecimalJulio;
                ctrDecimalJulio.txtDecimal.TextChanged += TxtDecimalToneladas_TextChanged;

                controlDecimal ctrDecimalAgosto = new controlDecimal
                {
                    valor = detalle.agosto,
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalAgosto",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalAgosto.txtDecimal.Tag = ctrDecimalAgosto;
                ctrDecimalAgosto.txtDecimal.TextChanged += TxtDecimalToneladas_TextChanged;

                controlDecimal ctrDecimalSeptiembre = new controlDecimal
                {
                    valor = detalle.septiembre,
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalSeptiembre",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalSeptiembre.txtDecimal.Tag = ctrDecimalSeptiembre;
                ctrDecimalSeptiembre.txtDecimal.TextChanged += TxtDecimalToneladas_TextChanged;

                controlDecimal ctrDecimalOctubre = new controlDecimal
                {
                    valor = detalle.octubre,
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalOctubre",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalOctubre.txtDecimal.Tag = ctrDecimalOctubre;
                ctrDecimalOctubre.txtDecimal.TextChanged += TxtDecimalToneladas_TextChanged;

                controlDecimal ctrDecimalNoviembre = new controlDecimal
                {
                    valor = detalle.noviembre,
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalNoviembre",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalNoviembre.txtDecimal.Tag = ctrDecimalNoviembre;
                ctrDecimalNoviembre.txtDecimal.TextChanged += TxtDecimalToneladas_TextChanged;

                controlDecimal ctrDecimalDiciembre = new controlDecimal
                {
                    valor = detalle.diciembre,
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalDiciembre",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalDiciembre.txtDecimal.Tag = ctrDecimalDiciembre;
                ctrDecimalDiciembre.txtDecimal.TextChanged += TxtDecimalToneladas_TextChanged;

                Label lblSumaAnual = new Label
                {
                    Name = "lblSumaAnual",
                    Content = detalle.sumaAnual.ToString("N2"),
                    Width = 140,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    FontWeight = FontWeights.Bold,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    BorderThickness = new Thickness(1, 0, 0, 0),
                    BorderBrush = Brushes.Black
                };

                stpPrincipal.Children.Add(lblModalidad);
                stpPrincipal.Children.Add(lblSubModalidad);

                stpPrincipal.Children.Add(ctrDecimalEnero);
                stpPrincipal.Children.Add(ctrDecimalFebrero);
                stpPrincipal.Children.Add(ctrDecimalMarzo);
                stpPrincipal.Children.Add(ctrDecimalAbril);
                stpPrincipal.Children.Add(ctrDecimalMayo);
                stpPrincipal.Children.Add(ctrDecimalJunio);
                stpPrincipal.Children.Add(ctrDecimalJulio);
                stpPrincipal.Children.Add(ctrDecimalAgosto);
                stpPrincipal.Children.Add(ctrDecimalSeptiembre);
                stpPrincipal.Children.Add(ctrDecimalOctubre);
                stpPrincipal.Children.Add(ctrDecimalNoviembre);
                stpPrincipal.Children.Add(ctrDecimalDiciembre);

                stpPrincipal.Children.Add(lblSumaAnual);

                switch (detalle.indicador)
                {
                    case Indicador.TONELADAS:
                        stpToneladas.Children.Add(stpPrincipal);
                        break;
                    case Indicador.VIAJES:
                        break;
                    case Indicador.KILOMETROS:
                        stpKm.Children.Add(stpPrincipal);
                        break;
                    case Indicador.LITROS:
                        stpLitros.Children.Add(stpPrincipal);
                        break;
                    case Indicador.VENTAS:
                        stpVentas.Children.Add(stpPrincipal);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        void addDetallePresupuestoEntero(DetallePresupuesto detalle)
        {
            try
            {
                StackPanel stpPrincipal = new StackPanel { Orientation = Orientation.Horizontal, Tag = detalle };

                Label lblModalidad = new Label
                {
                    Content = detalle.modalidad.modalidad,
                    Width = 100,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    FontWeight = FontWeights.Medium,
                    HorizontalContentAlignment = HorizontalAlignment.Center
                };

                Label lblSubModalidad = new Label
                {
                    Content = detalle.modalidad.descripcion,
                    Width = 100,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    FontWeight = FontWeights.Medium,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    BorderThickness = new Thickness(0, 0, 1, 0),
                    BorderBrush = Brushes.Black
                };

                controlEnteros ctrDecimalEnero = new controlEnteros
                {
                    valor = Convert.ToInt32(detalle.enero),
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalEnero",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalEnero.txtEntero.Tag = ctrDecimalEnero;
                ctrDecimalEnero.txtEntero.TextChanged += TxtEntero_TextChanged;

                controlEnteros ctrDecimalFebrero = new controlEnteros
                {
                    valor = Convert.ToInt32(detalle.febrero),
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalFebrero",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalFebrero.txtEntero.Tag = ctrDecimalFebrero;
                ctrDecimalFebrero.txtEntero.TextChanged += TxtEntero_TextChanged;

                controlEnteros ctrDecimalMarzo = new controlEnteros
                {
                    valor = Convert.ToInt32(detalle.marzo),
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalMarzo",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalMarzo.txtEntero.Tag = ctrDecimalMarzo;
                ctrDecimalMarzo.txtEntero.TextChanged += TxtEntero_TextChanged;

                controlEnteros ctrDecimalAbril = new controlEnteros
                {
                    valor = Convert.ToInt32(detalle.abril),
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalAbril",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalAbril.txtEntero.Tag = ctrDecimalAbril;
                ctrDecimalAbril.txtEntero.TextChanged += TxtEntero_TextChanged;

                controlEnteros ctrDecimalMayo = new controlEnteros
                {
                    valor = Convert.ToInt32(detalle.mayo),
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalMayo",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalMayo.txtEntero.Tag = ctrDecimalMayo;
                ctrDecimalMayo.txtEntero.TextChanged += TxtEntero_TextChanged;

                controlEnteros ctrDecimalJunio = new controlEnteros
                {
                    valor = Convert.ToInt32(detalle.junio),
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalJunio",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalJunio.txtEntero.Tag = ctrDecimalJunio;
                ctrDecimalJunio.txtEntero.TextChanged += TxtEntero_TextChanged;

                controlEnteros ctrDecimalJulio = new controlEnteros
                {
                    valor = Convert.ToInt32(detalle.julio),
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalJulio",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalJulio.txtEntero.Tag = ctrDecimalJulio;
                ctrDecimalJulio.txtEntero.TextChanged += TxtEntero_TextChanged;

                controlEnteros ctrDecimalAgosto = new controlEnteros
                {
                    valor = Convert.ToInt32(detalle.agosto),
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalAgosto",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalAgosto.txtEntero.Tag = ctrDecimalAgosto;
                ctrDecimalAgosto.txtEntero.TextChanged += TxtEntero_TextChanged;

                controlEnteros ctrDecimalSeptiembre = new controlEnteros
                {
                    valor = Convert.ToInt32(detalle.septiembre),
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalSeptiembre",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalSeptiembre.txtEntero.Tag = ctrDecimalSeptiembre;
                ctrDecimalSeptiembre.txtEntero.TextChanged += TxtEntero_TextChanged;

                controlEnteros ctrDecimalOctubre = new controlEnteros
                {
                    valor = Convert.ToInt32(detalle.octubre),
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalOctubre",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalOctubre.txtEntero.Tag = ctrDecimalOctubre;
                ctrDecimalOctubre.txtEntero.TextChanged += TxtEntero_TextChanged;

                controlEnteros ctrDecimalNoviembre = new controlEnteros
                {
                    valor = Convert.ToInt32(detalle.noviembre),
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalNoviembre",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalNoviembre.txtEntero.Tag = ctrDecimalNoviembre;
                ctrDecimalNoviembre.txtEntero.TextChanged += TxtEntero_TextChanged;

                controlEnteros ctrDecimalDiciembre = new controlEnteros
                {
                    valor = Convert.ToInt32(detalle.diciembre),
                    Width = 100,
                    Tag = stpPrincipal,
                    Name = "ctrDecimalDiciembre",
                    alienacionTexto = HorizontalAlignment.Right
                };
                ctrDecimalDiciembre.txtEntero.Tag = ctrDecimalDiciembre;
                ctrDecimalDiciembre.txtEntero.TextChanged += TxtEntero_TextChanged;

                Label lblSumaAnual = new Label
                {
                    Name = "lblSumaAnual",
                    Content = detalle.sumaAnual.ToString("N0"),
                    Width = 140,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    FontWeight = FontWeights.Bold,
                    HorizontalContentAlignment = HorizontalAlignment.Right,
                    BorderThickness = new Thickness(1, 0, 0, 0),
                    BorderBrush = Brushes.Black
                };

                stpPrincipal.Children.Add(lblModalidad);
                stpPrincipal.Children.Add(lblSubModalidad);

                stpPrincipal.Children.Add(ctrDecimalEnero);
                stpPrincipal.Children.Add(ctrDecimalFebrero);
                stpPrincipal.Children.Add(ctrDecimalMarzo);
                stpPrincipal.Children.Add(ctrDecimalAbril);
                stpPrincipal.Children.Add(ctrDecimalMayo);
                stpPrincipal.Children.Add(ctrDecimalJunio);
                stpPrincipal.Children.Add(ctrDecimalJulio);
                stpPrincipal.Children.Add(ctrDecimalAgosto);
                stpPrincipal.Children.Add(ctrDecimalSeptiembre);
                stpPrincipal.Children.Add(ctrDecimalOctubre);
                stpPrincipal.Children.Add(ctrDecimalNoviembre);
                stpPrincipal.Children.Add(ctrDecimalDiciembre);

                stpPrincipal.Children.Add(lblSumaAnual);

                switch (detalle.indicador)
                {
                    case Indicador.TONELADAS:
                        stpToneladas.Children.Add(stpPrincipal);
                        break;
                    case Indicador.VIAJES:
                        break;
                    case Indicador.KILOMETROS:
                        stpKm.Children.Add(stpPrincipal);
                        break;
                    case Indicador.LITROS:
                        stpLitros.Children.Add(stpPrincipal);
                        break;
                    case Indicador.VENTAS:
                        stpVentas.Children.Add(stpPrincipal);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void TxtEntero_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender != null)
            {
                TextBox txt = sender as TextBox;
                controlEnteros ctr = txt.Tag as controlEnteros;
                StackPanel stpPrincipal = ctr.Tag as StackPanel;
                DetallePresupuesto detalle = stpPrincipal.Tag as DetallePresupuesto;
                var d = ctr.valor;

                switch (ctr.Name)
                {
                    case "ctrDecimalEnero":
                        detalle.enero = ctr.valor;
                        break;
                    case "ctrDecimalFebrero":
                        detalle.febrero = ctr.valor;
                        break;
                    case "ctrDecimalMarzo":
                        detalle.marzo = ctr.valor;
                        break;
                    case "ctrDecimalAbril":
                        detalle.abril = ctr.valor;
                        break;
                    case "ctrDecimalMayo":
                        detalle.mayo = ctr.valor;
                        break;
                    case "ctrDecimalJunio":
                        detalle.junio = ctr.valor;
                        break;
                    case "ctrDecimalJulio":
                        detalle.julio = ctr.valor;
                        break;
                    case "ctrDecimalAgosto":
                        detalle.agosto = ctr.valor;
                        break;
                    case "ctrDecimalSeptiembre":
                        detalle.septiembre = ctr.valor;
                        break;
                    case "ctrDecimalOctubre":
                        detalle.octubre = ctr.valor;
                        break;
                    case "ctrDecimalNoviembre":
                        detalle.noviembre = ctr.valor;
                        break;
                    case "ctrDecimalDiciembre":
                        detalle.diciembre = ctr.valor;
                        break;
                    default:
                        break;
                }
                foreach (object item in stpPrincipal.Children.Cast<object>().ToList())
                {
                    if (item is Label)
                    {
                        Label lblSuma = item as Label;
                        if (lblSuma.Name == "lblSumaAnual")
                        {
                            lblSuma.Content = detalle.sumaAnual.ToString("N0");
                        }
                    }
                }
                sumarKms();
                sumarLitros();
                sumarToneladas();
                actualizarViajes();
                sumarVentas();
            }
        }

        void sumarToneladas()
        {
            List<DetallePresupuesto> listaDetalles = this.listaPreToneladas;

            lblTonEne.Content = listaDetalles.Sum(s => s.enero).ToString("N0");
            lblTonFeb.Content = listaDetalles.Sum(s => s.febrero).ToString("N0");
            lblTonMar.Content = listaDetalles.Sum(s => s.marzo).ToString("N0");
            lblTonAbril.Content = listaDetalles.Sum(s => s.abril).ToString("N0");
            lblTonMay.Content = listaDetalles.Sum(s => s.mayo).ToString("N0");
            lblTonJunio.Content = listaDetalles.Sum(s => s.junio).ToString("N0");
            lblTonJulio.Content = listaDetalles.Sum(s => s.julio).ToString("N0");
            lblTonAgo.Content = listaDetalles.Sum(s => s.agosto).ToString("N0");
            lblTonSep.Content = listaDetalles.Sum(s => s.septiembre).ToString("N0");
            lblTonOct.Content = listaDetalles.Sum(s => s.octubre).ToString("N0");
            lblTonNov.Content = listaDetalles.Sum(s => s.noviembre).ToString("N0");
            lblTonDic.Content = listaDetalles.Sum(s => s.diciembre).ToString("N0");
            lblTonSuma.Content = listaDetalles.Sum(s => s.sumaAnual).ToString("N0");
        }
        List<DetallePresupuesto> listaPreToneladas =>
            stpToneladas.Children.Count == 0 ? new List<DetallePresupuesto>() :
            stpToneladas.Children.Cast<StackPanel>().ToList().Select(s => s.Tag as DetallePresupuesto).ToList();
        List<DetallePresupuesto> listaPreViajes =>
            stpViajes.Children.Count == 0 ? new List<DetallePresupuesto>() :
            stpViajes.Children.Cast<StackPanel>().ToList().Select(s => s.Tag as DetallePresupuesto).ToList();
        List<DetallePresupuesto> listaPreKm =>
            stpKm.Children.Count == 0 ? new List<DetallePresupuesto>() :
            stpKm.Children.Cast<StackPanel>().ToList().Select(s => s.Tag as DetallePresupuesto).ToList();
        List<DetallePresupuesto> listaPreLitros =>
            stpLitros.Children.Count == 0 ? new List<DetallePresupuesto>() :
            stpLitros.Children.Cast<StackPanel>().ToList().Select(s => s.Tag as DetallePresupuesto).ToList();
        List<DetallePresupuesto> listaPreVentas =>
            stpVentas.Children.Count == 0 ? new List<DetallePresupuesto>() :
            stpVentas.Children.Cast<StackPanel>().ToList().Select(s => s.Tag as DetallePresupuesto).ToList();
        private void TxtDecimalToneladas_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender != null)
            {
                TextBox txt = sender as TextBox;
                controlDecimal ctr = txt.Tag as controlDecimal;
                StackPanel stpPrincipal = ctr.Tag as StackPanel;
                DetallePresupuesto detalle = stpPrincipal.Tag as DetallePresupuesto;
                var d = ctr.valor;

                switch (ctr.Name)
                {
                    case "ctrDecimalEnero":
                        detalle.enero = ctr.valor;
                        break;
                    case "ctrDecimalFebrero":
                        detalle.febrero = ctr.valor;
                        break;
                    case "ctrDecimalMarzo":
                        detalle.marzo = ctr.valor;
                        break;
                    case "ctrDecimalAbril":
                        detalle.abril = ctr.valor;
                        break;
                    case "ctrDecimalMayo":
                        detalle.mayo = ctr.valor;
                        break;
                    case "ctrDecimalJunio":
                        detalle.junio = ctr.valor;
                        break;
                    case "ctrDecimalJulio":
                        detalle.julio = ctr.valor;
                        break;
                    case "ctrDecimalAgosto":
                        detalle.agosto = ctr.valor;
                        break;
                    case "ctrDecimalSeptiembre":
                        detalle.septiembre = ctr.valor;
                        break;
                    case "ctrDecimalOctubre":
                        detalle.octubre = ctr.valor;
                        break;
                    case "ctrDecimalNoviembre":
                        detalle.noviembre = ctr.valor;
                        break;
                    case "ctrDecimalDiciembre":
                        detalle.diciembre = ctr.valor;
                        break;
                    default:
                        break;
                }
                foreach (object item in stpPrincipal.Children.Cast<object>().ToList())
                {
                    if (item is Label)
                    {
                        Label lblSuma = item as Label;
                        if (lblSuma.Name == "lblSumaAnual")
                        {
                            lblSuma.Content = detalle.sumaAnual.ToString("N2");
                        }
                    }
                }

                sumarToneladas();
                actualizarViajes();
                sumarVentas();
            }
        }
        void actualizarViajes()
        {
            stpViajes.Children.Clear();
            try
            {
                foreach (var detalleTon in listaPreToneladas)
                {
                    DetallePresupuesto detalle = new DetallePresupuesto()
                    {
                        modalidad = detalleTon.modalidad,
                        enero = Convert.ToInt32(detalleTon.enero / detalleTon.modalidad.subModalidad),
                        febrero = Convert.ToInt32(detalleTon.febrero / detalleTon.modalidad.subModalidad),
                        marzo = Convert.ToInt32(detalleTon.marzo / detalleTon.modalidad.subModalidad),
                        abril = Convert.ToInt32(detalleTon.abril / detalleTon.modalidad.subModalidad),
                        mayo = Convert.ToInt32(detalleTon.mayo / detalleTon.modalidad.subModalidad),
                        junio = Convert.ToInt32(detalleTon.junio / detalleTon.modalidad.subModalidad),
                        julio = Convert.ToInt32(detalleTon.julio / detalleTon.modalidad.subModalidad),
                        agosto = Convert.ToInt32(detalleTon.agosto / detalleTon.modalidad.subModalidad),
                        septiembre = Convert.ToInt32(detalleTon.septiembre / detalleTon.modalidad.subModalidad),
                        octubre = Convert.ToInt32(detalleTon.octubre / detalleTon.modalidad.subModalidad),
                        noviembre = Convert.ToInt32(detalleTon.noviembre / detalleTon.modalidad.subModalidad),
                        diciembre = Convert.ToInt32(detalleTon.diciembre / detalleTon.modalidad.subModalidad),
                        indicador = Indicador.VIAJES,
                        idPresupuesto = detalleTon.idPresupuesto
                    };
                    StackPanel stpPrincipal = new StackPanel { Orientation = Orientation.Horizontal, Tag = detalle };

                    Label lblModalidad = new Label
                    {
                        Content = detalle.modalidad.modalidad,
                        Width = 100,
                        VerticalContentAlignment = VerticalAlignment.Center,
                        FontWeight = FontWeights.Medium,
                        HorizontalContentAlignment = HorizontalAlignment.Center
                    };

                    Label lblSubModalidad = new Label
                    {
                        Content = detalle.modalidad.descripcion,
                        Width = 100,
                        VerticalContentAlignment = VerticalAlignment.Center,
                        FontWeight = FontWeights.Medium,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        BorderThickness = new Thickness(0, 0, 1, 0),
                        BorderBrush = Brushes.Black
                    };

                    Label ctrDecimalEnero = new Label
                    {
                        Content = detalle.enero.ToString("N0"),
                        Width = 100,
                        Tag = stpPrincipal,
                        Name = "ctrDecimalEnero",
                        HorizontalContentAlignment = HorizontalAlignment.Right,
                        Background = Brushes.Silver
                    };

                    Label ctrDecimalFebrero = new Label
                    {
                        Content = detalle.febrero.ToString("N0"),
                        Width = 100,
                        Tag = stpPrincipal,
                        Name = "ctrDecimalFebrero",
                        HorizontalContentAlignment = HorizontalAlignment.Right,
                        Background = Brushes.Silver
                    };

                    Label ctrDecimalMarzo = new Label
                    {
                        Content = detalle.marzo.ToString("N0"),
                        Width = 100,
                        Tag = stpPrincipal,
                        Name = "ctrDecimalMarzo",
                        HorizontalContentAlignment = HorizontalAlignment.Right,
                        Background = Brushes.Silver
                    };

                    Label ctrDecimalAbril = new Label
                    {
                        Content = detalle.abril.ToString("N0"),
                        Width = 100,
                        Tag = stpPrincipal,
                        Name = "ctrDecimalAbril",
                        HorizontalContentAlignment = HorizontalAlignment.Right,
                        Background = Brushes.Silver
                    };

                    Label ctrDecimalMayo = new Label
                    {
                        Content = detalle.mayo.ToString("N0"),
                        Width = 100,
                        Tag = stpPrincipal,
                        Name = "ctrDecimalMayo",
                        HorizontalContentAlignment = HorizontalAlignment.Right,
                        Background = Brushes.Silver
                    };

                    Label ctrDecimalJunio = new Label
                    {
                        Content = detalle.junio.ToString("N0"),
                        Width = 100,
                        Tag = stpPrincipal,
                        Name = "ctrDecimalJunio",
                        HorizontalContentAlignment = HorizontalAlignment.Right,
                        Background = Brushes.Silver
                    };

                    Label ctrDecimalJulio = new Label
                    {
                        Content = detalle.julio.ToString("N0"),
                        Width = 100,
                        Tag = stpPrincipal,
                        Name = "ctrDecimalJulio",
                        HorizontalContentAlignment = HorizontalAlignment.Right,
                        Background = Brushes.Silver
                    };

                    Label ctrDecimalAgosto = new Label
                    {
                        Content = detalle.agosto.ToString("N0"),
                        Width = 100,
                        Tag = stpPrincipal,
                        Name = "ctrDecimalAgosto",
                        HorizontalContentAlignment = HorizontalAlignment.Right,
                        Background = Brushes.Silver
                    };

                    Label ctrDecimalSeptiembre = new Label
                    {
                        Content = detalle.septiembre.ToString("N0"),
                        Width = 100,
                        Tag = stpPrincipal,
                        Name = "ctrDecimalSeptiembre",
                        HorizontalContentAlignment = HorizontalAlignment.Right,
                        Background = Brushes.Silver
                    };

                    Label ctrDecimalOctubre = new Label
                    {
                        Content = detalle.octubre.ToString("N0"),
                        Width = 100,
                        Tag = stpPrincipal,
                        Name = "ctrDecimalOctubre",
                        HorizontalContentAlignment = HorizontalAlignment.Right,
                        Background = Brushes.Silver
                    };

                    Label ctrDecimalNoviembre = new Label
                    {
                        Content = detalle.noviembre.ToString("N0"),
                        Width = 100,
                        Tag = stpPrincipal,
                        Name = "ctrDecimalNoviembre",
                        HorizontalContentAlignment = HorizontalAlignment.Right,
                        Background = Brushes.Silver
                    };

                    Label ctrDecimalDiciembre = new Label
                    {
                        Content = detalle.diciembre.ToString("N0"),
                        Width = 100,
                        Tag = stpPrincipal,
                        Name = "ctrDecimalDiciembre",
                        HorizontalContentAlignment = HorizontalAlignment.Right,
                        Background = Brushes.Silver
                    };

                    Label lblSumaAnual = new Label
                    {
                        Name = "lblSumaAnual",
                        Content = detalle.sumaAnual.ToString("N0"),
                        Width = 140,
                        VerticalContentAlignment = VerticalAlignment.Center,
                        FontWeight = FontWeights.Bold,
                        HorizontalContentAlignment = HorizontalAlignment.Right,
                        BorderThickness = new Thickness(1, 0, 0, 0),
                        BorderBrush = Brushes.Black
                    };

                    stpPrincipal.Children.Add(lblModalidad);
                    stpPrincipal.Children.Add(lblSubModalidad);

                    stpPrincipal.Children.Add(ctrDecimalEnero);
                    stpPrincipal.Children.Add(ctrDecimalFebrero);
                    stpPrincipal.Children.Add(ctrDecimalMarzo);
                    stpPrincipal.Children.Add(ctrDecimalAbril);
                    stpPrincipal.Children.Add(ctrDecimalMayo);
                    stpPrincipal.Children.Add(ctrDecimalJunio);
                    stpPrincipal.Children.Add(ctrDecimalJulio);
                    stpPrincipal.Children.Add(ctrDecimalAgosto);
                    stpPrincipal.Children.Add(ctrDecimalSeptiembre);
                    stpPrincipal.Children.Add(ctrDecimalOctubre);
                    stpPrincipal.Children.Add(ctrDecimalNoviembre);
                    stpPrincipal.Children.Add(ctrDecimalDiciembre);

                    stpPrincipal.Children.Add(lblSumaAnual);

                    stpViajes.Children.Add(stpPrincipal);
                }
                sumarViajes();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        void sumarViajes()
        {
            List<DetallePresupuesto> listaDetalles = this.listaPreViajes;

            lblViajeEne.Content = listaDetalles.Sum(s => s.enero).ToString("N0");
            lblViajeFeb.Content = listaDetalles.Sum(s => s.febrero).ToString("N0");
            lblViajeMar.Content = listaDetalles.Sum(s => s.marzo).ToString("N0");
            lblViajeAbr.Content = listaDetalles.Sum(s => s.abril).ToString("N0");
            lblViajeMay.Content = listaDetalles.Sum(s => s.mayo).ToString("N0");
            lblViajeJun.Content = listaDetalles.Sum(s => s.junio).ToString("N0");
            lblViajeJul.Content = listaDetalles.Sum(s => s.julio).ToString("N0");
            lblViajeAgo.Content = listaDetalles.Sum(s => s.agosto).ToString("N0");
            lblViajeSep.Content = listaDetalles.Sum(s => s.septiembre).ToString("N0");
            lblViajeOct.Content = listaDetalles.Sum(s => s.octubre).ToString("N0");
            lblViajeNov.Content = listaDetalles.Sum(s => s.noviembre).ToString("N0");
            lblViajeDic.Content = listaDetalles.Sum(s => s.diciembre).ToString("N0");
            lblViajeSum.Content = listaDetalles.Sum(s => s.sumaAnual).ToString("N0");
        }
        void sumarKms()
        {
            List<DetallePresupuesto> listaDetalles = this.listaPreKm;

            lblKmEne.Content = listaDetalles.Sum(s => s.enero).ToString("N0");
            lblKmFeb.Content = listaDetalles.Sum(s => s.febrero).ToString("N0");
            lblKmMar.Content = listaDetalles.Sum(s => s.marzo).ToString("N0");
            lblKmAbril.Content = listaDetalles.Sum(s => s.abril).ToString("N0");
            lblKmMay.Content = listaDetalles.Sum(s => s.mayo).ToString("N0");
            lblKmJunio.Content = listaDetalles.Sum(s => s.junio).ToString("N0");
            lblKmJulio.Content = listaDetalles.Sum(s => s.julio).ToString("N0");
            lblKmAgo.Content = listaDetalles.Sum(s => s.agosto).ToString("N0");
            lblKmSep.Content = listaDetalles.Sum(s => s.septiembre).ToString("N0");
            lblKmOct.Content = listaDetalles.Sum(s => s.octubre).ToString("N0");
            lblKmNov.Content = listaDetalles.Sum(s => s.noviembre).ToString("N0");
            lblKmDic.Content = listaDetalles.Sum(s => s.diciembre).ToString("N0");
            lblKmSuma.Content = listaDetalles.Sum(s => s.sumaAnual).ToString("N0");
        }
        void sumarLitros()
        {
            List<DetallePresupuesto> listaDetalles = this.listaPreLitros;

            lblLitrosEne.Content = listaDetalles.Sum(s => s.enero).ToString("N0");
            lblLitrosFeb.Content = listaDetalles.Sum(s => s.febrero).ToString("N0");
            lblLitrosMar.Content = listaDetalles.Sum(s => s.marzo).ToString("N0");
            lblLitrosAbril.Content = listaDetalles.Sum(s => s.abril).ToString("N0");
            lblLitrosMay.Content = listaDetalles.Sum(s => s.mayo).ToString("N0");
            lblLitrosJunio.Content = listaDetalles.Sum(s => s.junio).ToString("N0");
            lblLitrosJulio.Content = listaDetalles.Sum(s => s.julio).ToString("N0");
            lblLitrosAgo.Content = listaDetalles.Sum(s => s.agosto).ToString("N0");
            lblLitrosSep.Content = listaDetalles.Sum(s => s.septiembre).ToString("N0");
            lblLitrosOct.Content = listaDetalles.Sum(s => s.octubre).ToString("N0");
            lblLitrosNov.Content = listaDetalles.Sum(s => s.noviembre).ToString("N0");
            lblLitrosDic.Content = listaDetalles.Sum(s => s.diciembre).ToString("N0");
            lblLitrosSuma.Content = listaDetalles.Sum(s => s.sumaAnual).ToString("N0");
        }
        void sumarVentas()
        {
            List<DetallePresupuesto> listaDetalles = this.listaPreVentas;

            lblVentasEne.Content = listaDetalles.Sum(s => s.enero).ToString("C0");
            lblVentasFeb.Content = listaDetalles.Sum(s => s.febrero).ToString("C0");
            lblVentasMar.Content = listaDetalles.Sum(s => s.marzo).ToString("C0");
            lblVentasAbril.Content = listaDetalles.Sum(s => s.abril).ToString("C0");
            lblVentasMay.Content = listaDetalles.Sum(s => s.mayo).ToString("C0");
            lblVentasJunio.Content = listaDetalles.Sum(s => s.junio).ToString("C0");
            lblVentasJulio.Content = listaDetalles.Sum(s => s.julio).ToString("C0");
            lblVentasAgo.Content = listaDetalles.Sum(s => s.agosto).ToString("C0");
            lblVentasSep.Content = listaDetalles.Sum(s => s.septiembre).ToString("C0");
            lblVentasOct.Content = listaDetalles.Sum(s => s.octubre).ToString("C0");
            lblVentasNov.Content = listaDetalles.Sum(s => s.noviembre).ToString("C0");
            lblVentasDic.Content = listaDetalles.Sum(s => s.diciembre).ToString("C0");
            lblVentasSuma.Content = listaDetalles.Sum(s => s.sumaAnual).ToString("C0");
        }

        private void cbxAño_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxAño.SelectedItem != null)
            {
                int año = (int)cbxAño.SelectedItem;
                lblDiasOperativosEne.Content = DateTime.DaysInMonth(año, 1);
                lblDiasOperativosFeb.Content = DateTime.DaysInMonth(año, 2);
                lblDiasOperativosMar.Content = DateTime.DaysInMonth(año, 3);
                lblDiasOperativosAbril.Content = DateTime.DaysInMonth(año, 4);
                lblDiasOperativosMay.Content = DateTime.DaysInMonth(año, 5);
                lblDiasOperativosJunio.Content = DateTime.DaysInMonth(año, 6);
                lblDiasOperativosJulio.Content = DateTime.DaysInMonth(año, 7);
                lblDiasOperativosAgo.Content = DateTime.DaysInMonth(año, 8);
                lblDiasOperativosSep.Content = DateTime.DaysInMonth(año, 9);
                lblDiasOperativosOct.Content = DateTime.DaysInMonth(año, 10);
                lblDiasOperativosNov.Content = DateTime.DaysInMonth(año, 11);
                lblDiasOperativosDic.Content = DateTime.DaysInMonth(año, 12);

                lblDiasOperativosSuma.Content = new DateTime(año, 12, 31).DayOfYear;
                    //((int)lblSumaDiasOperativosEne.Content + (int)lblSumaDiasOperativosFeb.Content + (int)lblSumaDiasOperativosMar.Content + (int)lblSumaDiasOperativosAbril.Content + (int)lblSumaDiasOperativosMay.Content + (int)lblSumaDiasOperativosJunio.Content
                    // + (int)lblSumaDiasOperativosJulio.Content + (int)lblSumaDiasOperativosAgo.Content + (int)lblSumaDiasOperativosSep.Content + (int)lblSumaDiasOperativosOct.Content + (int)lblSumaDiasOperativosNov.Content + (int)lblSumaDiasOperativosDic.Content);

                llenarHeader();
            }
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                Presupuesto presupuesto = mapForm();
                var resp = new PresupuestoSvc().savePresupuesto(ref presupuesto);
                if (resp.typeResult == ResultTypes.success)
                {
                    buscarPresupuestoByClave(presupuesto.idPresupuesto);
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.editar | ToolbarCommands.cerrar);
                }
                return (resp);
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapForm(Presupuesto presupuesto)
        {
            stpToneladas.Children.Clear();
            stpViajes.Children.Clear();
            stpKm.Children.Clear();
            stpLitros.Children.Clear();
            stpVentas.Children.Clear();
            setDiasNoOperativos(null);
            sumarToneladas();
            sumarViajes();
            sumarKms();
            sumarLitros();
            sumarVentas();
            if (presupuesto != null)
            {
                ctrEmpresas.empresaSelected = presupuesto.empresa;
                ctrSucusal.zonaOperativa = presupuesto.sucursal;
                cbxClientes.SelectedItem = this.cbxClientes.ItemsSource.Cast<Cliente>().ToList<Cliente>().Find(s => s.clave == presupuesto.cliente.clave);
                cbxAño.SelectedItem = cbxAño.Items.Cast<int>().ToList().Find(s => s == presupuesto.anio);
                ctrFolio.valor = presupuesto.idPresupuesto;
                ctrFolio.Tag = presupuesto;

                stpControles.IsEnabled = false;

                cbxAño.SelectedItem = (cbxAño.Items.Cast<int>().ToList().Find(s => s == presupuesto.anio));

                foreach (var detalle in presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.TONELADAS || s.indicador == Indicador.VENTAS))
                {
                    addDetallePresupuestoEntero(detalle);
                }
                actualizarViajes();
                foreach (var detalle in presupuesto.listaDetalles.FindAll(s => s.indicador == Indicador.KILOMETROS || s.indicador == Indicador.LITROS))
                {
                    addDetallePresupuestoEntero(detalle);
                }

                setDiasNoOperativos(presupuesto.listaDetalles.Find(s => s.indicador == Indicador.DIAS_NO_OPERATIVOS));
                sumarToneladas();
                sumarViajes();
                sumarKms();
                sumarLitros();
                sumarVentas();
            }
            else
            {
                ctrFolio.valor = 0;
                ctrFolio.Tag = null;
                stpControles.IsEnabled = true;
            }
        }

        private Presupuesto mapForm()
        {
            try
            {
                Presupuesto presupuesto = new Presupuesto();
                presupuesto.idPresupuesto = ctrFolio.Tag == null ? 0 : (ctrFolio.Tag as Presupuesto).idPresupuesto;
                presupuesto.empresa = ctrEmpresas.empresaSelected;
                presupuesto.sucursal = ctrSucusal.zonaOperativa;
                presupuesto.cliente = cbxClientes.SelectedItem as Cliente;
                presupuesto.anio = (int)cbxAño.SelectedItem;
                presupuesto.usuario = mainWindow.usuario.nombreUsuario;
                presupuesto.usuarioBaja = null;
                presupuesto.fechaBaja = null;
                presupuesto.fechaRegistro = DateTime.Now;
                presupuesto.listaDetalles = new List<DetallePresupuesto>();
                presupuesto.estatus = true;

                presupuesto.listaDetalles.AddRange(listaPreToneladas);
                presupuesto.listaDetalles.AddRange(listaPreViajes);
                presupuesto.listaDetalles.AddRange(listaPreKm);
                presupuesto.listaDetalles.AddRange(listaPreLitros);
                presupuesto.listaDetalles.AddRange(listaPreVentas);
                List<DetallePresupuesto> listaRendimiento = getListaRendimiento();
                presupuesto.listaDetalles.AddRange(listaRendimiento);
                List<DetallePresupuesto> listaPrecioKm = getListaPrecioKm();
                presupuesto.listaDetalles.AddRange(listaPrecioKm);
                presupuesto.listaDetalles.Add(getDiasNoOperativos());

                return presupuesto;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private List<DetallePresupuesto> getListaRendimiento()
        {
            List<DetallePresupuesto> listaRendimiento = new List<DetallePresupuesto>();
            foreach (var item in listaPreLitros)
            {
                DetallePresupuesto detKm = listaPreKm.Find(s => s.modalidad.subModalidad == item.modalidad.subModalidad);
                DetallePresupuesto detalle = new DetallePresupuesto
                {
                    idDetallePresupuesto = 0,
                    indicador = Indicador.RENDIMIENTO,
                    modalidad = item.modalidad,
                    enero = item.enero <= 0 ? 0 : (detKm.enero/item.enero),
                    febrero = item.febrero <= 0 ? 0 : (detKm.febrero / item.febrero),
                    marzo = item.marzo <= 0 ? 0 : (detKm.marzo / item.marzo),
                    abril = item.abril <= 0 ? 0 : (detKm.abril / item.abril),
                    mayo = item.mayo <= 0 ? 0 : (detKm.mayo / item.mayo),
                    junio = item.junio <= 0 ? 0 : (detKm.junio / item.junio),
                    julio = item.julio <= 0 ? 0 : (detKm.julio / item.julio),
                    agosto = item.agosto <= 0 ? 0 : (detKm.agosto / item.agosto),
                    septiembre = item.septiembre <= 0 ? 0 : (detKm.septiembre / item.septiembre),
                    octubre = item.octubre <= 0 ? 0 : (detKm.octubre / item.octubre),
                    noviembre = item.noviembre <= 0 ? 0 : (detKm.noviembre / item.noviembre),
                    diciembre = item.diciembre <= 0 ? 0 : (detKm.diciembre / item.diciembre),
                    idPresupuesto = ctrFolio.Tag == null ? 0 : (ctrFolio.Tag as Presupuesto).idPresupuesto
                };
                listaRendimiento.Add(detalle);
            }
            return listaRendimiento;
        }
        private List<DetallePresupuesto> getListaPrecioKm()
        {
            List<DetallePresupuesto> listaRendimiento = new List<DetallePresupuesto>();
            foreach (var itemKm  in listaPreKm)
            {
                DetallePresupuesto detVenta = listaPreVentas.Find(s => s.modalidad.subModalidad == itemKm.modalidad.subModalidad);
                DetallePresupuesto detalle = new DetallePresupuesto
                {
                    idDetallePresupuesto = 0,
                    indicador = Indicador.PRECIO_KM,
                    modalidad = itemKm.modalidad,
                    enero = itemKm.enero <= 0 ? 0 : (detVenta.enero / itemKm.enero),
                    febrero = itemKm.febrero <= 0 ? 0 : (detVenta.febrero / itemKm.febrero),
                    marzo = itemKm.marzo <= 0 ? 0 : (detVenta.marzo / itemKm.marzo),
                    abril = itemKm.abril <= 0 ? 0 : (detVenta.abril / itemKm.abril),
                    mayo = itemKm.mayo <= 0 ? 0 : (detVenta.mayo / itemKm.mayo),
                    junio = itemKm.junio <= 0 ? 0 : (detVenta.junio / itemKm.junio),
                    julio = itemKm.julio <= 0 ? 0 : (detVenta.julio / itemKm.julio),
                    agosto = itemKm.agosto <= 0 ? 0 : (detVenta.agosto / itemKm.agosto),
                    septiembre = itemKm.septiembre <= 0 ? 0 : (detVenta.septiembre / itemKm.septiembre),
                    octubre = itemKm.octubre <= 0 ? 0 : (detVenta.octubre / itemKm.octubre),
                    noviembre = itemKm.noviembre <= 0 ? 0 : (detVenta.noviembre / itemKm.noviembre),
                    diciembre = itemKm.diciembre <= 0 ? 0 : (detVenta.diciembre / itemKm.diciembre),
                    idPresupuesto = ctrFolio.Tag == null ? 0 : (ctrFolio.Tag as Presupuesto).idPresupuesto
                };
                listaRendimiento.Add(detalle);
            }
            return listaRendimiento;
        }

        public override void buscar()
        {
            try
            {
                Presupuesto presupuesto = new selectPresupuestos().buscarPresupuesto();
                if (presupuesto != null)
                {
                    var resp = new PresupuestoSvc().getDetallePresupuestosByClave(presupuesto.idPresupuesto);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        presupuesto.listaDetalles = resp.result as List<DetallePresupuesto>;
                        mapForm(presupuesto);
                        base.buscar();
                        mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.editar | ToolbarCommands.cerrar);
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        DetallePresupuesto getDiasNoOperativos()
        {
            return new DetallePresupuesto
            {
                indicador = Indicador.DIAS_NO_OPERATIVOS,
                enero = ctrDiasNoOperEne.valor,
                febrero = ctrDiasNoOperFeb.valor,
                marzo = ctrDiasNoOperMar.valor,
                abril = ctrDiasNoOperAbr.valor,
                mayo = ctrDiasNoOperMay.valor,
                junio = ctrDiasNoOperJun.valor,
                julio = ctrDiasNoOperJul.valor,
                agosto = ctrDiasNoOperAgo.valor,
                septiembre = ctrDiasNoOperSep.valor,
                octubre = ctrDiasNoOperOct.valor,
                noviembre = ctrDiasNoOperNov.valor,
                diciembre = ctrDiasNoOperDic.valor,
                modalidad = null
            };
        }

        void setDiasNoOperativos(DetallePresupuesto detalle)
        {
            if (detalle != null)
            {
                ctrDiasNoOperEne.valor = Convert.ToInt32(detalle.enero);
                ctrDiasNoOperFeb.valor = Convert.ToInt32(detalle.febrero);
                ctrDiasNoOperMar.valor = Convert.ToInt32(detalle.marzo);
                ctrDiasNoOperAbr.valor = Convert.ToInt32(detalle.abril);
                ctrDiasNoOperMay.valor = Convert.ToInt32(detalle.mayo);
                ctrDiasNoOperJun.valor = Convert.ToInt32(detalle.junio);
                ctrDiasNoOperJul.valor = Convert.ToInt32(detalle.julio);
                ctrDiasNoOperAgo.valor = Convert.ToInt32(detalle.agosto);
                ctrDiasNoOperSep.valor = Convert.ToInt32(detalle.septiembre);
                ctrDiasNoOperOct.valor = Convert.ToInt32(detalle.octubre);
                ctrDiasNoOperNov.valor = Convert.ToInt32(detalle.noviembre);
                ctrDiasNoOperDic.valor = Convert.ToInt32(detalle.diciembre);
            }
            else
            {
                ctrDiasNoOperEne.valor = 0;
                ctrDiasNoOperFeb.valor = 0;
                ctrDiasNoOperMar.valor = 0;
                ctrDiasNoOperAbr.valor = 0;
                ctrDiasNoOperMay.valor = 0;
                ctrDiasNoOperJun.valor = 0;
                ctrDiasNoOperJul.valor = 0;
                ctrDiasNoOperAgo.valor = 0;
                ctrDiasNoOperSep.valor = 0;
                ctrDiasNoOperOct.valor = 0;
                ctrDiasNoOperNov.valor = 0;
                ctrDiasNoOperDic.valor = 0;
            }

        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (cbxModalidad.SelectedItem == null) return;

            Modalidades modalidad = cbxModalidad.SelectedItem as Modalidades;
            DetallePresupuesto detalle = new DetallePresupuesto();

            StackPanel stpTon = new StackPanel();
            foreach (var stp in stpToneladas.Children.Cast<StackPanel>().ToList())
            {
                if ((stp.Tag as DetallePresupuesto).modalidad.idModalidad == modalidad.idModalidad)
                {
                    detalle = (stp.Tag as DetallePresupuesto);
                    stpTon = stp;
                    break;
                }
            }

            StackPanel stpK = new StackPanel();
            foreach (var stp in stpKm.Children.Cast<StackPanel>().ToList())
            {
                if ((stp.Tag as DetallePresupuesto).modalidad.idModalidad == modalidad.idModalidad)
                {
                    stpK = stp;
                    break;
                }
            }

            StackPanel stpL = new StackPanel();
            foreach (var stp in stpLitros.Children.Cast<StackPanel>().ToList())
            {
                if ((stp.Tag as DetallePresupuesto).modalidad.idModalidad == modalidad.idModalidad)
                {
                    stpL = stp;
                    break;
                }
            }

            StackPanel stpV = new StackPanel();
            foreach (var stp in stpKm.Children.Cast<StackPanel>().ToList())
            {
                if ((stp.Tag as DetallePresupuesto).modalidad.idModalidad == modalidad.idModalidad)
                {
                    stpV = stp;
                    break;
                }
            }

            if (ctrFolio.Tag != null && detalle.idDetallePresupuesto > 0)
            {
                string texto = new ventanaInputTexto("CANCELAR MODALIDAD PRESUPUESTO", "Proporcionar el motivo de cancelación").obtenerRespuesta();
                if (!string.IsNullOrEmpty(texto))
                {
                    var resp = new PresupuestoSvc().bajarDetallePresupuesto((ctrFolio.Tag as Presupuesto).idPresupuesto, detalle.modalidad.idModalidad,
                    mainWindow.usuario.nombreUsuario, texto);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        stpToneladas.Children.Remove(stpTon);
                        actualizarViajes();
                        stpKm.Children.Remove(stpK);
                        stpLitros.Children.Remove(stpL);
                        stpVentas.Children.Remove(stpV);
                    }
                }
            }
            else
            {
                stpToneladas.Children.Remove(stpTon);
                actualizarViajes();
                stpKm.Children.Remove(stpK);
                stpLitros.Children.Remove(stpL);
                stpVentas.Children.Remove(stpV);
            }
        }
    }
}
