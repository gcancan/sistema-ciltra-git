﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for detallesCargaCombustible.xaml
    /// </summary>
    public partial class detallesCargaCombustible : Window
    {
        public detallesCargaCombustible()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new OperacionSvc().getSalidasEntradasByIdOrdenServ(idOrdenServ);
                if (resp.typeResult == ResultTypes.success)
                {
                    CargaCombustible carga = (CargaCombustible)resp.result;
                    mapForm(carga);
                    if (!carga.isExterno)
                    {
                        if (((CargaCombustible)resp.result).fechaCombustible != null)
                        {

                        }
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
                headerViaje.IsEnabled = false;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private int idOrdenServ = 0;
        public void mapearPantalla(int idOrdenServ)
        {
            try
            {
               
                this.idOrdenServ = idOrdenServ;
                ShowDialog();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            
        }
        bool llenarLista = false;
        UnidadTransporte unidad = new UnidadTransporte();
        private void mapForm(CargaCombustible result)
        {
            if (result != null)
            {
                txtFolio.Text = result.idOrdenServ.ToString();
                txtFolio.Tag = result;
                txtFolio.IsEnabled = false;
                mapOperador(result.masterOrdServ.chofer);

                foreach (var item in result.masterOrdServ.listTiposOrdenServicio)
                {
                    if (item.idOrdenSer == result.idOrdenServ)
                    {
                        unidad = item.unidadTrans;
                    }
                }
                if (unidad.tipoUnidad.descripcion.Contains("TRACTO"))
                {
                    var resp = new CargaCombustibleSvc().findUltimaCargaByIdUnidad(unidad.clave);
                    if (resp.typeResult == ResultTypes.recordNotFound)
                    {
                        if (result.fechaCombustible == null)
                        {
                            cbxCP.IsEnabled = true;
                            llenarLista = true;
                        }
                        else
                        {
                            //cbxCP.IsEnabled = false;
                            llenarLista = false;
                        }
                    }
                }
                else
                {
                    //cbxCP.IsEnabled = false;
                    llenarLista = false;
                }

                mapList(result.listCP);
                abilitarControles(unidad.tipoUnidad.clasificacion.ToUpper() == "TRACTOR");
                mapUnidades(TipoUnidad.TRACTOR, unidad);

                txtCombustibleUtilizado.Text = result.ltCombustibleUtilizado <= 0 ? "" : result.ltCombustibleUtilizado.ToString("N1");
                txtCombustiblePTO.Text = result.ltCombustiblePTO <= 0 ? "" : result.ltCombustiblePTO.ToString("N1");
                txtCombustibleEST.Text = result.ltCombustibleEST <= 0 ? "" : result.ltCombustibleEST.ToString("N1");
                txtCombustibleCargados.Text = result.ltCombustible <= 0 ? "" : result.ltCombustible.ToString("N1");

                txtTiempoTotal.Text = result.tiempoTotal;
                txtTiempoPTO.Text = result.tiempoPTO;
                txtTiempoEst.Text = result.tiempoEST;

                txtKmRecorridos.Text = result.kmRecorridos <= 0 ? "" : result.kmRecorridos.ToString("N1");
                txtKm.Text = result.kmDespachador <= 0 ? "" : result.kmDespachador.ToString("N1");

                mapDespachador(result.despachador);
                txtTanqueIzq.Text = result.sello1 <= 0 ? "" : result.sello1.ToString();
                txtTanqueDer.Text = result.sello2 <= 0 ? "" : result.sello2.ToString();
                txtTanqueRes.Text = result.selloReserva <= 0 ? "" : result.selloReserva.ToString();

                txtFechaEntrada.Text = result.fechaEntrada.ToString();
                txtFecha.Text = result.masterOrdServ.fechaCreacion.ToString();
                txtFechaGasolina.Text = result.fechaCombustible.ToString();
                txtDespachador.Focus();
                txtLtsExtra.Text = result.ltsExtra.ToString("N1");
                txtRelleno.Text = (result.ltsSumaTotales - result.ltCombustibleUtilizado).ToString("N1");
            }
            else
            {
                txtFolio.Clear();
                txtFolio.IsEnabled = true;
                mapOperador(null);
                mapUnidades(TipoUnidad.TRACTOR, null);
                unidad = null;
                txtCombustibleUtilizado.Clear();
                txtCombustiblePTO.Clear();
                txtCombustibleEST.Clear();
                txtCombustibleCargados.Clear();
                abilitarControles(true);
                txtTiempoTotal.Clear();
                txtTiempoPTO.Clear();
                txtTiempoEst.Clear();

                txtKmRecorridos.Clear();
                txtKm.Clear();
                mapDespachador(null);
                txtTanqueIzq.Clear();
                txtTanqueDer.Clear();
                txtTanqueRes.Clear();
                txtFechaEntrada.Clear();
                txtFecha.Clear();
                txtFolio.Clear();
                txtFolio.Focus();
                txtFechaGasolina.Clear();
                //cbxCP.IsEnabled = false;
                lvlCP.Items.Clear();
                llenarLista = false;
                txtLtsExtra.Clear();
                //lvlValesExtra.Items.Clear();
            }
        }
        private void mapDespachador(Personal personal)
        {
            if (personal != null)
            {
                txtDespachador.Tag = personal;
                txtDespachador.Text = personal.nombre;
                txtDespachador.IsEnabled = false;

                if (unidad.tipoUnidad.clasificacion.ToUpper() == "TRACTOR")
                {
                    txtKm.Focus();
                }
                else
                {
                    txtCombustibleCargados.Focus();
                }
            }
            else
            {
                txtDespachador.Tag = null;
                txtDespachador.Text = "";
                txtDespachador.IsEnabled = true;
            }
        }
        enum TipoUnidad
        {
            TRACTOR = 0,
            TOLVA = 1,
            DOLLY = 2,
            TOLVA_2 = 3
        }
        private void mapUnidades(TipoUnidad tipo, UnidadTransporte Unidad)
        {
            switch (tipo)
            {
                case TipoUnidad.TRACTOR:
                    if (Unidad != null)
                    {
                        txtTractor.Tag = Unidad;
                        txtTractor.Text = Unidad.clave;
                        txtTractor.IsEnabled = false;
                        txtOperador.Focus();
                    }
                    else
                    {
                        txtTractor.Tag = null;
                        txtTractor.Clear();
                        txtTractor.IsEnabled = true;
                    }
                    break;
                //case TipoUnidad.TOLVA:
                //    if (Unidad != null)
                //    {
                //        txtTolva1.Tag = Unidad;
                //        txtTolva1.Text = Unidad.clave;
                //    }
                //    else
                //    {
                //        txtTolva1.Tag = null;
                //        txtTolva1.Clear();
                //    }
                //    break;
                //case TipoUnidad.DOLLY:
                //    if (Unidad != null)
                //    {
                //        txtDolly.Tag = Unidad;
                //        txtDolly.Text = Unidad.clave;
                //    }
                //    else
                //    {
                //        txtDolly.Tag = null;
                //        txtDolly.Clear();
                //    }
                //    break;
                //case TipoUnidad.TOLVA_2:
                //    if (Unidad != null)
                //    {
                //        txtTolva2.Tag = Unidad;
                //        txtTolva2.Text = Unidad.clave;
                //    }
                //    else
                //    {
                //        txtTolva2.Tag = null;
                //        txtTolva2.Clear();
                //    }
                //    break;
                default:
                    break;
            }
        }
        private void abilitarControles(bool v)
        {
            //cbxLts.IsEnabled = v;
            cbxSellos.IsEnabled = v;
            cbxTiempos.IsEnabled = v;
            cbxKMs.IsEnabled = v;
        }
        private void mapList(List<CartaPorte> listCP)
        {
            foreach (var item in listCP)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvlCP.Items.Add(lvl);
            }
        }

        private void mapOperador(Personal operador)
        {
            if (operador != null)
            {
                txtOperador.Tag = operador;
                txtOperador.Text = operador.nombre;
                txtOperador.IsEnabled = false;
            }
            else
            {
                txtOperador.Tag = null;
                txtOperador.Text = "";
                txtOperador.IsEnabled = true;
            }
        }

        private void txtDespachador_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtKm_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtKmRecorridos_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtCombustibleUtilizado_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtCombustiblePTO_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtCombustibleEST_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtCombustibleCargados_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtTiempoTotal_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtTiempoPTO_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtTiempoEst_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtTanqueIzq_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void txtTanqueDer_KeyDown(object sender, KeyEventArgs e)
        {

        }
    }
}
