﻿using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoreFletera.Interfaces;
using Core.Models;
using Core.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for editRelacionTiposOrdenServ.xaml
    /// </summary>
    public partial class editRelacionTiposOrdenServ : ViewBase
    {
        public editRelacionTiposOrdenServ()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                var respSvc = new RelacionTiposOrdenServSvc().getTiposOrdenes();
                if (respSvc.typeResult == ResultTypes.success)
                {
                    cbxTipoOrden.ItemsSource = respSvc.result as List<TipoOrdenServClass>;
                }

                cbxCatalogo.ItemsSource = Enum.GetValues(typeof(CatTipoOrdenServ));
                
                nuevo();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                this.IsEnabled = false;
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }            
        }

        public override void nuevo()
        {
            base.nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
        }

        private void cbxCatalogo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxCatalogo.SelectedItem == null)
            {
                return;
            }
            cbxRelacion.ItemsSource = null;
            switch ((CatTipoOrdenServ)cbxCatalogo.SelectedItem)
            {
                case CatTipoOrdenServ.ACTIVIDADES:
                    buscarActividades();
                    break;
                case CatTipoOrdenServ.SERVICIOS:
                    buscarServicios();
                    break;
                case CatTipoOrdenServ.TIPO_SERVICIOS:
                    buscarTiposOrden();
                    break;
                default:
                    break;
            }
            buscarRelaciones();
        }

        private void buscarActividades()
        {
            try
            {               
                Cursor = Cursors.Arrow;
                cbxRelacion.DisplayMemberPath = "nombre";
                var resp = new RelacionTiposOrdenServSvc().getAllActividades();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxRelacion.ItemsSource = resp.result as List<Actividad>;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void buscarServicios()
        {
            try
            {
                Cursor = Cursors.Arrow;
                cbxRelacion.DisplayMemberPath = "nombre";
                var resp = new RelacionTiposOrdenServSvc().getAllServicios();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxRelacion.ItemsSource = resp.result as List<Servicio>;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void buscarTiposOrden()
        {
            try
            {
                Cursor = Cursors.Arrow;
                cbxRelacion.DisplayMemberPath = "nombreTipoOrden";
                var resp = new RelacionTiposOrdenServSvc().getAllTipoDeOrden();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxRelacion.ItemsSource = resp.result as List<TipoOrden>;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void cbxTipoOrden_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            buscarRelaciones();
        }

        private void buscarRelaciones()
        {
            if (cbxCatalogo.SelectedItem != null && cbxTipoOrden.SelectedItem != null)
            {
                lvlDetalles.Items.Clear();
                switch ((CatTipoOrdenServ)cbxCatalogo.SelectedItem)
                {
                    case CatTipoOrdenServ.ACTIVIDADES:
                        buscarRelacionActividades();
                        break;
                    case CatTipoOrdenServ.SERVICIOS:
                        buscarRelacionServicios();
                        break;
                    case CatTipoOrdenServ.TIPO_SERVICIOS:
                        buscarRelacionTiposOrden();
                        break;
                    default:
                        break;
                }
            }
        }

        private void buscarRelacionActividades()
        {
            var resp = new OrdenesTrabajoSvc().getRelacionActividad(((TipoOrdenServClass)cbxTipoOrden.SelectedItem).idTipoOrdServ);
            if (resp.typeResult == ResultTypes.success)
            {
                List<Actividad> lista = (List<Actividad>)resp.result;
                foreach (var item in lista)
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = item;
                    ContextMenu menu = new ContextMenu();
                    MenuItem itemMenu = new MenuItem { Header = "BORRAR" };                    
                    itemMenu.Click += ItemMenu_Click;
                    menu.Items.Add(itemMenu);
                    lvl.ContextMenu = menu;
                    lvlDetalles.Items.Add(lvl);
                }
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
            }
        }

        private void ItemMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TipoOrdenServClass tipoOrden = (TipoOrdenServClass)cbxTipoOrden.SelectedItem;
                CatTipoOrdenServ catalogo = (CatTipoOrdenServ)cbxCatalogo.SelectedItem;
                int idRelacion = 0;
                switch (catalogo)
                {
                    case CatTipoOrdenServ.ACTIVIDADES:
                        idRelacion = ((Actividad)((ListViewItem)lvlDetalles.SelectedItem).Content).idActividad;
                        break;
                    case CatTipoOrdenServ.SERVICIOS:
                        idRelacion = ((Servicio)((ListViewItem)lvlDetalles.SelectedItem).Content).idServicio;
                        break;
                    case CatTipoOrdenServ.TIPO_SERVICIOS:
                        idRelacion = ((TipoOrden)((ListViewItem)lvlDetalles.SelectedItem).Content).idTipoOrden;
                        break;
                    default:
                        idRelacion = 0;
                        break;
                }

                var resp = new RelacionTiposOrdenServSvc().deleteRelacioOrdenServ(tipoOrden, catalogo, idRelacion);
                if (resp.typeResult == ResultTypes.success)
                {
                    mainWindow.habilitar = true;
                    buscarRelaciones();
                }
                ImprimirMensaje.imprimir( resp);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void buscarRelacionServicios()
        {
            var resp = new OrdenesTrabajoSvc().getRelacionServicios(((TipoOrdenServClass)cbxTipoOrden.SelectedItem).idTipoOrdServ);
            if (resp.typeResult == ResultTypes.success)
            {
                List<Servicio> lista = (List<Servicio>)resp.result;
                foreach (var item in lista)
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = item;
                    ContextMenu menu = new ContextMenu();
                    MenuItem itemMenu = new MenuItem { Header = "BORRAR" };
                    itemMenu.Click += ItemMenu_Click;
                    menu.Items.Add(itemMenu);
                    lvl.ContextMenu = menu;
                    lvlDetalles.Items.Add(lvl);
                }
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
            }
        }

        private void buscarRelacionTiposOrden()
        {
            var resp = new OrdenesTrabajoSvc().getTipoDeOrden(((TipoOrdenServClass)cbxTipoOrden.SelectedItem).idTipoOrdServ);
            if (resp.typeResult == ResultTypes.success)
            {
                List<TipoOrden> lista = (List<TipoOrden>)resp.result;
                foreach (var item in lista)
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = item;
                    ContextMenu menu = new ContextMenu();
                    MenuItem itemMenu = new MenuItem { Header = "BORRAR" };                    
                    itemMenu.Click += ItemMenu_Click;
                    menu.Items.Add(itemMenu);
                    lvl.ContextMenu = menu;
                    lvlDetalles.Items.Add(lvl);
                }                
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
            }
        }

        public override OperationResult guardar()
        {
            try
            {
                TipoOrdenServClass tipoOrden = (TipoOrdenServClass)cbxTipoOrden.SelectedItem;
                CatTipoOrdenServ catalogo = (CatTipoOrdenServ)cbxCatalogo.SelectedItem;
                int idRelacion = 0;
                switch (catalogo)
                {
                    case CatTipoOrdenServ.ACTIVIDADES:
                        idRelacion = ((Actividad)cbxRelacion.SelectedItem).idActividad;
                        break;
                    case CatTipoOrdenServ.SERVICIOS:
                        idRelacion = ((Servicio)cbxRelacion.SelectedItem).idServicio;
                        break;
                    case CatTipoOrdenServ.TIPO_SERVICIOS:
                        idRelacion = ((TipoOrden)cbxRelacion.SelectedItem).idTipoOrden;
                        break;
                    default:
                        idRelacion = 0;
                        break;
                }

                var resp = new RelacionTiposOrdenServSvc().saveRelacioOrdenServ(tipoOrden, catalogo, idRelacion);
                if (resp.typeResult == ResultTypes.success)
                {
                    mainWindow.habilitar = true;
                    buscarRelaciones();
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
