﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using f = System.Windows.Forms;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for ConciliadorValesCombustible.xaml
    /// </summary>
    public partial class ConciliadorValesCombustible : ViewBase
    {
        public ConciliadorValesCombustible()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ctrDecimal.txtDecimal.KeyDown += TxtDecimal_KeyDown;
            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresa.loaded(mainWindow.empresa, true);

            cbxMes.ItemsSource = Enum.GetValues(typeof(Mes));

            for (int i = 2012; i < (DateTime.Now.Year + 1); i++)
            {
                cbxAño.Items.Add(i);
            }
            cbxAño.SelectedIndex = cbxAño.Items.Count - 1;           

            nuevo();
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            txtTractor.loaded(etipoUniadBusqueda.TRACTOR, ctrEmpresa.empresaSelected);
            txtTractor.txtUnidadTrans.Focus();
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            OperationResult resp = new CompacionValesCombustibleScv().getListaValesLis(
                txtTractor.unidadTransporte.clave, cbxMes.SelectedItem.ToString(), (int)cbxAño.SelectedItem);
            OperationResult respNew = new CompacionValesCombustibleScv().getListaVales(
                txtTractor.unidadTransporte.clave, (int)cbxMes.SelectedItem, (int)cbxAño.SelectedItem);

            if (resp.typeResult == ResultTypes.error)
            {
                ImprimirMensaje.imprimir(resp);
                return;
            }
            if (respNew.typeResult == ResultTypes.error)
            {
                ImprimirMensaje.imprimir(respNew);
                return;
            }

            llenarListas(resp.result as List<ComparacionValesCombustible>, respNew.result as List<ComparacionValesCombustible>);
        }

        public override void nuevo()
        {
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
            lvlNew.Items.Clear();
            lvlLiss.Items.Clear();
            txtRegistrosNew.Clear();
            txtConcidenciasNew.Clear();
            txtNoConcidenciasNew.Clear();
            txtLitrosNew.Clear();
            txtRegistrosLis.Clear();
            txtConcidenciasLis.Clear();
            txtNoConcidenciasLis.Clear();
            txtLitrosLis.Clear();
            txtDiferencia.Clear();

            txtTractor.limpiar();
        }

        private void llenarListas(List<ComparacionValesCombustible> ListaLis, List<ComparacionValesCombustible> listSistema)
        {
            lvlNew.Items.Clear();
            lvlLiss.Items.Clear();
            txtRegistrosNew.Clear();
            txtConcidenciasNew.Clear();
            txtNoConcidenciasNew.Clear();
            txtLitrosNew.Clear();
            txtRegistrosLis.Clear();
            txtConcidenciasLis.Clear();
            txtNoConcidenciasLis.Clear();
            txtLitrosLis.Clear();
            txtDiferencia.Clear();

            foreach (var itemSistema in listSistema)
            {
                bool encontro = false;
                foreach (var itemLis in ListaLis)
                {
                    if (itemLis.mach)
                    {
                        continue;
                    }
                    if (itemSistema.carga == itemLis.carga)
                    {
                        itemLis.mach = true;
                        itemLis.valeMach = itemSistema.vale;
                        encontro = true;
                        break;
                    }
                }
                ListViewItem lvl = new ListViewItem();
                lvl.Content = itemSistema;
                if (encontro)
                {
                    lvl.Background = Brushes.SteelBlue;
                }
                else
                {
                    lvl.Background = Brushes.Orange;
                }
                lvlNew.Items.Add(lvl);
            }

            foreach (var itemLis in ListaLis)
            {
                bool encontro = false;
                foreach (var itemSistema in listSistema)
                {
                    if (itemSistema.mach)
                    {
                        continue;
                    }
                    if (itemSistema.carga == itemLis.carga)
                    {
                        itemSistema.mach = true;
                        itemSistema.valeMach = itemLis.vale;
                        encontro = true;
                        break;
                    }
                }
                ListViewItem lvl = new ListViewItem();
                lvl.Content = itemLis;
                if (encontro)
                {
                    lvl.Background = Brushes.SteelBlue;
                }
                else
                {
                    lvl.Background = Brushes.Orange;
                }
                lvlLiss.Items.Add(lvl);
            }

            int registros = 0, coincidencias = 0, noCoincidencias = 0;
            decimal ltsTotalesNew = 0m;
            foreach (var item in listSistema)
            {
                registros++;
                if (item.mach)
                {
                    coincidencias++;
                }
                else
                {
                    noCoincidencias++;
                }
                ltsTotalesNew += item.carga;
            }
            txtRegistrosNew.Text = registros.ToString();
            txtConcidenciasNew.Text = coincidencias.ToString();
            txtNoConcidenciasNew.Text = noCoincidencias.ToString();
            txtLitrosNew.Text = ltsTotalesNew.ToString();

            registros = 0; coincidencias = 0; noCoincidencias = 0;
            decimal ltsTotalesLis = 0m;
            foreach (var item in ListaLis)
            {
                registros++;
                if (item.mach)
                {
                    coincidencias++;
                }
                else
                {
                    noCoincidencias++;
                }
                ltsTotalesLis += item.carga;
            }
            txtRegistrosLis.Text = registros.ToString();
            txtConcidenciasLis.Text = coincidencias.ToString();
            txtNoConcidenciasLis.Text = noCoincidencias.ToString();
            txtLitrosLis.Text = ltsTotalesLis.ToString();

            txtDiferencia.Text = Math.Abs(ltsTotalesLis - ltsTotalesNew).ToString();
        }

        private void txtVale_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrEmpty(txtVale.Text.Trim()))
                {
                    ctrDecimal.txtDecimal.Focus();
                }
            }
        }

        private void TxtDecimal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (ctrDecimal.valor>0)
                {
                    ctrDecimal.txtDecimal.Focus();
                    if (!string.IsNullOrEmpty(txtVale.Text.Trim()))
                    {
                        agregarLista();
                    }
                }
            }
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            if (ctrDecimal.valor > 0 && !string.IsNullOrEmpty(txtVale.Text.Trim()))
            {
                agregarLista();
            }
        }

        private void agregarLista()
        {
            if (txtTractor.unidadTransporte == null)
            {
                ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "Se requiere una unidad" });
                return;
            }

            if (cbxMes.SelectedItem == null)
            {
                ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "Se requiere elegir un mes" });
                return;
            }

            if (cbxAño.SelectedItem == null)
            {
                ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "Se requiere el año" });
                return;
            }

            ComparacionValesCombustible comparacion = new ComparacionValesCombustible
            {
                id = 0,
                mes = cbxMes.SelectedItem.ToString(),
                año = (int)cbxAño.SelectedItem,
                vale = txtVale.Text,
                unidadTransporte = txtTractor.unidadTransporte.clave,
                carga = ctrDecimal.valor
            };
            ListViewItem lvl = new ListViewItem();
            lvl.Content = comparacion;
            lvlNuevos.Items.Add(lvl);

            txtVale.Clear();
            ctrDecimal.valor = 0;
            txtVale.Focus();
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (lvlNuevos.Items.Count > 0)
            {
                List<ComparacionValesCombustible> lista = getLista();
                if (lista.Count > 0)
                {
                    var resp = new CompacionValesCombustibleScv().saveValesLIS(lista);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        lvlNuevos.Items.Clear();
                    }
                    ImprimirMensaje.imprimir(resp);
                }
            }
        }

        private List<ComparacionValesCombustible> getLista()
        {
            List<ComparacionValesCombustible> lista = new List<ComparacionValesCombustible>();
            foreach (ListViewItem item in lvlNuevos.Items)
            {
                lista.Add(item.Content as ComparacionValesCombustible);
            }
            return lista;
        }

        private void btnLimpiar_Click(object sender, RoutedEventArgs e)
        {
            lvlNuevos.Items.Clear();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                f.OpenFileDialog dialog = new f.OpenFileDialog();
                //dialog
                dialog.Filter = "|*.xlsx";
                dialog.Title = "Elige el archivo para subir";
                if (dialog.ShowDialog() == f.DialogResult.OK)
                {
                    OperationResult readExel = new LeerExcel().leerDocumentoComparacionValesCombustible(dialog.FileName);
                    if (readExel.typeResult == ResultTypes.error)
                    {
                        MessageBox.Show(readExel.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    if (readExel.typeResult == ResultTypes.warning)
                    {
                        MessageBox.Show(readExel.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    if (readExel.typeResult == ResultTypes.success)
                    {
                        foreach (var item in readExel.result as List<ComparacionValesCombustible>)
                        {
                            Cursor = Cursors.Wait;
                            ComparacionValesCombustible comparacion = new ComparacionValesCombustible
                            {
                                id = 0,
                                mes = item.mes,
                                año = item.año,
                                vale = item.vale,
                                unidadTransporte = item.unidadTransporte,
                                carga = item.carga
                            };
                            ListViewItem lvl = new ListViewItem();
                            lvl.Content = comparacion;
                            lvlNuevos.Items.Add(lvl);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }            
        }
    }
}
