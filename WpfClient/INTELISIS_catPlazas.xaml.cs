﻿using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using CoreINTELISIS.INTERFACES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para INTELISIS_catPlazas.xaml
    /// </summary>
    public partial class INTELISIS_catPlazas : UserControl
    {
        SincronizadorINTELISIS pantalla = null;
        public INTELISIS_catPlazas(SincronizadorINTELISIS pantalla)
        {
            InitializeComponent();
            this.pantalla = pantalla;
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            pantalla.stpContenedor.Children.Clear();
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lvlPlazas.ItemsSource = null;
                lblContador.Content = 0;
                lblActualizados.Content = 0;
                lblNuevos.Content = 0;
                Cursor = Cursors.Wait;
                var resp = new PlazaINTELISISSvc().getAllPlazas();
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Plaza> listaPlaza = resp.result as List<Plaza>;
                    lvlPlazas.ItemsSource = listaPlaza;
                    lblContador.Content = listaPlaza.Count;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                lblActualizados.Content = 0;
                lblNuevos.Content = 0;
                if (lvlPlazas.ItemsSource != null)
                {
                    List<Plaza> listaPlaza = lvlPlazas.ItemsSource as List<Plaza>;
                    var resp = new PlazaSvc().sincronizarPlazas(listaPlaza);
                    lblActualizados.Content = resp.noActualizados;
                    lblNuevos.Content = resp.noNuevos;
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
