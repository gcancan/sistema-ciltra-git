﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for ventanaInputTexto.xaml
    /// </summary>
    public partial class ventanaInputTexto : Window
    {
        string titulo;
        string texto;
        public ventanaInputTexto(string titulo, string texto)
        {
            this.titulo = titulo;
            this.texto = texto;
            InitializeComponent();
        }

        public string obtenerRespuesta()
        {
            try
            {
                var resp = ShowDialog();
                if (resp.Value && !string.IsNullOrEmpty(txtValor.Text.Trim()))
                {
                    return txtValor.Text.Trim();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Opacity = .92;
            Title = titulo.ToUpper();
            txtTexto.Text = texto;
        }        

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
