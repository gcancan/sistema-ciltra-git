﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para reporteKardexCombustible.xaml
    /// </summary>
    public partial class reporteKardexCombustible : ViewBase
    {
        public reporteKardexCombustible()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
            ctrZonaOperativa.cargarControl(mainWindow.zonaOperativa, mainWindow.usuario.idUsuario);
            limpiarListas();
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
        }

        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            limpiarListas();
            if (ctrZonaOperativa.zonaOperativa != null)
            {
                var resp = new TanqueCombustibleSvc().getTanqueByZonaOperativa(ctrZonaOperativa.zonaOperativa);
                if (resp.typeResult == ResultTypes.success)
                {
                    TanqueCombustible tanque = resp.result as TanqueCombustible;
                    txtNombreTanque.Tag = tanque;
                    txtNombreTanque.Text = tanque.nombre;
                }
                else
                {
                    txtNombreTanque.Tag = null;
                    txtNombreTanque.Clear();
                }
            }
        }
        void limpiarListas()
        {
            lvlTanque.ItemsSource = null;
            lvlTanqueEmpresa.ItemsSource = null;
        }
        List<KardexCombustible> listaResultado = new List<KardexCombustible>();
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                listaResultado = new List<KardexCombustible>();

                if (txtNombreTanque.Tag == null)
                {
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "No hay tanque de combustible para esta zona operativa"));
                    return;
                }

                TanqueCombustible tanque = txtNombreTanque.Tag as TanqueCombustible;

                var resp = new KardexCombustibleSvc().getKardexCombustible(tanque.idTanqueCombustible, ctrFechas.fechaInicial, ctrFechas.fechaFinal);
                if (resp.typeResult == ResultTypes.success)
                {
                    listaResultado = resp.result as List<KardexCombustible>;
                    lvlTanque.ItemsSource = listaResultado.FindAll(s => s.empresa == null);

                    lvlTanqueEmpresa.ItemsSource = listaResultado.FindAll(s => s.empresa != null);
                    CollectionView viewDet = (CollectionView)CollectionViewSource.GetDefaultView(lvlTanqueEmpresa.ItemsSource);
                    PropertyGroupDescription groupDescriptionDet = new PropertyGroupDescription("empresa.nombre");
                    viewDet.GroupDescriptions.Add(groupDescriptionDet);
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }


        private void btnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                List<GrupoKardex> listaGrupo = (
                from p in listaResultado
                group p by (p.empresa == null ? 0 : p.empresa.clave) into g
                select new GrupoKardex()
                {
                    empresa = g.Select(s => s.empresa).First(),
                    listaKardex = g.Select(s => s).ToList()
                }).ToList();

                listaGrupo = listaGrupo.OrderByDescending(s => (s.empresa == null ? 0 : s.empresa.clave)).ToList();
                new ReportView().generarKardexCombustible(listaGrupo, ctrFechas.fechaInicial, ctrFechas.fechaFinal);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
            
        }
    }
}
