﻿using Core.Interfaces;
using Core.Models;
using Core.Utils;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using f = Fletera;
namespace WpfClient
{
    internal static class ProcesosViaje
    {
        public static void procesosViaje(ComercialCliente cli, ComercialEmpresa emp, DatosFacturaXML datosFactura, Viaje _viaje, 
            int ordenServicio, bool isExterno, bool ImprimeVales, MainWindow mainWindow, int numeroVales, bool imprimeValeSalida)
        {            
            OperationResult r = guardarValesCarga(_viaje.listDetalles, _viaje.cliente, numeroVales);
            if (!isExterno)
            {
                if (r.typeResult == ResultTypes.success && _viaje.remolque.tipoUnidad.descripcion.Contains("CAJA VOLTEO") && ImprimeVales)
                {
                    ReportView reporte = new ReportView();
                    reporte.imprimirValesCarga((List<ValeCarga>)r.result, _viaje, new Util().getRutasImpresora("VIAJES_ATLANTE"));
                }
            }

            if (new Util().isActivoImpresion())
            {
                Viaje v = (Viaje)_viaje.Clone();
                var listaAgrupada = agruparByZona(v.listDetalles);
                foreach (var item in listaAgrupada)
                {
                    imprimirReportes(cli, emp, datosFactura, item, _viaje, mainWindow);
                }
            }

            if (!isExterno)
            {
                if (imprimeValeSalida)
                {
                    f.frmSolServ frm = new Fletera.frmSolServ(new Util().getConectionString().ToString(), mainWindow.inicio._usuario.nombreUsuario, mainWindow.empresa.clave, mainWindow.inicio._usuario.idUsuario);
                    frm.CreaTablaImprime();
                    frm.ImprimeOrdenServicio(ordenServicio, false, false, true, new Util().getRutasImpresora("VIAJES_ATLANTE"));
                }
            }

            return;
            if (isExterno)
            {
                var xc = MessageBox.Show("¿Quiere anexar una vale de combustible externo para este viaje?", "", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (xc == MessageBoxResult.Yes)
                {
                    MainWindow main = new MainWindow(mainWindow);
                    var respM = main.abrirVentanaCargaExterno(ordenServicio, _viaje);
                    if (respM != null)
                    {
                        if (respM.typeResult == ResultTypes.success)
                        {
                            OperationResult respEx = new CargaCombustibleSvc().asignarValeExternoCartaPortes(_viaje.listDetalles, ((CargaCombustible)respM.result).idCargaCombustible);
                            if (respEx.typeResult == ResultTypes.success)
                            {
                                var fin = MessageBox.Show("¿Quiere Finalizar El Viaje?", "", MessageBoxButton.YesNo, MessageBoxImage.Question);
                                if (fin == MessageBoxResult.Yes)
                                {
                                    var finCp = new CartaPorteSvc().cerrarCancelarCartaPorte(_viaje.NumGuiaId,
                                        TipoAccion.FINALIZAR, mainWindow.inicio._usuario.idUsuario, "", _viaje.FechaHoraViaje);
                                }
                            }
                        }
                    }
                }
            }
        }

        private static OperationResult guardarValesCarga(List<CartaPorte> listDetalles, Cliente cliente, int numeroVales)
        {
            try
            {
                List<ValeCarga> listaVales = new List<ValeCarga>();

                foreach (var item in listDetalles)
                {
                    listaVales.Add(new ValeCarga
                    {
                        idValeCarga = 0,
                        remision = item.remision,
                        numGuiaId = item.numGuiaId,
                        origen = item.origen,
                        destino = item.zonaSelect,
                        producto = item.producto,
                        volDescarga = item.volumenDescarga,
                        cartaPorte = item.Consecutivo,
                        precio = item.precio,
                        cliente = cliente,
                        vDescargado = item.volumenDescarga
                    });
                }
                for (int i = 0; i < numeroVales; i++)
                {
                    listaVales.Add(new ValeCarga() { numGuiaId = listDetalles[0].numGuiaId, cliente = cliente });
                }
                return new CartaPorteSvc().saveValesCarga(listaVales);
                //return new OperationResult { valor = 0, result = listaVales };
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }
        private static List<List<CartaPorte>> agruparByZona(List<CartaPorte> listDetalles)
        {
            var grupo = new AgruparCartaPortes();
            grupo.agruparByZona(listDetalles);
            return grupo.superLista;
        }
        private static void imprimirReportes(ComercialCliente respCliente, ComercialEmpresa respEmpresa, DatosFacturaXML datosFactura, List<CartaPorte> listaAgrupada, Viaje viaje, MainWindow mainWindow)
        {
            try
            {
                
                ReportView reporte = new ReportView(mainWindow);
                //bool resp = reporte.cartaPorteFalsoAtlante(respCliente, respEmpresa, datosFactura, listaAgrupada, viaje, new Util().getRutasImpresora("VIAJES_ATLANTE"));
                  reporte.cartaPorteFormatoNuevo(respCliente, respEmpresa, datosFactura, listaAgrupada, viaje, new Util().getRutasImpresora("VIAJES_ATLANTE"));
                //if (resp)
                //{
                //    //reporte.Show();
                //    //MessageBox.Show("Imprimiendo carta porte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                //}
                //else
                //{
                //    MessageBox.Show("Ocurrio un error al intentar imprimir la Carta Porte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }
    }
}
