﻿using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for ReporteBitacoraPrecios.xaml
    /// </summary>
    public partial class ReporteBitacoraPrecios : ViewBase
    {
        public ReporteBitacoraPrecios()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {            
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
        }       
        private void btnImprimir_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait; 
                OperationResult resp = new BitacoraCambioPreciosSvc().getBitacoraPrecios(ctrFechas.fechaInicial, ctrFechas.fechaFinal);
                var respReporte = new ReportView();
                if (respReporte.ReporteBitacoraPrecios((List<BitacoraCambioPrecios>)resp.result, ctrFechas.tapMes.IsSelected, ctrFechas.fechaInicial, ctrFechas.fechaFinal))
                {
                    respReporte.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
