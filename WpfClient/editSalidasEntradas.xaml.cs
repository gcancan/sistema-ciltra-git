﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for editSalidasEntradas.xaml
    /// </summary>
    public partial class editSalidasEntradas : ViewBase
    {
        public editSalidasEntradas()
        {
            InitializeComponent();
        }
        int claveEmpresa = 0;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            //string misDatos = HttpContext.Current.Server.MapPath(".");

            var resp = new MotivoEntradaSvc().getMotivos();
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    foreach (var item in (List<MotivoEntrada>)resp.result)
                    {
                        cbxMotivosBitacora.Items.Add(item);
                    }
                    break;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);

                    return;
                case ResultTypes.recordNotFound:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);

                    return;
                default:

                    return;
            }
            nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
        }
        public override void nuevo()
        {
            //mapUnidades(TipoUnidad.TRACTOR, null);
            //mapOperador(null);
            //txtKilometraje.Clear();
            //cbxMotivos.SelectedItem = null;
            //this.salidaEntrada = new SalidaEntrada();
            //txtTractor.Focus();

            mapForm(null);
            mapFormBitacora(null);

        }

        private void mapFormBitacora(BitacoraEntradasSalidas result)
        {
            if (result != null)
            {
                itemBitacoras.Tag = result;
                mapUnidadesBitacora(TipoUnidad.TRACTOR, result.tractor);
                mapUnidadesBitacora(TipoUnidad.TOLVA, result.tolva1);
                mapUnidadesBitacora(TipoUnidad.DOLLY, result.dolly);
                mapUnidadesBitacora(TipoUnidad.TOLVA_2, result.tolva2);
                mapOperadorBitacora(result.chofer);
                txtKilometrajeBitacora.Text = result.Kilometraje.ToString();
                btnSalidaBitacora.IsEnabled = true;
                btnEntradaBitacora.IsEnabled = false;
            }
            else
            {
                itemBitacoras.Tag = null;
                mapUnidadesBitacora(TipoUnidad.TRACTOR, null);
                mapUnidadesBitacora(TipoUnidad.TOLVA, null);
                mapUnidadesBitacora(TipoUnidad.DOLLY, null);
                mapUnidadesBitacora(TipoUnidad.TOLVA_2, null);
                mapOperadorBitacora(null);
                txtKilometrajeBitacora.Clear();
                btnSalidaBitacora.IsEnabled = false;
                btnEntradaBitacora.IsEnabled = true;
            }
        }

        enum TipoUnidad
        {
            TRACTOR = 0,
            TOLVA = 1,
            DOLLY = 2,
            TOLVA_2 = 3
        }

        private void mapUnidades(TipoUnidad tipo, UnidadTransporte Unidad)
        {
            switch (tipo)
            {
                case TipoUnidad.TRACTOR:
                    if (Unidad != null)
                    {
                        txtTractor.Tag = Unidad;
                        txtTractor.Text = Unidad.clave;
                        txtTractor.IsEnabled = false;
                        txtTolva1.Focus();
                    }
                    else
                    {
                        txtTractor.Tag = null;
                        txtTractor.Clear();
                        txtTractor.IsEnabled = true;
                    }
                    break;
                case TipoUnidad.TOLVA:
                    if (Unidad != null)
                    {
                        txtTolva1.Tag = Unidad;
                        txtTolva1.Text = Unidad.clave;
                        txtTolva1.IsEnabled = false;
                        txtDolly.Focus();
                    }
                    else
                    {
                        txtTolva1.Tag = null;
                        txtTolva1.Clear();
                        txtTolva1.IsEnabled = true;
                    }
                    break;
                case TipoUnidad.DOLLY:
                    if (Unidad != null)
                    {
                        txtDolly.Tag = Unidad;
                        txtDolly.Text = Unidad.clave;
                        txtDolly.IsEnabled = false;
                        txtTolva2.Focus();
                    }
                    else
                    {
                        txtDolly.Tag = null;
                        txtDolly.Clear();
                        txtDolly.IsEnabled = true;
                    }
                    break;
                case TipoUnidad.TOLVA_2:
                    if (Unidad != null)
                    {
                        txtTolva2.Tag = Unidad;
                        txtTolva2.Text = Unidad.clave;
                        txtTolva2.IsEnabled = false;
                        txtOperador.Focus();
                    }
                    else
                    {
                        txtTolva2.Tag = null;
                        txtTolva2.Clear();
                        txtTolva2.IsEnabled = true;
                    }
                    break;
                default:
                    break;
            }
        }

        private List<UnidadTransporte> buscarUnidades(string clave, int idEmpresa = 0)
        {
            OperationResult resp = new UnidadTransporteSvc().getUnidadesALLorById(idEmpresa, clave);
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    return (List<UnidadTransporte>)resp.result;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return null;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return null;
                case ResultTypes.recordNotFound:
                    return new List<UnidadTransporte>();

                default:
                    return null;
            }
        }

        private List<UnidadTransporte> buscarRemolques(int idEmpresa, string clave)
        {
            OperationResult resp = new UnidadTransporteSvc().buscarRemolques(idEmpresa, clave);
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    return (List<UnidadTransporte>)resp.result;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return null;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return null;
                case ResultTypes.recordNotFound:
                    return new List<UnidadTransporte>();

                default:
                    return null;
            }
        }

        private void completarClave(ref string clave)
        {
            if (clave.Length < 3)
            {
                for (int i = 0; i <= (3 - clave.Length); i++)
                {
                    clave = "0" + clave;
                }
            }
        }

        private void txtTractor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string clave = txtTractor.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                completarClave(ref clave);
                UnidadTransporte unidadTransporte = new UnidadTransporte();
                List<UnidadTransporte> ListUnidadT = buscarUnidades("TC" + clave);
                if (ListUnidadT.Count == 1)
                {
                    mapUnidades(TipoUnidad.TRACTOR, ListUnidadT[0]);
                    unidadTransporte = ListUnidadT[0];
                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("TC", claveEmpresa, clave);
                    var s = buscardor.buscarUnidades();
                    mapUnidades(TipoUnidad.TRACTOR, s);
                    unidadTransporte = s;
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    mapUnidades(TipoUnidad.TRACTOR, s);
                    unidadTransporte = s;
                }
                if (unidadTransporte != null)
                {
                    var resp = new MasterOrdServSvc().getMasterOrdServByCamion(unidadTransporte.clave);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        buscarMasterOrdenSer(((MasterOrdServ)resp.result).idPadreOrdSer);
                    }
                }
                //if (unidadTransporte != null)
                //{
                //    var resp = new OperacionSvc().getSalidaEntradaByCamion(unidadTransporte.clave);
                //    if (((SalidaEntrada)resp.result).tipoMoviemiento == eTipoMovimiento.ENTRADA)
                //    {
                //        if (MessageBox.Show("Esta unidad Tiene una entrada ya registrada." + Environment.NewLine
                //            + "¿Desea generar su salida?", "Aviso", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                //        {
                //            mapOperador(((SalidaEntrada)resp.result).masterOrdServ.chofer);
                //            ListSalidaEntrada = ((List<SalidaEntrada>)resp.result);
                //            guardar(eTipoMovimiento.SALIDA);
                //        }

                //    }
                //}
            }
        }
        List<SalidaEntrada> ListSalidaEntrada = new List<SalidaEntrada>();
        private void txtTolva1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {

                string clave = txtTolva1.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                completarClave(ref clave);

                List<UnidadTransporte> ListUnidadT = buscarUnidades("TV" + clave);
                if (ListUnidadT.Count == 1)
                {
                    mapUnidades(TipoUnidad.TOLVA, ListUnidadT[0]);

                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("TV", claveEmpresa, clave);
                    var s = buscardor.buscarUnidades();
                    mapUnidades(TipoUnidad.TOLVA, s);
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    mapUnidades(TipoUnidad.TOLVA, s);
                }
            }
        }

        private void txtDolly_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {

                string clave = txtDolly.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                completarClave(ref clave);

                List<UnidadTransporte> ListUnidadT = buscarUnidades("DL" + clave);
                if (ListUnidadT.Count == 1)
                {
                    mapUnidades(TipoUnidad.DOLLY, ListUnidadT[0]);

                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("DL", claveEmpresa, clave);
                    var s = buscardor.buscarUnidades();
                    mapUnidades(TipoUnidad.DOLLY, s);
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    mapUnidades(TipoUnidad.DOLLY, s);
                }
            }
        }

        private void txtTolva2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {

                string clave = txtTolva2.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                completarClave(ref clave);

                List<UnidadTransporte> ListUnidadT = buscarUnidades("TV" + clave);
                if (ListUnidadT.Count == 1)
                {
                    mapUnidades(TipoUnidad.TOLVA_2, ListUnidadT[0]);

                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("TV", claveEmpresa, clave);
                    var s = buscardor.buscarUnidades();
                    mapUnidades(TipoUnidad.TOLVA_2, s);
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    mapUnidades(TipoUnidad.TOLVA_2, s);
                }
            }
        }

        private void txtOperador_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtTractor.Tag == null)
                {
                    return;
                }
                string clave = txtOperador.Text.Trim();
                //completarClave(ref clave);
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                List<Personal> listPersonal = buscarPersonal(clave);
                if (listPersonal.Count == 1)
                {
                    mapOperador(listPersonal[0]);
                }
                else if (listPersonal.Count == 0)
                {
                    selectPersonal buscardor = new selectPersonal(clave, ((UnidadTransporte)txtTractor.Tag).empresa.clave);
                    var per = buscardor.buscarPersonal();
                    mapOperador(per);
                }
                else
                {
                    selectPersonal buscardor = new selectPersonal(clave, ((UnidadTransporte)txtTractor.Tag).empresa.clave);
                    var per = buscardor.buscarPersonal();
                    mapOperador(per);
                }
            }
        }

        private void mapOperador(Personal operador)
        {
            if (operador != null)
            {
                txtOperador.Tag = operador;
                txtOperador.Text = operador.nombre;
                txtOperador.IsEnabled = false;
                txtKilometraje.Focus();
            }
            else
            {
                txtOperador.Tag = null;
                txtOperador.Text = "";
                txtOperador.IsEnabled = true;
            }
        }

        private void mapOperadorBitacora(Personal operador)
        {
            if (operador != null)
            {
                txtOperadorBitacora.Tag = operador;
                txtOperadorBitacora.Text = operador.nombre;
                txtOperadorBitacora.IsEnabled = false;
                txtKilometrajeBitacora.Focus();
            }
            else
            {
                txtOperadorBitacora.Tag = null;
                txtOperadorBitacora.Text = "";
                txtOperadorBitacora.IsEnabled = true;
            }
        }

        private List<Personal> buscarPersonal(string nombre)
        {
            OperationResult resp = new OperadorSvc().getOperadoresByNombre(nombre);
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    return (List<Personal>)resp.result;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return null;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return null;
                case ResultTypes.recordNotFound:
                    return new List<Personal>();

                default:
                    return null;
            }
        }

        private void btnEntrada_Click(object sender, RoutedEventArgs e)
        {
            guardar(eTipoMovimiento.ENTRADA);
        }

        private void btnSalida_Click(object sender, RoutedEventArgs e)
        {            
            guardar(eTipoMovimiento.SALIDA);
        }
        private void guardar(List<SalidaEntrada> listSalidaEntrada, eTipoMovimiento tipoMovimiento)
        {
            if (!validarUbicaciones(tipoMovimiento))
            {
                return;
            }
            OperationResult resp = new OperationResult();
            foreach (var item in listSalidaEntrada)
            {
                item.fechaFin = DateTime.Now;
                item.tipoMoviemiento = tipoMovimiento;
            }

            resp = new OperacionSvc().saveSalidaEntrada(listSalidaEntrada);
            if (resp.typeResult == ResultTypes.success)
            {
                var masterOrdServ = listSalidaEntrada[0].masterOrdServ;
                List<UbicacionUnidades> listaUbicaciones = new List<UbicacionUnidades>
                    {
                        new UbicacionUnidades
                        {
                            unidadTrans = masterOrdServ.tractor,
                            descripcion = string.Format("{0} por Orden de Servicio", tipoMovimiento.ToString()).ToUpper(),
                            folio = masterOrdServ.idPadreOrdSer,
                            tipoMovimiento = tipoMovimiento,
                            ubicacionActual = EstatusUbicacion.BASE,
                            ubicacionNew = tipoMovimiento == eTipoMovimiento.ENTRADA ? EstatusUbicacion.BASE : EstatusUbicacion.RUTA
                        }
                    };

                if (masterOrdServ.tolva1 != null)
                {
                    listaUbicaciones.Add(
                        new UbicacionUnidades
                        {
                            unidadTrans = masterOrdServ.tolva1,
                            descripcion = string.Format("{0} por Orden de Servicio", tipoMovimiento.ToString()).ToUpper(),
                            folio = masterOrdServ.idPadreOrdSer,
                            tipoMovimiento = tipoMovimiento,
                            ubicacionActual = EstatusUbicacion.BASE,
                            ubicacionNew = tipoMovimiento == eTipoMovimiento.ENTRADA ? EstatusUbicacion.BASE : EstatusUbicacion.RUTA
                        });
                }
                if (masterOrdServ.tolva2 != null)
                {
                    listaUbicaciones.Add(
                        new UbicacionUnidades
                        {
                            unidadTrans = masterOrdServ.tolva2,
                            descripcion = string.Format("{0} por Orden de Servicio", tipoMovimiento.ToString()).ToUpper(),
                            folio = masterOrdServ.idPadreOrdSer,
                            tipoMovimiento = tipoMovimiento,
                            ubicacionActual = EstatusUbicacion.BASE,
                            ubicacionNew = tipoMovimiento == eTipoMovimiento.ENTRADA ? EstatusUbicacion.BASE : EstatusUbicacion.RUTA
                        });
                }
                if (masterOrdServ.dolly != null)
                {
                    listaUbicaciones.Add(
                        new UbicacionUnidades
                        {
                            unidadTrans = masterOrdServ.dolly,
                            descripcion = string.Format("{0} por Orden de Servicio", tipoMovimiento.ToString()).ToUpper(),
                            folio = masterOrdServ.idPadreOrdSer,
                            tipoMovimiento = tipoMovimiento,
                            ubicacionActual = EstatusUbicacion.BASE,
                            ubicacionNew = tipoMovimiento == eTipoMovimiento.ENTRADA ? EstatusUbicacion.BASE : EstatusUbicacion.RUTA
                        });
                }

                var respUbicacion = new UbicacionUnidadesSvc().saveUbicacionUnidades(listaUbicaciones);
                mainWindow.scrollContainer.IsEnabled = false;
            }
            imprimirMensaje(resp);
            nuevo();

        }

        private bool validarUbicaciones(eTipoMovimiento tipoMovimiento)
        {
            if (tipoMovimiento == eTipoMovimiento.ENTRADA)
            {
                if (!ReglasUbicacion.validar(EstatusUbicacion.BASE, (UnidadTransporte)txtTractor.Tag))
                {
                    return false;
                }
                if (txtTolva1.Tag != null)
                {
                    if (!ReglasUbicacion.validar(EstatusUbicacion.BASE, (UnidadTransporte)txtTolva1.Tag))
                    {
                        return false;
                    }
                }

                if (txtTolva2.Tag != null)
                {
                    if (!ReglasUbicacion.validar(EstatusUbicacion.BASE, (UnidadTransporte)txtTolva2.Tag))
                    {
                        return false;
                    }
                }

                if (txtDolly.Tag != null)
                {
                    if (!ReglasUbicacion.validar(EstatusUbicacion.BASE, (UnidadTransporte)txtDolly.Tag))
                    {
                        return false;
                    }
                }
            }
            else if(tipoMovimiento == eTipoMovimiento.SALIDA)
            {
                if (!ReglasUbicacion.validar(EstatusUbicacion.RUTA, (UnidadTransporte)txtTractor.Tag))
                {
                    return false;
                }
                if (txtTolva1.Tag != null)
                {
                    if (!ReglasUbicacion.validar(EstatusUbicacion.RUTA, (UnidadTransporte)txtTolva1.Tag))
                    {
                        return false;
                    }
                }

                if (txtTolva2.Tag != null)
                {
                    if (!ReglasUbicacion.validar(EstatusUbicacion.RUTA, (UnidadTransporte)txtTolva2.Tag))
                    {
                        return false;
                    }
                }

                if (txtDolly.Tag != null)
                {
                    if (!ReglasUbicacion.validar(EstatusUbicacion.RUTA, (UnidadTransporte)txtDolly.Tag))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public void guardar(eTipoMovimiento tipoMovimiento)
        {
            try
            {
                if (!validarUbicaciones(tipoMovimiento))
                {
                    return;
                }
                if (!chBoxChequeo.IsChecked.Value)
                {
                    //imprimirMensaje();
                    cambiarDatos();
                    return;
                }
                Cursor = Cursors.Wait;
                var val = validar();
                if (val.typeResult != ResultTypes.success)
                {
                    imprimirMensaje(val);
                    return;
                }
                var masterOrdServ = (MasterOrdServ)txtFolio.Tag;
                masterOrdServ.chofer = (Personal)txtOperador.Tag;
                //masterOrdServ.tractor = (UnidadTransporte)txtTractor.Tag;
                //masterOrdServ.tolva1 = (UnidadTransporte)txtTolva1.Tag;
                //masterOrdServ.dolly = txtDolly.Tag == null ? null : (UnidadTransporte)txtDolly.Tag;
                //masterOrdServ.tolva2 = txtTolva2.Tag == null ? null : (UnidadTransporte)txtTolva2.Tag;
                OperationResult resp = new OperationResult();
                var f = DateTime.Now;
                List<SalidaEntrada> listSal = new List<SalidaEntrada>();

                if (masterOrdServ.tractor != null)
                {
                    SalidaEntrada salEnt = new SalidaEntrada
                    {
                        chofer = masterOrdServ.chofer,
                        masterOrdServ = masterOrdServ,
                        fecha = f,
                        tipoMoviemiento = tipoMovimiento,
                        usuario = mainWindow.inicio._usuario
                    };
                    salEnt.unidadTransporte = masterOrdServ.tractor;
                    salEnt.km = Convert.ToDecimal(txtKilometraje.Text.Trim());
                    listSal.Add(salEnt);
                }
                if (masterOrdServ.tolva1 != null)
                {
                    SalidaEntrada salEnt = new SalidaEntrada
                    {
                        chofer = masterOrdServ.chofer,
                        masterOrdServ = masterOrdServ,
                        fecha = f,
                        tipoMoviemiento = tipoMovimiento,
                        usuario = mainWindow.inicio._usuario
                    };
                    salEnt.unidadTransporte = masterOrdServ.tolva1;
                    salEnt.km = 0;
                    listSal.Add(salEnt);
                }
                if (masterOrdServ.dolly != null)
                {
                    SalidaEntrada salEnt = new SalidaEntrada
                    {
                        chofer = masterOrdServ.chofer,
                        masterOrdServ = masterOrdServ,
                        fecha = f,
                        tipoMoviemiento = tipoMovimiento,
                        usuario = mainWindow.inicio._usuario
                    };
                    salEnt.unidadTransporte = masterOrdServ.dolly;
                    salEnt.km = 0;
                    listSal.Add(salEnt);
                }
                if (masterOrdServ.tolva2 != null)
                {
                    SalidaEntrada salEnt = new SalidaEntrada
                    {
                        chofer = masterOrdServ.chofer,
                        masterOrdServ = masterOrdServ,
                        fecha = f,
                        tipoMoviemiento = tipoMovimiento,
                        usuario = mainWindow.inicio._usuario
                    };
                    salEnt.unidadTransporte = masterOrdServ.tolva2;
                    salEnt.km = 0;
                    listSal.Add(salEnt);
                }

                resp = new OperacionSvc().saveSalidaEntrada(listSal);
                if (resp.typeResult == ResultTypes.success)
                {
                    mainWindow.scrollContainer.IsEnabled = false;

                    List<UbicacionUnidades> listaUbicaciones = new List<UbicacionUnidades>
                    {
                        new UbicacionUnidades
                        {
                            unidadTrans = masterOrdServ.tractor,
                            descripcion = string.Format("{0} por Orden de Servicio", tipoMovimiento.ToString()).ToUpper(),
                            folio = masterOrdServ.idPadreOrdSer,
                            tipoMovimiento = tipoMovimiento,
                            ubicacionActual = EstatusUbicacion.BASE,
                            ubicacionNew = tipoMovimiento == eTipoMovimiento.ENTRADA ? EstatusUbicacion.BASE : EstatusUbicacion.RUTA
                        }
                    };

                    if (masterOrdServ.tolva1 != null)
                    {
                        listaUbicaciones.Add(
                            new UbicacionUnidades
                            {
                                unidadTrans = masterOrdServ.tolva1,
                                descripcion = string.Format("{0} por Orden de Servicio", tipoMovimiento.ToString()).ToUpper(),
                                folio = masterOrdServ.idPadreOrdSer,
                                tipoMovimiento = tipoMovimiento,
                                ubicacionActual = EstatusUbicacion.BASE,
                                ubicacionNew = tipoMovimiento == eTipoMovimiento.ENTRADA ? EstatusUbicacion.BASE : EstatusUbicacion.RUTA
                            });
                    }
                    if (masterOrdServ.tolva2 != null)
                    {
                        listaUbicaciones.Add(
                            new UbicacionUnidades
                            {
                                unidadTrans = masterOrdServ.tolva2,
                                descripcion = string.Format("{0} por Orden de Servicio", tipoMovimiento.ToString()).ToUpper(),
                                folio = masterOrdServ.idPadreOrdSer,
                                tipoMovimiento = tipoMovimiento,
                                ubicacionActual = EstatusUbicacion.BASE,
                                ubicacionNew = tipoMovimiento == eTipoMovimiento.ENTRADA ? EstatusUbicacion.BASE : EstatusUbicacion.RUTA
                            });
                    }
                    if (masterOrdServ.dolly != null)
                    {
                        listaUbicaciones.Add(
                            new UbicacionUnidades
                            {
                                unidadTrans = masterOrdServ.dolly,
                                descripcion = string.Format("{0} por Orden de Servicio", tipoMovimiento.ToString()).ToUpper(),
                                folio = masterOrdServ.idPadreOrdSer,
                                tipoMovimiento = tipoMovimiento,
                                ubicacionActual = EstatusUbicacion.BASE,
                                ubicacionNew = tipoMovimiento == eTipoMovimiento.ENTRADA ? EstatusUbicacion.BASE : EstatusUbicacion.RUTA
                            });
                    }

                    var respUbicacion = new UbicacionUnidadesSvc().saveUbicacionUnidades(listaUbicaciones);
                }
                imprimirMensaje(resp);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void cambiarDatos()
        {
            try
            {
                MessageBoxResult result = MessageBox.Show(
                    @"Se requiere la validación del Chofer" + Environment.NewLine
                    + "¿La informacón es correcta?"
                    , "Aviso", MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                switch (result)
                {
                    case MessageBoxResult.Yes:
                        chBoxChequeo.IsChecked = true;
                        break;
                    case MessageBoxResult.No:
                        cambiar();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void cambiar()
        {
            try
            {
                //txtTractor.IsEnabled = true;
                //txtTolva1.IsEnabled = true;
                //txtTolva2.IsEnabled = true;
                //txtDolly.IsEnabled = true;
                txtOperador.IsEnabled = true;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private List<MotivoEntrada> optenerDatos()
        {
            var list = cbxMotivos.SelectedItems;
            List<MotivoEntrada> listMotivos = new List<MotivoEntrada>();
            foreach (MotivoEntrada item in list)
            {
                listMotivos.Add(item);
            }
            return listMotivos;
        }

        private OperationResult validar()
        {
            bool resp = true;
            List<String> error = new List<string>();

            if (txtTractor.Tag == null)
            {
                resp = false;
                error.Add("Elegir el tractor." + Environment.NewLine);
            }

            if (txtKilometraje.Text == "")
            {
                resp = false;
                error.Add("El kilometraje no es Valido." + Environment.NewLine);
            }

            var val = new RegistroKilometrajeSvc().getUltimoRegistroValido(((UnidadTransporte)txtTractor.Tag).clave);
            if (val.typeResult == ResultTypes.success)
            {
                if ((Convert.ToDecimal(txtKilometraje.Text.Trim()) - ((RegistroKilometraje)val.result).kmFinal) >= 10000)
                {
                    resp = false;
                    error.Add("El kilometraje no es Valido." + Environment.NewLine);
                }
                if (((RegistroKilometraje)val.result).kmFinal > Convert.ToDecimal(txtKilometraje.Text.Trim()))
                {
                    resp = false;
                    error.Add("El kilometraje no es Valido por que es menor al ultimo KM." + Environment.NewLine);
                }
            }

            //if (cbxTolva1.SelectedItem == null)
            //{
            //    resp = false;
            //    error.Add("Elegir el remolque 1." + Environment.NewLine);
            //}

            //if (cbxChofer.SelectedItem == null)
            //{
            //    resp = false;
            //    error.Add("Elegir el chofer." + Environment.NewLine);
            //}


            //if (cbxDolly.SelectedItem != null)
            //{
            //    if (cbxTolva2.SelectedItem == null)
            //    {
            //        resp = false;
            //        error.Add("Seleccionar el remolque 2." + Environment.NewLine);
            //    }
            //}

            //if (cbxTolva2.SelectedItem != null)
            //{
            //    if (cbxDolly.SelectedItem == null)
            //    {
            //        resp = false;
            //        error.Add("Seleccionar Dolly." + Environment.NewLine);
            //    }
            //}

            string cadena = "Para continuar se requiere corregir lo siguiente:" + Environment.NewLine;
            foreach (var item in error)
            {
                cadena += item;
            }
            return new OperationResult { valor = resp ? 0 : 2, result = resp, mensaje = cadena };
        }

        private void imprimirMensaje(OperationResult resp)
        {
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    break;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
                case ResultTypes.recordNotFound:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    break;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    break;
                default:
                    break;
            }
        }

        private void txtFolio_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                int i = 0;
                if (int.TryParse(txtFolio.Text.Trim(), out i))
                {
                    buscarMasterOrdenSer(i);
                }
                else
                {
                    MessageBox.Show("El folio de la orden de servicio no es valido", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void buscarMasterOrdenSer(int i)
        {
            var resp = new MasterOrdServSvc().getMasterOrdServ(i);
            if (resp.typeResult == ResultTypes.success)
            {

                var entSal = new OperacionSvc().getSalidaEntradaByidORden(i);
                if (entSal.typeResult == ResultTypes.success)
                {
                    if (((List<SalidaEntrada>)entSal.result)[0].tipoMoviemiento == eTipoMovimiento.ENTRADA)
                    {
                        if (((List<SalidaEntrada>)entSal.result)[0].fecha != null)
                        {
                            ListSalidaEntrada = (List<SalidaEntrada>)entSal.result;
                            if (MessageBox.Show("Esta orden ya Tiene una entrada registrada." + Environment.NewLine
                        + "¿Desea generar su salida?", "Aviso", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                            {
                                mapForm((MasterOrdServ)resp.result);
                                guardar(ListSalidaEntrada, eTipoMovimiento.SALIDA);
                                nuevo();
                                return;
                            }
                            else
                            {
                                mainWindow.scrollContainer.IsEnabled = false;
                            }
                        }
                        else
                        {
                            ListSalidaEntrada = (List<SalidaEntrada>)entSal.result;
                            mainWindow.scrollContainer.IsEnabled = false;
                        }

                    }
                    else if (((List<SalidaEntrada>)entSal.result)[0].tipoMoviemiento == eTipoMovimiento.SALIDA)
                    {
                        if (((List<SalidaEntrada>)entSal.result)[0].fechaFin != null)
                        {
                            ListSalidaEntrada = (List<SalidaEntrada>)entSal.result;
                            if (MessageBox.Show("Esta orden ya Tiene una salida registrada." + Environment.NewLine
                        + "¿Desea generar su Entrada?", "Aviso", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                            {
                                mapForm((MasterOrdServ)resp.result);
                                guardar(ListSalidaEntrada, eTipoMovimiento.ENTRADA);
                                nuevo();
                                return;
                            }
                            else
                            {
                                mainWindow.scrollContainer.IsEnabled = false;
                            }
                        }
                        else
                        {
                            ListSalidaEntrada = (List<SalidaEntrada>)entSal.result;
                            mainWindow.scrollContainer.IsEnabled = false;
                        }
                    }
                    else
                    {

                        string idTractor = txtTractor.Text;
                        nuevo();
                        itemBitacoras.Focus();
                        txtTractorBitacora.Text = idTractor.Replace("TC", string.Empty);
                        txtTractorBitacora.Focus();
                        return;
                        //mainWindow.scrollContainer.IsEnabled = false;
                        //ListSalidaEntrada = (List<SalidaEntrada>)entSal.result;
                        //txtFecha.Text = ((List<SalidaEntrada>)entSal.result)[0].fecha.ToString();
                        //txtKilometraje.Text = ((List<SalidaEntrada>)entSal.result)[0].km.ToString();
                    }

                    //}
                }
                else if (entSal.typeResult == ResultTypes.recordNotFound)
                {

                }
                else
                {
                    ImprimirMensaje.imprimir(entSal);
                    mainWindow.scrollContainer.IsEnabled = false;
                    return;
                }
                mapForm((MasterOrdServ)resp.result);
            }
            else
            {
                imprimirMensaje(resp);
            }
        }

        private void mapForm(MasterOrdServ result)
        {
            if (result != null)
            {
                txtFolio.Tag = (result);
                txtFolio.IsEnabled = false;
                txtFolio.Text = result.idPadreOrdSer.ToString();
                mapUnidades(TipoUnidad.TRACTOR, result.tractor);
                mapUnidades(TipoUnidad.TOLVA, result.tolva1);
                mapUnidades(TipoUnidad.DOLLY, result.dolly);
                mapUnidades(TipoUnidad.TOLVA_2, result.tolva2);
                mapOperador(result.chofer);
                txtFecha.Text = result.fechaCreacion.ToString("dd/MMM/yy HH:mm:ss");
                txtFechaEntrada.Text = ListSalidaEntrada == null ? "" :  ListSalidaEntrada[0].fecha == null ? "" : ((DateTime)(ListSalidaEntrada[0].fecha)).ToString("dd/MMM/yy HH:mm:ss");
                txtFechaSalida.Text = ListSalidaEntrada == null ? "" : ListSalidaEntrada[0].fechaFin == null ? "" : ((DateTime)(ListSalidaEntrada[0].fechaFin)).ToString("dd/MMM/yy HH:mm:ss");
                //lvlMotivos.ItemsSource = result.listTiposOrdenServicio;
                foreach (var item in result.listTiposOrdenServicio)
                {
                    if (!existeList(item))
                    {
                        ListViewItem lvl = new ListViewItem();
                        lvl.Content = item;
                        lvlMotivos.Items.Add(lvl);
                    }
                }

            }
            else
            {
                txtFolio.Tag = null;
                txtFolio.IsEnabled = true;
                txtFolio.Clear();
                mapUnidades(TipoUnidad.TRACTOR, null);
                mapUnidades(TipoUnidad.TOLVA, null);
                mapUnidades(TipoUnidad.DOLLY, null);
                mapUnidades(TipoUnidad.TOLVA_2, null);
                mapOperador(null);
                txtKilometraje.Clear();
                txtFolio.Focus();
                this.ListSalidaEntrada = null;
                lvlMotivos.Items.Clear();
                txtFecha.Text = DateTime.Now.ToString("dd/MMM/yy");
                txtFechaEntrada.Clear();
                txtFechaSalida.Clear();
            }
        }

        private bool existeList(TipoOrdenServicio tp)
        {
            if (lvlMotivos.Items.Count == 0)
            {
                return false;
            }
            foreach (ListViewItem item in lvlMotivos.Items)
            {
                if (((TipoOrdenServicio)item.Content).TipoServicio == tp.TipoServicio)
                {
                    return true;
                }
            }
            return false;
        }
        List<BitmapImage> listImagenes = new List<BitmapImage>();
        int indiceImagen = 0;
        private void btnCargarImag_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Multiselect = true;

            openFile.Title = "Seleccione la Imagen a Mostrar";
            openFile.Filter = "Imagenes | *.jpg; *.gif; *.png; *.bmp";
            if (openFile.ShowDialog() == true)
            {
                foreach (var item in openFile.FileNames)
                {
                    BitmapImage b = new BitmapImage();
                    b.BeginInit();
                    b.UriSource = new Uri(item);
                    b.EndInit();
                    listImagenes.Add(b);
                    imgBoxImagen.Source = listImagenes[listImagenes.Count - 1];
                    indiceImagen = listImagenes.Count - 1;
                    var x = imgBoxImagen;
                }
            }
        }

        private void btnAnterior_Click(object sender, RoutedEventArgs e)
        {
            if (listImagenes.Count > 0)
            {
                if (indiceImagen > 0)
                {
                    indiceImagen--;
                }
                imgBoxImagen.Source = listImagenes[indiceImagen <= 0 ? 0 : indiceImagen];
            }
        }

        private void btnSiguiente_Click(object sender, RoutedEventArgs e)
        {
            if (listImagenes.Count > 0)
            {
                if (indiceImagen < (listImagenes.Count - 1))
                {
                    indiceImagen++;
                }
                imgBoxImagen.Source = listImagenes[indiceImagen >= listImagenes.Count - 1 ? listImagenes.Count - 1 : indiceImagen];
            }
        }

        private void txtTractorBitacora_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string clave = txtTractorBitacora.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                completarClave(ref clave);
                UnidadTransporte unidadTransporte = new UnidadTransporte();
                List<UnidadTransporte> ListUnidadT = buscarUnidades("TC" + clave);
                if (ListUnidadT.Count == 1)
                {
                    mapUnidadesBitacora(TipoUnidad.TRACTOR, ListUnidadT[0]);
                    unidadTransporte = ListUnidadT[0];
                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("TC", claveEmpresa, clave);
                    var s = buscardor.buscarUnidades();
                    mapUnidadesBitacora(TipoUnidad.TRACTOR, s);
                    unidadTransporte = s;
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    mapUnidadesBitacora(TipoUnidad.TRACTOR, s);
                    unidadTransporte = s;
                }


                if (unidadTransporte != null)
                {
                    var resp = new BitacoraSvc().getBitacoraByIdTractor(unidadTransporte.clave);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mapFormBitacora((BitacoraEntradasSalidas)resp.result);
                    }
                }
            }
        }

        private void mapUnidadesBitacora(TipoUnidad tipo, UnidadTransporte Unidad)
        {
            switch (tipo)
            {
                case TipoUnidad.TRACTOR:
                    if (Unidad != null)
                    {
                        txtTractorBitacora.Tag = Unidad;
                        txtTractorBitacora.Text = Unidad.clave;
                        txtTractorBitacora.IsEnabled = false;
                        txtTolva1Bitacora.Focus();
                    }
                    else
                    {
                        txtTractorBitacora.Tag = null;
                        txtTractorBitacora.Clear();
                        txtTractorBitacora.IsEnabled = true;
                    }
                    break;
                case TipoUnidad.TOLVA:
                    if (Unidad != null)
                    {
                        txtTolva1Bitacora.Tag = Unidad;
                        txtTolva1Bitacora.Text = Unidad.clave;
                        txtTolva1Bitacora.IsEnabled = false;
                        txtDollyBitacora.Focus();
                    }
                    else
                    {
                        txtTolva1Bitacora.Tag = null;
                        txtTolva1Bitacora.Clear();
                        txtTolva1Bitacora.IsEnabled = true;
                    }
                    break;
                case TipoUnidad.DOLLY:
                    if (Unidad != null)
                    {
                        txtDollyBitacora.Tag = Unidad;
                        txtDollyBitacora.Text = Unidad.clave;
                        txtDollyBitacora.IsEnabled = false;
                        txtTolva2Bitacora.Focus();
                    }
                    else
                    {
                        txtDollyBitacora.Tag = null;
                        txtDollyBitacora.Clear();
                        txtDollyBitacora.IsEnabled = true;
                    }
                    break;
                case TipoUnidad.TOLVA_2:
                    if (Unidad != null)
                    {
                        txtTolva2Bitacora.Tag = Unidad;
                        txtTolva2Bitacora.Text = Unidad.clave;
                        txtTolva2Bitacora.IsEnabled = false;
                        txtOperadorBitacora.Focus();
                    }
                    else
                    {
                        txtTolva2Bitacora.Tag = null;
                        txtTolva2Bitacora.Clear();
                        txtTolva2Bitacora.IsEnabled = true;
                    }
                    break;
                default:
                    break;
            }
        }

        private void txtTolva1Bitacora_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtTractorBitacora.Tag == null)
                {
                    return;
                }

                string clave = txtTolva1Bitacora.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                completarClave(ref clave);
                List<UnidadTransporte> ListUnidadT = buscarRemolques(((UnidadTransporte)txtTractorBitacora.Tag).empresa.clave, clave);
                if (ListUnidadT.Count == 1)
                {
                    mapUnidadesBitacora(TipoUnidad.TOLVA, ListUnidadT[0]);
                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("", ((UnidadTransporte)txtTractorBitacora.Tag).empresa.clave, "");
                    var s = buscardor.buscarRemolques();
                    mapUnidadesBitacora(TipoUnidad.TOLVA, s);
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    mapUnidadesBitacora(TipoUnidad.TOLVA, s);
                }
            }
        }

        private void txtDollyBitacora_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtTractorBitacora.Tag == null)
                {
                    return;
                }
                string clave = txtDollyBitacora.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                completarClave(ref clave);

                List<UnidadTransporte> ListUnidadT = buscarUnidades("DL" + clave, ((UnidadTransporte)txtTractorBitacora.Tag).empresa.clave);
                if (ListUnidadT.Count == 1)
                {
                    mapUnidadesBitacora(TipoUnidad.DOLLY, ListUnidadT[0]);

                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("DL", ((UnidadTransporte)txtTractorBitacora.Tag).empresa.clave, clave);
                    var s = buscardor.buscarUnidades();
                    mapUnidadesBitacora(TipoUnidad.DOLLY, s);
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    mapUnidadesBitacora(TipoUnidad.DOLLY, s);
                }
            }
        }

        private void txtTolva2Bitacora_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtTractorBitacora.Tag == null)
                {
                    return;
                }

                string clave = txtTolva2Bitacora.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                completarClave(ref clave);
                List<UnidadTransporte> ListUnidadT = buscarRemolques(((UnidadTransporte)txtTractorBitacora.Tag).empresa.clave, clave);
                if (ListUnidadT.Count == 1)
                {
                    mapUnidadesBitacora(TipoUnidad.TOLVA_2, ListUnidadT[0]);

                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("", ((UnidadTransporte)txtTractorBitacora.Tag).empresa.clave, "");
                    var s = buscardor.buscarRemolques();
                    mapUnidadesBitacora(TipoUnidad.TOLVA_2, s);
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    mapUnidadesBitacora(TipoUnidad.TOLVA_2, s);
                }
            }
        }

        private void txtOperadorBitacora_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (txtTractorBitacora.Tag == null)
                {
                    return;
                }
                string clave = txtOperadorBitacora.Text.Trim();
                //completarClave(ref clave);
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                List<Personal> listPersonal = buscarPersonal(clave);
                if (listPersonal.Count == 1)
                {
                    mapOperadorBitacora(listPersonal[0]);
                }
                else if (listPersonal.Count == 0)
                {
                    selectPersonal buscardor = new selectPersonal(clave, ((UnidadTransporte)txtTractorBitacora.Tag).empresa.clave);
                    var per = buscardor.buscarPersonal();
                    mapOperadorBitacora(per);
                }
                else
                {
                    selectPersonal buscardor = new selectPersonal(clave, ((UnidadTransporte)txtTractorBitacora.Tag).empresa.clave);
                    var per = buscardor.buscarPersonal();
                    mapOperadorBitacora(per);
                }
            }
        }

        private void btnEntradaBitacora_Click(object sender, RoutedEventArgs e)
        {
            guardarBitacora();
        }

        public void guardarBitacora(bool isSalida = false)
        {
            try
            {
                OperationResult val = validarBitacora();
                if (val.typeResult != ResultTypes.success)
                {
                    imprimirMensaje(val);
                    return;
                }
                BitacoraEntradasSalidas bitacora = mapForm();
                if (bitacora != null)
                {
                    OperationResult result = new BitacoraSvc().saveBitacora(ref bitacora);
                    if (result.typeResult == ResultTypes.success)
                    {
                        if (isSalida)
                        {
                            nuevo();
                        }
                        else
                        {
                            mapFormBitacora(bitacora);
                        }

                    }
                    ImprimirMensaje.imprimir(result);

                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private OperationResult validarBitacora()
        {
            bool resp = true;
            List<String> error = new List<string>();

            if (txtTractorBitacora.Tag == null)
            {
                resp = false;
                error.Add("Elegir el tractor." + Environment.NewLine);
            }

            if (string.IsNullOrEmpty(txtKilometrajeBitacora.Text.Trim()))
            {
                resp = false;
                error.Add("El kilometraje no es Valido." + Environment.NewLine);
            }

            //var val = new RegistroKilometrajeSvc().getUltimoRegistroValido(((UnidadTransporte)txtTractorBitacora.Tag).clave);
            //if (val.typeResult == ResultTypes.success)
            //{
            //    if ((Convert.ToDecimal(txtKilometraje.Text.Trim()) - ((RegistroKilometraje)val.result).kmFinal) >= 10000)
            //    {
            //        resp = false;
            //        error.Add("El kilometraje no es Valido." + Environment.NewLine);
            //    }
            //    if (((RegistroKilometraje)val.result).kmFinal > Convert.ToDecimal(txtKilometraje.Text.Trim()))
            //    {
            //        resp = false;
            //        error.Add("El kilometraje no es Valido por que es menor al ultimo KM." + Environment.NewLine);
            //    }
            //}

            //if (txtTolva1Bitacora.Tag == null)
            //{
            //    resp = false;
            //    error.Add("Elegir el remolque 1." + Environment.NewLine);
            //}

            if (txtOperadorBitacora.Tag == null)
            {
                resp = false;
                error.Add("Elegir el chofer." + Environment.NewLine);
            }


            if (txtDollyBitacora.Tag != null)
            {
                if (txtTolva2Bitacora.Tag == null)
                {
                    resp = false;
                    error.Add("Seleccionar el remolque 2." + Environment.NewLine);
                }
            }

            if (txtTolva2Bitacora.Tag != null)
            {
                if (txtDollyBitacora.Tag == null)
                {
                    resp = false;
                    error.Add("Seleccionar Dolly." + Environment.NewLine);
                }
            }

            string cadena = "Para continuar se requiere corregir lo siguiente:" + Environment.NewLine;
            foreach (var item in error)
            {
                cadena += item;
            }
            return new OperationResult { valor = resp ? 0 : 2, result = resp, mensaje = cadena };
        }

        private BitacoraEntradasSalidas mapForm()
        {
            try
            {
                return new BitacoraEntradasSalidas
                {
                    idBitacora = itemBitacoras.Tag == null ? 0 : ((BitacoraEntradasSalidas)itemBitacoras.Tag).idBitacora,
                    tractor = (UnidadTransporte)txtTractorBitacora.Tag,
                    tolva1 = (UnidadTransporte)txtTolva1Bitacora.Tag,
                    dolly = (UnidadTransporte)txtDollyBitacora.Tag,
                    tolva2 = (UnidadTransporte)txtTolva2Bitacora.Tag,
                    chofer = (Personal)txtOperadorBitacora.Tag,
                    listaMotivos = getMotivos(),
                    Kilometraje = Convert.ToDecimal(txtKilometrajeBitacora.Text),
                    usuario = mainWindow.inicio._usuario.nombreUsuario
                };
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }

        private List<MotivoEntrada> getMotivos()
        {
            try
            {
                List<MotivoEntrada> list = new List<MotivoEntrada>();
                foreach (MotivoEntrada item in cbxMotivosBitacora.SelectedItems)
                {
                    list.Add(item);
                }
                return list;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }

        private void btnSalidaBitacora_Click(object sender, RoutedEventArgs e)
        {
            guardarBitacora(true);
        }
    }
}
