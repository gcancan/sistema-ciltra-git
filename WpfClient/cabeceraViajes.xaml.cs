﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using Core.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class cabeceraViajes : UserControl
    {
        public cabeceraViajes()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var resp = new ClienteSvc().getClientesALLorById(1);
            if (resp.typeResult != ResultTypes.success)
            {
                imprimeMensaje(resp);
                nuevo();
                return;
            }
            List<Cliente> listCliente = (List<Cliente>)resp.result;
        }

        

        public void nuevo()
        {
            mapControl(null);
        }

        private void imprimeMensaje(OperationResult resp)
        {
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    break;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    break;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    break;
                case ResultTypes.recordNotFound:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    break;
                default:
                    break;
            }
        }

        private void txtFolio_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (e.Key == Key.Enter)
                {
                    string[] list = txtFolio.Text.Trim().Split('-');
                    int i = 0;
                    if (!int.TryParse(list[0].Trim(), out i))
                    {
                        MessageBox.Show("Al parecer el Folio Proporcionado no es valido", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    buscar(i);             
                }
            }
            catch (Exception ex)
            {
                imprimeMensaje(new OperationResult { valor = 2, mensaje = ex.Message });
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }            
        }
        private int _idViaje
        {
            get
            {
                return Convert.ToInt32(txtFolio.Text.Trim());
            }
        }
        public void buscar(int idViaje = 0)
        {
            try
            {
                if (idViaje == 0)
                {
                    idViaje = _idViaje;
                }
                Cursor = Cursors.Wait;
                var resp = new CartaPorteSvc().getCartaPorteById(idViaje);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapControl(((List<Viaje>)resp.result)[0]);
                }
                else
                {
                    imprimeMensaje(resp);
                    mapControl(null);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                mapControl(null);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private Viaje _viaje { get; set; }
        public Viaje viaje
        {
            get
            {
                return _viaje;
            }
            set
            {
                _viaje = value;
            }
        }
        public void mapControl(Viaje viaje)
        {
            if (viaje != null)
            {
                txtFolio.Tag = viaje;
                txtEstatus.Text = viaje.EstatusGuia;
                txtFolio.Text = viaje.NumGuiaId.ToString();
                txtFecha.Value = viaje.FechaHoraViaje;
                txtTractor.Text = viaje.tractor.clave;
                txtTolva1.Text = viaje.remolque?.clave;
                txtDolly.Text = viaje.dolly == null ? string.Empty : viaje.dolly.clave;
                txtTolva2.Text = viaje.remolque2 == null ? string.Empty : viaje.remolque2.clave;
                txtChofer.Text = viaje.operador.nombre;
                txtChoferAux.Text = viaje.operadorCapacitacion == null ? string.Empty : viaje.operadorCapacitacion.nombre;
                txtKM.Text = viaje.km.ToString();
                selectComboCliente(viaje.cliente);
                this.IsEnabled = false;                
                this.viaje = viaje;
                this.DataContext = viaje;
            }
            else
            {
                txtFolio.Tag = null;
                txtFolio.Clear();
                txtEstatus.Clear();
                txtFecha.Text = string.Empty;
                txtTractor.Clear();
                txtTolva1.Clear();
                txtDolly.Clear();
                txtTolva2.Clear();
                txtChofer.Clear();
                txtKM.Clear();
                cbxClientes.Clear();
                this.IsEnabled = true;                
                this.viaje = null;
                this.DataContext = new object();
            }
        }

        private void selectComboCliente(Cliente cliente)
        {
            if (cliente != null)
            {
                cbxClientes.Text = cliente.nombre;
                cbxClientes.Tag = cliente;
            }
            else
            {
                cbxClientes.Clear();
                cbxClientes.Tag = null;
            }
        }
    }
}
