﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using CoreFletera.Interfaces;
    using CoreFletera.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Markup;
    using Xceed.Wpf.Toolkit;

    public partial class reporteConsumoUnidades : WpfClient.ViewBase
    {
        private List<DetallesConsumoUnidades> listaDetalles = new List<DetallesConsumoUnidades>();       
        public reporteConsumoUnidades()
        {
            this.InitializeComponent();
        }
        private void btnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                new ReportView().ReporteBitacoraConsumoUnidades(this.listaDetalles, this.ctrFechas.fechaInicial, this.ctrFechas.fechaFinal, this.ctrFechas.isMes, this.ctrEmpresa.empresaSelected);
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.listaDetalles = new List<DetallesConsumoUnidades>();
            this.lvlDetalles.ItemsSource = null;
            try
            {
                base.Cursor = Cursors.Wait;
                List<string> listaTractores = this.getLista();
                if (listaTractores.Count == 0)
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "Se requiere seleccionar minimo un tractor"
                    };
                    ImprimirMensaje.imprimir(resp); 
                }
                else
                {
                    OperationResult resp = new DetallesConsumoUnidadesSvc().getDetallesConsumoUnidades(this.ctrEmpresa.empresaSelected.clave, ((Cliente)this.cbxCliente.SelectedItem).clave, this.ctrFechas.fechaInicial, this.ctrFechas.fechaFinal, listaTractores, true);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        this.listaDetalles = (List<DetallesConsumoUnidades>)resp.result;
                        List<ListViewItem> list2 = new List<ListViewItem>();
                        foreach (DetallesConsumoUnidades unidades in this.listaDetalles)
                        {
                            ListViewItem item = new ListViewItem
                            {
                                Content = unidades
                            };
                            ContextMenu menu = new ContextMenu();
                            if (unidades.diesel > decimal.Zero)
                            {
                                MenuItem newItem = new MenuItem
                                {
                                    Header = "Ver Bitacora Cargas Combustible"
                                };
                                newItem.Tag = unidades.claveUnidad;
                                newItem.Click += new RoutedEventHandler(this.MenuReporteCargas_Click);
                                menu.Items.Add(newItem);
                            }
                            if (unidades.viajes > decimal.Zero)
                            {
                                MenuItem newItem = new MenuItem
                                {
                                    Header = "Ver Bitacora Viajes"
                                };
                                newItem.Tag = unidades.claveUnidad;
                                newItem.Click += new RoutedEventHandler(this.MenuViajes_Click);
                                menu.Items.Add(newItem);
                            }
                            if (unidades.lavadero > decimal.Zero)
                            {
                                MenuItem newItem = new MenuItem
                                {
                                    Header = "Ver Bitacora Lavadero"
                                };
                                newItem.Tag = unidades.claveUnidad;
                                newItem.Click += new RoutedEventHandler(this.MenuLavadero_Click);
                                menu.Items.Add(newItem);
                            }
                            item.ContextMenu = menu;
                            list2.Add(item);
                        }
                        this.lvlDetalles.ItemsSource = list2;
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.ctrEmpresa.empresaSelected != null)
            {
                OperationResult resp = new UnidadTransporteSvc().getTractores(this.ctrEmpresa.empresaSelected.clave, "%");
                if (resp.typeResult == ResultTypes.success)
                {
                    this.cbxListaUnidades.ItemsSource = (List<UnidadTransporte>)resp.result;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
                OperationResult result2 = new ClienteSvc().getClientesALLorById(this.ctrEmpresa.empresaSelected.clave, 0, true);
                if (result2.typeResult == ResultTypes.success)
                {
                    this.mapComboClientes(result2.result as List<Cliente>);
                }
                else if (result2.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(result2);
                }
            }
        }

        private void chBoxTodos_Checked(object sender, RoutedEventArgs e)
        {
            foreach (UnidadTransporte transporte in (IEnumerable)this.cbxListaUnidades.Items)
            {
                this.cbxListaUnidades.SelectedItems.Add(transporte);
            }
            this.cbxListaUnidades.IsEnabled = false;
        }

        private void chBoxTodos_Unchecked(object sender, RoutedEventArgs e)
        {
            this.cbxListaUnidades.SelectedItems.Clear();
            this.cbxListaUnidades.IsEnabled = true;
        }

        private List<string> getLista()
        {
            List<string> list = new List<string>();
            foreach (UnidadTransporte transporte in this.cbxListaUnidades.SelectedItems)
            {
                list.Add(transporte.clave);
            }
            return list;
        }
        private void mapComboClientes(List<Cliente> list)
        {
            this.cbxCliente.Items.Clear();
            this.cbxCliente.DisplayMemberPath = "nombre";
            if (list.Count > 0)
            {
                Cliente newItem = new Cliente
                {
                    clave = 0,
                    nombre = "TODOS"
                };
                this.cbxCliente.Items.Add(newItem);
                foreach (Cliente cliente in list)
                {
                    this.cbxCliente.Items.Add(cliente);
                }
                this.cbxCliente.SelectedIndex = 0;
            }
        }

        private void MenuLavadero_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = (MenuItem)sender;
            string tag = (string)item.Tag;
            new ventanaGeneral(this.ctrEmpresa.empresaSelected, this.ctrFechas.fechaInicial, this.ctrFechas.fechaFinal, tag).abrirBitacoraLavadero();
        }

        private void MenuReporteCargas_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = (MenuItem)sender;
            string tag = (string)item.Tag;
            new ventanaGeneral(base.mainWindow, this.ctrEmpresa.empresaSelected, this.ctrFechas.fechaInicial, this.ctrFechas.fechaFinal, tag, 0).abrirCargasCombustible();
        }

        private void MenuViajes_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = (MenuItem)sender;
            string tag = (string)item.Tag;
            new ventanaGeneral(base.mainWindow, this.ctrEmpresa.empresaSelected, this.ctrFechas.fechaInicial, this.ctrFechas.fechaFinal, tag, ((Cliente)this.cbxCliente.SelectedItem).clave).abrirViajes();
        }

        public override void nuevo()
        {
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
            if (base.mainWindow != null)
            {
                this.cbxListaUnidades.SelectedItems.Clear();
                this.lvlDetalles.ItemsSource = null;
                this.chBoxTodos.IsChecked = false;
            }
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.ctrEmpresa.cbxEmpresa.SelectionChanged += new SelectionChangedEventHandler(this.CbxEmpresa_SelectionChanged);
            this.ctrEmpresa.loaded(base.mainWindow.empresa, true);
            this.nuevo();
        }
    }
}
