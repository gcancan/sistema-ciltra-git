﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using Core.Interfaces;
using System.Deployment.Application;
using Core.Utils;
using System.Diagnostics;
using System.Reflection;
using System.Security.Principal;
using MahApps.Metro.Controls;
using System.Net.NetworkInformation;
using CoreFletera.Models;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for Inicio.xaml
    /// </summary>
    public partial class Inicio : MetroWindow
    {
        public Usuario _usuario = null;
        MainWindow mainWin;
        public string nombreEquipo = string.Empty;

        public Inicio()
        {            
            InitializeComponent();
        }

        public void InstallUpdateSyncWithInfo()
        {
            try
            {
                UpdateCheckInfo info = null;
                if (ApplicationDeployment.IsNetworkDeployed)
                {
                    ApplicationDeployment AD = ApplicationDeployment.CurrentDeployment;
                    try
                    {
                        info = AD.CheckForDetailedUpdate();
                    }
                    catch (Exception ex)
                    {
                        ImprimirMensaje.imprimir(ex);
                    }
                    if (info.UpdateAvailable)
                    {
                        try
                        {
                            AD.Update();
                            
                        }
                        catch (Exception ex)
                        {
                            ImprimirMensaje.imprimir(ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //if (!ValidaInicioAdministrador())
            //{
            //    //MessageBox.Show("Sorry, this application must be run as Administrator.");
            //    //this.Close();
            //}
            string version = string.Empty;
            try
            {
                version = " Compilación " + ApplicationDeployment.CurrentDeployment.CurrentVersion.ToString();
            }
            catch (Exception ex)
            {
                version = string.Empty;
            }
            Title = "Inicio de Sesión" + version;

            IPGlobalProperties computerProperties = IPGlobalProperties.GetIPGlobalProperties();
            nombreEquipo = computerProperties.HostName;

            txtUsuario.Text = new Util().getUltimoUsuario();
            if (!string.IsNullOrEmpty(txtUsuario.Text))
                txtPass.Focus();
            else
                txtUsuario.Focus();
        }

        private bool ValidaInicioAdministrador()
        {
            if (!IsRunAsAdministrator())
            {
                var processInfo = new ProcessStartInfo(Assembly.GetExecutingAssembly().CodeBase);
                processInfo.UseShellExecute = true;
                processInfo.Verb = "runas";
                try
                {
                    Process.Start(processInfo);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Sorry, this application must be run as Administrator.");
                }
                Application.Current.Shutdown();
                return false;
            }
            else
            {
                return true;
            }
        }
        private bool IsRunAsAdministrator()
        {
            var wi = WindowsIdentity.GetCurrent();
            var wp = new WindowsPrincipal(wi);
            return wp.IsInRole(WindowsBuiltInRole.Administrator);
        }
        private void btnIniciar_Click(object sender, RoutedEventArgs e)
        {
            buscarUsuario();
        }

        private void buscarUsuario()
        {
            if (string.IsNullOrEmpty(txtUsuario.Text.Trim()) || string.IsNullOrEmpty(txtPass.Password.Trim()))
            {
                MessageBox.Show("Proporcione el nombre de usuario y contraseña", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            OperationResult serv = new UsuarioSvc().getUserByUserPass(txtUsuario.Text.Trim(), txtPass.Password.Trim());

            if (serv.typeResult == ResultTypes.error)
            {
                MessageBox.Show(serv.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (serv.typeResult == ResultTypes.warning)
            {
                MessageBox.Show(serv.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            if (serv.typeResult == ResultTypes.recordNotFound)
            {
                MessageBox.Show(serv.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            if (serv.typeResult == ResultTypes.success)
            {
                try
                {                   

                    _usuario = (Usuario)serv.result;
                    new Util().setUltimoUsuario(_usuario.nombreUsuario);

                    BitacoraAcceso bitacoraAcceso = new BitacoraAcceso
                    {
                        idBitacoraAcceso = 0,
                        acceso = "INICIO_SESION",
                        fecha = DateTime.Now,
                        usuario = _usuario.nombreUsuario,
                        contraseña = _usuario.contrasena,
                        nombreEquipo = this.nombreEquipo
                    };
                    if (_usuario.cliente == null)
                    {
                        _usuario.cliente = new Cliente
                        {
                            clave = 0,
                            nombre = "SIN CLIENTE",
                            alias = "SIN CLIENTE",
                            
                        };
                    }

                    var resp = new BitacoraAccesoSvc().saveBitacoraAcceso(ref bitacoraAcceso);
                    if (resp.typeResult != ResultTypes.success)
                    {
                        ImprimirMensaje.imprimir(resp);
                    }

                    mainWin = new MainWindow(this);
                    mainWin.Closed += MainWin_Closed;
                    this.Hide();
                    mainWin.Owner = this;
                    try
                    {
                        mainWin.Show();
                    }
                    catch (Exception ex)
                    {
                        ImprimirMensaje.imprimir(ex);
                    }
                    finally
                    {
                        GC.WaitForPendingFinalizers();
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        GC.Collect();
                    }                    
                    
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Error");
                }
                finally
                {
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    GC.Collect();
                }
            }
        }

        private void MainWin_Closed(object sender, EventArgs e)
        {
            try
            {
                //mainWin.timer.Stop();
                mainWin = null;
                //this.Close();
                this.Visibility = Visibility.Visible;
                //txtUsuario.Clear();
                txtPass.Clear();
                txtPass.Focus();
                //mainWin = null;
                //txtUsuario.Focus();
            }
            catch (Exception ex)
            {
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }           
        }

        private void btnConfig_Click(object sender, RoutedEventArgs e)
        {
            //txtPass.PasswordRevealMode = null;
            editConfig config = new editConfig();
            //this.Hide();
            config.ShowDialog();
        }

        private void txtPass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                buscarUsuario();
            }
        }

        private void txtPass_LostFocus(object sender, RoutedEventArgs e)
        {
            //buscarUsuario();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                //mainWin.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
            finally
            {
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        private void btnActualizar_Click(object sender, RoutedEventArgs e)
        {
            //InstallUpdateSyncWithInfo();
            try
            {
                string startupPath = System.Windows.Forms.Application.StartupPath;
                startupPath += @"\Recursos\Sistema CILTRA.application";
                Process.Start(startupPath);
                this.Close();
            }
            catch (Exception ex)
            {
                
            }
        }
    }
}
