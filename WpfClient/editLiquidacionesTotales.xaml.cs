﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editLiquidacionesTotales.xaml
    /// </summary>
    public partial class editLiquidacionesTotales : ViewBase
    {
        public editLiquidacionesTotales()
        {
            InitializeComponent();
        }
        List<ConfiguracionClienteLiquidacion> listaConfig = new List<ConfiguracionClienteLiquidacion>();
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            //dtpFecha.SelectedDate = DateTime.Now;
            nuevo();
            var resp = new LiquidacionPegasoSvc().getAllConfigCliente();
            if (resp.typeResult == ResultTypes.success )
            {
                listaConfig = resp.result as List<ConfiguracionClienteLiquidacion>;
            }
            lvlPagosDobles.ItemsSource = listaConfig;
        }
        public override void nuevo()
        {
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            mapLvl(new List<LiquidacionPegaso>());
        }
        List<LiquidacionPegaso> listaLiquidacion = new List<LiquidacionPegaso>();
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            Task.Factory.StartNew(() =>
            {
                iniciarBusqueda();
            });

        }

        public void iniciarBusqueda()
        {
            Dispatcher.BeginInvoke((Action)(() =>
                   {
                       try
                       {
                           Cursor = Cursors.Wait;

                           listaLiquidacion = new List<LiquidacionPegaso>();

                           var r = new OperadorSvc().getOperadoresViaje(ctrFechas.fechaInicial, ctrFechas.fechaFinal);
                           if (r.typeResult == ResultTypes.success)
                           {
                               foreach (Personal chofer in r.result as List<Personal>)
                               {
                                   // Dispatcher.Invoke(new Action(() =>
                                   //{
                                   Thread.Sleep(1000);
                                   lblMensaje.Content = string.Format("Calculando Liquidación de: {0}", chofer.nombre.ToUpper());
                                //}));
                                buscarLiquidaciones(chofer);
                               }
                           }

                           else
                           {
                               ImprimirMensaje.imprimir(r);
                           }

                       }
                       catch (Exception ex)
                       {
                           ImprimirMensaje.imprimir(ex);
                       }
                       finally
                       {
                           Cursor = Cursors.Arrow;
                           mapLvl(listaLiquidacion);
                       }
                   }));
        }

        public void buscarLiquidaciones(Personal chofer)
        {

            try
            {
                var respV = new CartaPorteSvc().getViajesOperador(chofer.clave, ctrFechas.fechaInicial, ctrFechas.fechaFinal);

                SueldoFijo sueldo = null;

                if (respV.typeResult == ResultTypes.success)
                {
                    var respSueldo = new SueldoFijoSvc().getAllSueldoFijoOrByIdOperador(chofer.clave.ToString());
                    if (respSueldo.typeResult == ResultTypes.success)
                    {
                        sueldo = respSueldo.result as SueldoFijo;
                    }
                    else if (respSueldo.typeResult != ResultTypes.recordNotFound)
                    {
                        ImprimirMensaje.imprimir(respSueldo);
                        return;
                    }

                    var listaDetalles = mapLista2(respV.result as List<Viaje>);
                    LiquidacionPegaso liquidacion = new LiquidacionPegaso
                    {
                        idLiquidacion = 0,
                        fecha = DateTime.Now,
                        fechaInicio = ctrFechas.fechaInicial,
                        fechaFin = ctrFechas.fechaFinal.AddDays(-1),
                        operador = chofer,
                        sueldoFijoObj = sueldo,
                        realiza = mainWindow.inicio._usuario.nombreUsuario
                    };
                    liquidacion.listDetalles = new List<DetalleLiquidacion_old>();
                    liquidacion.listDetalles = listaDetalles;
                    listaLiquidacion.Add(liquidacion);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }

        }

        private void mapLvl(List<LiquidacionPegaso> listaLiquidacion)
        {
            lvlLiquidaciones.Items.Clear();
            foreach (var item in listaLiquidacion)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;

                //ContextMenu menu = new ContextMenu();
                //MenuItem itemMenu = new MenuItem { Header = "Cambiar", Tag = item };
                //itemMenu.Click += ItemMenu_Click;
                //menu.Items.Add(itemMenu);
                //lvl.ContextMenu = menu;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick;

                lvlLiquidaciones.Items.Add(lvl);
            }
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            LiquidacionPegaso liquidacion = (LiquidacionPegaso)(sender as ListViewItem).Content;
            MainWindow main = new MainWindow(mainWindow);
            var op = main.abrirVentanaLiquidacion(liquidacion);
            if (op != null)
            {
                if (op.typeResult == ResultTypes.success)
                {
                    int i = 0;
                    foreach (var item in listaLiquidacion)
                    {
                        if (item == liquidacion)
                        {
                            break;
                        }
                        i++;
                    }
                    listaLiquidacion[i] = op.result as LiquidacionPegaso;
                    mapLvl(listaLiquidacion);
                }
            }
        }

        private void ItemMenu_Click(object sender, RoutedEventArgs e)
        {
            LiquidacionPegaso liquidacion = (LiquidacionPegaso)(sender as MenuItem).Tag;
            MainWindow main = new MainWindow(mainWindow);
            var op = main.abrirVentanaLiquidacion(liquidacion);
            if (op != null)
            {
                if (op.typeResult == ResultTypes.success)
                {
                    int i = 0;
                    foreach (var item in listaLiquidacion)
                    {
                        if (item == liquidacion)
                        {
                            break;
                        }
                        i++;
                    }
                    listaLiquidacion[i] = op.result as LiquidacionPegaso;
                    mapLvl(listaLiquidacion);
                }
            }
        }

        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                List<LiquidacionPegaso> listaNueva = new List<LiquidacionPegaso>();
                OperationResult resp = new OperationResult();
                foreach (LiquidacionPegaso item in listaLiquidacion)
                {
                    LiquidacionPegaso nuevoLic = item;
                    resp = new LiquidacionPegasoSvc().saveLiquidacionPegaso(ref nuevoLic);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listaNueva.Add(nuevoLic);
                    }
                    else
                    {
                        return resp;
                    }
                }
                mainWindow.habilitar = true;
                mapLvl(listaNueva);
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        void actulizar(LiquidacionPegaso liquidacion)
        {

        }

        private List<DetalleLiquidacion_old> mapLista2(List<Viaje> listViaje)
        {
            
            List<DetalleLiquidacion_old> lista = new List<DetalleLiquidacion_old>();
            foreach (Viaje viaje in listViaje)
            {
                var config = listaConfig.Find(s => s.cliente.clave == viaje.cliente.clave);
                DetalleLiquidacion_old det = new DetalleLiquidacion_old
                {
                    config = config,
                    idDetalleLiquidacion = 0,
                    viaje = viaje
                };
                lista.Add(det);
            }
            return lista;

        }
    }
}
