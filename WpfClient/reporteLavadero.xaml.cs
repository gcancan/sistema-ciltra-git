﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for reporteLavadero.xaml
    /// </summary>
    public partial class reporteLavadero : ViewBase
    {
        private DateTime fechaInicial;
        private Empresa empresa;
        private DateTime fechaFinal;
        private string tractor;
        public reporteLavadero()
        {            
            InitializeComponent();
        }

        public reporteLavadero(Empresa empresa, string tractor, DateTime fechaInicial, DateTime fechaFinal)
        {
            this.empresa = empresa;
            this.fechaInicial = fechaInicial;
            this.fechaFinal = fechaFinal;
            this.tractor = tractor;
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged; ;
            ctrEmpresa.loaded(mainWindow == null ? empresa : mainWindow.empresa, true);

            if (mainWindow == null)
            {
                controlBusquedas.IsEnabled = false;
                ctrFechas.mostrarFechas(fechaInicial, fechaFinal);
                buscarBitacoraLavadero();
            }
            else
            {
                
            }
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ctrEmpresa.empresaSelected != null)
            {
                OperationResult resp = new UnidadTransporteSvc().getTractores(ctrEmpresa.empresaSelected.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxListaUnidades.ItemsSource = (List<UnidadTransporte>)resp.result;
                    int i = 0;
                    foreach (UnidadTransporte item in cbxListaUnidades.Items)
                    {
                        //TC378
                        if (((UnidadTransporte)item).clave == tractor)
                        {
                            cbxListaUnidades.SelectedItems.Add(cbxListaUnidades.Items[i]);
                            return;
                        }
                        i++;
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {

        }

        public void buscarBitacoraLavadero()
        {
            try
            {
                Cursor = Cursors.Wait;

                List<string> listaUnidades = getListaUnidades();
                if (listaUnidades.Count == 0)
                {
                    ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "Se requiere elegir minimo un tractor" });
                    return;
                }

                var resp = new BitacoraLavaderoSvc().getBitacoraLavadero(ctrEmpresa.empresaSelected.clave, ctrFechas.fechaInicial, ctrFechas.fechaFinal, listaUnidades);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapLista(resp.result as List<BitacoraLavadero>);
                }

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapLista(List<BitacoraLavadero> list)
        {
            lvlDetalles.Items.Clear();
            foreach (BitacoraLavadero lavadero in list)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = lavadero;
                ContextMenu menu = new ContextMenu();
                if (lavadero.estatus == "TERMINADO" || lavadero.estatus == "ENTREGADO")
                {
                    MenuItem itemLavadero = new MenuItem { Header = "Ver Detalle" , Tag = lavadero};
                    itemLavadero.Click += ItemLavadero_Click;
                    menu.Items.Add(itemLavadero);
                }
                lvl.ContextMenu = menu;
                lvlDetalles.Items.Add(lvl);
            }
        }

        private void ItemLavadero_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            BitacoraLavadero bitacora = item.Tag as BitacoraLavadero;
            ventanaGeneral ventana = new ventanaGeneral(bitacora);
            ventana.abrirDetalleLavadero();
        }

        private List<string> getListaUnidades()
        {
            List<string> lista = new List<string>();
            foreach (UnidadTransporte item in cbxListaUnidades.SelectedItems)
            {
                lista.Add(item.clave);
            }
            return lista;
        }
    }
}
