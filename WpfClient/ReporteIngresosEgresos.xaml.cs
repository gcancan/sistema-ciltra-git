﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoreFletera.Interfaces;
using CoreFletera.Models;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for ReporteIngresosEgresos.xaml
    /// </summary>
    public partial class ReporteIngresosEgresos : ViewBase
    {
        public ReporteIngresosEgresos()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {            
            nuevo();
            ctrEmpresas.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresas.loaded(mainWindow.empresa, true);
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ctrEmpresas.empresaSelected != null)
            {
                OperationResult resp = new UnidadTransporteSvc().getTractores(ctrEmpresas.empresaSelected.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxListaUnidades.ItemsSource = (List<UnidadTransporte>)resp.result;
                    ctrUnidad.loaded(etipoUniadBusqueda.TRACTOR, ctrEmpresas.empresaSelected);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
        }

        public override void nuevo()
        {
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo);
            cbxListaUnidades.SelectedItems.Clear();
            ctrUnidad.limpiar();
        }

                
        private void btnImprimir_Click(object sender, RoutedEventArgs e)
        {           
            try
            {
                Cursor = Cursors.Wait;
                var respReporte = new ReportView();
                if (itemAgrupado.IsSelected)
                {
                    #region reporteConcentrado
                    
                    OperationResult resp = new IngresosEgresosSvc().getIngresosEgresos(ctrFechas.fechaInicial, ctrFechas.fechaFinal, ctrEmpresas.empresaSelected.clave, getLista());
                    if (resp.typeResult == ResultTypes.success)
                    {
                        if (respReporte.reporteIngresosEgresos((List<IngresosEgresos>)resp.result, ctrEmpresas.empresaSelected, ctrFechas.fechaInicial, ctrFechas.fechaFinal, ctrFechas.isMes))
                        {
                            respReporte.ShowDialog();
                            nuevo();
                        }
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                    #endregion
                }
                else
                {
                    #region reporteDetallado
                    if (ctrUnidad.unidadTransporte == null)
                    {
                        ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "Se Requiere de la unidad de transporte para continuar" });
                        return;
                    }
                    OperationResult respInDet = new IngresosEgresosSvc().getIngresosDetalles(ctrUnidad.unidadTransporte.clave, ctrFechas.fechaInicial, ctrFechas.fechaFinal);
                    if (respInDet.typeResult == ResultTypes.error)
                    {
                        ImprimirMensaje.imprimir(respInDet);
                        return;
                    }
                    OperationResult respEgDet = new IngresosEgresosSvc().getEgresosDetallado(ctrUnidad.unidadTransporte.clave, ctrFechas.fechaInicial, ctrFechas.fechaFinal);
                    if (respEgDet.typeResult == ResultTypes.error)
                    {
                        ImprimirMensaje.imprimir(respEgDet);
                        return;
                    }
                    if (respReporte.reporteIngresosEgresosDetallado(ctrUnidad.unidadTransporte, (List<IngresosDetalle>)respInDet.result, (List<EgresosDetalle>)respEgDet.result, ctrFechas.fechaInicial, ctrFechas.fechaFinal, ctrFechas.isMes))
                    {
                        respReporte.ShowDialog();
                        nuevo();
                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private List<string> getLista()
        {
            try
            {
                List<string> list = new List<string>();
                if (chBoxTodos.IsChecked.Value)
                {
                    return list;
                }
                else
                {
                    foreach (UnidadTransporte item in cbxListaUnidades.SelectedItems)
                    {
                        list.Add(item.clave);
                    }
                    return list;
                }
                
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
