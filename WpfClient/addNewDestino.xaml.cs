﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using Core.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for addNewDestino.xaml
    /// </summary>
    public partial class addNewDestino : Window
    {
        Cliente cliente = new Cliente();
        Zona zona;
        public addNewDestino()
        {
            InitializeComponent();
        }
        public addNewDestino(Cliente cliente)
        {
            InitializeComponent();
            this.cliente = cliente;
        }

        public Zona addNewZona()
        {
            bool? sResult = ShowDialog();
            if (sResult.Value && zona != null)
            {
                return this.zona;
            }
            return null;
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            Zona zona = mapForm();
            OperationResult resp = new ZonasSvc().saveRutasDestino(ref zona);

            if (resp.typeResult == ResultTypes.success)
            {
                this.zona = (Zona)resp.result;
                EnvioCorreo sent = new EnvioCorreo();
                sent.envioNotificacionGranjaNueva(((Zona)resp.result).descripcion, "admin", cliente.nombre);
                DialogResult = true;
            }
            else
            {
                MessageBox.Show(resp.mensaje);
            }
        }

        private Zona mapForm()
        {
            return new Zona
            {
                descripcion = txtNombreDestino.Text.Trim(),
                clave_empresa = cliente.empresa.clave,
                claveCliente = cliente.clave
            };
        }
    }
}
