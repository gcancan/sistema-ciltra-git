﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for selectProductosCOM.xaml
    /// </summary>
    public partial class selectProductosCOM : Window
    {
        string parametro = string.Empty;
        public selectProductosCOM()
        {
            InitializeComponent();
        }
        public selectProductosCOM(string parametros)
        {
            InitializeComponent();
            this.parametro = parametros;
        }
        AreaTrabajo area = AreaTrabajo.TOTAS;
        public List<ProductosCOM> consultasTaller(AreaTrabajo area, bool validarEistencia = true)
        {
            try
            {
                GridView v = (GridView)lvlProductos.View;
                var x = v.Columns;
                v.Columns.Add(new GridViewColumn { Header = "Existencia", Width = 100, DisplayMemberBinding = new Binding("existencia") { StringFormat = "N2" } });
                lvlProductos.Width = 600;
                this.area = area;
                buscarConsultasTaller(this.area);
                bool? resp = ShowDialog();
                List<ProductosCOM> lista = getListaProductos();
                if (resp.Value && lista.Count > 0)
                {
                    return lista;
                    ProductosCOM producto = (ProductosCOM)((ListViewItem)lvlProductos.SelectedItem).Content;
                    //if (validarEistencia)
                    //{
                    //    //var respConsulta = new ConsultasTallerSvc().consultarExistenByProducto(producto.idProducto);
                    //    //if (respConsulta.typeResult == ResultTypes.success)
                    //    //{
                    //        decimal existencia = producto.existencia;
                    //        if (existencia == 0)
                    //        {
                    //            imprimir(new OperationResult { valor = 2, mensaje = "Este Producto no tiene existencias" });
                    //            return null;
                    //        }
                    //        else
                    //        {
                    //            producto.existencia = existencia;
                    //            return producto;
                    //        }
                    //    //}
                    //    //else
                    //    //{
                    //    //    imprimir(respConsulta);
                    //    //    return null;
                    //    //}
                    //}
                    //else
                    //{
                    //    return producto;
                    //}
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }
        public List<ProductosCOM> consultasTallerSinExistencias(AreaTrabajo area)
        {
            try
            {
                chaTipoBusqueda.Visibility = Visibility.Collapsed;
                this.area = area;
                buscarConsultasTaller(this.area, false);
                bool? resp = ShowDialog();
                List<ProductosCOM> lista = getListaProductos();
                if (resp.Value && lista.Count > 0)
                {
                    return lista;                    
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }

        private void buscarConsultasTaller(AreaTrabajo area, bool soloExistencias = true)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new ConsultasTallerSvc().getProductosCOM(area, "%", soloExistencias);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapLista(resp.result as List<ProductosCOM>);
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //getAllProductos();
            txtFind.Text = parametro;
            lvlProductos.Focus();
            load = true;
        }
        public ProductosCOM getProductos()
        {
            try
            {
                getAllProductos();
                bool? resp = ShowDialog();

                if (resp.Value && lvlProductos.SelectedItem != null)
                {
                    return (ProductosCOM)((ListViewItem)lvlProductos.SelectedItem).Content;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }
        private void getAllProductos()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new ProductosCOMSvc().getInsumosCOM();
                if (resp.typeResult == ResultTypes.success)
                {
                    mapLista((List<ProductosCOM>)resp.result);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapLista(List<ProductosCOM> result)
        {
            try
            {
                lvlProductos.ItemsSource = null; //.Clear();
                List<ListViewItem> listLvl = new List<ListViewItem>();
                foreach (var item in result)
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = item;
                    lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                    listLvl.Add(lvl);
                }
                lvlProductos.ItemsSource = listLvl;
                CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlProductos.ItemsSource);
                view.Filter = UserFilter;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((ProductosCOM)(item as ListViewItem).Content).codigo.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((ProductosCOM)(item as ListViewItem).Content).nombre.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }
        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlProductos.ItemsSource).Refresh();
        }
        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //DialogResult = true;
            ListViewItem lvl = (ListViewItem)sender;
            ProductosCOM productos = lvl.Content as ProductosCOM;
            añadirProducto(productos);
        }
        private void añadirProducto(ProductosCOM insumo)
        {
            try
            {
                var ins = getListaProductos().Find(x => x.idProducto == insumo.idProducto);
                if (ins != null)
                {
                    return;
                }

                StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal, Tag = insumo };
                Label lblProducto = new Label
                {
                    Content = stpContent,
                    Tag = insumo
                };
                #region contenido
                Label lblNombreProducto = new Label
                {
                    Content = insumo.nombreExistencia,
                    Width = 350,
                    Tag = insumo,
                    Foreground = insumo.existencia <= 0 ? Brushes.Red : Brushes.Black,
                    Margin = new Thickness(2)
                };
                stpContent.Children.Add(lblNombreProducto);

                controlDecimal ctrDecimal = new controlDecimal { Tag = insumo, valor = insumo.cantidad };
                ctrDecimal.txtDecimal.Tag = insumo;
                ctrDecimal.txtDecimal.TextChanged += TxtDecimal_TextChanged; ;
                //ctrDecimal.DataContextChanged += CtrDecimal_DataContextChanged;

                stpContent.Children.Add(ctrDecimal);

                Button btnQuitar = new Button
                {
                    Content = "Quitar",
                    Tag = insumo,
                    Foreground = Brushes.White,
                    Background = Brushes.Orange,
                    Margin = new Thickness(2),
                    Width = 100
                };
                btnQuitar.Click += BtnQuitar_Click;
                stpContent.Children.Add(btnQuitar);
                #endregion


                stpInsumos.Children.Add(lblProducto);
                ctrDecimal.valor = 1;
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void TxtDecimal_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                TextBox ctrl = sender as TextBox;
                ProductosCOM producto = ctrl.Tag as ProductosCOM;
                List<Label> listLBL = stpInsumos.Children.Cast<Label>().ToList();
                //int i = 0;
                foreach (Label item in listLBL)
                {
                    ProductosCOM pro = (item.Tag as ProductosCOM);
                    if (producto == pro)
                    {
                        pro.cantidad = Convert.ToDecimal(ctrl.Text.Trim());
                    }
                    //i++;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void BtnQuitar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = sender as Button;
                ProductosCOM producto = btn.Tag as ProductosCOM;
                List<Label> listLBL = stpInsumos.Children.Cast<Label>().ToList();
                int i = 0;
                foreach (Label item in listLBL)
                {
                    if (producto == (item.Tag as ProductosCOM))
                    {
                        stpInsumos.Children.Remove(stpInsumos.Children[i]);
                        return;
                    }
                    i++;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void lvlProductos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }
        bool load = false;
        private void chaTipoBusqueda_Checked(object sender, RoutedEventArgs e)
        {
            if (load)
            {
                buscarConsultasTaller(this.area, true);
            }
        }

        private void chaTipoBusqueda_Unchecked(object sender, RoutedEventArgs e)
        {
            buscarConsultasTaller(this.area, false);
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
        private List<ProductosCOM> getListaProductos()
        {
            List<ProductosCOM> lista = new List<ProductosCOM>();
            foreach (Label item in stpInsumos.Children.Cast<Label>().ToList())
            {
                lista.Add(item.Tag as ProductosCOM);
            }
            return lista;
        }
    }
}
