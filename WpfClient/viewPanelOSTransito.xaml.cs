﻿using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para viewPanelOSTransito.xaml
    /// </summary>
    public partial class viewPanelOSTransito : Window
    {
        private int idTipoOrdenServicio;
        //private bool _contentLoaded;
        public viewPanelOSTransito()
        {
            this.idTipoOrdenServicio = 0;
            this.InitializeComponent();
        }
        public viewPanelOSTransito(int idTipoOrdenServicio)
        {
            this.idTipoOrdenServicio = 0;
            this.InitializeComponent();
            this.idTipoOrdenServicio = idTipoOrdenServicio;
        }
        public void abrirPanel()
        {
            base.ShowDialog();
        }
        private void lvlPanel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult result = new PanelOSTransitoSvc().getPanelOSTransitoByTipoOrden(this.idTipoOrdenServicio);
                if (result.typeResult == ResultTypes.success)
                {
                    this.lvlPanel.ItemsSource = result.result as List<PanelOSTransito>;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                base.DialogResult = false;
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
    }
}
