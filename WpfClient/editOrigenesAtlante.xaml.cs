﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using CoreFletera.Interfaces;
    using CoreFletera.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Markup;

    public partial class editOrigenesAtlante : WpfClient.ViewBase
    {        
        public editOrigenesAtlante()
        {
            this.InitializeComponent();
        }      

        private bool cargarBases()
        {
            bool flag2;
            try
            {
                base.Cursor = Cursors.Arrow;
                OperationResult resp = new BaseSvc().getAllBase();
                if (resp.typeResult == ResultTypes.success)
                {
                    this.cbxBase.ItemsSource = resp.result as List<Base>;
                    return true;
                }
                if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                    return false;
                }
                flag2 = false;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                flag2 = false;
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return flag2;
        }

        private void cargarOrigenes()
        {
            try
            {
                OperationResult result = new OrigenSvc().getOrigenesAll();
                if (result.typeResult == ResultTypes.success)
                {
                    this.lvlOrigenes.ItemsSource = result.result as List<Origen>;
                    CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlOrigenes.ItemsSource);
                    PropertyGroupDescription item = new PropertyGroupDescription("estado");
                    defaultView.GroupDescriptions.Add(item);
                    defaultView.Filter = new Predicate<object>(this.UserFilter);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        public override OperationResult guardar()
        {
            OperationResult result2;
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult result = this.validar();
                if (result.typeResult > ResultTypes.success)
                {
                    return result;
                }
                Origen origen = this.mapForm();
                if (origen != null)
                {
                    OperationResult result3 = new OrigenSvc().saveOrigenCliente_v2(ref origen);
                    if (result3.typeResult == ResultTypes.success)
                    {
                        if (origen.activo)
                        {
                            this.mapForm(origen);
                        }
                        else
                        {
                            this.mapForm(null);
                        }
                        this.cargarOrigenes();
                        base.mainWindow.habilitar = true;
                    }
                    return result3;
                }
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = "REVISAR LOS DATOS PROPORCIONADOS"
                };
            }
            catch (Exception exception)
            {
                result2 = new OperationResult(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return result2;
        }       

        private void lvlOrigenes_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Origen selectedItem = (Origen)((ListView)sender).SelectedItem;
            if (selectedItem != null)
            {
                this.mapForm(selectedItem);
            }
        }

        private void mapClasificaciones()
        {
            try
            {
                OperationResult resp = new ClasificacionSvc().getAllClasificaciones();
                if (resp.typeResult == ResultTypes.success)
                {
                    foreach (Clasificacion clasificacion in (List<Clasificacion>)resp.result)
                    {
                        this.cbxClasificacion.Items.Add(clasificacion);
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void mapEstados()
        {
            try
            {
                OperationResult resp = new EstadoSvc().getEstadosAllorById(0);
                if (resp.typeResult == ResultTypes.success)
                {
                    foreach (Estado estado in (List<Estado>)resp.result)
                    {
                        this.cbxEstados.Items.Add(estado);
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private Origen mapForm()
        {
            try
            {
                return new Origen
                {
                    idOrigen = (this.txtCve.Tag == null) ? 0 : ((Origen)this.txtCve.Tag).idOrigen,
                    nombreOrigen = this.txtNombre.Text,
                    //clasificacion = ((Clasificacion)this.cbxClasificacion.SelectedItem).clasificacion,
                    estado = ((Estado)this.cbxEstados.SelectedItem).nombre,
                    idCliente = 0,
                    km = this.txtKM.valor,
                    listDestinos = new List<Zona>(),
                    activo = this.chcActivo.IsChecked.Value,
                    baseOrigen = this.cbxBase.SelectedItem as Base
                };
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return null;
            }
        }

        private void mapForm(Origen origen)
        {
            try
            {
                if (origen != null)
                {
                    this.txtCve.Tag = origen;
                    this.txtCve.valor = origen.idOrigen;
                    this.txtNombre.Text = origen.nombreOrigen;
                    this.txtKM.valor = origen.km;
                    //this.selectClasificacion(origen.clasificacion);
                    this.selectEstado(origen.estado);
                    this.chcActivo.IsChecked = new bool?(origen.activo);
                    if (origen.baseOrigen != null)
                    {
                        cbxBase.SelectedItem = (cbxBase.ItemsSource as List<Base>).Find(s => s.idBase == origen.baseOrigen.idBase);
                    }
                }
                else
                {
                    this.txtCve.Tag = null;
                    this.txtCve.valor = 0;
                    this.txtNombre.Clear();
                    this.txtKM.valor = decimal.Zero;
                    //this.cbxClasificacion.SelectedItem = null;
                    this.cbxEstados.SelectedItem = null;
                    this.chcActivo.IsChecked = true;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
            this.mapForm(null);
        }

        private void selectClasificacion(string clasificacion)
        {
            try
            {
                int num = 0;
                foreach (Clasificacion clasificacion2 in (IEnumerable)this.cbxClasificacion.Items)
                {
                    if (clasificacion == clasificacion2.clasificacion)
                    {
                        this.cbxClasificacion.SelectedIndex = num;
                        return;
                    }
                    num++;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void selectEstado(string estado)
        {
            try
            {
                int num = 0;
                foreach (Estado estado2 in (IEnumerable)this.cbxEstados.Items)
                {
                    if (estado == estado2.nombre)
                    {
                        this.cbxEstados.SelectedIndex = num;
                        return;
                    }
                    num++;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void txtFiltro_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.lvlOrigenes.ItemsSource != null)
            {
                CollectionViewSource.GetDefaultView(this.lvlOrigenes.ItemsSource).Refresh();
            }
        }

        private bool UserFilter(object item) =>
            ((item as Origen).nombreOrigen.IndexOf(this.txtNombre.Text, StringComparison.OrdinalIgnoreCase) >= 0);

        private OperationResult validar()
        {
            try
            {
                OperationResult result = new OperationResult
                {
                    valor = 0,
                    mensaje = "Para Continuar Se Requiere." + Environment.NewLine
                };
                if (this.cbxBase.SelectedItem == null)
                {
                    result.valor = 2;
                    result.mensaje = result.mensaje + " * Seleccionar la Base" + Environment.NewLine;
                }
                if (string.IsNullOrEmpty(this.txtNombre.Text))
                {
                    result.valor = 2;
                    result.mensaje = result.mensaje + " * Proporcionar El Nombre Del Origen" + Environment.NewLine;
                }
                if (this.cbxEstados.SelectedItem == null)
                {
                    result.valor = 2;
                    result.mensaje = result.mensaje + " * Seleccionar Un Estado" + Environment.NewLine;
                }
                //if (this.cbxClasificacion.SelectedItem == null)
                //{
                //    result.valor = 2;
                //    result.mensaje = result.mensaje + " * Seleccionar Una Clasificaci\x00f3n" + Environment.NewLine;
                //}
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                this.mapEstados();
                //this.mapClasificaciones();
                this.cargarBases();
                this.cargarOrigenes();
                this.nuevo();
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
    }
}
