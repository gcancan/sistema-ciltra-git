﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for editTipoCombustible.xaml
    /// </summary>
    public partial class editTipoCombustible : ViewBase
    {
        public editTipoCombustible()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            nuevo();
        }
        public override void nuevo()
        {
            mainWindow.enableToolBarCommands(ToolbarCommands.guardar | ToolbarCommands.cerrar| ToolbarCommands.nuevo);
            txtTipoCombustible.Clear();
        }
        public override OperationResult guardar()
        {
            try
            {
                TipoCombustible tipoCombustible = mapForm();
                if (tipoCombustible != null)
                {
                    OperationResult resp = new TipoCombustibleSvc().saveTipoCombustible(ref tipoCombustible);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult { valor = 1, mensaje = "Ocurrio un error" };
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        private TipoCombustible mapForm()
        {
            try
            {
                return new TipoCombustible
                {
                    idTipoCombustible = 0,
                    tipoCombustible = txtTipoCombustible.Text.Trim()
                };
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }
    }
}
