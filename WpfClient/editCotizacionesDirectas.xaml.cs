﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Interfaces;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using Core.Models;
using static WpfClient.ImprimirMensaje;
using ProcesosComercial;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editCotizacionesDirectas.xaml
    /// </summary>
    public partial class editCotizacionesDirectas : ViewBase
    {
        public editCotizacionesDirectas()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            txtClave.txtEntero.KeyDown += TxtEntero_KeyDown;
            ctrProveedor.DataContextChanged += CtrProveedor_DataContextChanged;
            ctrProveedor.cargarTodosLosProveedores();
            ctrEmpresa.loaded(mainWindow.empresa, true);
            nuevo();
        }

        private void TxtEntero_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                buscarCotizacion(txtClave.valor);
            }
        }
        private void buscarCotizacion(int i)
        {
            try
            {
                //Cursor = Cursors.Wait;
                var resp = new CotizacionesSvc().getCotizacionAllOrById(i);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm((CotizacionSolicitudOrdCompra)resp.result);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                //Cursor = Cursors.Arrow;
            }
        }

        private void CtrProveedor_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (ctrProveedor.DataContext != null)
            {
                if (ctrProveedor.proveedor != null)
                {
                    var lista = listaRadioButton;
                    RadioButton chcSelect = lista.Find(s => s.IsChecked.Value);
                    if (chcSelect != null)
                    {
                        añadirProveedor(chcSelect, ctrProveedor.proveedor);
                    }
                    ctrProveedor.DataContext = null;
                    ctrProveedor.proveedor = null;
                }
            }
            ctrProveedor.txtProveedor.Clear();
        }
        private void añadirProveedor(RadioButton chcSelect, Proveedor proveedor)
        {
            DetalleCotizacionSolicitudOrdCompra detalle = chcSelect.Tag as DetalleCotizacionSolicitudOrdCompra;
            Border border = chcSelect.Content as Border;
            StackPanel stpContent = border.Child as StackPanel;

            List<object> listaControles = stpContent.Children.Cast<object>().ToList();
            foreach (var item in listaControles)
            {
                if (item is StackPanel)
                {
                    StackPanel stp = item as StackPanel;
                    if (stp.Name == "stpProveedores")
                    {
                        if (validarProveedorDuplicado(stp, proveedor))
                        {
                            return;
                        }

                        DetalleCotizaciones detalleCotizacion = new DetalleCotizaciones
                        {
                            idDetalle = 0,
                            proveedor = proveedor,
                            costo = 0,
                            diasCredito = 0,
                            diasEntrega = 0,
                            elegido = false,
                            importe = 0
                        };

                        añadirControlesProveedores(chcSelect, proveedor, stp, detalle, detalleCotizacion);
                        if (detalle.listDetalles == null)
                        {
                            detalle.listDetalles = new List<DetalleCotizaciones>();
                        }
                        detalle.listDetalles.Add(detalleCotizacion);
                    }
                }
            }
        }

        private void añadirProveedores(RadioButton chcSelect, List<DetalleCotizaciones> listaDetalles)
        {
            DetalleCotizacionSolicitudOrdCompra detalle = chcSelect.Tag as DetalleCotizacionSolicitudOrdCompra;
            Border border = chcSelect.Content as Border;
            StackPanel stpContent = border.Child as StackPanel;

            List<object> listaControles = stpContent.Children.Cast<object>().ToList();
            foreach (var item in listaControles)
            {
                if (item is StackPanel)
                {
                    StackPanel stp = item as StackPanel;
                    if (stp.Name == "stpProveedores")
                    {
                        foreach (var itemDet in listaDetalles)
                        {
                            añadirControlesProveedores(chcSelect, itemDet.proveedor, stp, detalle, itemDet);
                        }
                    }
                }
            }
        }

        void añadirControlesProveedores(RadioButton chcSelect, Proveedor proveedor, StackPanel stp, DetalleCotizacionSolicitudOrdCompra detalle, DetalleCotizaciones detalleCotizacion)
        {
            RadioButton rbnProveedor = new RadioButton { FontWeight = FontWeights.Medium, Tag = chcSelect, DataContext = detalleCotizacion, IsChecked = detalleCotizacion.elegido };
            StackPanel stpContentProveedor = new StackPanel { Orientation = Orientation.Horizontal, Tag = proveedor };

            TextBox txtProveedor = new TextBox
            {
                Tag = proveedor,
                Text = proveedor.razonSocial,
                IsEnabled = false,
                Background = null,
                BorderBrush = null,
                CharacterCasing = CharacterCasing.Upper,
                AcceptsReturn = true,
                TextWrapping = TextWrapping.Wrap,
                Margin = new Thickness(2),
                Width = 300
            };
            stpContentProveedor.Children.Add(txtProveedor);

            controlDecimal txtPrecio = new controlDecimal { Name = "txtPrecio", Width = 120, FontSize = 11 };
            txtPrecio.txtDecimal.Tag = chcSelect;
            txtPrecio.txtDecimal.TextChanged += TxtDecimal_TextChanged;
            txtPrecio.txtDecimal.Text = detalleCotizacion.costo.ToString();
            stpContentProveedor.Children.Add(txtPrecio);

            controlEnteros txtDiasEntrega = new controlEnteros { Name = "txtDiasEntrega", Width = 120, FontSize = 11 };
            txtDiasEntrega.txtEntero.Tag = chcSelect;
            txtDiasEntrega.txtEntero.TextChanged += TxtDecimal_TextChanged;
            txtDiasEntrega.txtEntero.Text = detalleCotizacion.diasEntrega.ToString();
            stpContentProveedor.Children.Add(txtDiasEntrega);

            controlEnteros txtDiasCredito = new controlEnteros { Name = "txtDiasCredito", Width = 120, FontSize = 11 };
            txtDiasCredito.txtEntero.Tag = chcSelect;
            txtDiasCredito.txtEntero.TextChanged += TxtDecimal_TextChanged;
            txtDiasCredito.txtEntero.Text = detalleCotizacion.diasCredito.ToString();
            stpContentProveedor.Children.Add(txtDiasCredito);

            rbnProveedor.Checked += RbnProveedor_Checked;
            rbnProveedor.Content = stpContentProveedor;
            stp.Children.Add(rbnProveedor);


        }

        private void RbnProveedor_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                RadioButton rbn = sender as RadioButton;
                actualizarImporte(rbn.Tag as RadioButton);
            }
            catch (Exception ex)
            {

            }
        }

        private void actualizarImporte(RadioButton radioButton)
        {
            try
            {
                int cantidad = 0;
                decimal precio = 0m;

                foreach (RadioButton rbn in listaRadioButton)
                {
                    if (rbn == radioButton)
                    {
                        DetalleCotizacionSolicitudOrdCompra detalle = rbn.Tag as DetalleCotizacionSolicitudOrdCompra;

                        Border border = rbn.Content as Border;
                        StackPanel stp = border.Child as StackPanel;
                        List<object> listaControles = stp.Children.Cast<object>().ToList();
                        foreach (object control in listaControles)
                        {
                            /************************************************************************/
                            if (control is controlDecimal)
                            {
                                controlDecimal ctrDecimal = control as controlDecimal;
                                if (ctrDecimal.Name == "txtCantidad")
                                {
                                    cantidad = Convert.ToInt32(ctrDecimal.valor);
                                    detalle.cantidad = cantidad;
                                }
                            }
                            /************************************************************************/
                            if (control is StackPanel)
                            {
                                StackPanel stpProveedores = control as StackPanel;
                                foreach (RadioButton rbnPro in stpProveedores.Children.Cast<RadioButton>().ToList())
                                {
                                    DetalleCotizaciones detalleCot = rbnPro.DataContext as DetalleCotizaciones;
                                    if (rbnPro.IsChecked.Value)
                                    {
                                        detalleCot.elegido = true;
                                        detalle.proveedor = detalleCot.proveedor;
                                    }
                                    else
                                    {
                                        detalleCot.elegido = false;
                                    }
                                    StackPanel stpProveedorCheck = rbnPro.Content as StackPanel;
                                    foreach (object item in stpProveedorCheck.Children.Cast<object>().ToList())
                                    {
                                        if (item is controlDecimal)
                                        {
                                            controlDecimal ctrDecimal = item as controlDecimal;
                                            if (ctrDecimal.Name == "txtPrecio")
                                            {
                                                precio = detalleCot.elegido ? ctrDecimal.valor : precio;
                                                detalleCot.costo = ctrDecimal.valor;
                                                detalle.costo = ctrDecimal.valor;
                                            }
                                        }

                                        if (item is controlEnteros)
                                        {
                                            controlEnteros ctrEntero = item as controlEnteros;
                                            if (ctrEntero.Name == "txtDiasEntrega")
                                            {
                                                detalleCot.diasEntrega = ctrEntero.valor;
                                            }
                                            if (ctrEntero.Name == "txtDiasCredito")
                                            {
                                                detalleCot.diasCredito = ctrEntero.valor;
                                            }
                                        }
                                        detalleCot.importe = (cantidad * detalleCot.costo);
                                        detalle.importe = (cantidad * detalleCot.costo);
                                    }
                                }
                            }
                            /************************************************************************/
                            if (control is Label)
                            {
                                (control as Label).Content = (precio * cantidad).ToString("N2");
                            }
                            /************************************************************************/
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private bool validarProveedorDuplicado(StackPanel stp, Proveedor proveedor)
        {
            List<RadioButton> listRbn = stp.Children.Cast<RadioButton>().ToList();
            foreach (RadioButton rbn in listRbn)
            {
                if (proveedor.idProveedor == (rbn.DataContext as DetalleCotizaciones).proveedor.idProveedor)
                {
                    return true;
                }
            }
            return false;
        }

        List<RadioButton> listaRadioButton
        {
            get
            {
                try
                {
                    return stpInsumos.Children.Cast<RadioButton>().ToList();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
        List<DetalleCotizacionSolicitudOrdCompra> listaDetalleCotizacion
        {
            get
            {
                List<DetalleCotizacionSolicitudOrdCompra> lista = new List<DetalleCotizacionSolicitudOrdCompra>();
                foreach (var item in listaRadioButton)
                {
                    lista.Add(item.Tag as DetalleCotizacionSolicitudOrdCompra);
                }
                return lista;
            }
        }
        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);
        }

        private void btnBuscarInsumos_Click(object sender, RoutedEventArgs e)
        {
            buscarProducto();
        }
        void buscarProducto()
        {
            var lista = new selectProductosCOM("").consultasTallerSinExistencias(AreaTrabajo.TOTAS);
            if (lista != null)
            {
                if (lista.Count > 0)
                {
                    foreach (var item in lista)
                    {
                        var det = listaDetalleCotizacion.Find(s => s.producto.idProducto == item.idProducto);
                        if (det == null)
                        {
                            DetalleCotizacionSolicitudOrdCompra detalleCotizaciones = new DetalleCotizacionSolicitudOrdCompra
                            {
                                producto = item,
                                cantidad = Convert.ToInt16(item.cantidad),
                                costo = 0,
                                importe = 0,
                                idDetalleCotizacionSolicitudOrdCompra = 0,
                                listDetalles = new List<DetalleCotizaciones>()
                            };
                            llenarLista(detalleCotizaciones);
                        }
                    }
                }
            }
        }

        void llenarLista(DetalleCotizacionSolicitudOrdCompra detalleCotizaciones)
        {
            try
            {
                Border borderProducto = new Border { BorderThickness = new Thickness(1, 1, 1, 1), BorderBrush = Brushes.SteelBlue };
                RadioButton rbnProducto = new RadioButton() { Tag = detalleCotizaciones, FontWeight = FontWeights.Bold, Margin = new Thickness(1) };
                StackPanel stpContent = new StackPanel { Tag = detalleCotizaciones, Orientation = Orientation.Horizontal };

                TextBox txtNombre = new TextBox
                {
                    IsReadOnly = true,
                    Background = null,
                    BorderBrush = null,
                    Foreground = Brushes.SteelBlue,
                    Text = detalleCotizaciones.producto.nombre,
                    CharacterCasing = CharacterCasing.Upper,
                    Width = 300,
                    AcceptsReturn = true,
                    TextWrapping = TextWrapping.Wrap,
                    Margin = new Thickness(2),
                    FontSize = 11,
                    IsEnabled = false
                };
                stpContent.Children.Add(txtNombre);

                controlDecimal ctrCantidad = new controlDecimal() { Width = 120, valor = detalleCotizaciones.cantidad, Name = "txtCantidad", FontSize = 11 };
                ctrCantidad.Tag = detalleCotizaciones;
                ctrCantidad.txtDecimal.Tag = rbnProducto;
                ctrCantidad.txtDecimal.TextChanged += TxtDecimal_TextChanged;
                stpContent.Children.Add(ctrCantidad);

                StackPanel stpProveedores = new StackPanel() { Name = "stpProveedores", Width = 700 };

                //if (detalleCotizaciones.listDetalles.Count > 0)
                //{
                //    foreach (var item in detalleCotizaciones.listDetalles)
                //    {
                //        RadioButton rbnProveedor = new RadioButton { FontWeight = FontWeights.Medium, Tag = borderProducto, DataContext = detalleCotizaciones, IsChecked = item.elegido };
                //        StackPanel stpContentProveedor = new StackPanel { Orientation = Orientation.Horizontal, Tag = item.proveedor };

                //        TextBox txtProveedor = new TextBox
                //        {
                //            Tag = item.proveedor,
                //            Text = item.proveedor.razonSocial,
                //            IsEnabled = false,
                //            Background = null,
                //            BorderBrush = null,
                //            CharacterCasing = CharacterCasing.Upper,
                //            AcceptsReturn = true,
                //            TextWrapping = TextWrapping.Wrap,
                //            Margin = new Thickness(2),
                //            Width = 300
                //        };
                //        stpContentProveedor.Children.Add(txtProveedor);

                //        controlDecimal txtPrecio = new controlDecimal { Name = "txtPrecio", Width = 120, FontSize = 11 };
                //        txtPrecio.txtDecimal.Tag = rbnProducto;
                //        txtPrecio.txtDecimal.TextChanged += TxtDecimal_TextChanged;
                //        txtPrecio.txtDecimal.Text = item.costo.ToString();
                //        stpContentProveedor.Children.Add(txtPrecio);

                //        controlEnteros txtDiasEntrega = new controlEnteros { Name = "txtDiasEntrega", Width = 120, FontSize = 11 };
                //        txtDiasEntrega.txtEntero.Tag = rbnProducto;
                //        txtDiasEntrega.txtEntero.TextChanged += TxtDecimal_TextChanged;
                //        txtDiasEntrega.txtEntero.Text = item.diasEntrega.ToString();
                //        stpContentProveedor.Children.Add(txtDiasEntrega);

                //        controlEnteros txtDiasCredito = new controlEnteros { Name = "txtDiasCredito", Width = 120, FontSize = 11 };
                //        txtDiasCredito.txtEntero.Tag = rbnProducto;
                //        txtDiasCredito.txtEntero.TextChanged += TxtDecimal_TextChanged;
                //        txtDiasCredito.txtEntero.Text = item.diasCredito.ToString();
                //        stpContentProveedor.Children.Add(txtDiasCredito);

                //        rbnProveedor.Checked += RbnProveedor_Checked;
                //        rbnProveedor.Content = stpContentProveedor;
                //        stpProveedores.Children.Add(rbnProveedor);
                //    }
                //}

                stpContent.Children.Add(stpProveedores);

                Label lblImporte = new Label
                {
                    Name = "txtImporte",
                    //Content = detalleCotizaciones.importe.ToString("N2"),
                    Foreground = Brushes.Blue,
                    FontSize = 16,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    VerticalAlignment = VerticalAlignment.Center,
                    Width = 120
                };
                
                stpContent.Children.Add(lblImporte);



                borderProducto.Child = stpContent;
                rbnProducto.Content = borderProducto;
                stpInsumos.Children.Add(rbnProducto);

                if (detalleCotizaciones.listDetalles.Count > 0)
                {
                    añadirProveedores(rbnProducto, detalleCotizaciones.listDetalles);
                }
                lblImporte.Content = detalleCotizaciones.listDetalles.Find(s => s.elegido) == null ? "0" : detalleCotizaciones.listDetalles.Find(s => s.elegido).importe.ToString("N2");
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void TxtDecimal_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            RadioButton rbn = txt.Tag as RadioButton;
            actualizarImporte(rbn);
        }
        void actualizarValores(DetalleCotizacionSolicitudOrdCompra detalleCotizaciones)
        {

        }

        public override OperationResult guardar()
        {
            try
            {
                var res = listaDetalleCotizacion;

                CotizacionSolicitudOrdCompra cotizacion = mapForm();
                var resp = new CotizacionesSvc().saveCotizacion(ref cotizacion);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(cotizacion);
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        private void mapForm(CotizacionSolicitudOrdCompra cotizacion)
        {
            try
            {
                if (cotizacion != null)
                {
                    txtClave.Tag = cotizacion;
                    txtClave.valor = cotizacion.idCotizacionSolicitudOrdCompra;
                    stpInsumos.Children.Clear();
                    foreach (var item in cotizacion.listDetalle)
                    {
                        llenarLista(item);
                    }
                    if (cotizacion.estatus == "APROBADO" || cotizacion.estatus == "APROVADO")
                    {
                        stpInsumos.IsEnabled = false;
                    }
                }
                else
                {
                    txtClave.Tag = null;
                    txtClave.limpiar();
                    stpInsumos.Children.Clear();
                    stpInsumos.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        public CotizacionSolicitudOrdCompra mapForm()
        {
            try
            {
                CotizacionSolicitudOrdCompra cotizacion = new CotizacionSolicitudOrdCompra()
                {

                    empresa = txtClave.Tag == null ? ctrEmpresa.empresaSelected : (txtClave.Tag as CotizacionSolicitudOrdCompra).empresa ,
                    fecha = txtClave.Tag == null ? DateTime.Now : (txtClave.Tag as CotizacionSolicitudOrdCompra).fecha ,
                    estatus = txtClave.Tag == null ? "COTIZADO" : (txtClave.Tag as CotizacionSolicitudOrdCompra).estatus ,
                    isLlantas = false,
                    solicitud = txtClave.Tag == null ? null : (txtClave.Tag as CotizacionSolicitudOrdCompra).solicitud,
                    fechaAutorizacion = null,
                    idCotizacionSolicitudOrdCompra = txtClave.Tag == null ? 0 : (txtClave.Tag as CotizacionSolicitudOrdCompra).idCotizacionSolicitudOrdCompra,
                    usuarioCotiza = txtClave.Tag == null ? mainWindow.usuario.nombreUsuario : (txtClave.Tag as CotizacionSolicitudOrdCompra).usuarioCotiza,
                    usuarioAprueba = ""
                };
                cotizacion.listDetalle = new List<DetalleCotizacionSolicitudOrdCompra>();
                cotizacion.listDetalle = listaDetalleCotizacion;
                return cotizacion;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void btnAutoriza_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CotizacionSolicitudOrdCompra cotizacion = mapForm();
                cotizacion.fechaAutorizacion = DateTime.Now;
                cotizacion.usuarioAprueba = mainWindow.usuario.nombreUsuario;

                if (cotizacion.estatus == "APROBADO" || cotizacion.estatus == "APROVADO")
                {
                    imprimir(new OperationResult(ResultTypes.warning,  "ESTÁ SOLICITUD YA ESTA AUTORIZADA."));
                    return;
                }
                cotizacion.estatus = "APROBADO";
                List<OrdenCompra> listaOrdenCom = new List<OrdenCompra>();
                foreach (var detalle in cotizacion.listDetalle)
                {
                    bool existe = false;
                    foreach (OrdenCompra ord in listaOrdenCom)
                    {

                        if (ord.proveedor.idProveedor == detalle.proveedor.idProveedor)
                        {
                            ord.listaDetalles.Add(new DetalleOrdenCompra
                            {
                                idDetalleCompra = 0,
                                cantidad = detalle.cantidad,
                                costo = detalle.costo,
                                importe = detalle.importe,
                                factura = "",
                                llantaProducto = detalle.llantaProducto,
                                producto = detalle.producto,
                                pendientes = detalle.cantidad,
                                unidadTransporte = detalle.unidadTransporte
                            });
                            existe = true;
                            break;
                        }
                    }
                    if (existe)
                    {
                        continue;
                    }

                    OrdenCompra orden = new OrdenCompra()
                    {
                        idOrdenCompra = 0,
                        idCotizacion = cotizacion.idCotizacionSolicitudOrdCompra,
                        empresa = cotizacion.empresa,
                        estatus = "ACT",
                        fecha = DateTime.Now,
                        CIDDOCUMENTO = 0,
                        folioFactura = "",
                        proveedor = detalle.proveedor,
                        listaDetalles = new List<DetalleOrdenCompra>(),
                        isLLanta = cotizacion.isLlantas
                    };
                    orden.listaDetalles.Add(new DetalleOrdenCompra
                    {
                        idDetalleCompra = 0,
                        cantidad = detalle.cantidad,
                        costo = detalle.costo,
                        importe = detalle.importe,
                        factura = "",
                        llantaProducto = detalle.llantaProducto,
                        producto = detalle.producto,
                        pendientes = detalle.cantidad,
                        unidadTransporte = detalle.unidadTransporte
                    });
                    listaOrdenCom.Add(orden);
                    continue;
                }

                ProcesosForm proceso = new ProcesosForm();
                var respNew = proceso.realizarOrdenesCompra(listaOrdenCom, cotizacion);
                //proceso.Dispose();

                //System.Threading.Thread.Sleep(10000);
                imprimir(respNew);
                if (respNew.typeResult == ResultTypes.success)
                {
                    //var coti = ;
                    //buscarCotizacion(cotizacion.idCotizacionSolicitudOrdCompra);
                    //MessageBox.Show("");
                    stpInsumos.IsEnabled = false;
                }
                else
                {
                    //btnAutorizar.Visibility = Visibility.Visible;
                }              

            }
            catch (Exception EX)
            {
                
            }
        }
    }
}
