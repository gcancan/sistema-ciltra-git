﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using static WpfClient.ImprimirMensaje;
using CoreFletera.Interfaces;
using Core.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editFacturacionV2.xaml
    /// </summary>
    public partial class editFacturacionV2 : ViewBase
    {
        Usuario usuario = null;
        public editFacturacionV2()
        {
            InitializeComponent();
        }
        controlResultadoViajes controlResultadoViajes;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.usuario = mainWindow.usuario;
            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresa.loaded(usuario.empresa, new PrivilegioSvc().consultarPrivilegio("isAdmin", usuario.idUsuario).typeResult == ResultTypes.success);

            ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
            ctrZonaOperativa.cargarControl(usuario.zonaOperativa, usuario.idUsuario);

            cbxTipoServicio.ItemsSource = Enum.GetValues(typeof(enumTipoServicioFacturacion));
            cbxTipoServicio.SelectionChanged += CbxTipoServicio_SelectionChanged;

            //cbxOperacion.SelectionChanged += CbxOperacion_SelectionChanged;
            cbxTipoServicio.SelectedIndex = 0;

            List<Modalidad> list1 = new List<Modalidad>();
            Modalidad item = new Modalidad
            {
                nombre = "SENCILLO",
                letra = "S"
            };
            list1.Add(item);
            Modalidad modalidad2 = new Modalidad
            {
                nombre = "FULL",
                letra = "F"
            };
            list1.Add(modalidad2);
            this.cbxModalidad.ItemsSource = list1;
            List<string> list2 = new List<string> {
                    "FINALIZADO",
                    "ACTIVO"
                };
            this.cbxEstatus.ItemsSource = list2;

            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
            stpBotonesMarcar.Visibility = Visibility.Hidden;

            cbxOperacion.IsEnabled = new PrivilegioSvc().consultarPrivilegio("ADMIN_COMBO_CLIENTES", usuario.idUsuario).typeResult == ResultTypes.success;
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cargarListaOperaciones();
        }

        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cargarListaOperaciones();
        }
        void cargarListaOperaciones()
        {
            try
            {
                Cursor = Cursors.Wait;
                cbxOperacion.ItemsSource = null;

                if (ctrEmpresa.empresaSelected.clave == 1)
                {
                    if (ctrZonaOperativa.zonaOperativa != null)
                    {
                        var resp = new ClienteSvc().getClientesByZonasOperativas(new List<string> { (ctrZonaOperativa.zonaOperativa.idZonaOperativa.ToString()) }, ctrEmpresa.empresaSelected.clave);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            var listaOperaciones = resp.result as List<OperacionFletera>;
                            cbxOperacion.ItemsSource = listaOperaciones;
                            cbxOperacion.SelectedItem = cbxOperacion.Items.Cast<OperacionFletera>().ToList().Find(s => s.cliente.clave == usuario.cliente.clave);
                        }
                        else if (resp.typeResult != ResultTypes.recordNotFound)
                        {
                            imprimir(resp);
                        }
                    }
                }
                else
                {
                    var respCli = new ClienteSvc().getAllClienteByEmpresa(ctrEmpresa.empresaSelected.clave);
                    if (respCli.typeResult == ResultTypes.success)
                    {
                        List<Cliente> listaClientes = respCli.result as List<Cliente>;
                        List<OperacionFletera> listaOperacion = new List<OperacionFletera>();
                        foreach (var item in listaClientes)
                        {
                            listaOperacion.Add(new OperacionFletera
                            {
                                cliente = item,
                                empresa = ctrEmpresa.empresaSelected,
                                zonaOperativa = ctrZonaOperativa.zonaOperativa
                            });
                        }
                        cbxOperacion.ItemsSource = listaOperacion.OrderBy(s=>s.cliente.nombre).ToList();
                    }
                }
                    
                
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void CbxOperacion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                chcTodasRutas.IsChecked = false;
                cbxRutas.ItemsSource = null;
                if (sender != null)
                {
                    ComboBox cbx = sender as ComboBox;
                    if (cbx.SelectedItem != null)
                    {
                        if (ctrEmpresa.empresaSelected.clave == 1)
                        {
                            OperacionFletera operacion = cbxOperacion.SelectedItem as OperacionFletera;
                            var resp = new ZonasSvc().getZonasALLorById(0, operacion.cliente.clave.ToString());
                            if (resp.typeResult == ResultTypes.success)
                            {
                                cbxRutas.ItemsSource = resp.result as List<Zona>;
                                chcTodasRutas.IsChecked = true;
                            }
                            else
                            {
                                imprimir(resp);
                            }
                        }
                        else
                        {
                            OperacionFletera operacion = cbxOperacion.SelectedItem as OperacionFletera;
                            var resp = new ZonasSvc().getDestinosByIdEmpresa(ctrEmpresa.empresaSelected.clave);
                            if (resp.typeResult == ResultTypes.success)
                            {
                                cbxRutas.ItemsSource = resp.result as List<Zona>;
                                chcTodasRutas.IsChecked = true;
                            }
                            else
                            {
                                imprimir(resp);
                            }
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        controlResumenViajes controlResumen;
        private void CbxTipoServicio_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                stpResumen.Children.Clear();
                stpResultados.Children.Clear();

                if (sender != null)
                {
                    ComboBox cbx = sender as ComboBox;
                    if (cbx.SelectedItem != null)
                    {
                        switch ((enumTipoServicioFacturacion)cbx.SelectedItem)
                        {
                            case enumTipoServicioFacturacion.VIAJES:
                                controlResumen = new controlResumenViajes();
                                stpResumen.Children.Add(controlResumen);
                                controlResumen.cargarControles(new List<ReporteCartaPorteViajes>(),0);
                                controlResultadoViajes = new controlResultadoViajes();
                                controlResultadoViajes.DataContextChanged += ControlResultadoViajes_DataContextChanged;
                                stpResultados.Children.Add(controlResultadoViajes);
                                break;
                            case enumTipoServicioFacturacion.LAVADO_ESPECIALIZADO:
                                break;
                            case enumTipoServicioFacturacion.TRASPASOS:
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void ControlResultadoViajes_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                stpBotonesMarcar.Visibility = Visibility.Hidden;
                if (sender != null)
                {
                    controlResultadoViajes controlResultado = sender as controlResultadoViajes;
                    List<ReporteCartaPorteViajes> lista = controlResultado.DataContext as List<ReporteCartaPorteViajes>;
                    stpBotonesMarcar.Visibility = controlResultado.lvlViajes.Items.Count > 0 ? Visibility.Visible : Visibility.Hidden;
                    controlResumen.cargarControles(lista, diasEvaluados);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private List<ReporteCartaPorte> listaReporte = new List<ReporteCartaPorte>();
        private List<ReporteCartaPorteViajes> listaReporteViaje = new List<ReporteCartaPorteViajes>();
        private async void BtnGenerar_Click(object sender, RoutedEventArgs e)
        {
            await Dispatcher.Invoke(async () =>
             {
                 bool correcto = true;
                 if (cbxOperacion.SelectedItem == null)
                 {
                     imprimir(new OperationResult(ResultTypes.warning, "ELEGIR UNA OPERACIÓN"));
                     correcto = false;
                 }
                 if (cbxRutas.SelectedItems.Count == 0)
                 {
                     imprimir(new OperationResult(ResultTypes.warning, "ELEGIR RUTAS"));
                     correcto = false;
                 }
                 if (cbxModalidad.SelectedItems.Count == 0)
                 {
                     imprimir(new OperationResult(ResultTypes.warning, "ELEGIR UNA MODALIDAD"));
                     correcto = false;
                 }
                 if (correcto)
                 {
                     await Task.Run(async () => this.buscarViajes());
                 }
             });


        }
        private DateTime? fechaInicio = null;
        private DateTime? fechaFin = null;
        public async Task buscarViajes()
        {
            try
            {

                Dispatcher.Invoke(() =>
                {
                    popMensaje.IsOpen = true;
                    lblMensaje.Content = "BUSCANDO REGISTROS...";
                    btnGenerar.IsEnabled = false;
                    controlResultadoViajes.llenarLista(new List<ReporteCartaPorteViajes>());
                });

                List<string> listaRutas = new List<string>();
                int idCliente = 0;
                int idZonaOpetativa = 0;
                List<string> listaModalidades = new List<string>();
                Empresa empresa = null;
                bool byFechaDespacho = false;
                Dispatcher.Invoke(() =>
                {
                    fechaInicio = ctrFechas.fechaInicial;
                    fechaFin = ctrFechas.fechaFinal;
                    listaRutas = (from s in this.cbxRutas.SelectedItems.Cast<Zona>().ToList<Zona>() select s.clave.ToString()).ToList<string>();
                    byFechaDespacho = rbnFechaDespacho.IsChecked.Value;
                    idCliente = (cbxOperacion.SelectedItem as OperacionFletera).cliente.clave;
                    idZonaOpetativa = ctrZonaOperativa.zonaOperativa.idZonaOperativa;
                    listaModalidades = (from s in this.cbxModalidad.SelectedItems.Cast<Modalidad>().ToList<Modalidad>() select s.nombre.ToString()).ToList<string>();
                    empresa = ctrEmpresa.empresaSelected;
                });

                OperationResult resp = new ReporteCartaPorteSvc().getViajesFacturar(empresa.clave, fechaInicio.Value, fechaFin.Value, byFechaDespacho,
                idCliente, idZonaOpetativa, listaRutas, listaModalidades);
                if (resp.typeResult == ResultTypes.success)
                {
                    this.listaReporte = resp.result as List<ReporteCartaPorte>;
                    listaReporteViaje = (from v in this.listaReporte
                                         group v by v.folio into grupoViaje
                                         select new ReporteCartaPorteViajes
                                         {
                                             lista = grupoViaje.ToList<ReporteCartaPorte>(),
                                             fechaInicio = (from s in grupoViaje select s.fechaInicio).First(),
                                             fechaFin = (from s in grupoViaje select s.fechaFin).First(),
                                             fechaPrograma = (from s in grupoViaje select s.fechaPrograma).First(),
                                             isReEnviar = (from s in grupoViaje select s.isReEnviar).First(),
                                             imputable = (from s in grupoViaje select s.imputable).First()
                                         }).ToList();

                    await Dispatcher.Invoke(async () =>
                    {
                        await Task.Run(async () => await this.llenarLista(listaReporteViaje));
                    });
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    await Dispatcher.Invoke(async () =>
                    {
                        listaReporte = new List<ReporteCartaPorte>();
                        listaReporteViaje = new List<ReporteCartaPorteViajes>();
                        await Task.Run(async () => await this.llenarLista(listaReporteViaje));
                    });
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                imprimir(exception);
            }
            finally
            {
                //base.Cursor = Cursors.Arrow;
                Dispatcher.Invoke(() =>
                {
                    popMensaje.IsOpen = false;
                    btnGenerar.IsEnabled = true;
                });
            }
        }
        private async Task llenarLista(List<ReporteCartaPorteViajes> listaReporteViaje)
        {
            Dispatcher.Invoke(() =>
            {
                controlResultadoViajes.llenarLista(listaReporteViaje);
                controlResumen.cargarControles(listaReporteViaje, diasEvaluados);
            });
        }
        private void ChcTodasRutas_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    CheckBox check = sender as CheckBox;
                    if (check.IsChecked.Value)
                    {
                        foreach (var item in cbxRutas.ItemsSource)
                        {
                            cbxRutas.SelectedItems.Add(item);
                        }
                    }
                    else
                    {
                        cbxRutas.SelectedItems.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void BtnFacturar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (string.IsNullOrEmpty(txtFolioFactura.Text.Trim()))
                {
                    imprimir(new OperationResult(ResultTypes.warning, "SE REQUIERE EL FOLIO DE LA FACTURA"));
                    return;
                }
                FacturaServicios facturaServicios = mapForm();
                switch ((enumTipoServicioFacturacion)cbxTipoServicio.SelectedItem)
                {
                    case enumTipoServicioFacturacion.VIAJES:
                        var s = crearFacturaViajesAsync(facturaServicios);
                        //ImprimirMensaje.imprimir(s);
                        break;
                    case enumTipoServicioFacturacion.LAVADO_ESPECIALIZADO:
                        break;
                    case enumTipoServicioFacturacion.TRASPASOS:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                txtFolioFactura.Clear();
                txtObservaciones.Clear();
                Cursor = Cursors.Arrow;
            }
        }
        private FacturaServicios mapForm()
        {
            try
            {
                return new FacturaServicios
                {
                    idFacturaServicio = 0,
                    listDetalles = new List<DetalleFacturaServicio>(),
                    empresa = ctrEmpresa.empresaSelected,
                    zonaOperativa = ctrZonaOperativa.zonaOperativa,
                    cliente = (cbxOperacion.SelectedItem as OperacionFletera).cliente,
                    fechaFactura = DateTime.Now,
                    usuario = mainWindow.usuario.nombreUsuario,
                    usuarioCancelacion = string.Empty,
                    fechaCancelacion = null,
                    folioFactura = txtFolioFactura.Text.Trim(),
                    observaciones = txtObservaciones.Text.Trim()
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private async Task<OperationResult> crearFacturaViajesAsync(FacturaServicios facturaServicios)
        {
            try
            {
                //Dispatcher.Invoke(() =>
                //{
                //    popMensaje.IsOpen = true;
                //    lblMensaje.Content = "GENERANDO FACTURA...";
                //    btnGenerar.IsEnabled = false;
                //});

                facturaServicios.tipoFacturaServicio = enumTipoServicioFacturacion.VIAJES;
                var lista = controlResultadoViajes.DataContext as List<ReporteCartaPorteViajes>;
                foreach (var viaje in lista)
                {
                    facturaServicios.listDetalles.Add(new DetalleFacturaServicio
                    {
                        idDetalleFacturaServicio = 0,
                        idFacturaServicio = 0,
                        idDetalle = viaje.folio,
                        km = viaje.km,
                        cantidad = viaje.toneladas,
                        precio = viaje.precioVar,
                        precioFijo = viaje.precioFijo,
                        subTotal = viaje.subTotal,
                        importeIVA = viaje.impIva,
                        importeRetencion = viaje.impRetencion,
                        importe = viaje.importe
                    });
                }
                facturaServicios.subTotal = lista.Sum(s => s.subTotal);
                facturaServicios.importeIVA = lista.Sum(s => s.impIva);
                facturaServicios.importeRetencion = lista.Sum(s => s.impRetencion);
                facturaServicios.importe = lista.Sum(s => s.importe);

                if (facturaServicios.listDetalles.Count == 0)
                {
                    OperationResult val = new OperationResult(ResultTypes.warning, "SIN REGISTROS PARA GUARDAR");
                    imprimir(val);
                    return val;
                }

                var resp = new FacturaServicioSvc().saveFacturaServicio(ref facturaServicios);
                imprimir(resp);
                if (resp.typeResult == ResultTypes.success)
                {
                    Task.Run(() => this.buscarViajes());
                }
                return resp;
            }
            catch (Exception ex)
            {
                var val = new OperationResult(ex);
                imprimir(val);
                return val;
            }
            finally
            {
                Dispatcher.Invoke(() =>
                {
                    popMensaje.IsOpen = false;
                    lblMensaje.Content = "";
                    btnGenerar.IsEnabled = true;
                });
            }
        }
        int diasEvaluados =>
             !this.fechaFin.HasValue ? 0 : Convert.ToInt32((this.fechaFin.Value - this.fechaInicio.Value).TotalDays);
        private void BtnMarcar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                switch ((enumTipoServicioFacturacion)cbxTipoServicio.SelectedItem)
                {
                    case enumTipoServicioFacturacion.VIAJES:
                        controlResultadoViajes.marcarTodos();
                        break;
                    case enumTipoServicioFacturacion.LAVADO_ESPECIALIZADO:
                        break;
                    case enumTipoServicioFacturacion.TRASPASOS:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void BtnDesMarcar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                switch ((enumTipoServicioFacturacion)cbxTipoServicio.SelectedItem)
                {
                    case enumTipoServicioFacturacion.VIAJES:
                        controlResultadoViajes.desMarcarTodos();
                        break;
                    case enumTipoServicioFacturacion.LAVADO_ESPECIALIZADO:
                        break;
                    case enumTipoServicioFacturacion.TRASPASOS:
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
