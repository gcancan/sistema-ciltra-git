﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using CoreFletera.Interfaces;
    using CoreFletera.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Markup;
    using Xceed.Wpf.Toolkit;

    public partial class editCatalogoRutasAtlante : WpfClient.ViewBase
    {
        private bool privilegioGuargar = false;
        private List<Zona> listZonas = new List<Zona>();
        private List<Origen> listOrigenes = new List<Origen>();

        public editCatalogoRutasAtlante()
        {
            this.InitializeComponent();
        }


        private void btnAgregarCasetas_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<Caseta> list = new selectCasetas().getListaCasetas();
                if (list != null)
                {
                    this.llenarListaCasetas(list);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void btnQuitar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.txtClave.Tag != null)
                {
                    base.Cursor = Cursors.Wait;
                    if (this.cbxClientes.SelectedItems.Count != 0)
                    {
                        OrigenDestinos destinos = this.mapRelacion();
                        if (destinos != null)
                        {
                            OperationResult resp = new OperationResult();
                            foreach (Cliente cliente in this.cbxClientes.SelectedItems)
                            {
                                destinos.idCliente = cliente.clave;
                                resp = new ZonasSvc().borrarOrigenDestinoByRuta(destinos.origen.idOrigen, destinos.destino.clave, destinos.idCliente);
                                if (resp.typeResult > ResultTypes.success)
                                {
                                    ImprimirMensaje.imprimir(resp);
                                    return;
                                }
                            }
                            this.seleccionarClientesByRuta(destinos.origen.idOrigen, destinos.destino.clave, (destinos.clasificacion == null) ? null : new int?(destinos.clasificacion.idClasificacionProducto));
                            ImprimirMensaje.imprimir(resp);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void btnQuitarCaseta_Click(object sender, RoutedEventArgs e)
        {
            if (this.txtClave.Tag != null)
            {
                Ruta tag = this.txtClave.Tag as Ruta;
                foreach (Caseta caseta in this.lvlCasetas.SelectedItems)
                {
                    OperationResult resp = new RutaSvc().quitarRelacionRutaCasetas(tag.idRuta, caseta.idCaseta);
                    if (resp.typeResult > ResultTypes.success)
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
                this.buscarCasetasByRuta(tag);
            }
        }

        private void btnSignar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.txtClave.Tag != null)
                {
                    base.Cursor = Cursors.Wait;
                    if (this.cbxClientes.SelectedItems.Count != 0)
                    {
                        OrigenDestinos relacion = this.mapRelacion();
                        if (relacion != null)
                        {
                            OperationResult resp = new OperationResult();
                            foreach (Cliente cliente in this.cbxClientes.SelectedItems)
                            {
                                relacion.idCliente = cliente.clave;
                                resp = new ZonasSvc().saveOrigenDestino(ref relacion);
                                if (resp.typeResult > ResultTypes.success)
                                {
                                    ImprimirMensaje.imprimir(resp);
                                    return;
                                }
                            }
                            this.seleccionarClientesByRuta(relacion.origen.idOrigen, relacion.destino.clave, (relacion.clasificacion == null) ? null : new int?(relacion.clasificacion.idClasificacionProducto));
                            ImprimirMensaje.imprimir(resp);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void buscarCasetasByRuta(Ruta ruta)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                this.lvlCasetas.Items.Clear();
                OperationResult result = new CasetaSvc().getCasetasByIdRuta(ruta.idRuta);
                if (result.typeResult == ResultTypes.success)
                {
                    this.llenarListaCasetas(result.result as List<Caseta>);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void cargarDestinos()
        {
            try
            {
                OperationResult result = new ZonasSvc().getDestinosAtlante(base.mainWindow.empresa.clave);
                if (result.typeResult == ResultTypes.success)
                {
                    this.listZonas = result.result as List<Zona>;
                    this.cbxDestino.ItemsSource = this.listZonas;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void cargarOrigenes()
        {
            try
            {
                OperationResult result = new OrigenSvc().getOrigenesAll();
                if (result.typeResult == ResultTypes.success)
                {
                    this.listOrigenes = result.result as List<Origen>;
                    this.cbxOrigen.ItemsSource = this.listOrigenes;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void cargarRutas()
        {
            try
            {
                OperationResult result = new RutaSvc().getAllRutas();
                if (result.typeResult == ResultTypes.success)
                {
                    this.lvlRutas.ItemsSource = result.result as List<Ruta>;
                    CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlRutas.ItemsSource);
                    PropertyGroupDescription item = new PropertyGroupDescription("origen.baseNombre");
                    defaultView.GroupDescriptions.Add(item);
                    defaultView.Filter = new Predicate<object>(this.UserFilter);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        public override OperationResult guardar()
        {
            OperationResult result2;
            try
            {
                base.Cursor = Cursors.Wait;
                Ruta ruta = this.mapForm();
                if (ruta != null)
                {
                    OperationResult result = new RutaSvc().saveRuta(ref ruta);
                    if (result.typeResult == ResultTypes.success)
                    {
                        if (ruta.activo)
                        {
                            this.mapForm(ruta);
                        }
                        else
                        {
                            this.mapForm(null);
                        }
                        this.cargarRutas();
                        base.mainWindow.habilitar = true;
                    }
                    return result;
                }
                result2 = new OperationResult
                {
                    valor = 1,
                    mensaje = "Revisa Los valores Proporcionados"
                };
            }
            catch (Exception exception)
            {
                result2 = new OperationResult(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return result2;
        }
        private void llenarListaCasetas(List<Caseta> list)
        {
            foreach (Caseta caseta in list)
            {
                //if (this.lvlCasetas.Items.Cast<Caseta>().ToList<Caseta>().Find(s => s.idCaseta == caseta.idCaseta) == null)
                //{
                    this.lvlCasetas.Items.Add(caseta);
                //}
            }
        }

        private void lvUsers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
            {
                Ruta selectedItem = (sender as ListView).SelectedItem as Ruta;
                if (selectedItem != null)
                {
                    this.mapForm(selectedItem);
                    this.seleccionarClientesByRuta(selectedItem.origen.idOrigen, selectedItem.destino.clave, (selectedItem.clasificacion == null) ? null : new int?(selectedItem.clasificacion.idClasificacionProducto));
                    this.buscarCasetasByRuta(selectedItem);
                }
                
            }
        }

        private Ruta mapForm()
        {
            try
            {
                Ruta ruta2 = new Ruta
                {
                    idRuta = (this.txtClave.Tag == null) ? 0 : (this.txtClave.Tag as Ruta).idRuta,
                    cobroPorViaje = this.chcViaje.IsChecked.Value,
                    km = this.txtKm.valor,
                    origen = this.cbxOrigen.SelectedItem as Origen,
                    destino = this.cbxDestino.SelectedItem as Zona
                };
                TarifaRuta ruta1 = new TarifaRuta
                {
                    precioSencillo = this.txtPreSencillo.valor,
                    precioFull = this.txtPreFull.valor
                };
                ruta2.tarifaRuta = ruta1;
                PagoOperador operador1 = new PagoOperador
                {
                    precioSencillo = this.txtOperadorSencillo.valor,
                    precioFull = this.txtOperadorFull.valor,
                    tipoPago = (ClasificacionPagoChofer)this.cbxTipoPagoOperador.SelectedItem
                };
                ruta2.pagoOperador = operador1;
                ruta2.casetas = new CasetaRuta();
                ruta2.activo = this.chcActivo.IsChecked.Value;
                ruta2.listCaseta = this.lvlCasetas.Items.Cast<Caseta>().ToList<Caseta>();
                ruta2.clasificacion = (this.cbxClasificacion.SelectedItem == null) ? null : (this.cbxClasificacion.SelectedItem as ClasificacionProductos);
                return ruta2;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void mapForm(Ruta ruta)
        {
            try
            {
                if (ruta != null)
                {
                    this.txtClave.valor = ruta.idRuta;
                    this.txtClave.Tag = ruta;
                    this.selectOrigen(ruta.origen);
                    this.selectDestino(ruta.destino);
                    this.chcViaje.IsChecked = new bool?(ruta.cobroPorViaje);
                    this.txtPreSencillo.valor = ruta.tarifaRuta.precioSencillo;
                    this.txtPreFull.valor = ruta.tarifaRuta.precioFull;
                    this.txtKm.valor = ruta.km;
                    this.txtOperadorSencillo.valor = ruta.pagoOperador.precioSencillo;
                    this.txtOperadorFull.valor = ruta.pagoOperador.precioFull;
                    this.cbxTipoPagoOperador.SelectedItem = ruta.pagoOperador.tipoPago;
                    this.chcActivo.IsChecked = new bool?(ruta.activo);
                    if (ruta.clasificacion != null)
                    {
                        this.cbxClasificacion.SelectedItem = this.cbxClasificacion.ItemsSource.Cast<ClasificacionProductos>().ToList<ClasificacionProductos>().Find(s => s.idClasificacionProducto == ruta.clasificacion.idClasificacionProducto);
                    }
                    else
                    {
                        this.cbxClasificacion.SelectedItem = null;
                    }
                }
                else
                {
                    this.txtClave.limpiar();
                    this.txtClave.Tag = null;
                    this.cbxOrigen.SelectedItem = null;
                    this.cbxOrigen.IsEnabled = true;
                    this.cbxDestino.SelectedItem = null;
                    this.cbxDestino.IsEnabled = true;
                    this.chcViaje.IsChecked = false;
                    this.txtPreSencillo.valor = decimal.Zero;
                    this.txtPreFull.valor = decimal.Zero;
                    this.txtKm.valor = decimal.Zero;
                    this.txtOperadorSencillo.valor = decimal.Zero;
                    this.txtOperadorFull.valor = decimal.Zero;
                    this.cbxTipoPagoOperador.SelectedIndex = 0;
                    this.chcActivo.IsChecked = true;
                    this.cbxClientes.SelectedItems.Clear();
                    this.lvlCasetas.Items.Clear();
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private OrigenDestinos mapRelacion()
        {
            try
            {
                return new OrigenDestinos
                {
                    idOrigenDestino = 0,
                    origen = (Origen)this.cbxOrigen.SelectedItem,
                    destino = (Zona)this.cbxDestino.SelectedItem,
                    clasificacion = (this.cbxClasificacion.SelectedItem == null) ? null : (this.cbxClasificacion.SelectedItem as ClasificacionProductos),
                    precioSencillo = this.txtPreSencillo.valor,
                    precioFull = this.txtPreFull.valor,
                    km = this.txtKm.valor,
                    viaje = this.chcViaje.IsChecked.Value,
                    idCliente = 0,
                    viajeSencillo = this.txtOperadorSencillo.valor,
                    viajeFull = this.txtOperadorFull.valor,
                    tipoPago = (ClasificacionPagoChofer)this.cbxTipoPagoOperador.SelectedItem
                };
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return null;
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            if (this.privilegioGuargar)
            {
                base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
                this.stpAccionesCasetas.IsEnabled = true;
            }
            else
            {
                base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                this.stpAccionesCasetas.IsEnabled = false;
            }
            this.mapForm(null);
        }

        private void seleccionarClientesByRuta(int idOrigen, int idDestino, int? idClasificacion)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult result = new ClienteSvc().getClientesByRuta(idOrigen, idDestino, idClasificacion);
                if (result.typeResult == ResultTypes.success)
                {
                    List<Cliente> listCliente = result.result as List<Cliente>;
                    this.selectClientes(listCliente);
                }
                else
                {
                    this.cbxClientes.SelectedItems.Clear();
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void selectClientes(List<Cliente> listCliente)
        {
            this.cbxClientes.SelectedItems.Clear();
            foreach (Cliente cliente in listCliente)
            {
                foreach (Cliente cliente2 in (IEnumerable)this.cbxClientes.Items)
                {
                    if (cliente2.clave == cliente.clave)
                    {
                        this.cbxClientes.SelectedItems.Add(cliente2);
                    }
                }
            }
        }

        private void selectDestino(Zona zona)
        {
            int num = 0;
            foreach (Zona zona2 in (IEnumerable)this.cbxDestino.Items)
            {
                if (zona2.clave == zona.clave)
                {
                    this.cbxDestino.SelectedIndex = num;
                    this.cbxDestino.IsEnabled = false;
                    break;
                }
                num++;
            }
        }

        private void selectOrigen(Origen origen)
        {
            int num = 0;
            foreach (Origen origen2 in (IEnumerable)this.cbxOrigen.Items)
            {
                if (origen2.idOrigen == origen.idOrigen)
                {
                    this.cbxOrigen.SelectedIndex = num;
                    this.cbxOrigen.IsEnabled = false;
                    break;
                }
                num++;
            }
        }

        private void txtOrigen_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            if (this.lvlRutas.ItemsSource != null)
            {
                CollectionViewSource.GetDefaultView(this.lvlRutas.ItemsSource).Refresh();
            }
        }

        private bool UserFilter(object item) =>
            (((item as Ruta).origen.baseNombre.IndexOf(this.txtOrigen.Text, StringComparison.OrdinalIgnoreCase) >= 0) && ((item as Ruta).destino.descripcion.IndexOf(this.txtDestino.Text, StringComparison.OrdinalIgnoreCase) >= 0));

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                this.cargarOrigenes();
                this.cargarDestinos();
                this.cargarRutas();
                this.cbxTipoPagoOperador.ItemsSource = Enum.GetValues(typeof(ClasificacionPagoChofer));
                this.cbxTipoPagoOperador.SelectedIndex = 0;
                OperationResult result = new ClienteSvc().getClientesALLorById(base.mainWindow.empresa.clave, 0, true);
                if (result.typeResult == ResultTypes.success)
                {
                    this.cbxClientes.ItemsSource = result.result as List<Cliente>;
                }
                if (new PrivilegioSvc().consultarPrivilegio("GUARDAR_RUTA_ATL", base.mainWindow.inicio._usuario.idUsuario).typeResult == ResultTypes.success)
                {
                    this.privilegioGuargar = true;
                }
                else
                {
                    this.privilegioGuargar = false;
                }
                OperationResult result3 = new ClasificacionProductosSvc().getAllClasificacionProductos();
                if (result3.typeResult == ResultTypes.success)
                {
                    this.cbxClasificacion.ItemsSource = result3.result as List<ClasificacionProductos>;
                }
                this.nuevo();
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (lvlRutas.ItemsSource == null)
                    return;

                var lista = lvlRutas.ItemsSource as List<Ruta>;
                new ReportView().exportarListaRutasAtlante(lista);

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
