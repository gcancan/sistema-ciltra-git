﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for confirmacionCargaCombustible.xaml
    /// </summary>
    public partial class confirmacionCargaCombustible : Window
    {
        CargaCombustible carga = new CargaCombustible();
        public confirmacionCargaCombustible()
        {
            InitializeComponent();
        }
        public confirmacionCargaCombustible(CargaCombustible carga)
        {

            InitializeComponent();
            this.carga = carga;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtLtsTotales.Content = carga.ltCombustibleUtilizado.ToString();
            txtLtsCargados.Content = carga.ltCombustible.ToString();
            txtKmTotales.Content = carga.kmDespachador.ToString();
            txtKmRecorridos.Content = carga.kmRecorridos.ToString();
            if (carga.isCR)
            {
                stpLitrosTotales.Visibility = Visibility.Hidden;
                lblKmFinal.Content = "H. FINAL:";
                lblKmRecorridos.Content = "H. OPERATIVAS:";
            }
        }
        private void btnSi_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void btnNo_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        
    }
}
