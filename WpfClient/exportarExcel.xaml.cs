﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Runtime.InteropServices;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Markup;
    using System.Windows.Media;

    public partial class exportarExcel : WpfClient.ViewBase
    {
        private bool mostrarImportes;
        private int idCliente;
        private int claveEmpresa;
        private decimal sumaImportes;
        private decimal sumaIva;
        private decimal sumaRetencion;
        private Empresa empresa;
        private DateTime? fechaInicial;
        private DateTime? fechaFinal;
        private string tractor;
        
        public exportarExcel()
        {
            this.mostrarImportes = false;
            this.idCliente = 0;
            this.claveEmpresa = 0;
            this.sumaImportes = 0M;
            this.sumaIva = 0M;
            this.sumaRetencion = 0M;
            this.fechaInicial = null;
            this.fechaFinal = null;
            this.InitializeComponent();
        }

        public exportarExcel(MainWindow mainWindow, Empresa empresa, DateTime fechaInicial, DateTime fechaFinal, string tractor, int idCliente = 0)
        {
            this.mostrarImportes = false;
            this.idCliente = 0;
            this.claveEmpresa = 0;
            this.sumaImportes = 0M;
            this.sumaIva = 0M;
            this.sumaRetencion = 0M;
            this.fechaInicial = null;
            this.fechaFinal = null;
            base.mainWindow = mainWindow;
            this.idCliente = idCliente;
            this.empresa = empresa;
            this.fechaInicial = new DateTime?(fechaInicial);
            this.fechaFinal = new DateTime?(fechaFinal);
            this.tractor = tractor;
            this.InitializeComponent();
        }
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            this.buscarCartasPorte();
        }

        private void btnGenerarReporte_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.lvlSalidas.Items.Count <= 0)
                {
                    MessageBox.Show("Se requieren resultados para proceder", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    base.Cursor = Cursors.Wait;
                    ReportView view = new ReportView(base.mainWindow);
                    if (view.documentoReporte(this.getListCartaPOrtes(), this.dtpFechaInicio.SelectedDate.Value, this.dtpFechaFin.SelectedDate.Value, (Cliente)this.cbxClientes.SelectedItem, this.mostrarImportes))
                    {
                    }
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        private void btnGenerarReportePersonalizado_Click(object sender, RoutedEventArgs e)
        {
            if (this.lvlSalidas.Items.Count <= 0)
            {
                MessageBox.Show("Se requieren resultados para proceder", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                new ReportesPersonalizados(this.getListCartaPOrtes(), (Cliente)this.cbxClientes.SelectedItem, base.mainWindow.inicio._usuario, this.dtpFechaInicio.SelectedDate.Value, this.dtpFechaFin.SelectedDate.Value, this.mostrarImportes) { Owner = base.mainWindow }.ShowDialog();
            }
        }

        private void btnGenerarReporteViaje_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.lvlSalidas.Items.Count <= 0)
                {
                    MessageBox.Show("Se requieren resultados para proceder", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    base.Cursor = Cursors.Wait;
                    ReportView view = new ReportView(base.mainWindow);
                    if (view.documentoReporteViajes(this.getListCartaPOrtes(), this.dtpFechaInicio.SelectedDate.Value, this.dtpFechaFin.SelectedDate.Value, (Cliente)this.cbxClientes.SelectedItem, false))
                    {
                    }
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        private void btnGenerarReporteViajesPersonalizado_Click(object sender, RoutedEventArgs e)
        {
            if (this.lvlSalidas.Items.Count <= 0)
            {
                MessageBox.Show("Se requieren resultados para proceder", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                new ReportesPersonalizadosViajes(this.getListCartaPOrtes(), base.mainWindow.inicio._usuario, (Cliente)this.cbxClientes.SelectedItem, this.dtpFechaInicio.SelectedDate.Value, this.dtpFechaFin.SelectedDate.Value, this.mostrarImportes) { Owner = base.mainWindow }.ShowDialog();
            }
        }

        internal void buscarCartasPorte()
        {
            try
            {
                base.Cursor = Cursors.Wait;
                if (this.idCliente != 0)
                {
                    int num = 0;
                    foreach (Cliente cliente in (IEnumerable)this.cbxClientes.Items)
                    {
                        if (cliente.clave == this.idCliente)
                        {
                            this.cbxClientes.SelectedIndex = num;
                            break;
                        }
                        num++;
                    }
                }
                this.calcularImportes(null);
                string tipoViaje = "%";
                if (this.rbnSencillo.IsChecked.Value)
                {
                    tipoViaje = "s";
                }
                else if (this.rbnFull.IsChecked.Value)
                {
                    tipoViaje = "f";
                }
                this.lvlSalidas.Items.Clear();
                List<Viaje> list = new List<Viaje>();
                OperationResult result = new CartaPorteSvc().getCartaPorteByfiltros(this.dtpFechaInicio.SelectedDate.Value, this.dtpFechaFin.SelectedDate.Value.AddDays(1.0), (this.cbxDestino.SelectedIndex == 0) ? "%" : ((Zona)this.cbxDestino.SelectedItem).clave.ToString(), (this.cbxOperadores.SelectedIndex == 0) ? "%" : ((Personal)this.cbxOperadores.SelectedItem).clave.ToString(), (this.cbxCamion.SelectedIndex == 0) ? "%" : ((UnidadTransporte)this.cbxCamion.SelectedItem).clave, (this.cbxEstatus.SelectedIndex == 0) ? "%" : this.cbxEstatus.SelectedItem.ToString(), this.chcTodosClientes.IsChecked.Value ? "%" : ((Cliente)this.cbxClientes.SelectedItem).clave.ToString(), ((Empresa)this.cbxEmpresa.SelectedItem).clave, tipoViaje);
                if (result.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
                }
                else if (result.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else if ((result.typeResult != ResultTypes.recordNotFound) && (result.typeResult == ResultTypes.success))
                {
                    foreach (Viaje viaje in (List<Viaje>)result.result)
                    {
                        list.Add(viaje);
                    }
                }
                if (list.Count == 0)
                {
                    MessageBox.Show("No se encontraron registros", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    this.mapLista(list);
                }
                this.lvlProductos.Items.Clear();
            }
            catch (Exception exception)
            {
                OperationResult resp = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message
                };
                ImprimirMensaje.imprimir(resp);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void calcularImportes(CartaPorte det)
        {
            if (det != null)
            {
                this.sumaImportes += det.importeReal;
                this.sumaIva += det.ImporIVA;
                this.sumaRetencion += det.ImpRetencion;
            }
            else
            {
                this.sumaImportes = 0M;
                this.sumaIva = 0M;
                this.sumaRetencion = 0M;
            }
            this.lblSumaImportes.Content = this.sumaImportes.ToString("C4");
            this.lblRetencion.Content = this.sumaRetencion.ToString("C4");
            this.lblIVA.Content = this.sumaIva.ToString("C4");
            this.lblTOTAL.Content = (this.sumaImportes + (this.sumaIva - this.sumaRetencion)).ToString("C4");
        }

        private void cbxClientes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender != null)
            {
                if (this.cbxClientes.SelectedItem != null)
                {
                    Cliente selectedItem = (Cliente)this.cbxClientes.SelectedItem;
                    List<Zona> result = (List<Zona>)new ZonasSvc().getZonasALLorById(0, ((Cliente)this.cbxClientes.SelectedItem).clave.ToString()).result;
                    this.lvlProductos.Items.Clear();
                    this.lvlSalidas.Items.Clear();
                    this.cbxDestino.Items.Clear();
                    Zona newItem = new Zona
                    {
                        clave = 0,
                        descripcion = "TODOS"
                    };
                    this.cbxDestino.Items.Add(newItem);
                    foreach (Zona zona in result)
                    {
                        this.cbxDestino.Items.Add(zona);
                    }
                    this.cbxDestino.SelectedIndex = 0;
                }
                else
                {
                    this.cbxDestino.Items.Clear();
                }
            }
        }

        private void cbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.cbxEmpresa.SelectedItem != null)
            {
                this.mapOperadores();
                this.mapCliente();
                this.mapTractores();
            }
        }

        public override void cerrar()
        {
            base.cerrar();
        }

        private List<Viaje> convertListViajes(List<ViajeBachoco> result)
        {
            List<Viaje> list = new List<Viaje>();
            try
            {
                foreach (ViajeBachoco bachoco in result)
                {
                    Viaje item = new Viaje
                    {
                        NumGuiaId = bachoco.idViaje,
                        NumViaje = bachoco.idViaje,
                        cliente = bachoco.cliente,
                        tractor = bachoco.tractor,
                        remolque = bachoco.tolva1,
                        dolly = bachoco.dolly,
                        remolque2 = bachoco.tolva2,
                        FechaHoraViaje = bachoco.fechaViaje,
                        operador = bachoco.chofer,
                        EstatusGuia = bachoco.estatus.ToString(),
                        UserViaje = bachoco.usuarioBascula.idUsuario,
                        isBachoco = true,
                        listDetalles = new List<CartaPorte>()
                    };
                    foreach (CartaPorteBachoco bachoco2 in bachoco.listaCartaPortes)
                    {
                        CartaPorte porte1 = new CartaPorte
                        {
                            Consecutivo = bachoco2.idCartaPorte,
                            zonaSelect = bachoco2.destino,
                            producto = bachoco2.producto,
                            PorcIVA = bachoco2.iva,
                            PorcRetencion = bachoco2.retencion,
                            precio = bachoco2.precio,
                            volumenDescarga = bachoco2.volumenCarga,
                            numGuiaId = bachoco2.idViaje,
                            volumenCarga = decimal.Zero,
                            remision = bachoco2.remision
                        };
                        item.listDetalles.Add(porte1);
                    }
                    list.Add(item);
                }
                return list;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private TipoPrecio establecerPrecio(Viaje viaje)
        {
            if ((viaje.dolly != null) || (viaje.remolque2 != null))
            {
                return TipoPrecio.FULL;
            }
            if (viaje.remolque.tipoUnidad.descripcion == "TOLVA 35 TON")
            {
                return TipoPrecio.SENCILLO35;
            }
            return TipoPrecio.SENCILLO;
        }

        private void Generar_Full_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.lvlSalidas.Items.Count <= 0)
                {
                    MessageBox.Show("Se requieren resultados para proceder", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    base.Cursor = Cursors.Wait;
                    ReportView view = new ReportView(base.mainWindow);
                    if (!view.documentoReporteFull(this.getListCartaPOrtes(), this.dtpFechaInicio.SelectedDate.Value, this.dtpFechaFin.SelectedDate.Value))
                    {
                        MessageBox.Show("Ocurrio un error al intentar crear el reporte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Hand);
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private List<Viaje> getListCartaPOrtes()
        {
            List<Viaje> list = new List<Viaje>();
            foreach (ListViewItem item in (IEnumerable)this.lvlSalidas.Items)
            {
                Viaje content = (Viaje)item.Content;
                if (content.EstatusGuia != "CANCELADO")
                {
                    list.Add(content);
                }
            }
            return list;
        }

        private void ItemMenu_Click(object sender, RoutedEventArgs e)
        {
            int tag = (int)((MenuItem)sender).Tag;
            new ventanaGeneral(base.mainWindow).abrirInfoViajes(tag);
        }

        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            this.lvlProductos.Items.Clear();
            Viaje content = (Viaje)((ListViewItem)sender).Content;
            this.mapDetalles(content);
        }

        private void mapCliente()
        {
            List<Cliente> result = (List<Cliente>)new ClienteSvc().getClientesALLorById(((Empresa)this.cbxEmpresa.SelectedItem).clave, 0, true).result;
            this.mapComboClientes(result);
        }

        private void mapComboClientes(List<Cliente> listCliente)
        {
            this.cbxClientes.Items.Clear();
            foreach (Cliente cliente in listCliente)
            {
                this.cbxClientes.Items.Add(cliente);
            }
        }

        private void mapCombos(List<UnidadTransporte> list)
        {
            foreach (UnidadTransporte transporte in list)
            {
                if (transporte.tipoUnidad.clasificacion.ToUpper() == "TRACTOR")
                {
                    this.cbxCamion.Items.Add(transporte);
                }
            }
            if (string.IsNullOrEmpty(this.tractor))
            {
                this.cbxCamion.SelectedIndex = 0;
            }
            else
            {
                int num = 0;
                foreach (UnidadTransporte transporte2 in (IEnumerable)this.cbxCamion.Items)
                {
                    if (transporte2.clave == this.tractor)
                    {
                        this.cbxCamion.SelectedIndex = num;
                        break;
                    }
                    num++;
                }
            }
        }

        private void mapDetalles(Viaje cartaPorte)
        {
            foreach (CartaPorte porte in cartaPorte.listDetalles)
            {
                porte.tipoPrecio = this.establecerPrecio(cartaPorte);
                ListViewItem newItem = new ListViewItem
                {
                    Content = porte,
                    FontWeight = FontWeights.Bold
                };
                this.lvlProductos.Items.Add(newItem);
            }
        }

        private void mapLista(List<Viaje> result)
        {
            foreach (Viaje viaje in result)
            {
                foreach (CartaPorte porte in viaje.listDetalles)
                {
                    porte.tipoPrecio = this.establecerPrecio(viaje);
                    if (viaje.EstatusGuia != "CANCELADO")
                    {
                        this.calcularImportes(porte);
                    }
                }
                bool flag = this.validarIsFull(viaje);
                if (!this.rbnTodos.IsChecked.Value)
                {
                    bool flag4 = flag;
                    if (flag4)
                    {
                        if (!flag4 || this.rbnFull.IsChecked.Value)
                        {
                            goto Label_00FD;
                        }
                        continue;
                    }
                    if (!this.rbnSencillo.IsChecked.Value)
                    {
                        continue;
                    }
                }
                Label_00FD:
                if (this.cbxArea.SelectedIndex != 0)
                {
                    bool flag8 = false;
                    foreach (Viaje viaje2 in result)
                    {
                        bool flag9 = false;
                        foreach (CartaPorte porte2 in viaje.listDetalles)
                        {
                            if (porte2.zonaSelect.claveZona != this.cbxArea.SelectedItem.ToString())
                            {
                                flag9 = true;
                                break;
                            }
                        }
                        if (flag9)
                        {
                            flag8 = true;
                            break;
                        }
                    }
                    if (flag8)
                    {
                        continue;
                    }
                }
                ListViewItem newItem = new ListViewItem
                {
                    Content = viaje,
                    FontWeight = FontWeights.Bold
                };
                newItem.Selected += new RoutedEventHandler(this.Lvl_Selected);
                if (viaje.EstatusGuia.ToUpper() == "ACTIVO")
                {
                    newItem.Foreground = Brushes.SteelBlue;
                }
                if (viaje.EstatusGuia.ToUpper() == "CANCELADO")
                {
                    newItem.Foreground = Brushes.OrangeRed;
                }
                if (viaje.EstatusGuia.ToUpper() == "FINALIZADO")
                {
                    newItem.Foreground = Brushes.Blue;
                }
                ContextMenu menu = new ContextMenu();
                MenuItem item2 = new MenuItem
                {
                    Header = "Ver Informaci\x00f3n Del Viaje"
                };
                item2.Tag = viaje.NumGuiaId;
                item2.Click += new RoutedEventHandler(this.ItemMenu_Click);
                menu.Items.Add(item2);
                newItem.ContextMenu = menu;
                this.lvlSalidas.Items.Add(newItem);
            }
        }

        public void mapOperadores()
        {
            OperationResult result = new OperadorSvc().getOperadoresALLorById(0, ((Empresa)this.cbxEmpresa.SelectedItem).clave);
            if (result.typeResult == ResultTypes.error)
            {
                MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
            }
            else if (result.typeResult == ResultTypes.warning)
            {
                MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else if (result.typeResult == ResultTypes.recordNotFound)
            {
                MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else if (result.typeResult == ResultTypes.success)
            {
                List<Personal> list = (List<Personal>)result.result;
                Personal newItem = new Personal
                {
                    nombre = "TODOS",
                    clave = 0
                };
                this.cbxOperadores.Items.Add(newItem);
                foreach (Personal personal in list)
                {
                    this.cbxOperadores.Items.Add(personal);
                }
            }
            this.cbxOperadores.SelectedIndex = 0;
        }

        private void mapTractores()
        {
            OperationResult result = new UnidadTransporteSvc().getUnidadesALLorById(((Empresa)this.cbxEmpresa.SelectedItem).clave, "");
            if (result.typeResult == ResultTypes.error)
            {
                MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
            }
            else if (result.typeResult == ResultTypes.warning)
            {
                MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else if (result.typeResult == ResultTypes.recordNotFound)
            {
                MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else if (result.typeResult == ResultTypes.success)
            {
                List<UnidadTransporte> list = (List<UnidadTransporte>)result.result;
                this.cbxCamion.Items.Clear();
                UnidadTransporte newItem = new UnidadTransporte
                {
                    clave = "TODOS"
                };
                Core.Models.TipoUnidad unidad1 = new Core.Models.TipoUnidad
                {
                    clave = 0,
                    descripcion = ""
                };
                newItem.tipoUnidad = unidad1;
                this.cbxCamion.Items.Add(newItem);
                this.mapCombos(list);
            }
        }
        private bool validarIsFull(Viaje cartaPorte) =>
            ((cartaPorte.remolque.tipoUnidad.descripcion == "TOLVA 35 TON") || ((cartaPorte.dolly != null) || (cartaPorte.remolque2 != null)));

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.calcularImportes(null);
            if (this.fechaInicial.HasValue)
            {
                this.claveEmpresa = this.empresa.clave;
                this.dtpFechaInicio.SelectedDate = this.fechaInicial;
                this.dtpFechaFin.SelectedDate = this.fechaFinal;
                this.dtpFechaInicio.IsEnabled = false;
                this.dtpFechaFin.IsEnabled = false;
                if (this.idCliente == 0)
                {
                    this.cbxClientes.Visibility = Visibility.Collapsed;
                    this.chcTodosClientes.IsChecked = true;
                }
                this.lvlSalidas.Height = 300.0;
                GridView view = this.lvlSalidas.View as GridView;
                GridViewColumnCollection columns = view.Columns;
                GridViewColumn item = new GridViewColumn
                {
                    Header = "Importe",
                    Width = 120.0
                };
                Binding binding1 = new Binding("importe")
                {
                    StringFormat = "C4"
                };
                item.DisplayMemberBinding = binding1;
                view.Columns.Add(item);
                GridViewColumn column2 = new GridViewColumn
                {
                    Header = "Casetas",
                    Width = 120.0
                };
                Binding binding2 = new Binding("casetas")
                {
                    StringFormat = "C4"
                };
                column2.DisplayMemberBinding = binding2;
                view.Columns.Add(column2);
            }
            else
            {
                this.claveEmpresa = base.mainWindow.inicio._usuario.personal.empresa.clave;
                base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                this.dtpFechaInicio.SelectedDate = new DateTime?(Convert.ToDateTime(DateTime.Now.ToShortDateString()));
                string nombre = base.mainWindow.inicio._usuario.personal.nombre;
                this.dtpFechaFin.SelectedDate = new DateTime?(Convert.ToDateTime(DateTime.Now.ToShortDateString()));
            }
            this.cbxEstatus.Items.Add("TODOS");
            this.cbxEstatus.Items.Add("ACTIVO");
            this.cbxEstatus.Items.Add("EXTERNO");
            this.cbxEstatus.Items.Add("CANCELADO");
            this.cbxEstatus.Items.Add("FINALIZADO");
            this.cbxEstatus.Items.Add("FACTURADO");
            this.cbxEstatus.SelectedIndex = 0;
            this.cbxArea.Items.Add("TODOS");
            this.cbxArea.Items.Add("A");
            this.cbxArea.Items.Add("B");
            this.cbxArea.Items.Add("C");
            this.cbxArea.Items.Add("D");
            this.cbxArea.Items.Add("E");
            this.cbxArea.SelectedIndex = 0;
            OperationResult resp = new EmpresaSvc().getEmpresasALLorById(0);
            if (resp.typeResult == ResultTypes.success)
            {
                this.cbxEmpresa.ItemsSource = (List<Empresa>)resp.result;
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
            }
            int num = 0;
            foreach (Empresa empresa in (IEnumerable)this.cbxEmpresa.Items)
            {
                if (empresa.clave == ((this.empresa != null) ? this.empresa.clave : base.mainWindow.empresa.clave))
                {
                    this.cbxEmpresa.SelectedIndex = num;
                    break;
                }
                num++;
            }
            num = 0;
            if (this.fechaInicial.HasValue)
            {
                this.cbxClientes.SelectedIndex = 0;
                this.controlBusqueda.IsEnabled = false;
                this.stpArriba.IsEnabled = false;
                this.mostrarImportes = true;
                this.buscarCartasPorte();
            }
            else
            {
                foreach (Cliente cliente in (IEnumerable)this.cbxClientes.Items)
                {
                    if (cliente.clave == base.mainWindow.inicio._usuario.cliente.clave)
                    {
                        this.cbxClientes.SelectedIndex = num;
                        break;
                    }
                    num++;
                }
                OperationResult result2 = new PrivilegioSvc().consultarPrivilegio("ISADMIN", base.mainWindow.inicio._usuario.idUsuario);
                bool flag6 = false;
                if (result2.typeResult == ResultTypes.success)
                {
                    flag6 = true;
                }
                result2 = new PrivilegioSvc().consultarPrivilegio("ADMIN_COMBO_CLIENTES", base.mainWindow.inicio._usuario.idUsuario);
                bool flag7 = false;
                if (result2.typeResult == ResultTypes.success)
                {
                    flag7 = true;
                }
                this.cbxEmpresa.IsEnabled = flag6;
                this.cbxClientes.IsEnabled = flag7;
                if (new PrivilegioSvc().consultarPrivilegio("MOSTRAR_IMPORTES_REPORTES", base.mainWindow.inicio._usuario.idUsuario).typeResult == ResultTypes.success)
                {
                    this.stpImportes.Visibility = Visibility.Visible;
                    this.mostrarImportes = true;
                }
                else
                {
                    this.stpImportes.Visibility = Visibility.Hidden;
                    this.mostrarImportes = false;
                }
                if (new PrivilegioSvc().consultarPrivilegio("MOSTRAR_REPORTES_COBRANZA", base.mainWindow.inicio._usuario.idUsuario).typeResult == ResultTypes.success)
                {
                    this.btnGenerarReporte.Visibility = Visibility.Visible;
                    this.btnGenerarReportePersonalizado.Visibility = Visibility.Visible;
                }
                else
                {
                    this.btnGenerarReporte.Visibility = Visibility.Collapsed;
                    this.btnGenerarReportePersonalizado.Visibility = Visibility.Collapsed;
                }
                if (new PrivilegioSvc().consultarPrivilegio("MOSTRAR_REPORTE_COBRANZA_FULL", base.mainWindow.inicio._usuario.idUsuario).typeResult == ResultTypes.success)
                {
                    this.Generar_Full.Visibility = Visibility.Visible;
                }
                else
                {
                    this.Generar_Full.Visibility = Visibility.Collapsed;
                }
            }
        }
    }
}
