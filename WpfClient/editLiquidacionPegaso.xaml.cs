﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para LiquidacionPegaso.xaml
    /// </summary>
    public partial class editLiquidacionPegaso : ViewBase
    {
        public editLiquidacionPegaso()
        {
            InitializeComponent();
            cargarControles();
        }
        bool isVentana = false;
        public OperationResult result = new OperationResult() { valor = 3};
        public editLiquidacionPegaso(LiquidacionPegaso liquidacion, MainWindow main)
        {
            
            InitializeComponent();
            mainWindow = main;
            cargarControles();
            this.liquidacion = liquidacion;
            isVentana = true;
            mapForm(liquidacion);
        }

        void cargarControles()
        {
            ctrEmpresa.DataContextChanged += CtrEmpresa_DataContextChanged;
            ctrEmpresa.loaded(mainWindow.empresa);
            ctrOperador.DataContextChanged += CtrOperador_DataContextChanged;
            txtRescates.txtDecimal.TextChanged += TxtDecimal_TextChanged;
            txtViajes60.txtDecimal.TextChanged += TxtDecimal_TextChanged1;
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {            
            if (!isVentana)
            {
                mapSueldoFijo(null);
                nuevo();
            }
            else
            {
                gBoxBucador.IsEnabled = false;
                mainWindow.enableToolBarCommands(ToolbarCommands.guardar | ToolbarCommands.cerrar);
                ctrFechas.mostrarFechas(liquidacion.fechaInicio, liquidacion.fechaFin);
            }
            
        }
        public override void cerrar()
        {
            if (isVentana)
            {
                mainWindow.DialogResult = false;
            }
        }

        private void TxtDecimal_TextChanged1(object sender, TextChangedEventArgs e)
        {
            actualizarLista();
        }

        private void TxtDecimal_TextChanged(object sender, TextChangedEventArgs e)
        {
            actualizarLista();
        }

        private void CtrEmpresa_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            ctrOperador.loaded(eTipoBusquedaPersonal.OPERADORES, ctrEmpresa.empresaSelected);
        }

        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);

        }
        bool buscar = true;
        private void CtrOperador_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (ctrOperador.personal != null)
            {
                if (buscar)
                {
                    mapSueldoFijo(buscarSueldoFijo(ctrOperador.personal));
                }
                btnBuscar.Focus();
            }
            else
            {
                mapSueldoFijo(null);
            }
            buscar = true;
        }

        SueldoFijo buscarSueldoFijo(Personal operador)
        {
            var resp = new SueldoFijoSvc().getAllSueldoFijoOrByIdOperador(operador.clave.ToString());
            if (resp.typeResult == ResultTypes.success)
            {
                SueldoFijo sueldo = resp.result as SueldoFijo;
                //mapSueldoFijo(sueldo);
                return sueldo;
            }
            else if (resp.typeResult == ResultTypes.recordNotFound)
            {
                //mapSueldoFijo(null);
                return null;
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
                //mapSueldoFijo(null);
                return null;
            }
        }

        void mapSueldoFijo(SueldoFijo sueldo)
        {
            if (sueldo != null)
            {
                txtSueldoFijo.Tag = sueldo;
                txtSueldoFijo.Text = sueldo.importe.ToString("C2");
            }
            else
            {
                decimal v = 0;
                txtSueldoFijo.Tag = null;
                txtSueldoFijo.Text = v.ToString("C2");
            }
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            List<LiquidacionPegaso> listaliqui = new List<LiquidacionPegaso>();
            var r = new OperadorSvc().getOperadoresViaje(ctrFechas.fechaInicial, ctrFechas.fechaFinal);
            if (r.typeResult == ResultTypes.success)
            {
                List<LiquidacionPegaso> listaLiquidacion = new List<LiquidacionPegaso>();
                foreach (Personal chofer in r.result as List<Personal>)
                {
                    var respV = new CartaPorteSvc().getViajesOperador(chofer.clave, ctrFechas.fechaInicial, ctrFechas.fechaFinal);

                    SueldoFijo sueldo = null;

                    if (respV.typeResult == ResultTypes.success)
                    {
                        var respSueldo = new SueldoFijoSvc().getAllSueldoFijoOrByIdOperador(chofer.clave.ToString());
                        if (respSueldo.typeResult == ResultTypes.success)
                        {
                            sueldo = respSueldo.result as SueldoFijo;
                        }
                        else if (respSueldo.typeResult != ResultTypes.recordNotFound)
                        {
                            ImprimirMensaje.imprimir(respSueldo);
                            return;
                        }                       

                        var listaDetalles = mapLista2(respV.result as List<Viaje>);
                        LiquidacionPegaso liquidacion = new LiquidacionPegaso
                        {
                            idLiquidacion = 0,
                            fecha = DateTime.Now,
                            fechaInicio = ctrFechas.fechaInicial,
                            fechaFin = ctrFechas.fechaFinal.AddDays(-1),
                            operador = chofer,
                            //sumaModalidad = (decimal)txtSumaImportes.Tag,
                            extraSegregacion = (decimal)txtSegregacion.Tag,
                            sueldoFijoObj = sueldo,
                            //importePagado = (decimal)txtTotalPagas.Tag,
                            //sumaExtras = getCantidadExtras(),
                            realiza = mainWindow.inicio._usuario.nombreUsuario
                        };
                        liquidacion.listDetalles = new List<DetalleLiquidacion_old>();
                        liquidacion.listDetalles = listaDetalles;
                        listaLiquidacion.Add(liquidacion);
                    }
                }
            }
            return;
            if (ctrOperador.personal == null)
            {
                ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "NO SE ELIGIO AL OPERADOR" });
                ctrOperador.txtPersonal.Focus();
                return;
            }
            var resp = new CartaPorteSvc().getCartaPorteByfiltros(ctrFechas.fechaInicial, ctrFechas.fechaFinal,
                "%", ctrOperador.personal.clave.ToString(), "%", "FINALIZADO", "%", ctrEmpresa.empresaSelected.clave, "%");
            if (resp.typeResult == ResultTypes.success)
            {
                mapLista(resp.result as List<Viaje>);
            }
            else if (resp.typeResult == ResultTypes.recordNotFound)
            {
                mapLista(new List<Viaje>());
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
                mapLista(new List<Viaje>());
            }
        }

        private List<DetalleLiquidacion_old> mapLista2(List<Viaje> listViaje)
        {
            List<DetalleLiquidacion_old> lista = new List<DetalleLiquidacion_old>();
            foreach (Viaje viaje in listViaje)
            {
                DetalleLiquidacion_old det = new DetalleLiquidacion_old
                {
                    idDetalleLiquidacion = 0,
                    viaje = viaje
                };
                lista.Add(det);
            }
            return lista;
            
        }

        private void mapLista(List<Viaje> list)
        {
            lvlSalidas.Items.Clear();
            listaDetLiquidaciones = new List<DetalleLiquidacion_old>();
            foreach (Viaje viaje in list)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = viaje;
                lvlSalidas.Items.Add(lvl);
                mapListModalidades(viaje);
            }
            actualizarLista();

        }

        private void ItemMenu_Click(object sender, RoutedEventArgs e)
        {
            MenuItem itemMenu = sender as MenuItem;
            DetalleLiquidacion_old det = itemMenu.Tag as DetalleLiquidacion_old;
            addViaticos add = new addViaticos(det.viatico);
            det.viatico = add.getViatico();
            actualizarLista();
        }

        private void actualizarLista()
        {

            lvlModalidades.Items.Clear();
            decimal sumaImporte = 0;
            foreach (var item in listaDetLiquidaciones)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;

                ContextMenu menu = new ContextMenu();
                MenuItem itemMenu = new MenuItem { Header = "Agregar/Quitar Viatico" };
                itemMenu.Tag = item;
                itemMenu.Click += ItemMenu_Click;
                menu.Items.Add(itemMenu);
                lvl.ContextMenu = menu;
                lvlModalidades.Items.Add(lvl);
                if (item.liquidar)
                {
                    sumaImporte += item.importe;
                }
                
            }
            txtSumaImportes.Tag = sumaImporte;
            txtSumaImportes.Text = sumaImporte.ToString("C2");

            decimal sueldoFijo = 0;

            if (txtSueldoFijo.Tag == null)
            {
                decimal v = 0;
                txtSegregacion.Tag = v;
                txtSegregacion.Text = v.ToString("C2");
            }
            else
            {
                sueldoFijo = (txtSueldoFijo.Tag as SueldoFijo).importe;
                if (sueldoFijo >= sumaImporte)
                {
                    txtSegregacion.Tag = sueldoFijo - sumaImporte;
                    txtSegregacion.Text = (sueldoFijo - sumaImporte).ToString("C2");
                }
                else
                {
                    decimal v = 0;
                    txtSegregacion.Tag = v;
                    txtSegregacion.Text = v.ToString("C2");
                }
            }

            decimal extras = getCantidadExtras();
            calcularTotalPagar(sumaImporte, sueldoFijo, extras);
        }

        private decimal getCantidadExtras()
        {
            decimal val = 0;
            val = txtViajes60.valor + txtRescates.valor;
            txtSumaExtras.Tag = val;
            txtSumaExtras.Text = val.ToString("C2");
            return val;
        }

        private void calcularTotalPagar(decimal sumaImporte, decimal sueldoFijo, decimal extras)
        {
            Decimal totalPagar = 0;

            if (sueldoFijo >= sumaImporte)
            {
                totalPagar = sueldoFijo;
            }
            else
            {
                totalPagar = sumaImporte;
            }

            txtTotalPagas.Tag = (totalPagar + extras);
            txtTotalPagas.Text = (totalPagar + extras).ToString("C");
        }

        List<DetalleLiquidacion_old> listaDetLiquidaciones = new List<DetalleLiquidacion_old>();
        private LiquidacionPegaso liquidacion;

        private void mapListModalidades(Viaje viaje)
        {
            //var resp = new ClasificacionRutasSvc().getClasificacionRutasByViaje(viaje.cliente.clave, viaje.listDetalles[0].zonaSelect.km);
            //if (resp.typeResult == ResultTypes.success)
            ////{
            //    ClasificacionRutas clasificacion = resp.result as ClasificacionRutas;
            //    var l = listaDetLiquidaciones.FindAll(s => s.pagoPorModalidad.clasificacion.idClasificacion == clasificacion.idClasificacion);
            //    var respPago = new PagoPorModalidadSvc().getPagoModalidadByNoViaje(clasificacion.idClasificacion, (l.Count + 1));
            //    if (respPago.typeResult == ResultTypes.success)
            //    {
            DetalleLiquidacion_old det = new DetalleLiquidacion_old
            {
                idDetalleLiquidacion = 0,
                viaje = viaje,
                //pagoPorModalidad = respPago.result as PagoPorModalidad,
                //tipoRuta = getTipoRuta(viaje),
                //doble = isDoble(viaje)
            };
            listaDetLiquidaciones.Add(det);
            //}
            //else
            //{
            //    ImprimirMensaje.imprimir(resp);
            //}
            //}
            //else
            //{
            //    ImprimirMensaje.imprimir(resp);
            //}
        }

        private bool isDoble(Viaje viaje)
        {
            bool doble = false;
            int dia = (int)viaje.FechaHoraViaje.DayOfWeek;
            if (dia == 6)
            {
                if (viaje.FechaHoraViaje.Hour >= 12)
                {
                    return true;
                }
                return false;
            }
            else if (dia == 7)
            {
                if (viaje.FechaHoraViaje.Hour < 22)
                {
                    return true;
                }
                return false;
            }
            return doble;
        }

        private eModalidadRutas getTipoRuta(Viaje viaje)
        {
            if (viaje.remolque2 != null)
            {
                return eModalidadRutas.FULL;
            }
            if (viaje.remolque.tipoUnidad.TonelajeMax <= 24)
            {
                return eModalidadRutas.SENCILLO_24_TON;
            }
            if (viaje.remolque.tipoUnidad.TonelajeMax <= 30)
            {
                return eModalidadRutas.SENCILLO_30_TON;
            }
            if (viaje.remolque.tipoUnidad.TonelajeMax <= 35)
            {
                return eModalidadRutas.SENCILLO_35_TON;
            }
            else
                return eModalidadRutas.NINGUNO;
        }

        public override OperationResult guardar()
        {
            try
            {
                LiquidacionPegaso liquidacion = mapForm();

                //liquidacion.idLiquidacion = 20;
                //result.valor = 0;
                //result.result = liquidacion;
                //mainWindow.DialogResult = true;
                //return result;

                var resp = new LiquidacionPegasoSvc().saveLiquidacionPegaso(ref liquidacion);
                if (resp.typeResult == ResultTypes.success)
                {
                    if (isVentana)
                    {
                        result = resp;
                        result.result = liquidacion;
                        mainWindow.DialogResult = true;
                    }
                    mapForm(liquidacion);
                }                
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        private void mapForm(LiquidacionPegaso liquidacion)
        {
            if (liquidacion != null)
            {
                txtclave.Text = liquidacion.idLiquidacion.ToString();
                txtclave.Tag = liquidacion;
                listaDetLiquidaciones = new List<DetalleLiquidacion_old>();
                buscar = false;
                ctrOperador.personal = liquidacion.operador;
                var sueldo = buscarSueldoFijo(liquidacion.operador);
                if (sueldo != null)
                {
                    sueldo.importe = liquidacion.sueldoFijo;
                }
                
                mapSueldoFijo(sueldo);
                if (liquidacion.listaExtras != null)
                {
                    foreach (var item in liquidacion.listaExtras)
                    {
                        if (item.motivo == MotivoExtra.RESCATE)
                        {
                            txtRescates.valor = item.importe;
                        }
                        if (item.motivo == MotivoExtra.VIAJES_AL_60)
                        {
                            txtViajes60.valor = item.importe;
                        }
                    }
                }                

                
                foreach (var item in liquidacion.listDetalles)
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = item.viaje;
                    lvlSalidas.Items.Add(lvl);
                }
                listaDetLiquidaciones = liquidacion.listDetalles;
                actualizarLista();
                ctrFechas.dtpFechaInicio.SelectedDate = liquidacion.fechaInicio;
                ctrFechas.dtpFechaFin.SelectedDate = liquidacion.fechaFin;
                gBoxBucador.IsEnabled = false;
            }
            else
            {
                txtclave.Clear();
                txtclave.Tag = null;
                ctrOperador.limpiar();
                mapLista(new List<Viaje>());
                listaDetLiquidaciones = new List<DetalleLiquidacion_old>();
                txtRescates.valor = 0;
                txtViajes60.valor = 0;
                actualizarLista();
                gBoxBucador.IsEnabled = true;
            }
        }

        private LiquidacionPegaso mapForm()
        {
            try
            {
                LiquidacionPegaso liquidacion = new LiquidacionPegaso
                {
                    idLiquidacion = txtclave.Tag == null ? 0 : (txtclave.Tag as LiquidacionPegaso).idLiquidacion,
                    fecha = txtclave.Tag == null ? DateTime.Now : (txtclave.Tag as LiquidacionPegaso).fecha,
                    fechaInicio = ctrFechas.fechaInicial,
                    fechaFin = ctrFechas.fechaFinal.AddDays(-1),
                    operador = ctrOperador.personal,
                    sumaModalidad = (decimal)txtSumaImportes.Tag,
                    extraSegregacion = (decimal)txtSegregacion.Tag,
                    sueldoFijo = txtSueldoFijo.Tag == null ? 0 : (txtSueldoFijo.Tag as SueldoFijo).importe,
                    importePagado = (decimal)txtTotalPagas.Tag,
                    sumaExtras = getCantidadExtras(),
                    realiza = mainWindow.inicio._usuario.nombreUsuario
                };

                liquidacion.listDetalles = new List<DetalleLiquidacion_old>();
                liquidacion.listDetalles = listaDetLiquidaciones;
                liquidacion.listaExtras = getListaExtras();

                return liquidacion;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private List<PagosExtraOperadores> getListaExtras()
        {
            List<PagosExtraOperadores> lista = new List<PagosExtraOperadores>();
            if (txtViajes60.valor > 0)
            {
                lista.Add(new PagosExtraOperadores
                {
                    motivo = MotivoExtra.VIAJES_AL_60,
                    importe = txtViajes60.valor
                });
            }
            if (txtRescates.valor > 0)
            {
                lista.Add(new PagosExtraOperadores
                {
                    motivo = MotivoExtra.RESCATE,
                    importe = txtRescates.valor
                });
            }
            return lista;
        }

        private void txtclave_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                int i = 0;
                if (int.TryParse(txtclave.Text, out i))
                {
                    buscarLiquidacion(i);
                }
            }
        }

        private void buscarLiquidacion(int i)
        {
            var resp = new LiquidacionPegasoSvc().getLiquidacionById(i);
            if (resp.typeResult == ResultTypes.success)
            {
                mapForm(resp.result as LiquidacionPegaso);
            }
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {           
            actualizarLista();
        }

        private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
        {            
            actualizarLista();
        }
    }
}
