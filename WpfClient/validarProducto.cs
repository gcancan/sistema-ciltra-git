﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using CoreFletera.Models;

namespace WpfClient
{
    public static class validarProducto
    {
        internal static bool validarExistenProductos(DetalleOrdenCompra dellate, string empresa, bool isLlanta = true)
        {
            if (isLlanta)
            {                
                OperationResult resp = new SDKconsultasSvc().existeProductoComercial("LL" + dellate.llantaProducto.idLLantasProducto.ToString(), empresa);
                if (resp.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(resp.mensaje + Environment.NewLine + "Producto Llanta: " + dellate.llantaProducto.medida);
                    return false;
                }
                if (resp.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(resp.mensaje + Environment.NewLine + "Producto Llanta: " + dellate.llantaProducto.medida);
                    return false;
                }
                if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    //MessageBox.Show(cartaPorte.destino.idDestino.ToString() + " - " + cartaPorte.destino.descripcion);
                    //return true;
                    //MessageBox.Show("AGREGAR LLANTA");
                    return !GuardarServicio.guardarSDKSinMensaje(dellate.llantaProducto, eAction.NUEVO);
                }
                if (resp.typeResult == ResultTypes.success)
                {
                    //MessageBox.Show("¿Existe?");
                    return true;
                }
                return true;
            }
            else
            {
                MessageBox.Show("IS Producto: " + empresa);
                OperationResult resp = new SDKconsultasSvc().existeProductoComercial(dellate.producto.codigo, empresa);
                if (resp.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(resp.mensaje + Environment.NewLine + "Producto/Insumo: " + dellate.llantaProducto.medida);
                    return false;
                }
                if (resp.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(resp.mensaje + Environment.NewLine + "Producto/Insumo: " + dellate.llantaProducto.medida);
                    return false;
                }
                if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    //MessageBox.Show(cartaPorte.destino.idDestino.ToString() + " - " + cartaPorte.destino.descripcion);
                    //return true;
                    MessageBox.Show("Intento alta " + dellate.producto.nombre);
                    return !GuardarServicio.guardarSDKSinMensaje(dellate.producto, eAction.NUEVO);

                }
                if (resp.typeResult == ResultTypes.success)
                {
                    return true;
                }
                return true;
            }
            
        }
    }
}
