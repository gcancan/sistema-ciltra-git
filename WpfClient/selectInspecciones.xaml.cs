﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for selectInspecciones.xaml
    /// </summary>
    public partial class selectInspecciones : Window
    {
        public selectInspecciones()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;

                OperationResult tiposUnidad = new TipoUnidadSvc().getGruposTiposUnidad();
                if (tiposUnidad.typeResult == ResultTypes.success)
                {
                    cbxTipoUnidad.ItemsSource = (List<GrupoTipoUnidades>)tiposUnidad.result;
                }
                else
                {
                    ImprimirMensaje.imprimir(tiposUnidad);
                }
                OperationResult tipoOrden = new OperacionSvc().getTiposOrdenServicio();
                if (tipoOrden.typeResult == ResultTypes.success)
                {
                    cbxTipoOrdenServicio.ItemsSource = (List<TiposDeOrdenServicio>)tipoOrden.result;
                }
                else
                {
                    ImprimirMensaje.imprimir(tipoOrden);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        public Inspecciones getInspeccion()
        {
            try
            {
                bool? resp = ShowDialog();
                if (resp.Value || lvlInspecciones.SelectedItem != null)
                {
                    return ((Inspecciones)((ListViewItem)lvlInspecciones.SelectedItem).Content);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                txtFind.Clear();
                Cursor = Cursors.Wait;
                var resp = new InspeccionesSvc().getInspeccionesByFiltros(((TiposDeOrdenServicio)cbxTipoOrdenServicio.SelectedItem).idTipOrdServ, (GrupoTipoUnidades)cbxTipoUnidad.SelectedItem);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapLista((List<Inspecciones>)resp.result);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapLista(List<Inspecciones> result)
        {
            lvlInspecciones.ItemsSource = null;
            lvlInspecciones.Items.Clear();
            List<ListViewItem> lvlList = new List<ListViewItem>();
            foreach (Inspecciones item in result)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick; ;
                lvlList.Add(lvl);
                //lvlUnidades.Items.Add(lvl);
            }
            lvlInspecciones.ItemsSource = lvlList;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlInspecciones.ItemsSource);
            view.Filter = UserFilter;
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((Inspecciones)(item as ListViewItem).Content).nombreInspeccion.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlInspecciones.ItemsSource).Refresh();
        }
    }
}
