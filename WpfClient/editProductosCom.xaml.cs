﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoreFletera.Models;
using Core.Models;
using CoreFletera.Interfaces;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for editProductosCom.xaml
    /// </summary>
    public partial class editProductosCom : ViewBase
    {
        public editProductosCom()
        {
            InitializeComponent();
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            nuevo();
        }
        public override void nuevo()
        {
            mapForm(null);
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.buscar | ToolbarCommands.cerrar);
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult val = validarForm();
                ProductosCOM producto = mapForm();
                OperationResult resp = new ProductosCOMSvc().saveProductoCOM(ref producto);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(producto);
                    base.guardar();
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private OperationResult validarForm()
        {
            var resp = new OperationResult { valor = 0, mensaje = "SE REQUIERE:" };

            if (string.IsNullOrEmpty(txtCodigo.Text.Trim()))
            {
                resp.valor = 2;
                resp.mensaje = "    *PROPORCIONAR EL CODIGO DEL PRODUCTO/INSUMO";
            }
            if (string.IsNullOrEmpty(txtNombre.Text.Trim()))
            {
                resp.valor = 2;
                resp.mensaje = "    *PROPORCIONAR EL NOMBRE DEL PRODUCTO/INSUMO";
            }
            bool CHECADO = false;
            if (chBoxLavadero.IsChecked.Value)
            {
                CHECADO = true;
            }
            if (chBoxLlantera.IsChecked.Value)
            {
                CHECADO = true;
            }
            if (chBoxTaller.IsChecked.Value)
            {
                CHECADO = true;
            }
            if (!CHECADO)
            {
                resp.valor = 2;
                resp.mensaje = "    *MARCAR AL MENOS UN AREA";
            }

            return resp;
        }

        private void mapForm(ProductosCOM producto)
        {
            if (producto != null)
            {
                txtCodigo.Tag = producto;
                txtCodigo.Text = producto.codigo;
                txtNombre.Text = producto.nombre;
                txtPrecio.valor = producto.precio;
                chBoxLavadero.IsChecked = producto.lavadero;
                chBoxLlantera.IsChecked = producto.llantera;
                chBoxTaller.IsChecked = producto.taller;
            }
            else
            {
                txtCodigo.Tag = null;
                txtCodigo.Clear();
                txtNombre.Clear();
                txtPrecio.valor = 0;
                chBoxLavadero.IsChecked = false;
                chBoxLlantera.IsChecked = false;
                chBoxTaller.IsChecked = false; ;
            }
        }

        private ProductosCOM mapForm()
        {
            try
            {
                return new ProductosCOM
                {
                    idProducto = txtCodigo.Tag == null ? 0 : ((ProductosCOM)txtCodigo.Tag).idProducto,
                    codigo = txtCodigo.Text,
                    nombre = txtNombre.Text,
                    precio = txtPrecio.valor,
                    taller = chBoxTaller.IsChecked.Value,
                    lavadero = chBoxLavadero.IsChecked.Value,
                    llantera = chBoxLlantera.IsChecked.Value
                };
            }
            catch (Exception)
            {
                return null;
            }
        }
        public override void buscar()
        {
            selectProductosCOM pro = new selectProductosCOM();
            var producto = pro.getProductos();
            if (producto != null)
            {
                mapForm(producto);
                base.buscar();
            }
            else
            {
                return;
            }
        }
        public override OperationResult eliminar()
        {
            try
            {
                Cursor = Cursors.Wait;
                ProductosCOM producto = mapForm();
                OperationResult resp = new ProductosCOMSvc().deleteProductoCOM(producto);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(producto);
                    nuevo();
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult() { valor = 1, mensaje = ex.Message };
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
