﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for checkViajes.xaml
    /// </summary>
    public partial class checkViajes : ViewBase
    {
        public checkViajes()
        {
            InitializeComponent();
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                dtpFechaInicio.SelectedDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                dtpFechaFin.SelectedDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());

                OperationResult respPrivilegio = new PrivilegioSvc().consultarPrivilegio("ISADMIN", mainWindow.inicio._usuario.idUsuario);
                bool enableCombo = false;
                if (respPrivilegio.typeResult == ResultTypes.success)
                {
                    enableCombo = true;
                }
                respPrivilegio = new PrivilegioSvc().consultarPrivilegio("ADMIN_COMBO_CLIENTES", mainWindow.inicio._usuario.idUsuario);
                bool enableComboClientes = false;
                if (respPrivilegio.typeResult == ResultTypes.success)
                {
                    enableComboClientes = true;
                }
                OperationResult resp;
                resp = new EmpresaSvc().getEmpresasALLorById();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxEmpresa.ItemsSource = (List<Empresa>)resp.result;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
                int i = 0;
                foreach (Empresa item in cbxEmpresa.Items)
                {
                    if (item.clave == mainWindow.empresa.clave)
                    {
                        cbxEmpresa.SelectedIndex = i;
                        break;
                    }
                    i++;
                }
                if (cbxCliente.Items.Count > 0)
                {
                    i = 0;
                    foreach (Cliente item in cbxCliente.Items)
                    {
                        if (item.clave == mainWindow.cliente.clave)
                        {
                            cbxCliente.SelectedIndex = i;
                            break;
                        }
                        i++;
                    }
                }
                cbxEmpresa.IsEnabled = enableCombo;
                cbxCliente.IsEnabled = enableComboClientes;

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                mainWindow.scrollContainer.IsEnabled = false;
            }
            finally
            { Cursor = Cursors.Arrow; }
        }
        void mostrarMensaje()
        {
            var resp = new CartaPorteSvc().ConsultarViajesFinalizados(((Cliente)cbxCliente.SelectedItem).clave);
            if (resp.typeResult == ResultTypes.success)
            {
                List<object> list = (List<object>)resp.result;
                lblMensaje.Visibility = Visibility.Visible;
                lblTxtMensaje.Text = string.Format("Existe(n) {0} viaje(s) Finalizados desde la fecha {1}.", list[1].ToString(), ((DateTime)list[0]).ToString("dd/MMMM/yyyy"));
            }
            else
            {
                lblMensaje.Visibility = Visibility.Collapsed;
            }
        }
        private void cbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (cbxEmpresa.SelectedItem != null)
                {
                    OperationResult resp = new ClienteSvc().getClientesALLorById(((Empresa)cbxEmpresa.SelectedItem).clave);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        cbxCliente.ItemsSource = (List<Cliente>)resp.result;
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
                else
                {
                    cbxCliente.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally { Cursor = Cursors.Arrow; }
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            buscarCartasPorte();
        }
        internal void buscarCartasPorte()
        {
            try
            {
                Cursor = Cursors.Wait;
                lvlSalidas.Items.Clear();
                if (cbxCliente.SelectedItem != null)
                {
                    List<Viaje> listViaje = new List<Viaje>();
                    OperationResult resp = new CartaPorteSvc().getCartaPortesFinalizados(dtpFechaInicio.SelectedDate.Value,
                        dtpFechaFin.SelectedDate.Value.AddDays(1),
                        ((Cliente)cbxCliente.SelectedItem).clave);
                    if (resp.typeResult == ResultTypes.error)
                    {
                        MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    else if (resp.typeResult == ResultTypes.warning)
                    {
                        MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else if (resp.typeResult == ResultTypes.recordNotFound)
                    {
                        //MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                    else if (resp.typeResult == ResultTypes.success)
                    {
                        foreach (var item in (List<Viaje>)resp.result)
                        {
                            listViaje.Add(item);
                        }
                        mapLista(listViaje);
                    }
                    lvlProductos.Items.Clear();
                    sumarVolumenCargas();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        internal void sumarVolumenCargas()
        {
            decimal suma = 0.00m;
            decimal sumaImportes = 0.00m;
            decimal sumaIva = 0.00m;
            decimal sumaRetencion = 0.00m;
            foreach (ListViewItem item in lvlProductos.Items)
            {
                CartaPorte det = (CartaPorte)item.Content;
                suma += det.volumenDescarga;
                sumaImportes += det.importeReal;
                sumaIva += det.ImporIVA;
                sumaRetencion = det.ImpRetencion;
            }

            //txtSumValProgramados.Text = suma.ToString("0.00");
            txtTotalImporte.Text = sumaImportes.ToString("0.00");
            txtTotalRetencion.Text = sumaIva.ToString("0.00");
            txtTotalFlete.Text = sumaRetencion.ToString("0.00");
            txtSumaTotal.Text = ((sumaIva + sumaImportes) - sumaRetencion).ToString("0.00");
        }
        private void mapLista(List<Viaje> result)
        {
            lvlSalidas.Items.Clear();
            lvlProductos.Items.Clear();
            foreach (Viaje item in result)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.FontWeight = FontWeights.Bold;
                lvl.Selected += Lvl_Selected;
                if (item.EstatusGuia.ToUpper() == "ACTIVO")
                {
                    lvl.Foreground = Brushes.SteelBlue;
                }
                if (item.EstatusGuia.ToUpper() == "CANCELADO")
                {
                    lvl.Foreground = Brushes.OrangeRed;
                }
                if (item.EstatusGuia.ToUpper() == "FINALIZADO")
                {
                    lvl.Foreground = Brushes.Blue;
                }
                lvlSalidas.Items.Add(lvl);
            }
        }

        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            lvlProductos.Items.Clear();
            Viaje select = (Viaje)(((ListViewItem)sender).Content);
            mapDetalles(select);
        }
        private void mapDetalles(Viaje cartaPorte)
        {
            foreach (var item in cartaPorte.listDetalles)
            {
                item.tipoPrecio = establecerPrecio(cartaPorte);
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.FontWeight = FontWeights.Bold;
                lvlProductos.Items.Add(lvl);
            }
            sumarVolumenCargas();
        }

        private TipoPrecio establecerPrecio(Viaje viaje)
        {
            if (viaje.dolly != null || viaje.remolque2 != null)
            {
                return TipoPrecio.FULL;
            }
            else if (viaje.remolque.tipoUnidad.descripcion == "TOLVA 35 TON")
            {
                return TipoPrecio.SENCILLO35;
            }
            else
            {
                return TipoPrecio.SENCILLO;
            }
        }

        private bool validarIsFull(Viaje cartaPorte)
        {
            if (cartaPorte.remolque.tipoUnidad.descripcion == "TOLVA 35 TON")
            {
                return true;
            }
            if (cartaPorte.dolly != null || cartaPorte.remolque2 != null)
            {
                return true;
            }
            return false;
        }

        private void cbxCliente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (cbxCliente.SelectedItem != null)
                {
                    mostrarMensaje();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally { Cursor = Cursors.Arrow; }
        }

        public override OperationResult guardar()
        {
            OperationResult resp = new OperationResult();
            List<Viaje> listaViajes = new List<Viaje>();
            try
            {
                //ListViewItem lvl = (ListViewItem)lvlSalidas.SelectedItem;
                var list = lvlSalidas.SelectedItems;
                foreach (ListViewItem item in list)
                {
                    Viaje cartaporte = (Viaje)item.Content;
                    if (cartaporte.EstatusGuia.ToUpper() != "FINALIZADO")
                    {
                        continue;
                    }
                    Viaje viaje = (Viaje)item.Content;
                    resp = new CartaPorteSvc().checarCartaPorte(ref viaje, mainWindow.inicio._usuario.idUsuario);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listaViajes.Add(viaje);
                    }
                    else
                    {
                        break;
                    }
                }

                foreach (var item in listaViajes)
                {
                    cambiarLista(item, TipoAccion.CHECAR);
                }
                return resp;
            }
            catch (Exception e)
            {
                //Declaraciones.fCierraEmpresa();
                //Declaraciones.fTerminaSDK();
                return new OperationResult { valor = 1, mensaje = e.Message, result = null };
            }
            finally
            {
                mainWindow.scrollContainer.IsEnabled = true;
                mostrarMensaje();
                //Declaraciones.fCierraEmpresa();
                //Declaraciones.fTerminaSDK();
            }
        }
        void cambiarLista(Viaje viaje, TipoAccion accion)
        {
            var i = 0;
            ListViewItem newItem = new ListViewItem();
            ListViewItem oldItem = new ListViewItem();
            foreach (ListViewItem item in lvlSalidas.Items)
            {
                var lvlViaje = (Viaje)item.Content;
                if (lvlViaje.NumGuiaId == viaje.NumGuiaId)
                {
                    switch (accion)
                    {
                        case TipoAccion.CANCELAR:
                            newItem.Foreground = Brushes.OrangeRed;
                            break;
                        case TipoAccion.FINALIZAR:
                            newItem.Foreground = Brushes.Blue;
                            break;
                        case TipoAccion.CHECAR:
                            newItem.Foreground = Brushes.SteelBlue;
                            break;
                        default:
                            break;
                    }
                    oldItem = item;
                    item.Content = viaje;
                    newItem.Content = viaje;
                    break;
                }
                i++;
            }

            lvlSalidas.Items.Remove(oldItem);
            lvlSalidas.Items.Insert(i, newItem);
        }

        private void btnChecar_Click(object sender, RoutedEventArgs e)
        {
            if (lvlSalidas.SelectedItem != null)
            {
                guardar();
            }

        }
    }
}
