﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para controlExistenciaPegasoGlobal.xaml
    /// </summary>
    public partial class controlExistenciaPegasoGlobal : UserControl
    {
        List<ExistenciaDiario> listaExixtencias = new List<ExistenciaDiario>();
        public controlExistenciaPegasoGlobal(List<ExistenciaDiario> listaExixtencias)
        {
            InitializeComponent();
            this.listaExixtencias = listaExixtencias;
        }
        public controlExistenciaPegasoGlobal()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            llenarDiasExistencias();
        }
        private void llenarDiasExistencias()
        {
            List<GrupoDiasExistenciaDiario> listaGrupoDias = (from v in listaExixtencias
                                                              group v by v.fecha into grupo
                                                              select new GrupoDiasExistenciaDiario
                                                              {
                                                                  fecha = grupo.Select(s => s.fecha).First(),
                                                                  listaExistencias = grupo.ToList()
                                                              }).ToList();
            listaGrupoDias = listaGrupoDias.OrderBy(s => s.fecha).ToList();
            foreach (var lista in listaGrupoDias)
            {
                StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal };
                List<ExistenciaDiario> listaExis = lista.listaExistencias;
                Label lblFinal = new Label
                {
                    Content = listaExis.Sum(s => s.final).ToString("N2"),
                    Width = 110,
                    BorderThickness = new Thickness(1),
                    BorderBrush = Brushes.Black,
                    HorizontalContentAlignment = HorizontalAlignment.Right,
                    FontSize = 10,
                   
                };
                stpContent.Children.Add(lblFinal);

                stpExistencias.Children.Add(stpContent);
            }
        }
    }
}
