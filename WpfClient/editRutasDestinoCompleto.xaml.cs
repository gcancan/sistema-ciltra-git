﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Interfaces;
using Microsoft.Win32;
using Core.Utils;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for editRutasDestino.xaml
    /// </summary>
    public partial class editRutasDestinoCompleto : ViewBase
    {
        public editRutasDestinoCompleto()
        {
            InitializeComponent();
        }
        eAction action = eAction.NUEVO;
        

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            OperationResult resp = new EmpresaSvc().getEmpresasALLorById();
            if (resp.typeResult == ResultTypes.error)
            {
                MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                controles.IsEnabled = false;
            }
            if (resp.typeResult == ResultTypes.warning)
            {
                MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                controles.IsEnabled = false;
            }
            if (resp.typeResult == ResultTypes.recordNotFound)
            {
                MessageBox.Show(resp.mensaje, "No se encontraron registros", MessageBoxButton.OK, MessageBoxImage.Warning);
                controles.IsEnabled = false;
            }
            if (resp.typeResult == ResultTypes.success)
            {
                List<Empresa> listEmpresa = (List<Empresa>)resp.result;
                mapComboEmpresa(listEmpresa);
            }

            resp = new ClienteSvc().getClientesALLorById(mainWindow.inicio._usuario.personal.empresa.clave);
            if (resp.typeResult == ResultTypes.error)
            {
                MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                controles.IsEnabled = false;
            }
            if (resp.typeResult == ResultTypes.warning)
            {
                MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                controles.IsEnabled = false;
            }
            if (resp.typeResult == ResultTypes.recordNotFound)
            {
                MessageBox.Show(resp.mensaje, "No se encontraron registros", MessageBoxButton.OK, MessageBoxImage.Warning);
                controles.IsEnabled = false;
            }
            if (resp.typeResult == ResultTypes.success)
            {
                List<Cliente> listCliente = (List<Cliente>)resp.result;
                mapComboClientes(listCliente);
            }
            nuevo();
            //iniciaSDK();
        }

        private void mapComboClientes(List<Cliente> listCliente)
        {
            foreach (Cliente item in listCliente)
            {
                cbxCliente.Items.Add(item);
            }
        }

        private void mapComboEmpresa(List<Empresa> listEmpresa)
        {
            foreach (var item in listEmpresa)
            {
                cbxEmpresa.Items.Add(item);
            }
        }

        public override void nuevo()
        {
            mapForm(null);
            action = eAction.NUEVO;
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
        }

         public override void buscar()
        {
            if (cbxCliente.SelectedItem == null || cbxEmpresa.SelectedItem==null)
            {
                MessageBox.Show("Se necesita elegir una empresa y un cliente");
                return;
            }
            selectZonas findZonas = new selectZonas(txtClave.Text.Trim());
            Zona resultado = findZonas.findZonasIncompletas(((Cliente)cbxCliente.SelectedItem).clave);
            if (resultado != null)
            {
                mapForm(resultado);
                mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar | ToolbarCommands.guardar);
            }
        }        

        private void mapForm(Zona resultado)
        {
            if (resultado != null)
            {
                int i = 0;
                foreach (Empresa item in cbxEmpresa.Items)
                {
                    if (item.clave == resultado.clave_empresa)
                    {
                        cbxEmpresa.SelectedIndex = i;
                        cbxEmpresa.IsEnabled = false;
                        break;
                    }
                    else
                    {
                        i++;
                    }
                }
                i = 0;
                foreach (Cliente item in cbxCliente.Items)
                {
                    if (item.clave == resultado.claveCliente)
                    {
                        cbxCliente.SelectedIndex = i;
                        cbxCliente.IsEnabled = false;
                        break;
                    }
                    else
                    {
                        i++;
                    }
                }

                txtClave.Text = resultado.clave.ToString();
                txtClave.Tag = resultado;
                txtDescripcion.Text = resultado.descripcion;
                txtDescripcion.IsEnabled = false;
                txtPrecioFull.Text = resultado.costoFull.ToString("#.##");
                txtPrecioSencillo.Text = resultado.costoSencillo.ToString("#.##");                
                txtKM.Text = resultado.km.ToString("#.##");
                txtPrecioSencillo35.Text = resultado.costoSencillo35.ToString("#.##");
                txtZona.Text = resultado.claveZona;
            }
            else
            {
                cbxEmpresa.SelectedItem = null;
                cbxCliente.SelectedItem = null;
                txtClave.Clear();
                txtClave.Tag = null;
                txtDescripcion.Clear();
                txtPrecioFull.Clear();
                txtPrecioSencillo.Clear();
                txtKM.Clear();
                txtPrecioSencillo35.Clear();
                txtZona.Clear();
                cbxEmpresa.IsEnabled = true;
                cbxCliente.IsEnabled = true;
                txtDescripcion.IsEnabled = true;
            }
        }

        public override void cerrar()
        {
            try
            {
                Declaraciones.fCierraEmpresa();                
            }
            catch (Exception)
            {
            }
            try
            {
                Declaraciones.fTerminaSDK();
            }
            catch (Exception)
            {
            }

            //KeySistema.Dispose();
            base.cerrar();

        }

        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult validar = validarForm();

                if (validar.typeResult == ResultTypes.success)
                {
                    Zona zona = mapForm();
                    if (zona != null)
                    {
                        OperationResult resp = new ZonasSvc().saveRutasDestinoIncompletos(zona);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            mapForm((Zona)resp.result);
                            base.guardar();
                            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);                                               
                        }
                        return resp;
                    }
                    else
                    {
                        return new OperationResult { mensaje = "Ocurrio un error al querer leer la información del formulario", valor = 1, result = null };
                    }
                }
                else
                {
                    return validar;
                }    
            }
            catch (Exception e)
            {
                return new OperationResult { mensaje = e.Message, valor = 1, result = null };
            }
            finally
            {
                Cursor = Cursors.Arrow;
                //Declaraciones.fCierraEmpresa();
            }
            
            
        }

        public override void editar()
        {
            action = eAction.EDITAR;
            base.editar();
        }

        private Zona mapForm()
        {
            try
            {
                return new Zona
                {
                    clave = txtClave.Tag == null ? 0 : ((Zona)txtClave.Tag).clave,
                    descripcion = txtDescripcion.Text,
                    clave_empresa = ((Empresa)cbxEmpresa.SelectedItem).clave,
                    costoSencillo = Convert.ToDecimal(txtPrecioSencillo.Text.Trim()),
                    costoSencillo35 = Convert.ToDecimal(txtPrecioSencillo35.Text.Trim()),
                    costoFull = Convert.ToDecimal(txtPrecioFull.Text.Trim()),
                    km = Convert.ToDecimal(txtKM.Text.Trim()),
                    claveCliente = ((Cliente)cbxCliente.SelectedItem).clave
                };
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// Funcion para validar formulario 
        /// Si no hay error devuelve true en caso contrario devuelve false
        /// </summary>
        /// <returns>True o false</returns>
        private OperationResult validarForm()
        {
            bool correcto = true;
            string cadenaError = "Para proceder se requiere corregir lo siguiente:" + Environment.NewLine;
            if (cbxEmpresa.SelectedItem == null)
            {
                cadenaError += "* Seleccionar una empresa." + Environment.NewLine;
                correcto = false;
            }
            //if (string.IsNullOrEmpty(txtClave.Text.Trim()))
            //{
            //    cadenaError += "* Escribir una clave." + Environment.NewLine;
            //    correcto = false;
            //}
            //else
            //{
            //    int val;
            //    if (!int.TryParse(txtClave.Text.Trim(), out val))
            //    {
            //        cadenaError += "* Escribir una clave Valida." + Environment.NewLine;
            //    }
            //}
            if (string.IsNullOrEmpty(txtDescripcion.Text.Trim()))
            {
                cadenaError += "* Escribir una descripción." + Environment.NewLine;
                correcto = false;
            }

            if (string.IsNullOrEmpty(txtKM.Text.Trim()))
            {
                cadenaError += "* Proporcionar un kilometraje." + Environment.NewLine;
                correcto = false;
            }
            else
            {
                decimal val;
                if (!decimal.TryParse(txtKM.Text.Trim(), out val))
                {
                    cadenaError += "* Escribir un kilometraje valido." + Environment.NewLine;
                }
            }

            if (string.IsNullOrEmpty(txtPrecioSencillo35.Text.Trim()))
            {
                cadenaError += "* Proporcionar el precio del flete sencillo de 35 Toneladas." + Environment.NewLine;
                correcto = false;
            }
            else
            {
                decimal val;
                if (!decimal.TryParse(txtPrecioSencillo35.Text.Trim(), out val))
                {
                    cadenaError += "* Escribir un precio de flete Sencillo de 35 toneladas valido." + Environment.NewLine;
                }
            }

            if (string.IsNullOrEmpty(txtPrecioSencillo.Text.Trim()))
            {
                cadenaError += "* Proporcionar el precio del flete sencillo." + Environment.NewLine;
                correcto = false;
            }
            else
            {
                decimal val;
                if (!decimal.TryParse(txtPrecioSencillo.Text.Trim(), out val))
                {
                    cadenaError += "* Escribir un precio de flete Sencillo valido." + Environment.NewLine;
                }
            }

            if (string.IsNullOrEmpty(txtPrecioFull.Text.Trim()))
            {
                cadenaError += "* Proporcionar el precio del flete Full." + Environment.NewLine;
                correcto = false;
            }
            else
            {
                decimal val;
                if (!decimal.TryParse(txtPrecioFull.Text.Trim(), out val))
                {
                    cadenaError += "* Escribir un precio de flete Full valido." + Environment.NewLine;
                }
            }


            if (correcto)
            {
                return new OperationResult { mensaje = "Correcto", valor = 0 };
            }
            else
            {
                return new OperationResult { mensaje = cadenaError, valor = 2 };
            }
        }
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            buscar();
        }

        private void cbxCliente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            mapForm(null);
        }
        
    }
}
