﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CoreFletera.Interfaces;
using CoreFletera.Models;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para ReporteMantenimiento.xaml
    /// </summary>
    public partial class ReporteMantenimiento : ViewBase
    {
        public ReporteMantenimiento()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresa.loaded(mainWindow.empresa, true);
            nuevo();
        }
        public override void nuevo()
        {
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
            if (mainWindow != null)
            {
                cbxListaUnidades.SelectedItems.Clear();
                //lvlDetalles.ItemsSource = null;
                chBoxTodos.IsChecked = false;
            }
            //          
        }
        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ctrEmpresa.empresaSelected != null)
            {
                OperationResult resp = new UnidadTransporteSvc().getUnidadesALLorById(ctrEmpresa.empresaSelected.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxListaUnidades.ItemsSource = (List<UnidadTransporte>)resp.result;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
        }

        private void chBoxTodos_Checked(object sender, RoutedEventArgs e)
        {
            foreach (UnidadTransporte item in cbxListaUnidades.Items)
            {
                cbxListaUnidades.SelectedItems.Add(item);

            }
            cbxListaUnidades.IsEnabled = false;
        }

        private void chBoxTodos_Unchecked(object sender, RoutedEventArgs e)
        {
            cbxListaUnidades.SelectedItems.Clear();
            cbxListaUnidades.IsEnabled = true;
        }
        private List<string> getLista()
        {
            List<string> lista = new List<string>();
            foreach (UnidadTransporte item in cbxListaUnidades.SelectedItems)
            {
                lista.Add(item.clave);
            }
            return lista;
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new MantenimientoSvc().getMantenimientoByFiltros(ctrFechas.fechaInicial, ctrFechas.fechaFinal, getLista());
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Mantenimiento> listMatto = resp.result as List<Mantenimiento>;
                    lvlOS.ItemsSource = listMatto;

                    CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlOS.ItemsSource);
                    PropertyGroupDescription groupDescription = new PropertyGroupDescription("tipoOrdenServ.nombreTioServ");
                    view.GroupDescriptions.Add(groupDescription);

                    List<DetalleMantenimiento> listDetMatto = new List<DetalleMantenimiento>();
                    foreach (var mtto in listMatto)
                    {
                        foreach (var det in mtto.listaActividades)
                        {
                            listDetMatto.Add(det);
                        }
                    }

                    lvDetlOS.ItemsSource = listDetMatto;
                    CollectionView viewDet = (CollectionView)CollectionViewSource.GetDefaultView(lvDetlOS.ItemsSource);
                    PropertyGroupDescription groupDescriptionDet = new PropertyGroupDescription("actividad");
                    viewDet.GroupDescriptions.Add(groupDescriptionDet);

                    List<InsumosOrdenTrabajo> listaInsumo = new List<InsumosOrdenTrabajo>();
                    foreach (var detMtto in listDetMatto)
                    {
                        foreach (var insumo in detMtto.listaInsumos)
                        {
                            listaInsumo.Add(insumo);
                        }
                    }
                    lvInsumos.ItemsSource = listaInsumo;
                    CollectionView viewIns = (CollectionView)CollectionViewSource.GetDefaultView(lvInsumos.ItemsSource);
                    PropertyGroupDescription groupDescriptionIns = new PropertyGroupDescription("encabezado");
                    viewIns.GroupDescriptions.Add(groupDescriptionIns);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void btnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                var listMtto = lvlOS.ItemsSource as List<Mantenimiento>;
                if (listMtto != null)
                {
                    if (new ReportView().generarReporteMatto(listMtto, ctrEmpresa.empresaSelected, ctrFechas.isMes, ctrFechas.fechaInicial, ctrFechas.fechaFinal))
                    {

                    }
                }                
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
            
        }
    }
}
