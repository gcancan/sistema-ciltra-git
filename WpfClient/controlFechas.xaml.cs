﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for controlFechas.xaml
    /// </summary>
    public partial class controlFechas : UserControl
    {
        public controlFechas()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            dtpFechaInicio.SelectedDate = _fechaInicial != null ? _fechaInicial : Convert.ToDateTime(DateTime.Now.ToShortDateString());
            dtpFechaFin.SelectedDate = _fechaFinal != null ? _fechaFinal : Convert.ToDateTime(DateTime.Now.ToShortDateString());

            cbxMes.ItemsSource = Enum.GetValues(typeof(Mes));
            cbxMes.SelectedIndex = DateTime.Now.Month - 1;
            int anio = DateTime.Now.Year;
            for (int x = anio; x > anio - 10; x--)
            {
                cbxAnio.Items.Add(x);
            }
            cbxAnio.SelectedIndex = 0;
        }
        public bool isMes { get; set; }
        private DateTime? _fechaInicial { get; set; }
        public DateTime fechaInicial
        {
            get
            {
                if (tapMes.IsSelected)
                {
                    var dia = DateTime.DaysInMonth((int)(cbxAnio.SelectedItem), (int)((Mes)cbxMes.SelectedItem));
                    isMes = true;
                    return Convert.ToDateTime(string.Format("01/{0}/{1} 00:00:00", ((int)((Mes)cbxMes.SelectedItem)).ToString(), (cbxAnio.SelectedItem)));
                    
                }
                else
                {
                    if (dtpFechaInicio.SelectedDate == null)
                    {
                        return Convert.ToDateTime(DateTime.Now.ToShortDateString());
                    }         
                    return dtpFechaInicio.SelectedDate.Value;
                }
            }
            set
            {
                dtpFechaInicio.SelectedDate = value;
            }
        }
        internal void mostrarFechas(DateTime fechaInicial, DateTime fechaFinal)
        {
            this._fechaInicial = fechaInicial;
            this._fechaFinal = fechaFinal;
            this.fechaInicial = fechaInicial;
            this.fechaFinal = fechaFinal;
        }
        private DateTime? _fechaFinal { get; set; }
        public DateTime fechaFinal
        {
            get
            {
                if (tapMes.IsSelected)
                {
                    var dia = DateTime.DaysInMonth((int)(cbxAnio.SelectedItem), (int)((Mes)cbxMes.SelectedItem));
                    isMes = true;
                    return Convert.ToDateTime(string.Format("{0}/{1}/{2} 23:59:59", dia.ToString(), ((int)((Mes)cbxMes.SelectedItem)).ToString(), (cbxAnio.SelectedItem)));

                }
                else
                {
                    if (dtpFechaFin.SelectedDate == null)
                    {
                       return Convert.ToDateTime(DateTime.Now.AddDays(1).ToShortDateString());
                    }
                    DateTime fecha = dtpFechaFin.SelectedDate.Value;

                    return Convert.ToDateTime(string.Format("{0}/{1}/{2} 23:59:59", fecha.Day.ToString(), fecha.Month.ToString(), fecha.Year.ToString()));

                    return dtpFechaFin.SelectedDate.Value.AddDays(1);
                }
            }
            set
            {
                dtpFechaFin.SelectedDate = value;
            }
        }
    }
}
