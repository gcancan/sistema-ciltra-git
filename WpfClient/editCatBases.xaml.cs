﻿namespace WpfClient
{
    using Core.Models;
    using CoreFletera.Interfaces;
    using CoreFletera.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Markup;

    public partial class editCatBases : WpfClient.ViewBase
    {
        public editCatBases()
        {
            this.InitializeComponent();
        }
        private bool cargarBases()
        {
            bool flag2;
            try
            {
                this.lvlBases.Items.Clear();
                base.Cursor = Cursors.Arrow;
                OperationResult resp = new BaseSvc().getAllBase();
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Base> list = resp.result as List<Base>;
                    foreach (Base base2 in list)
                    {
                        ListViewItem newItem = new ListViewItem
                        {
                            Content = base2
                        };
                        newItem.Selected += new RoutedEventHandler(this.Lvl_Selected);
                        newItem.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                        this.lvlBases.Items.Add(newItem);
                    }
                    return true;
                }
                if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                    return false;
                }
                flag2 = false;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                flag2 = false;
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return flag2;
        }

        private Base getBase(object sender)
        {
            try
            {
                return ((sender as ListViewItem).Content as Base);
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return null;
            }
        }

        public override OperationResult guardar()
        {
            OperationResult result3;
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult result = this.validar();
                if (result.typeResult > ResultTypes.success)
                {
                    return result;
                }
                Base bse = this.mapForm();
                OperationResult result2 = new BaseSvc().saveBase(ref bse);
                if (result2.typeResult == ResultTypes.success)
                {
                    if (bse.activo)
                    {
                        this.mapForm(bse);
                    }
                    else
                    {
                        this.mapForm(null);
                    }
                    base.mainWindow.habilitar = true;
                    this.cargarBases();
                }
                result3 = result2;
            }
            catch (Exception exception)
            {
                result3 = new OperationResult(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return result3;
        }
        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Base bse = this.getBase(sender);
            if (bse != null)
            {
                this.mapForm(bse);
            }
        }
        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            Base bse = this.getBase(sender);
            if (bse != null)
            {
                this.mapForm(bse);
            }
        }
        private Base mapForm()
        {
            try
            {
                return new Base
                {
                    idBase = (this.ctrClave.Tag == null) ? 0 : (this.ctrClave.Tag as Base).idBase,
                    estado = this.cbxEstado.estado,
                    nombreBase = this.txtNombre.Text,
                    activo = this.chcActivo.IsChecked.Value
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void mapForm(Base bse)
        {
            try
            {
                if (bse != null)
                {
                    this.ctrClave.Tag = bse;
                    this.ctrClave.valor = bse.idBase;
                    this.cbxEstado.estado = bse.estado;
                    this.txtNombre.Text = bse.nombreBase;
                    this.chcActivo.IsChecked = new bool?(bse.activo);
                }
                else
                {
                    this.ctrClave.Tag = null;
                    this.ctrClave.limpiar();
                    this.cbxEstado.estado = null;
                    this.txtNombre.Clear();
                    this.chcActivo.IsChecked = true;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        public override void nuevo()
        {
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
            this.mapForm(null);
        }
        public OperationResult validar()
        {
            try
            {
                OperationResult result = new OperationResult(ResultTypes.success, "PARA CONTINUAR SE REQUIERE:" + Environment.NewLine, null);
                if (this.cbxEstado.estado == null)
                {
                    result.valor = 2;
                    result.mensaje = result.mensaje + "  * Elegir El Estado" + Environment.NewLine;
                }
                if (string.IsNullOrEmpty(this.txtNombre.Text.Trim()))
                {
                    result.valor = 2;
                    result.mensaje = result.mensaje + "  * Proporcionar El Nombre De La Base" + Environment.NewLine;
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            if (!this.cargarBases())
            {
                base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                base.IsEnabled = false;
            }
            else
            {
                this.nuevo();
            }
        }
    }
}
