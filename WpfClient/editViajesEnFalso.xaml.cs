﻿using Core.Interfaces;
using Core.Models;
using Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for editViajesEnFalso.xaml
    /// </summary>
    public partial class editViajesEnFalso : ViewBase
    {
        List<Zona> _listZonas = new List<Zona>();
        List<Producto> _listProducto = new List<Producto>();
        public editViajesEnFalso()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            nuevo();
            mapComboMotivos();
        }

        private void mapComboMotivos()
        {
            var resp = new MotivosViajeFalsoSvc().getMotivosAllOrById();
            if (resp.typeResult == ResultTypes.success)
            {
                foreach (MotivoViajeFalso item in (List<MotivoViajeFalso>)resp.result)
                {
                    ComboBoxItem cbx = new ComboBoxItem();
                    switch (item.imputable)
                    {
                        case eImputable.CLIENTE:
                            cbx.Foreground = Brushes.SteelBlue;
                            break;
                        case eImputable.PEGASO:
                            cbx.Foreground = Brushes.DarkGreen;
                            break;
                        case eImputable.CASO_ESPECIAL:
                            cbx.Foreground = Brushes.DarkRed;
                            break;
                    }
                    //cbx.d
                    cbx.Content = item.motivo;
                    cbx.Tag = item;
                    cbxMotivos.Items.Add(cbx);
                }
                //cbxMotivos.ItemsSource = (List<MotivoViajeFalso>)resp.result;
            }
        }

        public override void nuevo()
        {
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar | ToolbarCommands.guardar);
            cabecera.nuevo();
            lvlCartaPortes.Items.Clear();
            _viaje = null;
            cbxMotivos.SelectedItem = null;
        }
        Viaje _viaje = new Viaje();
        private void cabecera_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var dataContext = ((cabeceraViajes)sender).DataContext;
            if (dataContext.ToString() != "System.Object")
            {
                _viaje = (Viaje)dataContext;
                mapDetalles(_viaje.listDetalles);

                var respZonas = new ZonasSvc().getZonasALLorById(0, _viaje.cliente.clave.ToString());
                //_listZonas = (List<Zona>)respZonas.result;
                //_listProducto = ((Cliente)cabecera.cbxClientes.Tag).fraccion.listProductos;
                if (_viaje.EstatusGuia.ToUpper() == "CANCELADO")
                {
                    mainWindow.scrollContainer.IsEnabled = false;
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo);
                }

            }
            else
            {
                _viaje = null;
            }
        }

        private void mapDetalles(List<CartaPorte> listDetalles)
        {

            lvlCartaPortes.Items.Clear();
            foreach (CartaPorte cp in listDetalles)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = cp;
                lvl.Selected += Lvl_Selected;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                lvlCartaPortes.Items.Add(lvl);
            }
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
            {
                ListViewItem lvl = (ListViewItem)sender;
                ListViewItem lvl2 = new ListViewItem() { Content = lvl.Content };
                lvl2.MouseDoubleClick += Lvl2_MouseDoubleClick;
                lvlRemisiones.Items.Add(lvl2);
                lvlCartaPortes.Items.Remove(lvl);
            }
        }

        private void Lvl2_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
            {
                ListViewItem lvl = (ListViewItem)sender;
                ListViewItem lvl2 = new ListViewItem() { Content = lvl.Content };
                lvl2.MouseDoubleClick += Lvl_MouseDoubleClick;
                lvlCartaPortes.Items.Add(lvl2);
                lvlRemisiones.Items.Remove(lvl);
            }
        }

        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                ListViewItem lvl = (ListViewItem)sender;
                if (lvl != null)
                {
                    mapCampos((CartaPorte)lvl.Content);
                }
            }

        }

        private void mapCampos(CartaPorte cp = null)
        {
            if (cp != null)
            {
                txtRemision.Tag = cp;
                txtRemision.Text = cp.remision.ToString();
                mapProductos(cp.producto);
                mapDestinos(cp.zonaSelect);
                txtVolProgramado.Text = cp.volumenCarga.ToString();
                txtVolDescarga.Text = (cp.volumenDescarga * 1000).ToString();
            }
            else
            {
                txtRemision.Tag = null;
                txtRemision.Clear();
                mapProductos();
                mapDestinos();
                txtVolProgramado.Clear();
                txtVolDescarga.Clear();
            }
        }
        private void mapProductos(Producto producto = null)
        {
            if (producto != null)
            {
                txtProducto.Tag = producto;
                txtProducto.Text = producto.descripcion;
                //txtProducto.IsEnabled = false;
                txtDestino.Focus();
            }
            else
            {
                txtProducto.Tag = null;
                txtProducto.Clear();
                //txtProducto.IsEnabled = true;
            }
        }
        private void mapDestinos(Zona zona = null)
        {
            if (zona != null)
            {
                txtDestino.Tag = zona;
                txtDestino.Text = zona.descripcion;
                //txtDestino.IsEnabled = false;
                txtVolProgramado.Focus();
            }
            else
            {
                txtDestino.Tag = null;
                txtDestino.Clear();
                //txtDestino.IsEnabled = true;
            }
        }

        private void btnNewDetalle_Click(object sender, RoutedEventArgs e)
        {
            if (txtRemision.Tag != null)
            {
                CartaPorte cp = (CartaPorte)txtRemision.Tag;
                cp.remision = txtRemision.Text.Trim();
                cp.producto = (Producto)txtProducto.Tag;
                cp.zonaSelect = (Zona)txtDestino.Tag;
                cp.volumenDescarga = Convert.ToDecimal(txtVolDescarga.Text.Trim()) / 1000;
                ListViewItem lvl = new ListViewItem();
                lvl.Content = cp;
                lvlCartaPortes.Items.Insert(lvlCartaPortes.SelectedIndex, lvl);
                lvlCartaPortes.Items.Remove(lvlCartaPortes.SelectedItem);
                lvlCartaPortes.Items.Refresh();
                mapCampos();
            }

        }

        private void txtProducto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                List<Producto> listProducto = new List<Producto>();
                var mySearch = _listProducto.FindAll(S => S.descripcion.IndexOf(txtProducto.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (var item in mySearch)
                {
                    listProducto.Add(item);
                }
                selectProducto buscador = new selectProducto(txtProducto.Text);
                switch (listProducto.Count)
                {
                    case 1:
                        mapProductos(listProducto[0]);
                        break;
                    default:
                        var resp = buscador.buscarProductos(_listProducto);
                        mapProductos(resp);
                        break;
                }
            }
        }

        private void txtDestino_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                List<Zona> listZonas = new List<Zona>();
                var mySearch = _listZonas.FindAll(S => S.descripcion.IndexOf(txtDestino.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (var item in mySearch)
                {
                    listZonas.Add(item);
                }

                selectDestinos buscador = new selectDestinos(txtDestino.Text);
                switch (listZonas.Count)
                {
                    case 1:
                        mapDestinos(listZonas[0]);
                        break;
                    default:
                        var resp = buscador.buscarZona(_listZonas);
                        mapDestinos(resp);
                        break;
                }
            }
        }

        //private void cbxMotivos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    if (sender == null)
        //    {
        //        return;
        //    }
        //    MotivoViajeFalso motivo = (MotivoViajeFalso)(((ComboBoxItem)((ComboBox)sender).SelectedItem).Tag);
        //    if (motivo == null)
        //    {
        //        lvlCartaPortes.IsEnabled = false;
        //        stpControles.IsEnabled = false;
        //        txtVolDescarga.IsReadOnly = true;
        //        txtDestino.IsReadOnly = true;
        //        chbReenvio.IsEnabled = false;
        //        chbReenvio.IsChecked = false;
        //        return;
        //    }
        //    if (motivo.imputable == eImputable.PEGASO)
        //    {
        //        lvlCartaPortes.IsEnabled = true;
        //        stpControles.IsEnabled = true;
        //        txtVolDescarga.IsReadOnly = false;
        //        txtDestino.IsReadOnly = false;
        //        chbReenvio.IsEnabled = true;
        //    }
        //    else if (motivo.imputable == eImputable.CLIENTE)
        //    {
        //        lvlCartaPortes.IsEnabled = true;
        //        stpControles.IsEnabled = true;
        //        txtVolDescarga.IsReadOnly = true;
        //        txtDestino.IsReadOnly = false;
        //        chbReenvio.IsEnabled = true;
        //    }
        //    else
        //    {
        //        lvlCartaPortes.IsEnabled = false;
        //        stpControles.IsEnabled = false;
        //        txtVolDescarga.IsReadOnly = true;
        //        txtDestino.IsReadOnly = true;
        //        chbReenvio.IsEnabled = false;
        //        chbReenvio.IsChecked = false;
        //    }
        //    mapCampos(null);
        //    if (_viaje != null)
        //    {
        //        mapDetalles(_viaje.listDetalles);
        //    }

        //}

        public override OperationResult guardar()
        {
            try
            {
                if (_viaje == null)
                {
                    return new OperationResult { valor = 2, mensaje = "No hay un viaje seleccionado" };
                }
                if (cbxMotivos.SelectedItem == null)
                {
                    return new OperationResult { valor = 2, mensaje = "Es necesario elegir un motivo" };
                }
                Cursor = Cursors.Wait;
                MotivoViajeFalso motivo = (MotivoViajeFalso)(((ComboBoxItem)cbxMotivos.SelectedItem).Tag);
                MessageBoxResult respuesta = new MessageBoxResult();
                OperationResult resp = new OperationResult();
                Viaje viaje = _viaje;
                viaje.listDetalles = getLista();
                switch (motivo.imputable)
                {
                    case eImputable.CLIENTE:                       
                        if (viaje.listDetalles.Count <= 0)
                        {
                            return new OperationResult { valor = 2, mensaje = "No hay Remisiones a liberar" };
                        }
                        respuesta = MessageBox.Show(string.Format("Se liberarán las remisiones para un viaje nuevo"
                                                    + Environment.NewLine
                                                    + "¿Desea continuar?", _viaje.NumGuiaId.ToString()),
                                                    "Liberar Remisiones",
                                                    MessageBoxButton.YesNo, MessageBoxImage.Question);
                        if (respuesta == MessageBoxResult.Yes)
                        {
                            resp = new CartaPorteSvc().saveRemisionesLiberadas(viaje);
                            return resp;
                        }
                        else
                        {
                            return new OperationResult { valor = 2, mensaje = "No se realizo ninguna Operación" };
                        }                   
                    case eImputable.PEGASO:
                        if (viaje.listDetalles.Count <= 0)
                        {
                            respuesta = MessageBox.Show(string.Format("Se cancelara el viaje {0} y se liberarán las remisiones para un viaje nuevo"
                                                    + Environment.NewLine
                                                    + "¿Desea continuar?", _viaje.NumGuiaId.ToString()),
                                                    "Cancelar Viaje y liberar Remisiones",
                                                    MessageBoxButton.YesNo, MessageBoxImage.Question);

                            if (respuesta == MessageBoxResult.Yes)
                            {
                                resp = new CartaPorteSvc().cerrarCancelarCartaPorte(
                                                                                   _viaje.NumGuiaId,
                                                                                   TipoAccion.CANCELAR,
                                                                                   mainWindow.inicio._usuario.idUsuario,
                                                                                   cbxMotivos.SelectedItem.ToString(),
                                                                                   DateTime.Now);
                                if (resp.typeResult == ResultTypes.success)
                                {
                                    resp = new CartaPorteSvc().saveRemisionesLiberadas(viaje);
                                }
                                return resp;
                            }
                            else
                            {
                                return new OperationResult { valor = 2, mensaje = "No se realizo ninguna Operación" };
                            }
                        }
                        else //IMPUTABLE A PEGASO(NO SE RENVIA EL VIAJE)
                        {
                            respuesta = MessageBox.Show(string.Format("Se cancelara el viaje {0} sin liberar remisiones" + Environment.NewLine
                                                        + "¿Desea continuar?", _viaje.NumGuiaId.ToString()),
                                                        "Cancelar Viaje",
                                                        MessageBoxButton.YesNo, MessageBoxImage.Question);
                            if (respuesta == MessageBoxResult.Yes)
                            {
                                resp = new CartaPorteSvc().cerrarCancelarCartaPorte(
                                                                                _viaje.NumGuiaId,
                                                                                TipoAccion.CANCELAR,
                                                                                mainWindow.inicio._usuario.idUsuario,
                                                                                cbxMotivos.SelectedItem.ToString(),
                                                                                DateTime.Now);
                                return resp;
                            }
                            else
                            {
                                return new OperationResult { valor = 2, mensaje = "No se realizo ninguna Operación" };
                            }

                        }
                    case eImputable.CASO_ESPECIAL:
                        return new OperationResult { valor = 2, mensaje = "Operación sin definir" };
                    default:
                        return new OperationResult { valor = 2, mensaje = "No se realizo ninguna Operación" };
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private List<CartaPorte> getLista()
        {
            try
            {
                List<CartaPorte> lista = new List<CartaPorte>();
                foreach (ListViewItem item in lvlRemisiones.Items)
                {
                    lista.Add((CartaPorte)item.Content);
                }
                return lista;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(new OperationResult { valor = 1, mensaje = ex.Message });
                return null;
            }
        }

        private OperationResult save(Viaje newViaje)
        {
            try
            {
                var ruta = new Util().getRutaXML();
                if (string.IsNullOrEmpty(ruta))
                {
                    return new OperationResult { valor = 2, mensaje = "Para proceder se requiere la ruta del archivo XML" };
                }
                #region MAPEO
                ComercialCliente cli = new ComercialCliente
                {
                    nombre = newViaje.cliente.nombre,
                    calle = newViaje.cliente.domicilio,
                    rfc = newViaje.cliente.rfc,
                    colonia = newViaje.cliente.colonia,
                    estado = newViaje.cliente.estado,
                    municipio = newViaje.cliente.municipio,
                    pais = newViaje.cliente.pais,
                    telefono = newViaje.cliente.telefono,
                    cp = newViaje.cliente.cp
                };

                ComercialEmpresa emp = new ComercialEmpresa
                {
                    nombre = ((Empresa)mainWindow.inicio._usuario.personal.empresa).nombre,
                    calle = (mainWindow.inicio._usuario.personal.empresa).direccion,
                    rfc = (mainWindow.inicio._usuario.personal.empresa).rfc,
                    colonia = (mainWindow.inicio._usuario.personal.empresa).colonia,
                    estado = (mainWindow.inicio._usuario.personal.empresa).estado,
                    municipio = (mainWindow.inicio._usuario.personal.empresa).municipio,
                    pais = (mainWindow.inicio._usuario.personal.empresa).pais,
                    telefono = (mainWindow.inicio._usuario.personal.empresa).telefono,
                    cp = (mainWindow.inicio._usuario.personal.empresa).cp
                };

                var datosFactura = ObtenerDatosFactura.obtenerDatos(ruta);
                if (datosFactura == null)
                {
                    return new OperationResult { valor = 1, mensaje = "Ocurrio un error al intentar obtener datos del XML" };
                }
                #endregion
                int f = 0;
                OperationResult resp = new CartaPorteSvc().saveCartaPorte(newViaje, ref f,true);
                if (resp.typeResult == ResultTypes.success)
                {
                    Viaje saveViaje = (Viaje)resp.result;
                    envioCorreo(saveViaje);

                    if (new Util().isActivoImpresion())
                    {
                        List<CartaPorte> listaAgrupada = agrupar(saveViaje.listDetalles);
                        foreach (var item in listaAgrupada)
                        {
                            imprimirReportes(cli, emp, datosFactura, item);
                        }
                    }
                    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                    return resp;
                }
                else
                {
                    return resp;
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        private void imprimirReportes(ComercialCliente respCliente, ComercialEmpresa respEmpresa, DatosFacturaXML datosFactura, CartaPorte cartaPorte)
        {
            try
            {
                Cursor = Cursors.Wait;
                ReportView reporte = new ReportView(mainWindow);
                bool resp = reporte.cartaPorteFalso(respCliente, respEmpresa, datosFactura, cartaPorte);
                if (resp)
                {
                    //reporte.Show();
                    //MessageBox.Show("Imprimiendo carta porte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Ocurrio un error al intentar imprimir la Carta Porte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private List<CartaPorte> agrupar(List<CartaPorte> listDetalles)
        {
            var grupo = new AgruparCartaPortes(listDetalles);
            return grupo._listaAgrupada;
        }
        private void envioCorreo(Viaje viaje)
        {
            EnvioCorreo envioCorreo = new EnvioCorreo();
            if (new Util().isActivoCorreoGranjas())
            {
                var mensaje = envioCorreo.envioCorreoDeCartaPorte(viaje);
            }
            if (new Util().isActivoCorreoCSI())
            {
                var mensaje2 = envioCorreo.envioCorreoCSI(viaje);
            }
        }
        private Viaje mapForm()
        {
            try
            {
                Viaje viaje = _viaje;
                viaje.NumGuiaId = 0;
                viaje.EstatusGuia = "ACTIVO";
                viaje.listDetalles = new List<CartaPorte>();
                foreach (ListViewItem item in lvlCartaPortes.Items)
                {
                    CartaPorte cp = new CartaPorte();
                    cp = (CartaPorte)item.Content;
                    cp.Consecutivo = 0;
                    viaje.listDetalles.Add(cp);
                }

                return viaje;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
