﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para catServicios.xaml
    /// </summary>
    public partial class catServicios : ViewBase
    {
        public catServicios()
        {
            InitializeComponent();
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            cbxTipoOrdenServ.ItemsSource = Enum.GetValues(typeof(enumTipoOrdServ));
            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);
        }
        void mapForm(Servicio servicio)
        {
            try
            {
                if (servicio != null)
                {
                    ctrClave.Tag = servicio;
                    ctrClave.valor = servicio.idServicio;
                    chcActivo.IsChecked = servicio.activo;
                    txtNombreServicio.Text = servicio.nombre;
                    cbxTipoOrdenServ.SelectedItem = cbxTipoOrdenServ.ItemsSource.Cast<enumTipoOrdServ>().ToList().Find(s => s == servicio.enumTipoOrdServ);
                    chcLlantera.IsChecked = servicio.llantera;
                    chcTaller.IsChecked = servicio.taller;
                    chcLavadero.IsChecked = servicio.lavadero;
                }
                else
                {
                    ctrClave.Tag = null;
                    ctrClave.valor = 0;
                    chcActivo.IsChecked = true;
                    txtNombreServicio.Clear();
                    cbxTipoOrdenServ.SelectedItem = null;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                mapForm(null);
            }
        }
        Servicio mapForm()
        {
            try
            {
                Servicio servicio = new Servicio
                {
                    idServicio = ctrClave.Tag == null ? 0 : (ctrClave.Tag as Servicio).idServicio,
                    nombre = txtNombreServicio.Text,
                    descipcion = txtNombreServicio.Text,
                    //enumTipoOrdServ =  (enumTipoOrdServ)cbxTipoOrdenServ.SelectedItem,
                    activo = chcActivo.IsChecked.Value,
                    taller = chcTaller.IsChecked.Value,
                    lavadero = chcLavadero.IsChecked.Value,
                    llantera = chcLlantera.IsChecked.Value
                };
                return servicio;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult validacion = validar();
                if (validacion.typeResult != ResultTypes.success) return validacion;

                Servicio servicio = mapForm();
                if (servicio != null)
                {
                    var resp = new ServiciosSvc().saveServicio(ref servicio);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mapForm(servicio);
                        base.guardar();
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult(ResultTypes.warning, "OCURRIO UN ERROR AL LEER LA INFORMACIÓN DE LA PANTALLA.");
                }

            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public override OperationResult eliminar()
        {
            try
            {
                Cursor = Cursors.Wait;

                Servicio servicio = mapForm();
                servicio.activo = false;
                if (servicio != null)
                {
                    var resp = new ServiciosSvc().saveServicio(ref servicio);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mainWindow.habilitar = true;
                        nuevo();
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult(ResultTypes.warning, "OCURRIO UN ERROR AL LEER LA INFORMACIÓN DE LA PANTALLA.");
                }

            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private OperationResult validar()
        {
            try
            {
                OperationResult operation = new OperationResult() { valor = 0, mensaje = "PARA CONTINUAR SE REQUIERE:" + Environment.NewLine };
                if (string.IsNullOrEmpty(txtNombreServicio.Text.Trim()))
                {
                    operation.valor = 2;
                    operation.mensaje += "  *   Proporcionar el nombre del Servicio" + Environment.NewLine;
                }
                //if (cbxTipoOrdenServ.SelectedItem == null)
                //{
                //    operation.valor = 2;
                //    operation.mensaje += "  *   Elegir el tipo de orden de servicio" + Environment.NewLine;
                //}
                return operation;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public override void buscar()
        {
            Servicio servicio = new selectServicios().getServicio();
            if (servicio != null)
            {
                mapForm(servicio);
                base.buscar();
            }
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new ServiciosSvc().getAllServicios();
                if (resp.typeResult == ResultTypes.success)
                {
                    var lista = resp.result as List<Servicio>;
                    new ReportView().exportarListaServicios(lista);
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
