﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using Core.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for editCotizarOrdenes.xaml
    /// </summary>
    public partial class editCotizarOrdenes : ViewBase
    {
        public editCotizarOrdenes()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresa.loaded(mainWindow.empresa, true);
            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            lvlPreOrd.Items.Clear();
            stpPreOrdenes.Children.Clear();
            mapForm(null);
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lvlPreOrd.Items.Clear();
            stpPreOrdenes.Children.Clear();
        }

        private void btnAñadirOrdenes_Click(object sender, RoutedEventArgs e)
        {
            if (ctrEmpresa.empresaSelected == null)
            {
                ImprimirMensaje.imprimir(new OperationResult { valor = 3, mensaje = "Elegir una empresa" });
                return;
            }
            List<PreOrdenCompra> lista = new buscarPreOrdenesCompra(ctrEmpresa.empresaSelected, getListaPreOrdenCompra()).getPreOrdByFiltros();
            if (lista != null)
            {
                mapLista(lista);
            }
        }

        private void mapLista(List<PreOrdenCompra> lista)
        {
            foreach (PreOrdenCompra item in lista)
            {
                ListViewItem lvl = new ListViewItem { Content = item };
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                lvlPreOrd.Items.Add(lvl);
            }
            agruparDetalles(lista);
        }
        List<DetallePreOrdenCompra> listaLvl = new List<DetallePreOrdenCompra>();

        List<PreOrdenCompra> getListaPreOrdenCompra()
        {
            List<PreOrdenCompra> lista = new List<PreOrdenCompra>();
            foreach (ListViewItem item in lvlPreOrd.Items)
            {
                lista.Add(item.Content as PreOrdenCompra);
            }
            return lista;
        }

        private void agruparDetalles(List<PreOrdenCompra> lista)
        {
            listaLvl = new List<DetallePreOrdenCompra>();
            var listLbl = stpPreOrdenes.Children.Cast<Label>().ToList();
            foreach (Label item in listLbl)
            {
                listaLvl.Add(item.Tag as DetallePreOrdenCompra);
            }

            foreach (PreOrdenCompra pre in lista)
            {
                if (pre.listaDetalles == null)
                {
                    continue;
                }
                foreach (DetallePreOrdenCompra detalle in pre.listaDetalles)
                {
                    bool econtro = false;
                    int i = 0;
                    foreach (DetallePreOrdenCompra det in listaLvl)
                    {
                        if (det.producto.idProducto == detalle.producto.idProducto)
                        {
                            econtro = true;
                            break;
                        }
                        i++;
                    }
                    if (econtro)
                    {
                        listaLvl[i].cantidadSolicitada += detalle.cantidadSolicitada;
                    }
                    else
                    {
                        listaLvl.Add(detalle);
                    }
                }
            }
            stpPreOrdenes.Children.Clear();
            foreach (var item in listaLvl)
            {
                Label lblPrincipal = new Label { Tag = item };
                StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal, Margin = new Thickness(0) };

                Label lblOrdenTrabajo = new Label
                {
                    Tag = item,
                    Content = item.producto.nombre,
                    HorizontalContentAlignment = HorizontalAlignment.Left,
                    Width = 300,
                    Margin = new Thickness(0)
                };
                stpContent.Children.Add(lblOrdenTrabajo);

                Label lblFecha = new Label
                {
                    Tag = item,
                    Content = item.cantidadSolicitada,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Width = 130
                };
                stpContent.Children.Add(lblFecha);

                controlEnteros txtCantidad = new controlEnteros
                {
                    valor = Convert.ToInt32(item.cantidad),
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Width = 130
                };
                stpContent.Children.Add(txtCantidad);

                lblPrincipal.Content = stpContent;
                stpPreOrdenes.Children.Add(lblPrincipal);
            }
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            lvlPreOrd.Items.Remove(lvlPreOrd.SelectedItem);
        }

        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult resp = new OperationResult();
                SolicitudOrdenCompra solicitud = mapForm();
                List<int> listaPreOC = getLista();
                resp = new SolicitudOrdenCompraSvc().saveSolicitudOrdenCompra(ref solicitud, listaPreOC);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(solicitud);
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private List<int> getLista()
        {
            List<int> lista = new List<int>();
            foreach (ListViewItem item in lvlPreOrd.Items)
            {
                lista.Add(((PreOrdenCompra)item.Content).idPreOrdenCompra);
            }
            return lista;
        }

        private void mapForm(SolicitudOrdenCompra solicitud)
        {
            if (solicitud != null)
            {
                txtFolio.Tag = solicitud;
                txtFolio.Text = solicitud.idSolicitudOrdenCompra.ToString();
                
            }
            else
            {
                txtFolio.Tag = null;
                txtFolio.Clear();
                ctrEmpresa.cbxEmpresa.SelectedItem = null;
            }
        }

        private SolicitudOrdenCompra mapForm()
        {
            try
            {
                SolicitudOrdenCompra solicitud = new SolicitudOrdenCompra
                {
                    idSolicitudOrdenCompra = txtFolio.Tag == null ? 0 : ((SolicitudOrdenCompra)txtFolio.Tag).idSolicitudOrdenCompra,
                    empresa = mainWindow.empresa,
                    estatus = "ACTIVO",
                    fecha = DateTime.Now,
                    isLlantas = false,
                    obsevaciones = "",
                    usuario = mainWindow.inicio._usuario.nombreUsuario
                };
                solicitud.listaDetalles = obtenerLista();
                return solicitud;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private List<DetalleSolicitudOrdenCompra> obtenerLista()
        {
            List<DetalleSolicitudOrdenCompra> lista = new List<DetalleSolicitudOrdenCompra>();
            List<Label> listLabel = stpPreOrdenes.Children.Cast<Label>().ToList();

            foreach (Label lbl in listLabel)
            {
                DetallePreOrdenCompra det = lbl.Tag as DetallePreOrdenCompra;
                DetalleSolicitudOrdenCompra detSolicitud = new DetalleSolicitudOrdenCompra();

                detSolicitud.idDetalleSolitudOrdenCompra = 0;
                detSolicitud.producto = det.producto;

                StackPanel stp = (StackPanel)lbl.Content;
                foreach (var item in stp.Children.Cast<object>().ToList())
                {
                    if (item is controlEnteros)
                    {
                        detSolicitud.cantidad = ((controlEnteros)item).valor;
                    }
                }
                detSolicitud.unidadTrans = null;

                lista.Add(detSolicitud);
            }
            return lista;
        }
    }
}
