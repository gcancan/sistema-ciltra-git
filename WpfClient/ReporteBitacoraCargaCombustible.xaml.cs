﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using CoreFletera.Interfaces;
    using CoreFletera.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Input;
    using System.Windows.Markup;

    public partial class ReporteBitacoraCargaCombustible : WpfClient.ViewBase
    {
        private List<BitacoraCargaCombustible> lista;
        private List<GrupoCargaCombustible> listGrupoCargaCombustible;
        private Empresa empresa;
        private DateTime fechaInicial;
        private DateTime fechaFinal;
        private string tractor;        
        public ReporteBitacoraCargaCombustible()
        {
            this.lista = new List<BitacoraCargaCombustible>();
            this.listGrupoCargaCombustible = new List<GrupoCargaCombustible>();
            this.tractor = "%";
            this.InitializeComponent();
        }
        public ReporteBitacoraCargaCombustible(Empresa empresa, DateTime fechaInicial, DateTime fechaFinal, string tractor)
        {
            this.lista = new List<BitacoraCargaCombustible>();
            this.listGrupoCargaCombustible = new List<GrupoCargaCombustible>();
            this.tractor = "%";
            this.empresa = empresa;
            this.fechaInicial = fechaInicial;
            this.fechaFinal = fechaFinal;
            this.tractor = tractor;
            this.InitializeComponent();
        }
        private void addLista(List<BitacoraCargaCombustible> result)
        {
            this.lista = result;
            this.lvlResultado.ItemsSource = null;
            this.lvlResultado.Items.Clear();
            this.listGrupoCargaCombustible = new List<GrupoCargaCombustible>();
            List<ListViewItem> list = new List<ListViewItem>();
            foreach (BitacoraCargaCombustible combustible in result)
            {
                ContextMenu menu = new ContextMenu();
                MenuItem newItem = new MenuItem
                {
                    Header = "Ver Detalles",
                    Tag = combustible
                };
                newItem.Click += new RoutedEventHandler(this.MenuDetalles_Click);
                menu.Items.Add(newItem);
                this.agruparCargaCombustible(combustible);
                ListViewItem item = new ListViewItem
                {
                    Content = combustible
                };
                item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                item.ContextMenu = menu;
                list.Add(item);
            }
            this.lvlResultado.ItemsSource = list;
            CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlResultado.ItemsSource);
            defaultView.Filter = new Predicate<object>(this.UserFilter);
            List<ListViewItem> list2 = new List<ListViewItem>();
            List<GrupoCargaCombustible> list3 = (from g in result
                                                 group g by g.unidad into grp
                                                 select new GrupoCargaCombustible
                                                 {
                                                     idTransporte = (from s in grp select s.unidad).First<string>(),
                                                     kmRecorrido = grp.Sum<BitacoraCargaCombustible>(s => s.kmDiferencia),
                                                     litros = grp.Sum<BitacoraCargaCombustible>(s => s.sumaLitros)
                                                 }).ToList<GrupoCargaCombustible>();
            this.lvlResumen.ItemsSource = list3;
            CollectionView view2 = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlResumen.ItemsSource);
            view2.Filter = new Predicate<object>(this.UserFilter2);
        }

        private void agruparCargaCombustible(BitacoraCargaCombustible bitacora)
        {
            int num = 0;
            foreach (GrupoCargaCombustible combustible in this.listGrupoCargaCombustible)
            {
                if (combustible.idTransporte == bitacora.unidad)
                {
                    GrupoCargaCombustible local1 = this.listGrupoCargaCombustible[num];
                    local1.kmRecorrido += bitacora.kmRecorridos;
                    GrupoCargaCombustible local2 = this.listGrupoCargaCombustible[num];
                    local2.litros += bitacora.sumaLitros;
                    return;
                }
                num++;
            }
            GrupoCargaCombustible item = new GrupoCargaCombustible
            {
                idTransporte = bitacora.unidad,
                kmRecorrido = bitacora.kmRecorridos,
                litros = bitacora.sumaLitros
            };
            this.listGrupoCargaCombustible.Add(item);
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            this.buscarCargas(this.tractor);
        }

        private void btnImprimir_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                if (this.lista.Count > 0)
                {
                    ReportView view = new ReportView();
                    if (view.ReporteBitacoraCargaCombustible(this.lista, this.ctlFechas.fechaInicial, this.ctlFechas.fechaFinal, this.ctlFechas.isMes, this.ctrEmpresas.empresaSelected))
                    {
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void buscarCargas(string tractor)
        {
            try
            {
                this.txtFiltro.Clear();
                this.lvlResultado.ItemsSource = null;
                this.lvlResultado.Items.Clear();
                lvlResumen.ItemsSource = null;
                lvlResumen.Items.Clear();
                base.Cursor = Cursors.Wait;
                if (string.IsNullOrEmpty(tractor))
                {
                    OperationResult resp = new BitacoraCargaCombustibleSvc().getBitacoraCargaCombustible(this.ctlFechas.fechaInicial, this.ctlFechas.fechaFinal, this.ctrEmpresas.empresaSelected.clave, tractor, (this.cbxProveedor.SelectedItem as ProveedorCombustible).idProveedor);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        this.addLista((List<BitacoraCargaCombustible>)resp.result);
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
                else
                {
                    List<string> listaUnidades = cbxUnidadesTrans.SelectedItems.Cast<UnidadTransporte>().ToList().Select(s => s.clave).ToList();
                    List<string> listaProveedores = cbxProveedor.SelectedItems.Cast<ProveedorCombustible>().ToList().Select(s => s.idProveedor.ToString()).ToList();
                    List<string> listaOperadores = cbxOperadores.SelectedItems.Cast<Personal>().ToList().Select(s => s.clave.ToString()).ToList();
                    var resp = new BitacoraCargaCombustibleSvc().getBitacoraCargaCombustibleByFiltros(ctlFechas.fechaInicial, ctlFechas.fechaFinal,
                        ctrEmpresas.empresaSelected.clave, ctrZonaOperativa.zonaOperativa.idZonaOperativa, listaProveedores, chcTodosProveedores.IsChecked.Value,
                        listaUnidades, chcTodosUnidades.IsChecked.Value,
                        listaOperadores, chcTodosOperadores.IsChecked.Value);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        this.addLista((List<BitacoraCargaCombustible>)resp.result);
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
                
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private bool cargarProveedores()
        {
            try
            {
                OperationResult result = new CargaCombustibleSvc().getProveedor(0);
                if (result.typeResult == ResultTypes.success)
                {
                    List<ProveedorCombustible> list = (List<ProveedorCombustible>)result.result;
                   
                    foreach (ProveedorCombustible combustible in list.OrderBy(s=>s.nombre))
                    {
                        this.cbxProveedor.Items.Add(combustible);
                    }
                    //this.cbxProveedor.SelectedIndex = 0;
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
        void cargarOperadores()
        {
            try
            {
                chcTodosOperadores.IsChecked = false;
                cbxOperadores.ItemsSource = null;
                var resp = new OperadorSvc().getOperadoresALLorById(0, ctrEmpresas.empresaSelected.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxOperadores.ItemsSource = (resp.result as List<Personal>);
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
        }

        private void MenuDetalles_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = (MenuItem)sender;
            BitacoraCargaCombustible tag = (BitacoraCargaCombustible)item.Tag;
            new detallesCargaCombustible().mapearPantalla(tag.orden);
        }

        public override void nuevo()
        {
            base.nuevo();
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
        }       

        private void txtFiltro_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.lvlResultado.ItemsSource != null)
            {
                CollectionViewSource.GetDefaultView(this.lvlResultado.ItemsSource).Refresh();
                CollectionViewSource.GetDefaultView(this.lvlResumen.ItemsSource).Refresh();
            }
        }

        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtFiltro.Text) || (((BitacoraCargaCombustible)(item as ListViewItem).Content).unidad.IndexOf(this.txtFiltro.Text, StringComparison.OrdinalIgnoreCase) >= 0));

        private bool UserFilter2(object item) =>
            (string.IsNullOrEmpty(this.txtFiltro.Text) || ((item as GrupoCargaCombustible).idTransporte.IndexOf(this.txtFiltro.Text, StringComparison.OrdinalIgnoreCase) >= 0));

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.cargarProveedores())
            {
            }
            if (base.mainWindow == null)
            {
                this.ctrEmpresas.loaded(this.empresa, false);
                this.ctlFechas.IsEnabled = false;
                this.btnBuscar.IsEnabled = false;
                this.ctlFechas.mostrarFechas(this.fechaInicial, this.fechaFinal);
                this.buscarCargas(this.tractor);
                
            }
            else
            {
                ctrEmpresas.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
                this.ctrEmpresas.loaded(base.mainWindow.empresa, true);
                this.nuevo();
                ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
                ctrZonaOperativa.cargarControl(mainWindow.zonaOperativa, mainWindow.usuario.idUsuario);
            }
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cargarUnidades();
            cargarOperadores();
        }

        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cargarUnidades();
        }
        void cargarUnidades()
        {
            try
            {
                Cursor = Cursors.Wait;
                if (ctrZonaOperativa.zonaOperativa != null)
                {
                    var resp = new UnidadTransporteSvc().getUnidadesCarganByEmpresaZonaOperativa(ctrEmpresas.empresaSelected.clave, ctrZonaOperativa.zonaOperativa.idZonaOperativa);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        cbxUnidadesTrans.ItemsSource = resp.result as List<UnidadTransporte>;
                    }
                    else
                    {
                        cbxUnidadesTrans.ItemsSource = null;
                    }
                }
                else
                {
                    cbxUnidadesTrans.ItemsSource = null;
                }

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public class GrupoCargaCombustible
        {
            public string idTransporte { get; set; }
            public decimal kmRecorrido { get; set; }
            public decimal litros { get; set; }
            public decimal rendimiento =>
                (this.kmRecorrido / this.litros);
        }

        private void ChcTodosProveedores_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                cbxProveedor.SelectedItems.Clear();
                Cursor = Cursors.Wait;
                CheckBox chc = sender as CheckBox;
                if (chc.IsChecked.Value)
                {
                    foreach (var item in cbxProveedor.Items)
                    {
                        cbxProveedor.SelectedItems.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void ChcTodosOperadores_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                cbxOperadores.SelectedItems.Clear();
                Cursor = Cursors.Wait;
                CheckBox chc = sender as CheckBox;
                if (chc.IsChecked.Value)
                {
                    foreach (var item in cbxOperadores.Items)
                    {
                        cbxOperadores.SelectedItems.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void ChcTodosUnidades_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                cbxUnidadesTrans.SelectedItems.Clear();
                Cursor = Cursors.Wait;
                CheckBox chc = sender as CheckBox;
                if (chc.IsChecked.Value)
                {
                    foreach (var item in cbxUnidadesTrans.Items)
                    {
                        cbxUnidadesTrans.SelectedItems.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
