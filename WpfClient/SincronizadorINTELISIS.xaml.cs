﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para SincronizadorINTELISIS.xaml
    /// </summary>
    public partial class SincronizadorINTELISIS : ViewBase
    {
        public SincronizadorINTELISIS()
        {
            InitializeComponent();
        }
        private void sincEmpresas_Click(object sender, RoutedEventArgs e)
        {            
            if (stpContenedor.Children.Count > 0)
            {
                object obj = stpContenedor.Children[0];
                if (!(obj is INTELISIS_catEmpresas))
                {
                    stpContenedor.Children.Clear();
                    stpContenedor.Children.Add(new INTELISIS_catEmpresas(this));
                }
            }
            else
            {
                stpContenedor.Children.Add(new INTELISIS_catEmpresas(this));
            }            
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
        }

        private void sincSucursal_Click(object sender, RoutedEventArgs e)
        {
            if (stpContenedor.Children.Count > 0)
            {
                object obj = stpContenedor.Children[0];
                if (!(obj is INTELISIS_catSucursal))
                {
                    stpContenedor.Children.Clear();
                    stpContenedor.Children.Add(new INTELISIS_catSucursal(this));
                }
            }
            else
            {
                stpContenedor.Children.Add(new INTELISIS_catSucursal(this));
            }
        }

        private void sincClientes_Click(object sender, RoutedEventArgs e)
        {
            if (stpContenedor.Children.Count > 0)
            {
                object obj = stpContenedor.Children[0];
                if (!(obj is INTELISIS_catClientes))
                {
                    stpContenedor.Children.Clear();
                    stpContenedor.Children.Add(new INTELISIS_catClientes(this));
                }
            }
            else
            {
                stpContenedor.Children.Add(new INTELISIS_catClientes(this));
            }
        }

        private void sincPersonal_Click(object sender, RoutedEventArgs e)
        {
            if (stpContenedor.Children.Count > 0)
            {
                object obj = stpContenedor.Children[0];
                if (!(obj is INTELISIS_catPersonal))
                {
                    stpContenedor.Children.Clear();
                    stpContenedor.Children.Add(new INTELISIS_catPersonal(this));
                }
            }
            else
            {
                stpContenedor.Children.Add(new INTELISIS_catPersonal(this));
            }
        }

        private void sinUEN_Click(object sender, RoutedEventArgs e)
        {
            if (stpContenedor.Children.Count > 0)
            {
                object obj = stpContenedor.Children[0];
                if (!(obj is INTELISIS_catUEN))
                {
                    stpContenedor.Children.Clear();
                    stpContenedor.Children.Add(new INTELISIS_catUEN(this));
                }
            }
            else
            {
                stpContenedor.Children.Add(new INTELISIS_catUEN(this));
            }
        }

        private void sincSucursalClientes_Click(object sender, RoutedEventArgs e)
        {
            if (stpContenedor.Children.Count > 0)
            {
                object obj = stpContenedor.Children[0];
                if (!(obj is INTELISIS_catSucursalCliente))
                {
                    stpContenedor.Children.Clear();
                    stpContenedor.Children.Add(new INTELISIS_catSucursalCliente(this));
                }
            }
            else
            {
                stpContenedor.Children.Add(new INTELISIS_catSucursalCliente(this));
            }
        }

        private void sincArticulos_Click(object sender, RoutedEventArgs e)
        {
            if (stpContenedor.Children.Count > 0)
            {
                object obj = stpContenedor.Children[0];
                if (!(obj is INTELISIS_catArticulos))
                {
                    stpContenedor.Children.Clear();
                    stpContenedor.Children.Add(new INTELISIS_catArticulos(this));
                }
            }
            else
            {
                stpContenedor.Children.Add(new INTELISIS_catArticulos(this));
            }
        }

        private void sincPuestos_Click(object sender, RoutedEventArgs e)
        {
            if (stpContenedor.Children.Count > 0)
            {
                object obj = stpContenedor.Children[0];
                if (!(obj is INTELISIS_catPuestos))
                {
                    stpContenedor.Children.Clear();
                    stpContenedor.Children.Add(new INTELISIS_catPuestos(this));
                }
            }
            else
            {
                stpContenedor.Children.Add(new INTELISIS_catPuestos(this));
            }
        }

        private void sincDpto_Click(object sender, RoutedEventArgs e)
        {
            if (stpContenedor.Children.Count > 0)
            {
                object obj = stpContenedor.Children[0];
                if (!(obj is INTELISIS_catDepartamentos))
                {
                    stpContenedor.Children.Clear();
                    stpContenedor.Children.Add(new INTELISIS_catDepartamentos(this));
                }
            }
            else
            {
                stpContenedor.Children.Add(new INTELISIS_catDepartamentos(this));
            }
        }

        private void sincPlazas_Click(object sender, RoutedEventArgs e)
        {
            if (stpContenedor.Children.Count > 0)
            {
                object obj = stpContenedor.Children[0];
                if (!(obj is INTELISIS_catPlazas))
                {
                    stpContenedor.Children.Clear();
                    stpContenedor.Children.Add(new INTELISIS_catPlazas(this));
                }
            }
            else
            {
                stpContenedor.Children.Add(new INTELISIS_catPlazas(this));
            }
        }

        private void sincUnidadTransporte_Click(object sender, RoutedEventArgs e)
        {
            if (stpContenedor.Children.Count > 0)
            {
                object obj = stpContenedor.Children[0];
                if (!(obj is INTELISIS_catUnidadTransporte))
                {
                    stpContenedor.Children.Clear();
                    stpContenedor.Children.Add(new INTELISIS_catUnidadTransporte(this));
                }
            }
            else
            {
                stpContenedor.Children.Add(new INTELISIS_catUnidadTransporte(this));
            }
        }
    }
}
