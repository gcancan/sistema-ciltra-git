﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for servicioLLantera.xaml
    /// </summary>
    public partial class servicioLLantera : ViewBase
    {
        public servicioLLantera()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            nuevo();
        }
        void cargarDiagrama(DiagramaLLantas diagrama, TipoOrdenServicio orden)
        {
            eje1.Children.Clear();
            eje2.Children.Clear();
            eje3.Children.Clear();
            eje4.Children.Clear();
            var listaStps = stpDiagrama.Children.Cast<StackPanel>().ToList();

            string path = System.IO.Directory.GetCurrentDirectory();
            int posicion = 0;
            for (int i = 1; i <= diagrama.numEjes; i++)
            {
                int llantas = diagrama.listaDetalles[i - 1].llantas;
                for (int j = 1; j <= llantas; j++)
                {
                    posicion++;
                    /**/
                    Image img = new Image();
                    img.Height = 40;
                    BitmapImage ima = new BitmapImage(new Uri(path + "\\Recursos\\BalanceNew.png"));
                    Label lblLLanta = new Label();
                    //chc.VerticalContentAlignment = VerticalAlignment.Bottom;
                    StackPanel stp = new StackPanel() { Orientation = Orientation.Vertical };
                    stp.Children.Add(new Label { Content = posicion, HorizontalContentAlignment = HorizontalAlignment.Center });
                    img.Source = ima;
                    stp.Children.Add(img);
                    Label lblTitulo = new Label() { FontWeight = FontWeights.Medium };
                    var respLLanta = new LlantaSvc().getLLantaByPosAndUnidadTrans(posicion, orden.unidadTrans);
                    if (respLLanta.typeResult == ResultTypes.success)
                    {
                        Llanta llanta = (Llanta)respLLanta.result;
                        lblTitulo.Content = llanta.clave;
                        lblLLanta.Tag = llanta;
                        StackPanel stpToolTip = new StackPanel { Orientation = Orientation.Vertical };
                        stpToolTip.Children.Add(new Label { Content = "INFORMACIÓN DE LA LLANTA", FontWeight = FontWeights.Bold, FontSize = 16 });

                        StackPanel stpLabels = new StackPanel();

                        StackPanel stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "UBICACIÓN:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.unidadTransporte.clave + " POS: " + llanta.posicion.ToString() });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "MARCA:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.marca.descripcion });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "TIPO LLANTA:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.tipoLlanta.nombre });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "DISEÑO:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.diseño });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "MEDIDA:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.medida });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "PROFUNDIDAD:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.profundidad });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "FECHA:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.fecha });
                        stpLabels.Children.Add(stpControl);

                        stpControl = new StackPanel() { Orientation = Orientation.Horizontal };
                        stpControl.Children.Add(new Label { Width = 120, Content = "RFID:", FontWeight = FontWeights.Bold });
                        stpControl.Children.Add(new Label { Content = llanta.rfid });
                        stpLabels.Children.Add(stpControl);

                        stpToolTip.Children.Add(stpLabels);
                        lblLLanta.ToolTip = new ToolTip { AllowDrop = true, Content = stpToolTip /*"LLANTA " + ((Llanta)respLLanta.result).clave*/ };

                    }
                    else
                    {
                        //lblLLanta.ToolTip = new ToolTip { Content = "SIN ASIGNAR" };
                        ContextMenu menu = new ContextMenu();
                        MenuItem itemAgregar = new MenuItem { Header = "Asignar llanta" };
                        menu.Items.Add(itemAgregar);
                        lblLLanta.ContextMenu = menu;
                        lblTitulo.Content = "SIN ASIGNAR";

                    }
                    stp.Children.Add(lblTitulo);
                    lblLLanta.Content = stp;
                    listaStps[i - 1].Children.Add(lblLLanta);
                    /**/
                }
            }
        }

        public override void nuevo()
        {
            txtOrdenPadre.Clear();
            txtOrdenPadre.IsEnabled = true;
            lvlOrdenes.Items.Clear();
            eje1.Children.Clear();
            eje2.Children.Clear();
            eje3.Children.Clear();
            eje4.Children.Clear();
            mapOrdenesTrabajo(new List<DetalleLlantera>());

            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
        }

        private void txtOrdenPadre_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void txtOrdenPadre_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter && !string.IsNullOrEmpty(txtOrdenPadre.Text.Trim()))
            {
                int i = 0;
                if (int.TryParse(txtOrdenPadre.Text.Trim(), out i))
                {
                    buscarMasterOrdenSer(i);
                }
                else
                {
                    MessageBox.Show("El folio de la orden de servicio no es valido", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
        MasterOrdServ master = new MasterOrdServ();
        private void buscarMasterOrdenSer(int i)
        {
            master = new MasterOrdServ();
            try
            {
                nuevo();
                var resp = new MasterOrdServSvc().getMasterOrdServ(i);
                if (resp.typeResult == ResultTypes.success)
                {
                    txtOrdenPadre.Text = i.ToString();
                    txtOrdenPadre.IsEnabled = false;
                    master = (MasterOrdServ)resp.result;
                    //txtPersonalEntrega.personal = master.chofer;
                    maplvlOrdenes(master.listTiposOrdenServicio);
                    if (lvlOrdenes.Items.Count == 0)
                    {
                        ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "NO EXISTEN TRABAJOS DE LLANTERA PARA ESTE FOLIO" });
                        nuevo();
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        void maplvlOrdenes(List<TipoOrdenServicio> list)
        {
            lvlOrdenes.Items.Clear();
            foreach (var item in list)
            {
                if (item.TipoServicio.ToUpper() == "LLANTAS")
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = item;
                    lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                    lvl.Selected += Lvl_Selected;
                    ContextMenu menu = new ContextMenu();

                    MenuItem menuAsignar = new MenuItem { Header = "Asignar Personal", Tag = item, IsEnabled = item.estatus.ToUpper() == "PRE" };
                    menuAsignar.Click += MenuAsignar_Click;
                    menu.Items.Add(menuAsignar);

                    MenuItem menuNuevaOrdenTrabajo = new MenuItem { Header = "Agregar Nueva Orden De Trabajo", Tag = item/*, IsEnabled = item.estatus.ToUpper() == "ASIG"*/ };
                    menuNuevaOrdenTrabajo.Click += MenuNuevaOrdenTrabajo_Click;
                    menu.Items.Add(menuNuevaOrdenTrabajo);

                    lvl.ContextMenu = menu;
                    lvlOrdenes.Items.Add(lvl);
                }

            }
        }

        private void MenuNuevaOrdenTrabajo_Click(object sender, RoutedEventArgs e)
        {
            TipoOrdenServicio tipoOrde = (TipoOrdenServicio)((MenuItem)sender).Tag;
            agregarOrdenLlantera agregarOrd = new agregarOrdenLlantera(tipoOrde, true);
            if (agregarOrd.ShowDialog().Value)
            {
                int master = Convert.ToInt32(txtOrdenPadre.Text);
                buscarMasterOrdenSer(master);
                int i = 0;
                foreach (ListViewItem item in lvlOrdenes.Items)
                {
                    TipoOrdenServicio _tipoOrde = (TipoOrdenServicio)item.Content;
                    if (tipoOrde.idOrdenSer == _tipoOrde.idOrdenSer)
                    {
                        lvlOrdenes.SelectedIndex = i;
                        return;
                    }
                    i++;
                }
            }

        }

        private void MenuAsignar_Click(object sender, RoutedEventArgs e)
        {
            TipoOrdenServicio ordenServ = (TipoOrdenServicio)((MenuItem)sender).Tag;
            asignarPersonalOrdenServicio pantalla = new asignarPersonalOrdenServicio(ordenServ, master, mainWindow);
            var resp = pantalla.ShowDialog();
            if (resp.Value)
            {
                buscarMasterOrdenSer(Convert.ToInt32(txtOrdenPadre.Text));
                int i = 0;
                foreach (ListViewItem item in lvlOrdenes.Items)
                {
                    TipoOrdenServicio tOrd = (TipoOrdenServicio)item.Content;
                    if (tOrd.idOrdenSer == ordenServ.idOrdenSer)
                    {
                        lvlOrdenes.SelectedIndex = i;
                        break;
                    }
                    i++;
                }
            }
        }

        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                eventoOrdenes((TipoOrdenServicio)((ListViewItem)sender).Content);
            }
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
            {
                eventoOrdenes((TipoOrdenServicio)((ListViewItem)sender).Content);
            }
        }
        void eventoOrdenes(TipoOrdenServicio orden)
        {
            try
            {
                Cursor = Cursors.Wait;
                lvlActividades.Items.Clear();

                var r = new DiagramasLLantasSvc().getDiagramaByTipoUnidadTrans(orden.unidadTrans.tipoUnidad);
                if (r.typeResult != ResultTypes.success)
                {
                    ImprimirMensaje.imprimir(r);
                }
                else
                {
                    cargarDiagrama((DiagramaLLantas)r.result, orden);
                }

                var respLlanta = new LlantaSvc().getDetallesServLlantera(orden.idOrdenSer);
                if (respLlanta.typeResult == ResultTypes.success)
                {
                    mapOrdenesTrabajo((List<DetalleLlantera>)respLlanta.result);
                }
                else
                {
                    ImprimirMensaje.imprimir(respLlanta);
                }             
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapOrdenesTrabajo(List<DetalleLlantera> result)
        {
            lvlActividades.Items.Clear();
            foreach (var item in result)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                ContextMenu menu = new ContextMenu();
                MenuItem menuItem = new MenuItem { Header = "Finalizar Orden de Trabajo", Tag = item, IsEnabled = item.estatus == "ASIG" };
                if (item.estatus == "TER")
                {
                    lvl.Foreground = Brushes.SteelBlue;
                }
                menuItem.Click += MenuItem_Click;
                menu.Items.Add(menuItem);
                lvl.ContextMenu = menu;
                lvl.Selected += Lvl_Selected1;
                lvl.MouseDoubleClick += Lvl_Selected1;
                lvlActividades.Items.Add(lvl);
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            DetalleLlantera ordenTrab = ((DetalleLlantera)((MenuItem)sender).Tag);
            List<DetalleLlantera> listaOrdenes = new List<DetalleLlantera>() { ordenTrab };
            finalizarOrdenes(listaOrdenes, ordenTrab.idOrdenServ);
        }

        private void finalizarOrdenes(List<DetalleLlantera> listaOrdenes, int idOrdenServ)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new LlantaSvc().finalizarOrdenesTrabajo(listaOrdenes, idOrdenServ);
                if (resp.typeResult == ResultTypes.success)
                {
                    buscarMasterOrdenSer(Convert.ToInt32(txtOrdenPadre.Text));
                    int i = 0;
                    foreach (ListViewItem item in lvlOrdenes.Items)
                    {
                        TipoOrdenServicio tOrd = (TipoOrdenServicio)item.Content;
                        if (tOrd.idOrdenSer == idOrdenServ)
                        {
                            lvlOrdenes.SelectedIndex = i;
                            break;
                        }
                        i++;
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }  
            finally
            {
                Cursor = Cursors.Arrow;
            }          
        }

        private void Lvl_Selected1(object sender, RoutedEventArgs e)
        {
            DetalleLlantera det = ((DetalleLlantera)((ListViewItem)sender).Content);
            seleccionarImagen(det);
        }

        private void seleccionarImagen(DetalleLlantera det)
        {
            List<StackPanel> listaStps = stpDiagrama.Children.Cast<StackPanel>().ToList();
            foreach (var stpLabel in listaStps)
            {
                List<Label> listaLabel = stpLabel.Children.Cast<Label>().ToList();
                foreach (var item in listaLabel)
                {
                    Llanta llanta = (Llanta)item.Tag;
                    if (llanta.clave == det.llanta.clave)
                    {
                        item.Background = Brushes.SteelBlue;
                    }
                    else
                    {
                        item.Background = null;
                    }
                }
            }
        }

        private void btnFinalizarOrdenes_Click(object sender, RoutedEventArgs e)
        {
            List<DetalleLlantera> listaOrdenes = new List<DetalleLlantera>();
            foreach (ListViewItem item in lvlActividades.Items)
            {
                if (((DetalleLlantera)item.Content).estatus == "ASIG")
                {
                    listaOrdenes.Add((DetalleLlantera)item.Content);
                }
            }
            if (listaOrdenes.Count > 0)
            {
                finalizarOrdenes(listaOrdenes, listaOrdenes[0].idOrdenServ);
            }
            else
            {
                ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "NO HAY ORDENES PARA FINALIZAR" });
            }
        }
    }
}
