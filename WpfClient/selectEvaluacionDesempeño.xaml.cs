﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectEvaluacionDesempeño.xaml
    /// </summary>
    public partial class selectEvaluacionDesempeño : MetroWindow
    {
        public selectEvaluacionDesempeño()
        {
            InitializeComponent();
        }
        bool mostrarResultados = false;
        public selectEvaluacionDesempeño(bool mostrarResultados)
        {
            InitializeComponent();
            this.mostrarResultados = mostrarResultados;
        }

        private void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (mostrarResultados)
            {
                GridView view = this.lvlEvaluaciones.View as GridView;
                GridViewColumnCollection columns = view.Columns;
                GridViewColumn itemResultado = new GridViewColumn
                {
                    Header = "Resultado",
                    Width = 90,
                    DisplayMemberBinding = new Binding("resultadoP2")
                    {
                        StringFormat = "P2"
                    }
                };
                columns.Add(itemResultado);
            }
        }
        int idDepto = 0;
        public EvaluacionDesempeño getEvaluacionByDepartamento(int idDepto)
        {
            try
            {
                this.idDepto = idDepto;
                getAllEvaluacionDepto(idDepto);
                bool? result = ShowDialog();
                if (result.Value && lvlEvaluaciones.SelectedItem != null)
                {
                    return (lvlEvaluaciones.SelectedItem as ListViewItem).Content as EvaluacionDesempeño;
                }
                return null;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }
        public EvaluacionDesempeño getEvaluacionByIdPersonal(int idPersonal)
        {
            try
            {
                getAllEvaluacionByIdPersonal(idPersonal);
                bool? result = ShowDialog();
                if (result.Value && lvlEvaluaciones.SelectedItem != null)
                {
                    return (lvlEvaluaciones.SelectedItem as ListViewItem).Content as EvaluacionDesempeño;
                }
                return null;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }
        public EvaluacionDesempeño getAllEvaluacion()
        {
            try
            {
                getAllEvaluaciones();
                bool? result = ShowDialog();
                if (result.Value && lvlEvaluaciones.SelectedItem != null)
                {
                    return (lvlEvaluaciones.SelectedItem as ListViewItem).Content as EvaluacionDesempeño;
                }
                return null;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }
        void getAllEvaluacionDepto(int idDepto)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new EvaluacionDesempeñoSvc().getEvaluacionesByDepartamento(idDepto);
                if (resp.typeResult == ResultTypes.success)
                {
                    llenarLista(resp.result as List<EvaluacionDesempeño>);
                }
                else if(resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        void getAllEvaluacionByIdPersonal(int idPersonal)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new EvaluacionDesempeñoSvc().getEvaluacionesByIdPersonal(idPersonal);
                if (resp.typeResult == ResultTypes.success)
                {
                    llenarLista(resp.result as List<EvaluacionDesempeño>);
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        void getAllEvaluaciones()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new EvaluacionDesempeñoSvc().getAlltEvaluaciones();
                if (resp.typeResult == ResultTypes.success)
                {
                    llenarLista(resp.result as List<EvaluacionDesempeño>);
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void llenarLista(List<EvaluacionDesempeño> listaEvaluaciones)
        {
            try
            {
                lvlEvaluaciones.ItemsSource = null;
                List<ListViewItem> list = new List<ListViewItem>();
                foreach (EvaluacionDesempeño evaluacion in listaEvaluaciones)
                {
                    ListViewItem item = new ListViewItem
                    {
                        Content = evaluacion
                    };
                    item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                    list.Add(item);
                }
                this.lvlEvaluaciones.ItemsSource = list;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlEvaluaciones.ItemsSource);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private bool UserFilter(object item) =>
           (string.IsNullOrEmpty(this.txtBuscador.Text) ||
           ((((EvaluacionDesempeño)(item as ListViewItem).Content).personalEvaluada.nombre.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(this.lvlEvaluaciones.ItemsSource).Refresh();
        }
    }
}
