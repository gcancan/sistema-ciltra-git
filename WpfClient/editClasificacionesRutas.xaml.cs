﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using Core.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editClasificacionesRutas.xaml
    /// </summary>
    public partial class editClasificacionesRutas : ViewBase
    {
        public editClasificacionesRutas()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            nuevo();

            cargarClientes(mainWindow.empresa.clave);
        }

        private void cargarClientes(int clave)
        {
            OperationResult resp = new ClienteSvc().getClientesALLorById(clave, 0, true);
            if (resp.typeResult == ResultTypes.success)
            {
                cbxCliente.ItemsSource = resp.result as List<Cliente>;
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);       
            
        }

        public override OperationResult guardar()
        {

            var val = validar();
            if (val.typeResult != ResultTypes.success)
            {
                return val;
            }
            var clasificacion = mapForm();
            if (clasificacion == null)
            {
                return new OperationResult { valor = 1, mensaje = "OCURRIO UN ERROR AL LEER LOS DATOS DE LA PANTALLA." };
            }

            var resp = new ClasificacionRutasSvc().saveClasificacionRutas(ref clasificacion);
            if (resp.typeResult == ResultTypes.success)
            {
                mapForm(clasificacion);
            }
            return resp;
        }

        public override OperationResult eliminar()
        {
            var clasificacion = mapForm();
            clasificacion.estatus = false;
            var resp = new ClasificacionRutasSvc().saveClasificacionRutas(ref clasificacion);
            if (resp.typeResult == ResultTypes.success)
            {
                mapForm(null);
                buscarClasificaciones(clasificacion.cliente);
            }
            return resp;
        }

        public ClasificacionRutas mapForm()
        {
            try
            {
                return new ClasificacionRutas
                {
                    idClasificacion = txtID.Tag == null ? 0 : ((ClasificacionRutas)txtID.Tag).idClasificacion,
                    cliente = (Cliente)cbxCliente.SelectedItem,
                    clasificacion =  txtClave.Text[0],
                    kmMenor = ctrKmMenor.valor,
                    kmMayor = ctrKmMayor.valor,
                    estatus = true                    
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void cbxCliente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxCliente.SelectedItem != null)
            {
                Cliente cliente = (Cliente)cbxCliente.SelectedItem;
                buscarClasificaciones(cliente);                
            }
            else
            {
                llenarListaClasificaciones(new List<ClasificacionRutas>());
            }
            mapForm(null);
        }

        void buscarClasificaciones(Cliente cliente)
        {
            var res = new ClasificacionRutasSvc().getClasificacionRutas(cliente.clave);
            if (res.typeResult == ResultTypes.success)
            {
                llenarListaClasificaciones(res.result as List<ClasificacionRutas>);
            }
            else
            {
                ImprimirMensaje.imprimir(res);
                llenarListaClasificaciones(new List<ClasificacionRutas>());
            }
        }

        private void llenarListaClasificaciones(List<ClasificacionRutas> list)
        {
            lvlClasificaciones.Items.Clear();
            foreach (ClasificacionRutas item in list)
            {
                addLista(item);
            }
        }

        private void addLista(ClasificacionRutas clasificacion)
        {
            ListViewItem lvl = new ListViewItem();
            lvl.Content = clasificacion;
            lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
            lvlClasificaciones.Items.Add(lvl);
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListViewItem lvl = (ListViewItem)sender;
            ClasificacionRutas clasif = lvl.Content as ClasificacionRutas;
            mapForm(clasif);
            
        }

        private void mapForm(ClasificacionRutas clasif)
        {
            if (clasif != null)
            {
                //cbxCliente.SelectedItem = null;
                txtID.Text = clasif.idClasificacion.ToString();
                txtID.Tag = clasif;
                txtClave.Text = clasif.clasificacion.ToString();
                ctrKmMenor.valor = clasif.kmMenor;
                ctrKmMayor.valor = clasif.kmMayor;
                gBoxDatos.IsEnabled = false;

                
                int i = 0;
                foreach (Cliente item in cbxCliente.Items)
                {
                    if (item.clave == clasif.cliente.clave)
                    {
                        cbxCliente.SelectedIndex = i;
                        break;
                    }
                    i++;
                }

                buscarClasificaciones(clasif.cliente);

                mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.editar | ToolbarCommands.eliminar | ToolbarCommands.cerrar);
            }
            else
            {
                txtID.Clear();
                txtID.Tag = null;
                txtClave.Clear();
                ctrKmMenor.valor = 0;
                ctrKmMayor.valor = 0;
                gBoxDatos.IsEnabled = true;
                mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            }
        }

        public override void editar()
        {
            base.editar();
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            gBoxDatos.IsEnabled = true;
        }

        OperationResult validar()
        {
            try
            {
                OperationResult resp = new OperationResult() { valor = 0, mensaje = "PARA CONTINUAR SE REQUIERE:" + Environment.NewLine };

                if (cbxCliente.SelectedItem == null)
                {
                    resp.valor = 3;
                    resp.mensaje="  *- Elegir un Cliente." + Environment.NewLine;
                }
                if (string.IsNullOrEmpty(txtClave.Text.Trim()))
                {
                    resp.valor = 3;
                    resp.mensaje = "  *- Proporcionar la Descripción." + Environment.NewLine;
                }

                if (ctrKmMayor.valor <= 0)
                {
                    resp.valor = 3;
                    resp.mensaje = "  *- El KM MAYOR no puede ser CERO." + Environment.NewLine;
                }

                if (ctrKmMayor.valor <= ctrKmMenor.valor)
                {
                    resp.valor = 3;
                    resp.mensaje = "  *- El KM MAYOR no puede ser igual o menor al KM MENOR." + Environment.NewLine;
                }

                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
