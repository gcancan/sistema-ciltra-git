﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for controlCondicionLlantas.xaml
    /// </summary>
    public partial class controlCondicionLlantas : UserControl
    {
        public controlCondicionLlantas()
        {
            InitializeComponent();
        }
        public CondicionLlanta condicionSelect
        {
            get
            {
                if (cbxCondicion.SelectedItem != null)
                {
                    return (CondicionLlanta)cbxCondicion.SelectedItem;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                CondicionLlanta condicion = value;
                if (condicion == null)
                {
                    cbxCondicion.SelectedItem = null;
                }
                else
                {                    
                    int i = 0;
                    foreach (var item in (List<CondicionLlanta>)cbxCondicion.ItemsSource)
                    {
                        if (item.idCondicion == condicion.idCondicion)
                        {
                            cbxCondicion.SelectedIndex = i;
                            break;
                        }
                        i++;
                    }
                }                
            }
        }
        private void cbxCondicion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        public void cargarControl()
        {
            try
            {
                OperationResult resp = new CondicionSvc().getCondicionAllOrById();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxCondicion.ItemsSource = (List<CondicionLlanta>)resp.result;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    this.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                this.IsEnabled = false;
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
