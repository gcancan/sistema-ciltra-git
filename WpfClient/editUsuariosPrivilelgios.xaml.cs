﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Interfaces;
using CoreFletera.Models;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for editUsuariosPrivilelgios.xaml
    /// </summary>
    public partial class editUsuariosPrivilelgios : ViewBase
    {
        public editUsuariosPrivilelgios()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            OperationResult resp = new EmpresaSvc().getEmpresasALLorById();
            if (resp.typeResult == ResultTypes.success)
            {
                cbxEmpresa.ItemsSource = (List<Empresa>)resp.result;
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
            }
            int i = 0;
            foreach (Empresa item in cbxEmpresa.Items)
            {
                if (item.clave == mainWindow.empresa.clave)
                {
                    cbxEmpresa.SelectedIndex = i;
                    break;
                }
                i++;
            }
            //if (!mapClientes())
            //{
            //    return;
            //}
            //if (!mapPersonal())
            //{
            //    return;
            //}
            ctrZonaOperativa.cargarControl(mainWindow.usuario.zonaOperativa, mainWindow.usuario.idUsuario);
            nuevo();
        }

        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);
        }

        private bool mapClientes()
        {
            OperationResult resp = new ClienteSvc().getClientesALLorById(mainWindow.inicio._usuario.personal.empresa.clave);
            if (validarOperationResult(resp))
            {
                int i = 0;
                int index = 0;
                foreach (var item in (List<Cliente>)resp.result)
                {
                    cbxClientes.Items.Add(item);
                    if (item.clave == mainWindow.inicio._usuario.cliente.clave)
                    {
                        index = i;
                        continue;
                    }
                    i++;
                }
                cbxClientes.SelectedIndex = index;
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool mapPersonal()
        {
            OperationResult resp = new OperadorSvc().getPersonalALLorById();
            if (validarOperationResult(resp))
            {
                foreach (var item in (List<Personal>)resp.result)
                {
                    //cbxPersonal.Items.Add(item);                    
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool validarOperationResult(OperationResult resp)
        {
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    return true;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return false;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                case ResultTypes.recordNotFound:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                default:
                    return false;
            }
        }

        private void addPrivilegio_Click(object sender, RoutedEventArgs e)
        {
            List<Privilegio> listaPrivilegiosUsuarios = new List<Privilegio>();
            if (txtNomUsuario.Tag != null)
            {
                listaPrivilegiosUsuarios = (txtNomUsuario.Tag as Usuario).listPrivilegios;
            }
            selectPrivilegios buscar = new selectPrivilegios(listaPrivilegiosUsuarios);
            var resp = buscar.findPrivilegios();
            if (resp != null)
            {
                addLista(resp);
            }
        }

        private void addLista(List<Privilegio> resp)
        {
            foreach (var item in resp)
            {
                if (!existeEnLaLista(item))
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = item;
                    lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                    lvlPrivilegios.Items.Add(lvl);
                }
                else
                {
                    //MessageBox.Show(("El privilegio " + item.clave + " ya se encuentra en la lista"), "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }


        }

        private bool existeEnLaLista(Privilegio resp)
        {
            if (lvlPrivilegios.Items.Count <= 0)
            {
                return false;
            }
            else
            {
                foreach (ListViewItem item in lvlPrivilegios.Items)
                {
                    if (((Privilegio)item.Content).idPrivilegio == resp.idPrivilegio)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                var validar = validarForm();
                if (validar.typeResult == ResultTypes.success)
                {
                    Usuario usuario = mapForm();
                    if (usuario != null)
                    {
                        OperationResult resp = new UsuarioSvc().saveUsuario(ref usuario);
                        if (validarOperationResult(resp))
                        {
                            if (usuario.activo)
                            {
                                mapForm(usuario);
                                base.guardar();
                            }
                            else
                            {
                                mainWindow.habilitar = true;
                                nuevo();
                            }
                        }
                        return resp;
                    }
                    else
                    {
                        return new OperationResult { valor = 1, mensaje = "Ocurrio un error al construir el objeto" };
                    }
                }
                else
                {
                    return validar;
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            { Cursor = Cursors.Arrow; }
        }
        public override OperationResult eliminar()
        {
            try
            {
                Cursor = Cursors.Wait;
                var validar = validarForm();
                if (validar.typeResult == ResultTypes.success)
                {
                    Usuario usuario = mapForm();
                    if (usuario != null)
                    {
                        usuario.activo = false;
                        OperationResult resp = new UsuarioSvc().saveUsuario(ref usuario);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            nuevo();
                            base.eliminar();
                        }
                        return resp;
                    }
                    else
                    {
                        return new OperationResult { valor = 1, mensaje = "Ocurrio un error al construir el objeto" };
                    }
                }
                else
                {
                    return validar;
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            { Cursor = Cursors.Arrow; }
        }
        private void mapForm(Usuario usuario)
        {
            if (usuario != null)
            {
                txtNomUsuario.Text = usuario.nombreUsuario;
                txtContrasena.Password = usuario.contrasena;
                txtConfirmaContrasena.Password = usuario.contrasena;
                mapList(usuario.listPrivilegios);
                txtNomUsuario.Tag = usuario;
                chcActivo.IsChecked = usuario.activo;
                int i = 0;
                cbxEmpresa.SelectedItem = cbxEmpresa.Items.Cast<Empresa>().ToList().Find(s => s.clave == usuario.empresa.clave);
                //foreach (Empresa item in cbxEmpresa.Items)
                //{
                //    if (item.clave == usuario.personal.empresa.clave)
                //    {
                //        cbxEmpresa.SelectedIndex = i;
                //        break;
                //    }
                //    else
                //    {
                //        i++;
                //    }
                //}
                if (cbxClientes.Items.Count > 0)
                {
                    if (usuario.cliente != null)
                    {
                        cbxClientes.SelectedItem = cbxClientes.Items.Cast<Cliente>().ToList().Find(s => s.clave == usuario.cliente.clave);
                    }
                    else
                    {
                        cbxClientes.SelectedItem = null;
                    }
                }
                ctrZonaOperativa.zonaOperativa = usuario.zonaOperativa;
                //foreach (Cliente item in cbxClientes.Items)
                //{
                //    if (item.clave == usuario.cliente.clave)
                //    {
                //        cbxClientes.SelectedIndex = i;
                //        break;
                //    }
                //    else
                //    {
                //        i++;
                //    }
                //}
                i = 0;
                mapChofer(usuario.personal);
            }
            else
            {
                //cbxClientes.SelectedItem = null;
                //cbxPersonal.SelectedItem = null;
                txtNomUsuario.Clear();
                txtContrasena.Clear();
                txtConfirmaContrasena.Clear();
                lvlPrivilegios.Items.Clear();
                txtNomUsuario.Tag = null;
                mapChofer(null);
                chcActivo.IsChecked = true;
            }
        }

        private void mapList(List<Privilegio> listPrivilegios)
        {
            lvlPrivilegios.Items.Clear();
            foreach (Privilegio item in listPrivilegios)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                lvl.Content = item;
                lvlPrivilegios.Items.Add(lvl);
            }
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (txtNomUsuario.Tag == null)
            {
                lvlPrivilegios.Items.Remove(lvlPrivilegios.SelectedItem);
            }
            else
            {
                var resp = new PrivilegioSvc().quitarPrivilegio(
                    ((Usuario)txtNomUsuario.Tag).idUsuario,
                    ((Privilegio)((ListViewItem)lvlPrivilegios.SelectedItem).Content).idPrivilegio);
                if (resp.typeResult != ResultTypes.success)
                {
                    MessageBox.Show(resp.mensaje);
                }
                else
                {
                    {
                        lvlPrivilegios.Items.Remove(lvlPrivilegios.SelectedItem);
                    }
                }
            }
        }

        private Usuario mapForm()
        {
            try
            {
                Usuario usuario = new Usuario();
                usuario.empresa = cbxEmpresa.SelectedItem as Empresa;
                usuario.zonaOperativa = ctrZonaOperativa.zonaOperativa;
                usuario.idUsuario = txtNomUsuario.Tag == null ? 0 : ((Usuario)txtNomUsuario.Tag).idUsuario;
                usuario.nombreUsuario = txtNomUsuario.Text.Trim();
                usuario.contrasena = txtContrasena.Password;
                usuario.cliente = cbxClientes.SelectedItem == null ? null : (Cliente)cbxClientes.SelectedItem;
                usuario.personal = ((Personal)cbxPersonal.Tag);
                usuario.activo = chcActivo.IsChecked.Value;

                usuario.listPrivilegios = new List<Privilegio>();
                foreach (ListViewItem item in lvlPrivilegios.Items)
                {
                    usuario.listPrivilegios.Add((Privilegio)item.Content);
                }
                return usuario;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private OperationResult validarForm()
        {
            string cadena = "Para proceder se necesita corregir los siguientes errores:" + Environment.NewLine;
            bool error = false;
            //if (cbxClientes.SelectedItem == null)
            //{
            //    cadena += "* Seleccionar un Cliente";
            //    error = true;
            //}
            if (ctrZonaOperativa.zonaOperativa == null)
            {
                cadena += "* Seleccionar una zona operativa";
                error = true;
            }

            if (cbxPersonal.Tag == null)
            {
                cadena += "* Seleccionar un Personal";
                error = true;
            }

            if (string.IsNullOrEmpty(txtNomUsuario.Text.Trim()))
            {
                cadena += "* Escribir el nombre del usuario";
                error = true;
            }

            if (string.IsNullOrEmpty(txtContrasena.Password.Trim()))
            {
                cadena += "* Escribir la contraseña del usuario";
                error = true;
                if (txtContrasena.Password.Trim() != txtConfirmaContrasena.Password.Trim())
                {
                    cadena += "* Las contraseñas no coinciden";
                    error = true;
                }
            }
            if (error)
            {
                return new OperationResult { valor = 2, mensaje = cadena };
            }
            else
            {
                return new OperationResult { valor = 0 };
            }
        }

        public override void buscar()
        {
            var buscar = new selectUsuario();
            var usuario = buscar.findUsuarios();
            if (usuario != null)
            {
                mapForm(usuario);
                base.buscar();
                if (!usuario.activo)
                {
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.editar | ToolbarCommands.cerrar);
                }
            }

        }

        private void cbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (cbxEmpresa.SelectedItem != null)
                {
                    cbxClientes.ItemsSource = null;
                    OperationResult resp = new ClienteSvc().getClientesALLorById(((Empresa)cbxEmpresa.SelectedItem).clave, 0, true);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        cbxClientes.ItemsSource = (List<Cliente>)resp.result;
                    }
                    else if (resp.typeResult != ResultTypes.recordNotFound)
                    {
                        ImprimirMensaje.imprimir(resp);

                    }

                    //mapChofer(null);
                }
                else
                {
                    cbxClientes.ItemsSource = null;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally { Cursor = Cursors.Arrow; }
        }

        private void cbxPersonal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (cbxEmpresa.SelectedItem == null)
                {
                    return;
                }
                string clave = cbxPersonal.Text.Trim();
                //completarClave(ref clave);
                //if (string.IsNullOrEmpty(clave))
                //{
                //    return;
                //}
                List<Personal> listPersonal = buscarPersonal(clave);
                if (listPersonal.Count == 1)
                {
                    mapChofer(listPersonal[0]);
                }
                else /*if (listPersonal.Count == 0)*/
                {
                    selectPersonal buscardor = new selectPersonal(clave, ((Empresa)cbxEmpresa.SelectedItem).clave);
                    var per = buscardor.buscarAllPersonal();
                    mapChofer(per);
                }
                //else
                //{
                //    selectPersonal buscardor = new selectPersonal(clave, ((Empresa)cbxEmpresa.SelectedItem).clave);
                //    var per = buscardor.buscarPersonal();
                //    mapChofer(per);
                //}
            }
        }

        private void mapChofer(Personal personal)
        {
            if (personal != null)
            {
                cbxPersonal.Text = personal.nombre;
                cbxPersonal.Tag = personal;
                cbxPersonal.IsEnabled = false;
                //txtRemision.Focus();
                txtNomUsuario.Focus();
            }
            else
            {
                cbxPersonal.Text = "";
                cbxPersonal.Tag = null;
                cbxPersonal.IsEnabled = true;
            }
        }

        private List<Personal> buscarPersonal(string nombre)
        {
            OperationResult resp = new OperadorSvc().getPersonalByNombre(nombre, ((Empresa)cbxEmpresa.SelectedItem).clave);
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    return (List<Personal>)resp.result;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return null;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return null;
                case ResultTypes.recordNotFound:
                    return new List<Personal>();

                default:
                    return null;
            }
        }

        private void BtnCambiarPersonal_Click(object sender, RoutedEventArgs e)
        {
            mapChofer(null);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (rbnUsuarios.IsChecked.Value)
                {
                    var resp = new UsuarioSvc().getReporteUsuarios();
                    if (resp.typeResult == ResultTypes.success)
                    {
                        new ReportView().exportarListaUsuarios(resp.result as List<ReporteUsuarios>);
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
                else if (rbnPrivilegios.IsChecked.Value)
                {
                    var resp = new UsuarioSvc().getReportePrivilegios();
                    if (resp.typeResult == ResultTypes.success)
                    {
                        new ReportView().exportarListaPrivilegios(resp.result as List<ReportePrivilegios>);
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
                else if (rbnPrivilegiosUsuarios.IsChecked.Value)
                {
                    var resp = new UsuarioSvc().getReporteUsuariosPrivilegios();
                    if (resp.typeResult == ResultTypes.success)
                    {
                        new ReportView().exportarListaUsuarios(resp.result as List<ReporteUsuarios>, true);
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void BtnQuitarCliente_Click(object sender, RoutedEventArgs e)
        {
            cbxClientes.SelectedItem = null;
        }
    }
}
