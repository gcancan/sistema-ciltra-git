﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using Core.Interfaces;
using CoreINTELISIS.INTERFACES;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para INTELISIS_catDepartamentos.xaml
    /// </summary>
    public partial class INTELISIS_catDepartamentos : UserControl
    {
        SincronizadorINTELISIS pantalla = null;
        public INTELISIS_catDepartamentos(SincronizadorINTELISIS pantalla)
        {
            InitializeComponent();
            this.pantalla = pantalla;
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            pantalla.stpContenedor.Children.Clear();
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lvlDpto.ItemsSource = null;
                lblContador.Content = 0;
                lblActualizados.Content = 0;
                lblNuevos.Content = 0;
                Cursor = Cursors.Wait;
                var resp = new DepartamentoINTELISISSvc().getAllDepartamentos();
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Departamento> listaDpto = resp.result as List<Departamento>;
                    lvlDpto.ItemsSource = listaDpto;
                    lblContador.Content = listaDpto.Count;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                lblActualizados.Content = 0;
                lblNuevos.Content = 0;
                if (lvlDpto.ItemsSource != null)
                {
                    List<Departamento> listaDepto = lvlDpto.ItemsSource as List<Departamento>;
                    var resp = new DepartamentoSvc().sincronizarDepartamentos(listaDepto);
                    lblActualizados.Content = resp.noActualizados;
                    lblNuevos.Content = resp.noNuevos;
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
