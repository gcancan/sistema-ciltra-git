﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectCargasExtra.xaml
    /// </summary>
    public partial class selectCargasExtra : Window
    {
        public selectCargasExtra()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
        private string idUnidad = string.Empty;
        List<int> listaId = new List<int>();
        public CargaCombustibleExtra getCargaCombustibleExtraLibreByUnidad(string idUnidad, List<int> listaId)
        {
            try
            {
                this.listaId = listaId;
                this.idUnidad = idUnidad;
                buscarCargasExtraLibreByUnidad();
                bool? result = ShowDialog();
                if (result.Value && lvlCargasExtra.SelectedItem != null)
                {
                    return (lvlCargasExtra.SelectedItem as ListViewItem).Content as CargaCombustibleExtra;
                }
                return null;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }
        private void buscarCargasExtraLibreByUnidad()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new CargaCombustibleSvc().getCargaCombustibleExtraLibresByUnidad(this.idUnidad);
                if (resp.typeResult == ResultTypes.success)
                {
                    llenarLista((resp.result as List<CargaCombustibleExtra>).Where(s=> !listaId.Contains(s.idCargaCombustibleExtra)).ToList());
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void llenarLista(List<CargaCombustibleExtra> listaCargasExtra)
        {
            try
            {
                lvlCargasExtra.ItemsSource = null;
                List<ListViewItem> list = new List<ListViewItem>();
                foreach (CargaCombustibleExtra col in listaCargasExtra)
                {
                    ListViewItem item = new ListViewItem
                    {
                        Content = col
                    };
                    item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                    list.Add(item);
                }
                this.lvlCargasExtra.ItemsSource = list;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlCargasExtra.ItemsSource);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtBuscador.Text) ||
            ((((CargaCombustibleExtra)(item as ListViewItem).Content).idCargaCombustibleExtra.ToString().IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(this.lvlCargasExtra.ItemsSource).Refresh();
        }
    }
}
