﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para ReportePreciosPorDespachos.xaml
    /// </summary>
    public partial class ReportePreciosPorDespachos : ViewBase
    {
        public ReportePreciosPorDespachos()
        {
            InitializeComponent();
        }
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            this.buscarDespachos();
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += new SelectionChangedEventHandler(this.CbxZonaOpetativa_SelectionChanged);
            this.ctrZonaOperativa.cargarControl(base.mainWindow.usuario.zonaOperativa, base.mainWindow.usuario.idUsuario);
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
        }
        List<Despacho> listaGeneralDespachos = new List<Despacho>();
        List<Despacho> listaGeneralNewDespachos = new List<Despacho>();
        private void buscarDespachos()
        {
            try
            {
                lblSumaSuministros.Content = "0.00";
                lblSumaLitros.Content = "0.00";
                lblSumaFinales.Content = "0.00";
                listaGeneralDespachos = new List<Despacho>();
                base.Cursor = Cursors.Wait;
                this.lvlDespachos.ItemsSource = null;
                this.lvlResumen.ItemsSource = null;
                this.lblTotal.Content = "0.0000";
                lvlSuministros.ItemsSource = null;
                lvlResulFinal.ItemsSource = null;
                List<NivelInicial> list = new List<NivelInicial>();
                OperationResult resp = new SuministroCombustibleSvc().getNivelesIniciales((this.txtNombreTanque.Tag as TanqueCombustible).idTanqueCombustible, this.ctrFechas.fechaInicial);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<NivelInicial> lista = resp.result as List<NivelInicial>;
                    foreach (var item in lista)
                    {
                        list.Add(item);
                    }

                    this.ctrExistIni.valor = list[0].cantidad;
                    this.ctrPrecioIni.valor = list[0].precio;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    return;
                }
                List<NivelInicial> listaIniciales = new List<NivelInicial>();
                OperationResult result2 = new SuministroCombustibleSvc().getDetalleSuministroCombustible(this.ctrFechas.fechaInicial, this.ctrFechas.fechaFinal, (this.txtNombreTanque.Tag as TanqueCombustible).idTanqueCombustible, 0);
                this.lvlSuministros.ItemsSource = null;
                if (result2.typeResult == ResultTypes.success)
                {
                    List<ReporteSuministrosCombustible> list3 = result2.result as List<ReporteSuministrosCombustible>;
                    foreach (ReporteSuministrosCombustible combustible in list3)
                    {
                        NivelInicial item = new NivelInicial
                        {
                            fecha = combustible.fecha,
                            cantidad = combustible.ltsTotales,
                            precio = combustible.precio,
                            factura = combustible.factura
                        };
                        list.Add(item);
                    }
                    listaIniciales = (from g in list
                                      orderby g.fecha
                                      group g by new { g.fecha, g.precio, g.factura } into gr
                                      select new NivelInicial
                                      {
                                          fecha = (from s in gr select s.fecha).First<DateTime>(),
                                          cantidad = gr.Sum<NivelInicial>(s => s.cantidad),
                                          precio = (from s in gr select s.precio).First<decimal>(),
                                          factura = (from s in gr select s.factura).First<string>()
                                      }).ToList<NivelInicial>();
                    List<ListViewItem> listalvl = new List<ListViewItem>();
                    foreach (var item in listaIniciales)
                    {
                        ListViewItem lvl = new ListViewItem();
                        lvl.Content = item;
                        lvl.Selected += Lvl_Selected;
                        ContextMenu menus = new ContextMenu();
                        
                        MenuItem menuItem = new MenuItem { Header = "EXPORTAR.", Tag = item };
                        menus.Items.Add(menuItem);
                        lvl.ContextMenu = menus;
                        menuItem.Click += MenuItem_Click;
                        listalvl.Add(lvl);
                    }
                    this.lvlSuministros.ItemsSource = listalvl;
                    lblSumaSuministros.Content = listaIniciales.Sum(s => s.cantidad).ToString("N4");
                }
                else if (result2.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(result2);
                }
                //int contadorDespachos = 0;
                OperationResult result3 = new CargaCombustibleSvc().getDespachosCombustibleByFiltros(this.ctrFechas.fechaInicial, this.ctrFechas.fechaFinal, (this.txtNombreTanque.Tag as TanqueCombustible).idTanqueCombustible, "%");
                if (result3.typeResult == ResultTypes.success)
                {
                    List<Despacho> listaDespachos = result3.result as List<Despacho>;
                    int i = 0;
                    //decimal sumaDespachos = 0M;
                    List<Despacho> listaNewDespachos = new List<Despacho>();
                    int indiceDespachos = 0;
                    foreach (Despacho despacho in listaDespachos)
                    {
                        listaIniciales[i].cantidadFinal -= despacho.despachado;
                        if (listaIniciales[i].cantidadFinal > 0)
                        {
                            despacho.precio = listaIniciales[i].precio;
                            despacho.factura = listaIniciales[i].factura;
                            listaNewDespachos.Add(despacho);
                        }
                        else
                        {
                            decimal diferencia = Math.Abs(listaIniciales[i].cantidadFinal);
                            decimal _despacho = despacho.despachado - diferencia;

                            decimal importe1 = diferencia * listaIniciales[i].precio;
                            decimal importe2 = _despacho * listaIniciales[i + 1].precio;



                            listaNewDespachos.Add(new Despacho(despacho) { precio = listaIniciales[i].precio, despachado = _despacho, factura = listaIniciales[i].factura });
                            listaNewDespachos.Add(new Despacho(despacho) { precio = listaIniciales[i + 1].precio, despachado = diferencia, factura = listaIniciales[i + 1].factura });

                            despacho.precio = (importe1 + importe2) / despacho.despachado;
                            despacho.factura = string.Format("{0},{1}", listaIniciales[i].factura, listaIniciales[i + 1].factura);

                            i++;
                            if (i > (listaIniciales.Count))
                            {
                                i--;
                            }
                            listaIniciales[i].cantidadFinal -= diferencia;
                        }
                        indiceDespachos++;
                    }
                    listaGeneralDespachos = listaNewDespachos;
                    listaGeneralNewDespachos = listaDespachos;
                    this.lvlDespachos.ItemsSource = listaNewDespachos;
                    List<GroupDespachos> list5 = (from p in listaDespachos
                                                  group p by p.cliente into g
                                                  select new GroupDespachos
                                                  {
                                                      cliente = (from s in g select s.cliente).First<string>(),
                                                      lts = g.Sum<Despacho>(s => s.despachado)
                                                  }).ToList<GroupDespachos>();
                    this.lvlResumen.ItemsSource = list5;
                    this.lblTotal.Content = listaDespachos.Sum<Despacho>(s => s.despachado).ToString("N4");

                    lvlResulFinal.ItemsSource = listaIniciales.FindAll(s => s.cantidadFinal > 0);
                    lblSumaFinales.Content = listaIniciales.FindAll(s => s.cantidadFinal > 0).Sum(s=> s.cantidadFinal).ToString("N4");
                    lblSumaLitros.Content = listaGeneralDespachos.Sum(s => s.despachado).ToString("N4");
                }
                else if (result3.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(result3);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    MenuItem menuItem = sender as MenuItem;
                    NivelInicial nivelInicial = menuItem.Tag as NivelInicial;
                    List<Despacho> listaDespacho = listaGeneralDespachos.FindAll(s => s.factura == nivelInicial.factura);

                    List<NivelInicial> listaNiveles = new List<NivelInicial> { nivelInicial };
                    new ReportView().exportarDespachosLitos(listaDespacho, listaNiveles);

                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
            
        }

        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            limpiarListas();
            if (ctrZonaOperativa.zonaOperativa != null)
            {
                OperationResult result = new TanqueCombustibleSvc().getTanqueByZonaOperativa(this.ctrZonaOperativa.zonaOperativa);
                if (result.typeResult == ResultTypes.success)
                {
                    TanqueCombustible combustible = result.result as TanqueCombustible;
                    this.txtNombreTanque.Tag = combustible;
                    this.txtNombreTanque.Text = combustible.nombre;
                }
                else
                {
                    this.txtNombreTanque.Tag = null;
                    this.txtNombreTanque.Clear();
                }
            }
        }
        private void limpiarListas()
        {
            this.lvlDespachos.ItemsSource = null;
            this.lvlResumen.ItemsSource = null;
            this.lvlSuministros.ItemsSource = null;
        }
        private void Exportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.lvlDespachos.ItemsSource != null)
                {
                    base.Cursor = Cursors.Wait;
                    List<NivelInicial> listaNiveles = (this.lvlSuministros.ItemsSource as List<ListViewItem>).Select(s => s.Content as NivelInicial).ToList();
                    new ReportView().exportarDespachosLitos(listaGeneralDespachos, listaNiveles);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;

                DateTime fecha = ctrFechas.fechaFinal.AddDays(1);
                List<NivelInicial> lista = lvlResulFinal.ItemsSource as List<NivelInicial>;

                var resp = new SuministroCombustibleSvc().saveNivelesIniciales(fecha, lista, (txtNombreTanque.Tag as TanqueCombustible).idTanqueCombustible, listaGeneralNewDespachos);
                ImprimirMensaje.imprimir(resp);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void LvlSuministros_Selected(object sender, RoutedEventArgs e)
        {
            var ll = lvlSuministros.SelectedItems;
        }
        NivelInicial nivelSelect = null;
        private void LvlSuministros_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                lvlSuministros.SelectedItems.Clear();
                lvlDespachos.ItemsSource = listaGeneralDespachos;
                decimal sumaLitros = listaGeneralDespachos.Sum(s => s.despachado);
                lblSumaLitros.Content = sumaLitros.ToString("N4");
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }

        }
        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    ListViewItem lvl = sender as ListViewItem;
                    NivelInicial nivelInicial = lvl.Content as NivelInicial;
                    var listaDespacho  = listaGeneralDespachos.FindAll(s => s.factura.Contains(nivelInicial.factura));
                    lvlDespachos.ItemsSource = listaDespacho;
                    decimal sumaLitros = listaDespacho.Sum(s => s.despachado);
                    lblSumaLitros.Content = sumaLitros.ToString("N4");
                }
                else
                {
                    lvlDespachos.ItemsSource = listaGeneralDespachos;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void LvlSuministros_GotFocus(object sender, RoutedEventArgs e)
        {

        }
    }
}
