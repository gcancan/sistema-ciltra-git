﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para ControlBajarCartaPortes.xaml
    /// </summary>
    public partial class ControlBajarCartaPortes : Window
    {
        public ControlBajarCartaPortes()
        {
            InitializeComponent();
        }
        List<int> listaConsecutivos = new List<int>();
        Usuario usuario = null;
        string motivo = string.Empty;
        OperationResult respuesta = new OperationResult(ResultTypes.warning, "OCURRIO UN ERROR DURANTE EL PROCESO");

        public OperationResult darBajaRemisiones(List<int> listaConsecutivos, Usuario usuario)
        {
            try
            {
                this.listaConsecutivos = listaConsecutivos;
                this.usuario = usuario;
                this.motivo = motivo;

                ShowDialog();

                return respuesta;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        private void BtnAceptar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<string> listaStr = listaConsecutivos.Select(s => s.ToString()).ToList();
                var resp = new CartaPorteSvc().bajarRemisiones(listaStr, usuario.idUsuario, txtMotivo.Text.Trim());
                if (resp.typeResult == ResultTypes.success)
                {
                    respuesta = resp;
                    DialogResult = true;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir( new OperationResult(ex));
            }
        }
    }
}
