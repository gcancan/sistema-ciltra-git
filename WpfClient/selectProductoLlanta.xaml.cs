﻿using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectProductoLlanta.xaml
    /// </summary>
    public partial class selectProductoLlanta : Window
    {
        public selectProductoLlanta()
        {
            InitializeComponent();
        }
        private void getAllProductoLlantas()
        {
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult resp = new LlantaSvc().getAllLLantasProducto();
                if (resp.typeResult == ResultTypes.success)
                {
                    this.llenarLista(resp.result as List<LLantasProducto>);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    base.DialogResult = false;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        public LLantasProducto GetLLantasProducto()
        {
            try
            {
                this.getAllProductoLlantas();
                if (base.ShowDialog().Value && (this.lvlProductoLlantas.SelectedItem != null))
                {
                    return (LLantasProducto)((ListViewItem)this.lvlProductoLlantas.SelectedItem).Content;
                }
                return null;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return null;
            }
        }
        private void llenarLista(List<LLantasProducto> listaLLantasProductos)
        {
            List<ListViewItem> list = new List<ListViewItem>();
            foreach (LLantasProducto producto in listaLLantasProductos)
            {
                ListViewItem item = new ListViewItem
                {
                    Content = producto
                };
                item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                list.Add(item);
            }
            lvlProductoLlantas.ItemsSource = list;
            CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlProductoLlantas.ItemsSource);
            defaultView.Filter = this.UserFilter;
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            base.DialogResult = true;
        }
        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.lvlProductoLlantas.ItemsSource != null)
            {
                CollectionViewSource.GetDefaultView(this.lvlProductoLlantas.ItemsSource).Refresh();
            }
        }

        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtFind.Text) || ((((((((LLantasProducto)(item as ListViewItem).Content).descripcion.ToString().IndexOf(this.txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0) || (((LLantasProducto)(item as ListViewItem).Content).marca.descripcion.ToString().IndexOf(this.txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0)) || (((((LLantasProducto)(item as ListViewItem).Content).diseño == null) ? "" : ((LLantasProducto)(item as ListViewItem).Content).diseño.descripcion).IndexOf(this.txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0)) || (((((LLantasProducto)(item as ListViewItem).Content).diseño == null) ? "" : ((LLantasProducto)(item as ListViewItem).Content).diseño.profundidad.ToString()).IndexOf(this.txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0)) || (((((LLantasProducto)(item as ListViewItem).Content).diseño.medida == null) ? "" : ((LLantasProducto)(item as ListViewItem).Content).diseño.medida.medida).IndexOf(this.txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0)) || (((((LLantasProducto)(item as ListViewItem).Content).diseño.tipoLlanta == null) ? "" : ((LLantasProducto)(item as ListViewItem).Content).diseño.tipoLlanta.nombre).IndexOf(this.txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }
    }
}
