﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectRutasGlobal.xaml
    /// </summary>
    public partial class selectRutasGlobal : Window
    {
        public selectRutasGlobal()
        {
            this.parametro = "";
            InitializeComponent();
        }
        public selectRutasGlobal(string parametro)
        {
            this.parametro = "";
            this.InitializeComponent();
            this.parametro = parametro;
        }
        private string parametro;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.txtFind.Text = this.parametro;
            this.lvlDestinos.Focus();
        }
        public Zona buscarZona(List<Zona> listZonas)
        {
            this.mapListaZonas(listZonas);
            if ((base.ShowDialog().Value && (this.lvlDestinos != null)) && (this.lvlDestinos.SelectedItem != null))
            {
                return (Zona)((ListViewItem)this.lvlDestinos.SelectedItem).Content;
            }
            return null;
        }
        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            base.DialogResult = true;
        }

        private void lvlDestinos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                base.DialogResult = true;
            }
        }

        private void mapListaZonas(List<Zona> listZonas)
        {
            this.lvlDestinos.Items.Clear();
            List<ListViewItem> list = new List<ListViewItem>();
            foreach (Zona zona in listZonas)
            {
                ListViewItem item = new ListViewItem
                {
                    Content = zona
                };
                item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                list.Add(item);
            }
            this.lvlDestinos.ItemsSource = list;
            CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlDestinos.ItemsSource);
            defaultView.Filter = new Predicate<object>(this.UserFilter);
        }
        private void txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                this.lvlDestinos.Focus();
            }
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(this.lvlDestinos.ItemsSource).Refresh();
        }

        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtFind.Text) || 
            ((((Zona)(item as ListViewItem).Content).clave.ToString().IndexOf(this.txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0) || 
            (((Zona)(item as ListViewItem).Content).descripcion.IndexOf(this.txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0)));
    }
}
