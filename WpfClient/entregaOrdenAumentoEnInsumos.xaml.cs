﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using Toolkit = Xceed.Wpf.Toolkit;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for entregaOrdenAumentoEnInsumos.xaml
    /// </summary>
    public partial class entregaOrdenAumentoEnInsumos : ViewBase
    {
        public entregaOrdenAumentoEnInsumos()
        {
            InitializeComponent();
        }

        private void txtOrdenServicio_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrEmpty(txtOrdenServicio.Text.Trim()))
                {
                    int i = 0;
                    if (int.TryParse(txtOrdenServicio.Text.Trim(), out i))
                    {
                        buscarOrdenServicio(i);
                    }
                }
            }

        }

        private void buscarOrdenServicio(int idOrden)
        {
            try
            {
                var resp = new OperacionSvc().getOrdenServById(idOrden);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm((TipoOrdenServicio)resp.result);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void mapForm(TipoOrdenServicio result)
        {
            if (result != null)
            {
                txtOrdenServicio.Tag = result;
                txtOrdenServicio.Text = result.idOrdenSer.ToString();
                txtOrdenServicio.IsEnabled = false;
                txtTipoOrden.Text = result.TipoServicio;
                txtUnidadTransporte.Text = result.unidadTrans.clave + " " + result.unidadTrans.tipoUnidad.descripcion;
                txtEstatus.Text = getEstatus(result.estatus);
                var resp = new MasterOrdServSvc().getActividadesByOrdenSer(result.idOrdenSer);
                //txtPersonalEntrega.personal = orden.personaEntrega;
                if (resp.typeResult == ResultTypes.success)
                {
                    mapActividades((List<Actividad>)resp.result);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            else
            {
                txtOrdenServicio.Tag = result;
                txtOrdenServicio.Clear();
                txtOrdenServicio.IsEnabled = true;
                txtTipoOrden.Clear();
                txtUnidadTransporte.Clear();
                txtEstatus.Clear();
                lvlActividades.Items.Clear();
                scrollContenedor.Content = null;
            }
        }
        private string getEstatus(string estatus)
        {
            if (string.IsNullOrEmpty(estatus))
            {
                return string.Empty;
            }
            else if (estatus.ToUpper() == "PRE")
            {
                return "SIN ASIGNAR";
            }
            else if (estatus.ToUpper() == "ASIG")
            {
                return "ASIGNADO";
            }
            else if (estatus.ToUpper() == "DIAG")
            {
                return "EN PROCESO";
            }
            else if (estatus.ToUpper() == "TER")
            {
                return "TERMINADO";
            }
            else if (estatus.ToUpper() == "ENT")
            {
                return "ENTREGADO";
            }
            else if (estatus.ToUpper() == "CAN")
            {
                return "CANCELADO";
            }
            else
            {
                return string.Empty;
            }
        }
        private void mapActividades(List<Actividad> result)
        {
            lvlActividades.Items.Clear();
            foreach (var item in result)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick1;
                lvl.Selected += Lvl_Selected1;
                lvlActividades.Items.Add(lvl);
            }
        }
        private void Lvl_Selected1(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                eventoActividades((Actividad)((ListViewItem)sender).Content);
            }
        }
        private void Lvl_MouseDoubleClick1(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (lvlActividades.SelectedItem != null)
                {
                    Actividad actividad = ((Actividad)((ListViewItem)lvlActividades.SelectedItem).Content);
                    eventoActividades(actividad);
                }

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        void eventoActividades(Actividad actividad)
        {
            StackPanel stpMayor = new StackPanel { Orientation = Orientation.Vertical };
            List<ProductosActividad> listPro = actividad.listProductos;
            foreach (var insumo in listPro)
            {
                StackPanel stp = new StackPanel() { VerticalAlignment = VerticalAlignment.Top, HorizontalAlignment = HorizontalAlignment.Left };
                stp.Orientation = Orientation.Horizontal;
                stp.Children.Add(new Label() { Content = insumo.nombre, Width = 220, Height = 24 });
                stp.Children.Add(new Label() { Content = "AGINADOS: " + insumo.cantidad.ToString(), Width = 120, Height = 24 });
                Toolkit.IntegerUpDown control = new Toolkit.IntegerUpDown() { Width = 50, Height = 25, Minimum = 0, Value = 0 };
                stp.Children.Add(control);
                //stp.Children.Add(new Label() { Content = insumo.um.Trim() + string.Format(" EXTRA(MAX{0})", (insumo.cantMax - insumo.cantidad).ToString()), Width = 200, Height = 24 });
                stp.Tag = insumo;
                stpMayor.Children.Add(stp);
            }
            scrollContenedor.Content = stpMayor;
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            txtPersonal.loaded(eTipoBusquedaPersonal.TODOS, null);
            nuevo();
        }
        public override void nuevo()
        {
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
            mapForm(null);
            txtPersonal.limpiar();
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            var lista = getListaInsumos();
            if (lista != null && lista.Count > 0)
            {
                if (lvlActividades.SelectedItem == null)
                {
                    return;
                }
                Actividad act = ((Actividad)((ListViewItem)lvlActividades.SelectedItem).Content);
                if (act.estatus.ToUpper() != "ASIG")
                {
                    ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "SOLO LAS ACTIVIDADES EN PROCESO O ASIGNADAS SE LES PODRA AGREGAR INSUMOS" });
                    return;
                }
                OperationResult resp = new OperacionSvc().agregarInsumosExtra(lista, Convert.ToInt32(txtOrdenServicio.Text));
                if (resp.typeResult == ResultTypes.success)
                {
                    int id = Convert.ToInt32(txtOrdenServicio.Text);
                    //var report = new ReportView().crearSolicitudSalidaInsumos(lista,
                    //        (TipoOrdenServicio)txtOrdenServicio.Tag,
                    //        DateTime.Now,
                    //        ((Actividad)((ListViewItem)lvlActividades.SelectedItem).Content).personal1, true);
                    nuevo();
                    buscarOrdenServicio(id);
                }
                ImprimirMensaje.imprimir(resp);
            }
        }
        private List<ProductosActividad> getListaInsumos()
        {
            try
            {
                if (scrollContenedor.Content == null)
                {
                    return null;
                }
                List<ProductosActividad> listProductos = new List<ProductosActividad>();
                List<StackPanel> listPanel = ((StackPanel)scrollContenedor.Content).Children.Cast<StackPanel>().ToList();
                foreach (StackPanel sp in listPanel)
                {
                    ProductosActividad producto = (ProductosActividad)sp.Tag;
                    List<Control> listControl = sp.Children.Cast<Control>().ToList();
                    foreach (var item in listControl)
                    {

                        if (item is Toolkit.IntegerUpDown)
                        {
                            producto.extra = Convert.ToDecimal(((Toolkit.IntegerUpDown)item).Value);
                        }

                    }
                    var resp = new ConsultasTallerSvc().consultarExistenByProducto(producto.idProducto);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        producto.existencia = (decimal)resp.result;
                    }
                    else
                    {
                        producto.existencia = 0;
                    }
                    if (producto.extra > 0)
                    {
                        listProductos.Add(producto);
                    }

                }
                return listProductos;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void btnEntregar_Click(object sender, RoutedEventArgs e)
        {

            if (txtPersonal.personal == null)
            {
                ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "SELECCIONAR AL PERSONAL QUE RECIVE" });
                return;
            }
            OperationResult resp = new OperacionSvc().entregarOrdenServicio(((TipoOrdenServicio)txtOrdenServicio.Tag).idOrdenSer, txtPersonal.personal, mainWindow.inicio._usuario.nombreUsuario);
            if (resp.typeResult == ResultTypes.success)
            {
                int id = Convert.ToInt32(txtOrdenServicio.Text);
                nuevo();
                buscarOrdenServicio(id);
            }
            ImprimirMensaje.imprimir(resp);
        }
    }
}
