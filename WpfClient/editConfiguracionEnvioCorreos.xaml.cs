﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editConfiguracionEnvioCorreos.xaml
    /// </summary>
    public partial class editConfiguracionEnvioCorreos : ViewBase
    {
        public editConfiguracionEnvioCorreos()
        {
            InitializeComponent();
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            cbxDominios.ItemsSource = Enum.GetValues(typeof(enumDominiosCorreos));
            cbxProcesos.ItemsSource = Enum.GetValues(typeof(enumProcesosEnvioCorreo));
            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);
        }
        private void CbxDominios_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender != null)
            {
                if (cbxDominios.SelectedItem != null)
                {
                    enumDominiosCorreos dominiosCorreos = (enumDominiosCorreos)(sender as ComboBox).SelectedItem;
                    var resp = enumDesDominiosCorreos.objEnum(dominiosCorreos);
                    enumDesDominiosCorreos x = (enumDesDominiosCorreos)resp;
                    txtDominio.Text = x.dominio;
                    txtDominio.IsEnabled = dominiosCorreos == enumDominiosCorreos.OTRO;
                }
                else
                {
                    txtDominio.Clear();
                    txtDominio.IsEnabled = false;
                }
                
            }
        }
        void mapForm(ConfiguracionEnvioCorreo envioCorreo)
        {
            if (envioCorreo != null)
            {
                ctrClave.Tag = envioCorreo;
                ctrClave.valor = envioCorreo.idConfiguracionEnvioCorreo;
                cbxDominios.SelectedItem = cbxDominios.ItemsSource.Cast<enumDominiosCorreos>().ToList().Find(s => s == envioCorreo.enumDominios);
                txtDireccion.Text = envioCorreo.direccion;
                txtDominio.Text = envioCorreo.dominio;
                cbxProcesos.SelectedItems.Clear();
                if (envioCorreo.listaProcesosEnvioCorreo != null)
                {
                    foreach (var item in envioCorreo.listaProcesosEnvioCorreo)
                    {
                        cbxProcesos.SelectedItems.Add(cbxProcesos.ItemsSource.Cast<enumProcesosEnvioCorreo>().ToList().Find(s => s == item));
                    }
                }

                txtNombre.Text = envioCorreo.nomDes;
                chcActivo.IsChecked = envioCorreo.activo;
            }
            else
            {
                ctrClave.Tag = null;
                ctrClave.valor = 0;
                cbxDominios.SelectedItem = null;
                txtDireccion.Clear();
                txtDominio.Clear();
                cbxProcesos.SelectedItems.Clear();
                txtNombre.Clear();
                chcActivo.IsChecked = true;
            }
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;

                if (string.IsNullOrEmpty(txtDireccion.Text.Trim()))
                {
                    return new OperationResult(ResultTypes.warning, "FALTO LA DIRECCIÓN DE CORREO");
                }
                if (string.IsNullOrEmpty(txtDominio.Text.Trim()) || txtDominio.Text.Trim() == "@")
                {
                    return new OperationResult(ResultTypes.warning, "FALTO LA DIRECCIÓN DE CORREO");
                }

                ConfiguracionEnvioCorreo envioCorreo = mapForm();
                if (envioCorreo!= null)
                {
                    var resp = new ConfiguracionEnvioCorreoSvc().saveCorreoElectronico(ref envioCorreo);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        base.guardar();
                        mapForm(envioCorreo);
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult(ResultTypes.warning, "Ocurrio un error al leer la información de la pantalla");
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        ConfiguracionEnvioCorreo mapForm()
        {
            try
            {
                ConfiguracionEnvioCorreo envioCorreo = new ConfiguracionEnvioCorreo
                {
                    idConfiguracionEnvioCorreo = ctrClave.Tag == null ? 0 : (ctrClave.Tag as ConfiguracionEnvioCorreo).idConfiguracionEnvioCorreo,
                    direccion = txtDireccion.Text.Trim(),
                    dominio = txtDominio.Text.Trim(),
                    enumDominios = (enumDominiosCorreos)cbxDominios.SelectedItem,
                    activo = chcActivo.IsChecked.Value,
                    nomDes = txtNombre.Text.Trim(),
                    listaProcesosEnvioCorreo = new List<enumProcesosEnvioCorreo>()
                };

                if (cbxProcesos.SelectedItems.Count > 0)
                {
                    envioCorreo.listaProcesosEnvioCorreo = cbxProcesos.SelectedItems.Cast<enumProcesosEnvioCorreo>().ToList();
                }

                return envioCorreo;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public override void buscar()
        {
            try
            {
                ConfiguracionEnvioCorreo envioCorreo = new selectCorreosElectronicos().getCorreos();
                if (envioCorreo != null)
                {
                    mapForm(envioCorreo);
                    base.buscar();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new ConfiguracionEnvioCorreoSvc().getAllCorreosElectronicos();
                if (resp.typeResult == ResultTypes.success)
                {
                    List<ConfiguracionEnvioCorreo> lista = resp.result as List<ConfiguracionEnvioCorreo>;
                    foreach (var item in lista)
                    {
                        var respDet = new ConfiguracionEnvioCorreoSvc().getProcesosByIdCorreo(item.idConfiguracionEnvioCorreo);
                        if (respDet.typeResult == ResultTypes.success)
                        {
                            item.listaProcesosEnvioCorreo = respDet.result as List<enumProcesosEnvioCorreo>;
                        }
                        else if (respDet.typeResult == ResultTypes.recordNotFound)
                        {
                            item.listaProcesosEnvioCorreo = new List<enumProcesosEnvioCorreo>();
                        }
                        else
                        {
                            ImprimirMensaje.imprimir(respDet);
                            return;
                        }
                    }
                    new ReportView().exportarListaConfiguracionEnvioCorreo(lista);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
