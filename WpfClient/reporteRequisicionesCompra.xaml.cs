﻿using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para reporteRequisicionesCompra.xaml
    /// </summary>
    public partial class reporteRequisicionesCompra : ViewBase
    {
        public reporteRequisicionesCompra()
        {
            InitializeComponent();
        }
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                this.lvlRequisiciones.ItemsSource = null;
                OperationResult resp = new RequisicionCompraSvc().getRequisicioCompraByFecha(this.ctrFehas.fechaInicial, this.ctrFehas.fechaFinal);
                if (resp.typeResult == ResultTypes.success)
                {
                    this.lvlRequisiciones.ItemsSource = resp.result as List<RequisicionCompra>;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void btnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.lvlRequisiciones.ItemsSource != null)
                {
                    base.Cursor = Cursors.Wait;
                    new ReportView().generarReporteRequisionesCompra(this.ctrFehas.fechaInicial, this.ctrFehas.fechaFinal, this.lvlRequisiciones.ItemsSource as List<RequisicionCompra>);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
        }
    }
}
