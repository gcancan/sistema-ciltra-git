﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfClient
{
    public enum eTipoBusquedaPersonal
    {
        TODOS = 0,
        OPERADORES = 1,
        DESPACHADORES = 2,
        LAVADORES = 3,
        LLANTEROS = 4,
        MECANICOS = 5,
        DEPARTAMENTO = 6,
        ALL_PERSONAL_ACTIVO = 7
    }
}
