﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editLigarTicketFacturaProveedor.xaml
    /// </summary>
    public partial class editLigarTicketFacturaProveedor : ViewBase
    {
        public editLigarTicketFacturaProveedor()
        {
            InitializeComponent();
        }
        Usuario usuario = null;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                usuario = mainWindow.usuario;
                ctrEmpresa.loaded(usuario.empresa, (new PrivilegioSvc().consultarPrivilegio("isadmin", usuario.idUsuario).typeResult == ResultTypes.success));

                var resp = new ProveedorSvc().getAllProveedoresV2();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxProveedores.ItemsSource = (resp.result as List<ProveedorCombustible>).FindAll(s => s.diesel && s.estatus);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    IsEnabled = false;
                    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    return;
                }

                var respTipoComb = new TipoCombustibleSvc().getTiposCombustibleAllOrById();
                if (respTipoComb.typeResult == ResultTypes.success)
                {
                    cbxTipoCombustible.ItemsSource = respTipoComb.result as List<TipoCombustible>;
                    cbxTipoCombustible.SelectedIndex = 0;
                }
                else
                {
                    ImprimirMensaje.imprimir(respTipoComb);
                    IsEnabled = false;
                    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    return;
                }

                //ctrZonaOperativa.cargarControl(usuario.zonaOperativa, usuario.idUsuario);
                nuevo();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void ChcSeleccionado_Checked(object sender, RoutedEventArgs e)
        {
            calcularImportes();
        }

        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            bucarTickets();
        }
        private void bucarTickets()
        {
            try
            {
                Cursor = Cursors.Wait;

                lvlResultados.ItemsSource = null;
                todo = false;
                btnAccion.Content = "MARCAR TODO";

                if (cbxProveedores.SelectedItem != null && cbxTipoCombustible.SelectedItem != null)
                {
                    var resp = new TicketsCombustibleSvc().getTicketsCombustibleByProveedor(
                        (cbxProveedores.SelectedItem as ProveedorCombustible).idProveedor,
                        ctrEmpresa.empresaSelected.clave,
                        (cbxTipoCombustible.SelectedItem as TipoCombustible).idTipoCombustible);

                    if (resp.typeResult == ResultTypes.success)
                    {
                        lvlResultados.ItemsSource = resp.result as List<TicketsCombustible>;
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
                calcularImportes();
            }
        }
        bool todo = false;
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            todo = !todo;

            if (todo)
            {
                if (lvlResultados.ItemsSource != null)
                {
                    foreach (var item in lvlResultados.ItemsSource as List<TicketsCombustible>)
                    {
                        item.isSeleccionado = true;
                    }
                }
                btnAccion.Content = "DESMARCAR TODO";
            }
            else
            {
                if (lvlResultados.ItemsSource != null)
                {
                    foreach (var item in lvlResultados.ItemsSource as List<TicketsCombustible>)
                    {
                        item.isSeleccionado = false;
                    }
                }
                btnAccion.Content = "MARCAR TODO";
            }


        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }
        void calcularImportes()
        {
            try
            {
                Cursor = Cursors.Wait;
                if (lvlResultados.ItemsSource != null)
                {
                    List<TicketsCombustible> listaTickes = new List<TicketsCombustible>();
                    listaTickes = (lvlResultados.ItemsSource as List<TicketsCombustible>).FindAll(s=>s.isSeleccionado);
                    decimal sumaImportes = listaTickes.Sum(s => s.importe);
                    decimal sumaIEPS = listaTickes.Sum(s => s.IEMPS);
                    //decimal subTotal = (sumaImportes - sumaIEPS) / 1.16m;
                    //decimal IVA = subTotal * .16m;

                    txtSumaLitros.Text = listaTickes.FindAll(s => s.isSeleccionado).Sum(s => s.litros).ToString("N4");
                    txtSubTotalIEPS.Text = listaTickes.Sum(s => s.subTotalConIEPS).ToString("C4");
                    txtSubTotal.Text = listaTickes.Sum(s => s.subTotal).ToString("C4");
                    txtIVA.Text = listaTickes.Sum(s => s.iva).ToString("C4");
                    txtSumaIEPS.Text = sumaIEPS.ToString("C4");
                    txtSumaImportes.Text = sumaImportes.ToString("C4");
                    lblContadorSelect.Content = listaTickes.Count(s => s.isSeleccionado).ToString("N0");
                    lblContadorTodos.Content = (lvlResultados.ItemsSource as List<TicketsCombustible>).Count.ToString("N0");

                    if (listaTickes.Count == listaTickes.Count(s => s.isSeleccionado))
                    {
                        todo = true;
                        btnAccion.Content = "DESMARCAR TODO";
                    }
                    else
                    {
                        todo = false;
                        btnAccion.Content = "MARCAR TODO";
                    }
                }
                else
                {
                    txtSumaLitros.Text = decimal.Zero.ToString("N4");
                    txtSumaIEPS.Text = decimal.Zero.ToString("C4");
                    txtSumaImportes.Text = decimal.Zero.ToString("C4");
                    txtSubTotal.Text = decimal.Zero.ToString("C4");
                    txtSubTotalIEPS.Text = decimal.Zero.ToString("C4");
                    txtIVA.Text = decimal.Zero.ToString("C4");
                    todo = false;
                    btnAccion.Content = "MARCAR TODO";
                    lblContadorSelect.Content = "0";
                    lblContadorTodos.Content = "0";
                }

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        OperationResult validar()
        {
            try
            {
                if (ctrEmpresa.empresaSelected == null)
                {
                    return new OperationResult(ResultTypes.warning, "SELECCIONAR UNA EMPRESA PARA CONTINUAR");
                }
                //if (ctrZonaOperativa.zonaOperativa == null)
                //{
                //    return new OperationResult(ResultTypes.warning, "SELECCIONAR UNA ZONA OPERATIVA PARA CONTINUAR");
                //}
                if (cbxProveedores.SelectedItem == null)
                {
                    return new OperationResult(ResultTypes.warning, "SELECCIONAR UN PROVEEDOR PARA CONTINUAR");
                }
                if (cbxTipoCombustible.SelectedItem == null)
                {
                    return new OperationResult(ResultTypes.warning, "SELECCIONAR UN TIPO DE COMBUSTIBLE PARA CONTINUAR");
                }
                if (string.IsNullOrEmpty(txtFactura.Text.Trim()))
                {
                    return new OperationResult(ResultTypes.warning, "CONTINUAR SE REQUIERE LA FACTURA");
                }
                if (string.IsNullOrEmpty(txtFolioFiscal.Text.Trim()))
                {
                    return new OperationResult(ResultTypes.warning, "PARA CONTINUAR SE REQUIERE EL FOLIO FISCAL");
                }
                if (dtpFechaFactura.Value == null)
                {
                    return new OperationResult(ResultTypes.warning, "PARA CONTINUAR SE REQUIERE ESPECIFICAR LA FECHA");
                }
                if (lvlResultados.ItemsSource == null)
                {
                    return new OperationResult(ResultTypes.warning, "NO HAY TICKETS PARA GUARDAR A LA FACTURA");
                }
                else
                {
                    if ((lvlResultados.ItemsSource as List<TicketsCombustible>).Count(s => s.isSeleccionado) <= 0)
                    {
                        return new OperationResult(ResultTypes.warning, "NO HAY TICKETS PARA GUARDAR A LA FACTURA");
                    }
                }

                return new OperationResult(ResultTypes.success, "");
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;

                var val = validar();
                if (val.typeResult != ResultTypes.success) return val;

                FacturaProveedorCombustible factura = mapForm();
                if (factura != null)
                {
                    var resp = new TicketsCombustibleSvc().saveFaturaProveedorCombustible(ref factura);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mainWindow.habilitar = true;
                        nuevo();
                        bucarTickets();
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LA INFORMACIÓN DE LA PANTALLA");
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public override void nuevo()
        {
            base.nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            txtFactura.Clear();
            txtFolioFiscal.Clear();
            
            lvlResultados.ItemsSource = null;
            dtpFechaFactura.Value = DateTime.Now;
            calcularImportes();
        }
        private FacturaProveedorCombustible mapForm()
        {
            try
            {
                FacturaProveedorCombustible facturaProveedorCombustible = new FacturaProveedorCombustible
                {
                    idFacturaProveedorCombustible = 0,
                    empresa = ctrEmpresa.empresaSelected,
                    factura = txtFactura.Text.Trim(),
                    fechaCaptura = DateTime.Now,
                    fechaFactura = dtpFechaFactura.Value.Value,
                    proveedor = cbxProveedores.SelectedItem as ProveedorCombustible,
                    tipoCombustible = cbxTipoCombustible.SelectedItem as TipoCombustible,
                    usuario = usuario.nombreUsuario,
                    listaDetalles = new List<DetalleFacturaProveedorCombustible>(),
                    folioFiscal = txtFolioFiscal.Text.Trim(),
                    zonaOperativa = null// ctrZonaOperativa.zonaOperativa
                };

                foreach (var tickets in (lvlResultados.ItemsSource as List<TicketsCombustible>).FindAll(s => s.isSeleccionado))
                {
                    facturaProveedorCombustible.listaDetalles.Add(new DetalleFacturaProveedorCombustible
                    {
                        idDetallaFacturaProveedorCombustible = 0,
                        idCargaCombustible = tickets.vale,
                        ticket = tickets.ticket,
                        litros = tickets.litros,
                        precioNeto = tickets.precio,
                        importe = tickets.importe,
                        subTotalIEPS = tickets.subTotalConIEPS,
                        iva = tickets.iva,
                        unidadTrans = tickets.unidad,
                        IEMPS = tickets.IEMPS,
                        normal = tickets.normal
                    });
                }

                return facturaProveedorCombustible;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }

        private void CbxProveedores_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            nuevo();
        }

        private void CbxTipoCombustible_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            nuevo();
        }
    }
}
