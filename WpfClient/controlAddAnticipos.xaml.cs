﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para controlAddAnticipos.xaml
    /// </summary>
    public partial class controlAddAnticipos : UserControl
    {
        public controlAddAnticipos(AnticipoMaster anticipo)
        {
            InitializeComponent();
            this.anticipo = anticipo;
        }
        public controlAddAnticipos()
        {
            InitializeComponent();
        }
        AnticipoMaster anticipo = null;
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (anticipo != null)
            {
                mapAnticipo(anticipo);
                recalcularValor();
            }
        }

        private void mapAnticipo(AnticipoMaster anticipo)
        {
            if (anticipo != null)
            {
                ctrFolio.Tag = anticipo;
                ctrFolio.valor = anticipo.idAnticipoMaster;
                ctrSubTotal.valor = decimal.Round(anticipo.listaDetalles.Sum(s => s.importe), 2);
                ctrFechaSolicitud.Value = anticipo.fechaPago;
                ctrFechaSolicitud.IsEnabled = anticipo.idAnticipoMaster > 0 ? false : true;
                if (anticipo.idAnticipoMaster != 0)
                {
                    ctrFechaSolicitud.IsEnabled = false;
                }
                foreach (var det in anticipo.listaDetalles)
                {
                    llenarDetalles(det);
                }
            }
        }

        private void llenarDetalles(DetalleAnticipoMaster det)
        {
            try
            {
                StackPanel stpContent = new StackPanel { Tag = det, Orientation = Orientation.Horizontal, HorizontalAlignment = HorizontalAlignment.Right };
                Label lbl = new Label
                {
                    Content = det.tipoAnticipo.anticipo + ".  $",
                    HorizontalContentAlignment = HorizontalAlignment.Right,
                    FontWeight = FontWeights.Medium,
                    Foreground = Brushes.SteelBlue,
                    VerticalAlignment = VerticalAlignment.Center
                };
                stpContent.Children.Add(lbl);

                controlDecimal ctrDec = new controlDecimal
                {
                    FontWeight = FontWeights.Bold,
                    alienacionTexto = HorizontalAlignment.Right,
                    Width = 100,
                    Tag = det,
                    IsEnabled = det.idDetalleAnticipoMaster > 0 ? false : true
                };
                ctrDec.txtDecimal.Tag = ctrDec;
                ctrDec.txtDecimal.TextChanged += TxtDecimal_TextChanged;
                ctrDec.valor = decimal.Round(det.importe, 2);
                stpContent.Children.Add(ctrDec);

                stpDetalles.Children.Add(stpContent);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void TxtDecimal_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                TextBox txt = sender as TextBox;
                controlDecimal ctr = txt.Tag as controlDecimal;
                DetalleAnticipoMaster detalle = ctr.Tag as DetalleAnticipoMaster;
                detalle.importe = ctr.valor;

                recalcularValor();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        void recalcularValor()
        {
            try
            {
                List<StackPanel> listaStp = stpDetalles.Children.Cast<StackPanel>().ToList();
                List<DetalleAnticipoMaster> listaDet = listaStp.Select(s => s.Tag as DetalleAnticipoMaster).ToList();
                ctrSubTotal.valor = listaDet.Sum(s => s.importe);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        public decimal subTotal => ctrSubTotal.valor;
    }
}
