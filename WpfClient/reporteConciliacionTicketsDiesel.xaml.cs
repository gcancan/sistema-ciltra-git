﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static WpfClient.ImprimirMensaje;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para reporteConciliacionTicketsDiesel.xaml
    /// </summary>
    public partial class reporteConciliacionTicketsDiesel : ViewBase
    {
        public reporteConciliacionTicketsDiesel()
        {
            InitializeComponent();
        }
        Usuario usuario = null;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                usuario = mainWindow.usuario;
                ctrEmpresa.loaded(usuario.empresa, (new PrivilegioSvc().consultarPrivilegio("isAdmin", usuario.idUsuario).typeResult == ResultTypes.success));

                //var respZonas = new ZonaOperativaSvc().getAllZonasOperativas();
                //if (respZonas.typeResult == ResultTypes.success)
                //{
                //    cbxZonasOperativas.ItemsSource = respZonas.result as List<ZonaOperativa>;
                //    cbxZonasOperativas.SelectedItem = (cbxZonasOperativas.ItemsSource as List<ZonaOperativa>).Find(s => s.idZonaOperativa == usuario.zonaOperativa.idZonaOperativa);
                //    stpZonaOperativa.IsEnabled = new PrivilegioSvc().consultarPrivilegio("MULTI_ZONA_OPETATIVA", usuario.idUsuario).typeResult == ResultTypes.success;
                //}
                //else
                //{
                //    ImprimirMensaje.imprimir(respZonas);
                //    IsEnabled = false;
                //    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                //    return;
                //}

                var resp = new ProveedorSvc().getAllProveedoresV2();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxProveedores.ItemsSource = (resp.result as List<ProveedorCombustible>).FindAll(s => s.diesel && s.estatus);
                    cbxProveedoresFac.ItemsSource = (resp.result as List<ProveedorCombustible>).FindAll(s => s.diesel && s.estatus);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    IsEnabled = false;
                    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    return;
                }

                var respTipoComb = new TipoCombustibleSvc().getTiposCombustibleAllOrById();
                if (respTipoComb.typeResult == ResultTypes.success)
                {
                    cbxTipoCombustible.ItemsSource = respTipoComb.result as List<TipoCombustible>;
                    cbxTipoCombustible.SelectedIndex = 0;
                }
                else
                {
                    ImprimirMensaje.imprimir(respTipoComb);
                    IsEnabled = false;
                    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    return;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
            }
        }

        private void ChcTodosZonas_Unchecked(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                CheckBox check = sender as CheckBox;
                if (check.IsChecked.Value)
                {
                    cbxZonasOperativas.SelectAll();
                }
                else
                {
                    cbxZonasOperativas.SelectedItems.Clear();
                }
            }
        }

        private void ChcTodosProveedores_Unchecked(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                CheckBox check = sender as CheckBox;
                if (check.IsChecked.Value)
                {
                    cbxProveedores.SelectAll();
                }
                else
                {
                    cbxProveedores.SelectedItems.Clear();
                }
            }
        }

        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                List<string> listaZonas = cbxZonasOperativas.SelectedItems.Cast<ZonaOperativa>().ToList().Select(s => s.idZonaOperativa.ToString()).ToList();
                List<string> listaProveedores = cbxProveedores.SelectedItems.Cast< ProveedorCombustible>().ToList().Select(s => s.idProveedor.ToString()).ToList();

                var resp = new TicketsCombustibleSvc().getFacturasProveedorDieselByFiltros(ctrEmpresa.empresaSelected.clave,
                    ctrFechas.fechaInicial, ctrFechas.fechaFinal, listaZonas, chcTodosZonas.IsChecked.Value, listaProveedores, chcTodosProveedores.IsChecked.Value,
                    (cbxTipoCombustible.SelectedItem as TipoCombustible).idTipoCombustible);

                if (resp.typeResult == ResultTypes.success)
                {
                    var lista = resp.result as List<FacturaProveedorCombustible>;
                    List<ListViewItem> listaLvl = new List<ListViewItem>();
                    foreach (var item in lista)
                    {
                        ListViewItem lvl = new ListViewItem()
                        { Content = item };
                        lvl.Selected += Lvl_Selected;
                        lvl.MouseDoubleClick += Lvl_Selected;
                        listaLvl.Add(lvl);
                    };
                    lvlResultados.ItemsSource = listaLvl;
                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                ListViewItem lvl = sender as ListViewItem;
                FacturaProveedorCombustible factura = lvl.Content as FacturaProveedorCombustible;
                mapControlBusqueda(factura);
            }
            else
            {
                mapControlBusqueda(null);
            }
        }

        private void mapControlBusqueda(FacturaProveedorCombustible factura)
        {
            if (factura != null)
            {
                txtFactura.Text = factura.factura;
                cbxProveedoresFac.SelectedItem = (cbxProveedoresFac.ItemsSource as List<ProveedorCombustible>).Find(s => s.idProveedor == factura.proveedor.idProveedor);
            }
            else
            {
                txtFactura.Clear();
                cbxProveedoresFac.SelectedItem = null;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    Button btn = sender as Button;
                }
            }
            catch (Exception ex)
            {
                
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lvlResultados.SelectedItem != null)
                {
                    FacturaProveedorCombustible facturaProveedor = lvlResultados.SelectedItem as FacturaProveedorCombustible;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (lvlResultados.ItemsSource == null) return;

                List<FacturaProveedorCombustible> listaFActuras = (lvlResultados.ItemsSource as List<ListViewItem>).Select(s => s.Content as FacturaProveedorCombustible).ToList();


                new ReportView().exportarListaFacturaProveedorCombustible(listaFActuras);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BtnBuscarFactura_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;

                if (cbxProveedoresFac.SelectedItem == null) return;
                if (string.IsNullOrEmpty(txtFactura.Text.Trim())) return;

                var resp = new TicketsCombustibleSvc().getFacturasProveedorByFactura((cbxProveedoresFac.SelectedItem as ProveedorCombustible).idProveedor, txtFactura.Text.Trim());
                if (resp.typeResult == ResultTypes.success)
                {
                    new vistaFacturaProveedorDiesel().cargarControles(resp.result as FacturaProveedorCombustible);
                }
                else
                {
                    imprimir(resp);
                }
                
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
