﻿using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectCargaTicketsDiesel.xaml
    /// </summary>
    public partial class selectCargaTicketsDiesel : Window
    {       
        public selectCargaTicketsDiesel(Usuario usuario)
        {
            InitializeComponent();
            this.usuario = usuario;
        }
        public selectCargaTicketsDiesel()
        {
            InitializeComponent();
        }
        Usuario usuario = null;
        bool filtros = true;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (filtros)
            {
                if (usuario!= null)
                {
                    ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
                    ctrEmpresa.loaded(usuario);
                    ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
                    ctrZonaOperativa.cargarControl(usuario.zonaOperativa, usuario.idUsuario);

                    buscar();
                }
            }
            else
            {
                stpFiltros.Visibility = Visibility.Collapsed;
            }
        }
        public CargaTicketDiesel seleccionarCargaDisel()
        {
            try
            {
                bool? resp = ShowDialog();
                if (resp.Value && lvlResultados.SelectedItem != null)
                {
                    return ((lvlResultados.SelectedItem as ListViewItem).Content as CargaTicketDiesel);
                }
                return null;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }
        private string idUnidad = string.Empty;
        List<int> listaId = new List<int>();
        public CargaTicketDiesel seleccionarCargaDiselByUnidad(string idUnidad, List<int> listaId)
        {
            try
            {
                this.idUnidad = idUnidad;
                this.listaId = listaId;
                this.filtros = false;
                buscarTicketsByUnidad();
                bool? resp = ShowDialog();
                if (resp.Value && lvlResultados.SelectedItem != null)
                {
                    return ((lvlResultados.SelectedItem as ListViewItem).Content as CargaTicketDiesel);
                }
                return null;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }
        private void buscarTicketsByUnidad()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new CargaTicketDieselSvc().getCargaTicketsDieselByUnidad(this.idUnidad);
                if (resp.typeResult == ResultTypes.success)
                {
                    llenarLista((resp.result as List<CargaTicketDiesel>).Where(s => !listaId.Contains(s.clave)).ToList());
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lvlResultados.ItemsSource = null;
        }
        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lvlResultados.ItemsSource = null;
        }
        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            buscar();
        }
        private void buscar()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new CargaTicketDieselSvc().getCargaTicketsDieselByEmpresaZonaOperativa(ctrEmpresa.empresaSelected.clave, ctrZonaOperativa.zonaOperativa.idZonaOperativa);
                if (resp.typeResult == ResultTypes.success)
                {
                    llenarLista( resp.result as List<CargaTicketDiesel>);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void llenarLista(List<CargaTicketDiesel> lista)
        {
            try
            {
                lvlResultados.ItemsSource = null;
                List<ListViewItem> list = new List<ListViewItem>();
                foreach (CargaTicketDiesel item in lista)
                {
                    ListViewItem lvl = new ListViewItem
                    {
                        Content = item
                    };
                    lvl.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                    list.Add(lvl);
                }
                this.lvlResultados.ItemsSource = list;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlResultados.ItemsSource);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private bool UserFilter(object item)
        {
            if (lvlResultados.ItemsSource == null) return false;
            return (string.IsNullOrEmpty(this.txtBuscador.Text) ||
          ((CargaTicketDiesel)(item as ListViewItem).Content).ticket.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
          ((CargaTicketDiesel)(item as ListViewItem).Content).folioImpreso.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
          ((CargaTicketDiesel)(item as ListViewItem).Content).tractor.clave.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
          ((CargaTicketDiesel)(item as ListViewItem).Content).proveedor.nombre.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0);
        }
          

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (lvlResultados.ItemsSource == null) return;
            CollectionViewSource.GetDefaultView(this.lvlResultados.ItemsSource).Refresh();
        }
    }
}
