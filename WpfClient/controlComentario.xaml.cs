﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para controlComentario.xaml
    /// </summary>
    public partial class controlComentario : UserControl
    {
        public controlComentario()
        {
            InitializeComponent();
        }
        public controlComentario(string usuario, DateTime fecha, string comentario)
        {
            InitializeComponent();
            lblUsuario.Content = usuario.ToUpper();
            lblFecha.Content = fecha.ToString("dd/MM/yyyy HH:mm");
            txtComentario.Text = comentario;
        }
    }
}
