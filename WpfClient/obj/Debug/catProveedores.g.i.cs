﻿#pragma checksum "..\..\catProveedores.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "999BF4E19854E890422F4ED2FF28BC9D5EA01B340CC4B75614FABE6B0C446BAC"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using MahApps.Metro;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfClient;


namespace WpfClient {
    
    
    /// <summary>
    /// catProveedores
    /// </summary>
    public partial class catProveedores : WpfClient.ViewBase, System.Windows.Markup.IComponentConnector {
        
        
        #line 21 "..\..\catProveedores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnExportar;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\catProveedores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlEnteros ctrClaveProveedor;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\catProveedores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtEstatus;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\catProveedores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtRazonSocial;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\catProveedores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtRFC;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\catProveedores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtDireccion;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\catProveedores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtTelefono;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\catProveedores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtColonia;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\catProveedores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCambiarColonia;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\catProveedores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtCP;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\catProveedores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtCiudad;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\catProveedores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtEstado;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\catProveedores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtPais;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\catProveedores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chcVehiculos;
        
        #line default
        #line hidden
        
        
        #line 102 "..\..\catProveedores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chcLlantasNuevas;
        
        #line default
        #line hidden
        
        
        #line 103 "..\..\catProveedores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chcRenovadorLlantas;
        
        #line default
        #line hidden
        
        
        #line 104 "..\..\catProveedores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chcReparadorLlantas;
        
        #line default
        #line hidden
        
        
        #line 105 "..\..\catProveedores.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chcDiesel;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Sistema CILTRA;component/catproveedores.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\catProveedores.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.btnExportar = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\catProveedores.xaml"
            this.btnExportar.Click += new System.Windows.RoutedEventHandler(this.BtnExportar_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.ctrClaveProveedor = ((WpfClient.controlEnteros)(target));
            return;
            case 3:
            this.txtEstatus = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.txtRazonSocial = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.txtRFC = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.txtDireccion = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.txtTelefono = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.txtColonia = ((System.Windows.Controls.TextBox)(target));
            
            #line 67 "..\..\catProveedores.xaml"
            this.txtColonia.KeyDown += new System.Windows.Input.KeyEventHandler(this.txtColonia_KeyDown);
            
            #line default
            #line hidden
            return;
            case 9:
            this.btnCambiarColonia = ((System.Windows.Controls.Button)(target));
            
            #line 69 "..\..\catProveedores.xaml"
            this.btnCambiarColonia.Click += new System.Windows.RoutedEventHandler(this.btnCambiarColonia_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.txtCP = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.txtCiudad = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.txtEstado = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.txtPais = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.chcVehiculos = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 15:
            this.chcLlantasNuevas = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 16:
            this.chcRenovadorLlantas = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 17:
            this.chcReparadorLlantas = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 18:
            this.chcDiesel = ((System.Windows.Controls.CheckBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

