﻿#pragma checksum "..\..\editLiquidaciones.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "6970F1BE1D8A1D5E24A68483B4E679D050C5493A5AD4A0D37DE779C505B3BC82"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfClient;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Chromes;
using Xceed.Wpf.Toolkit.Core.Converters;
using Xceed.Wpf.Toolkit.Core.Input;
using Xceed.Wpf.Toolkit.Core.Media;
using Xceed.Wpf.Toolkit.Core.Utilities;
using Xceed.Wpf.Toolkit.Panels;
using Xceed.Wpf.Toolkit.Primitives;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Commands;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Xceed.Wpf.Toolkit.Zoombox;


namespace WpfClient {
    
    
    /// <summary>
    /// editLiquidaciones
    /// </summary>
    public partial class editLiquidaciones : WpfClient.ViewBase, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 42 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlFechas ctrFecha;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlEnteros ctrClave;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlEmpresas ctrEmpresa;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.CheckComboBox cbxDepartamentos;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.CheckComboBox cbxPuestos;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBuscar;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnLiquidar;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border brdExportar;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rbnResumen;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rbnDetalle;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnExportar;
        
        #line default
        #line hidden
        
        
        #line 87 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Expander expDetalles;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox lBoxDeptos;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox lBoxPuestos;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBuscador;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtTotal;
        
        #line default
        #line hidden
        
        
        #line 110 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvlResumen;
        
        #line default
        #line hidden
        
        
        #line 158 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblNombrePersonal;
        
        #line default
        #line hidden
        
        
        #line 161 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblBalanceHeader;
        
        #line default
        #line hidden
        
        
        #line 171 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSumaViajesBalanceador;
        
        #line default
        #line hidden
        
        
        #line 175 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvlViajesAlimentosBalanceados;
        
        #line default
        #line hidden
        
        
        #line 205 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSumaViajesSecos;
        
        #line default
        #line hidden
        
        
        #line 209 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvlViajesSecos;
        
        #line default
        #line hidden
        
        
        #line 239 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSumaViajesAtlantes;
        
        #line default
        #line hidden
        
        
        #line 243 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvlViajesAtlantes;
        
        #line default
        #line hidden
        
        
        #line 272 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSumaSueldoFijos;
        
        #line default
        #line hidden
        
        
        #line 276 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvlPagosFijos;
        
        #line default
        #line hidden
        
        
        #line 304 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSumaApoyo;
        
        #line default
        #line hidden
        
        
        #line 308 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvlPagosApoyos;
        
        #line default
        #line hidden
        
        
        #line 326 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Expander expDescuentos;
        
        #line default
        #line hidden
        
        
        #line 335 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSumaDescuentos;
        
        #line default
        #line hidden
        
        
        #line 339 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpDescuentos;
        
        #line default
        #line hidden
        
        
        #line 340 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvlDescuentos;
        
        #line default
        #line hidden
        
        
        #line 376 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAddDescuento;
        
        #line default
        #line hidden
        
        
        #line 377 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnDeleteDescuento;
        
        #line default
        #line hidden
        
        
        #line 383 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Expander expExtras;
        
        #line default
        #line hidden
        
        
        #line 392 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSumaExtras;
        
        #line default
        #line hidden
        
        
        #line 396 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpExtras;
        
        #line default
        #line hidden
        
        
        #line 397 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvlExtras;
        
        #line default
        #line hidden
        
        
        #line 421 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAddExtra;
        
        #line default
        #line hidden
        
        
        #line 422 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnDeleteExtra;
        
        #line default
        #line hidden
        
        
        #line 428 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Expander expHorasExtra;
        
        #line default
        #line hidden
        
        
        #line 437 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblSumaHorasExtras;
        
        #line default
        #line hidden
        
        
        #line 441 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpHorasExtras;
        
        #line default
        #line hidden
        
        
        #line 442 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvlHoraExtras;
        
        #line default
        #line hidden
        
        
        #line 481 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAddHoraExtra;
        
        #line default
        #line hidden
        
        
        #line 482 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnDeleteHoraExtra;
        
        #line default
        #line hidden
        
        
        #line 494 "..\..\editLiquidaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblBalance;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Sistema CILTRA;component/editliquidaciones.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\editLiquidaciones.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ctrFecha = ((WpfClient.controlFechas)(target));
            return;
            case 2:
            this.ctrClave = ((WpfClient.controlEnteros)(target));
            return;
            case 3:
            this.ctrEmpresa = ((WpfClient.controlEmpresas)(target));
            return;
            case 4:
            this.cbxDepartamentos = ((Xceed.Wpf.Toolkit.CheckComboBox)(target));
            return;
            case 5:
            this.cbxPuestos = ((Xceed.Wpf.Toolkit.CheckComboBox)(target));
            return;
            case 6:
            this.btnBuscar = ((System.Windows.Controls.Button)(target));
            
            #line 57 "..\..\editLiquidaciones.xaml"
            this.btnBuscar.Click += new System.Windows.RoutedEventHandler(this.BtnBuscar_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.btnLiquidar = ((System.Windows.Controls.Button)(target));
            
            #line 63 "..\..\editLiquidaciones.xaml"
            this.btnLiquidar.Click += new System.Windows.RoutedEventHandler(this.BtnLiquidar_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.brdExportar = ((System.Windows.Controls.Border)(target));
            return;
            case 9:
            this.rbnResumen = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 10:
            this.rbnDetalle = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 11:
            this.btnExportar = ((System.Windows.Controls.Button)(target));
            
            #line 75 "..\..\editLiquidaciones.xaml"
            this.btnExportar.Click += new System.Windows.RoutedEventHandler(this.BtnExportar_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.expDetalles = ((System.Windows.Controls.Expander)(target));
            return;
            case 13:
            this.lBoxDeptos = ((System.Windows.Controls.ListBox)(target));
            return;
            case 14:
            this.lBoxPuestos = ((System.Windows.Controls.ListBox)(target));
            return;
            case 15:
            this.txtBuscador = ((System.Windows.Controls.TextBox)(target));
            
            #line 106 "..\..\editLiquidaciones.xaml"
            this.txtBuscador.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txtBuscador_TextChanged);
            
            #line default
            #line hidden
            return;
            case 16:
            this.txtTotal = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.lvlResumen = ((System.Windows.Controls.ListView)(target));
            return;
            case 18:
            this.lblNombrePersonal = ((System.Windows.Controls.Label)(target));
            return;
            case 19:
            this.lblBalanceHeader = ((System.Windows.Controls.Label)(target));
            return;
            case 20:
            this.lblSumaViajesBalanceador = ((System.Windows.Controls.Label)(target));
            return;
            case 21:
            this.lvlViajesAlimentosBalanceados = ((System.Windows.Controls.ListView)(target));
            return;
            case 22:
            this.lblSumaViajesSecos = ((System.Windows.Controls.Label)(target));
            return;
            case 23:
            this.lvlViajesSecos = ((System.Windows.Controls.ListView)(target));
            return;
            case 24:
            this.lblSumaViajesAtlantes = ((System.Windows.Controls.Label)(target));
            return;
            case 25:
            this.lvlViajesAtlantes = ((System.Windows.Controls.ListView)(target));
            return;
            case 26:
            this.lblSumaSueldoFijos = ((System.Windows.Controls.Label)(target));
            return;
            case 27:
            this.lvlPagosFijos = ((System.Windows.Controls.ListView)(target));
            return;
            case 29:
            this.lblSumaApoyo = ((System.Windows.Controls.Label)(target));
            return;
            case 30:
            this.lvlPagosApoyos = ((System.Windows.Controls.ListView)(target));
            return;
            case 32:
            this.expDescuentos = ((System.Windows.Controls.Expander)(target));
            return;
            case 33:
            this.lblSumaDescuentos = ((System.Windows.Controls.Label)(target));
            return;
            case 34:
            this.stpDescuentos = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 35:
            this.lvlDescuentos = ((System.Windows.Controls.ListView)(target));
            return;
            case 37:
            this.btnAddDescuento = ((System.Windows.Controls.Button)(target));
            
            #line 376 "..\..\editLiquidaciones.xaml"
            this.btnAddDescuento.Click += new System.Windows.RoutedEventHandler(this.BtnAddDescuento_Click);
            
            #line default
            #line hidden
            return;
            case 38:
            this.btnDeleteDescuento = ((System.Windows.Controls.Button)(target));
            
            #line 377 "..\..\editLiquidaciones.xaml"
            this.btnDeleteDescuento.Click += new System.Windows.RoutedEventHandler(this.BtnDeleteDescuento_Click);
            
            #line default
            #line hidden
            return;
            case 39:
            this.expExtras = ((System.Windows.Controls.Expander)(target));
            return;
            case 40:
            this.lblSumaExtras = ((System.Windows.Controls.Label)(target));
            return;
            case 41:
            this.stpExtras = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 42:
            this.lvlExtras = ((System.Windows.Controls.ListView)(target));
            return;
            case 44:
            this.btnAddExtra = ((System.Windows.Controls.Button)(target));
            
            #line 421 "..\..\editLiquidaciones.xaml"
            this.btnAddExtra.Click += new System.Windows.RoutedEventHandler(this.BtnAddExtra_Click);
            
            #line default
            #line hidden
            return;
            case 45:
            this.btnDeleteExtra = ((System.Windows.Controls.Button)(target));
            
            #line 422 "..\..\editLiquidaciones.xaml"
            this.btnDeleteExtra.Click += new System.Windows.RoutedEventHandler(this.BtnDeleteExtra_Click);
            
            #line default
            #line hidden
            return;
            case 46:
            this.expHorasExtra = ((System.Windows.Controls.Expander)(target));
            return;
            case 47:
            this.lblSumaHorasExtras = ((System.Windows.Controls.Label)(target));
            return;
            case 48:
            this.stpHorasExtras = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 49:
            this.lvlHoraExtras = ((System.Windows.Controls.ListView)(target));
            return;
            case 52:
            this.btnAddHoraExtra = ((System.Windows.Controls.Button)(target));
            
            #line 481 "..\..\editLiquidaciones.xaml"
            this.btnAddHoraExtra.Click += new System.Windows.RoutedEventHandler(this.BtnAddHoraExtra_Click);
            
            #line default
            #line hidden
            return;
            case 53:
            this.btnDeleteHoraExtra = ((System.Windows.Controls.Button)(target));
            
            #line 482 "..\..\editLiquidaciones.xaml"
            this.btnDeleteHoraExtra.Click += new System.Windows.RoutedEventHandler(this.BtnDeleteHoraExtra_Click);
            
            #line default
            #line hidden
            return;
            case 54:
            this.lblBalance = ((System.Windows.Controls.Label)(target));
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 28:
            
            #line 284 "..\..\editLiquidaciones.xaml"
            ((System.Windows.Controls.ComboBox)(target)).SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.CbxCombo_SelectionChanged);
            
            #line default
            #line hidden
            break;
            case 31:
            
            #line 316 "..\..\editLiquidaciones.xaml"
            ((System.Windows.Controls.ComboBox)(target)).SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.CbxCombo_SelectionChanged);
            
            #line default
            #line hidden
            break;
            case 36:
            
            #line 360 "..\..\editLiquidaciones.xaml"
            ((System.Windows.Controls.TextBox)(target)).LostFocus += new System.Windows.RoutedEventHandler(this.TxtCantidadDescuento_LostFocus);
            
            #line default
            #line hidden
            break;
            case 43:
            
            #line 411 "..\..\editLiquidaciones.xaml"
            ((System.Windows.Controls.TextBox)(target)).LostFocus += new System.Windows.RoutedEventHandler(this.TxtCantidadDescuento_LostFocus);
            
            #line default
            #line hidden
            break;
            case 50:
            
            #line 456 "..\..\editLiquidaciones.xaml"
            ((System.Windows.Controls.TextBox)(target)).LostFocus += new System.Windows.RoutedEventHandler(this.TxtCantidadDescuento_LostFocus);
            
            #line default
            #line hidden
            break;
            case 51:
            
            #line 463 "..\..\editLiquidaciones.xaml"
            ((System.Windows.Controls.TextBox)(target)).LostFocus += new System.Windows.RoutedEventHandler(this.TxtCantidadDescuento_LostFocus);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

