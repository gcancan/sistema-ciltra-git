﻿#pragma checksum "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "26865483615B6EA2DC96939E0567A046D5F71EB58C441D490F8CD09626BD119B"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfClient;
using WpfClient.ProcesosOS;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Chromes;
using Xceed.Wpf.Toolkit.Core.Converters;
using Xceed.Wpf.Toolkit.Core.Input;
using Xceed.Wpf.Toolkit.Core.Media;
using Xceed.Wpf.Toolkit.Core.Utilities;
using Xceed.Wpf.Toolkit.Panels;
using Xceed.Wpf.Toolkit.Primitives;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Commands;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Xceed.Wpf.Toolkit.Zoombox;


namespace WpfClient.ProcesosOS {
    
    
    /// <summary>
    /// ProcesamientoOrdenServicio
    /// </summary>
    public partial class ProcesamientoOrdenServicio : WpfClient.ViewBase, System.Windows.Markup.IComponentConnector {
        
        
        #line 36 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlEnteros ctrFolioOrdenServicio;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlUnidadesTransporte ctrUnidadTransporte;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtTipoOrdenServicio;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpTipoServicio;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtTipoServicio;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvlOrdenesTrabajo;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer scrollControles;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Expander expanderRecepcion;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chcExpRecepcion;
        
        #line default
        #line hidden
        
        
        #line 81 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpRecepcion;
        
        #line default
        #line hidden
        
        
        #line 84 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlPersonal ctrPersonalEntrega;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCambiarPersonalRecibe;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DateTimePicker dtpFechaRecepcion;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Expander expanderActividades;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chcExpActividades;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpActividades;
        
        #line default
        #line hidden
        
        
        #line 102 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBuscadorActividad;
        
        #line default
        #line hidden
        
        
        #line 104 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpControlesPosicion;
        
        #line default
        #line hidden
        
        
        #line 106 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.CheckComboBox cbxChcPosLlantas;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chcTodosPosiciones;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAddActividad;
        
        #line default
        #line hidden
        
        
        #line 122 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpAtividadesOrdenServ;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Expander expanderAsignacion;
        
        #line default
        #line hidden
        
        
        #line 131 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chcExpAsignacion;
        
        #line default
        #line hidden
        
        
        #line 133 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpAsignacion;
        
        #line default
        #line hidden
        
        
        #line 137 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label chcResponsable;
        
        #line default
        #line hidden
        
        
        #line 138 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlPersonal ctrResponsable;
        
        #line default
        #line hidden
        
        
        #line 139 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCambiarResponsable;
        
        #line default
        #line hidden
        
        
        #line 142 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label chcAuxiliar1;
        
        #line default
        #line hidden
        
        
        #line 143 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlPersonal ctrAuxiliar1;
        
        #line default
        #line hidden
        
        
        #line 144 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCambiarA;
        
        #line default
        #line hidden
        
        
        #line 150 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label chcAuxiliar2;
        
        #line default
        #line hidden
        
        
        #line 151 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlPersonal ctrAuxiliar2;
        
        #line default
        #line hidden
        
        
        #line 152 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCambiarAuxiliar2;
        
        #line default
        #line hidden
        
        
        #line 156 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DateTimePicker dtpFechaAsignacion;
        
        #line default
        #line hidden
        
        
        #line 161 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCambiarPersonal;
        
        #line default
        #line hidden
        
        
        #line 165 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Expander expanderTerminacion;
        
        #line default
        #line hidden
        
        
        #line 167 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chcExpTerminacion;
        
        #line default
        #line hidden
        
        
        #line 169 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpTerminacion;
        
        #line default
        #line hidden
        
        
        #line 173 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DateTimePicker dtpFechaTerminacion;
        
        #line default
        #line hidden
        
        
        #line 177 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtObservaciones;
        
        #line default
        #line hidden
        
        
        #line 183 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Expander expanderEntrega;
        
        #line default
        #line hidden
        
        
        #line 185 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chcExpEntrega;
        
        #line default
        #line hidden
        
        
        #line 187 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpEntrega;
        
        #line default
        #line hidden
        
        
        #line 190 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlPersonal ctrPersonalRecibe;
        
        #line default
        #line hidden
        
        
        #line 191 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPersonalRecibe;
        
        #line default
        #line hidden
        
        
        #line 195 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.DateTimePicker dtpFechaEntrega;
        
        #line default
        #line hidden
        
        
        #line 219 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpActividadCheck;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Sistema CILTRA;component/procesosos/procesamientoordenservicio.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ctrFolioOrdenServicio = ((WpfClient.controlEnteros)(target));
            return;
            case 2:
            this.ctrUnidadTransporte = ((WpfClient.controlUnidadesTransporte)(target));
            return;
            case 3:
            this.txtTipoOrdenServicio = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.stpTipoServicio = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 5:
            this.txtTipoServicio = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.lvlOrdenesTrabajo = ((System.Windows.Controls.ListView)(target));
            return;
            case 7:
            this.scrollControles = ((System.Windows.Controls.ScrollViewer)(target));
            return;
            case 8:
            this.expanderRecepcion = ((System.Windows.Controls.Expander)(target));
            return;
            case 9:
            this.chcExpRecepcion = ((System.Windows.Controls.CheckBox)(target));
            
            #line 79 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
            this.chcExpRecepcion.Checked += new System.Windows.RoutedEventHandler(this.CheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 79 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
            this.chcExpRecepcion.Unchecked += new System.Windows.RoutedEventHandler(this.CheckBox_Checked);
            
            #line default
            #line hidden
            return;
            case 10:
            this.stpRecepcion = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 11:
            this.ctrPersonalEntrega = ((WpfClient.controlPersonal)(target));
            return;
            case 12:
            this.btnCambiarPersonalRecibe = ((System.Windows.Controls.Button)(target));
            
            #line 85 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
            this.btnCambiarPersonalRecibe.Click += new System.Windows.RoutedEventHandler(this.btnCambiarPersonalRecibe_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.dtpFechaRecepcion = ((Xceed.Wpf.Toolkit.DateTimePicker)(target));
            return;
            case 14:
            this.expanderActividades = ((System.Windows.Controls.Expander)(target));
            return;
            case 15:
            this.chcExpActividades = ((System.Windows.Controls.CheckBox)(target));
            
            #line 96 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
            this.chcExpActividades.Checked += new System.Windows.RoutedEventHandler(this.CheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 96 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
            this.chcExpActividades.Unchecked += new System.Windows.RoutedEventHandler(this.CheckBox_Checked);
            
            #line default
            #line hidden
            return;
            case 16:
            this.stpActividades = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 17:
            this.txtBuscadorActividad = ((System.Windows.Controls.TextBox)(target));
            
            #line 102 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
            this.txtBuscadorActividad.KeyDown += new System.Windows.Input.KeyEventHandler(this.txtBuscadorActividad_KeyDown);
            
            #line default
            #line hidden
            return;
            case 18:
            this.stpControlesPosicion = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 19:
            this.cbxChcPosLlantas = ((Xceed.Wpf.Toolkit.CheckComboBox)(target));
            return;
            case 20:
            this.chcTodosPosiciones = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 21:
            this.btnAddActividad = ((System.Windows.Controls.Button)(target));
            
            #line 108 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
            this.btnAddActividad.Click += new System.Windows.RoutedEventHandler(this.btnAddActividad_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            this.stpAtividadesOrdenServ = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 23:
            this.expanderAsignacion = ((System.Windows.Controls.Expander)(target));
            return;
            case 24:
            this.chcExpAsignacion = ((System.Windows.Controls.CheckBox)(target));
            
            #line 131 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
            this.chcExpAsignacion.Checked += new System.Windows.RoutedEventHandler(this.CheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 131 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
            this.chcExpAsignacion.Unchecked += new System.Windows.RoutedEventHandler(this.CheckBox_Checked);
            
            #line default
            #line hidden
            return;
            case 25:
            this.stpAsignacion = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 26:
            this.chcResponsable = ((System.Windows.Controls.Label)(target));
            return;
            case 27:
            this.ctrResponsable = ((WpfClient.controlPersonal)(target));
            return;
            case 28:
            this.btnCambiarResponsable = ((System.Windows.Controls.Button)(target));
            
            #line 139 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
            this.btnCambiarResponsable.Click += new System.Windows.RoutedEventHandler(this.BtnCambiarResponsable_Click);
            
            #line default
            #line hidden
            return;
            case 29:
            this.chcAuxiliar1 = ((System.Windows.Controls.Label)(target));
            return;
            case 30:
            this.ctrAuxiliar1 = ((WpfClient.controlPersonal)(target));
            return;
            case 31:
            this.btnCambiarA = ((System.Windows.Controls.Button)(target));
            
            #line 144 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
            this.btnCambiarA.Click += new System.Windows.RoutedEventHandler(this.BtnCambiarA_Click);
            
            #line default
            #line hidden
            return;
            case 32:
            this.chcAuxiliar2 = ((System.Windows.Controls.Label)(target));
            return;
            case 33:
            this.ctrAuxiliar2 = ((WpfClient.controlPersonal)(target));
            return;
            case 34:
            this.btnCambiarAuxiliar2 = ((System.Windows.Controls.Button)(target));
            
            #line 152 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
            this.btnCambiarAuxiliar2.Click += new System.Windows.RoutedEventHandler(this.BtnCambiarAuxiliar2_Click);
            
            #line default
            #line hidden
            return;
            case 35:
            this.dtpFechaAsignacion = ((Xceed.Wpf.Toolkit.DateTimePicker)(target));
            return;
            case 36:
            this.btnCambiarPersonal = ((System.Windows.Controls.Button)(target));
            
            #line 161 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
            this.btnCambiarPersonal.Click += new System.Windows.RoutedEventHandler(this.btnCambiarPersonal_Click);
            
            #line default
            #line hidden
            return;
            case 37:
            this.expanderTerminacion = ((System.Windows.Controls.Expander)(target));
            return;
            case 38:
            this.chcExpTerminacion = ((System.Windows.Controls.CheckBox)(target));
            
            #line 167 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
            this.chcExpTerminacion.Checked += new System.Windows.RoutedEventHandler(this.CheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 167 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
            this.chcExpTerminacion.Unchecked += new System.Windows.RoutedEventHandler(this.CheckBox_Checked);
            
            #line default
            #line hidden
            return;
            case 39:
            this.stpTerminacion = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 40:
            this.dtpFechaTerminacion = ((Xceed.Wpf.Toolkit.DateTimePicker)(target));
            return;
            case 41:
            this.txtObservaciones = ((System.Windows.Controls.TextBox)(target));
            return;
            case 42:
            this.expanderEntrega = ((System.Windows.Controls.Expander)(target));
            return;
            case 43:
            this.chcExpEntrega = ((System.Windows.Controls.CheckBox)(target));
            
            #line 185 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
            this.chcExpEntrega.Checked += new System.Windows.RoutedEventHandler(this.CheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 185 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
            this.chcExpEntrega.Unchecked += new System.Windows.RoutedEventHandler(this.CheckBox_Checked);
            
            #line default
            #line hidden
            return;
            case 44:
            this.stpEntrega = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 45:
            this.ctrPersonalRecibe = ((WpfClient.controlPersonal)(target));
            return;
            case 46:
            this.btnPersonalRecibe = ((System.Windows.Controls.Button)(target));
            
            #line 191 "..\..\..\ProcesosOS\ProcesamientoOrdenServicio.xaml"
            this.btnPersonalRecibe.Click += new System.Windows.RoutedEventHandler(this.btnPersonalRecibe_Click);
            
            #line default
            #line hidden
            return;
            case 47:
            this.dtpFechaEntrega = ((Xceed.Wpf.Toolkit.DateTimePicker)(target));
            return;
            case 48:
            this.stpActividadCheck = ((System.Windows.Controls.StackPanel)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

