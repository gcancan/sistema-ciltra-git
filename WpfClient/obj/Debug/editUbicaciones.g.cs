﻿#pragma checksum "..\..\editUbicaciones.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "3FC8B8453A547D6C125E592685BD05E9740B643944E24DC373076E430B40E3E4"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfClient;


namespace WpfClient {
    
    
    /// <summary>
    /// editUbicaciones
    /// </summary>
    public partial class editUbicaciones : WpfClient.ViewBase, System.Windows.Markup.IComponentConnector {
        
        
        #line 19 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnExportar;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpControlesPais;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlEnteros ctrClavePais;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNombrePais;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNuevoPais;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGuardarPais;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpControlesEstado;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlEnteros ctrClaveEstado;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxPais;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNombreEstado;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNuevoEstado;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGuardarEstado;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpControlesCiudad;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlEnteros ctrCleveCiudad;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxEstado;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNombreCiudad;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBuevoCiudad;
        
        #line default
        #line hidden
        
        
        #line 97 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGuardarCiudad;
        
        #line default
        #line hidden
        
        
        #line 105 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpControlesColonia;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlEnteros ctrClaveColonia;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxEstado2;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxCiudad;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNombreColonia;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlEnteros ctrCP;
        
        #line default
        #line hidden
        
        
        #line 135 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNuevoColonia;
        
        #line default
        #line hidden
        
        
        #line 136 "..\..\editUbicaciones.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNuevoGuardar;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Sistema CILTRA;component/editubicaciones.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\editUbicaciones.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.btnExportar = ((System.Windows.Controls.Button)(target));
            
            #line 19 "..\..\editUbicaciones.xaml"
            this.btnExportar.Click += new System.Windows.RoutedEventHandler(this.BtnExportar_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.stpControlesPais = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 3:
            this.ctrClavePais = ((WpfClient.controlEnteros)(target));
            return;
            case 4:
            this.txtNombrePais = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.btnNuevoPais = ((System.Windows.Controls.Button)(target));
            
            #line 38 "..\..\editUbicaciones.xaml"
            this.btnNuevoPais.Click += new System.Windows.RoutedEventHandler(this.BtnNuevoPais_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.btnGuardarPais = ((System.Windows.Controls.Button)(target));
            
            #line 39 "..\..\editUbicaciones.xaml"
            this.btnGuardarPais.Click += new System.Windows.RoutedEventHandler(this.BtnGuardarPais_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.stpControlesEstado = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 8:
            this.ctrClaveEstado = ((WpfClient.controlEnteros)(target));
            return;
            case 9:
            this.cbxPais = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 10:
            this.txtNombreEstado = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.btnNuevoEstado = ((System.Windows.Controls.Button)(target));
            
            #line 67 "..\..\editUbicaciones.xaml"
            this.btnNuevoEstado.Click += new System.Windows.RoutedEventHandler(this.BtnNuevoEstado_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.btnGuardarEstado = ((System.Windows.Controls.Button)(target));
            
            #line 68 "..\..\editUbicaciones.xaml"
            this.btnGuardarEstado.Click += new System.Windows.RoutedEventHandler(this.BtnGuardarEstado_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.stpControlesCiudad = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 14:
            this.ctrCleveCiudad = ((WpfClient.controlEnteros)(target));
            return;
            case 15:
            this.cbxEstado = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 16:
            this.txtNombreCiudad = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.btnBuevoCiudad = ((System.Windows.Controls.Button)(target));
            
            #line 96 "..\..\editUbicaciones.xaml"
            this.btnBuevoCiudad.Click += new System.Windows.RoutedEventHandler(this.BtnBuevoCiudad_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.btnGuardarCiudad = ((System.Windows.Controls.Button)(target));
            
            #line 97 "..\..\editUbicaciones.xaml"
            this.btnGuardarCiudad.Click += new System.Windows.RoutedEventHandler(this.BtnGuardarCiudad_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.stpControlesColonia = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 20:
            this.ctrClaveColonia = ((WpfClient.controlEnteros)(target));
            return;
            case 21:
            this.cbxEstado2 = ((System.Windows.Controls.ComboBox)(target));
            
            #line 113 "..\..\editUbicaciones.xaml"
            this.cbxEstado2.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.CbxEstado2_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 22:
            this.cbxCiudad = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 23:
            this.txtNombreColonia = ((System.Windows.Controls.TextBox)(target));
            return;
            case 24:
            this.ctrCP = ((WpfClient.controlEnteros)(target));
            return;
            case 25:
            this.btnNuevoColonia = ((System.Windows.Controls.Button)(target));
            
            #line 135 "..\..\editUbicaciones.xaml"
            this.btnNuevoColonia.Click += new System.Windows.RoutedEventHandler(this.BtnNuevoColonia_Click);
            
            #line default
            #line hidden
            return;
            case 26:
            this.btnNuevoGuardar = ((System.Windows.Controls.Button)(target));
            
            #line 136 "..\..\editUbicaciones.xaml"
            this.btnNuevoGuardar.Click += new System.Windows.RoutedEventHandler(this.BtnNuevoGuardar_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

