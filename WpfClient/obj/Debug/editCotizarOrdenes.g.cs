﻿#pragma checksum "..\..\editCotizarOrdenes.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "9F376BC86BD8DEB945752BFBE2B02269C87A2AECACC7599DF45D3A27C64CDC28"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfClient;


namespace WpfClient {
    
    
    /// <summary>
    /// editCotizarOrdenes
    /// </summary>
    public partial class editCotizarOrdenes : WpfClient.ViewBase, System.Windows.Markup.IComponentConnector {
        
        
        #line 19 "..\..\editCotizarOrdenes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtFolio;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\editCotizarOrdenes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlEmpresas ctrEmpresa;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\editCotizarOrdenes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAñadirOrdenes;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\editCotizarOrdenes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvlPreOrd;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\editCotizarOrdenes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ScrollViewer scrollPreOrdenes;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\editCotizarOrdenes.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpPreOrdenes;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Sistema CILTRA;component/editcotizarordenes.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\editCotizarOrdenes.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.txtFolio = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.ctrEmpresa = ((WpfClient.controlEmpresas)(target));
            return;
            case 3:
            this.btnAñadirOrdenes = ((System.Windows.Controls.Button)(target));
            
            #line 32 "..\..\editCotizarOrdenes.xaml"
            this.btnAñadirOrdenes.Click += new System.Windows.RoutedEventHandler(this.btnAñadirOrdenes_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.lvlPreOrd = ((System.Windows.Controls.ListView)(target));
            return;
            case 5:
            this.scrollPreOrdenes = ((System.Windows.Controls.ScrollViewer)(target));
            return;
            case 6:
            this.stpPreOrdenes = ((System.Windows.Controls.StackPanel)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

