﻿#pragma checksum "..\..\editLiquidacionPegaso.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "CD709B189D5D03204E0203C65440EAD67527E5B7528E818535483E6C7EC4AE3F"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfClient;


namespace WpfClient {
    
    
    /// <summary>
    /// editLiquidacionPegaso
    /// </summary>
    public partial class editLiquidacionPegaso : WpfClient.ViewBase, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 18 "..\..\editLiquidacionPegaso.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtclave;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\editLiquidacionPegaso.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GroupBox gBoxBucador;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\editLiquidacionPegaso.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlFechas ctrFechas;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\editLiquidacionPegaso.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlEmpresas ctrEmpresa;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\editLiquidacionPegaso.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlPersonal ctrOperador;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\editLiquidacionPegaso.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBuscar;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\editLiquidacionPegaso.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSueldoFijo;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\editLiquidacionPegaso.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSumaImportes;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\editLiquidacionPegaso.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSegregacion;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\editLiquidacionPegaso.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSumaExtras;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\editLiquidacionPegaso.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtTotalPagas;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\editLiquidacionPegaso.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvlSalidas;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\editLiquidacionPegaso.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvlModalidades;
        
        #line default
        #line hidden
        
        
        #line 137 "..\..\editLiquidacionPegaso.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlDecimal txtRescates;
        
        #line default
        #line hidden
        
        
        #line 141 "..\..\editLiquidacionPegaso.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlDecimal txtViajes60;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Sistema CILTRA;component/editliquidacionpegaso.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\editLiquidacionPegaso.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.txtclave = ((System.Windows.Controls.TextBox)(target));
            
            #line 18 "..\..\editLiquidacionPegaso.xaml"
            this.txtclave.KeyDown += new System.Windows.Input.KeyEventHandler(this.txtclave_KeyDown);
            
            #line default
            #line hidden
            return;
            case 2:
            this.gBoxBucador = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 3:
            this.ctrFechas = ((WpfClient.controlFechas)(target));
            return;
            case 4:
            this.ctrEmpresa = ((WpfClient.controlEmpresas)(target));
            return;
            case 5:
            this.ctrOperador = ((WpfClient.controlPersonal)(target));
            return;
            case 6:
            this.btnBuscar = ((System.Windows.Controls.Button)(target));
            
            #line 34 "..\..\editLiquidacionPegaso.xaml"
            this.btnBuscar.Click += new System.Windows.RoutedEventHandler(this.btnBuscar_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.txtSueldoFijo = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.txtSumaImportes = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.txtSegregacion = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.txtSumaExtras = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.txtTotalPagas = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.lvlSalidas = ((System.Windows.Controls.ListView)(target));
            return;
            case 13:
            this.lvlModalidades = ((System.Windows.Controls.ListView)(target));
            return;
            case 16:
            this.txtRescates = ((WpfClient.controlDecimal)(target));
            return;
            case 17:
            this.txtViajes60 = ((WpfClient.controlDecimal)(target));
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 14:
            
            #line 105 "..\..\editLiquidacionPegaso.xaml"
            ((System.Windows.Controls.CheckBox)(target)).Checked += new System.Windows.RoutedEventHandler(this.CheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 105 "..\..\editLiquidacionPegaso.xaml"
            ((System.Windows.Controls.CheckBox)(target)).Unchecked += new System.Windows.RoutedEventHandler(this.CheckBox_Unchecked);
            
            #line default
            #line hidden
            break;
            case 15:
            
            #line 117 "..\..\editLiquidacionPegaso.xaml"
            ((System.Windows.Controls.CheckBox)(target)).Checked += new System.Windows.RoutedEventHandler(this.CheckBox_Checked);
            
            #line default
            #line hidden
            
            #line 117 "..\..\editLiquidacionPegaso.xaml"
            ((System.Windows.Controls.CheckBox)(target)).Unchecked += new System.Windows.RoutedEventHandler(this.CheckBox_Unchecked);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

