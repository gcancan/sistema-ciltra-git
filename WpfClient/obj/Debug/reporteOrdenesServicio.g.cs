﻿#pragma checksum "..\..\reporteOrdenesServicio.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "66B926D9EC0D4CE450089321C72DFEE9AD5C4713C52E6E717C0BA8DF19306FFC"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfClient;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Chromes;
using Xceed.Wpf.Toolkit.Core.Converters;
using Xceed.Wpf.Toolkit.Core.Input;
using Xceed.Wpf.Toolkit.Core.Media;
using Xceed.Wpf.Toolkit.Core.Utilities;
using Xceed.Wpf.Toolkit.Panels;
using Xceed.Wpf.Toolkit.Primitives;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Commands;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Xceed.Wpf.Toolkit.Zoombox;


namespace WpfClient {
    
    
    /// <summary>
    /// reporteOrdenesServicio
    /// </summary>
    public partial class reporteOrdenesServicio : WpfClient.ViewBase, System.Windows.Markup.IComponentConnector {
        
        
        #line 21 "..\..\reporteOrdenesServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlEmpresas ctrEmpresa;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\reporteOrdenesServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlZonaOperativa ctrZonaOperativa;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\reporteOrdenesServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpOperacion;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\reporteOrdenesServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.CheckComboBox cbxOperacion;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\reporteOrdenesServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chcTodosOperaciones;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\reporteOrdenesServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.CheckComboBox cbxUnidades;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\reporteOrdenesServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chcTodosUnidades;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\reporteOrdenesServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlFechas ctrFechas;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\reporteOrdenesServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.CheckComboBox cbxTipoServicio;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\reporteOrdenesServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chcTodosTipoServicio;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\reporteOrdenesServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.CheckComboBox cbxTipoOrdenServicio;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\reporteOrdenesServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chcTodosTipoOrdServ;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\reporteOrdenesServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.CheckComboBox cbxEstatus;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\reporteOrdenesServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chcTodosEstatus;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\reporteOrdenesServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBuscar;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\reporteOrdenesServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnExportar;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\reporteOrdenesServicio.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvlOs;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Sistema CILTRA;component/reporteordenesservicio.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\reporteOrdenesServicio.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ctrEmpresa = ((WpfClient.controlEmpresas)(target));
            return;
            case 2:
            this.ctrZonaOperativa = ((WpfClient.controlZonaOperativa)(target));
            return;
            case 3:
            this.stpOperacion = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 4:
            this.cbxOperacion = ((Xceed.Wpf.Toolkit.CheckComboBox)(target));
            
            #line 31 "..\..\reporteOrdenesServicio.xaml"
            this.cbxOperacion.ItemSelectionChanged += new Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventHandler(this.CbxOperacion_ItemSelectionChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.chcTodosOperaciones = ((System.Windows.Controls.CheckBox)(target));
            
            #line 32 "..\..\reporteOrdenesServicio.xaml"
            this.chcTodosOperaciones.Checked += new System.Windows.RoutedEventHandler(this.ChcTodosOperaciones_Checked);
            
            #line default
            #line hidden
            
            #line 32 "..\..\reporteOrdenesServicio.xaml"
            this.chcTodosOperaciones.Unchecked += new System.Windows.RoutedEventHandler(this.ChcTodosOperaciones_Checked);
            
            #line default
            #line hidden
            return;
            case 6:
            this.cbxUnidades = ((Xceed.Wpf.Toolkit.CheckComboBox)(target));
            return;
            case 7:
            this.chcTodosUnidades = ((System.Windows.Controls.CheckBox)(target));
            
            #line 37 "..\..\reporteOrdenesServicio.xaml"
            this.chcTodosUnidades.Checked += new System.Windows.RoutedEventHandler(this.ChcTodosUnidades_Checked);
            
            #line default
            #line hidden
            
            #line 37 "..\..\reporteOrdenesServicio.xaml"
            this.chcTodosUnidades.Unchecked += new System.Windows.RoutedEventHandler(this.ChcTodosUnidades_Checked);
            
            #line default
            #line hidden
            return;
            case 8:
            this.ctrFechas = ((WpfClient.controlFechas)(target));
            return;
            case 9:
            this.cbxTipoServicio = ((Xceed.Wpf.Toolkit.CheckComboBox)(target));
            return;
            case 10:
            this.chcTodosTipoServicio = ((System.Windows.Controls.CheckBox)(target));
            
            #line 45 "..\..\reporteOrdenesServicio.xaml"
            this.chcTodosTipoServicio.Checked += new System.Windows.RoutedEventHandler(this.ChcTodosTipoServicio_Checked);
            
            #line default
            #line hidden
            
            #line 45 "..\..\reporteOrdenesServicio.xaml"
            this.chcTodosTipoServicio.Unchecked += new System.Windows.RoutedEventHandler(this.ChcTodosTipoServicio_Checked);
            
            #line default
            #line hidden
            return;
            case 11:
            this.cbxTipoOrdenServicio = ((Xceed.Wpf.Toolkit.CheckComboBox)(target));
            return;
            case 12:
            this.chcTodosTipoOrdServ = ((System.Windows.Controls.CheckBox)(target));
            
            #line 50 "..\..\reporteOrdenesServicio.xaml"
            this.chcTodosTipoOrdServ.Checked += new System.Windows.RoutedEventHandler(this.ChcTodosTipoOrdServ_Checked);
            
            #line default
            #line hidden
            
            #line 50 "..\..\reporteOrdenesServicio.xaml"
            this.chcTodosTipoOrdServ.Unchecked += new System.Windows.RoutedEventHandler(this.ChcTodosTipoOrdServ_Checked);
            
            #line default
            #line hidden
            return;
            case 13:
            this.cbxEstatus = ((Xceed.Wpf.Toolkit.CheckComboBox)(target));
            return;
            case 14:
            this.chcTodosEstatus = ((System.Windows.Controls.CheckBox)(target));
            
            #line 55 "..\..\reporteOrdenesServicio.xaml"
            this.chcTodosEstatus.Checked += new System.Windows.RoutedEventHandler(this.ChcTodosEstatus_Checked);
            
            #line default
            #line hidden
            
            #line 55 "..\..\reporteOrdenesServicio.xaml"
            this.chcTodosEstatus.Unchecked += new System.Windows.RoutedEventHandler(this.ChcTodosEstatus_Checked);
            
            #line default
            #line hidden
            return;
            case 15:
            this.btnBuscar = ((System.Windows.Controls.Button)(target));
            
            #line 58 "..\..\reporteOrdenesServicio.xaml"
            this.btnBuscar.Click += new System.Windows.RoutedEventHandler(this.BtnBuscar_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.btnExportar = ((System.Windows.Controls.Button)(target));
            
            #line 59 "..\..\reporteOrdenesServicio.xaml"
            this.btnExportar.Click += new System.Windows.RoutedEventHandler(this.BtnExportar_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.lvlOs = ((System.Windows.Controls.ListView)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

