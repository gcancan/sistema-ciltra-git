﻿#pragma checksum "..\..\ReportesPersonalizados.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "CFF27D4D59421857C1DABD4C1D621B0854F007EE0796B36029262D5114F77F84"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfClient;


namespace WpfClient {
    
    
    /// <summary>
    /// ReportesPersonalizados
    /// </summary>
    public partial class ReportesPersonalizados : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 36 "..\..\ReportesPersonalizados.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxPerfiles;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\ReportesPersonalizados.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNamePerfil;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\ReportesPersonalizados.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chxFecha;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\ReportesPersonalizados.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chxHora;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\ReportesPersonalizados.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chxOrden;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\ReportesPersonalizados.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chxTC;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\ReportesPersonalizados.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chxTolva1;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\ReportesPersonalizados.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chxTolvar2;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\ReportesPersonalizados.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chxOperador;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\ReportesPersonalizados.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chxDestino;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\ReportesPersonalizados.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chxKM;
        
        #line default
        #line hidden
        
        
        #line 75 "..\..\ReportesPersonalizados.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chxProducto;
        
        #line default
        #line hidden
        
        
        #line 76 "..\..\ReportesPersonalizados.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chxToneladas;
        
        #line default
        #line hidden
        
        
        #line 77 "..\..\ReportesPersonalizados.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chxPrecio;
        
        #line default
        #line hidden
        
        
        #line 79 "..\..\ReportesPersonalizados.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chxImporte;
        
        #line default
        #line hidden
        
        
        #line 80 "..\..\ReportesPersonalizados.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chxIvas;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\ReportesPersonalizados.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chxRetencion;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\ReportesPersonalizados.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chxTotal;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\ReportesPersonalizados.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Generar;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Sistema CILTRA;component/reportespersonalizados.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\ReportesPersonalizados.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 10 "..\..\ReportesPersonalizados.xaml"
            ((WpfClient.ReportesPersonalizados)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.cbxPerfiles = ((System.Windows.Controls.ComboBox)(target));
            
            #line 36 "..\..\ReportesPersonalizados.xaml"
            this.cbxPerfiles.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.cbxPerfiles_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.txtNamePerfil = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            
            #line 41 "..\..\ReportesPersonalizados.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.chxFecha = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 6:
            this.chxHora = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 7:
            this.chxOrden = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 8:
            this.chxTC = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 9:
            this.chxTolva1 = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 10:
            this.chxTolvar2 = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 11:
            this.chxOperador = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 12:
            this.chxDestino = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 13:
            this.chxKM = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 14:
            this.chxProducto = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 15:
            this.chxToneladas = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 16:
            this.chxPrecio = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 17:
            this.chxImporte = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 18:
            this.chxIvas = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 19:
            this.chxRetencion = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 20:
            this.chxTotal = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 21:
            this.Generar = ((System.Windows.Controls.Button)(target));
            
            #line 90 "..\..\ReportesPersonalizados.xaml"
            this.Generar.Click += new System.Windows.RoutedEventHandler(this.Generar_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

