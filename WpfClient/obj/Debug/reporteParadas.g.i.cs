﻿#pragma checksum "..\..\reporteParadas.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "9250F8725306417669A5B1197FE31EAFE148076D77E0045C950BE9E3B67E56BC"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfClient;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Chromes;
using Xceed.Wpf.Toolkit.Core.Converters;
using Xceed.Wpf.Toolkit.Core.Input;
using Xceed.Wpf.Toolkit.Core.Media;
using Xceed.Wpf.Toolkit.Core.Utilities;
using Xceed.Wpf.Toolkit.Panels;
using Xceed.Wpf.Toolkit.Primitives;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using Xceed.Wpf.Toolkit.PropertyGrid.Commands;
using Xceed.Wpf.Toolkit.PropertyGrid.Converters;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;
using Xceed.Wpf.Toolkit.Zoombox;


namespace WpfClient {
    
    
    /// <summary>
    /// reporteParadas
    /// </summary>
    public partial class reporteParadas : WpfClient.ViewBase, System.Windows.Markup.IComponentConnector {
        
        
        #line 20 "..\..\reporteParadas.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlFechas ctrFechas;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\reporteParadas.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxCuenta;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\reporteParadas.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnConsultar;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\reporteParadas.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnExportar;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\reporteParadas.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.CheckComboBox cbxTractores;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\reporteParadas.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chcTodosTractores;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\reporteParadas.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.CheckComboBox cbxEstatus;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\reporteParadas.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chcTodosEstatus;
        
        #line default
        #line hidden
        
        
        #line 43 "..\..\reporteParadas.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.CheckComboBox cbxGeoCercas;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\reporteParadas.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chcTodosGeoCercas;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\reporteParadas.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.MaskedTextBox txtTiempoMinimo;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\reporteParadas.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Xceed.Wpf.Toolkit.MaskedTextBox txtTiempoMaximo;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\reporteParadas.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvlParadas;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Sistema CILTRA;component/reporteparadas.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\reporteParadas.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ctrFechas = ((WpfClient.controlFechas)(target));
            return;
            case 2:
            this.cbxCuenta = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 3:
            this.btnConsultar = ((System.Windows.Controls.Button)(target));
            
            #line 26 "..\..\reporteParadas.xaml"
            this.btnConsultar.Click += new System.Windows.RoutedEventHandler(this.btnConsultar_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btnExportar = ((System.Windows.Controls.Button)(target));
            
            #line 27 "..\..\reporteParadas.xaml"
            this.btnExportar.Click += new System.Windows.RoutedEventHandler(this.btnExportar_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.cbxTractores = ((Xceed.Wpf.Toolkit.CheckComboBox)(target));
            
            #line 33 "..\..\reporteParadas.xaml"
            this.cbxTractores.ItemSelectionChanged += new Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventHandler(this.cbx_ItemSelectionChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.chcTodosTractores = ((System.Windows.Controls.CheckBox)(target));
            
            #line 34 "..\..\reporteParadas.xaml"
            this.chcTodosTractores.Checked += new System.Windows.RoutedEventHandler(this.chc_Checked);
            
            #line default
            #line hidden
            
            #line 34 "..\..\reporteParadas.xaml"
            this.chcTodosTractores.Unchecked += new System.Windows.RoutedEventHandler(this.chc_Checked);
            
            #line default
            #line hidden
            return;
            case 7:
            this.cbxEstatus = ((Xceed.Wpf.Toolkit.CheckComboBox)(target));
            
            #line 38 "..\..\reporteParadas.xaml"
            this.cbxEstatus.ItemSelectionChanged += new Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventHandler(this.cbx_ItemSelectionChanged);
            
            #line default
            #line hidden
            return;
            case 8:
            this.chcTodosEstatus = ((System.Windows.Controls.CheckBox)(target));
            
            #line 39 "..\..\reporteParadas.xaml"
            this.chcTodosEstatus.Checked += new System.Windows.RoutedEventHandler(this.chc_Checked);
            
            #line default
            #line hidden
            
            #line 39 "..\..\reporteParadas.xaml"
            this.chcTodosEstatus.Unchecked += new System.Windows.RoutedEventHandler(this.chc_Checked);
            
            #line default
            #line hidden
            return;
            case 9:
            this.cbxGeoCercas = ((Xceed.Wpf.Toolkit.CheckComboBox)(target));
            
            #line 43 "..\..\reporteParadas.xaml"
            this.cbxGeoCercas.ItemSelectionChanged += new Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventHandler(this.cbx_ItemSelectionChanged);
            
            #line default
            #line hidden
            return;
            case 10:
            this.chcTodosGeoCercas = ((System.Windows.Controls.CheckBox)(target));
            
            #line 44 "..\..\reporteParadas.xaml"
            this.chcTodosGeoCercas.Checked += new System.Windows.RoutedEventHandler(this.chc_Checked);
            
            #line default
            #line hidden
            
            #line 44 "..\..\reporteParadas.xaml"
            this.chcTodosGeoCercas.Unchecked += new System.Windows.RoutedEventHandler(this.chc_Checked);
            
            #line default
            #line hidden
            return;
            case 11:
            this.txtTiempoMinimo = ((Xceed.Wpf.Toolkit.MaskedTextBox)(target));
            
            #line 52 "..\..\reporteParadas.xaml"
            this.txtTiempoMinimo.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_TextChanged);
            
            #line default
            #line hidden
            return;
            case 12:
            this.txtTiempoMaximo = ((Xceed.Wpf.Toolkit.MaskedTextBox)(target));
            
            #line 56 "..\..\reporteParadas.xaml"
            this.txtTiempoMaximo.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.txt_TextChanged);
            
            #line default
            #line hidden
            return;
            case 13:
            this.lvlParadas = ((System.Windows.Controls.ListView)(target));
            
            #line 67 "..\..\reporteParadas.xaml"
            this.lvlParadas.MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(this.lvlParadas_MouseDoubleClick);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

