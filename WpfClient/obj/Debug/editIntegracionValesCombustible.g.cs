﻿#pragma checksum "..\..\editIntegracionValesCombustible.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "9184F3348775433C4E5DFA26743706C7485BDCC0176822B94084AE56A0C6A261"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfClient;


namespace WpfClient {
    
    
    /// <summary>
    /// editIntegracionValesCombustible
    /// </summary>
    public partial class editIntegracionValesCombustible : WpfClient.ViewBase, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 18 "..\..\editIntegracionValesCombustible.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlEnteros ctrVale;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\editIntegracionValesCombustible.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtFecha;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\editIntegracionValesCombustible.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlDecimal ctrLitros;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\editIntegracionValesCombustible.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlDecimal ctrTotalLitros;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\editIntegracionValesCombustible.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlUnidadesTransporte ctrUnidad;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\editIntegracionValesCombustible.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlPersonal ctrOperador;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\editIntegracionValesCombustible.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlPersonal ctrDespachadora;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\editIntegracionValesCombustible.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBuscarCargasExtra;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\editIntegracionValesCombustible.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlDecimal ctrSubTotalExtra;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\editIntegracionValesCombustible.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvlCargasExtra;
        
        #line default
        #line hidden
        
        
        #line 104 "..\..\editIntegracionValesCombustible.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnBuscarTickets;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\editIntegracionValesCombustible.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal WpfClient.controlDecimal ctrSubTotalTickets;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\editIntegracionValesCombustible.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvlTickets;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Sistema CILTRA;component/editintegracionvalescombustible.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\editIntegracionValesCombustible.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ctrVale = ((WpfClient.controlEnteros)(target));
            return;
            case 2:
            this.txtFecha = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.ctrLitros = ((WpfClient.controlDecimal)(target));
            return;
            case 4:
            this.ctrTotalLitros = ((WpfClient.controlDecimal)(target));
            return;
            case 5:
            this.ctrUnidad = ((WpfClient.controlUnidadesTransporte)(target));
            return;
            case 6:
            this.ctrOperador = ((WpfClient.controlPersonal)(target));
            return;
            case 7:
            this.ctrDespachadora = ((WpfClient.controlPersonal)(target));
            return;
            case 8:
            this.btnBuscarCargasExtra = ((System.Windows.Controls.Button)(target));
            
            #line 58 "..\..\editIntegracionValesCombustible.xaml"
            this.btnBuscarCargasExtra.Click += new System.Windows.RoutedEventHandler(this.BtnBuscarCargasExtra_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.ctrSubTotalExtra = ((WpfClient.controlDecimal)(target));
            return;
            case 10:
            this.lvlCargasExtra = ((System.Windows.Controls.ListView)(target));
            return;
            case 12:
            this.btnBuscarTickets = ((System.Windows.Controls.Button)(target));
            
            #line 104 "..\..\editIntegracionValesCombustible.xaml"
            this.btnBuscarTickets.Click += new System.Windows.RoutedEventHandler(this.BtnBuscarTickets_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.ctrSubTotalTickets = ((WpfClient.controlDecimal)(target));
            return;
            case 14:
            this.lvlTickets = ((System.Windows.Controls.ListView)(target));
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 11:
            
            #line 73 "..\..\editIntegracionValesCombustible.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.BtnQuitarCargaExtra_Click);
            
            #line default
            #line hidden
            break;
            case 15:
            
            #line 119 "..\..\editIntegracionValesCombustible.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.BtnQuitarTicket_Click);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

