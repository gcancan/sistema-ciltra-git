﻿#pragma checksum "..\..\..\editConfig.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "D5D4FEA5E368F476FB936353E6AD66EF"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfClient;


namespace WpfClient {
    
    
    /// <summary>
    /// editConfig
    /// </summary>
    public partial class editConfig : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 14 "..\..\..\editConfig.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridPrincipal;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\editConfig.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNameServer;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\editConfig.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNameDB;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\editConfig.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNameUser;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\..\editConfig.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox txtPass;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\editConfig.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnProbarConexion;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\..\editConfig.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNameServerComercial;
        
        #line default
        #line hidden
        
        
        #line 83 "..\..\..\editConfig.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNameDBComercial;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\..\editConfig.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNameUserComercial;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\..\editConfig.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox txtPassComercial;
        
        #line default
        #line hidden
        
        
        #line 97 "..\..\..\editConfig.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnProbarConexionComercial;
        
        #line default
        #line hidden
        
        
        #line 119 "..\..\..\editConfig.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxPuertos;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\..\editConfig.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBaudRate;
        
        #line default
        #line hidden
        
        
        #line 129 "..\..\..\editConfig.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxDataBits;
        
        #line default
        #line hidden
        
        
        #line 133 "..\..\..\editConfig.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxTiempo;
        
        #line default
        #line hidden
        
        
        #line 136 "..\..\..\editConfig.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnProbarConexionPuerto;
        
        #line default
        #line hidden
        
        
        #line 142 "..\..\..\editConfig.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAceptar;
        
        #line default
        #line hidden
        
        
        #line 143 "..\..\..\editConfig.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCancelar;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Salida Viajes;component/editconfig.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\editConfig.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 12 "..\..\..\editConfig.xaml"
            ((WpfClient.editConfig)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            
            #line 12 "..\..\..\editConfig.xaml"
            ((WpfClient.editConfig)(target)).KeyDown += new System.Windows.Input.KeyEventHandler(this.Window_KeyDown);
            
            #line default
            #line hidden
            return;
            case 2:
            this.gridPrincipal = ((System.Windows.Controls.Grid)(target));
            return;
            case 3:
            this.txtNameServer = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.txtNameDB = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.txtNameUser = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.txtPass = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 7:
            this.btnProbarConexion = ((System.Windows.Controls.Button)(target));
            
            #line 61 "..\..\..\editConfig.xaml"
            this.btnProbarConexion.Click += new System.Windows.RoutedEventHandler(this.btnProbarConexion_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.txtNameServerComercial = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.txtNameDBComercial = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.txtNameUserComercial = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.txtPassComercial = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 12:
            this.btnProbarConexionComercial = ((System.Windows.Controls.Button)(target));
            
            #line 97 "..\..\..\editConfig.xaml"
            this.btnProbarConexionComercial.Click += new System.Windows.RoutedEventHandler(this.btnProbarConexionComercial_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.cbxPuertos = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 14:
            this.txtBaudRate = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.cbxDataBits = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 16:
            this.cbxTiempo = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 17:
            this.btnProbarConexionPuerto = ((System.Windows.Controls.Button)(target));
            
            #line 136 "..\..\..\editConfig.xaml"
            this.btnProbarConexionPuerto.Click += new System.Windows.RoutedEventHandler(this.btnProbarConexionPuerto_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.btnAceptar = ((System.Windows.Controls.Button)(target));
            
            #line 142 "..\..\..\editConfig.xaml"
            this.btnAceptar.Click += new System.Windows.RoutedEventHandler(this.btnAceptar_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.btnCancelar = ((System.Windows.Controls.Button)(target));
            
            #line 143 "..\..\..\editConfig.xaml"
            this.btnCancelar.Click += new System.Windows.RoutedEventHandler(this.btnCancelar_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

