﻿#pragma checksum "..\..\..\EditCartaPorte.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "0D8E2E73E24392397B7A2DF3CD3C4517"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfClient;


namespace WpfClient {
    
    
    /// <summary>
    /// EditCartaPorte
    /// </summary>
    public partial class EditCartaPorte : WpfClient.ViewBase, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 11 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid controles;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtFolio;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtFecha;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNumViaje;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxSucursal;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxClientes;
        
        #line default
        #line hidden
        
        
        #line 82 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxCamion;
        
        #line default
        #line hidden
        
        
        #line 97 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxTolva1;
        
        #line default
        #line hidden
        
        
        #line 112 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxDolly;
        
        #line default
        #line hidden
        
        
        #line 126 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxTolva2;
        
        #line default
        #line hidden
        
        
        #line 143 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbxChofer;
        
        #line default
        #line hidden
        
        
        #line 159 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtFraccion;
        
        #line default
        #line hidden
        
        
        #line 180 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtRetencion;
        
        #line default
        #line hidden
        
        
        #line 185 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtFlete;
        
        #line default
        #line hidden
        
        
        #line 199 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtCargaTotal;
        
        #line default
        #line hidden
        
        
        #line 204 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSumValProgramados;
        
        #line default
        #line hidden
        
        
        #line 222 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtBusquedaRemision;
        
        #line default
        #line hidden
        
        
        #line 234 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvlProductos;
        
        #line default
        #line hidden
        
        
        #line 289 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnNewDetalle;
        
        #line default
        #line hidden
        
        
        #line 295 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCalcular;
        
        #line default
        #line hidden
        
        
        #line 301 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAddRemision;
        
        #line default
        #line hidden
        
        
        #line 307 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnQuitar;
        
        #line default
        #line hidden
        
        
        #line 318 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtTotalImporte;
        
        #line default
        #line hidden
        
        
        #line 323 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtTotalRetencion;
        
        #line default
        #line hidden
        
        
        #line 328 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtTotalFlete;
        
        #line default
        #line hidden
        
        
        #line 333 "..\..\..\EditCartaPorte.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSumaTotal;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Salida Viajes;component/editcartaporte.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\EditCartaPorte.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.controles = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.txtFolio = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.txtFecha = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.txtNumViaje = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.cbxSucursal = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 6:
            this.cbxClientes = ((System.Windows.Controls.ComboBox)(target));
            
            #line 66 "..\..\..\EditCartaPorte.xaml"
            this.cbxClientes.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.cbxClientes_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 7:
            this.cbxCamion = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 8:
            this.cbxTolva1 = ((System.Windows.Controls.ComboBox)(target));
            
            #line 97 "..\..\..\EditCartaPorte.xaml"
            this.cbxTolva1.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.cbxTolva1_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 9:
            this.cbxDolly = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 10:
            this.cbxTolva2 = ((System.Windows.Controls.ComboBox)(target));
            
            #line 126 "..\..\..\EditCartaPorte.xaml"
            this.cbxTolva2.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.cbxTolva2_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 11:
            this.cbxChofer = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 12:
            this.txtFraccion = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.txtRetencion = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.txtFlete = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            this.txtCargaTotal = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.txtSumValProgramados = ((System.Windows.Controls.TextBox)(target));
            return;
            case 17:
            this.txtBusquedaRemision = ((System.Windows.Controls.TextBox)(target));
            
            #line 222 "..\..\..\EditCartaPorte.xaml"
            this.txtBusquedaRemision.KeyDown += new System.Windows.Input.KeyEventHandler(this.txtBusquedaRemision_KeyDown);
            
            #line default
            #line hidden
            return;
            case 18:
            this.lvlProductos = ((System.Windows.Controls.ListView)(target));
            return;
            case 21:
            this.btnNewDetalle = ((System.Windows.Controls.Button)(target));
            
            #line 289 "..\..\..\EditCartaPorte.xaml"
            this.btnNewDetalle.Click += new System.Windows.RoutedEventHandler(this.btnNewDetalle_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            this.btnCalcular = ((System.Windows.Controls.Button)(target));
            
            #line 295 "..\..\..\EditCartaPorte.xaml"
            this.btnCalcular.Click += new System.Windows.RoutedEventHandler(this.btnCalcular_Click);
            
            #line default
            #line hidden
            return;
            case 23:
            this.btnAddRemision = ((System.Windows.Controls.Button)(target));
            
            #line 301 "..\..\..\EditCartaPorte.xaml"
            this.btnAddRemision.Click += new System.Windows.RoutedEventHandler(this.btnAddRemision_Click);
            
            #line default
            #line hidden
            return;
            case 24:
            this.btnQuitar = ((System.Windows.Controls.Button)(target));
            
            #line 307 "..\..\..\EditCartaPorte.xaml"
            this.btnQuitar.Click += new System.Windows.RoutedEventHandler(this.btnQuitar_Click);
            
            #line default
            #line hidden
            return;
            case 25:
            this.txtTotalImporte = ((System.Windows.Controls.TextBox)(target));
            return;
            case 26:
            this.txtTotalRetencion = ((System.Windows.Controls.TextBox)(target));
            return;
            case 27:
            this.txtTotalFlete = ((System.Windows.Controls.TextBox)(target));
            return;
            case 28:
            this.txtSumaTotal = ((System.Windows.Controls.TextBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 19:
            
            #line 240 "..\..\..\EditCartaPorte.xaml"
            ((System.Windows.Controls.TextBox)(target)).KeyDown += new System.Windows.Input.KeyEventHandler(this.TextBox_KeyDown);
            
            #line default
            #line hidden
            break;
            case 20:
            
            #line 272 "..\..\..\EditCartaPorte.xaml"
            ((System.Windows.Controls.TextBox)(target)).LostFocus += new System.Windows.RoutedEventHandler(this.TextBox_LostFocus);
            
            #line default
            #line hidden
            
            #line 272 "..\..\..\EditCartaPorte.xaml"
            ((System.Windows.Controls.TextBox)(target)).TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.TextBox_TextChanged);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

