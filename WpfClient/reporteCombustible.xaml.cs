﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static WpfClient.ImprimirMensaje;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using Core.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para reporteCombustible.xaml
    /// </summary>
    public partial class reporteCombustible : ViewBase
    {
        public reporteCombustible()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Usuario usuario = mainWindow.usuario;
                Cursor = Cursors.Wait;
                ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
                ctrEmpresa.loaded(usuario.empresa, (new PrivilegioSvc().consultarPrivilegio("isAdmin", usuario.idUsuario).typeResult == ResultTypes.success));

                var respZonas = new ZonaOperativaSvc().getAllZonasOperativas();
                if (respZonas.typeResult == ResultTypes.success)
                {
                    cbxZonaOperativa.ItemsSource = respZonas.result as List<ZonaOperativa>;
                    cbxZonaOperativa.SelectedItem = (cbxZonaOperativa.ItemsSource as List<ZonaOperativa>).Find(s => s.idZonaOperativa == usuario.zonaOperativa.idZonaOperativa);
                }
                else
                {
                    imprimir(respZonas);
                    IsEnabled = false;
                }
                
                OperationResult respProveedor = new ProveedorSvc().getAllProveedoresV2();
                if (respProveedor.typeResult == ResultTypes.success)
                {
                    List<ProveedorCombustible> listaProveedores = new List<ProveedorCombustible> { new ProveedorCombustible { idProveedor = 0, nombre = "INTEGRA" } };
                    listaProveedores.AddRange((respProveedor.result as List<ProveedorCombustible>).FindAll(s => s.estatus && s.diesel).OrderBy(s => s.nombre).ToList());
                    cbxProveedores.ItemsSource = listaProveedores;
                    
                }
                if (new PrivilegioSvc().consultarPrivilegio("MOSTRAR_PRECIOS_IMPORTES_REPORTE_COMBUSTIBLE", base.mainWindow.inicio._usuario.idUsuario).typeResult == ResultTypes.success)
                {
                    this.chcPrecio.Visibility = Visibility.Visible;
                    this.chcPrecioExtra.Visibility = Visibility.Visible;
                    this.chcImporte.Visibility = Visibility.Visible;
                    chcSubTotal.Visibility = Visibility.Visible;
                    chcIEPS.Visibility = Visibility.Visible;
                    chcSubTotalIEPS.Visibility = Visibility.Visible;
                    chcIva.Visibility = Visibility.Visible;
                    this.stpCostoDiesel.Visibility = Visibility.Visible;

                    chcPrecioSinIEMP.Visibility = Visibility.Visible;
                    chcPrecioSinIVa.Visibility = Visibility.Visible;
                    chcPrecioSinIEPSExtra.Visibility = Visibility.Visible;
                    chcPrecioSinIVaExtra.Visibility = Visibility.Visible;
                }
                else
                {
                    this.chcPrecio.Visibility = Visibility.Collapsed;
                    this.chcPrecioExtra.Visibility = Visibility.Collapsed;
                    this.chcImporte.Visibility = Visibility.Collapsed;
                    chcSubTotal.Visibility = Visibility.Collapsed;
                    chcIEPS.Visibility = Visibility.Collapsed;
                    chcSubTotalIEPS.Visibility = Visibility.Collapsed;
                    chcIva.Visibility = Visibility.Collapsed;
                    this.stpCostoDiesel.Visibility = Visibility.Collapsed;

                    chcPrecioSinIEMP.Visibility = Visibility.Collapsed;
                    chcPrecioSinIVa.Visibility = Visibility.Collapsed;
                    chcPrecioSinIEPSExtra.Visibility = Visibility.Collapsed;
                    chcPrecioSinIVaExtra.Visibility = Visibility.Collapsed;

                }
                //chcFacturaProveedor
                if (new PrivilegioSvc().consultarPrivilegio("MOSTRAR_COLUMNA_FACTURA_PROVEEDOR", base.mainWindow.inicio._usuario.idUsuario).typeResult == ResultTypes.success)
                {
                    //chcFacturaProveedor.Visibility = Visibility.Visible;
                }
                else
                {
                    //chcFacturaProveedor.Visibility = Visibility.Collapsed;
                }
                llenarTablaResumen();
                cargarPerfiles();
            }
            catch (Exception ex)
            {
                imprimir(ex);
                this.IsEnabled = false;
            }
            finally
            {
                Cursor = Cursors.Arrow;
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
            }
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cargarUnidades();
            cargarOperadores();
        }
        void cargarUnidades()
        {
            try
            {
                Cursor = Cursors.Wait;
                cbxUnidades.ItemsSource = null;
                chcTodosUnidades.IsChecked = false;
                if (cbxZonaOperativa.SelectedItems.Count > 0)
                {
                    List<UnidadTransporte> listaUnidades = new List<UnidadTransporte>();
                    foreach (var item in cbxZonaOperativa.SelectedItems.Cast<ZonaOperativa>().ToList())
                    {
                        var resp = new UnidadTransporteSvc().getUnidadesCarganByEmpresaZonaOperativa(ctrEmpresa.empresaSelected.clave, item.idZonaOperativa);
                        if (resp.typeResult == ResultTypes.success)
                        {                            
                            listaUnidades.AddRange(resp.result as List<UnidadTransporte>);
                        }
                    }
                    cbxUnidades.ItemsSource = listaUnidades.OrderBy(s=>s.clave).ToList();
                }
                else
                {
                    cbxUnidades.ItemsSource = null;
                }

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        void cargarOperadores()
        {
            try
            {
                cbxOperadores.ItemsSource = null;
                chcTodosOperadores.IsChecked = false;
                Cursor = Cursors.Wait;
                var resp = new OperadorSvc().getOperadoresALLorById(0, ctrEmpresa.empresaSelected.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxOperadores.ItemsSource = (resp.result as List<Personal>).OrderBy(s => s.nombre).ToList();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void ChcTodosZonas_Checked(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                CheckBox check = sender as CheckBox;
                switch (check.IsChecked.Value)
                {
                    case true:
                        cbxZonaOperativa.SelectAll();
                        break;
                    case false:
                        cbxZonaOperativa.SelectedItems.Clear();
                        break;
                }
            }
        }

        private void ChcTodosProveedores_Checked(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                CheckBox check = sender as CheckBox;
                switch (check.IsChecked.Value)
                {
                    case true:
                        cbxProveedores.SelectAll();
                        break;
                    case false:
                        cbxProveedores.SelectedItems.Clear();
                        break;
                }
            }
        }
        private void ChcTodosUnidades_Checked(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                CheckBox check = sender as CheckBox;
                switch (check.IsChecked.Value)
                {
                    case true:
                        cbxUnidades.SelectAll();
                        break;
                    case false:
                        cbxUnidades.SelectedItems.Clear();
                        break;
                }
            }
        }
        private void CbxZonaOperativa_ItemSelectionChanged(object sender, Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventArgs e)
        {
            cargarUnidades();
        }

        private void CbxProveedores_ItemSelectionChanged(object sender, Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventArgs e)
        {

        }

        private void ChcTodosOperadores_Checked(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                CheckBox check = sender as CheckBox;
                switch (check.IsChecked.Value)
                {
                    case true:
                        cbxOperadores.SelectAll();
                        break;
                    case false:
                        cbxOperadores.SelectedItems.Clear();
                        break;
                }
            }
        }

        private void CheckBox_CheckedColom(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                if (sender != null)
                {
                    CheckBox chc = sender as CheckBox;
                    GridView view = (GridView)lvlResultados.View;
                    if (chc.IsChecked.Value)
                    {
                        string[] strArray = chc.Tag.ToString().Split(',');
                        if (strArray.Length > 1)
                        {
                            GridViewColumn item = new GridViewColumn
                            {
                                Header = chc.Content
                            };
                            Binding binding1 = new Binding(strArray[0])
                            {
                                StringFormat = strArray[1]
                            };
                            item.DisplayMemberBinding = binding1;
                            item.Width = Convert.ToInt32(chc.DataContext);
                            view.Columns.Add(item);
                        }
                        else
                        {
                            GridViewColumn item = new GridViewColumn
                            {
                                Header = chc.Content,
                                DisplayMemberBinding = new Binding(strArray[0]),
                                Width = Convert.ToInt32(chc.DataContext)
                            };
                            view.Columns.Add(item);
                        }
                        this.listString.Add(chc.Content.ToString());
                        dictionaryStr.Add(chc.Content.ToString(), strArray[0]);
                    }
                    else
                    {
                        this.listString.Remove(chc.Content.ToString());
                        dictionaryStr.Remove(chc.Content.ToString());
                        List<GridViewColumn> list = view.Columns.Cast<GridViewColumn>().ToList<GridViewColumn>();
                        view.Columns.Remove(list.Find(s => s.Header.ToString() == chc.Content.ToString()));
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        private List<string> listString = new List<string>
        {
            "VALE",
            "EMPRESA",
            "OPERACIÓN",
            "FECHA CARGA",
            "HORA CARGA",
            "UNIDAD",
            "LITROS CARGADOS"
        };
        Dictionary<string, string> dictionaryStr = new Dictionary<string, string>
        {
            {"VALE","vale"},
            {"EMPRESA","empresa"},
            {"OPERACIÓN","operación"},
            {"FECHA CARGA","fechaCarga"},
            {"HORA CARGA","horaCarga"},
            {"UNIDAD","unidad"},
            {"LITROS CARGADOS","litrosCargados"}
        };

        private void Expander_Expanded(object sender, RoutedEventArgs e)
        {
            lvlResultados.Height = 185;
        }

        private void Expander_Collapsed(object sender, RoutedEventArgs e)
        {
            lvlResultados.Height = 305;
        }
        private DateTime? fechaInicio = null;
        private DateTime? fechaFin = null;
        void llenarTablaResumen()
        {
            try
            {
                if (lvlResultados.ItemsSource != null)
                {
                    List<ReporteCargasDiesel> lista = lvlResultados.ItemsSource as List<ReporteCargasDiesel>;
                    txtContUnidades.Text = (from n in lista group lista by n.unidad).ToList().Count().ToString("N0");
                    txtContDias.Text = !this.fechaFin.HasValue ? "0" : Convert.ToInt32((this.fechaFin.Value - this.fechaInicio.Value).TotalDays).ToString("N0");
                    txtContCargas.Text = lista.Count.ToString("N0");
                    txtSumaTrips.Text = lista.Sum(s=> s.tripFisico).ToString("N4");
                    txtSumaLitros.Text = lista.Sum(s => s.litrosCargados).ToString("N4");
                    txtSumaImportes.Text = lista.Sum(s => s.importe).ToString("C4");
                }
                else
                {
                    txtContUnidades.Text = decimal.Zero.ToString("N0");
                    txtContDias.Text = !this.fechaFin.HasValue ? "0" : Convert.ToInt32((this.fechaFin.Value - this.fechaInicio.Value).TotalDays).ToString("N0");
                    txtContCargas.Text = decimal.Zero.ToString("N0");
                    txtSumaTrips.Text = decimal.Zero.ToString("N0");
                    txtSumaImportes.Text = decimal.Zero.ToString("C4");
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                lvlResultados.ItemsSource = null;
                fechaInicio = ctrFechas.fechaInicial;
                fechaFin = ctrFechas.fechaFinal;
                List<string> listaZonas = cbxZonaOperativa.SelectedItems.Cast<ZonaOperativa>().ToList().Select(s => s.idZonaOperativa.ToString()).ToList();
                List<string> listaProveedores = cbxProveedores.SelectedItems.Cast<ProveedorCombustible>().ToList().Select(s => s.idProveedor.ToString()).ToList();
                List<string> listaUnidades = cbxUnidades.SelectedItems.Cast<UnidadTransporte>().ToList().Select(s => s.clave.ToString()).ToList();
                List<string> listaOperadores = cbxOperadores.SelectedItems.Cast<Personal>().ToList().Select(s => s.clave.ToString()).ToList();
                var resp = new ReporteCargaCombustibleSvc().getReporteCombustibleByFiltros(ctrEmpresa.empresaSelected.clave, ctrFechas.fechaInicial, ctrFechas.fechaFinal,
                    listaZonas, chcTodosZonas.IsChecked.Value, listaProveedores, chcTodosProveedores.IsChecked.Value, listaUnidades,
                    chcTodosUnidades.IsChecked.Value, listaOperadores , chcTodosOperadores.IsChecked.Value);

                if (resp.typeResult == ResultTypes.success)
                {
                    lvlResultados.ItemsSource = resp.result as List<ReporteCargasDiesel>;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
                llenarTablaResumen();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                new ReportView().exportarReporteCombustible(lvlResultados.ItemsSource as List<ReporteCargasDiesel>, listString, dictionaryStr);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BtnGuardarPerfil_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                if (string.IsNullOrEmpty(this.txtNombrePerfil.Text.Trim()))
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "NO SE PROPORCIONO EL NOMBRE DEL PERFIL"
                    };
                    ImprimirMensaje.imprimir(resp);
                }
                else
                {
                    PerfilReporte perfilReporte = new PerfilReporte
                    {
                        nombrePerfil = this.txtNombrePerfil.Text.Trim(),
                        usuraio = base.mainWindow.usuario,
                        tipoReporte = enumPerfilRporte.COMBUSTIBLE,
                        activo = true
                    };
                    perfilReporte.cargarlista(this.listString);
                    if (perfilReporte.listDetalles.Count == 0)
                    {
                        System.Windows.MessageBox.Show("Se requiere al menos una opci\x00f3n", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                    else
                    {
                        OperationResult resp = new PerfilReporteSvc().savePerfilReporte(ref perfilReporte);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            this.cargarPerfiles();
                        }
                        ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        public bool cargarPerfiles()
        {
            this.cbxPerfiles.Items.Clear();
            OperationResult resp = new PerfilReporteSvc().getPerfilesReportes(base.mainWindow.usuario.idUsuario, enumPerfilRporte.COMBUSTIBLE);
            if (resp.typeResult == ResultTypes.success)
            {
                foreach (PerfilReporte reporte in resp.result as List<PerfilReporte>)
                {
                    this.cbxPerfiles.Items.Add(reporte);
                }
                return true;
            }
            if (resp.typeResult != ResultTypes.recordNotFound)
            {
                ImprimirMensaje.imprimir(resp);
                return false;
            }
            return true;
        }
        private void BtnQuitarPerfil_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                if (this.cbxPerfiles.SelectedItem != null)
                {
                    PerfilReporte selectedItem = this.cbxPerfiles.SelectedItem as PerfilReporte;
                    selectedItem.usuraio = base.mainWindow.usuario;
                    selectedItem.tipoReporte = enumPerfilRporte.COMBUSTIBLE;
                    selectedItem.activo = false;
                    OperationResult resp = new PerfilReporteSvc().savePerfilReporte(ref selectedItem);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        this.cargarPerfiles();
                        this.txtNombrePerfil.Clear();
                        this.cbxPerfiles.SelectedItem = null;
                    }
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        private void CbxPerfiles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender != null) && (this.cbxPerfiles.SelectedItem != null))
            {
                PerfilReporte selectedItem = this.cbxPerfiles.SelectedItem as PerfilReporte;
                this.txtNombrePerfil.Text = selectedItem.nombrePerfil;
                this.cargarChecColumnas(selectedItem.listDetalles);
            }
        }
        private void cargarChecColumnas(List<DetallesPerfilReporte> listDetalles)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                List<CheckBox> list = this.wrapColumnas.Children.Cast<CheckBox>().ToList<CheckBox>();
                foreach (CheckBox chc in list)
                {
                    if (chc.Visibility == Visibility.Visible)
                    {
                        if (listDetalles.Find(s => s.nombreDetalle == chc.Content.ToString()) != null)
                        {
                            chc.IsChecked = true;
                        }
                        else
                        {
                            chc.IsChecked = false;
                        }
                    }
                    else
                    {
                        chc.IsChecked = false;
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
    }
}
