﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para controlTxtProveedores.xaml
    /// </summary>
    public partial class controlTxtProveedores : UserControl
    {
        public controlTxtProveedores()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            txtProveedor.Width = this.Width - 104;
        }
        List<Proveedor> listaProveedores = new List<Proveedor>();
        public void cargarTodosLosProveedores()
        {
            try
            {
                Cursor = Cursors.Arrow;
                var resp = new ProveedorSvc().getAllProveedores();
                if (resp.typeResult == ResultTypes.success)
                {
                    listaProveedores = resp.result as List<Proveedor>;
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    imprimir(resp);
                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                this.IsEnabled = false;
            }
        }

        private void txtProveedor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var lista = listaProveedores.FindAll(s => s.razonSocial.IndexOf(txtProveedor.Text.Trim(), StringComparison.OrdinalIgnoreCase) >= 0);
                if (lista.Count == 1)
                {
                    proveedor = lista[0];
                    this.DataContext = lista[0];
                }
                else
                {
                    var result = new selectProveedores().getProveedor(listaProveedores, txtProveedor.Text.Trim());
                    if (result != null)
                    {
                        proveedor = result;
                        this.DataContext = result as Proveedor;
                    }
                    else
                    {
                        DataContext = null;
                    }
                }
            }
        }
        private Proveedor _proveedor { get; set; }
        public Proveedor proveedor
        {
            get
            {
                return _proveedor;
            }
            set
            {
                _proveedor = value;
            }
        }
    }
}
