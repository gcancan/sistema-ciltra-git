﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using Core.Interfaces;
using CoreFletera.Models;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for editViajesPendientesAtlante.xaml
    /// </summary>
    public partial class editViajesPendientesAtlante : ViewBase
    {
        Empresa empresa = new Empresa();
        public editViajesPendientesAtlante()
        {
            InitializeComponent();
        }
        public override void nuevo()
        {
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            txtTractor.limpiar();
            txtTolva1.limpiar();
            txtDolly.limpiar();
            txtTolva2.limpiar();
            txtChofer.limpiar();

            txtDespachador.limpiar();

            txtCombustibleCargados.Clear();
            txtCombustibleUtilizado.Clear();

            txtKm.Clear();
            txtKmRecorridos.Clear();

            chBoxTipo.IsChecked = true;
            chBoxTipo.IsChecked = false;

            txtTanqueDer.Clear();
            txtTanqueIzq.Clear();
            txtTanqueRes.Clear();

            txtFolioViaje.Clear();
            txtValeCarga.Clear();
            lvlProductos.Items.Clear();
            txtTractor.txtUnidadTrans.Focus();
            cant = 0;
            txtCantidad.Clear();
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                dpFecha.SelectedDate = DateTime.Today;

                empresa = mainWindow.inicio._usuario.personal.empresa;
                OperationResult resp = new ClienteSvc().getClientesALLorById(empresa.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxClientes.ItemsSource = (List<Cliente>)resp.result;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    IsEnabled = false;
                    return;
                }

                cbxClientes.SelectionChanged += CbxClientes_SelectionChanged;

                txtTractor.loaded(etipoUniadBusqueda.TRACTOR, empresa);
                txtTractor.DataContextChanged += TxtControles_DataContextChanged;
                txtTolva1.loaded(etipoUniadBusqueda.TOLVA, empresa);
                txtTolva1.DataContextChanged += TxtControles_DataContextChanged;
                txtDolly.loaded(etipoUniadBusqueda.DOLLY, empresa);
                txtDolly.DataContextChanged += TxtControles_DataContextChanged;
                txtTolva2.loaded(etipoUniadBusqueda.TOLVA, empresa);
                txtTolva2.DataContextChanged += TxtControles_DataContextChanged;
                txtChofer.loaded(eTipoBusquedaPersonal.OPERADORES, empresa);
                txtChofer.DataContextChanged += TxtPersonal_DataContextChanged;

                txtDespachador.loaded(eTipoBusquedaPersonal.DESPACHADORES, null);
                txtDespachador.DataContextChanged += TxtPersonal_DataContextChanged;
                nuevo();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                IsEnabled = false;
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }

        }
        List<Origen> _listOrigen = new List<Origen>();
        List<Producto> _lisProducto = new List<Producto>();
        private void CbxClientes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    Cliente cliente = (Cliente)((ComboBox)sender).SelectedItem;
                    _lisProducto = cliente.fraccion.listProductos;
                    OperationResult resp = new OrigenSvc().getOrigenesAll(cliente.clave);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        _listOrigen = (List<Origen>)resp.result;
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                _listOrigen = new List<Origen>();
                _lisProducto = new List<Producto>();
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }

        }

        private void TxtPersonal_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var control = (controlPersonal)sender;
            if (control.DataContext != null)
            {
                switch (control.Name)
                {
                    case "txtChofer":
                        txtOrigen.Focus();
                        break;
                    case "txtDespachador":
                        txtKm.Focus();
                        break;
                }

            }
        }
        decimal cant = 0;
        private void TxtControles_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var control = (controlUnidadesTransporte)sender;
            if (control.DataContext != null)
            {
                switch (control.Name)
                {
                    case "txtTractor":
                        txtTolva1.txtUnidadTrans.Focus();
                        break;
                    case "txtTolva1":
                        txtDolly.txtUnidadTrans.Focus();
                        cant += control.unidadTransporte.tipoUnidad.TonelajeMax;
                        txtCantidad.Text = cant.ToString();
                        break;
                    case "txtDolly":
                        txtTolva2.txtUnidadTrans.Focus();                        
                        break;
                    case "txtTolva2":
                        txtChofer.txtPersonal.Focus();
                        cant += control.unidadTransporte.tipoUnidad.TonelajeMax;
                        txtCantidad.Text = cant.ToString();
                        break;
                }
            }
        }

        private void txtOrigen_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                List<Origen> listOrigen = new List<Origen>();
                if (_listOrigen.Count <= 0) { return; }
                var mySearch = _listOrigen.FindAll(S => S.nombreOrigen.IndexOf(txtOrigen.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (var item in mySearch)
                {
                    listOrigen.Add(item);
                }
                selectOrigenes buscador = new selectOrigenes(txtProducto.Text);
                switch (listOrigen.Count)
                {
                    case 1:
                        mapOrigenes(listOrigen[0]);
                        break;
                    default:
                        var resp = buscador.buscarOrigenes(_listOrigen);
                        mapOrigenes(resp);
                        break;
                }
            }
        }

        private void mapOrigenes(Origen origen)
        {
            if (origen != null)
            {
                txtOrigen.Tag = origen;
                txtOrigen.Text = origen.nombreOrigen;
                txtOrigen.IsEnabled = false;
                txtDestino.Focus();
                buscarDestinos();
            }
            else
            {
                txtOrigen.Tag = null;
                txtOrigen.Clear();
                txtOrigen.IsEnabled = true;
            }
        }
        List<Zona> _listZonas = new List<Zona>();

        private void buscarDestinos()
        {
            try
            {
                if (txtOrigen.Tag == null)
                {
                    return;
                }
                OperationResult resp = new ZonasSvc().getDestinosByOrigenAndIdCliente(((Cliente)cbxClientes.SelectedItem).clave, ((Origen)txtOrigen.Tag).idOrigen);
                if (resp.typeResult == ResultTypes.success)
                {
                    _listZonas = (List<Zona>)resp.result;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void txtDestino_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (_listZonas.Count <= 0) { return; }
                List<Zona> listZonas = new List<Zona>();
                var mySearch = _listZonas.FindAll(S => S.descripcion.IndexOf(txtDestino.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (var item in mySearch)
                {
                    listZonas.Add(item);
                }

                selectDestinos buscador = new selectDestinos(txtDestino.Text);
                switch (listZonas.Count)
                {
                    case 1:
                        mapDestinos(listZonas[0]);
                        break;
                    default:
                        var resp = buscador.buscarZona(_listZonas);
                        mapDestinos(resp);
                        break;
                }
            }
        }

        private void mapDestinos(Zona zona)
        {
            if (zona != null)
            {
                txtDestino.Tag = zona;
                txtDestino.Text = zona.descripcion;
                txtDestino.IsEnabled = false;
                txtProducto.Focus();
            }
            else
            {
                txtDestino.Tag = null;
                txtDestino.Clear();
                txtDestino.IsEnabled = true;
            }
            //}

        }
        private void txtProducto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                List<Producto> listProducto = new List<Producto>();
                if (_lisProducto.Count <= 0) { return; }
                foreach (var item in _lisProducto)
                {
                    if (string.IsNullOrEmpty(item.descripcion))
                    {

                    }
                }
                if (!string.IsNullOrEmpty(txtProducto.Text))
                {
                    var mySearch = _lisProducto.FindAll(S => S.descripcion.IndexOf(txtProducto.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                    foreach (var item in mySearch)
                    {
                        listProducto.Add(item);
                    }
                }
                selectProducto buscador = new selectProducto(txtProducto.Text);
                switch (listProducto.Count)
                {
                    case 1:
                        mapProductos(listProducto[0]);
                        break;
                    default:
                        var resp = buscador.buscarProductos(_lisProducto);
                        mapProductos(resp);
                        break;
                }
            }
        }

        private void mapProductos(Producto producto)
        {
            if (producto != null)
            {
                txtProducto.Tag = producto;
                txtProducto.Text = producto.descripcion;
                txtProducto.IsEnabled = false;
                txtCantidad.Focus();
                if (!string.IsNullOrEmpty(txtCantidad.Text))
                {
                    txtCantidad.Select(0, txtCantidad.Text.Length);
                }
                lblCantidad.Content = "Cantidad" + " " + producto.clave_unidad_de_medida;
            }
            else
            {
                txtProducto.Tag = null;
                txtProducto.Clear();
                txtProducto.IsEnabled = true;
                lblCantidad.Content = "Cantidad";
            }
        }

        private void txtCantidad_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                decimal d = 0m;
                if (decimal.TryParse(txtCantidad.Text.Trim(), out d))
                {
                    if (cbxClientes.SelectedItem != null && txtTolva1.unidadTransporte != null/*cbxClientes.SelectedItem != null && cbxTolva1.SelectedItem != null*/)
                    {
                        mapDetalles();
                    }
                }
            }
        }
        private void mapDetalles()
        {
            CartaPorte det = new CartaPorte
            {
                producto = (Producto)txtProducto.Tag,
                zonaSelect = (Zona)txtDestino.Tag,
                origen = (Origen)txtOrigen.Tag,
                PorcIVA = ((Cliente)cbxClientes.SelectedItem).impuesto.valor,
                PorcRetencion = ((Cliente)cbxClientes.SelectedItem).impuestoFlete.valor,
                tipoPrecio = establecerPrecio(),
                remision = "",
                volumenDescarga = Convert.ToDecimal(txtCantidad.Text.Trim())
            };
            ListViewItem lvl = new ListViewItem();
            lvl.Content = det;
            lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
            lvlProductos.Items.Add(lvl);
            limpiarDetalles();
            txtOrigen.Focus();
        }
        void limpiarDetalles()
        {
            mapDestinos(null);
            mapProductos(null);
            mapOrigenes(null);
            txtCantidad.Text = cant.ToString();
        }
        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            lvlProductos.Items.Remove(lvlProductos.SelectedItem);
        }
        private TipoPrecio establecerPrecio()
        {
            UnidadTransporte x = (UnidadTransporte)txtTolva1.Tag;//cbxTolva1.SelectedItem;

            if (((Cliente)cbxClientes.SelectedItem).clave == 3)
            {
                if (txtDolly.unidadTransporte != null || txtTolva2.unidadTransporte != null)
                {
                    return TipoPrecio.FULL;
                }
                //else if (x.tipoUnidad.descripcion == "TOLVA 35 TON")
                //{
                //    return TipoPrecio.SENCILLO;
                //}
                else
                {
                    return TipoPrecio.SENCILLO;
                }
            }
            else
            {
                if (txtDolly.unidadTransporte != null || txtTolva2.unidadTransporte != null)
                {
                    return TipoPrecio.FULL;
                }

                else
                {
                    return TipoPrecio.SENCILLO;
                }
            }
        }

        private void btnNewDetalle_Click(object sender, RoutedEventArgs e)
        {
            decimal d = 0m;
            if (decimal.TryParse(txtCantidad.Text.Trim(), out d))
            {
                if (cbxClientes.SelectedItem != null && txtTolva1.unidadTransporte != null/*cbxClientes.SelectedItem != null && cbxTolva1.SelectedItem != null*/)
                {
                    mapDetalles();
                }
            }
        }

        private void chBoxTipo_Checked(object sender, RoutedEventArgs e)
        {
            cbxCombustible.IsEnabled = false;
            txtCombustiblePTO.Clear();
            txtCombustibleEST.Clear();
            cbxTiempos.IsEnabled = false;
            txtTiempoTotal.Text = "00:00:00";
            txtTiempoPTO.Text = "00:00:00";
            txtTiempoEst.Text = "00:00:00";
            txtTanqueIzq.Clear();
            txtTanqueDer.Clear();
            txtTanqueRes.Clear();
            stpExt.IsEnabled = true;
        }

        private void chBoxTipo_Unchecked(object sender, RoutedEventArgs e)
        {
            cbxCombustible.IsEnabled = true;
            cbxTiempos.IsEnabled = true;
            stpExt.IsEnabled = false;
            txtTicket.Clear();
            txtFolioImpreso.Clear();
        }

        public override OperationResult guardar()
        {
            try
            {
                var val = validarViaje();
                if (val.typeResult != ResultTypes.success)
                {
                    return val;
                }
                val = validarValeCarga();
                if (val.typeResult != ResultTypes.success)
                {
                    return val;
                }
                Cursor = Cursors.Wait;
                Viaje viaje = mapFormViaje();
                CargaCombustible cargaCombustible = mapFormCargaCombustible();

                var resp = new ViajesPendientesSvc().saveViajesCargasPendientes(ref viaje, ref cargaCombustible);
                if (resp.typeResult == ResultTypes.success)
                {
                    txtFolioViaje.Text = viaje.NumGuiaId.ToString();
                    txtValeCarga.Text = cargaCombustible.idCargaCombustible.ToString();
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private CargaCombustible mapFormCargaCombustible()
        {
            try
            {
                CargaCombustible carga = new CargaCombustible
                {
                    despachador = txtDespachador.personal,
                    fechaCombustible = dpFecha.SelectedDate.Value,
                    folioImpreso = txtFolioImpreso.Text,
                    idCargaCombustible = 0,
                    idOrdenServ = 0,
                    isExterno = chBoxTipo.IsChecked.Value,
                    idSalidaEntrada = 0,
                    kmFinal = 0,
                    kmDespachador = Convert.ToDecimal(txtKm.Text),
                    kmRecorridos = Convert.ToDecimal(txtKmRecorridos.Text) <= 1 ? (Convert.ToDecimal(txtKm.Text) - ultimoKm) : Convert.ToDecimal(txtKmRecorridos.Text),
                    kmInicial = 0,
                    ltCombustible = Convert.ToDecimal(txtCombustibleCargados.Text),
                    ltCombustibleUtilizado = Convert.ToDecimal(txtCombustibleUtilizado.Text),
                    ltCombustibleEST = string.IsNullOrEmpty(txtCombustibleEST.Text) ? 0 : Convert.ToDecimal(txtCombustibleEST.Text),
                    ltCombustiblePTO = string.IsNullOrEmpty(txtCombustiblePTO.Text) ? 0 : Convert.ToDecimal(txtCombustiblePTO.Text),
                    sello1 = Convert.ToInt32(txtTanqueIzq.Text),
                    sello2 = Convert.ToInt32(txtTanqueDer.Text),
                    selloReserva =string.IsNullOrEmpty(txtTanqueRes.Text) ? 0: Convert.ToInt32(txtTanqueRes.Text),
                    ticket = txtTicket.Text,
                    tiempoTotal = txtTiempoTotal.Text,
                    tiempoEST = txtTiempoEst.Text,
                    tiempoPTO = txtTiempoPTO.Text
                };
                return carga;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private Viaje mapFormViaje()
        {
            try
            {
                Viaje viaje = new Viaje
                {
                    NumGuiaId = 0,
                    NumViaje = 0,
                    cliente = (Cliente)cbxClientes.SelectedItem,
                    tractor = txtTractor.unidadTransporte,
                    remolque = txtTolva1.unidadTransporte,
                    dolly = txtDolly.unidadTransporte,
                    remolque2 = txtTolva2.unidadTransporte,
                    EstatusGuia = "FINALIZADO",
                    fechaBachoco = null,
                    FechaHoraViaje = dpFecha.SelectedDate.Value,
                    IdEmpresa = mainWindow.empresa.clave,
                    FechaHoraFin = dpFecha.SelectedDate.Value,
                    isExterno = chBoxTipo.IsChecked.Value,
                    operador = (Personal)txtChofer.personal,
                    tipoViaje = txtTolva2.unidadTransporte == null ? 'S' : 'F',
                    UserViaje = mainWindow.inicio._usuario.idUsuario,
                    listDetalles = getDetalles()
                };
                return viaje;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        OperationResult validarViaje()
        {
            try
            {
                OperationResult resp = new OperationResult { valor = 0, mensaje = "" };
                resp.mensaje = "PARA PROCEDER SE REQUIERE LO SIGUIENTE:" + Environment.NewLine;
                if (cbxClientes.SelectedItem == null)
                {
                    resp.valor = 2;
                    resp.mensaje += "    *Seleccionar un cliente" + Environment.NewLine;
                }
                if (txtTractor.unidadTransporte == null)
                {
                    resp.valor = 2;
                    resp.mensaje += "    *Seleccionar un el tractor" + Environment.NewLine;
                }
                if (txtTolva1.unidadTransporte == null)
                {
                    resp.valor = 2;
                    resp.mensaje += "    *Seleccionar un el remolque 1" + Environment.NewLine;
                }
                if (txtTolva2.unidadTransporte != null || txtDolly.unidadTransporte != null)
                {
                    if (txtDolly.unidadTransporte == null)
                    {
                        resp.valor = 2;
                        resp.mensaje += "    *Seleccionar un el dolly" + Environment.NewLine;
                    }
                    if (txtTolva2.unidadTransporte == null)
                    {
                        resp.valor = 2;
                        resp.mensaje += "    *Seleccionar un el remolque 2" + Environment.NewLine;
                    }
                }
                if (dpFecha.SelectedDate == null)
                {
                    resp.valor = 2;
                    resp.mensaje += "    *Seleccionar una fecha" + Environment.NewLine;
                }
                if (txtChofer.personal == null)
                {
                    resp.valor = 2;
                    resp.mensaje += "    *Seleccionar al operador" + Environment.NewLine;
                }
                if (lvlProductos.Items.Count <= 0)
                {
                    resp.valor = 2;
                    resp.mensaje += "    *Agregar al menos un origen con su destinos, producto y cantidad" + Environment.NewLine;
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        OperationResult validarValeCarga()
        {
            try
            {
                OperationResult resp = new OperationResult { valor = 0, mensaje = "" };
                resp.mensaje = "PARA PROCEDER SE REQUIERE LO SIGUIENTE:" + Environment.NewLine;
                if (txtDespachador.personal == null)
                {
                    resp.valor = 2;
                    resp.mensaje += "   *Seleccionar al Despachador" + Environment.NewLine;
                }
                int v = Validaciones.validarDecimal(txtKm.Text.Trim());
                if (v == 1)
                {
                    resp.valor = 2;
                    resp.mensaje += "   *Proporcionar el km Total" + Environment.NewLine;
                }
                else if (v == 2)
                {
                    resp.valor = 2;
                    resp.mensaje += "   *El km Total no es valido" + Environment.NewLine;
                }
                v = Validaciones.validarDecimal(txtKmRecorridos.Text.Trim());
                if (v == 1)
                {
                    resp.valor = 2;
                    resp.mensaje += "   *Proporcionar el km Recorrido" + Environment.NewLine;
                }
                else if (v == 2)
                {
                    resp.valor = 2;
                    resp.mensaje += "   *El km recorrido no es valido" + Environment.NewLine;
                }
                v = Validaciones.validarDecimal(txtCombustibleUtilizado.Text.Trim());
                if (v == 1)
                {
                    resp.valor = 2;
                    resp.mensaje += "   *Proporcionar el combustible total" + Environment.NewLine;
                }
                else if (v == 2)
                {
                    resp.valor = 2;
                    resp.mensaje += "   *El combustible total no es valido" + Environment.NewLine;
                }
                v = Validaciones.validarDecimal(txtCombustibleUtilizado.Text.Trim()) ;
                if (v == 1)
                {
                    resp.valor = 2;
                    resp.mensaje += "   *Proporcionar el combustible total" + Environment.NewLine;
                }
                else if (v == 2)
                {
                    resp.valor = 2;
                    resp.mensaje += "   *El combustible total no es valido" + Environment.NewLine;
                }
                if (chBoxTipo.IsChecked.Value)
                {
                    if (string.IsNullOrEmpty(txtTicket.Text.Trim()))
                    {
                        resp.valor = 2;
                        resp.mensaje += "   *Proporcionar el ticket" + Environment.NewLine;
                    }
                    if (string.IsNullOrEmpty(txtFolioImpreso.Text.Trim()))
                    {
                        resp.valor = 2;
                        resp.mensaje += "   *Proporcionar el folio Impreso" + Environment.NewLine;
                    }
                }
                else
                {
                    v = Validaciones.validarDecimal(txtCombustibleEST.Text.Trim());
                    if (v == 1)
                    {
                        resp.valor = 2;
                        resp.mensaje += "   *Proporcionar el combustible EST" + Environment.NewLine;
                    }
                    else if (v == 2)
                    {
                        resp.valor = 2;
                        resp.mensaje += "   *El combustible EST no es valido" + Environment.NewLine;
                    }
                    v = Validaciones.validarDecimal(txtCombustiblePTO.Text.Trim());
                    if (v == 1)
                    {
                        resp.valor = 2;
                        resp.mensaje += "   *Proporcionar el combustible PTO" + Environment.NewLine;
                    }
                    else if (v == 2)
                    {
                        resp.valor = 2;
                        resp.mensaje += "   *El combustible PTO no es valido" + Environment.NewLine;
                    }
                }

                var split = txtTiempoTotal.Text.Split(':');
                try
                {
                    TimeSpan tiempo = new TimeSpan(Convert.ToInt32(split[0]), Convert.ToInt32(split[1]), Convert.ToInt32(split[2]));
                }
                catch (Exception e)
                {
                    resp.valor = 2;
                    resp.mensaje = "    *El valor del tiempo Total no es valido" + Environment.NewLine;
                }

                split = txtTiempoPTO.Text.Split(':');
                try
                {
                    TimeSpan tiempo = new TimeSpan(Convert.ToInt32(split[0]), Convert.ToInt32(split[1]), Convert.ToInt32(split[2]));
                }
                catch (Exception e)
                {
                    resp.valor = 2;
                    resp.mensaje = "    *El valor del tiempo PTO no es valido" + Environment.NewLine;
                }
                split = txtTiempoEst.Text.Split(':');
                try
                {
                    TimeSpan tiempo = new TimeSpan(Convert.ToInt32(split[0]), Convert.ToInt32(split[1]), Convert.ToInt32(split[2]));
                }
                catch (Exception e)
                {
                    resp.valor = 2;
                    resp.mensaje = "    *El valor del tiempo EST no es valido" + Environment.NewLine;
                }

                int s = Validaciones.validarInt(txtTanqueIzq.Text.Trim());
                if (s == 1)
                {
                    resp.valor = 2;
                    resp.mensaje += "   *Proporcionar el sello Izq" + Environment.NewLine;
                }
                else if (s == 2)
                {
                    resp.valor = 2;
                    resp.mensaje += "   *sello Izq no es valido" + Environment.NewLine;
                }
                s = Validaciones.validarInt(txtTanqueDer.Text.Trim());
                if (s == 1)
                {
                    resp.valor = 2;
                    resp.mensaje += "   *Proporcionar el sello Der" + Environment.NewLine;
                }
                else if (s == 2)
                {
                    resp.valor = 2;
                    resp.mensaje += "   *sello Der no es valido" + Environment.NewLine;
                }               
                s = Validaciones.validarInt(txtTanqueRes.Text.Trim());
                if (s == 2)
                {
                    resp.valor = 2;
                    resp.mensaje += "   *sello Res no es valido";
                }

                /***************************/
                if (txtTractor.unidadTransporte != null)
                {
                    decimal kmUlt = ultimoKm;
                    if (kmUlt > 0)
                    {
                        if (Convert.ToDecimal(txtKm.Text) < kmUlt)
                        {
                            resp.valor = 2;
                            resp.mensaje += "    -*El KM no puede ser menor al ultimo KM" + Environment.NewLine;
                        }
                        kmUlt += 5000;
                        if (Convert.ToDecimal(txtKm.Text) > kmUlt)
                        {
                            resp.valor = 2;
                            resp.mensaje += "    -*El KM rebasa el Maximo permitido" + Environment.NewLine;
                        }
                    }
                }
                /********************************/

                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        private decimal ultimoKm
        {
            get
            {
                var resp = new RegistroKilometrajeSvc().getUltimoKM(txtTractor.unidadTransporte.clave, (DateTime)dpFecha.SelectedDate);
                if (resp.typeResult == ResultTypes.success)
                {
                    return (decimal)resp.result;
                }
                else
                {
                    return 0;
                }
            }
        }

        private List<CartaPorte> getDetalles()
        {
            List<CartaPorte> list = new List<CartaPorte>();
            foreach (ListViewItem item in lvlProductos.Items)
            {
                list.Add((CartaPorte)item.Content);
            }
            return list;
        }
    }
}
