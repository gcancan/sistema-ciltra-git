﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using Core.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for buscarPreOrdenesCompra.xaml
    /// </summary>
    public partial class buscarPreOrdenesCompra : Window
    {
        Empresa empresa = new Empresa();
        List<PreOrdenCompra> listaPreOrdenCom = new List<PreOrdenCompra>();
        public buscarPreOrdenesCompra(Empresa empresa, List<PreOrdenCompra> listaPreOrdenCom)
        {
            this.empresa = empresa;
            this.listaPreOrdenCom = listaPreOrdenCom;
            InitializeComponent();
        }
        bool inicio = true;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
        }
        public void cargarPantalla()
        {
            var respSvc = new RelacionTiposOrdenServSvc().getTiposOrdenes();
            if (respSvc.typeResult == ResultTypes.success)
            {
                cbxTipoOrden.Items.Add(new TipoOrdenServClass { idTipoOrdServ = 0, nomTipoOrdenSer = "TODOS" });
                foreach (var item in respSvc.result as List<TipoOrdenServClass>)
                {
                    cbxTipoOrden.Items.Add(item);
                }
                cbxTipoOrden.SelectedIndex = 0;
            }
        }

        public List<PreOrdenCompra> getPreOrdByEmpresa()
        {
            try
            {
                cargarPantalla();
                buscarByIdEmpresa();
                bool? resp = ShowDialog();
                if (resp.Value)
                {
                    List<PreOrdenCompra> lista = new List<PreOrdenCompra>();
                    lista = getListaChc();
                    if (lista.Count > 0)
                    {
                        foreach (var item in lista)
                        {
                            item.listaDetalles = buscarDetalles(item.idPreOrdenCompra);
                        }
                        return lista;
                    }
                    return null;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<PreOrdenCompra> getPreOrdTodas()
        {
            try
            {
                cargarPantalla();
                buscarTodos();
                bool? resp = ShowDialog();
                if (resp.Value)
                {
                    List<PreOrdenCompra> lista = new List<PreOrdenCompra>();
                    lista = getListaChc();
                    if (lista.Count > 0)
                    {
                        foreach (var item in lista)
                        {
                            item.listaDetalles = buscarDetalles(item.idPreOrdenCompra);
                        }
                        return lista;
                    }
                    return null;

                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<PreOrdenCompra> getPreOrdByFiltros()
        {
            try
            {
                cargarPantalla();
                getByFiltros();
                bool? resp = ShowDialog();
                if (resp.Value)
                {
                    List<PreOrdenCompra> lista = new List<PreOrdenCompra>();
                    lista = getListaChc();
                    if (lista.Count > 0)
                    {
                        foreach (var item in lista)
                        {
                            item.listaDetalles = buscarDetalles(item.idPreOrdenCompra);
                        }
                        return lista;
                    }
                    return null;

                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private List<PreOrdenCompra> getListaChc()
        {
            List<PreOrdenCompra> lista = new List<PreOrdenCompra>();
            List<CheckBox> listChc = stpPreOrdenes.Children.Cast<CheckBox>().ToList();
            foreach (CheckBox chc in listChc)
            {
                if (chc.IsChecked.Value)
                {
                    lista.Add(chc.Tag as PreOrdenCompra);
                }
            }
            return lista;
        }

        private void buscarByIdEmpresa()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new CotizacionesSvc().getPreOrdCompraByEmpresa(empresa.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapLista(resp.result as List<PreOrdenCompra>);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void buscarTodos()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new CotizacionesSvc().getPreOrdCompraByEmpresa(0);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapLista(resp.result as List<PreOrdenCompra>);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }        

        private void mapLista(List<PreOrdenCompra> listPre)
        {
            foreach (var item in listPre)
            {
                CheckBox chcPrincipal = new CheckBox { Tag = item };
                chcPrincipal.GotFocus += ChcPrincipal_GotFocus;
                chcPrincipal.MouseMove += ChcPrincipal_MouseMove;
                chcPrincipal.MouseLeave += ChcPrincipal_MouseLeave;
                StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal };
                Label lblOrdenServ = new Label
                {
                    Tag = item,
                    Content = item.ordenServicio,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Width = 100
                };
                stpContent.Children.Add(lblOrdenServ);

                Label lblTipoOrden = new Label
                {
                    Tag = item,
                    Content = item.NomTipOrdServ,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Width = 110
                };
                stpContent.Children.Add(lblTipoOrden);

                Label lblunidadTransporte = new Label
                {
                    Tag = item,
                    Content = item.unidadTransporte,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Width = 100
                };
                stpContent.Children.Add(lblunidadTransporte);

                Label lblOrdenTrabajo = new Label
                {
                    Tag = item,
                    Content = item.idOrdenTrabajo,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Width = 100
                };
                stpContent.Children.Add(lblOrdenTrabajo);

                Label lblFecha = new Label
                {
                    Tag = item,
                    Content = item.fechaPreOrden.ToString("dd/MM/yyyy"),
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Width = 130
                };
                stpContent.Children.Add(lblFecha);

                Label lblUsuario = new Label
                {
                    Tag = item,
                    Content = item.usuario,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Width = 130
                };
                stpContent.Children.Add(lblUsuario);

                Label lblEstatus = new Label
                {
                    Tag = item,
                    Content = item.estatus,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    Width = 130
                };
                stpContent.Children.Add(lblEstatus);

                chcPrincipal.Content = stpContent;
                if (item.estatus.ToUpper() == "PRE")
                {
                    chcPrincipal.IsEnabled = !listaPreOrdenCom.Any(x => x.idPreOrdenCompra == item.idPreOrdenCompra);
                }
                else
                {
                    chcPrincipal.IsEnabled = false;
                }
                //chcPrincipal.IsEnabled = item.estatus.ToUpper() == "PRE";


                stpPreOrdenes.Children.Add(chcPrincipal);
            }
        }

        private void ChcPrincipal_MouseLeave(object sender, MouseEventArgs e)
        {
            lvlProductos.ItemsSource = null;
            idOr = 0;
        }
        int idOr = 0;
        private void ChcPrincipal_MouseMove(object sender, MouseEventArgs e)
        {
            if (sender is CheckBox)
            {
                var item = (sender as CheckBox).Tag as PreOrdenCompra;
                if (item.idPreOrdenCompra != idOr)
                {
                    mostrarDetalles(buscarDetalles(item.idPreOrdenCompra));
                }                
            }
        }

        private void ChcPrincipal_GotFocus(object sender, RoutedEventArgs e)
        {
            //if (sender is CheckBox)
            //{
            //    var item = (sender as CheckBox).Tag as PreOrdenCompra;
            //    mostrarDetalles(buscarDetalles(item.idPreOrdenCompra));
            //}
        }

        private void mostrarDetalles(List<DetallePreOrdenCompra> list)
        {
            lvlProductos.ItemsSource = null;
            lvlProductos.ItemsSource = list;
        }

        public List<DetallePreOrdenCompra> buscarDetalles(int id)
        {
            try
            {
                Cursor = Cursors.Wait;
                idOr = id;
                var respDet = new CotizacionesSvc().getDetPreOrdCompraBy(id);
                if (respDet.typeResult == ResultTypes.success)
                {
                    return (respDet.result as List<DetallePreOrdenCompra>);
                }
                else if (respDet.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(respDet);
                    return new List<DetallePreOrdenCompra>(); ;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            getByFiltros();
        }

        private void getByFiltros()
        {
            try
            {
                Cursor = Cursors.Wait;
                stpPreOrdenes.Children.Clear();
                var resp = new CotizacionesSvc().getPreOrdCompraByFiltros(ctrFechas.fechaInicial, ctrFechas.fechaFinal, empresa.clave, 
                    ((cbxTipoOrden.SelectedItem as TipoOrdenServClass).idTipoOrdServ == 0 ? "%" : Convert.ToString((cbxTipoOrden.SelectedItem as TipoOrdenServClass).idTipoOrdServ)));
                if (resp.typeResult == ResultTypes.success)
                {
                    mapLista(resp.result as List<PreOrdenCompra>);
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    if (inicio)
                    {
                        inicio = false;
                        return;
                    }
                    
                    ImprimirMensaje.imprimir(resp);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void Expander_Collapsed(object sender, RoutedEventArgs e)
        {
            
        }

        private void Expander_Expanded(object sender, RoutedEventArgs e)
        {

        }
    }
}