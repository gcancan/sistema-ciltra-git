﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using ctr = MahApps.Metro.Controls;
using mtr = MahApps.Metro;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para controlClienteRemisiones.xaml
    /// </summary>
    public partial class controlClienteRemisiones : UserControl
    {
        Empresa empresa = null;
        ZonaOperativa zonaOperativa = null;
        Zona zona = null;
        List<CartaPorte> listaCartaPortes = null;
        TipoPrecio tipoPrecio = TipoPrecio.SENCILLO;
        bool habilitarPrecio = false;
        Usuario usuario = null;
        public controlClienteRemisiones(Usuario usuario, Empresa empresa, ZonaOperativa zonaOperativa, Zona zona, TipoPrecio tipoPrecio, bool habilitarPrecio, List<CartaPorte> listaCartaPortes = null, bool primera = false)
        {
            InitializeComponent();
            this.usuario = usuario;
            this.empresa = empresa;
            this.zona = zona;
            this.zonaOperativa = zonaOperativa;
            this.listaCartaPortes = listaCartaPortes;
            this.tipoPrecio = tipoPrecio;
            this.habilitarPrecio = habilitarPrecio;
            this.btnQuitar.Visibility = primera ? Visibility.Hidden : Visibility.Visible;
        }
        public controlClienteRemisiones()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            cargarListaClientes();
            if (listaCartaPortes != null)
            {
                Cliente cliente = listaCartaPortes[0].cliente;
                cbxClientes.SelectedItem = cbxClientes.ItemsSource.Cast<Cliente>().ToList().Find(s => s.clave == cliente.clave);
                foreach (var cp in listaCartaPortes)
                {
                    addRemision(cp);
                }
                ctrPrecioFlete.valor = listaCartaPortes.Sum(s=>s.importeReal);
                cbxClientes.IsEnabled = false;
            }
            else
            {
                ctrPrecioFlete.valor = tipoPrecio == TipoPrecio.SENCILLO ? zona.precioFijoSencillo : zona.precioFijoFull;
            }

            if (this.habilitarPrecio)
            {
                ctrPrecioFlete.soloLectura = false;
            }
        }
        private void cargarListaClientes()
        {
            var resp = new ClienteSvc().getClientesALLorById(empresa.clave, 0, true);
            if (resp.typeResult == ResultTypes.success)
            {
                var listaClientes = resp.result as List<Cliente>;
                cbxClientes.ItemsSource = listaClientes;
                if (listaClientes.Count == 1)
                {
                    cbxClientes.SelectedIndex = 0;
                }
            }
            else if (resp.typeResult != ResultTypes.recordNotFound)
            {
                ImprimirMensaje.imprimir(resp);
            }
        }
        private void addRemision(CartaPorte cartaPorte, bool primero = false)
        {
            if (cbxClientes.SelectedItem == null) return;

            Cliente cliente = cbxClientes.SelectedItem as Cliente;
            cartaPorte = cartaPorte ??
                new CartaPorte
                {
                    cliente = cliente,
                    zonaSelect = zona,
                    viaje = true,
                    PorcIVA = cliente.impuesto.valor,
                    PorcRetencion = cliente.impuestoFlete.valor,
                    tipoPrecio = this.tipoPrecio,
                    tipoPrecioChofer = this.tipoPrecio
                };
            StackPanel stpContent = new StackPanel { Tag = cartaPorte, Orientation = Orientation.Horizontal };

            TextBox txtRemision = new TextBox
            {
                Tag = cartaPorte,
                FontWeight = FontWeights.Medium,
                Width = 146,
                Margin = new Thickness(2)
            };
            txtRemision.TextChanged += TxtRemision_TextChanged;
            txtRemision.Text = cartaPorte.remision;

            TextBox txtProducto = new TextBox
            {
                Tag = stpContent,
                FontWeight = FontWeights.Medium,
                Width = 228,
                Margin = new Thickness(2),
            };
            txtProducto.KeyDown += TxtProducto_KeyDown;
            txtProducto.DataContextChanged += TxtProducto_DataContextChanged;
            txtProducto.DataContext = cartaPorte.producto == null ? null : cartaPorte.producto;
            mapProductos(cartaPorte.producto, txtProducto);

            Button btnLimpiarProducto = new Button
            {
                Content = "X",
                Background = Brushes.Red,
                Foreground = Brushes.White,
                VerticalAlignment = VerticalAlignment.Center,
                Tag = txtProducto,
                Visibility = Visibility.Visible
            };
            ctr.ControlsHelper.SetCornerRadius(btnLimpiarProducto, new CornerRadius(6));
            btnLimpiarProducto.Click += BtnLimpiarProducto_Click;

            controlDecimal ctrCantidad = new controlDecimal { Width = 110, Tag = cartaPorte };
            ctrCantidad.txtDecimal.Tag = ctrCantidad;
            ctrCantidad.txtDecimal.TextChanged += TxtDecimal_TextChanged;
            ctrCantidad.valor = cartaPorte.volumenDescarga;

            Label lblMedida = new Label
            {
                Content = cartaPorte.producto == null ? string.Empty : cartaPorte.producto.clave_unidad_de_medida,
                Width = 136,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                FontWeight = FontWeights.Medium,
                VerticalAlignment = VerticalAlignment.Center,
                Name = "lblMedida"
            };
            ControlUnidadesMedida ctrUnidadMedida = new ControlUnidadesMedida(cartaPorte.unidadMedida)
            {
                Tag = cartaPorte
            };
            ctrUnidadMedida.cbxUnidadMedida.Tag = ctrUnidadMedida;
            ctrUnidadMedida.cbxUnidadMedida.SelectionChanged += CbxUnidadMedida_SelectionChanged;
            Button btnQuitarRemision = new Button
            {
                Content = "X",
                Background = Brushes.Red,
                Foreground = Brushes.White,
                VerticalAlignment = VerticalAlignment.Center,
                Tag = stpContent
            };
            ctr.ControlsHelper.SetCornerRadius(btnQuitarRemision, new CornerRadius(6));
            btnQuitarRemision.Click += BtnQuitarRemision_Click;

            stpContent.Children.Add(txtRemision);
            stpContent.Children.Add(txtProducto);
            stpContent.Children.Add(btnLimpiarProducto);
            stpContent.Children.Add(ctrCantidad);
            //stpContent.Children.Add(lblMedida);
            stpContent.Children.Add(ctrUnidadMedida);

            //if (cartaPorte.Consecutivo == 0)
            //{
            if (!primero) stpContent.Children.Add(btnQuitarRemision);
            //}



            stpRemisiones.Children.Add(stpContent);
        }

        private void CbxUnidadMedida_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender != null)
            {
                ComboBox cbx = sender as ComboBox;
                ControlUnidadesMedida ctr = cbx.Tag as ControlUnidadesMedida;
                CartaPorte cp = ctr.Tag as CartaPorte;
                cp.unidadMedida = ctr.unidadMedida;
            }
        }

        private void BtnQuitarRemision_Click(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                Button btn = sender as Button;
                StackPanel stp = btn.Tag as StackPanel;

                CartaPorte cp = stp.Tag as CartaPorte;
                if (cp.Consecutivo == 0)
                {
                    stpRemisiones.Children.Remove(stp);
                }
                else
                {
                    OperationResult resp = new ControlBajarCartaPortes().darBajaRemisiones(new List<int> { cp.Consecutivo }, new Usuario());
                    if (resp.typeResult == ResultTypes.success)
                    {
                        stpRemisiones.Children.Remove(stp);
                    }
                }                
            }
        }

        private void TxtDecimal_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender != null)
            {
                TextBox txt = sender as TextBox;
                controlDecimal ctrDecimal = txt.Tag as controlDecimal;
                CartaPorte cp = ctrDecimal.Tag as CartaPorte;
                cp.vDescarga = ctrDecimal.valor;
                cp.volumenCarga = ctrDecimal.valor;
                cp.volumenDescarga = ctrDecimal.valor;
            }
        }

        private void BtnLimpiarProducto_Click(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                Button btn = sender as Button;
                TextBox txt = btn.Tag as TextBox;
                mapProductos(null, txt);
            }
        }

        private void TxtRemision_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    TextBox txt = sender as TextBox;
                    CartaPorte cp = txt.Tag as CartaPorte;
                    cp.remision = txt.Text;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private void TxtProducto_KeyDown(object sender, KeyEventArgs e)
        {
            if (sender == null) return;
            if (cbxClientes.SelectedItem == null) return;

            if (e.Key == Key.Enter)
            {
                TextBox txtProducto = sender as TextBox;
                List<Producto> list = new List<Producto>();
                list = this.listaProductos.FindAll(S => S.descripcion.IndexOf(txtProducto.Text, StringComparison.OrdinalIgnoreCase) >= 0);

                selectProducto producto = new selectProducto(txtProducto.Text);
                if (list.Count == 1)
                {
                    this.mapProductos(list[0], txtProducto);
                }
                else
                {
                    Producto producto3 = producto.buscarProductos(this.listaProductos);
                    this.mapProductos(producto3, txtProducto);
                }
            }
        }
        private void mapProductos(Producto producto, TextBox txtProducto)
        {
            if (producto != null)
            {
                txtProducto.Text = producto.descripcion;
                txtProducto.IsEnabled = false;
                txtProducto.DataContext = producto;
            }
            else
            {
                txtProducto.Clear();
                txtProducto.IsEnabled = true;
                txtProducto.DataContext = null;
            }
        }
        private void TxtProducto_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (sender != null)
            {
                TextBox textBox = sender as TextBox;
                StackPanel stp = textBox.Tag as StackPanel;
                CartaPorte cartaPorte = stp.Tag as CartaPorte;
                cartaPorte.producto = textBox.DataContext == null ? null : textBox.DataContext as Producto;

                foreach (var obj in stp.Children.Cast<object>().ToList())
                {
                    if (obj is Label)
                    {
                        Label lbl = obj as Label;
                        lbl.Content = cartaPorte.producto == null ? string.Empty : cartaPorte.producto.clave_unidad_de_medida;
                    }
                    if (obj is ControlUnidadesMedida)
                    {
                        ControlUnidadesMedida ctr = obj as ControlUnidadesMedida;
                        if (cartaPorte.producto == null)
                        {
                            ctr.unidadMedida = null;
                        }
                        else
                        {
                            ctr.unidadMedida = cartaPorte.producto.clave_unidad_de_medida;
                        }
                        
                    }
                }
            }
        }
        private List<Producto> listaProductos;
        private void CbxCliente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;

                stpRemisiones.Children.Clear();


                if (cbxClientes.SelectedItem == null) return;

                if (listaCartaPortes == null) addRemision(null, true);

                Cliente cliente = cbxClientes.SelectedItem as Cliente;
                OperationResult result2 = new ProductoSvc().getALLProductosByidFraccionEmpresa(empresa.clave, cliente.clave);
                if (result2.typeResult == ResultTypes.success)
                {
                    this.listaProductos = (List<Producto>)result2.result;
                }
                else
                {
                    this.listaProductos = new List<Producto>();
                    ImprimirMensaje.imprimir(result2);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (cbxClientes.SelectedItem == null) return;
            addRemision(null, false);
        }
        public List<CartaPorte> getListaCartaPortes()
        {
            List<CartaPorte> lista = new List<CartaPorte>();
            lista = stpRemisiones.Children.Cast<StackPanel>().ToList().Select(s => s.Tag as CartaPorte).ToList();
            int i = 0;
            foreach (CartaPorte cp in lista)
            {
                cp.precio = 0;
                cp.precioFijo = ctrPrecioFlete.valor;
                cp.importeReal = ctrPrecioFlete.valor;
                
                if (i > 0)
                {
                    cp.importeReal = 0;
                }
                i++;
            }
            return lista;
        }
    }
}
