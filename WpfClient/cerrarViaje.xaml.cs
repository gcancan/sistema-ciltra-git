﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using Core.Utils;
    using Microsoft.VisualBasic;
    using Microsoft.Win32;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Text;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Forms;
    using System.Windows.Input;
    using System.Windows.Markup;
    using System.Windows.Media;
    using Xceed.Wpf.Toolkit;

    public partial class cerrarViaje : WpfClient.ViewBase
    {
        public int _empresa;
        public int _idCliente;
        private bool privilegioFecha = false;
        private int lResult = 0;
        private double lFolio;
        private StringBuilder lSerieDocto = new StringBuilder(12);
        private Declaraciones.tDocumento ltDocto = new Declaraciones.tDocumento();
        private Declaraciones.tMovimiento ltMovto = new Declaraciones.tMovimiento();
        private int lIdDocto;
        private int LIdMovto;
        public cerrarViaje()
        {
            this.InitializeComponent();
        }
        private bool abrirEmpresa()
        {
            string ruta = System.Windows.Forms.Application.StartupPath + @"\config.ini";
            this.lResult = Declaraciones.fAbreEmpresa(@"C:\Compac\Empresas\" + new Core.Utils.Util().Read("CONECTION_STRING_COMERCIAL", "DB_NAME", ruta));
            if (this.lResult == 0)
            {
                return true;
            }
            Declaraciones.MuestraError(this.lResult);
            return false;
        }

        private void bb_Click(object sender, RoutedEventArgs e)
        {
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            this.buscarCartasPorte();
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            if (this.lvlSalidas.SelectedItem != null)
            {
                Viaje content = (Viaje)((System.Windows.Controls.ListViewItem)this.lvlSalidas.SelectedItem).Content;
                if ((content.EstatusGuia.ToUpper() == "ACTIVO") || (content.EstatusGuia.ToUpper() == "EXTERNO"))
                {
                    OperationResult result = this.eliminar();
                    switch (result.typeResult)
                    {
                        case ResultTypes.success:
                            System.Windows.MessageBox.Show(result.mensaje, "Correcto", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                            break;

                        case ResultTypes.error:
                            System.Windows.MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
                            break;

                        case ResultTypes.warning:
                            System.Windows.MessageBox.Show(result.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                            break;
                    }
                }
                else
                {
                    System.Windows.MessageBox.Show("Este elemento ya no se puede cancelar", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.lvlSalidas.SelectedItem != null)
                {
                    Viaje content = (Viaje)((System.Windows.Controls.ListViewItem)this.lvlSalidas.SelectedItem).Content;
                    if ((content.EstatusGuia.ToUpper() == "ACTIVO") || (content.EstatusGuia.ToUpper() == "EXTERNO"))
                    {
                        base.mainWindow.guardar();
                    }
                    else
                    {
                        System.Windows.MessageBox.Show("Este elemento ya no se puede Cerrar", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
            }
            catch (Exception exception)
            {
                System.Windows.MessageBox.Show(exception.Message, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            finally
            {
                base.mainWindow.scrollContainer.IsEnabled = true;
            }
        }

        internal void buscarCartasPorte()
        {
            try
            {
                base.Cursor = System.Windows.Input.Cursors.Wait;
                this.lvlSalidas.Items.Clear();
                if (this.cbxCliente.SelectedItem != null)
                {
                    List<Viaje> list = new List<Viaje>();
                    OperationResult result = new CartaPorteSvc().getCartaPorteByFecha(this.dtpFechaInicio.SelectedDate.Value, this.dtpFechaFin.SelectedDate.Value.AddDays(1.0), this.chcTodos.IsChecked.Value ? "%" : ((Cliente)this.cbxCliente.SelectedItem).clave.ToString(), ((Empresa)this.cbxEmpresa.SelectedItem).clave);
                    if (result.typeResult == ResultTypes.error)
                    {
                        System.Windows.MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
                    }
                    else if (result.typeResult == ResultTypes.warning)
                    {
                        System.Windows.MessageBox.Show(result.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                    else if ((result.typeResult != ResultTypes.recordNotFound) && (result.typeResult == ResultTypes.success))
                    {
                        foreach (Viaje viaje in (List<Viaje>)result.result)
                        {
                            list.Add(viaje);
                        }
                    }
                    if (list.Count == 0)
                    {
                        System.Windows.MessageBox.Show("No se encontraron registros", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                    else
                    {
                        this.mapLista(list);
                    }
                    this.lvlProductos.Items.Clear();
                    this.sumarVolumenCargas();
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }

        private void cambiarLista(Viaje viaje, TipoAccion accion)
        {
            int insertIndex = 0;
            System.Windows.Controls.ListViewItem item = new System.Windows.Controls.ListViewItem();
            System.Windows.Controls.ListViewItem removeItem = new System.Windows.Controls.ListViewItem();
            foreach (System.Windows.Controls.ListViewItem item3 in (IEnumerable)this.lvlSalidas.Items)
            {
                Viaje content = (Viaje)item3.Content;
                if (content.NumGuiaId == viaje.NumGuiaId)
                {
                    switch (accion)
                    {
                        case TipoAccion.CANCELAR:
                            item3.Foreground = Brushes.OrangeRed;
                            break;

                        case TipoAccion.FINALIZAR:
                            item3.Foreground = Brushes.Blue;
                            break;
                    }
                    removeItem = item3;
                    item3.Content = viaje;
                    item = item3;
                    break;
                }
                insertIndex++;
            }
            this.lvlSalidas.Items.Remove(removeItem);
            this.lvlSalidas.Items.Insert(insertIndex, removeItem);
        }

        private void cbxCliente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                base.Cursor = System.Windows.Input.Cursors.Wait;
                if (this.cbxCliente.SelectedItem != null)
                {
                    this.mostrarMensaje();
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }

        private void cbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                base.Cursor = System.Windows.Input.Cursors.Wait;
                if (this.cbxEmpresa.SelectedItem != null)
                {
                    OperationResult resp = new ClienteSvc().getClientesALLorById(((Empresa)this.cbxEmpresa.SelectedItem).clave, 0, true);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        this.cbxCliente.ItemsSource = (List<Cliente>)resp.result;
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
                else
                {
                    this.cbxCliente.ItemsSource = null;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = System.Windows.Input.Cursors.Arrow;
            }
        }

        public override void cerrar()
        {
            try
            {
                Declaraciones.fCierraEmpresa();
                Declaraciones.fTerminaSDK();
            }
            catch (Exception)
            {
            }
            base.cerrar();
        }

        private List<Viaje> convertListViajes(List<ViajeBachoco> result)
        {
            List<Viaje> list = new List<Viaje>();
            try
            {
                foreach (ViajeBachoco bachoco in result)
                {
                    Viaje item = new Viaje
                    {
                        NumGuiaId = bachoco.idViaje,
                        NumViaje = bachoco.idViaje,
                        cliente = bachoco.cliente,
                        tractor = bachoco.tractor,
                        remolque = bachoco.tolva1,
                        dolly = bachoco.dolly,
                        remolque2 = bachoco.tolva2,
                        FechaHoraViaje = bachoco.fechaViaje,
                        operador = bachoco.chofer,
                        EstatusGuia = bachoco.estatus.ToString(),
                        UserViaje = bachoco.usuarioBascula.idUsuario,
                        isBachoco = true,
                        FechaHoraFin = bachoco.fechaFin,
                        listDetalles = new List<CartaPorte>()
                    };
                    foreach (CartaPorteBachoco bachoco2 in bachoco.listaCartaPortes)
                    {
                        CartaPorte porte1 = new CartaPorte
                        {
                            Consecutivo = bachoco2.idCartaPorte,
                            zonaSelect = bachoco2.destino,
                            producto = bachoco2.producto,
                            PorcIVA = bachoco2.iva,
                            PorcRetencion = bachoco2.retencion,
                            precio = bachoco2.precio,
                            volumenDescarga = bachoco2.volumenCarga,
                            numGuiaId = bachoco2.idViaje,
                            volumenCarga = decimal.Zero,
                            remision = bachoco2.remision
                        };
                        item.listDetalles.Add(porte1);
                    }
                    list.Add(item);
                }
                return list;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private bool crearDocumentoRemision(Viaje viaje) =>
            (this.abrirEmpresa() && this.llenarEstructuraDocumento(viaje));

        public override OperationResult eliminar()
        {
            try
            {
                System.Windows.Controls.ListViewItem selectedItem = (System.Windows.Controls.ListViewItem)this.lvlSalidas.SelectedItem;
                Viaje content = (Viaje)selectedItem.Content;
                string str = Interaction.InputBox("Escribir el motivo de la baja", "Motivo de Baja", "", -1, -1);
                if (!string.IsNullOrEmpty(str))
                {
                    OperationResult result = new CartaPorteSvc().cerrarCancelarCartaPorte(content.NumGuiaId, TipoAccion.CANCELAR, base.mainWindow.inicio._usuario.idUsuario, str, null);
                    if (result.typeResult == ResultTypes.success)
                    {
                        EnvioCorreo correo = new EnvioCorreo();
                        //string str2 = correo.envioCorreoCSI(content, true);
                        this.cambiarLista(((List<Viaje>)result.result)[0], TipoAccion.CANCELAR);
                    }
                    return result;
                }
                return new OperationResult
                {
                    mensaje = "Se deber\x00e1 proporcionar un motivo para proceder con la baja",
                    valor = 2,
                    result = null
                };
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
        }

        private TipoPrecio establecerPrecio(Viaje viaje)
        {
            if ((viaje.dolly != null) || (viaje.remolque2 != null))
            {
                return TipoPrecio.FULL;
            }
            if (viaje.remolque.tipoUnidad.descripcion == "TOLVA 35 TON")
            {
                return TipoPrecio.SENCILLO35;
            }
            return TipoPrecio.SENCILLO;
        }

        public override OperationResult guardar()
        {
            OperationResult result4;
            OperationResult result = new OperationResult
            {
                valor = 0,
                mensaje = "SE TERMINO EL PROCESO"
            };
            List<Viaje> list = new List<Viaje>();
            try
            {
                IList selectedItems = this.lvlSalidas.SelectedItems;
                foreach (System.Windows.Controls.ListViewItem item in selectedItems)
                {
                    Viaje content = (Viaje)item.Content;
                    if (content.EstatusGuia.ToUpper() == "ACTIVO")
                    {
                        OperationResult result2 = this.validarFecha();
                        if (result2.typeResult > ResultTypes.success)
                        {
                            return result2;
                        }
                        bool omitirValidacionViaje = true;
                        if (content.taller || content.tallerExterno)
                        {

                        }
                        else
                        {
                            omitirValidacionViaje = new acompletarCartaPorte(content, base.mainWindow).ShowDialog().Value;
                        }


                        if (!omitirValidacionViaje)
                        {
                            OperationResult resp = new OperationResult
                            {
                                valor = 3,
                                mensaje = $"SE REQUIERE DE TODA LA INFORMACIÓN PARA CERRAR EL VIAJE {content.NumGuiaId}"
                            };
                            ImprimirMensaje.imprimir(resp);
                        }
                        else
                        {
                            result = new CartaPorteSvc().cerrarCancelarCartaPorte(content.NumGuiaId, TipoAccion.FINALIZAR, base.mainWindow.inicio._usuario.idUsuario, "", this.privilegioFecha ? this.dtFecha.Value : null);
                            OperationResult result3 = new CartaPorteSvc().getCartaPorteById(content.NumGuiaId);
                            if (result3.typeResult == ResultTypes.success)
                            {
                                list.Add(((List<Viaje>)result3.result)[0]);
                            }
                        }
                    }
                }
                foreach (Viaje viaje2 in list)
                {
                    this.cambiarLista(viaje2, TipoAccion.FINALIZAR);
                }
                result4 = result;
            }
            catch (Exception exception)
            {
                result4 = new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
            finally
            {
                base.mainWindow.scrollContainer.IsEnabled = true;
                this.mostrarMensaje();
            }
            return result4;
        }

        private bool hayError(int error)
        {
            if (error != 0)
            {
                Declaraciones.MuestraError(error);
                return true;
            }
            return false;
        }

        private void iniciaSDK()
        {
            try
            {
                string name = "SOFTWARE\\\\Computaci\x00f3n en Acci\x00f3n, SA CV\\\\CONTPAQ I COMERCIAL";
                this.lResult = Declaraciones.SetCurrentDirectory(Registry.LocalMachine.OpenSubKey(name).GetValue("DirectorioBase").ToString());
                if (this.lResult != 0)
                {
                    Declaraciones.fInicializaSDK();
                    Declaraciones.fSetNombrePAQ("CONTPAQ I COMERCIAL");
                }
                else
                {
                    Declaraciones.MuestraError(this.lResult);
                }
            }
            catch (Exception exception)
            {
                System.Windows.MessageBox.Show("Ocurrio un error al tratar de iniciar el SDK." + Environment.NewLine + exception.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
                base.mainWindow.scrollContainer.IsEnabled = false;
            }
        }
        private bool llenarEstructuraDocumento(Viaje viaje)
        {
            this.lSerieDocto.Append("");
            if (!GuardarAgente.guardarAgente(viaje.operador.clave.ToString(), viaje.operador.nombre))
            {
                return false;
            }
            if (this.hayError(Declaraciones.fSiguienteFolio("3", this.lSerieDocto, ref this.lFolio)))
            {
                return false;
            }
            this.ltDocto.aCodConcepto = "3";
            this.ltDocto.aSerie = this.lSerieDocto.ToString();
            this.ltDocto.aFolio = viaje.NumGuiaId;
            this.ltDocto.aFecha = viaje.FechaHoraViaje.ToString("MM/dd/yyyy");
            this.ltDocto.aCodigoCteProv = viaje.cliente.clave.ToString();
            this.ltDocto.aCodigoAgente = viaje.operador.clave.ToString();
            this.ltDocto.aSistemaOrigen = 0;
            this.ltDocto.aNumMoneda = 1;
            this.ltDocto.aTipoCambio = 1.0;
            this.ltDocto.aImporte = 0.0;
            this.ltDocto.aDescuentoDoc1 = 0.0;
            this.ltDocto.aDescuentoDoc2 = 0.0;
            this.ltDocto.aAfecta = 1;
            if (this.hayError(Declaraciones.fAltaDocumento(ref this.lIdDocto, ref this.ltDocto)))
            {
                return false;
            }
            foreach (CartaPorte porte in viaje.listDetalles)
            {
                this.ltMovto.aCodAlmacen = "1";
                this.ltMovto.aConsecutivo = 1;
                this.ltMovto.aCodProdSer = porte.zonaSelect.clave.ToString();
                this.ltMovto.aUnidades = double.Parse(porte.volumenDescarga.ToString());
                this.ltMovto.aPrecio = ((viaje.remolque2 != null) || (viaje.remolque.tipoUnidad.descripcion == "TOLVA 35 TON")) ? double.Parse(porte.zonaSelect.costoFull.ToString()) : double.Parse(porte.zonaSelect.costoSencillo.ToString());
                this.ltMovto.aCosto = 0.0;
                if (this.hayError(Declaraciones.fAltaMovimiento(this.lIdDocto, ref this.LIdMovto, ref this.ltMovto)))
                {
                    return false;
                }
                OperationResult result = new SDKconsultasSvc().updateMovimiento(this.LIdMovto.ToString(), porte.remision.ToString(), porte.producto.descripcion);
            }
            return true;
        }

        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            this.lvlProductos.Items.Clear();
            Viaje content = (Viaje)((System.Windows.Controls.ListViewItem)sender).Content;
            this.mapDetalles(content);
        }

        private void mapDetalles(Viaje cartaPorte)
        {
            foreach (CartaPorte porte in cartaPorte.listDetalles)
            {
                porte.tipoPrecio = this.establecerPrecio(cartaPorte);
                System.Windows.Controls.ListViewItem newItem = new System.Windows.Controls.ListViewItem
                {
                    Content = porte,
                    FontWeight = FontWeights.Bold
                };
                this.lvlProductos.Items.Add(newItem);
            }
            this.sumarVolumenCargas();
        }

        private void mapLista(List<Viaje> result)
        {
            this.lvlSalidas.Items.Clear();
            this.lvlProductos.Items.Clear();
            foreach (Viaje viaje in result)
            {
                System.Windows.Controls.ListViewItem newItem = new System.Windows.Controls.ListViewItem
                {
                    Content = viaje,
                    FontWeight = FontWeights.Bold
                };
                newItem.Selected += new RoutedEventHandler(this.Lvl_Selected);
                if (viaje.EstatusGuia.ToUpper() == "ACTIVO")
                {
                    newItem.Foreground = Brushes.SteelBlue;
                }
                if (viaje.EstatusGuia.ToUpper() == "CANCELADO")
                {
                    newItem.Foreground = Brushes.OrangeRed;
                }
                if (viaje.EstatusGuia.ToUpper() == "FINALIZADO")
                {
                    newItem.Foreground = Brushes.Blue;
                }
                this.lvlSalidas.Items.Add(newItem);
            }
        }

        private void mostrarMensaje()
        {
            OperationResult result = new CartaPorteSvc().ConsultarViajesActivos(((Cliente)this.cbxCliente.SelectedItem).clave);
            if (result.typeResult == ResultTypes.success)
            {
                List<object> list = (List<object>)result.result;
                this.lblMensaje.Visibility = Visibility.Visible;
                this.lblTxtMensaje.Text = $"Existe(n) {list[1].ToString()} viaje(s) activos desde la fecha {((DateTime)list[0]).ToString("dd/MMMM/yyyy")}.";
            }
            else
            {
                this.lblMensaje.Visibility = Visibility.Collapsed;
            }
        }

        internal void sumarVolumenCargas()
        {
            decimal num = new decimal(0, 0, 0, false, 2);
            decimal num2 = new decimal(0, 0, 0, false, 2);
            decimal num3 = new decimal(0, 0, 0, false, 2);
            decimal impRetencion = new decimal(0, 0, 0, false, 2);
            foreach (System.Windows.Controls.ListViewItem item in (IEnumerable)this.lvlProductos.Items)
            {
                CartaPorte content = (CartaPorte)item.Content;
                num += content.volumenDescarga;
                num2 += content.importeReal;
                num3 += content.ImporIVA;
                impRetencion = content.ImpRetencion;
            }
            this.txtTotalImporte.Text = num2.ToString("0.00");
            this.txtTotalRetencion.Text = num3.ToString("0.00");
            this.txtTotalFlete.Text = impRetencion.ToString("0.00");
            this.txtSumaTotal.Text = ((num3 + num2) - impRetencion).ToString("0.00");
        }
        private void txtFolioViaje_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                char[] separator = new char[] { '-' };
                string[] strArray = this.txtFolioViaje.Text.Trim().Split(separator);
                int num = 0;
                if (!int.TryParse(strArray[0].Trim(), out num))
                {
                    char[] chArray2 = new char[] { Convert.ToChar("'") };
                    strArray = this.txtFolioViaje.Text.Trim().Split(chArray2);
                    if (!int.TryParse(strArray[0].Trim(), out num))
                    {
                        System.Windows.MessageBox.Show("Al parecer el Folio Proporcionado no es valido", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        return;
                    }
                }
                OperationResult result = new CartaPorteSvc().getCartaPorteById(Convert.ToInt32(strArray[0].Trim()));
                if (result.typeResult == ResultTypes.error)
                {
                    System.Windows.MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
                }
                else if (result.typeResult == ResultTypes.warning)
                {
                    System.Windows.MessageBox.Show(result.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else if ((result.typeResult != ResultTypes.recordNotFound) && (result.typeResult == ResultTypes.success))
                {
                    this.mapLista((List<Viaje>)result.result);
                    this.lvlSalidas.SelectedIndex = 0;
                    Viaje content = (Viaje)((System.Windows.Controls.ListViewItem)this.lvlSalidas.SelectedItem).Content;
                    if (content.EstatusGuia == "ACTIVO")
                    {
                        this.guardar();
                    }
                }
                this.txtFolioViaje.Clear();
            }
        }

        private bool validarExistenProductos(List<CartaPorte> listDetalles)
        {
            foreach (CartaPorte porte in listDetalles)
            {
                OperationResult result = new SDKconsultasSvc().existeProductoComercial(porte.zonaSelect.clave.ToString(), "");
                if (result.typeResult == ResultTypes.error)
                {
                    System.Windows.MessageBox.Show(result.mensaje + Environment.NewLine + "Servicio: " + porte.zonaSelect.descripcion);
                    return false;
                }
                if (result.typeResult == ResultTypes.warning)
                {
                    System.Windows.MessageBox.Show(result.mensaje + Environment.NewLine + "Servicio: " + porte.zonaSelect.descripcion);
                    return false;
                }
                if (result.typeResult == ResultTypes.recordNotFound)
                {
                    if (this.abrirEmpresa())
                    {
                        GuardarServicio.guardarSDK(porte.zonaSelect, eAction.NUEVO);
                        continue;
                    }
                    Declaraciones.fCierraEmpresa();
                }
                if (result.typeResult != ResultTypes.success)
                {
                }
            }
            return true;
        }

        private OperationResult validarFecha()
        {
            try
            {
                DateTime now = DateTime.Now;
                if (DateTime.TryParse(this.dtFecha.Value.ToString(), out now))
                {
                    return new OperationResult
                    {
                        valor = 0,
                        mensaje = "Fecha Correcta",
                        result = null
                    };
                }
                return new OperationResult
                {
                    valor = 2,
                    mensaje = "El formato de la fecha no es correcto",
                    result = null
                };
            }
            catch (Exception exception)
            {
                return new OperationResult
                {
                    valor = 1,
                    mensaje = exception.Message,
                    result = null
                };
            }
        }

        private bool validarIsFull(Viaje cartaPorte) =>
            ((cartaPorte.remolque.tipoUnidad.descripcion == "TOLVA 35 TON") || ((cartaPorte.dolly != null) || (cartaPorte.remolque2 != null)));

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
            this.dtpFechaInicio.SelectedDate = new DateTime?(Convert.ToDateTime(DateTime.Now.ToShortDateString()));
            this.dtpFechaFin.SelectedDate = new DateTime?(Convert.ToDateTime(DateTime.Now.ToShortDateString()));
            this.dtFecha.Value = new DateTime?(DateTime.Now);
            if (new PrivilegioSvc().consultarPrivilegio("APLICAR_FECHA_VIAJE", base.mainWindow.inicio._usuario.idUsuario).typeResult == ResultTypes.success)
            {
                this.dtFecha.Visibility = Visibility.Visible;
                this.privilegioFecha = true;
            }
            else
            {
                this.dtFecha.Visibility = Visibility.Collapsed;
            }
            OperationResult result = new PrivilegioSvc().consultarPrivilegio("ISADMIN", base.mainWindow.inicio._usuario.idUsuario);
            bool flag = false;
            if (result.typeResult == ResultTypes.success)
            {
                flag = true;
            }
            result = new PrivilegioSvc().consultarPrivilegio("ADMIN_COMBO_CLIENTES", base.mainWindow.inicio._usuario.idUsuario);
            bool flag2 = false;
            if (result.typeResult == ResultTypes.success)
            {
                flag2 = true;
            }
            OperationResult resp = new EmpresaSvc().getEmpresasALLorById(0);
            if (resp.typeResult == ResultTypes.success)
            {
                this.cbxEmpresa.ItemsSource = (List<Empresa>)resp.result;
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
            }
            int num = 0;
            foreach (Empresa empresa in (IEnumerable)this.cbxEmpresa.Items)
            {
                if (empresa.clave == base.mainWindow.empresa.clave)
                {
                    this.cbxEmpresa.SelectedIndex = num;
                    break;
                }
                num++;
            }
            if (this.cbxCliente.Items.Count > 0)
            {
                num = 0;
                foreach (Cliente cliente in (IEnumerable)this.cbxCliente.Items)
                {
                    if (cliente.clave == base.mainWindow.cliente.clave)
                    {
                        this.cbxCliente.SelectedIndex = num;
                        break;
                    }
                    num++;
                }
            }
            this.cbxEmpresa.IsEnabled = flag;
            this.cbxCliente.IsEnabled = flag2;
        }
    }
}
