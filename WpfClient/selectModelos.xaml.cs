﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectModelos.xaml
    /// </summary>
    public partial class selectModelos : Window
    {
        public selectModelos()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
        
        private void TxtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(this.lvlModelos.ItemsSource).Refresh();
        }
        public Modelo buscarModelo()
        {
            try
            {
                buscarTodosModelos();
                bool? result = ShowDialog();
                if (result.Value && lvlModelos.SelectedItem != null)
                {
                    return (lvlModelos.SelectedItem as ListViewItem).Content as Modelo;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private void buscarTodosModelos()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new ModeloSvc().getAllModelos();
                if (resp.typeResult  ==  ResultTypes.success)
                {
                    var lisModelos = resp.result as List<Modelo>;
                    llenarLista(lisModelos);
                }
                else 
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void llenarLista(List<Modelo> lisModelos)
        {
            try
            {
                lvlModelos.ItemsSource = null;
                List<ListViewItem> list = new List<ListViewItem>();
                foreach (Modelo col in lisModelos)
                {
                    ListViewItem item = new ListViewItem
                    {
                        Content = col
                    };
                    item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                    list.Add(item);
                }
                this.lvlModelos.ItemsSource = list;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlModelos.ItemsSource);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtBuscador.Text) ||
            ((((Modelo)(item as ListViewItem).Content).modelo.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }
    }
}
