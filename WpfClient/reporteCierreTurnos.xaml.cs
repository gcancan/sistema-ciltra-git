﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para reporteCierreTurnos.xaml
    /// </summary>
    public partial class reporteCierreTurnos : ViewBase
    {
        public reporteCierreTurnos()
        {
            InitializeComponent();
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ctrZonaOperativa.cargarControl(mainWindow.zonaOperativa, mainWindow.usuario.idUsuario);
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
        }
        List<NivelesTanque> list = new List<NivelesTanque>();
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                list = new List<NivelesTanque>();
                lvlResultado.Items.Clear();
                var resp = new NivelesTanqueSvc().getNivelTanqueByFiltros(ctrZonaOperativa.zonaOperativa, ctrFechas.fechaInicial, ctrFechas.fechaFinal);
                if (resp.typeResult == ResultTypes.success)
                {
                    list = resp.result as List<NivelesTanque>;
                    llenarLista(list);
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void llenarLista(List<NivelesTanque> list)
        {
            try
            {
                Cursor = Cursors.Wait;
                foreach (var nivel in list)
                {
                    ListViewItem lvl = new ListViewItem();

                    if (nivel.fechaFinal != null)
                    {
                        ContextMenu menu = new ContextMenu();
                        MenuItem menuItem = new MenuItem { Header = "REIMPRIMIR", Tag = nivel };
                        menuItem.Click += MenuItem_Click;
                        menu.Items.Add(menuItem);
                        lvl.ContextMenu = menu;
                    }

                    lvl.Content = nivel;
                    lvl.MouseMove += Lvl_MouseMove;
                    lvl.MouseLeave += Lvl_MouseLeave;
                    lvlResultado.Items.Add(lvl);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    MenuItem menuItem = sender as MenuItem;
                    NivelesTanque nivelesTanque = menuItem.Tag as NivelesTanque;

                    var resp = new NivelesTanqueSvc().getResumenCargasInternas(nivelesTanque.fechaInicio, nivelesTanque.fechaFinal.Value, ctrZonaOperativa.zonaOperativa.idZonaOperativa);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        new ReportView().imprimirCierreContadores(nivelesTanque, resp.result as List<ResumenCargasInternas>);
                    }                    
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void Lvl_MouseLeave(object sender, MouseEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                lvlDetalle.Items.Clear();
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void Lvl_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    ListViewItem lvl = sender as ListViewItem;
                    NivelesTanque nivelesTanque = lvl.Content as NivelesTanque;
                    mapListaDetalle(nivelesTanque.listaDetalles);
                }
                else
                {
                    mapListaDetalle(null);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapListaDetalle(List<DetalleNivelesTanque> listaDetalles)
        {
            lvlDetalle.Items.Clear();
            if (listaDetalles != null)
            {
                foreach (var item in listaDetalles)
                {
                    lvlDetalle.Items.Add(item);
                }
            }
            else
            {
                lvlDetalle.Items.Clear();
            }
        }

        private void btnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (list.Count == 0) return;

                new ReportView().exportarCierreTurnos(list);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
