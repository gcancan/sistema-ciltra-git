﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editMesContable.xaml
    /// </summary>
    public partial class editMesContable : ViewBase
    {
        public editMesContable()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            cbxMes.ItemsSource = Enum.GetValues(typeof(Mes));
            cbxMes.SelectedIndex = DateTime.Now.Month - 1;
            int anio = DateTime.Now.Year;
            for (int x = anio; x > anio - 15; x--)
            {
                cbxAnio.Items.Add(x);
            }
            cbxAnio.SelectedIndex = 0;
            nuevo();
            var respPrivilegio = new PrivilegioSvc().consultarPrivilegio("isAdmin", mainWindow.inicio._usuario.idUsuario);
            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresa.loaded(mainWindow.empresa, (respPrivilegio.typeResult == ResultTypes.success));

            //ctrZonaOperativa.cargarControl(mainWindow.zonaOperativa, mainWindow.inicio._usuario.idUsuario);
            
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ctrEmpresa.empresaSelected != null)
            {
                var resp = new MesContableSvc().mesContableAbiertoByIdEmpresa(ctrEmpresa.empresaSelected.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    MesContable mesContable = resp.result as MesContable;
                    mapForm(mesContable);
                }
                else if(resp.typeResult == ResultTypes.recordNotFound)
                {
                    mapForm(null);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
        }

        private void mapForm(MesContable mesContable)
        {
            if (mesContable != null)
            {
                txtClave.Tag = mesContable;
                txtClave.valor = mesContable.idMesContable;
                selectComboMes(mesContable.mes);
                selectComboAnio(mesContable.anio);
                txtInicio.Text = mesContable.fechaInicio.ToString("dd/MM/yy HH:mm:ss");
                txtFin.Text = mesContable.fechaFin == null ? string.Empty : ((DateTime)mesContable.fechaFin).ToString("dd/MM/yy HH:mm:ss");
                if (mesContable.fechaFin == null)
                {
                    btnAbrir.IsEnabled = false;
                    btnCerrar.IsEnabled = true;
                }
                else
                {
                    btnAbrir.IsEnabled = false;
                    btnCerrar.IsEnabled = false;
                }
            }
            else
            {
                txtClave.Tag = null;
                txtClave.valor = 0;
                selectComboMes(DateTime.Now.Month - 1);
                selectComboAnio(DateTime.Now.Year);
                txtInicio.Clear();
                txtFin.Clear();
                btnAbrir.IsEnabled = true;
                btnCerrar.IsEnabled = false;
            }
        }

        private void selectComboAnio(int anio)
        {
            int i = 0;
            foreach (int item in cbxAnio.Items)
            {
                if (item == anio)
                {
                    cbxAnio.SelectedIndex = i;
                    return;
                }
                i++;
            }
        }
        private void selectComboMes(int mes)
        {
            int i = 0;
            foreach (Mes item in cbxMes.Items)
            {
                if ((int)item == mes)
                {
                    cbxMes.SelectedIndex = i;
                    return;
                }
                i++;
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            //btnAbrir.IsEnabled = false;
            //btnCerrar.IsEnabled = false;
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
            mapForm(null);
        }

        private void btnConsultar_Click(object sender, RoutedEventArgs e)
        {
            if ((cbxAnio.SelectedItem != null) && (cbxMes.SelectedItem != null) && (ctrEmpresa.empresaSelected != null))
            {
                var resp = new MesContableSvc().getMesContable(((int)cbxMes.SelectedItem), ((int)cbxAnio.SelectedItem), ctrEmpresa.empresaSelected.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    MesContable mesContable = resp.result as MesContable;
                    mapForm(mesContable);
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    mapForm(null);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            else
            {
                ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "FALTO PROPORCIONAR ALGUNOS DATOS"));
            }
        }

        private void btnAbrir_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                MesContable mesContable = mapForm();
                if (mesContable != null)
                {
                    var resp = new MesContableSvc().abrirMesContable(ref mesContable);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mapForm(mesContable);
                    }
                    ImprimirMensaje.imprimir(resp);
                }
                else
                {
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.error, "ERROR AL LEER LOS DATOS DE LA PANTALLA"));
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private MesContable mapForm()
        {
            try
            {
                if (txtClave.Tag != null)
                {
                    MesContable mesContable = txtClave.Tag as MesContable;
                    mesContable.fechaFin = DateTime.Now;
                    return mesContable;
                }
                else
                {
                    return new MesContable
                    {
                        idMesContable = txtClave.Tag == null ? 0 : (txtClave.Tag as MesContable).idMesContable,
                        idEmpresa = ctrEmpresa.empresaSelected.clave,
                        mes = (int)cbxMes.SelectedItem,
                        anio = (int)cbxAnio.SelectedItem,
                        fechaInicio = DateTime.Now,
                        fechaFin = null
                    };
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                MesContable mesContable = mapForm();
                if (mesContable != null)
                {
                    var resp = new MesContableSvc().cerrarMesContable(ref mesContable);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mapForm(mesContable);
                    }
                    ImprimirMensaje.imprimir(resp);
                }
                else
                {
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.error, "ERROR AL LEER LOS DATOS DE LA PANTALLA"));
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
