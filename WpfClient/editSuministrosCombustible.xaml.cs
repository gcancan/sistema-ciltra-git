﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editSuministrosCombustible.xaml
    /// </summary>
    public partial class editSuministrosCombustible : ViewBase
    {
        public editSuministrosCombustible()
        {
            InitializeComponent();
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e) //GRCC
        {
            try
            {
                Cursor = Cursors.Wait;
                ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
                ctrZonaOperativa.cargarControl(mainWindow.zonaOperativa, mainWindow.usuario.idUsuario);

                txtLtsTotales.txtDecimal.TextChanged += TxtDecimal_TextChanged; ;

                txtRecibe.loaded(eTipoBusquedaPersonal.TODOS, null);
                nuevo();
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void TxtDecimal_TextChanged(object sender, TextChangedEventArgs e) //GRCC
        {
            actualizarCantidades();
        }

        private void actualizarCantidades() //GRCC
        {
            decimal ltTotal = txtLtsTotales.valor;
            decimal ltSuma = 0m;

            foreach (Label lbl in stpEmpresas.Children.Cast<Label>().ToList())
            {
                StackPanel stpContent = lbl.Content as StackPanel;
                DetalleSuminstroCombustible detalle = lbl.Tag as DetalleSuminstroCombustible;
                foreach (object obj in stpContent.Children.Cast<object>().ToList())
                {
                    if (obj is controlDecimal)
                    {
                        ltSuma += (obj as controlDecimal).valor;
                        detalle.ltsCombustible = (obj as controlDecimal).valor;
                    }
                }
            }
            txtSuma.valor = ltSuma;

            txtDiferencia.valor = ltTotal - ltSuma;
        }

        private void cargarEmpresas() //GRCC
        {
            try
            {
                stpEmpresas.Children.Clear();
                var resp = new EmpresaSvc().getEmpresasALLorById();
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Empresa> list = resp.result as List<Empresa>;
                    stpEmpresas.Children.Clear();
                    foreach (Empresa empresa in list)
                    {
                        if (empresa.clave == 1 || empresa.clave == 2 || empresa.clave == 5)
                        {
                            DetalleSuminstroCombustible detalle = new DetalleSuminstroCombustible
                            {
                                empresa = empresa
                            };
                            mapDetalleSuministro(detalle);
                        }                        
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void mapDetalleSuministro(DetalleSuminstroCombustible detalle) //GRCC
        {
            try
            {
                Label lblPrincipal = new Label { Tag = detalle };
                StackPanel stpContect = new StackPanel { Orientation = Orientation.Horizontal };

                Label lblNombreEmpresa = new Label
                {
                    Tag = detalle,
                    Width = 300,
                    Content = detalle.empresa.nombre
                };
                stpContect.Children.Add(lblNombreEmpresa);

                controlDecimal ctrDecimal = new controlDecimal
                {
                    Tag = detalle,
                    valor = detalle.ltsCombustible
                };
                ctrDecimal.txtDecimal.TextChanged += TxtDecimal_TextChanged;
                stpContect.Children.Add(ctrDecimal);

                lblPrincipal.Content = stpContect;
                stpEmpresas.Children.Add(lblPrincipal);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private void TxtDecimal_KeyDown1(object sender, KeyEventArgs e)
        {
            actualizarCantidades();
        }
        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ctrZonaOperativa.zonaOperativa != null)
            {
                getNivelesByZonaOperativa(ctrZonaOperativa.zonaOperativa);
            }
        }
        private void getNivelesByZonaOperativa(ZonaOperativa zonaOperativa)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new NivelesTanqueSvc().getNivelTanqueByZonaOperativa(zonaOperativa);
                var nivelesTanque = resp.result as NivelesTanque;
                if (resp.typeResult == ResultTypes.success)
                {
                    if (nivelesTanque.fechaFinal != null)
                    {
                        ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "NO ESTAN ABIERTOS LOS NIVELES"));
                        mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                        return;
                    }
                    mapNivelTanque(nivelesTanque);
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
                }
                else
                {
                    mapNivelTanque(null);
                    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void mapNivelTanque(NivelesTanque nivelesTanque)
        {
            if (nivelesTanque != null)
            {
                txtClave.Tag = nivelesTanque;
                txtClave.valor = nivelesTanque.idNivel;
                txtNombreTanque.Text = nivelesTanque.tanqueCombustible.nombre;
                txtFechaInicio.Text = nivelesTanque.fechaInicio.ToString("dd/MM/yyyy HH:mm:ss");
            }
            else
            {
                txtClave.Tag = null;
                txtClave.valor = 0;
                txtNombreTanque.Clear();
                txtFechaInicio.Clear();
            }
        }
        public override void nuevo()
        {
            base.nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar | ToolbarCommands.buscar);
            
            txtLtsTotales.valor = 0;
            dtpFecha.Value = DateTime.Now;
            mapForm(null);
        }

        public override OperationResult guardar()
        {
            try
            {
                var val = validarForm();
                if (val.typeResult != ResultTypes.success)
                {
                    return val;
                }

                SuministroCombustible suministro = mapForm();

                OperationResult resp = new OperationResult();
                if (suministro.idSuministroCombustible == 0)
                {
                    resp = new SuministroCombustibleSvc().saveSuministroCombustible(ref suministro);
                }
                else
                {
                    resp = new SuministroCombustibleSvc().actualizarSuministro(suministro);
                }                
                
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(suministro);
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        private SuministroCombustible mapForm()
        {
            try
            {
                SuministroCombustible suministro = new SuministroCombustible
                {
                    idSuministroCombustible = txtClaveSuministro.Tag == null ? 0 : (txtClaveSuministro.Tag as SuministroCombustible).idSuministroCombustible,
                    ltsCombustible = txtLtsTotales.valor,
                    usuario = mainWindow.usuario,
                    fecha = DateTime.Now,
                    tanqueCombustible = (txtClave.Tag as NivelesTanque).tanqueCombustible,
                    factura = txtFatura.Text,
                    importe = txtImporte.valor,
                    noPipa = txtPipa.Text,
                    placasPipa = txtPlaca.Text,
                    fechaRecibe = (DateTime)dtpFecha.Value,
                    operador = txtChofer.Text,
                    PersonalRecibe = txtRecibe.personal
                };
                suministro.listaDetalles = new List<DetalleSuminstroCombustible>();
                suministro.listaDetalles = getListaDetalles();
                return suministro;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }
        private void mapForm(SuministroCombustible suministro)
        {
            stpControles.IsEnabled = true;
            if (suministro != null)
            {
                txtClaveSuministro.Tag = suministro;
                txtClaveSuministro.valor = suministro.idSuministroCombustible;
                txtFatura.Text = suministro.factura;
                txtImporte.valor = suministro.importe;
                txtPipa.Text = suministro.noPipa;
                txtPlaca.Text = suministro.placasPipa;
                dtpFecha.Value = suministro.fechaRecibe;
                txtChofer.Text = suministro.operador;
                txtRecibe.personal = suministro.PersonalRecibe;
                stpEmpresas.Children.Clear();
                foreach (DetalleSuminstroCombustible detalle in suministro.listaDetalles)
                {
                    mapDetalleSuministro(detalle);
                }
                if (string.IsNullOrEmpty(suministro.factura))
                {
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.editar | ToolbarCommands.cerrar);
                }
                else
                {
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                }
            }
            else
            {
                txtClaveSuministro.Tag = null;
                txtClaveSuministro.valor = 0;
                txtFatura.Clear();
                txtImporte.valor = 0;
                txtPipa.Clear();
                txtPlaca.Clear();
                dtpFecha.Value = DateTime.Now;
                txtChofer.Clear();
                txtRecibe.personal = null;
                cargarEmpresas();
                txtSuma.valor = 0;
                txtDiferencia.valor = 0;
            }
        }
        public OperationResult validarForm()
        {
            try
            {
                OperationResult resp = new OperationResult(ResultTypes.success, "");
                if (txtClaveSuministro.Tag != null)
                {
                    if (string.IsNullOrEmpty(txtFatura.Text.Trim()))
                    {
                        resp.valor = 2;
                        resp.mensaje = "SE REQUIERE PROPORCIONAR LA FACTURA.";
                    }
                    if (txtImporte.valor <= 0)
                    {
                        resp.valor = 2;
                        resp.mensaje = "EL IMPORTE TIENE QUE SER MAYOR A 0.";
                    }
                    if (string.IsNullOrEmpty(txtPipa.Text.Trim()))
                    {
                        resp.valor = 2;
                        resp.mensaje = "SE REQUIERE PROPORCIONAR EL No. DE PIPA.";
                    }
                    if (string.IsNullOrEmpty(txtPlaca.Text.Trim()))
                    {
                        resp.valor = 2;
                        resp.mensaje = "SE REQUIERE PROPORCIONAR LA PLACAS DE LA PIPA.";
                    }
                    if (string.IsNullOrEmpty(txtChofer.Text.Trim()))
                    {
                        resp.valor = 2;
                        resp.mensaje = "SE REQUIERE PROPORCIONAR EL NOMBRE DEL CHOFER DE LA PIPA.";
                    }
                }
                if (dtpFecha.Value == null)
                {
                    resp.valor = 2;
                    resp.mensaje = "SE REQUIERE PROPORCIONAR LA FECHA DE SUMINISTRO.";
                }
                
                if (txtRecibe.personal == null)
                {
                    resp.valor = 2;
                    resp.mensaje = "SE REQUIERE PROPORCIONAR EL NOMBRE DE QUIEN RECIVE LA PIPA.";
                }
                if (txtDiferencia.valor != 0)
                {
                    resp.valor = 2;
                    resp.mensaje = "LA SUMA DE LITROS NO COINCIDE CON LOS LITROS TOTALES.";
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        public List<DetalleSuminstroCombustible> getListaDetalles()
        {
            List<DetalleSuminstroCombustible> lista = new List<DetalleSuminstroCombustible>();
            foreach (Label lbl in stpEmpresas.Children.Cast<Label>().ToList())
            {
                lista.Add(lbl.Tag as DetalleSuminstroCombustible);
            }
            return lista;
        }
        public override void buscar()
        {            
            var suministro = new selectSuministroCombustible().GetSuministroCombustible((txtClave.Tag as NivelesTanque).tanqueCombustible);
            if (suministro != null)
            {
                base.buscar();
                mapForm(suministro);
            }
        }

        private void TxtClave_Loaded(object sender, RoutedEventArgs e)
        {

        }
        public override void editar()
        {
            base.editar();
            stpControles.IsEnabled = false;
        }
    }
}
