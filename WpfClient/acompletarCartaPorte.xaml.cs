﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using CoreFletera.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Markup;

    public partial class acompletarCartaPorte : Window
    {
        private Viaje viaje = null;
        private MainWindow mainWindow = null;
        
        public acompletarCartaPorte(Viaje viaje, MainWindow mainWindow)
        {
            this.InitializeComponent();
            this.viaje = viaje;
            this.mainWindow = mainWindow;
        }
        private void btnCambiar_Click(object sender, RoutedEventArgs e)
        {
            this.lblChofer.limpiar();
        }

        private void Guardar_Click(object sender, RoutedEventArgs e)
        {
            List<CartaPorte> list = new List<CartaPorte>();
            List<StackPanel> list2 = this.stpDetalles.Children.Cast<StackPanel>().ToList<StackPanel>();
            if (this.lblChofer.personal == null)
            {
                OperationResult resp = new OperationResult
                {
                    valor = 3,
                    mensaje = "SE REQUIERE ESPECIFICAR EL OPERADOR QUE FINALIZA EL VIAJE"
                };
                ImprimirMensaje.imprimir(resp);
            }
            else
            {
                foreach (StackPanel panel in list2)
                {
                    list.Add(panel.Tag as CartaPorte);
                }
                if ((list.FindAll(s => s.remision == string.Empty).Count > 0) || (list.FindAll(s => s.volumenDescarga <= decimal.Zero).Count > 0))
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 3,
                        mensaje = "TODA LA INFORMACI\x00d3N ES OBLIGATORIA PARA CERRAR EL VIAJE"
                    };
                    ImprimirMensaje.imprimir(resp);
                }
                else
                {
                    this.viaje.OperadorFinViaje = this.lblChofer.personal;
                    this.viaje.ayudante = this.lblChoferAyudante.personal;
                    this.viaje.listDetalles = list;

                    if (this.viaje.cliente.cobroXkm)
                    {
                        this.viaje.listDetalles = (from s in viaje.listDetalles
                                              orderby s.zonaSelect.km descending
                                              select s).ToList<CartaPorte>();
                        for (int i = 0; i < viaje.listDetalles.Count; i++)
                        {
                            if (i == 0)
                            {
                                viaje.listDetalles[i].importeReal = (viaje.listDetalles[i].precio * viaje.listDetalles[i].zonaSelect.km) + viaje.listDetalles[i].precioFijo;
                            }
                            else
                            {
                                viaje.listDetalles[i].importeReal = decimal.Zero;
                            }
                            viaje.listDetalles[i].cobroXkm = true;
                        }
                    }
                    else if (viaje.listDetalles.Exists(s => s.viaje))
                    {
                        viaje.listDetalles = (from s in viaje.listDetalles
                                              orderby s.zonaSelect.km descending
                                              select s).ToList<CartaPorte>();
                        for (int i = 0; i < viaje.listDetalles.Count; i++)
                        {
                            if (i == 0)
                            {
                                viaje.listDetalles[i].importeReal = viaje.listDetalles[i].precioFijo;
                            }
                            else
                            {
                                viaje.listDetalles[i].importeReal = decimal.Zero;
                            }
                            viaje.listDetalles[i].viaje = true;
                        }
                    }

                    this.viaje.usuarioCambia = this.mainWindow.usuario.nombreUsuario;
                    OperationResult resp = new CartaPorteSvc().saveAjusteViaje(ref this.viaje, new int?(this.lblChofer.personal.clave));
                    if (resp.typeResult == ResultTypes.success)
                    {
                        base.DialogResult = true;
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
            }
        }
        private void mapDetallesCartaPorte(List<CartaPorte> listDetalles)
        {
            try
            {
                foreach (CartaPorte porte in listDetalles)
                {
                    porte.tipoPrecio = this.tipoPrecio;
                    porte.tipoPrecioChofer = this.tipoPrecioChofer;

                    StackPanel element = new StackPanel
                    {
                        Tag = porte,
                        Orientation = Orientation.Horizontal
                    };
                    element.IsEnabled = string.IsNullOrEmpty(porte.remision) && (porte.volumenDescarga <= decimal.Zero);
                    TextBox box = new TextBox
                    {
                        Width = 116.0,
                        Margin = new Thickness(2.0),
                        Tag = element,
                        IsEnabled = string.IsNullOrEmpty(porte.remision),
                        Text = porte.remision
                    };
                    box.TextChanged += new TextChangedEventHandler(this.TxtRemision_TextChanged);
                    element.Children.Add(box);
                    Label label = new Label
                    {
                        Content = porte.zonaSelect.descripcion,
                        Width = 220.0,
                        FontSize = 10.0,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        Margin = new Thickness(2.0)
                    };
                    element.Children.Add(label);
                    Label label2 = new Label
                    {
                        Content = porte.producto.descripcion,
                        Width = 220.0,
                        FontSize = 10.0,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        Margin = new Thickness(2.0)
                    };
                    element.Children.Add(label2);
                    controlDecimal num = new controlDecimal
                    {
                        valor = porte.volumenDescarga,
                        Tag = element,
                        IsEnabled = porte.volumenCarga == decimal.Zero
                    };
                    num.txtDecimal.Tag = element;
                    num.txtDecimal.TextChanged += new TextChangedEventHandler(this.TxtDecimal_TextChanged);
                    element.Children.Add(num);
                    this.stpDetalles.Children.Add(element);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                base.DialogResult = false;
            }
        }
        private void TxtDecimal_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox box = sender as TextBox;
            StackPanel tag = box.Tag as StackPanel;
            CartaPorte porte = tag.Tag as CartaPorte;
            porte.limpiarImporte();
            porte.usuarioAjuste = new int?(this.mainWindow.usuario.idUsuario);
            this.viaje.usuarioCambia = this.mainWindow.usuario.nombreUsuario;
            porte.volumenDescarga = Convert.ToDecimal(box.Text) / 1000M;
        }

        private void TxtRemision_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox box = sender as TextBox;
            StackPanel tag = box.Tag as StackPanel;
            CartaPorte porte = tag.Tag as CartaPorte;
            porte.usuarioAjuste = new int?(this.mainWindow.usuario.idUsuario);
            this.viaje.usuarioCambia = this.mainWindow.usuario.nombreUsuario;
            porte.remision = box.Text;
        }
        public TipoPrecio tipoPrecio = TipoPrecio.SENCILLO;
        public TipoPrecio tipoPrecioChofer = TipoPrecio.SENCILLO;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Empresa empresa = new Empresa
                {
                    clave = this.viaje.IdEmpresa
                };
                lblChofer.DataContextChanged += LblChofer_DataContextChanged;
                this.lblChofer.loaded(eTipoBusquedaPersonal.OPERADORES, empresa);
                this.lblCliente.Content = this.viaje.cliente.nombre;
                this.lblChofer.personal = this.viaje.operador;
                this.lblTractor.Content = this.viaje.tractor.clave;
                this.lblRemolque1.Content = this.viaje.remolque == null ? String.Empty : this.viaje.remolque.clave;
                this.lblDolly.Content = (this.viaje.dolly == null) ? string.Empty : this.viaje.dolly.clave;
                this.lblRemolque2.Content = (this.viaje.remolque2 == null) ? string.Empty : this.viaje.remolque2.clave;
                this.lblFolioViaje.Content = this.viaje.NumGuiaId.ToString();
                this.lblFecha.Content = this.viaje.FechaHoraViaje.ToString("dd/MM/yyyy HH:mm");
                this.stpAyudante.IsEnabled = this.viaje.tipoViaje.ToString().ToUpper() == "F";

                tipoPrecio = EstablecerPrecios.establecerPrecio(viaje.remolque, viaje.dolly, viaje.remolque2, (new Empresa { clave = viaje.IdEmpresa }), viaje.cliente);
                tipoPrecioChofer = EstablecerPrecios.establecerPrecioChofer(viaje.remolque, viaje.dolly, viaje.remolque2, (new Empresa { clave = viaje.IdEmpresa }));
                this.mapDetallesCartaPorte(this.viaje.listDetalles);
                ctrNumero.IsEnabled = false;
                ctrNumero.valor = viaje.noViaje;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void LblChofer_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            controlPersonal ctr = sender as controlPersonal;
            if (ctr.personal != null)
            {
                DateTime fecha = viaje.FechaHoraViaje;
                var resp = new CartaPorteSvc().getNoViajeOperador(fecha, ctr.personal.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    NoViajeOperador noViaje = resp.result as NoViajeOperador;

                    TimeSpan time = fecha - noViaje.fechaFin;
                    if (time.TotalHours < noViaje.horas && time.TotalHours > 0)
                    {
                        noViaje.noViaje += 1;
                    }

                    ctrNumero.valor = noViaje.noViaje;
                    ctrNumero.IsEnabled = false;
                    editoNoViaje = false;
                }
                else
                {
                    ctrNumero.IsEnabled = false;
                    ctrNumero.valor = 0;
                }
            }
            else
            {
                ctrNumero.IsEnabled = false;
                ctrNumero.valor = 0;
            }
        }

        bool editoNoViaje = false;
        private void BtnLimpiarCont_Click(object sender, RoutedEventArgs e)
        {
            editoNoViaje = true;
            ctrNumero.IsEnabled = true;
        }
    }
}
