﻿using CoreFletera.Models;
using Core.Models;
using CoreFletera.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for selectLlantas.xaml
    /// </summary>
    public partial class selectLlantas : Window
    {
        string parametro = string.Empty;
        public selectLlantas()
        {
            InitializeComponent();
        }
        public selectLlantas(string parametro)
        {
            this.parametro = parametro;
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtFind.Text = parametro;
            lvlLlantas.KeyDown += LvlLlantas_KeyDown;
        }

        private void LvlLlantas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }

        public Llanta getLlantasAlmacen(int idAlmacen = 0)
        {
            try
            {
                buscarLLantasAlmacen(idAlmacen);
                bool? r = ShowDialog();
                if (r.Value && lvlLlantas.SelectedItem != null)
                {
                    return (Llanta)((ListViewItem)lvlLlantas.SelectedItem).Content;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }

        private void buscarLLantasAlmacen(int idAlmacen)
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult resp = new LlantaSvc().getLlantasEnAlmacen(idAlmacen);
                validarResp(resp);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void validarResp(OperationResult resp)
        {
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    mapLista((List<Llanta>)resp.result);
                    break;
                case ResultTypes.error:
                    ImprimirMensaje.imprimir(resp);
                    break;
                case ResultTypes.warning:
                    ImprimirMensaje.imprimir(resp);
                    break;
                case ResultTypes.recordNotFound:
                    ImprimirMensaje.imprimir(resp);
                    break;
                default:
                    break;
            }
        }

        private void mapLista(List<Llanta> result)
        {
            lvlLlantas.Items.Clear();
            List<ListViewItem> lvlList = new List<ListViewItem>();
            foreach (Llanta item in result)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                lvlList.Add(lvl);
            }
            lvlLlantas.ItemsSource = lvlList;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlLlantas.ItemsSource);
            view.Filter = UserFilter;
            lvlLlantas.Focus();
        }

        private bool UserFilter(object item)
        {
            if (string.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((Llanta)(item as ListViewItem).Content).clave.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Llanta)(item as ListViewItem).Content).noSerie.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Llanta)(item as ListViewItem).Content).marca.descripcion.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Llanta)(item as ListViewItem).Content).diseño.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }
        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlLlantas.ItemsSource).Refresh();
        }
        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                lvlLlantas.Focus();
            }
        }

        
    }
}
