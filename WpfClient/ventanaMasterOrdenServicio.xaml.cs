﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using MahApps.Metro.Controls;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para ventanaMasterOrdenServicio.xaml
    /// </summary>
    public partial class ventanaMasterOrdenServicio : MetroWindow
    {
        public ventanaMasterOrdenServicio()
        {
            InitializeComponent();
        }
        MasterOrdServ masterOrdServ = null;
        List<CargaCombustibleExtra> listaCargasExtra = null;
        public ventanaMasterOrdenServicio(MasterOrdServ masterOrdServ, string nombreImpresora, List<CargaCombustibleExtra> listaCargasExtra)
        {
            InitializeComponent();
            this.masterOrdServ = masterOrdServ;
            this.nombreImpresora = nombreImpresora;
            this.listaCargasExtra = listaCargasExtra;
        }
        string nombreImpresora { get; set; }
        private async void MetroWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                await Dispatcher.Invoke(async () =>
               {
                   lblFolioMaster.Visibility = Visibility.Collapsed;
                   stpTitulos.Visibility = Visibility.Collapsed;
                   if (masterOrdServ != null)
                   {
                       lblFolioMaster.Visibility = Visibility.Visible;
                       stpTitulos.Visibility = Visibility.Visible;
                       lblFolioMaster.Content = string.Format("SE GENERÓ FOLIO: {0}", masterOrdServ.idPadreOrdSer.ToString());

                       string cadena = string.Empty;
                       foreach (var item in masterOrdServ.listaCabOrdenServicio.Select(s => s.unidad.clave).Distinct().ToList<string>())
                       {
                           cadena += item + " ";
                       }
                       lblUnidades.Content = cadena.ToString();

                       var respLista = (from v in masterOrdServ.listaCabOrdenServicio
                                        group v by new { v.enumTipoOrden, v.enumTipoOrdServ } into grupo
                                        select new CabOrdenServicio
                                        {
                                            enumTipoOrden = grupo.Select(s => s.enumTipoOrden).First(),
                                            enumTipoOrdServ = grupo.Select(s => s.enumTipoOrdServ).First(),
                                            listaIdCabOS = grupo.Select(s => s.idOrdenSer).ToList()
                                        });

                       foreach (var cabOrden in respLista)
                       {
                           StackPanel stpControl = new StackPanel { Orientation = Orientation.Horizontal };
                           stpControl.Children.Add(new Label
                           {
                               Content = cabOrden.enumTipoOrdServ.ToString(),
                               Foreground = Brushes.SteelBlue,
                               FontWeight = FontWeights.Bold,
                               Width = 120,
                               VerticalContentAlignment = VerticalAlignment.Center,
                               VerticalAlignment = VerticalAlignment.Center,
                               FontSize = 14
                           });

                           if (cabOrden.enumTipoOrdServ == enumTipoOrdServ.TALLER)
                               stpControl.Children.Add(new Label
                               {
                                   Content = cabOrden.enumTipoOrden.ToString(),
                                   FontWeight = FontWeights.Medium,
                                   Width = 100,
                                   HorizontalContentAlignment = HorizontalAlignment.Center,
                                   VerticalAlignment = VerticalAlignment.Center,
                                   FontSize = 11
                               });

                           string cadena2 = string.Empty;
                           foreach (var item in cabOrden.listaIdCabOS)
                           {
                               cadena2 += item.ToString() + " ";
                           }

                           stpControl.Children.Add(new Label
                           {
                               Content = cadena2.Trim(),
                               FontWeight = FontWeights.Medium,
                               HorizontalContentAlignment = HorizontalAlignment.Center,
                               VerticalAlignment = VerticalAlignment.Center,
                               FontSize = 9
                           });
                           stpOrdenesServicio.Children.Add(stpControl);
                       }
                   }
                   stpCargasExtra.Visibility = Visibility.Hidden;
                   foreach (var carga in this.listaCargasExtra)
                   {
                       stpCargasExtra.Visibility = Visibility.Visible;
                       StackPanel stpControl = new StackPanel { Orientation = Orientation.Horizontal };
                       stpControl.Children.Add(new Label { Content = carga.tipoOrdenCombustible.nombreTipoOrdenCombustible.ToString(), Foreground = Brushes.SteelBlue, FontWeight = FontWeights.Bold, MinWidth = 120, VerticalContentAlignment = VerticalAlignment.Center, FontSize = 14 });
                       stpControl.Children.Add(new Label { Content = carga.unidadTransporte.clave.ToString(), FontWeight = FontWeights.Medium, Width = 100, HorizontalContentAlignment = HorizontalAlignment.Center, FontSize = 11 });

                       stpControl.Children.Add(new Label { Content = carga.idCargaCombustibleExtra.ToString(), FontWeight = FontWeights.Medium, HorizontalContentAlignment = HorizontalAlignment.Center, FontSize = 9 });
                       stpOrdenesCargasExtra.Children.Add(stpControl);
                   }
               });
                await Dispatcher.Invoke(async () =>
                 {
                     await Task.Run(async () => await imprimir());
                 });
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        public async Task imprimir()
        {
            if (masterOrdServ != null)
            {
                Dispatcher.Invoke(() =>
                           {
                               foreach (var item in masterOrdServ.listaCabOrdenServicio)
                               {
                                   var rr = new ReportView();
                                   rr.Title = "SOLICITUD DE ORDEN DE SERVICIO.";
                                   rr.btnEXCEL.Visibility = Visibility.Collapsed;
                                   rr.crearSolicitudOrdServ_v3(masterOrdServ, item, this.nombreImpresora);
                                   rr.Show();
                               }
                           });
            }
            Dispatcher.Invoke(() =>
            {
                foreach (var item in listaCargasExtra)
                {
                    new ReportView().crearSolicitudCargaCombustibleExtra(item, this.nombreImpresora);
                }
            });
        }
    }
}
