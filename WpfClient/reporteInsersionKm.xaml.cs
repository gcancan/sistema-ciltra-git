﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para reporteKardexMttoPreventivo.xaml
    /// </summary>
    public partial class reporteInsersionKm : ViewBase
    {
        public reporteInsersionKm()
        {
            InitializeComponent();
        }
        Usuario usuario = null;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.usuario = mainWindow.usuario;
            ctrEmpresas.lbl.Visibility = Visibility.Collapsed;// = 100;
            ctrEmpresas.cbxEmpresa.Width = 270;
            ctrEmpresas.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresas.loaded(usuario.empresa, new PrivilegioSvc().consultarPrivilegio("isAdmin", usuario.idUsuario).typeResult == ResultTypes.success);
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);

            ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
            ctrZonaOperativa.cargarControl(usuario.zonaOperativa, usuario.idUsuario);

            var resp = new ServiciosSvc().getAllServicios();
            if (resp.typeResult == ResultTypes.success)
            {
                var lista = resp.result as List<Servicio>;
                lista = lista.FindAll(s => s.idServicio == 1 || s.idServicio == 2 || s.idServicio == 13).ToList();
                cbxServicios.ItemsSource = lista;
            }
            ctrEmpresas.cbxEmpresa.Width = 270;
            List<TipoInsersion> listaInsersion = new List<TipoInsersion>();
            if (new PrivilegioSvc().consultarPrivilegio("INSERSION_KM_CAMBIO_ODOMETRO", usuario.idUsuario).typeResult == ResultTypes.success)
            {
                listaInsersion.Add(new TipoInsersion { enumTipoInsersionKM = enumTipoInsersionKM.CAMBIO_HODÓMETRO });
            }
            if (new PrivilegioSvc().consultarPrivilegio("INSERSION_KM_REGISTRO_KM_PREVENTIVO", usuario.idUsuario).typeResult == ResultTypes.success)
            {
                listaInsersion.Add(new TipoInsersion { enumTipoInsersionKM = enumTipoInsersionKM.REGISTRO_KM_PREVENTIVO });
            }
            if (new PrivilegioSvc().consultarPrivilegio("INSERSION_KM_REGISTRO_KM_TALLER_PEGASO", usuario.idUsuario).typeResult == ResultTypes.success)
            {
                listaInsersion.Add(new TipoInsersion { enumTipoInsersionKM = enumTipoInsersionKM.REGISTRO_KM_TALLER_PEGASO });
            }
            cbxTipoInsersion.ItemsSource = listaInsersion;
        }
        internal class TipoInsersion
        {
            public enumTipoInsersionKM enumTipoInsersionKM { get; set; }
            public string nombre => enumTipoInsersionKM.ToString().Replace("_", " ");
        }
        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                chcTodos.IsChecked = false;
                cbxUnidades.ItemsSource = null;
                if (sender != null)
                {
                    //var resp = new UnidadTransporteSvc().getTractores(ctrEmpresas.empresaSelected.clave);
                    //if (resp.typeResult == ResultTypes.success)
                    //{
                    //    cbxUnidades.ItemsSource = resp.result as List<UnidadTransporte>;
                    //    chcTodos.IsChecked = true;
                    //}
                    //else
                    //{
                    //    cbxUnidades.ItemsSource = null;
                    //}
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                cbxOperacion.ItemsSource = null;
                if (sender != null)
                {
                    if (ctrZonaOperativa.zonaOperativa != null)
                    {
                        var resp = new ClienteSvc().getClientesByZonasOperativas(new List<string> { (ctrZonaOperativa.zonaOperativa.idZonaOperativa.ToString()) }, ctrEmpresas.empresaSelected.clave);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            var listaOperaciones = resp.result as List<OperacionFletera>;
                            cbxOperacion.ItemsSource = listaOperaciones;
                            //cbxOperacion.SelectedItems.Add(cbxOperacion.ItemsSource.Cast<OperacionFletera>().ToList().Find(s => s.cliente.clave == usuario.cliente.clave));
                        }
                        else if (resp.typeResult != ResultTypes.recordNotFound)
                        {
                            imprimir(resp);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void CbxOperacion_ItemSelectionChanged(object sender, Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                List<UnidadTransporte> listaUnidades = new List<UnidadTransporte>();
                chcTodos.IsChecked = false;
                cbxUnidades.SelectedItems.Clear();

                if (sender != null)
                {
                    if (cbxOperacion.SelectedItems.Count >= 0)
                    {
                        foreach (OperacionFletera operacion in cbxOperacion.SelectedItems)
                        {
                            var resp = new FlotaSvc().getFlota(operacion);
                            if (resp.typeResult == ResultTypes.success)
                            {
                                foreach (Flota flota in resp.result as List<Flota>)
                                {
                                    if (flota.tractor != null)
                                    {
                                        listaUnidades.Add(flota.tractor);
                                    }
                                    //if (flota.remolque1 != null)
                                    //{
                                    //    listaUnidades.Add(flota.remolque1);
                                    //}
                                    //if (flota.dolly != null)
                                    //{
                                    //    listaUnidades.Add(flota.dolly);
                                    //}
                                    //if (flota.remolque2 != null)
                                    //{
                                    //    listaUnidades.Add(flota.remolque2);
                                    //}
                                }
                            }
                        }
                    }
                    cbxUnidades.ItemsSource = listaUnidades.OrderBy(s => s.clave);
                    //chcTodos.IsChecked = true;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void ChcTodos_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    CheckBox check = sender as CheckBox;
                    switch (check.IsChecked.Value)
                    {
                        case true:
                            foreach (var item in cbxUnidades.Items)
                            {
                                cbxUnidades.SelectedItems.Add(item);
                            }
                            break;
                        case false:
                            cbxUnidades.SelectedItems.Clear();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lvlResultados.ItemsSource = null;
                Cursor = Cursors.Wait;

                if (cbxTipoInsersion.SelectedItem == null) return;

                List<string> listaUnidades = cbxUnidades.SelectedItems.Cast<UnidadTransporte>().ToList().Select(s => s.clave).ToList();
                List<string> listaServicio = cbxServicios.SelectedItems.Cast<Servicio>().ToList().Select(s => s.idServicio.ToString()).ToList();

                TipoInsersion tipoInsersion = cbxTipoInsersion.SelectedItem as TipoInsersion;
                enumTipoInsersionKM enumTipo = tipoInsersion.enumTipoInsersionKM;
                switch (enumTipo)
                {
                    case enumTipoInsersionKM.REGISTRO_KM_PREVENTIVO:
                        var resp = new KardexMttoPreventivoSvc().getKardexMttoPreventivo(listaUnidades, listaServicio, ctrFechas.fechaInicial, ctrFechas.fechaFinal);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            var lista = resp.result as List<KardexMttoPreventivo>;
                            lvlResultados.ItemsSource = lista;
                        }
                        break;
                    default:
                        var resp2 = new KardexMttoPreventivoSvc().getInsersionKm(listaUnidades, ctrFechas.fechaInicial, ctrFechas.fechaFinal, enumTipo);
                        if (resp2.typeResult == ResultTypes.success)
                        {
                            var lista = resp2.result as List<KardexMttoPreventivo>;
                            lvlResultados.ItemsSource = lista;
                        }
                        break;
                }

                
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void ChcTodosServicios_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                cbxServicios.SelectedItems.Clear();
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    CheckBox check = sender as CheckBox;
                    switch (check.IsChecked.Value)
                    {
                        case true:
                            foreach (var item in cbxServicios.Items)
                            {
                                cbxServicios.SelectedItems.Add(item);
                            }
                            break;
                        case false:
                            cbxServicios.SelectedItems.Clear();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void ChcTodosOperacion_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    CheckBox check = sender as CheckBox;
                    switch (check.IsChecked.Value)
                    {
                        case true:
                            foreach (var item in cbxOperacion.Items)
                            {
                                cbxOperacion.SelectedItems.Add(item);
                            }
                            break;
                        case false:
                            cbxOperacion.SelectedItems.Clear();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (lvlResultados.ItemsSource == null) return;

                var lista = lvlResultados.ItemsSource as List<KardexMttoPreventivo>;
                new ReportView().exportarListaKardexMttoPreventivo(lista);
                
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CbxTipoInsersion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    TipoInsersion tipoInsersion = cbxTipoInsersion.SelectedItem as TipoInsersion;
                    enumTipoInsersionKM enumTipo = tipoInsersion.enumTipoInsersionKM;
                    switch (enumTipo)
                    {
                        case enumTipoInsersionKM.REGISTRO_KM_PREVENTIVO:
                            stpServicios.IsEnabled = true;
                            break;
                        default:
                            cbxServicios.SelectedItem = null;
                            stpServicios.IsEnabled = false;
                            chcTodosServicios.IsChecked = false;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
    }
}
