﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for RelacionServicioActividades.xaml
    /// </summary>
    public partial class RelacionServicioActividades : ViewBase
    {
        public RelacionServicioActividades()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                nuevo();
                Cursor = Cursors.Wait;
                mapServicios();
                mapActividades();
                
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }            
        }

        private void mapServicios()
        {
            try
            {
                cbxServicio.ItemsSource = null;
                var resp = new OrdenesTrabajoSvc().getAllServicios();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxServicio.ItemsSource = resp.result as List<Servicio>;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void mapActividades()
        {
            try
            {
                var resp = new OrdenesTrabajoSvc().getAllActividades();
                if (resp.typeResult == ResultTypes.success)
                {
                    foreach (Actividad act in resp.result as List<Actividad>)
                    {
                        ListViewItem lvl = new ListViewItem();
                        lvl.Content = act;
                        lvlActividades.Items.Add(lvl);
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        public override void nuevo()
        {
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
        }

        private void cbxServicio_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxServicio.SelectedItem == null)
            {
                lvlRelacion.Items.Clear();
            }
            else
            {
                actualizarLista();
            }
        }

        private void actualizarLista()
        {
            lvlRelacion.Items.Clear();
            Servicio serv = (Servicio)cbxServicio.SelectedItem;
            foreach (Actividad act in serv.listaActividades)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = act;

                ContextMenu menu = new ContextMenu();
                MenuItem menuBorrar = new MenuItem
                {
                    Header = "Quitar Actividad Del Servicio"
                };
                menuBorrar.Click += MenuBorrar_Click;
                menuBorrar.Tag = act;
                menu.Items.Add(menuBorrar);               

                lvl.ContextMenu = menu;
                lvlRelacion.Items.Add(lvl);
            }
        }

        private void MenuBorrar_Click(object sender, RoutedEventArgs e)
        {
            eliminar();
        }

        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                List<Actividad> listaActividades = getListaActividades();
                Servicio serv = (Servicio)cbxServicio.SelectedItem;
                var resp = new OrdenesTrabajoSvc().saveRelServAct(serv, listaActividades);
                int idServicio = serv.idServicio;
                if (resp.typeResult == ResultTypes.success)
                {
                    mapServicios();
                    int i = 0;
                    foreach (Servicio item in cbxServicio.Items)
                    {
                        if (item.idServicio == serv.idServicio)
                        {
                            cbxServicio.SelectedIndex = i;
                            break;
                        }
                        i++;
                    }
                    actualizarLista();
                }
                mainWindow.habilitar = true;
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        public override OperationResult eliminar()
        {
            try
            {
                Cursor = Cursors.Wait;
                List<Actividad> listaActividades = new List<Actividad>()
                {
                    ((Actividad)((ListViewItem)lvlRelacion.SelectedItem).Content)
                };
                Servicio serv = (Servicio)cbxServicio.SelectedItem;
                var resp = new OrdenesTrabajoSvc().saveRelServAct(serv, listaActividades, false);
                int idServicio = serv.idServicio;
                if (resp.typeResult == ResultTypes.success)
                {
                    mapServicios();
                    int i = 0;
                    foreach (Servicio item in cbxServicio.Items)
                    {
                        if (item.idServicio == serv.idServicio)
                        {
                            cbxServicio.SelectedIndex = i;
                            break;
                        }
                        i++;
                    }
                    actualizarLista();
                    nuevo();
                }
                mainWindow.habilitar = true;
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private List<Actividad> getListaActividades()
        {
            List<Actividad> listaActividades = new List<Actividad>();

            foreach (ListViewItem item in lvlActividades.SelectedItems)
            {
                listaActividades.Add((Actividad)item.Content);
            }

            return listaActividades;
        }
    }
}
