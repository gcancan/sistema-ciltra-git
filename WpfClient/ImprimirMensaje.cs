﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using System.Windows;

namespace WpfClient
{
    public static class ImprimirMensaje
    { //Exception
        public static void imprimir(OperationResult resp)
        {
            try
            {
                switch (resp.typeResult)
                {
                    case ResultTypes.success:
                        MessageBox.Show(resp.mensaje, "Exito", MessageBoxButton.OK, MessageBoxImage.Information);
                        break;
                    case ResultTypes.error:
                        MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    case ResultTypes.warning:
                        MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                        break;
                    case ResultTypes.recordNotFound:
                        MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                        break;

                    default:
                        MessageBox.Show(resp.mensaje, "Tipo de Resultado Desconocido", MessageBoxButton.OK, MessageBoxImage.Warning);
                        break;
                }
            }
            catch (Exception ex)
            {
            }            
        }

        public static void imprimir(Exception ex)
        {
            imprimir(new OperationResult (ex));
        }
    }
}
