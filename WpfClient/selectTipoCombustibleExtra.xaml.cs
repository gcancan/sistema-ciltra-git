﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectTipoCombustibleExtra.xaml
    /// </summary>
    public partial class selectTipoCombustibleExtra : Window
    {
        public selectTipoCombustibleExtra()
        {
            InitializeComponent();
        }
        private void buscarAllTipoCargaExta()
        {
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult resp = new CargaCombustibleSvc().getTipoOrdenCombustibleExtra();
                if (resp.typeResult == ResultTypes.success)
                {
                    this.mapCargasExtra(resp.result as List<TipoOrdenCombustible>);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        public TipoOrdenCombustible buscarAllTipoCargaExtra()
        {
            try
            {
                this.buscarAllTipoCargaExta();
                if (base.ShowDialog().Value && (this.lvlCargasExtra.SelectedItem != null))
                {
                    return ((this.lvlCargasExtra.SelectedItem as ListViewItem).Content as TipoOrdenCombustible);
                }
                return null;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return null;
            }
        }
        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            base.DialogResult = true;
        }

        private void lvlCargasExtra_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                base.DialogResult = true;
            }
        }

        private void mapCargasExtra(List<TipoOrdenCombustible> listaCargas)
        {
            this.lvlCargasExtra.Items.Clear();
            List<ListViewItem> list = new List<ListViewItem>();
            foreach (TipoOrdenCombustible combustible in listaCargas)
            {
                ListViewItem item = new ListViewItem
                {
                    Content = combustible
                };
                item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                list.Add(item);
            }
            this.lvlCargasExtra.ItemsSource = list;
            CollectionView defaultView =(CollectionView)CollectionViewSource.GetDefaultView(this.lvlCargasExtra.ItemsSource);
            defaultView.Filter = new Predicate<object>(this.UserFilter);
        }
        private void txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                this.lvlCargasExtra.Focus();
            }
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.lvlCargasExtra.ItemsSource != null)
            {
                CollectionViewSource.GetDefaultView(this.lvlCargasExtra.ItemsSource).Refresh();
            }
        }

        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtFind.Text) || (((TipoOrdenCombustible)(item as ListViewItem).Content).nombreTipoOrdenCombustible.ToString().IndexOf(this.txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0));

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }
    }
}
