﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editInsersionKM.xaml
    /// </summary>
    public partial class editInsersionKM : ViewBase
    {
        public editInsersionKM()
        {
            InitializeComponent();
        }
        Usuario usuario = new Usuario();
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            usuario = mainWindow.usuario;

            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresa.loaded(usuario.empresa, new PrivilegioSvc().consultarPrivilegio("isAdmin", usuario.idUsuario).typeResult == ResultTypes.success);

            ctrUnidadTransporte.DataContextChanged += CtrUnidadTransporte_DataContextChanged;
            ctrUnidadTransporte.loaded(etipoUniadBusqueda.TRACTOR, ctrEmpresa.empresaSelected);

            cbxTipoInsersion.SelectionChanged += CbxTipoInsersion_SelectionChanged;
            

            var resp = new ServiciosSvc().getAllServicios();
            if (resp.typeResult == ResultTypes.success)
            {
                var lista = resp.result as List<Servicio>;
                lista = lista.FindAll(s => s.idServicio == 1 || s.idServicio == 2 || s.idServicio == 13 || s.idServicio == 23 || s.idServicio == 24).ToList();
                cbxServicio.ItemsSource = lista;
            }
            List<TipoInsersion> listaInsersion = new List<TipoInsersion>();
            if (new PrivilegioSvc().consultarPrivilegio("INSERSION_KM_CAMBIO_ODOMETRO", usuario.idUsuario).typeResult == ResultTypes.success)
            {
                listaInsersion.Add(new TipoInsersion { enumTipoInsersionKM = enumTipoInsersionKM.CAMBIO_HODÓMETRO });
            }
            if (new PrivilegioSvc().consultarPrivilegio("INSERSION_KM_REGISTRO_KM_PREVENTIVO", usuario.idUsuario).typeResult == ResultTypes.success)
            {
                listaInsersion.Add(new TipoInsersion { enumTipoInsersionKM = enumTipoInsersionKM.REGISTRO_KM_PREVENTIVO });
            }
            if (new PrivilegioSvc().consultarPrivilegio("INSERSION_KM_REGISTRO_KM_TALLER_PEGASO", usuario.idUsuario).typeResult == ResultTypes.success)
            {
                listaInsersion.Add(new TipoInsersion { enumTipoInsersionKM = enumTipoInsersionKM.REGISTRO_KM_TALLER_PEGASO });
            }
            cbxTipoInsersion.ItemsSource = listaInsersion;
            //ctrEmpresa.WidthCombo = 250;

            //cbxTipoInsersion.SelectedIndex = 0;
            nuevo();

            dtpFecha.IsEnabled = new PrivilegioSvc().consultarPrivilegio("AJUSTE_FECHAS_INSERSION_KM", usuario.idUsuario).typeResult == ResultTypes.success;
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ctrUnidadTransporte.loaded(etipoUniadBusqueda.TRACTOR, ctrEmpresa.empresaSelected);
        }
        internal class TipoInsersion
        {
            public enumTipoInsersionKM enumTipoInsersionKM { get; set; }
            public string nombre => enumTipoInsersionKM.ToString().Replace("_", " ");
        }
        private void CbxTipoInsersion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (sender != null)
                {
                    TipoInsersion tipoInsersion = cbxTipoInsersion.SelectedItem as TipoInsersion;
                    enumTipoInsersionKM enumTipo = tipoInsersion.enumTipoInsersionKM;
                    switch (enumTipo)
                    {                        
                        case enumTipoInsersionKM.REGISTRO_KM_PREVENTIVO:
                            cbxServicio.IsEnabled = true;
                            break;
                        default:
                            cbxServicio.SelectedItem = null;
                            cbxServicio.IsEnabled = false;
                            break;
                    }
                    buscarUltimaInsersionKm();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            dtpFecha.Value = DateTime.Now;
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            mapForm(null);
        }
        private void CtrUnidadTransporte_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            buscarUltimaInsersionKm();
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult val = validar();
                if (val.typeResult != ResultTypes.success)
                {
                    return val;
                }
                InsersionKm insersionKm = mapForm();
                if (insersionKm != null)
                {
                    var resp = new InsersionKmSvc().saveInsersionKm(ref insersionKm);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mapForm(insersionKm);
                        mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LA INFORAMACIÓN DE LA PANTALL");
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapForm(InsersionKm insersionKm)
        {
            try
            {
                if (insersionKm != null)
                {
                    ctrClave.Tag = insersionKm;
                    ctrClave.valor = insersionKm.idInsersionKm;
                    ctrUnidadTransporte.unidadTransporte = insersionKm.unidadTransporte;
                    dtpFecha.Value = insersionKm.fechaInsersion;
                    cbxTipoInsersion.SelectedItem = cbxTipoInsersion.Items.Cast<TipoInsersion>().ToList().Find(s => s.enumTipoInsersionKM == insersionKm.enumTipoInsersion);
                    cbxServicio.SelectedItem = insersionKm.servicio == null ? null : cbxServicio.ItemsSource.Cast<Servicio>().ToList().Find(s => s.idServicio == insersionKm.servicio.idServicio);
                    ctrKm.valor = insersionKm.km;
                }
                else
                {
                    ctrClave.Tag = null;
                    ctrClave.valor = 0;
                    ctrUnidadTransporte.unidadTransporte = null;
                    dtpFecha.Value = DateTime.Now;
                    cbxTipoInsersion.SelectedIndex = 0;
                    cbxServicio.SelectedItem = null;
                    ctrKm.valor =0;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private OperationResult validar()
        {
            try
            {
                if (ctrUnidadTransporte.unidadTransporte == null)
                {
                    return new OperationResult(ResultTypes.warning, "SE REQUIERE LA UNIDAD DE TRANSPORTE");
                }
                if (dtpFecha.Value == null)
                {
                    return new OperationResult(ResultTypes.warning, "SE REQUIERE LA FECHA");
                }
                if (cbxTipoInsersion.SelectedItem == null)
                {
                    return new OperationResult(ResultTypes.warning, "SE REQUIERE EL TIPO DE INSERSIÓN");
                }
                if ((cbxTipoInsersion.SelectedItem  as TipoInsersion).enumTipoInsersionKM == enumTipoInsersionKM.REGISTRO_KM_PREVENTIVO)
                {
                    if (cbxServicio.SelectedItem == null)
                    {
                        return new OperationResult(ResultTypes.warning, "SE REQUIERE ESPECIFICAR EL SERVICIO");
                    }
                }
                if (ctrKm.valor <= 0)
                {
                    return new OperationResult(ResultTypes.warning, "SE REQUIERE EL KM");
                }
                
                return new OperationResult(ResultTypes.success, "");
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        InsersionKm mapForm()
        {
            try
            {
                return new InsersionKm
                {
                    idInsersionKm = 0,
                    fechaCaptura = DateTime.Now,
                    usuarioCaptura = mainWindow.usuario.nombreUsuario,
                    unidadTransporte = ctrUnidadTransporte.unidadTransporte,
                    fechaInsersion = dtpFecha.Value.Value,
                    enumTipoInsersion = (cbxTipoInsersion.SelectedItem as TipoInsersion).enumTipoInsersionKM,
                    servicio = cbxServicio.SelectedItem == null ? null : (cbxServicio.SelectedItem as Servicio),
                    km = ctrKm.valor
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void BtnCambiarUnidad_Click(object sender, RoutedEventArgs e)
        {
            ctrUnidadTransporte.unidadTransporte = null;
        }
        void buscarUltimaInsersionKm()
        {
            try
            {
                mapUltimaInsersion(null);
                Cursor = Cursors.Wait;
                if (ctrUnidadTransporte.unidadTransporte == null || cbxTipoInsersion.SelectedItem == null) return;

                var resp = new InsersionKmSvc().getUltimaInsercionByUnidadAndTipo(((TipoInsersion)cbxTipoInsersion.SelectedItem).enumTipoInsersionKM, ctrUnidadTransporte.unidadTransporte.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapUltimaInsersion(resp.result as InsersionKm);
                }
                else
                {
                    mapUltimaInsersion(null);
                }

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapUltimaInsersion(InsersionKm insersionKm)
        {
            if (insersionKm != null)
            {
                txtUltimaFecha.Text = insersionKm.fechaInsersion.ToString("dd/MM/yy HH:mm");
                ctrUltimoKm.valor = insersionKm.km;
                txtUltimoServicio.Text = insersionKm.servicio == null ? string.Empty: insersionKm.servicio.descipcion;
            }
            else
            {
                txtUltimaFecha.Clear();
                ctrUltimoKm.valor = 0;
                txtUltimoServicio.Clear();
            }
        }
    }
}
