﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.Primitives;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para reporteinventarioLlantas.xaml
    /// </summary>
    public partial class reporteinventarioLlantas : ViewBase
    {
        private List<InventarioLlantas> listaInventario = new List<InventarioLlantas>();
        public reporteinventarioLlantas()
        {
            InitializeComponent();
        }
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                this.listaInventario = new List<InventarioLlantas>();
                OperationResult resp = new InventarioLlantasSvc().getInventarioLlantas(this.getListaUnidades);
                if (resp.typeResult == ResultTypes.success)
                {
                    this.listaInventario = resp.result as List<InventarioLlantas>;
                    this.lblContador.Content = this.listaInventario.Count<InventarioLlantas>();
                    this.ctrInventariadas.valor = this.listaInventario.Count<InventarioLlantas>(s => s.estatus == estatusInventario.COMPLETO);
                    this.ctrIncompletas.valor = this.listaInventario.Count<InventarioLlantas>(s => s.estatus == estatusInventario.INCOMPLETO);
                    this.ctrSinInventariar.valor = this.listaInventario.Count<InventarioLlantas>(s => s.estatus == estatusInventario.VACIO);
                    this.filtrar();
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void btnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                new ReportView().generarReporteSemaforoLlantas(this.filtrar());
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void buscarUnidades()
        {
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult resp = new UnidadTransporteSvc().getUnidadesByGrupos(this.ctrEmpresa.empresaSelected.clave, this.listaGrupoStr);
                if (resp.typeResult == ResultTypes.success)
                {
                    this.cbxUnidades.ItemsSource = resp.result as List<UnidadTransporte>;
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                    this.cbxUnidades.ItemsSource = null;
                }
                else
                {
                    this.cbxUnidades.ItemsSource = null;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                this.cbxUnidades.ItemsSource = null;
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                this.cbxTipoUnidad.SelectedItems.Clear();
                this.cbxTipoUnidad.IsEnabled = true;
                this.chcTodosTiposUnidad.IsChecked = false;
                if (this.ctrEmpresa.empresaSelected != null)
                {
                    OperationResult result = new TipoUnidadSvc().getGruposTiposUnidad(this.ctrEmpresa.empresaSelected.clave);
                    if (result.typeResult == ResultTypes.success)
                    {
                        this.cbxTipoUnidad.ItemsSource = result.result as List<GrupoTipoUnidades>;
                    }
                    else
                    {
                        this.cbxTipoUnidad.ItemsSource = null;
                    }
                }
                else
                {
                    this.cbxTipoUnidad.ItemsSource = null;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void cbxTipoUnidad_ItemSelectionChanged(object sender, ItemSelectionChangedEventArgs e)
        {
            this.buscarUnidades();
        }

        private void chc_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                this.filtrar();
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void chcTodosTiposUnidad_Checked(object sender, RoutedEventArgs e)
        {
            this.cbxTipoUnidad.SelectedItems.Clear();
            foreach (object obj2 in (IEnumerable)this.cbxTipoUnidad.Items)
            {
                this.cbxTipoUnidad.SelectedItems.Add(obj2);
            }
            this.cbxTipoUnidad.IsEnabled = false;
            this.buscarUnidades();
        }

        private void chcTodosTiposUnidad_Unchecked(object sender, RoutedEventArgs e)
        {
            this.cbxTipoUnidad.SelectedItems.Clear();
            this.cbxTipoUnidad.IsEnabled = true;
            this.cbxUnidades.SelectedItems.Clear();
            this.cbxUnidades.IsEnabled = true;
            this.chcTodosUnidades.IsChecked = false;
        }

        private void chcTodosUnidades_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                this.cbxUnidades.SelectedItems.Clear();
                foreach (object obj2 in (IEnumerable)this.cbxUnidades.Items)
                {
                    this.cbxUnidades.SelectedItems.Add(obj2);
                }
                this.cbxUnidades.IsEnabled = false;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void chcTodosUnidades_Unchecked(object sender, RoutedEventArgs e)
        {
            this.cbxUnidades.SelectedItems.Clear();
            this.cbxUnidades.IsEnabled = true;
        }

        private List<InventarioLlantas> filtrar()
        {
            List<InventarioLlantas> list2;
            try
            {
                base.Cursor = Cursors.Wait;
                List<InventarioLlantas> listaInventario = new List<InventarioLlantas>();
                if (this.chcInventariadas.IsChecked.Value)
                {
                    foreach (InventarioLlantas llantas in this.listaInventario.FindAll(s => s.estatus == estatusInventario.COMPLETO))
                    {
                        listaInventario.Add(llantas);
                    }
                }
                if (this.chcIncompletas.IsChecked.Value)
                {
                    foreach (InventarioLlantas llantas2 in this.listaInventario.FindAll(s => s.estatus == estatusInventario.INCOMPLETO))
                    {
                        listaInventario.Add(llantas2);
                    }
                }
                if (this.chcSinInventariar.IsChecked.Value)
                {
                    foreach (InventarioLlantas llantas3 in this.listaInventario.FindAll(s => s.estatus == estatusInventario.VACIO))
                    {
                        listaInventario.Add(llantas3);
                    }
                }
                this.mapResultados(listaInventario);
                list2 = listaInventario;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                list2 = null;
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return list2;
        }

        private void mapResultados(List<InventarioLlantas> listaInventario)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                Dispatcher.Invoke(() => {
                    this.stpLlantas.Children.Clear();
                    List<DetalleInventarioLlantas> source = new List<DetalleInventarioLlantas>();
                    foreach (InventarioLlantas llantas in listaInventario)
                    {
                        foreach (DetalleInventarioLlantas llantas2 in llantas.listaDetalles)
                        {
                            source.Add(llantas2);
                        }
                    }
                    this.ctrTotalLlantas.valor = source.Count;
                    this.ctrVerde.valor = source.Count<DetalleInventarioLlantas>(s => s.estatus == estatusDetalleInventario.VERDE);
                    this.ctrGold.valor = source.Count<DetalleInventarioLlantas>(s => s.estatus == estatusDetalleInventario.AMARILLO);
                    this.ctrRojo.valor = source.Count<DetalleInventarioLlantas>(s => s.estatus == estatusDetalleInventario.ROJO);
                    foreach (InventarioLlantas llantas3 in listaInventario)
                    {
                        int num;
                        StackPanel panel = new StackPanel
                        {
                            Orientation = Orientation.Horizontal
                        };
                        Label element = new Label
                        {
                            Content = llantas3.clave,
                            FontSize = 10.0,
                            Width = 50.0,
                            Foreground = Brushes.Black,
                            HorizontalContentAlignment = HorizontalAlignment.Center,
                            VerticalAlignment = VerticalAlignment.Center,
                            FontWeight = FontWeights.Bold
                        };
                        panel.Children.Add(element);
                        Label label3 = new Label
                        {
                            Content = llantas3.noLlantas,
                            FontSize = 10.0,
                            Width = 50.0,
                            Foreground = Brushes.Black,
                            HorizontalContentAlignment = HorizontalAlignment.Center,
                            VerticalAlignment = VerticalAlignment.Center,
                            FontWeight = FontWeights.Bold
                        };
                        panel.Children.Add(label3);
                        Label label4 = new Label
                        {
                            Content = llantas3.inv,
                            FontSize = 10.0,
                            Width = 50.0,
                            Foreground = Brushes.Black,
                            HorizontalContentAlignment = HorizontalAlignment.Center,
                            VerticalAlignment = VerticalAlignment.Center,
                            FontWeight = FontWeights.Bold
                        };
                        panel.Children.Add(label4);
                        Label label5 = new Label
                        {
                            Content = llantas3.sinInv,
                            FontSize = 10.0,
                            Width = 50.0,
                            Foreground = Brushes.Black,
                            HorizontalContentAlignment = HorizontalAlignment.Center,
                            VerticalAlignment = VerticalAlignment.Center,
                            FontWeight = FontWeights.Bold
                        };
                        panel.Children.Add(label5);
                        List<Border> list2 = new List<Border>();
                        for (int i = 1; i <= llantas3.noLlantas; i = num + 1)
                        {
                            Border item = new Border
                            {
                                Tag = i,
                                CornerRadius = new CornerRadius(8.0),
                                BorderBrush = Brushes.Black,
                                BorderThickness = new Thickness(0.8),
                                Margin = new Thickness(1.0),
                                Width = 98.0
                            };
                            DetalleInventarioLlantas llantas4 = llantas3.listaDetalles.Find(s => s.posision == i);
                            Label label2 = new Label
                            {
                                FontSize = 10.0,
                                Foreground = Brushes.Black,
                                FontWeight = FontWeights.Bold,
                                HorizontalContentAlignment = HorizontalAlignment.Center,
                                VerticalContentAlignment = VerticalAlignment.Center
                            };
                            if (llantas4 != null)
                            {
                                if (llantas4.estatus == estatusDetalleInventario.ROJO)
                                {
                                    item.Background = Brushes.Red;
                                }
                                else if (llantas4.estatus == estatusDetalleInventario.AMARILLO)
                                {
                                    item.Background = Brushes.Gold;
                                }
                                else
                                {
                                    item.Background = Brushes.Green;
                                }
                                label2.Tag = llantas4;
                                label2.Content = llantas4.folioLlanta;
                                item.Child = label2;
                            }
                            else
                            {
                                label2.Content = "S/Inv";
                                item.Background = Brushes.White;
                                item.Child = label2;
                            }
                            list2.Add(item);
                            num = i;
                        }
                        foreach (Border border3 in list2)
                        {
                            panel.Children.Add(border3);
                        }
                        Border border = new Border
                        {
                            Tag = llantas3,
                            BorderThickness = new Thickness(0.8),
                            BorderBrush = Brushes.Black,
                            Background = Brushes.SteelBlue,
                            CornerRadius = new CornerRadius(8.0),
                            Margin = new Thickness(1.0)
                        };
                        border.Child = panel;
                        this.stpLlantas.Children.Add(border);
                    }
                });
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.ctrEmpresa.cbxEmpresa.SelectionChanged += new SelectionChangedEventHandler(this.CbxEmpresa_SelectionChanged);
            this.ctrEmpresa.loaded(base.mainWindow.usuario.empresa, new PrivilegioSvc().consultarPrivilegio("ISADMIN", base.mainWindow.usuario.idUsuario).typeResult == ResultTypes.success);
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
        }

        private List<string> listaGrupoStr
        {
            get
            {
                List<string> list = new List<string>();
                foreach (GrupoTipoUnidades unidades in this.cbxTipoUnidad.SelectedItems)
                {
                    list.Add(unidades.grupo);
                }
                return list;
            }
        }

        private List<string> getListaUnidades
        {
            get
            {
                List<string> list = new List<string>();
                foreach (UnidadTransporte transporte in this.cbxUnidades.SelectedItems)
                {
                    list.Add(transporte.clave);
                }
                return list;
            }
        }
    }
}
