﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editClientes.xaml
    /// </summary>
    public partial class catClientesV2 : ViewBase
    {
        public catClientesV2()
        {
            InitializeComponent();
        }
        Empresa empresa = null;
        Usuario usuario = null;
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            usuario = mainWindow.usuario;
            empresa = mainWindow.empresa;
            ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresa.loaded(empresa, (new PrivilegioSvc().consultarPrivilegio("isAdmin", usuario.idUsuario).typeResult == ResultTypes.success));
            ctrEmpresa.cbxEmpresa.FontWeight = FontWeights.Bold;

            var resp = new ImpuestosSvc().getImpuestosALLorById();
            if (resp.typeResult == ResultTypes.success)
            {
                var lista = resp.result as List<Impuestos>;
                cbxIVA.ItemsSource = lista;
                cbxRetencion.ItemsSource = lista;
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
                this.IsEnabled = false;
                return;
            }
            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            empresa = ctrEmpresa.empresaSelected;
        }

        private void TxtColonia_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (e.Key == Key.Enter)
                {
                    if (string.IsNullOrEmpty(txtColonia.Text.Trim()))
                    {
                        var colonia = new selectColonias().getAllColoniaByNombre("");
                        if (colonia != null)
                        {
                            mapColonia(colonia);
                        }
                    }

                    List<Colonia> listaColonias = new List<Colonia>();
                    var resp = new DireccionSvc().getALLColoniasByNombre(txtColonia.Text.Trim());
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listaColonias = resp.result as List<Colonia>;
                        if (listaColonias.Count == 1)
                        {
                            mapColonia(listaColonias[0]);
                        }
                        else
                        {
                            var colonia = new selectColonias().getAllColoniaByNombre(txtColonia.Text.Trim());
                            if (colonia != null)
                            {
                                mapColonia(colonia);
                            }
                        }
                    }
                    else if (resp.typeResult == ResultTypes.recordNotFound)
                    {
                        var colonia = new selectColonias().getAllColoniaByNombre("");
                        if (colonia != null)
                        {
                            mapColonia(colonia);
                        }
                    }
                    else
                    {
                        imprimir(resp);
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void mapColonia(Colonia colonia)
        {
            try
            {
                if (colonia != null)
                {
                    txtColonia.Text = colonia.nombre;
                    txtColonia.IsEnabled = false;
                    txtColonia.Tag = colonia;
                    txtCP.Text = colonia.cp;
                    txtCiudad.Text = colonia.ciudad.nombre;
                    txtEstado.Text = colonia.ciudad.estado.nombre;
                    txtPais.Text = colonia.ciudad.estado.pais.nombrePais;
                }
                else
                {
                    txtColonia.Clear();
                    txtColonia.IsEnabled = true;
                    txtColonia.Tag = null;
                    txtCP.Clear();
                    txtCiudad.Clear();
                    txtEstado.Clear();
                    txtPais.Clear();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void BtnCambiarColonia_Click(object sender, RoutedEventArgs e)
        {
            mapColonia(null);
        }
        private void CbxIVA_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxIVA.SelectedItem != null)
            {
                Impuestos impuestos = cbxIVA.SelectedItem as Impuestos;
                txtValorIVA.Text = string.Format("{0} %", impuestos.valor.ToString("N2"));
            }
        }
        private void CbxRetencion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxRetencion.SelectedItem != null)
            {
                Impuestos impuestos = cbxRetencion.SelectedItem as Impuestos;
                txtValorRetencion.Text = string.Format("{0} %", impuestos.valor.ToString("N2"));
            }
        }
        Cliente mapForm()
        {
            try
            {
                Cliente cliente = new Cliente
                {
                    clave = ctrClave.Tag == null ? 0 : (ctrClave.Tag as Cliente).clave,
                    clave_empresa = empresa.clave,
                    empresa = empresa,
                    objColonia = txtColonia.Tag as Colonia,
                    impuesto = cbxIVA.SelectedItem as Impuestos,
                    impuestoFlete = cbxRetencion.SelectedItem as Impuestos,
                    nombre = txtRazonSocial.Text.Trim(),
                    alias = txtAlias.Text.Trim(),
                    rfc = txtRFC.Text.Trim(),
                    telefono = txtTelefono.Text.Trim(),
                    cobroXkm = chcCobroKm.IsChecked.Value,
                    omitirRemision = chcOmitirRemisiones.IsChecked.Value,
                    activo = chcActivo.IsChecked.Value,
                    fraccion = empresa.clave == 2 ? new Fraccion
                    {
                        clave = 6,
                        clave_empresa = 2,
                        descripcion = "CLIENTES ATLANTE"
                    } : null,
                    domicilio = txtDireccion.Text.Trim(),
                    penalizacion = ctrPenalizacion.valor
                };
                return cliente;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                Cliente cliente = mapForm();

                var resp = new ClienteSvc().saveCliente(ref cliente);
                if (resp.typeResult == ResultTypes.success)
                {
                    if (cliente.activo)
                    {
                        mapForm(cliente);
                        base.guardar();
                    }
                    else
                    {
                        nuevo();
                        mainWindow.habilitar = true;
                    }

                }
                return resp;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public override OperationResult eliminar()
        {
            try
            {
                Cursor = Cursors.Wait;
                Cliente cliente = mapForm();
                cliente.activo = false;
                var resp = new ClienteSvc().saveCliente(ref cliente);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(null);
                    base.eliminar();

                }
                return resp;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        void mapForm(Cliente cliente)
        {
            try
            {
                if (cliente != null)
                {
                    ctrClave.Tag = cliente;
                    ctrClave.valor = cliente.clave;
                    ctrEmpresa.empresaSelected = cliente.empresa;
                    txtRazonSocial.Text = cliente.nombre;
                    txtAlias.Text = cliente.alias;
                    txtRFC.Text = cliente.rfc;
                    txtDireccion.Text = cliente.domicilio;
                    txtTelefono.Text = cliente.telefono;
                    mapColonia(cliente.objColonia);
                    cbxIVA.SelectedItem = cbxIVA.Items.Cast<Impuestos>().ToList().Find(s => s.clave == cliente.impuesto.clave);
                    cbxRetencion.SelectedItem = cbxRetencion.Items.Cast<Impuestos>().ToList().Find(s => s.clave == cliente.impuestoFlete.clave);
                    ctrPenalizacion.valor = cliente.penalizacion;
                    chcOmitirRemisiones.IsChecked = cliente.omitirRemision;
                    chcCobroKm.IsChecked = cliente.cobroXkm;
                    chcActivo.IsChecked = cliente.activo;                    
                }
                else
                {
                    ctrClave.Tag = null;
                    ctrClave.valor = 0;
                    ctrEmpresa.empresaSelected = usuario.empresa;
                    txtRazonSocial.Clear();
                    txtAlias.Clear();
                    txtRFC.Clear();
                    txtDireccion.Clear();
                    txtTelefono.Clear();
                    mapColonia(null);
                    cbxIVA.SelectedIndex = 1;
                    cbxRetencion.SelectedIndex = 2;
                    ctrPenalizacion.valor = 0;
                    chcOmitirRemisiones.IsChecked = false;
                    chcCobroKm.IsChecked = false;
                    chcActivo.IsChecked = true;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        public override void buscar()
        {
            Cliente cliente = new selectClientes(string.Empty).getAllClientesByEmpresa(ctrEmpresa.empresaSelected.clave);
            if (cliente != null)
            {
                mapForm(cliente);
                base.buscar();
            }
            else
            {
                base.nuevo();
            }

        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new ClienteSvc().getAllClienteByEmpresa(ctrEmpresa.empresaSelected.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    new ReportView().exportarListaClientes(resp.result as List<Cliente>);
                }
                else if(resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
