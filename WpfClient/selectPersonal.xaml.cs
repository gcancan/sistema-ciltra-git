﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for selectPersonal.xaml
    /// </summary>
    public partial class selectPersonal : Window
    {
        string parametro = "";
        private int idEmpresa;

        public selectPersonal()
        {
            InitializeComponent();
        }

        public selectPersonal(string parametro, int idEmpresa)
        {
            InitializeComponent();
            this.parametro = parametro;
            this.idEmpresa = idEmpresa;
        }

        public Personal buscarPersonal()
        {
            buscarPersonas();
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlPersonal != null && lvlPersonal.SelectedItem != null)
            {
                return (Personal)((ListViewItem)lvlPersonal.SelectedItem).Content;
            }
            return null;
        }
        private int idCliente = 0;
        public Personal buscarOperadores(int idCliente)
        {
            this.idCliente = idCliente;
            buscarOperadoresByIdCliente();
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlPersonal != null && lvlPersonal.SelectedItem != null)
            {
                return (Personal)((ListViewItem)lvlPersonal.SelectedItem).Content;
            }
            return null;
        }
        public Personal buscarAllPersonal()
        {            
            buscarTodasPersonas();
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlPersonal != null && lvlPersonal.SelectedItem != null)
            {
                return (Personal)((ListViewItem)lvlPersonal.SelectedItem).Content;
            }
            return null;
        }

        public Personal buscarAllDespachador()
        {
            buscarDespachador();
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlPersonal != null && lvlPersonal.SelectedItem != null)
            {
                return (Personal)((ListViewItem)lvlPersonal.SelectedItem).Content;
            }
            return null;
        }
        public Personal buscarAllLavador()
        {
            buscarLavador();
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlPersonal != null && lvlPersonal.SelectedItem != null)
            {
                return (Personal)((ListViewItem)lvlPersonal.SelectedItem).Content;
            }
            return null;
        }

        internal Personal buscarAllLlanteros()
        {
            buscarLlanteros();
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlPersonal != null && lvlPersonal.SelectedItem != null)
            {
                return (Personal)((ListViewItem)lvlPersonal.SelectedItem).Content;
            }
            return null;
        }

        internal Personal buscarAllMecanicos()
        {
            buscarMECANICOS();
            bool? dResult = ShowDialog();
            if (dResult.Value && lvlPersonal != null && lvlPersonal.SelectedItem != null)
            {
                return (Personal)((ListViewItem)lvlPersonal.SelectedItem).Content;
            }
            return null;
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (lvlPersonal.ItemsSource == null) return;
            CollectionViewSource.GetDefaultView(lvlPersonal.ItemsSource).Refresh();
        }       

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtFind.Text = this.parametro;
            lvlPersonal.Focus();
        }
        private void buscar()
        {
            try
            {
                Cursor = Cursors.Wait;
                var find = new OperadorSvc().getOperadoresALLorById(0, idEmpresa);

                if (find.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.success)
                {
                    lvlPersonal.Items.Clear();
                    List<Personal> listPersonal = (List<Personal>)find.result;
                    mapPersonal(listPersonal);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void buscarPersonas()
        {
            try
            {
                Cursor = Cursors.Wait;
                var find = new OperadorSvc().getOperadoresALLorById(0, idEmpresa);

                if (find.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.success)
                {
                    lvlPersonal.Items.Clear();
                    List<Personal> listPersonal = (List<Personal>)find.result;
                    mapPersonal(listPersonal);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void buscarOperadoresByIdCliente()
        {
            try
            {
                Cursor = Cursors.Wait;
                var find = new OperadorSvc().getOperadoresByIdCliente(idCliente);

                if (find.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.success)
                {
                    lvlPersonal.Items.Clear();
                    List<Personal> listPersonal = (List<Personal>)find.result;
                    mapPersonal(listPersonal);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void buscarTodasPersonas()
        {
            try
            {
                Cursor = Cursors.Wait;
                var find = new OperadorSvc().getPersonalALLorById(0);

                if (find.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.success)
                {
                    lvlPersonal.Items.Clear();
                    List<Personal> listPersonal = (List<Personal>)find.result;
                    mapPersonal(listPersonal);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void buscarDespachador()
        {
            try
            {
                Cursor = Cursors.Wait;
                var find = new OperadorSvc().getDespachadoresByNombre();

                if (find.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.success)
                {
                    lvlPersonal.Items.Clear();
                    List<Personal> listPersonal = (List<Personal>)find.result;
                    mapPersonal(listPersonal);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void buscarLavador()
        {
            try
            {
                Cursor = Cursors.Wait;
                var find = new OperadorSvc().getLavadoresByNombre("%");

                if (find.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.success)
                {
                    lvlPersonal.Items.Clear();
                    List<Personal> listPersonal = (List<Personal>)find.result;
                    mapPersonal(listPersonal);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void buscarLlanteros()
        {
            try
            {
                Cursor = Cursors.Wait;
                var find = new OperadorSvc().getLlanterosByNombre("%");

                if (find.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.success)
                {
                    lvlPersonal.Items.Clear();
                    List<Personal> listPersonal = (List<Personal>)find.result;
                    mapPersonal(listPersonal);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void buscarMECANICOS()
        {
            try
            {
                Cursor = Cursors.Wait;
                var find = new OperadorSvc().getMecanicosByNombre("%");

                if (find.typeResult == ResultTypes.error)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.recordNotFound)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.warning)
                {
                    MessageBox.Show(string.Format(find.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                    controles.IsEnabled = false;
                }
                else if (find.typeResult == ResultTypes.success)
                {
                    lvlPersonal.Items.Clear();
                    List<Personal> listPersonal = (List<Personal>)find.result;
                    mapPersonal(listPersonal);
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void mapPersonal(List<Personal> listPersonal)
        {
            lvlPersonal.Items.Clear();
            List<ListViewItem> lvlList = new List<ListViewItem>();
            foreach (Personal item in listPersonal)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick; ;
                lvlList.Add(lvl);
                //lvlUnidades.Items.Add(lvl);
            }
            lvlPersonal.ItemsSource = lvlList;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlPersonal.ItemsSource);
            view.Filter = UserFilter;
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((Personal)(item as ListViewItem).Content).clave.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Personal)(item as ListViewItem).Content).empresa.nombre.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((Personal)(item as ListViewItem).Content).nombre.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void lvlPersonal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                DialogResult = false;
            }
        }

        private void txtFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Down)
            {
                lvlPersonal.Focus();
            }
        }
    }
}
