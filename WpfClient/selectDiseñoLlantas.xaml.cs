﻿using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectDiseñoLlantas.xaml
    /// </summary>
    public partial class selectDiseñoLlantas : Window
    {
        public selectDiseñoLlantas()
        {
            InitializeComponent();
        }
        private void cargarDiseños()
        {
            try
            {
                OperationResult resp = new DiseñosLlantaSvc().getDiseñosLlanta();
                if (resp.typeResult == ResultTypes.success)
                {
                    this.llenarLista((resp.result as List<Diseño>).OrderBy(s => s.descripcion).ToList());
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    base.DialogResult = false;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        public Diseño getDiseñoLlanta()
        {
            try
            {
                this.cargarDiseños();
                if (base.ShowDialog().Value && (this.lvlDiseños.SelectedItem != null))
                {
                    return (Diseño)((ListViewItem)this.lvlDiseños.SelectedItem).Content;
                }
                return null;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }
        private void llenarLista(List<Diseño> listaDiseños)
        {
            List<ListViewItem> list = new List<ListViewItem>();
            foreach (Diseño diseño in listaDiseños)
            {
                ListViewItem item = new ListViewItem
                {
                    Content = diseño
                };
                item.MouseDoubleClick += Lvl_MouseDoubleClick;
                list.Add(item);
            }
            this.lvlDiseños.ItemsSource = list;
            CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(lvlDiseños.ItemsSource);
            defaultView.Filter = UserFilter;
        }
        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }
        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.lvlDiseños.ItemsSource != null)
            {
                CollectionViewSource.GetDefaultView(this.lvlDiseños.ItemsSource).Refresh();
            }
        }
        private bool UserFilter(object item)
        {
            try
            {
                if (string.IsNullOrEmpty(txtFind.Text))
                {
                    return true;
                }
                var r = (
                    ((Diseño)(item as ListViewItem).Content).descripcion.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                    (((Diseño)(item as ListViewItem).Content).medida == null ? "" : ((Diseño)(item as ListViewItem).Content).medida.medida).IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                    (((Diseño)(item as ListViewItem).Content).tipoLlanta == null ? "" : ((Diseño)(item as ListViewItem).Content).tipoLlanta.nombre).IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0
                       );
                return r;
            }
            catch (Exception ex)
            {
                return false;
            }            
        }


    }
}
