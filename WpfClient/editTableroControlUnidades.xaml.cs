﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editTableroControlUnidades.xaml
    /// </summary>
    public partial class editTableroControlUnidades : ViewBase
    {
        public editTableroControlUnidades()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                ctrTractor.DataContextChanged += CtrTractor_DataContextChanged;
                var resp = new PrivilegioSvc().consultarPrivilegio("ISADMIN", mainWindow.inicio._usuario.idUsuario);
                if (resp.typeResult == ResultTypes.error)
                {
                    return;
                }
                ctrEmpresa.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
                ctrEmpresa.loaded(mainWindow.empresa, resp.typeResult == ResultTypes.success);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void CtrTractor_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            iniciarBuqueda();
        }

        private void iniciarBuqueda()
        {
            try
            {
                Cursor = Cursors.Wait;
                lvlEnDestino.SelectedItem = null;
                btnLlegadaDestino.IsEnabled = false;
                btnSalidaDestino.IsEnabled = false;
                if (ctrTractor.unidadTransporte != null)
                {
                    var respTablero = new TableroControlUnidadesSvc().getUltimoTableroActivoByTractor(ctrTractor.unidadTransporte.clave);
                    if (respTablero.typeResult == ResultTypes.success)
                    {
                        TableroControlUnidades tablero = respTablero.result as TableroControlUnidades;
                        mapTablero(tablero);
                    }
                    else if (respTablero.typeResult == ResultTypes.recordNotFound)
                    {
                        var resp = new CartaPorteSvc().getUltimoViajeByIdTractor(ctrTractor.unidadTransporte.clave);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            Viaje viaje = (resp.result as List<Viaje>)[0];
                            if (viaje.EstatusGuia != "ACTIVO")
                            {
                                imprimir(new OperationResult(ResultTypes.warning, "No hay Viajes Activos para este Tractor"));
                                nuevo();
                                return;
                            }
                            mapViaje(viaje);
                        }
                        else
                        {
                            imprimir(resp);
                        }
                    }
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapTablero(TableroControlUnidades tablero)
        {
            try
            {
                if (tablero != null)
                {
                    gboxDestinos.Tag = tablero;
                    mapViaje(tablero.viaje, false);
                    mapDetalles(tablero.listaDetalles);
                    gBoxInicioTrayecto.IsEnabled = false;
                    txtEntradaPlanta.Text = tablero.fechaPrimeraEntradaPlanta == null ? string.Empty : tablero.fechaPrimeraEntradaPlanta.ToString();
                    txtSalidaPlanta.Text = tablero.fechaSalidaPlanta.ToString();
                    txtllegadaGranja.Text = tablero.fechaLlegadaDestino == null ? string.Empty : tablero.fechaLlegadaDestino.ToString();
                    txtlSalidaGranja.Text = tablero.fechaSalidaDestino == null ? string.Empty : tablero.fechaSalidaDestino.ToString();
                    txtRetornoPlanta.Text = tablero.fechaEntradaPlanta == null ? string.Empty : tablero.fechaEntradaPlanta.ToString();
                    
                }
                else
                {
                    gboxDestinos.Tag = null;
                    mapViaje(null);
                    mapDetalles(new List<DetalleTableroControlUnidades>());
                    txtEntradaPlanta.Clear();
                    txtSalidaPlanta.Clear();
                    txtllegadaGranja.Clear();
                    txtlSalidaGranja.Clear();
                    txtRetornoPlanta.Clear();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void mapViaje(Viaje viaje, bool omitirDetalles = true)
        {
            try
            {
                if (viaje != null)
                {
                    ctrRemolque1.unidadTransporte = viaje.remolque;
                    ctrDolly.unidadTransporte = viaje.dolly;
                    ctrRemolque2.unidadTransporte = viaje.remolque2;
                    ctrOperadores.personal = viaje.operador;
                    ctrFolioViaje.valor = viaje.NumGuiaId;
                    ctrFolioViaje.Tag = viaje;
                    txtFechaViaje.Text = viaje.FechaHoraViaje.ToString();
                    txtEstatusViaje.Text = viaje.EstatusGuia;
                    if (omitirDetalles)
                    {
                        mapDetalles(viaje.listDetalles);
                        gboxDestinos.IsEnabled = false;
                    }
                    else
                    {
                        gboxDestinos.IsEnabled = true;
                    }
                }
                else
                {
                    ctrTractor.limpiar();
                    ctrRemolque1.limpiar();
                    ctrDolly.limpiar();
                    ctrRemolque2.limpiar();
                    ctrOperadores.limpiar();
                    ctrFolioViaje.limpiar();
                    ctrFolioViaje.Tag = null;
                    txtFechaViaje.Clear();
                    txtEstatusViaje.Clear();
                    mapDetalles(new List<CartaPorte>());
                    gBoxInicioTrayecto.IsEnabled = true;
                    gboxDestinos.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private void mapDetalles(List<CartaPorte> listDetalles)
        {
            lvlEnDestino.Items.Clear();
            foreach (var item in listDetalles)
            {
                ListViewItem lvl = new ListViewItem();
                DetalleTableroControlUnidades detalle = new DetalleTableroControlUnidades()
                {
                    idDetalleTableroControlUnidades = 0,
                    cartaPorte = item,
                    fechaLLegada = null,
                    fechaSalida = null,
                    consecutivo = item.Consecutivo,
                    tiempo = 0
                };
                lvl.Content = detalle;
                lvlEnDestino.Items.Add(lvl);
            }
        }
        private void mapDetalles(List<DetalleTableroControlUnidades> listDetalles)
        {
            lvlEnDestino.Items.Clear();
            foreach (var item in listDetalles)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.Selected += Lvl_Selected;
                lvl.LostFocus += Lvl_LostFocus;
                lvlEnDestino.Items.Add(lvl);
            }
        }

        private void Lvl_LostFocus(object sender, RoutedEventArgs e)
        {
            //btnLlegadaDestino.IsEnabled = false;
            //btnSalidaDestino.IsEnabled = false;
        }

        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            DetalleTableroControlUnidades detalle = (sender as ListViewItem).Content as DetalleTableroControlUnidades;
            if (detalle.fechaLLegada == null)
            {
                btnLlegadaDestino.IsEnabled = true;
                btnSalidaDestino.IsEnabled = false;
            }
            else if (detalle.fechaSalida == null)
            {
                btnLlegadaDestino.IsEnabled = false;
                btnSalidaDestino.IsEnabled = true;
            }
            else
            {
                btnLlegadaDestino.IsEnabled = false;
                btnSalidaDestino.IsEnabled = false;
            }
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            nuevo();
            ctrTractor.loaded(etipoUniadBusqueda.TRACTOR, ctrEmpresa.empresaSelected);
        }
        public override void nuevo()
        {
            base.nuevo();
            dtpFecha.Value = DateTime.Now;
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
            mapViaje(null);
            mapTablero(null);
            btnLlegadaDestino.IsEnabled = false;
            btnSalidaDestino.IsEnabled = false;
        }

        private void btnIniciarEstado_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (ctrFolioViaje.Tag != null)
                {
                    Viaje viaje = ctrFolioViaje.Tag as Viaje;
                    TableroControlUnidades tableroControlUnidades = new TableroControlUnidades();
                    tableroControlUnidades.viaje = viaje;
                    tableroControlUnidades.fechaSalidaPlanta = viaje.FechaHoraViaje;

                    tableroControlUnidades.listaDetalles = new List<DetalleTableroControlUnidades>();
                    foreach (CartaPorte cp in viaje.listDetalles)
                    {
                        tableroControlUnidades.listaDetalles.Add(new DetalleTableroControlUnidades
                        {
                            idDetalleTableroControlUnidades = 0,
                            cartaPorte = cp,
                            tiempo = 0,
                            fechaLLegada = null,
                            fechaSalida = null
                        });
                    }

                    var resp = new TableroControlUnidadesSvc().iniciarTrayectoGranja(ref tableroControlUnidades);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        iniciarBuqueda();
                    }
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void btnLlegadaDestino_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //DetalleTableroControlUnidades detalle = (sender as ListViewItem).Content as DetalleTableroControlUnidades;
                
                if (!validaSiPuedeEntrar())
                {
                    imprimir(new OperationResult(ResultTypes.warning, "FALTA EL REGISTRO DE SALIDA DE OTROS EVENTOS"));
                    return;
                }
                if (gboxDestinos.Tag != null && lvlEnDestino.SelectedItem != null && dtpFecha.Value != null)
                {
                    TableroControlUnidades tablero = gboxDestinos.Tag as TableroControlUnidades;
                    DetalleTableroControlUnidades detalle = (lvlEnDestino.SelectedItem as ListViewItem).Content as DetalleTableroControlUnidades;
                    var respRegistro = new TableroControlUnidadesSvc().registrarEntradaGranja(tablero.idTableroControlUnidades, detalle.idDetalleTableroControlUnidades, (DateTime)dtpFecha.Value);
                    if (respRegistro.typeResult == ResultTypes.success)
                    {
                        iniciarBuqueda();
                    }
                    imprimir(respRegistro);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private void btnSalidaDestino_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!validaSiPuedeSalir())
                {
                    //imprimir(new OperationResult(ResultTypes.warning, "FALTA EL REGISTRO DE SALIDA DE OTROS EVENTOS"));
                    return;
                }
                if (gboxDestinos.Tag != null && lvlEnDestino.SelectedItem != null && dtpFecha.Value != null)
                {
                    TableroControlUnidades tablero = gboxDestinos.Tag as TableroControlUnidades;
                    DetalleTableroControlUnidades detalle = (lvlEnDestino.SelectedItem as ListViewItem).Content as DetalleTableroControlUnidades;
                    var respRegistro = new TableroControlUnidadesSvc().registrarSalidaGranja(tablero.idTableroControlUnidades, detalle.idDetalleTableroControlUnidades, (DateTime)dtpFecha.Value);
                    if (respRegistro.typeResult == ResultTypes.success)
                    {
                        iniciarBuqueda();
                    }
                    imprimir(respRegistro);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private bool validaSiPuedeEntrar()
        {
            try
            {
                TableroControlUnidades tablero = gboxDestinos.Tag as TableroControlUnidades;
                var list = tablero.listaDetalles.Find(s => s.fechaLLegada != null && s.fechaSalida == null);
                return list == null;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return false;
            }
        }

        private bool validaSiPuedeSalir()
        {
            try
            {
                if (lvlEnDestino.SelectedItem != null)
                {
                    DetalleTableroControlUnidades detalle = (lvlEnDestino.SelectedItem as ListViewItem).Content as DetalleTableroControlUnidades;
                    if (dtpFecha.Value.Value < detalle.fechaLLegada)
                    {
                        imprimir(new OperationResult(ResultTypes.warning, "La fecha de Salida no puede ser menor a la de salida"));
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return false;
            }
        }


    }
}
