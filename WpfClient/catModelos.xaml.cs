﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using Core.Interfaces;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para catModelos.xaml
    /// </summary>
    public partial class catModelos : ViewBase
    {
        public catModelos()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new MarcasSvc().getMarcasALLorById();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxMarcas.ItemsSource = (resp.result as List<Marca>).OrderBy(s =>s.descripcion).ToList().FindAll(s => s.activo);
                }
                else
                {
                    this.IsEnabled = false;
                    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    imprimir(resp);
                    return;
                }
                nuevo();
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);
        }
        public override OperationResult eliminar()
        {
            try
            {
                Cursor = Cursors.Wait;
                Modelo modelo = (ctrClave.Tag as Modelo);
                modelo.activo = false;
                var resp = new ModeloSvc().saveModelo(ref modelo);
                if (resp.typeResult == ResultTypes.success)
                {
                    nuevo();
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;

                OperationResult valResp = validar();
                if (valResp.typeResult != ResultTypes.success) return valResp;

                Modelo modelo = mapForm();
                if (modelo != null)
                {
                    var resp = new ModeloSvc().saveModelo(ref modelo);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mapForm(modelo);
                        base.guardar();
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LA INFORMACIÓN DE LA PANTALLA");
                }

            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private Modelo mapForm()
        {
            try
            {
                return new Modelo
                {
                    idModelo = ctrClave.Tag == null ? 0 : (ctrClave.Tag as Modelo).idModelo,
                    marca = cbxMarcas.SelectedItem as Marca,
                    modelo = txtModelo.Text.Trim(),
                    activo = chcActivo.IsChecked.Value
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private void mapForm(Modelo modelo)
        {
            if (modelo != null)
            {
                ctrClave.Tag = modelo;
                ctrClave.valor = modelo.idModelo;
                cbxMarcas.SelectedItem = (cbxMarcas.ItemsSource as List<Marca>).Find(s => s.clave == modelo.marca.clave);
                txtModelo.Text = modelo.modelo;
                chcActivo.IsChecked = modelo.activo;
            }
            else
            {
                ctrClave.Tag = null;
                ctrClave.valor = 0;
                cbxMarcas.SelectedItem = null;
                txtModelo.Clear();
                chcActivo.IsChecked = true;
            }
        }
        public override void buscar()
        {
            Modelo modelo = new selectModelos().buscarModelo();
            if (modelo != null)
            {
                base.buscar();
                mapForm(modelo);
                if (!modelo.activo)
                {
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.editar | ToolbarCommands.cerrar);
                }
            }            
        }
        private OperationResult validar()
        {
            try
            {
                if (cbxMarcas.SelectedItem == null)
                {
                    return new OperationResult(ResultTypes.warning, "NO SE PROPORCIONO LA MARCA PARA EL MODELO");
                }
                if (string.IsNullOrEmpty(txtModelo.Text.Trim()))
                {
                    return new OperationResult(ResultTypes.warning, "NO SE PROPORCIONO LA DESCRIPCIÓN");
                }
                return new OperationResult(ResultTypes.success, "");
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
