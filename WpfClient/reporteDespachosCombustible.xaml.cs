﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para reporteDespachosCombustible.xaml
    /// </summary>
    public partial class reporteDespachosCombustible : ViewBase
    {
        private bool inicio = true;
        public reporteDespachosCombustible()
        {
            InitializeComponent();
        }
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            this.buscarDespachos();
        }
        private void buscarDespachos()
        {
            try
            {
                if (this.txtNombreTanque.Tag == null) return;

                base.Cursor = Cursors.Wait;
                this.lvlDespachos.ItemsSource = null;
                this.lvlResumen.ItemsSource = null;
                this.lblTotal.Content = "0.0000";
                OperationResult resp = new CargaCombustibleSvc().getDespachosCombustibleByFiltros(this.ctrFechas.fechaInicial, this.ctrFechas.fechaFinal, (this.txtNombreTanque.Tag as TanqueCombustible).idTanqueCombustible, this.chcTodasEmpresas.IsChecked.Value ? "%" : this.ctrEmpresa.empresaSelected.clave.ToString());
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Despacho> source = resp.result as List<Despacho>;
                    this.lvlDespachos.ItemsSource = source;
                    List<GroupDespachos> list2 = (from p in source
                                                  group p by p.cliente into g
                                                  select new GroupDespachos
                                                  {
                                                      cliente = (from s in g select s.cliente).First<string>(),
                                                      lts = g.Sum<Despacho>(s => s.despachado)
                                                  }).ToList<GroupDespachos>();
                    this.lvlResumen.ItemsSource = list2;
                    this.lblTotal.Content = source.Sum<Despacho>(s => s.despachado).ToString("N4");
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    if (!this.inicio)
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
                this.inicio = false;
            }
        }
        private void Exportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                if (this.lvlDespachos.ItemsSource != null)
                {
                    new ReportView().generarReporteDespachosCombustible(this.ctrFechas.fechaInicial, this.ctrFechas.fechaFinal, this.lvlDespachos.ItemsSource as List<Despacho>);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.ctrEmpresa.loaded(base.mainWindow.empresa, true);

            this.ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += new SelectionChangedEventHandler(this.CbxZonaOpetativa_SelectionChanged);
            this.ctrZonaOperativa.cargarControl(base.mainWindow.usuario.zonaOperativa, base.mainWindow.usuario.idUsuario);

            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
        }
        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            limpiarListas();
            if (ctrZonaOperativa.zonaOperativa != null)
            {
                OperationResult result = new TanqueCombustibleSvc().getTanqueByZonaOperativa(this.ctrZonaOperativa.zonaOperativa);
                if (result.typeResult == ResultTypes.success)
                {
                    TanqueCombustible combustible = result.result as TanqueCombustible;
                    this.txtNombreTanque.Tag = combustible;
                    this.txtNombreTanque.Text = combustible.nombre;
                }
                else
                {
                    this.txtNombreTanque.Tag = null;
                    this.txtNombreTanque.Clear();
                }
            }
        }
        private void limpiarListas()
        {
            this.lvlDespachos.ItemsSource = null;
            this.lvlResumen.ItemsSource = null;
        }
    }
}
