﻿using Core.Interfaces;
using Core.Models;
using Core.Utils;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ProcesosComercial;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for cotizacionSolicitudOrdenCompra.xaml
    /// </summary>
    public partial class cotizacionSolicitudOrdenCompra : ViewBase
    {
        public cotizacionSolicitudOrdenCompra()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            //cbxEmpresa.loaded(null);
            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);
        }

        private void txtFolioSolicitud_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrEmpty(txtFolioSolicitud.Text.Trim()))
                {
                    int i = 0;
                    if (int.TryParse(txtFolioSolicitud.Text.Trim(), out i))
                    {
                        buscarSolicitudOrden(i);
                    }
                }
                else
                {
                    buscar();
                }
            }
        }

        private void buscarSolicitudOrden(int i)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new SolicitudOrdenCompraSvc().getSolicitudOrdComAllOrById(i);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapFormSolicitud((SolicitudOrdenCompra)resp.result);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapFormSolicitud(SolicitudOrdenCompra solicitud, bool bandera = false)
        {
            if (solicitud != null)
            {
                txtFolioSolicitud.Tag = solicitud;
                txtFolioSolicitud.Text = solicitud.idSolicitudOrdenCompra.ToString();
                txtFechaSolicitud.Text = solicitud.fecha.ToString();
                txtEstatus.Text = solicitud.estatus;
                gBoxSolicitud.IsEnabled = false;
                cbxEmpresa.loaded(solicitud.empresa);
                if (solicitud.estatus == "ACTIVO")
                {                    
                    mapListaDetalles(solicitud.listaDetalles, solicitud.isLlantas);                    
                    stpDetalles.Focus();
                }
                else if (solicitud.estatus == "COTIZADO")
                {
                    if (bandera)
                    {
                        var resp = new CotizacionesSvc().getCotizacionAllOrByIdSolicitud(solicitud.idSolicitudOrdenCompra);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            mapForm(resp.result as CotizacionSolicitudOrdCompra, true);
                        }
                        else
                        {
                            ImprimirMensaje.imprimir(resp);
                        }
                    }
                }
            }
            else
            {
                txtFolioSolicitud.Tag = solicitud;
                txtFolioSolicitud.Clear();
                txtFolioSolicitud.Clear();
                txtEstatus.Clear();
                gBoxSolicitud.IsEnabled = true;
                cbxEmpresa.loaded(null);
                mapListaDetalles(new List<DetalleSolicitudOrdenCompra>());
                txtFechaSolicitud.Clear();
            }
        }

        private void mapListaDetalles(List<DetalleSolicitudOrdenCompra> listaDetalles, bool isLlantas = true)
        {
            stpDetalles.Children.Clear();
            foreach (DetalleSolicitudOrdenCompra item in listaDetalles)
            {
                DetalleCotizacionSolicitudOrdCompra detalle = new DetalleCotizacionSolicitudOrdCompra
                {
                    medidaLlanta = item.medidaLlanta,
                    llantaProducto = item.productoLlanta,
                    producto = item.producto,
                    cantidad = item.cantidad,
                    unidadTransporte = item.unidadTrans,
                    listDetalles = new List<DetalleCotizaciones>()
                };
                if (isLlantas)
                {
                    tapControl.SelectedIndex = 0;
                    llenarLista(detalle);
                }
                else
                {
                    tapControl.SelectedIndex = 1;
                    llenarListaInsumos(detalle);
                }
            }
        }

        private void llenarLista(DetalleCotizacionSolicitudOrdCompra detalle)
        {
            CheckBox chcPrincipal = new CheckBox { FontSize = 11, Tag = detalle };
            StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal, Tag = detalle };
            if ((stpDetalles.Children.Count % 2) == 0)
            {
                stpContent.Background = Brushes.Silver;
            }

            Label lblCantidad = new Label
            {
                Name = "lblCantidad",
                Width = 60,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                VerticalContentAlignment = VerticalAlignment.Center,
                Content = detalle.cantidad
            };
            stpContent.Children.Add(lblCantidad);

            Label lblUnidad = new Label
            {
                Width = 80,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                VerticalContentAlignment = VerticalAlignment.Center,
                Content = detalle.unidadTransporte == null ? "" : detalle.unidadTransporte.clave
            };
            stpContent.Children.Add(lblUnidad);
            Label lblProducto = new Label
            {
                Width = 180,
                Content = detalle.medidaLlanta,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                VerticalContentAlignment = VerticalAlignment.Center
            };
            stpContent.Children.Add(lblProducto);
            if (detalle.idDetalleCotizacionSolicitudOrdCompra == 0)
            {
                controlMarcas ctrMarcas = new controlMarcas() { Tag = chcPrincipal };
                ctrMarcas.cbxMarcas.Tag = chcPrincipal;
                ctrMarcas.cbxMarcas.SelectionChanged += CbxMarcas_SelectionChanged;
                ctrMarcas.cargarControl(detalle.medidaLlanta);
                ctrMarcas.productoLlanta = detalle.llantaProducto;
                stpContent.Children.Add(ctrMarcas);

                StackPanel stpProveedores = new StackPanel
                {
                    Name = "stpProveedores",
                    Orientation = Orientation.Vertical,
                    VerticalAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(0),
                    Width = 510
                };
                stpContent.Children.Add(stpProveedores);
            }
            else
            {
                Label lblProductoLlanta = new Label
                {
                    Width = 240,
                    Content = detalle.llantaProducto.descripcion,
                    FontWeight = FontWeights.Medium,
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    VerticalContentAlignment = VerticalAlignment.Center,
                };
                stpContent.Children.Add(lblProductoLlanta);

                StackPanel stpProveedores = new StackPanel
                {
                    Name = "stpProveedores",
                    Orientation = Orientation.Vertical,
                    VerticalAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(0),
                    Width = 510
                };
                
                StackPanel stpSecundario = new StackPanel { Orientation = Orientation.Vertical };
                foreach (var item in detalle.listDetalles)
                {
                    RadioButton rbn = new RadioButton
                    {
                        DataContext = item.proveedor,
                        Width = 504,
                        VerticalContentAlignment = VerticalAlignment.Center,
                        Tag = chcPrincipal,
                        HorizontalAlignment = HorizontalAlignment.Left,
                        FontSize = 10,
                        IsChecked = item.elegido
                    };
                    rbn.Checked += Rbn_Checked;
                    rbn.MouseDoubleClick += Rbn_MouseDoubleClick;
                    StackPanel stpLbl = new StackPanel { Orientation = Orientation.Horizontal, HorizontalAlignment = HorizontalAlignment.Left };
                    Label lblProveedor = new Label
                    {
                        Content =item.proveedor.razonSocial,
                        Width = 220,
                        FontSize = 10,
                        Foreground = Brushes.SteelBlue,
                        FontWeight = FontWeights.Bold
                    };
                    stpLbl.Children.Add(lblProveedor);

                    Label lblUltimoPrecio = new Label
                    {
                        Content = item.proveedor.ultimoCosto,
                        Width = 60,
                        FontSize = 10,
                        Foreground = Brushes.SteelBlue,
                        FontWeight = FontWeights.Bold
                    };
                    stpLbl.Children.Add(lblUltimoPrecio);

                    controlDecimal ctrDecimal = new controlDecimal
                    {
                        Tag = rbn,
                        Width = 60,
                        FontSize = 10,
                        valor = item.costo,
                        Foreground = Brushes.SteelBlue,
                        FontWeight = FontWeights.Bold,
                        IsEnabled = false
                    };
                    ctrDecimal.txtDecimal.Tag = chcPrincipal;
                    ctrDecimal.txtDecimal.TextChanged += Txt_TextChanged;
                    stpLbl.Children.Add(ctrDecimal);

                    controlEnteros ctrDiasEntrega = new controlEnteros
                    {
                        Tag = rbn,
                        Name = "ctrDiasEntrega",
                        Width = 60,
                        FontSize = 10,
                        IsEnabled = false,
                        valor = item.diasEntrega,
                        Foreground = Brushes.SteelBlue,
                        FontWeight = FontWeights.Bold
                    };
                    ctrDiasEntrega.txtEntero.Tag = chcPrincipal;
                    ctrDiasEntrega.txtEntero.TextChanged += Txt_TextChanged;
                    stpLbl.Children.Add(ctrDiasEntrega);

                    controlEnteros ctrDiasCredito = new controlEnteros
                    {
                        Tag = rbn,
                        Name = "ctrDiasCredito",
                        Width = 60,
                        FontSize = 10,
                        valor = item.diasCredito,
                        IsEnabled = false,
                        Foreground = Brushes.SteelBlue,
                        FontWeight = FontWeights.Bold
                    };
                    ctrDiasCredito.txtEntero.Tag = chcPrincipal;
                    ctrDiasCredito.txtEntero.TextChanged += Txt_TextChanged;
                    stpLbl.Children.Add(ctrDiasCredito);
                    
                    rbn.Content = stpLbl;
                    stpSecundario.Children.Add(rbn);
                }

                stpProveedores.Children.Add(stpSecundario);
                stpContent.Children.Add(stpProveedores);

                

                //Label lblProveedor = new Label
                //{
                //    Width = 510,
                //    Content = detalle.proveedor.razonSocial,
                //    FontWeight = FontWeights.Medium,
                //    HorizontalContentAlignment = HorizontalAlignment.Left,
                //    VerticalContentAlignment = VerticalAlignment.Center,
                //};
                //stpContent.Children.Add(lblProveedor);
            }

            Label lblCostoUnitario = new Label
            {
                Width = 120,
                Content = detalle.costo,
                Name = "lblCostoUnitario",
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.Blue,
                FontWeight = FontWeights.Bold
            };
            stpContent.Children.Add(lblCostoUnitario);

            Label lblImporte = new Label
            {
                Width = 150,
                Content = detalle.importe,
                Name = "lblImporte",
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.Blue,
                FontWeight = FontWeights.Bold
            };
            stpContent.Children.Add(lblImporte);

            chcPrincipal.Content = stpContent;
            if (detalle.idDetalleCotizacionSolicitudOrdCompra != 0)
            {
                //chcPrincipal.IsEnabled = false;
                chcPrincipal.IsChecked = true;
            }
            stpDetalles.Children.Add(chcPrincipal);
        }

        private void llenarListaInsumos(DetalleCotizacionSolicitudOrdCompra detalle)
        {
            CheckBox chcPrincipal = new CheckBox { FontSize = 11, Tag = detalle };
            StackPanel stpContent = new StackPanel { Orientation = Orientation.Horizontal, Tag = detalle };
            if ((stpDetallesInsumos.Children.Count % 2) == 0)
            {
                stpContent.Background = Brushes.Silver;
            }

            Label lblCantidad = new Label
            {
                Name = "lblCantidad",
                Width = 60,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                VerticalContentAlignment = VerticalAlignment.Center,
                Content = detalle.cantidad
            };
            stpContent.Children.Add(lblCantidad);

            Label lblProducto = new Label
            {
                Width = 300,
                Content = detalle.producto.nombre,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                VerticalContentAlignment = VerticalAlignment.Center
            };
            stpContent.Children.Add(lblProducto);
            if (detalle.idDetalleCotizacionSolicitudOrdCompra == 0)
            {
                //controlMarcas ctrMarcas = new controlMarcas() { Tag = chcPrincipal };
                //ctrMarcas.cbxMarcas.Tag = chcPrincipal;
                //ctrMarcas.cbxMarcas.SelectionChanged += CbxMarcas_SelectionChanged;
                //ctrMarcas.cargarControl(detalle.medidaLlanta);
                //ctrMarcas.productoLlanta = detalle.llantaProducto;
                //stpContent.Children.Add(ctrMarcas);

                StackPanel stpProveedores = new StackPanel
                {
                    Name = "stpProveedores",
                    Orientation = Orientation.Vertical,
                    VerticalAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(0),
                    Width = 510
                };
                stpContent.Children.Add(stpProveedores);

                var resp = new ProveedorSvc().getAllProveedores();

                if (resp.typeResult == ResultTypes.success)
                {
                    ComboBox cbxProveedores = new ComboBox
                    {
                        Tag = chcPrincipal,
                        Width = 505,
                        Height = 22,
                        ItemsSource = (List<Proveedor>)resp.result,
                        DisplayMemberPath = "razonSocial",
                        FontSize = 11,
                        VerticalAlignment = VerticalAlignment.Center,
                        Margin = new Thickness(2)
                    };

                    cbxProveedores.SelectionChanged += CbxProveedores_SelectionChanged;
                    stpProveedores.Children.Add(cbxProveedores);
                    StackPanel stpRbn = new StackPanel
                    {
                        Orientation = Orientation.Vertical,
                        Name = "stpRbn",
                        VerticalAlignment = VerticalAlignment.Center,
                        HorizontalAlignment = HorizontalAlignment.Left
                    };
                    stpProveedores.Children.Add(stpRbn);
                }
                else
                {
                    Label lblProveedor = new Label
                    {
                        Content = "NO EXISTE PROVEEDOR ASIGNADO PARA ESTA MARCA",
                        Width = 504,
                        FontWeight = FontWeights.Bold,
                        Foreground = Brushes.Red
                    };
                    stpProveedores.Children.Add(lblProveedor);
                    //chcPrincipal.IsEnabled = false;
                }

            }
            else
            {
                StackPanel stpProveedores = new StackPanel
                {
                    Name = "stpProveedores",
                    Orientation = Orientation.Vertical,
                    VerticalAlignment = VerticalAlignment.Center,
                    Margin = new Thickness(0),
                    Width = 510
                };

                StackPanel stpSecundario = new StackPanel { Orientation = Orientation.Vertical };
                foreach (var item in detalle.listDetalles)
                {
                    RadioButton rbn = new RadioButton
                    {
                        DataContext = item.proveedor,
                        Width = 504,
                        VerticalContentAlignment = VerticalAlignment.Center,
                        Tag = chcPrincipal,
                        HorizontalAlignment = HorizontalAlignment.Left,
                        FontSize = 10,
                        IsChecked = item.elegido
                    };
                    rbn.Checked += Rbn_Checked;
                    rbn.MouseDoubleClick += Rbn_MouseDoubleClick;
                    StackPanel stpLbl = new StackPanel { Orientation = Orientation.Horizontal, HorizontalAlignment = HorizontalAlignment.Left };
                    Label lblProveedor = new Label
                    {
                        Content = item.proveedor.razonSocial,
                        Width = 220,
                        FontSize = 10,
                        Foreground = Brushes.SteelBlue,
                        FontWeight = FontWeights.Bold
                    };
                    stpLbl.Children.Add(lblProveedor);

                    Label lblUltimoPrecio = new Label
                    {
                        Content = item.proveedor.ultimoCosto,
                        Width = 60,
                        FontSize = 10,
                        Foreground = Brushes.SteelBlue,
                        FontWeight = FontWeights.Bold
                    };
                    stpLbl.Children.Add(lblUltimoPrecio);

                    controlDecimal ctrDecimal = new controlDecimal
                    {
                        Tag = rbn,
                        Width = 60,
                        FontSize = 10,
                        valor = item.costo,
                        Foreground = Brushes.SteelBlue,
                        FontWeight = FontWeights.Bold,
                        IsEnabled = false
                    };
                    ctrDecimal.txtDecimal.Tag = chcPrincipal;
                    ctrDecimal.txtDecimal.TextChanged += Txt_TextChanged;
                    stpLbl.Children.Add(ctrDecimal);

                    controlEnteros ctrDiasEntrega = new controlEnteros
                    {
                        Tag = rbn,
                        Name = "ctrDiasEntrega",
                        Width = 60,
                        FontSize = 10,
                        IsEnabled = false,
                        valor = item.diasEntrega,
                        Foreground = Brushes.SteelBlue,
                        FontWeight = FontWeights.Bold
                    };
                    ctrDiasEntrega.txtEntero.Tag = chcPrincipal;
                    ctrDiasEntrega.txtEntero.TextChanged += Txt_TextChanged;
                    stpLbl.Children.Add(ctrDiasEntrega);

                    controlEnteros ctrDiasCredito = new controlEnteros
                    {
                        Tag = rbn,
                        Name = "ctrDiasCredito",
                        Width = 60,
                        FontSize = 10,
                        valor = item.diasCredito,
                        IsEnabled = false,
                        Foreground = Brushes.SteelBlue,
                        FontWeight = FontWeights.Bold
                    };
                    ctrDiasCredito.txtEntero.Tag = chcPrincipal;
                    ctrDiasCredito.txtEntero.TextChanged += Txt_TextChanged;
                    stpLbl.Children.Add(ctrDiasCredito);

                    rbn.Content = stpLbl;
                    stpSecundario.Children.Add(rbn);
                }

                stpProveedores.Children.Add(stpSecundario);
                stpContent.Children.Add(stpProveedores);

                //Label lblProveedor = new Label
                //{
                //    Width = 510,
                //    //Content = detalle.proveedor.razonSocial,
                //    FontWeight = FontWeights.Medium,
                //    HorizontalContentAlignment = HorizontalAlignment.Left,
                //    VerticalContentAlignment = VerticalAlignment.Center,
                //};
                //stpContent.Children.Add(lblProveedor);
            }

            Label lblCostoUnitario = new Label
            {
                Width = 120,
                Content = detalle.costo,
                Name = "lblCostoUnitario",
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.Blue,
                FontWeight = FontWeights.Bold
            };
            stpContent.Children.Add(lblCostoUnitario);

            Label lblImporte = new Label
            {
                Width = 150,
                Content = detalle.importe,
                Name = "lblImporte",
                VerticalAlignment = VerticalAlignment.Center,
                HorizontalAlignment = HorizontalAlignment.Center,
                HorizontalContentAlignment = HorizontalAlignment.Center,
                Foreground = Brushes.Blue,
                FontWeight = FontWeights.Bold
            };
            stpContent.Children.Add(lblImporte);

            chcPrincipal.Content = stpContent;
            if (detalle.idDetalleCotizacionSolicitudOrdCompra != 0)
            {
                //chcPrincipal.IsEnabled = false;
                chcPrincipal.IsChecked = true;
            }
            stpDetallesInsumos.Children.Add(chcPrincipal);
        }

        private void CbxMarcas_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                ComboBox comboMarcas = (ComboBox)sender;
                if (comboMarcas.SelectedItem == null)
                {
                    return;
                }

                LLantasProducto proLlanta = (LLantasProducto)comboMarcas.SelectedItem;
                CheckBox chcPrincipal = (CheckBox)comboMarcas.Tag;
                DetalleCotizacionSolicitudOrdCompra detalle = (DetalleCotizacionSolicitudOrdCompra)chcPrincipal.Tag;
                detalle.llantaProducto = proLlanta;
                StackPanel stpContent = (StackPanel)chcPrincipal.Content;

                foreach (object obj in stpContent.Children.Cast<object>().ToList())
                {
                    if (obj is StackPanel)
                    {
                        StackPanel stpProveedores = (StackPanel)obj;
                        if (stpProveedores.Name == "stpProveedores")
                        {
                            stpProveedores.Children.Clear();
                            if (detalle.idDetalleCotizacionSolicitudOrdCompra == 0)
                            {
                                var resp = new ProveedorSvc().getAllProveedoresBiIdMarca(proLlanta.marca.clave);

                                if (resp.typeResult == ResultTypes.success)
                                {
                                    ComboBox cbxProveedores = new ComboBox
                                    {
                                        Tag = chcPrincipal,
                                        Width = 505,
                                        Height = 22,
                                        ItemsSource = (List<Proveedor>)resp.result,
                                        DisplayMemberPath = "razonSocial",
                                        FontSize = 11,
                                        VerticalAlignment = VerticalAlignment.Center,
                                        Margin = new Thickness(2)
                                    };

                                    cbxProveedores.SelectionChanged += CbxProveedores_SelectionChanged;
                                    stpProveedores.Children.Add(cbxProveedores);
                                    StackPanel stpRbn = new StackPanel
                                    {
                                        Orientation = Orientation.Vertical,
                                        Name = "stpRbn",
                                        VerticalAlignment = VerticalAlignment.Center,
                                        HorizontalAlignment = HorizontalAlignment.Left
                                    };
                                    stpProveedores.Children.Add(stpRbn);
                                }
                                else
                                {
                                    Label lblProveedor = new Label
                                    {
                                        Content = "NO EXISTE PROVEEDOR ASIGNADO PARA ESTA MARCA",
                                        Width = 504,
                                        FontWeight = FontWeights.Bold,
                                        Foreground = Brushes.Red
                                    };
                                    stpProveedores.Children.Add(lblProveedor);
                                    //chcPrincipal.IsEnabled = false;
                                }
                                //stpContent.Children.Add(stpProveedores);
                            }
                            else
                            {
                                Label lblProveedor = new Label
                                {
                                    Tag = chcPrincipal,
                                    Width = 400,
                                    //Content = detalle.proveedor.razonSocial,
                                    FontSize = 12,
                                    FontWeight = FontWeights.Bold,
                                    VerticalAlignment = VerticalAlignment.Center
                                };
                                stpProveedores.Children.Add(lblProveedor);
                            }
                        }
                    }
                    else if (obj is Label)
                    {
                        if (((Label)obj).Name == "lblCostoUnitario")
                        {
                            ((Label)obj).Content = 0;
                        }//obj
                        if (((Label)obj).Name == "lblImporte")
                        {
                            ((Label)obj).Content = 0;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CbxProveedores_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CheckBox chcPrincipal = (CheckBox)((ComboBox)sender).Tag;
            DetalleCotizacionSolicitudOrdCompra obj = chcPrincipal.Tag as DetalleCotizacionSolicitudOrdCompra;
            var stp = ((StackPanel)chcPrincipal.Content).Children;
            Proveedor proveedor = (Proveedor)((ComboBox)sender).SelectedItem;
            foreach (var item in stp.Cast<object>().ToList())
            {
                if (item is StackPanel)
                {
                    StackPanel stpPro = (StackPanel)item;
                    if (stpPro.Name == "stpProveedores")
                    {
                        foreach (object stpInterno in stpPro.Children.Cast<object>().ToList())
                        {
                            if (stpInterno is StackPanel)
                            {
                                StackPanel stpInterno2 = (StackPanel)stpInterno;
                                foreach (RadioButton rbrItem in stpInterno2.Children.Cast<RadioButton>().ToList())
                                {
                                    Proveedor pro = (Proveedor)rbrItem.DataContext;
                                    if (pro.idProveedor == proveedor.idProveedor)
                                    {
                                        return;
                                    }
                                }
                                DetalleCotizaciones detCot = new DetalleCotizaciones
                                {
                                    proveedor = proveedor,
                                    costo = 0,
                                    diasCredito = 0,
                                    diasEntrega = 0,
                                    elegido = false,
                                    idDetalle = 0,
                                    importe = 0                                    
                                };
                                RadioButton rbn = new RadioButton
                                {
                                    DataContext = proveedor,
                                    Width = 504,
                                    VerticalContentAlignment = VerticalAlignment.Center,
                                    Tag = chcPrincipal,
                                    HorizontalAlignment = HorizontalAlignment.Left,
                                    FontSize = 10
                                };
                                rbn.Checked += Rbn_Checked;
                                rbn.MouseDoubleClick += Rbn_MouseDoubleClick;
                                StackPanel stpLbl = new StackPanel { Orientation = Orientation.Horizontal, HorizontalAlignment = HorizontalAlignment.Left };
                                Label lblProveedor = new Label
                                {
                                    Content = proveedor.razonSocial,
                                    Width = 220,
                                    FontSize = 10,
                                    Foreground = Brushes.SteelBlue,
                                    FontWeight = FontWeights.Bold
                                };
                                stpLbl.Children.Add(lblProveedor);

                                Label lblUltimoPrecio = new Label
                                {
                                    Content = proveedor.ultimoCosto,
                                    Width = 60,
                                    FontSize = 10,
                                    Foreground = Brushes.SteelBlue,
                                    FontWeight = FontWeights.Bold
                                };
                                stpLbl.Children.Add(lblUltimoPrecio);

                                controlDecimal ctrDecimal = new controlDecimal
                                {
                                    Tag = rbn,
                                    Width = 60,
                                    FontSize = 10,
                                    valor = proveedor.ultimoCosto,
                                    Foreground = Brushes.SteelBlue,
                                    FontWeight = FontWeights.Bold
                                };
                                detCot.costo = proveedor.ultimoCosto;
                                ctrDecimal.txtDecimal.Tag = chcPrincipal;
                                ctrDecimal.txtDecimal.TextChanged += Txt_TextChanged;
                                stpLbl.Children.Add(ctrDecimal);

                                controlEnteros ctrDiasEntrega = new controlEnteros
                                {
                                    Tag = rbn,
                                    Name = "ctrDiasEntrega",
                                    Width = 60,
                                    FontSize = 10,
                                    valor = 0,
                                    Foreground = Brushes.SteelBlue,
                                    FontWeight = FontWeights.Bold
                                };
                                ctrDiasEntrega.txtEntero.Tag = chcPrincipal;
                                ctrDiasEntrega.txtEntero.TextChanged += Txt_TextChanged;
                                stpLbl.Children.Add(ctrDiasEntrega);

                                controlEnteros ctrDiasCredito = new controlEnteros
                                {
                                    Tag = rbn,
                                    Name = "ctrDiasCredito",
                                    Width = 60,
                                    FontSize = 10,
                                    valor = 0,
                                    Foreground = Brushes.SteelBlue,
                                    FontWeight = FontWeights.Bold
                                };
                                ctrDiasCredito.txtEntero.Tag = chcPrincipal;
                                ctrDiasCredito.txtEntero.TextChanged += Txt_TextChanged;
                                stpLbl.Children.Add(ctrDiasCredito);

                                obj.listDetalles.Add(detCot);
                                rbn.Content = stpLbl;
                                stpInterno2.Children.Add(rbn);
                            }
                        }
                    }
                }
            }
        }

        private void Txt_TextChanged(object sender, TextChangedEventArgs e)
       {
            CheckBox chcPrincipal = (CheckBox)((TextBox)sender).Tag;
            var stp = ((StackPanel)chcPrincipal.Content).Children;
            Proveedor proveedor = (Proveedor)((TextBox)sender).DataContext;
            buscarCambiarCostoUnitarioDetalle(chcPrincipal, proveedor);
        }

        private void Rbn_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            CheckBox chcPrincipal = (CheckBox)((RadioButton)sender).Tag;
            var stp = ((StackPanel)chcPrincipal.Content).Children;
            Proveedor proveedor = (Proveedor)((RadioButton)sender).DataContext;
            buscarCambiarCostoUnitario(chcPrincipal, proveedor);
        }

        private void buscarCambiarCostoUnitarioDetalle(CheckBox chcControl, Proveedor proveedor)
        {
            DetalleCotizacionSolicitudOrdCompra detalle = (DetalleCotizacionSolicitudOrdCompra)chcControl.Tag;
            foreach (var det in detalle.listDetalles)
            {
                if (det.proveedor.idProveedor == proveedor.idProveedor)
                {
                    var stp = ((StackPanel)chcControl.Content).Children;
                    decimal costoUnitario = 0;
                    decimal cantitdad = 0;
                    foreach (var item in stp.Cast<object>().ToList())
                    {
                        if (item is StackPanel)
                        {
                            StackPanel stpPro = (StackPanel)item;
                            if (stpPro.Name == "stpProveedores")
                            {
                                foreach (object stpInterno in stpPro.Children.Cast<object>().ToList())
                                {
                                    if (stpInterno is StackPanel)
                                    {
                                        StackPanel stpInterno2 = (StackPanel)stpInterno;
                                        foreach (RadioButton rbrItem in stpInterno2.Children.Cast<RadioButton>().ToList())
                                        {
                                            Proveedor pro = (Proveedor)rbrItem.DataContext;
                                            det.elegido = rbrItem.IsChecked.Value;
                                            if (pro.idProveedor == proveedor.idProveedor)
                                            {
                                                StackPanel stpContet = (StackPanel)rbrItem.Content;
                                                foreach (var obj in stpContet.Children.Cast<object>().ToList())
                                                {
                                                    if (obj is controlDecimal)
                                                    {
                                                       costoUnitario = ((controlDecimal)obj).valor;
                                                        det.costo = costoUnitario;
                                                        if (det.elegido)
                                                        {
                                                            buscarCambiarCostoUnitario(chcControl, proveedor);
                                                        }                                                                                                          
                                                    }
                                                    else if (obj is controlEnteros)
                                                    {
                                                        controlEnteros ctr = obj as controlEnteros;
                                                        if (ctr.Name == "ctrDiasEntrega")
                                                        {
                                                            det.diasEntrega = ctr.valor;
                                                        }
                                                        if (ctr.Name == "ctrDiasCredito")
                                                        {
                                                            det.diasCredito = ctr.valor;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (item is Label)
                        {
                            if (((Label)item).Name == "lblCantidad")
                            {
                                cantitdad = Convert.ToDecimal(((Label)item).Content);
                            }
                            //if (((Label)item).Name == "lblCostoUnitario")
                            //{
                            //    ((Label)item).Content = costoUnitario;
                            //    det.costo = costoUnitario;
                            //}//lblImporte
                            if (((Label)item).Name == "lblImporte")
                            {
                                //((Label)item).Content = costoUnitario * cantitdad;
                                //itemDet.importe = costoUnitario * cantitdad;
                                det.importe = costoUnitario * cantitdad;
                            }
                        }
                    }
                }
                else
                {
                    det.elegido = false;
                }
            }
        }

        private void Rbn_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox chcPrincipal = (CheckBox)((RadioButton)sender).Tag;
            var stp = ((StackPanel)chcPrincipal.Content).Children;
            Proveedor proveedor = (Proveedor)((RadioButton)sender).DataContext;
            buscarCambiarCostoUnitario(chcPrincipal, proveedor);
        }

        void buscarCambiarCostoUnitario(CheckBox chcControl, Proveedor proveedor)
        {
            DetalleCotizacionSolicitudOrdCompra detalle = (DetalleCotizacionSolicitudOrdCompra)chcControl.Tag;
            foreach (var itemDet in detalle.listDetalles)
            {
                if (itemDet.proveedor.idProveedor == proveedor.idProveedor)
                {
                    itemDet.elegido = true;
                    detalle.proveedor = proveedor;
                    detalle.costo = itemDet.costo;
                    detalle.importe = itemDet.importe;
                    var stp = ((StackPanel)chcControl.Content).Children;
                    decimal cantitdad = 0;
                    foreach (var item in stp.Cast<object>().ToList())
                    {
                        if (item is Label)
                        {
                            if (((Label)item).Name == "lblCantidad")
                            {
                                cantitdad = Convert.ToDecimal(((Label)item).Content);
                            }
                            if (((Label)item).Name == "lblCostoUnitario")
                            {
                                ((Label)item).Content = itemDet.costo;
                            }
                            if (((Label)item).Name == "lblImporte")
                            {
                                ((Label)item).Content = itemDet.importe;
                            }
                        }
                    }
                }
                else
                {
                    itemDet.elegido = false;
                }
            }

            return;

            foreach (var itemDet in detalle.listDetalles)
            {
                itemDet.proveedor = proveedor;
                var stp = ((StackPanel)chcControl.Content).Children;
                decimal costoUnitario = 0;
                decimal cantitdad = 0;
                foreach (var item in stp.Cast<object>().ToList())
                {
                    if (item is StackPanel)
                    {
                        StackPanel stpPro = (StackPanel)item;
                        if (stpPro.Name == "stpProveedores")
                        {
                            foreach (object stpInterno in stpPro.Children.Cast<object>().ToList())
                            {
                                if (stpInterno is StackPanel)
                                {
                                    StackPanel stpInterno2 = (StackPanel)stpInterno;
                                    foreach (RadioButton rbrItem in stpInterno2.Children.Cast<RadioButton>().ToList())
                                    {
                                        Proveedor pro = (Proveedor)rbrItem.DataContext;
                                        if (pro.idProveedor == proveedor.idProveedor)
                                        {
                                            StackPanel stpContet = (StackPanel)rbrItem.Content;
                                            foreach (var obj in stpContet.Children.Cast<object>().ToList())
                                            {
                                                if (obj is controlDecimal)
                                                {
                                                    costoUnitario = ((controlDecimal)obj).valor;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (item is Label)
                    {
                        if (((Label)item).Name == "lblCantidad")
                        {
                            cantitdad = Convert.ToDecimal(((Label)item).Content);
                        }
                        if (((Label)item).Name == "lblCostoUnitario")
                        {
                            ((Label)item).Content = costoUnitario;
                            itemDet.costo = costoUnitario;
                        }//lblImporte
                        if (((Label)item).Name == "lblImporte")
                        {
                            ((Label)item).Content = costoUnitario * cantitdad;
                            itemDet.importe = costoUnitario * cantitdad;
                        }
                    }
                }
            }

            
        }

        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                CotizacionSolicitudOrdCompra cotizacion = mapForm();
                var resp = new CotizacionesSvc().saveCotizacion(ref cotizacion);
                if (resp.typeResult == ResultTypes.success)
                {
                    mainWindow.habilitar = true;
                    mapForm(cotizacion);
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapForm(CotizacionSolicitudOrdCompra cotizacion, bool bandera = false)
        {
            lvlOrdenesCompra.ItemsSource = null;
            if (cotizacion != null)
            {
                txtFolioCotizacion.Text = cotizacion.idCotizacionSolicitudOrdCompra.ToString();
                txtFolioCotizacion.Tag = cotizacion;
                txtFolioCotizacion.IsEnabled = false;
                txtEstatusCotizacion.Text = cotizacion.estatus;

                if (!bandera)
                {
                    buscarSolicitudOrden(cotizacion.solicitud.idSolicitudOrdenCompra);
                }

                stpDetalles.Children.Clear();
                stpDetallesInsumos.Children.Clear();
                foreach (var item in cotizacion.listDetalle)
                {
                    if (cotizacion.isLlantas)
                    {
                        tapControl.SelectedIndex = 0;
                        llenarLista(item);
                    }
                    else
                    {
                        tapControl.SelectedIndex = 1;
                        llenarListaInsumos(item);
                    }

                }
                mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                //MessageBox.Show(cotizacion.estatus);
                OperationResult respPrivilegio = new PrivilegioSvc().consultarPrivilegio("APROVAR_COTIZACION_COMPRA", mainWindow.inicio._usuario.idUsuario);

                if (cotizacion.estatus == "COTIZADO")
                {
                    if (respPrivilegio.typeResult == ResultTypes.success)
                    {
                        btnAutorizar.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        btnAutorizar.Visibility = Visibility.Hidden;
                    }
                }
                else
                {
                    btnAutorizar.Visibility = Visibility.Hidden;
                }

                var respOr = new OrdenCompraSvc().getOrdenCompraByIdCotizacion(cotizacion.idCotizacionSolicitudOrdCompra);
                if (respOr.typeResult == ResultTypes.success)
                {
                    lvlOrdenesCompra.ItemsSource = (List<OrdenCompra>)respOr.result;
                }
            }
            else
            {
                txtFolioCotizacion.Clear();
                txtFolioCotizacion.Tag = null;
                txtFolioCotizacion.IsEnabled = true;
                txtEstatusCotizacion.Text = "NUEVO";
                mapFormSolicitud(null);
                mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
                btnAutorizar.Visibility = Visibility.Hidden;
                stpDetalles.Children.Clear();
                stpDetallesInsumos.Children.Clear();
            }
        }

        private CotizacionSolicitudOrdCompra mapForm()
        {
            try
            {
                CotizacionSolicitudOrdCompra cotizacion = new CotizacionSolicitudOrdCompra
                {
                    idCotizacionSolicitudOrdCompra = 0,
                    solicitud = (SolicitudOrdenCompra)txtFolioSolicitud.Tag,
                    empresa = cbxEmpresa.empresaSelected,
                    fecha = DateTime.Now,
                    fechaAutorizacion = null,
                    usuarioCotiza = mainWindow.inicio._usuario.nombreUsuario,
                    usuarioAprueba = "",
                    estatus = txtEstatusCotizacion.Text == "NUEVO" ? "COTIZADO" : "APROVADO",
                    isLlantas = ((SolicitudOrdenCompra)txtFolioSolicitud.Tag).isLlantas
                };

                cotizacion.listDetalle = new List<DetalleCotizacionSolicitudOrdCompra>();
                OperationResult resp = getListaDetalles(cotizacion.isLlantas);
                if (resp.typeResult != ResultTypes.success)
                {
                    ImprimirMensaje.imprimir(resp);
                    return null;
                }
                cotizacion.listDetalle = (List<DetalleCotizacionSolicitudOrdCompra>)resp.result;
                return cotizacion;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(new OperationResult { valor = 1, mensaje = ex.Message });
                return null;
            }
        }

        private OperationResult getListaDetalles(bool isLlantas = true)
        {
            try
            {
                List<DetalleCotizacionSolicitudOrdCompra> list = new List<DetalleCotizacionSolicitudOrdCompra>();
                OperationResult resp = new OperationResult { valor = 0, mensaje = "CORREGIR LOS SIGUIENTES ERRORES PARA CONTINUAR:" + Environment.NewLine, result = list };
                List<CheckBox> listChc = new List<CheckBox>();
                if (isLlantas)
                {
                    listChc = stpDetalles.Children.Cast<CheckBox>().ToList();
                }
                else
                {
                    listChc = stpDetallesInsumos.Children.Cast<CheckBox>().ToList();
                }
                foreach (CheckBox chcElemento in listChc)
                {
                    if (chcElemento.IsChecked.Value)
                    {
                        DetalleCotizacionSolicitudOrdCompra detalle = (DetalleCotizacionSolicitudOrdCompra)chcElemento.Tag;
                        if (detalle.proveedor == null)
                        {
                            resp.valor = 2;
                            resp.mensaje += "   -* " + detalle.llantaProducto.marca.descripcion +
                                " " + detalle.llantaProducto.diseño.descripcion + " "
                                    + detalle.llantaProducto.medida + " no tiene asignado Proveedor." + Environment.NewLine;
                        }
                        if (detalle.costo <= 0)
                        {
                            resp.valor = 2;
                            resp.mensaje += "   -* " + detalle.llantaProducto.marca.descripcion +
                                " " + detalle.llantaProducto.diseño.descripcion + " "
                                    + detalle.llantaProducto.medida + " su costo unitario tiene que ser mayor 0." + Environment.NewLine;
                        }
                        list.Add(detalle);
                    }
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
        }

        private OperationResult getListaDetallesInsumos()
        {
            throw new NotImplementedException();
        }

        private void txtFolioCotizacion_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (!string.IsNullOrEmpty(txtFolioCotizacion.Text.Trim()))
                {
                    int i = 0;
                    if (int.TryParse(txtFolioCotizacion.Text.Trim(), out i))
                    {
                        buscarCotizacion(i);
                    }
                }
            }
        }

        private void buscarCotizacion(int i)
        {
            try
            {
                //Cursor = Cursors.Wait;
                var resp = new CotizacionesSvc().getCotizacionAllOrById(i);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm((CotizacionSolicitudOrdCompra)resp.result);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                //Cursor = Cursors.Arrow;
            }
        }

        private void btnAutorizar_Click(object sender, RoutedEventArgs e)
        {
            btnAutorizar.Visibility = Visibility.Hidden;
            CotizacionSolicitudOrdCompra cotizacion = (CotizacionSolicitudOrdCompra)txtFolioCotizacion.Tag;
            List<OrdenCompra> listaOrdenCom = new List<OrdenCompra>();
            foreach (var detalle in cotizacion.listDetalle)
            {
                bool existe = false;
                foreach (OrdenCompra ord in listaOrdenCom)
                {

                    if (ord.proveedor.idProveedor == detalle.proveedor.idProveedor)
                    {
                        ord.listaDetalles.Add(new DetalleOrdenCompra
                        {
                            idDetalleCompra = 0,
                            cantidad = detalle.cantidad,
                            costo = detalle.costo,
                            importe = detalle.importe,
                            factura = "",
                            llantaProducto = detalle.llantaProducto,
                            producto = detalle.producto,
                            pendientes = detalle.cantidad,
                            unidadTransporte = detalle.unidadTransporte
                        });
                        existe = true;
                        break;
                    }
                }
                if (existe)
                {
                    continue;
                }

                OrdenCompra orden = new OrdenCompra()
                {
                    idOrdenCompra = 0,
                    idCotizacion = ((CotizacionSolicitudOrdCompra)txtFolioCotizacion.Tag).idCotizacionSolicitudOrdCompra,
                    empresa = cotizacion.empresa,
                    estatus = "ACT",
                    fecha = DateTime.Now,
                    CIDDOCUMENTO = 0,
                    folioFactura = "",
                    proveedor = detalle.proveedor,
                    listaDetalles = new List<DetalleOrdenCompra>(),
                    isLLanta = cotizacion.isLlantas
                };
                orden.listaDetalles.Add(new DetalleOrdenCompra
                {
                    idDetalleCompra = 0,
                    cantidad = detalle.cantidad,
                    costo = detalle.costo,
                    importe = detalle.importe,
                    factura = "",
                    llantaProducto = detalle.llantaProducto,
                    producto = detalle.producto,
                    pendientes = detalle.cantidad,
                    unidadTransporte = detalle.unidadTransporte
                });
                listaOrdenCom.Add(orden);
                continue;
            }

            ProcesosForm proceso = new ProcesosForm();
            var respNew = proceso.realizarOrdenesCompra(listaOrdenCom);
            proceso.Dispose();

            
            if (respNew.typeResult == ResultTypes.success)
            {
                var coti = (CotizacionSolicitudOrdCompra)txtFolioCotizacion.Tag;
                buscarCotizacion(coti.idCotizacionSolicitudOrdCompra);
                //MessageBox.Show("");
            }
            else
            {
                btnAutorizar.Visibility = Visibility.Visible;
            }
            ImprimirMensaje.imprimir(respNew);
        }
        //crearOrdenCompra classOrden = null;
        private OperationResult guardarOrdenes(List<OrdenCompra> listaOrdenCom)
        {

            //ReportView view2 = new ReportView();
            //view2.imprimirOrdenCompra(listaOrdenCom, (CotizacionSolicitudOrdCompra)txtFolioCotizacion.Tag, new Util().getRutasImpresora("VIAJES_ATLANTE"));
            //return new OperationResult { valor = 2, mensaje = "probando imprecion" };
            bool resp = false;
            crearOrdenCompra classOrden = new crearOrdenCompra(ref resp);
            if (!resp)
            {
                return new OperationResult { valor = 2, mensaje = "ERROR AL INICIAR LA INSTANCIA DEL COMERCIAL" };
            }
            try
            {
                Cursor = Cursors.Wait;
                var guardo = classOrden.generarOrdenCompra(listaOrdenCom);
                //classOrden.cerrar();
                if (guardo)
                {
                    var resp2 = new OrdenCompraSvc().saveOrdenCompraLlantas(classOrden.newListOrdenCompra);
                    if (resp2.typeResult == ResultTypes.success)
                    {
                        List<OrdenCompra> listaNueva = (List<OrdenCompra>)resp2.result;
                        lvlOrdenesCompra.ItemsSource = listaNueva;
                        //ReportView view = new ReportView();
                        //view.imprimirOrdenCompra(listaNueva, (CotizacionSolicitudOrdCompra)txtFolioCotizacion.Tag, new Util().getRutasImpresora("VIAJES_ATLANTE"));
                    }
                    return resp2;
                }
                else
                {
                    return new OperationResult { valor = 1, mensaje = "ERROR AL GUARDAR LA OPERACIÓN EN EL COMERCIAL" };
                }
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void btnBuscarSolicitud_Click(object sender, RoutedEventArgs e)
        {
            SolicitudOrdenCompra solicitud = new selectSolicitudOrdenCompra().buscarTodasLasSolicitudes();
            if (solicitud != null)
            {
                mapFormSolicitud(solicitud, true);
            }

        }

        private void btnBuscarCotizacion_Click(object sender, RoutedEventArgs e)
        {
            var coti = new selectCotizacionesOrdenCompra().getCotizacion();
            if (coti!= null)
            {
                buscarCotizacion(coti.idCotizacionSolicitudOrdCompra);
            }
        }
    }
}
