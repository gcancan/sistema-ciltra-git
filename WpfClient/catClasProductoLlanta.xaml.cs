﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para catClasProductoLlanta.xaml
    /// </summary>
    public partial class catClasProductoLlanta : ViewBase
    {
        public catClasProductoLlanta()
        {
            InitializeComponent();
        }
        public override void buscar()
        {
            base.buscar();
            LLantasProducto lLantasProducto = new selectProductoLlanta().GetLLantasProducto();
            if (lLantasProducto != null)
            {
                base.buscar();
                this.mapForm(lLantasProducto);
                base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.buscar | ToolbarCommands.editar | ToolbarCommands.nuevo);
            }
        }

        private void cbxDiseño_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.cbxDiseño.SelectedItem != null)
            {
                Diseño selectedItem = this.cbxDiseño.SelectedItem as Diseño;
                this.txtDiseño.Text = selectedItem.descripcion;
                this.txtDiseñoMedida.Text = (selectedItem.medida == null) ? string.Empty : selectedItem.medida.medida;
                this.txtDiseñoProfunidad.Text = selectedItem.profundidad.ToString();
                this.txtDiseñoTipoLlanta.Text = (selectedItem.tipoLlanta == null) ? string.Empty : selectedItem.tipoLlanta.nombre;
            }
            else
            {
                this.txtDiseño.Clear();
                this.txtDiseñoMedida.Clear();
                this.txtDiseñoProfunidad.Clear();
                this.txtDiseñoTipoLlanta.Clear();
            }
        }

        public override OperationResult guardar()
        {
            try
            {
                LLantasProducto lLantasProducto = this.mapForm();
                if (lLantasProducto != null)
                {
                    OperationResult result = new LlantaSvc().saveProductoLlanta(ref lLantasProducto);
                    this.mapForm(lLantasProducto);
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                    return result;
                }
                return new OperationResult(ResultTypes.warning, "OCURRIO UN ERROR AL LEER LA INFORMACI\x00d3N DE LA PANTALLA", null);
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }
        public LLantasProducto mapForm()
        {
            try
            {
                return new LLantasProducto
                {
                    idLLantasProducto = (this.ctrClave.Tag == null) ? 0 : (this.ctrClave.Tag as LLantasProducto).idLLantasProducto,
                    diseño = this.cbxDiseño.SelectedItem as Diseño,
                    medida = (this.cbxDiseño.SelectedItem as Diseño).medida.medida,
                    marca = this.cbxMarcas.SelectedItem as Marca,
                    activo = true,
                    profundidad = (this.cbxDiseño.SelectedItem as Diseño).profundidad
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void mapForm(LLantasProducto lLantasProducto)
        {
            if (lLantasProducto != null)
            {
                this.ctrClave.Tag = lLantasProducto;
                this.ctrClave.valor = lLantasProducto.idLLantasProducto;
                this.cbxMarcas.SelectedItem = (this.cbxMarcas.ItemsSource as List<Marca>).Find(s => s.clave == lLantasProducto.marca.clave);
                this.cbxDiseño.SelectedItem = (this.cbxDiseño.ItemsSource as List<Diseño>).Find(s => s.idDiseño == lLantasProducto.diseño.idDiseño);
            }
            else
            {
                this.ctrClave.Tag = null;
                this.ctrClave.limpiar();
                this.cbxMarcas.SelectedItem = null;
                this.cbxDiseño.SelectedItem = null;
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            this.mapForm(null);
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            OperationResult resp = new MarcasSvc().getMarcasLlantas();
            if (resp.typeResult == ResultTypes.success)
            {
                this.cbxMarcas.ItemsSource = resp.result as List<Marca>;
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
                base.IsEnabled = false;
                return;
            }
            OperationResult result2 = new DiseñosLlantaSvc().getDiseñosLlanta();
            if (result2.typeResult == ResultTypes.success)
            {
                List<Diseño> listaDiseños = result2.result as List<Diseño>;
                this.cbxDiseño.ItemsSource = listaDiseños.OrderBy(s=> s.nombreMostrar);
            }
            else
            {
                ImprimirMensaje.imprimir(result2);
                base.IsEnabled = false;
                return;
            }
            this.nuevo();
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult resp = new LlantaSvc().getAllLLantasProducto();
                if (resp.typeResult == ResultTypes.success)
                {
                    List<LLantasProducto> lista = resp.result as List<LLantasProducto>;
                    new ReportView().exportarListaLLantasProducto(lista);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow; 
            }
        }
    }
}
