﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoreFletera.Interfaces;
using Core.Interfaces;
using Core.Models;
using CoreFletera.Models;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for editInspecciones.xaml
    /// </summary>
    public partial class editInspecciones : ViewBase
    {
        public editInspecciones()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;

                OperationResult tiposUnidad = new TipoUnidadSvc().getGruposTiposUnidad();
                if (tiposUnidad.typeResult == ResultTypes.success)
                {
                    cbxTipoUnidad.ItemsSource = (List<GrupoTipoUnidades>)tiposUnidad.result;
                }
                else
                {
                    ImprimirMensaje.imprimir(tiposUnidad);
                    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    return;
                }
                OperationResult tipoOrden = new OperacionSvc().getTiposOrdenServicio();
                if (tipoOrden.typeResult == ResultTypes.success)
                {
                    cbxTipoOrdenServicio.ItemsSource = (List<TiposDeOrdenServicio>)tipoOrden.result;
                }
                else
                {
                    ImprimirMensaje.imprimir(tipoOrden);
                    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    return;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                return;
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
            nuevo();

        }
        public override void nuevo()
        {
            base.nuevo();
            cbxTipoUnidad.SelectedItem = null;
            mapForm(null);
        }

        private void txtInsumo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (cbxTipoOrdenServicio.SelectedItem == null)
                {
                    ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "ELIJA UN TIPO DE ORDEN" });
                    cbxTipoOrdenServicio.IsDropDownOpen = true;
                    return;
                }
                selectInsumo sel = new selectInsumo(txtInsumo.Text.Trim());
                string comboSelect = ((TiposDeOrdenServicio)cbxTipoOrdenServicio.SelectedItem).NomTipOrdServ;
                var insumoVal = sel.buscarTodos(comboSelect == "TALLER", comboSelect == "LAVADERO", comboSelect == "LLANTAS");
                addListaInsumos(insumoVal);
            }
        }

        private void addListaInsumos(ProductoInsumo insumoVal)
        {
            if (insumoVal != null)
            {

                StackPanel stpMayor = new StackPanel { Orientation = Orientation.Vertical };
                StackPanel stp = new StackPanel() { VerticalAlignment = VerticalAlignment.Top, HorizontalAlignment = HorizontalAlignment.Left };
                stp.Orientation = Orientation.Horizontal;

                stp.Children.Add(new Label() { Content = insumoVal.nombre, Width = 300, Height = 22, Margin = new Thickness(2, 2, 2, 2) });

                controlDecimal txt = new controlDecimal() { valor = insumoVal.cMax };
                //txt.KeyDown += Txt_KeyDown;
                stp.Children.Add(txt);

                Button boton = new Button { Content = "Quitar", Tag = insumoVal, Height = 24, Width = 60, Foreground = Brushes.White, Background = Brushes.Red, Margin = new Thickness(2, 2, 2, 2) };
                boton.Click += Boton_Click;
                stp.Children.Add(boton);

                stp.Tag = insumoVal;
                if (scrollContenedor.Content == null)
                {
                    scrollContenedor.Content = stpMayor;
                }
                StackPanel controles = (StackPanel)scrollContenedor.Content;
                List<StackPanel> listControl = controles.Children.Cast<StackPanel>().ToList();
                foreach (StackPanel item in listControl)
                {
                    ProductoInsumo tag = (ProductoInsumo)item.Tag;
                    if (insumoVal.idInsumo == tag.idInsumo)
                    {
                        return;
                    }
                }
                controles.Children.Add(stp);
                scrollContenedor.Content = controles;
            }
        }

        private void Txt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key < Key.D0 || e.Key > Key.D9)
            {
                if (e.Key < Key.NumPad0 || e.Key > Key.NumPad9)
                {
                    if (e.Key == Key.OemPeriod || e.Key == Key.Decimal)
                    {
                        if (((TextBox)sender).Text.Contains("."))
                        {
                            e.Handled = true;
                        }
                    }
                    else if (e.Key == Key.Tab)
                    {
                        //e.Handled = false;
                    }
                    else
                    {
                        e.Handled = true;
                    }                    
                }
            }          
        }

        private void Boton_Click(object sender, RoutedEventArgs e)
        {
            var insumo = ((ProductoInsumo)((Button)sender).Tag);

            StackPanel controles = (StackPanel)scrollContenedor.Content;
            List<StackPanel> listControl = controles.Children.Cast<StackPanel>().ToList();
            int i = 0;
            foreach (StackPanel item in listControl)
            {
                ProductoInsumo tag = (ProductoInsumo)item.Tag;
                if (insumo.idInsumo == tag.idInsumo)
                {
                    break;
                }
                i++;
            }
            listControl.Remove(listControl[i]);
            controles.Children.Clear();
            foreach (var item in listControl)
            {
                controles.Children.Add(item);
            }
            scrollContenedor.Content = controles;
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                Inspecciones inspecciones = mapForm();
                var resp =  new InspeccionesSvc().saveInspecciones(ref inspecciones);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(inspecciones);
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.editar| ToolbarCommands.cerrar);
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult { valor = 1, mensaje = ex.Message };
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapForm(Inspecciones inspecciones)
        {
            try
            {
                if (inspecciones != null)
                {
                    txtClave.Tag = inspecciones;
                    txtClave.Text = inspecciones.idInspeccion.ToString();
                    txtClaveDiagrama.Text = inspecciones.claveDiagrama;
                    txtNombreInspeccion.Text = inspecciones.nombreInspeccion;
                    int i = 0;
                    foreach (TiposDeOrdenServicio item in cbxTipoOrdenServicio.Items)
                    {
                        if (item.idTipOrdServ == inspecciones.tipoOrdenServicio.idTipOrdServ)
                        {
                            cbxTipoOrdenServicio.SelectedIndex = i;
                            break;
                        }
                        i++;
                    }
                    i = 0;
                    foreach (GrupoTipoUnidades item in cbxTipoUnidad.Items)
                    {
                        if (item.grupo == inspecciones.grupo.grupo)
                        {
                            cbxTipoUnidad.SelectedIndex = i;
                            break;
                        }
                        i++;
                    }
                    txtDias.valor = inspecciones.dias;
                    foreach (var item in inspecciones.listaInsumos)
                    {
                        addListaInsumos(item);
                    }              
                }
                else
                {
                    txtClave.Tag = null;
                    txtClave.Clear();
                    txtNombreInspeccion.Clear();
                    cbxTipoOrdenServicio.SelectedItem = null;
                    cbxTipoUnidad.SelectedItem = null;
                    txtClaveDiagrama.Clear();
                    txtDias.valor = 0;
                    scrollContenedor.Content = null;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private Inspecciones mapForm()
        {
            try
            {
                Inspecciones inspecciones = new Inspecciones
                {
                    claveDiagrama = txtClaveDiagrama.Text,
                    idInspeccion = txtClave.Tag == null ? 0 : ((Inspecciones)txtClave.Tag).idInspeccion,
                    nombreInspeccion = txtNombreInspeccion.Text.Trim(),
                    estatus = true,
                    dias = txtDias.valor,
                    tipoOrdenServicio = (TiposDeOrdenServicio)cbxTipoOrdenServicio.SelectedItem,
                    grupo = (GrupoTipoUnidades)cbxTipoUnidad.SelectedItem
                };
                inspecciones.listaTipoUnidades = new List<GrupoTipoUnidades>();
                //foreach (GrupoTipoUnidades item in cbxTipoUnidad.SelectedItems)
                //{
                //    inspecciones.listaTipoUnidades.Add(item);
                //}
                inspecciones.listaTipoUnidades.Add((GrupoTipoUnidades)cbxTipoUnidad.SelectedItem);
                var listInsumos = getListaInsumos();
                if (listInsumos == null )
                {
                    return null;
                }
                inspecciones.listaInsumos = listInsumos;
                                
                return inspecciones;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private List<ProductoInsumo> getListaInsumos()
        {
            try
            {
                if (scrollContenedor.Content != null)
                {
                    List<ProductoInsumo> lista = new List<ProductoInsumo>();
                    StackPanel stpMayor = (StackPanel)scrollContenedor.Content;
                    List<StackPanel> listSpHijos = stpMayor.Children.Cast<StackPanel>().ToList();
                    foreach (StackPanel item in listSpHijos)
                    {
                        //lista.Add((ProductoInsumo)item.Tag);
                        List<Control> controles = item.Children.Cast<Control>().ToList();
                        foreach (Control ctrl in controles)
                        {
                            if (ctrl is controlDecimal)
                            {
                                ProductoInsumo producto = (ProductoInsumo)item.Tag;
                                producto.cMax = ((controlDecimal)ctrl).valor;
                                lista.Add(producto);
                            }
                        }
                    }
                    return lista;
                }
                else
                {
                    return new List<ProductoInsumo>();
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void cbxTipoUnidad_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string path = System.IO.Directory.GetCurrentDirectory();
            if (cbxTipoUnidad.SelectedItem != null)
            {
                string nombreDiagrama = string.Empty;
                switch (((GrupoTipoUnidades)cbxTipoUnidad.SelectedItem).grupo)
                {
                    case "CAJAS DE VOLTEO":
                        nombreDiagrama = "Volteo.png";
                        break;
                    case "CAJAS SECAS":
                        nombreDiagrama = "Seca.png";
                        break;
                    case "DOLLYS":
                        nombreDiagrama = "Dolly.png";
                        break;
                    case "HOOPERS":
                        nombreDiagrama = "Hopper.png";
                        break;
                    case "GONDOLAS":
                        nombreDiagrama = "Chichona.png";
                        break;
                    case "PLATAFORMAS":
                        nombreDiagrama = "Plana.png";
                        break;
                    case "TOLVAS":
                        nombreDiagrama = "Tolva.png";
                        break;
                    case "TRACTO CAMION":
                        nombreDiagrama = "Tractor.png";
                        break;
                    default:
                        break;
                }
                if (!string.IsNullOrEmpty(nombreDiagrama))
                {
                    BitmapImage ima = new BitmapImage(new Uri(path + "\\Diagramas\\" + nombreDiagrama));
                    imaDiagrama.Source = ima;
                }
                else
                {
                    imaDiagrama.Source = null;
                }                
            }
            else
            {
                imaDiagrama.Source = null;
            }
        }

        public override void buscar()
        {
            selectInspecciones select = new selectInspecciones();
            Inspecciones inspeccion = select.getInspeccion();
            if (inspeccion != null)
            {
                mapForm(inspeccion);
                base.buscar();
            }
            //base.buscar();
        }
    }
}
