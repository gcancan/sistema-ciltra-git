﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CoreFletera.Models;
using ctr = MahApps.Metro.Controls;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for controlUnidadesTransporte.xaml
    /// </summary>
    public partial class controlUnidadesTransporte : UserControl
    {
        etipoUniadBusqueda tipoBusqueda = etipoUniadBusqueda.TRACTOR;
        Empresa empresa = new Empresa();
        ZonaOperativa zonaOperativa = new ZonaOperativa();
        Cliente cliente = null;
        public controlUnidadesTransporte()
        {
            InitializeComponent();
        }
        public controlUnidadesTransporte(etipoUniadBusqueda tipoBusqueda, Empresa empresa)
        {
            InitializeComponent();
            this.tipoBusqueda = tipoBusqueda;
        }
        public string marcaAgua
        {
            set
            {
                ctr.TextBoxHelper.SetWatermark(txtUnidadTrans, value);
            }
        }
        private bool _validacion { get; set; }
        public bool validacion
        {
            set
            {
                _validacion = value;
            }
        }
        public void loaded(etipoUniadBusqueda tipoBusqueda, Empresa empresa, ZonaOperativa zonaOperativa = null, Cliente cliente = null)
        {
            this.tipoBusqueda = tipoBusqueda;
            this.empresa = empresa;
            this.zonaOperativa = zonaOperativa;
            this.cliente = cliente;
            limpiar();
        }
        public UnidadTransporte unidadTransporte
        {
            get
            {
                if (txtUnidadTrans.Tag == null)
                {
                    return null;
                }
                else
                {
                    return (UnidadTransporte)txtUnidadTrans.Tag;
                }
            }
            set
            {
                mapTxt(value);
            }
        }
        public void limpiar()
        {
            mapTxt(null);
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            txtUnidadTrans.Width = Width - 4;
        }

        private void txtUnidadTrans_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (e.Key == Key.Enter)
                {
                    string clave = txtUnidadTrans.Text.Trim();
                    if (!string.IsNullOrEmpty(clave))
                    {
                        List<UnidadTransporte> ListUnidadT = buscarUnidades(clave);
                        if (ListUnidadT.Count == 1)
                        {
                            mapTxt(ListUnidadT[0]);
                        }
                        else if (ListUnidadT.Count == 0)
                        {
                            selectUnidadTransporte buscardor = new selectUnidadTransporte(this.empresa);
                            UnidadTransporte s = null;
                            switch (tipoBusqueda)
                            {
                                case etipoUniadBusqueda.TRACTOR:
                                    s = buscardor.buscarTractores(zonaOperativa, cliente);
                                    break;
                                case etipoUniadBusqueda.TOLVA:
                                    s = buscardor.buscarTolvas(zonaOperativa, cliente);
                                    break;
                                case etipoUniadBusqueda.DOLLY:
                                    s = buscardor.buscarDollys(zonaOperativa, cliente);
                                    break;
                                case etipoUniadBusqueda.TODAS:
                                    s = buscardor.buscarUnidades();
                                    break;
                                default:
                                    break;
                            }
                            mapTxt(s);
                        }
                        else
                        {
                            selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                            mapTxt(buscardor.selectUnidadTransporteFind());
                        }
                    }
                    else
                    {
                        selectUnidadTransporte buscardor = new selectUnidadTransporte(this.empresa);
                        UnidadTransporte s = null;
                        switch (tipoBusqueda)
                        {
                            case etipoUniadBusqueda.TRACTOR:
                                s = buscardor.buscarTractores(zonaOperativa, cliente);
                                break;
                            case etipoUniadBusqueda.TOLVA:
                                s = buscardor.buscarTolvas(zonaOperativa, cliente);
                                break;
                            case etipoUniadBusqueda.DOLLY:
                                s = buscardor.buscarDollys(zonaOperativa, cliente);
                                break;
                            case etipoUniadBusqueda.TODAS:
                                s = buscardor.buscarUnidades();
                                break;
                            default:
                                break;
                        }
                        mapTxt(s);
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }   
            finally
            {
                Cursor = Cursors.Arrow;
            }         
        }

        private void mapTxt(UnidadTransporte unidadTransporte)
        {
            if (unidadTransporte != null)
            {
                txtUnidadTrans.Text = unidadTransporte.clave;
                txtUnidadTrans.Tag = unidadTransporte;
                txtUnidadTrans.IsEnabled = false;
                Background = _validacion ? Brushes.Green : null;
            }
            else
            {
                txtUnidadTrans.Clear();
                txtUnidadTrans.Tag = null;
                txtUnidadTrans.IsEnabled = true;
                Background = null;
            }
            DataContext = unidadTransporte;
        }

        private List<UnidadTransporte> buscarUnidades(string clave)
        {            
            OperationResult resp = new OperationResult();
            switch (this.tipoBusqueda)
            {
                case etipoUniadBusqueda.TRACTOR:
                    resp = new UnidadTransporteSvc().getTractores((empresa == null ? 0 : empresa.clave), clave, (zonaOperativa == null ? 0 : zonaOperativa.idZonaOperativa), (cliente == null ? 0 : cliente.clave));
                    break;
                case etipoUniadBusqueda.TOLVA:
                    resp = new UnidadTransporteSvc().getTolvas(empresa == null ? 0 : empresa.clave, clave, (zonaOperativa == null ? 0 : zonaOperativa.idZonaOperativa), (cliente == null ? 0 : cliente.clave));
                    break;
                case etipoUniadBusqueda.DOLLY:
                    resp = new UnidadTransporteSvc().getDollys(empresa == null ? 0 : empresa.clave, clave, (zonaOperativa == null ? 0 : zonaOperativa.idZonaOperativa), (cliente == null ? 0 : cliente.clave));
                    break;
                case etipoUniadBusqueda.TODAS:
                    resp = new UnidadTransporteSvc().getUnidadesALLorById(empresa == null ? 0 : empresa.clave, "%"+clave+"%");
                    break;
                default:
                    break;
            }
            
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    return (List<UnidadTransporte>)resp.result;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return null;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return null;
                case ResultTypes.recordNotFound:
                    return new List<UnidadTransporte>();
                default:
                    return null;
            }
        }

        private void UserControl_MouseLeave(object sender, MouseEventArgs e)
        {

        }
        
    }
}
