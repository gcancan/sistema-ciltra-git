﻿using Core.Models;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;

namespace WpfClient
{
    public class ViewBase : UserControl, IView
    {
        public ViewBase()
        {
            this.FontFamily = new System.Windows.Media.FontFamily("Tahoma");
            this.Opacity = .92;

            System.Windows.Media.LinearGradientBrush lineas = new System.Windows.Media.LinearGradientBrush();
            lineas.StartPoint = new Point(0.5, 1);
            lineas.EndPoint = new Point(0.5, 0);

            GradientStop gra = new GradientStop(Colors.White, 0);
            GradientStop gra1 = new GradientStop(Colors.White, 1);
            Color color = new Color();
            color = (Color)ColorConverter.ConvertFromString("#FFEEEEEE");
            GradientStop gra2 = new GradientStop(color, 0.346);
            Color color2 = new Color();
            color2 = (Color)ColorConverter.ConvertFromString("#FFEAEAEA");
            GradientStop gra3 = new GradientStop(color2, 0.681);


            lineas.GradientStops.Add(gra);
            lineas.GradientStops.Add(gra1);
            lineas.GradientStops.Add(gra2);
            lineas.GradientStops.Add(gra3);
            this.Background = lineas;
            //Background = Brushes.Azure;
            //this.Margin = new Thickness(4);
            this.BorderBrush = Brushes.SteelBlue;
            BorderThickness = new Thickness(1);
        }
        public MainWindow mainWindow { get; set; }

        public Boolean bandeja { get; set; }

        public virtual void nuevo()
        {
            mainWindow.enableToolBarCommands(
                ToolbarCommands.buscar |
                ToolbarCommands.guardar |
                ToolbarCommands.nuevo |
                ToolbarCommands.cerrar
            );

            IsEnabled = true;
        }        

        public virtual void cerrar()
        {

        }
        public virtual OperationResult guardar()
        {
            mainWindow.enableToolBarCommands(
                ToolbarCommands.eliminar |
                ToolbarCommands.editar |
                ToolbarCommands.nuevo |
                ToolbarCommands.cerrar);
            return new OperationResult { mensaje ="Se implemento codigo de guardado", valor = (int)ResultTypes.success};
        }

        public virtual OperationResult eliminar()
        {
            nuevo();
            return new OperationResult { mensaje = "Se implemento codigo para eliminar", valor = (int)ResultTypes.success };
        }

        public virtual void editar()
        {
            IsEnabled = true;
            mainWindow.enableToolBarCommands(
                //ToolbarCommands.buscar |              
                //ToolbarCommands.eliminar |
                ToolbarCommands.guardar |
                ToolbarCommands.nuevo |
                ToolbarCommands.cerrar
            );
            mainWindow.scrollContainer.IsEnabled = true;
        }

        public virtual void buscar()
        {
            mainWindow.enableToolBarCommands(
                ToolbarCommands.eliminar |
                ToolbarCommands.editar |
                ToolbarCommands.nuevo |
                ToolbarCommands.cerrar);
         mainWindow.scrollContainer.IsEnabled = false;
        }

             
        protected delegate T ProcessDelegate<out T>(object[] arguments);
        protected delegate void ProcessCompleted<in T>(object[] arguments, T processResult);
        public static bool isBusy { get; private set; }      

        protected void tryExecuteAsync<T>(object[] arguments, ProcessDelegate<T> processDelegate, ProcessCompleted<T> completed)
        {
            processDelegate.BeginInvoke(arguments,
                                        result =>
                                        {
                                            var p = (ProcessDelegate<T>)((AsyncResult)result).AsyncDelegate;
                                            var iResult = p.EndInvoke(result);

                                            Dispatcher.Invoke((Action)(() => completed.Invoke(arguments, iResult)));

                                        },
                                        null
                );
        }

        public static void executeLongProcess(Delegate metodoAInvocar, Delegate operacionCompletada)
        {
            var bgw = new BackgroundWorker();

            bgw.DoWork += delegate
            {
                isBusy = true;
                metodoAInvocar.DynamicInvoke();
            };
            bgw.RunWorkerCompleted += delegate
            {
                isBusy = false;
                operacionCompletada.DynamicInvoke();
            };

            bgw.RunWorkerAsync();
        }

        
    }
}
