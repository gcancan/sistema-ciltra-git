﻿using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for controlMotivoRescate.xaml
    /// </summary>
    public partial class controlMotivoRescate : UserControl
    {
        public controlMotivoRescate()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            cbxMotivo.Width = Width - 4;
        }
        public void cargarControl()
        {
            try
            {
                OperationResult resp = new RescateLlantasSvc().getMotivoRescateAllOrBy();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxMotivo.ItemsSource = (List<MotivosRescate>)resp.result;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    this.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                this.IsEnabled = false;
            }
        }

        public MotivosRescate motivoRescateSelect
        {
            get
            {
                if (cbxMotivo.SelectedItem != null)
                {
                    return (MotivosRescate)cbxMotivo.SelectedItem;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                MotivosRescate motivo = value;
                if (motivo == null)
                {
                    cbxMotivo.SelectedItem = null;
                }
                else
                {
                    int i = 0;
                    foreach (var item in (List<MotivosRescate>)cbxMotivo.ItemsSource)
                    {
                        if (item.idMotivoRescate == motivo.idMotivoRescate)
                        {
                            cbxMotivo.SelectedIndex = i;
                            break;
                        }
                        i++;
                    }
                }
            }
        }
    }
}
