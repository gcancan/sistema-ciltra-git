﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editCatTipoCombustibleExtra.xaml
    /// </summary>
    public partial class editCatTipoCombustibleExtra : ViewBase
    {
        public editCatTipoCombustibleExtra()
        {
            InitializeComponent();
        }
        public override void buscar()
        {
            TipoOrdenCombustible tipo = new selectTipoCombustibleExtra().buscarAllTipoCargaExtra();
            if (tipo != null)
            {
                this.mapForm(tipo);
                base.buscar();
            }
        }

        public override OperationResult eliminar()
        {
            OperationResult result2;
            try
            {
                base.Cursor = Cursors.Wait;
                TipoOrdenCombustible tag = this.txtClave.Tag as TipoOrdenCombustible;
                OperationResult result = new CargaCombustibleSvc().saveTipoCombustibleEtra(ref tag);
                tag.activo = false;
                if (result.typeResult == ResultTypes.success)
                {
                    base.nuevo();
                    this.mapForm(null);
                }
                result2 = result;
            }
            catch (Exception exception)
            {
                result2 = new OperationResult(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return result2;
        }

        public override OperationResult guardar()
        {
            OperationResult result2;
            try
            {
                base.Cursor = Cursors.Wait;
                TipoOrdenCombustible tipoOrden = this.mapForm();
                OperationResult result = new CargaCombustibleSvc().saveTipoCombustibleEtra(ref tipoOrden);
                if (result.typeResult == ResultTypes.success)
                {
                    base.guardar();
                    this.mapForm(tipoOrden);
                }
                result2 = result;
            }
            catch (Exception exception)
            {
                result2 = new OperationResult(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return result2;
        }
        public TipoOrdenCombustible mapForm()
        {
            try
            {
                return new TipoOrdenCombustible
                {
                    idTipoOrdenCombustible = (this.txtClave.Tag == null) ? 0 : (this.txtClave.Tag as TipoOrdenCombustible).idTipoOrdenCombustible,
                    nombreTipoOrdenCombustible = this.txtDescripción.Text,
                    operativo = this.chcOperativo.IsChecked.Value,
                    requiereViaje = this.chcIsViaje.IsChecked.Value,
                    activo = true
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void mapForm(TipoOrdenCombustible tipo)
        {
            try
            {
                if (tipo != null)
                {
                    this.txtClave.Tag = tipo;
                    this.txtClave.valor = tipo.idTipoOrdenCombustible;
                    this.txtDescripción.Text = tipo.nombreTipoOrdenCombustible;
                    this.chcIsViaje.IsChecked = new bool?(tipo.requiereViaje);
                    this.chcOperativo.IsChecked = new bool?(tipo.operativo);
                }
                else
                {
                    this.txtClave.Tag = null;
                    this.txtClave.valor = 0;
                    this.txtDescripción.Clear();
                    this.chcIsViaje.IsChecked = false;
                    this.chcOperativo.IsChecked = false;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            this.mapForm(null);
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.nuevo();
        }
    }
}
