﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using Core.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para reporteSemaforoMantenimiento.xaml
    /// </summary>
    public partial class reporteSemaforoMantenimiento : ViewBase
    {
        public reporteSemaforoMantenimiento()
        {
            InitializeComponent();

        }
        Usuario usuario = null;
        List<Servicio> lista = new List<Servicio>();
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.usuario = mainWindow.usuario;

            var resp = new ServiciosSvc().getAllServicios();
            if (resp.typeResult == ResultTypes.success)
            {
                lista = resp.result as List<Servicio>;
                lista = lista.FindAll(s => s.idServicio == 1 || s.idServicio == 2 || s.idServicio == 13 || s.idServicio == 23 || s.idServicio == 24).ToList();

            }

            ctrEmpresas.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresas.loaded(usuario.empresa, new PrivilegioSvc().consultarPrivilegio("isAdmin", usuario.idUsuario).typeResult == ResultTypes.success);
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);

            ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
            ctrZonaOperativa.cargarControl(usuario.zonaOperativa, usuario.idUsuario);
        }
        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                cbxOperacion.ItemsSource = null;
                if (sender != null)
                {
                    if (ctrZonaOperativa.zonaOperativa != null)
                    {

                        if (empresa.clave == 1)
                        {
                            cargarOperaciones();
                        }
                        else if (empresa.clave == 2)
                        {
                            buscarTcByFlotaAtlante();
                        }

                        
                    }
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        void cargarOperaciones()
        {
            cbxOperacion.ItemsSource = null;
            var resp = new ClienteSvc().getClientesByZonasOperativas(new List<string> { (ctrZonaOperativa.zonaOperativa.idZonaOperativa.ToString()) }, ctrEmpresas.empresaSelected.clave);
            if (resp.typeResult == ResultTypes.success)
            {
                var listaOperaciones = resp.result as List<OperacionFletera>;
                cbxOperacion.ItemsSource = listaOperaciones;
                //cbxOperacion.SelectedItems.Add(cbxOperacion.ItemsSource.Cast<OperacionFletera>().ToList().Find(s => s.cliente.clave == usuario.cliente.clave));
            }
            else if (resp.typeResult != ResultTypes.recordNotFound)
            {
                imprimir(resp);
            }
        }
        private void CbxOperacion_ItemSelectionChanged(object sender, Xceed.Wpf.Toolkit.Primitives.ItemSelectionChangedEventArgs e)
        {
            try
            {
                List<UnidadTransporte> listaUnidades = new List<UnidadTransporte>();
                chcTodos.IsChecked = false;
                cbxUnidades.SelectedItems.Clear();
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    if (cbxOperacion.SelectedItems.Count >= 0)
                    {
                        foreach (OperacionFletera operacion in cbxOperacion.SelectedItems)
                        {
                            var resp = new FlotaSvc().getFlota(operacion);
                            if (resp.typeResult == ResultTypes.success)
                            {
                                foreach (Flota flota in resp.result as List<Flota>)
                                {
                                    if (flota.tractor != null)
                                    {
                                        listaUnidades.Add(flota.tractor);
                                    }
                                }
                            }
                        }
                    }
                    cbxUnidades.ItemsSource = listaUnidades.OrderBy(s => s.clave);
                    //chcTodos.IsChecked = true;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }

        }
        Empresa empresa = null;
        void buscarTcByFlotaAtlante()
        {
            try
            {
                List<UnidadTransporte> listaUnidades = new List<UnidadTransporte>();
                chcTodos.IsChecked = false;
                cbxUnidades.SelectedItems.Clear();
                Cursor = Cursors.Wait;
                OperacionFletera operacion = new OperacionFletera { empresa = empresa, zonaOperativa = ctrZonaOperativa.zonaOperativa, cliente = null };
                var resp = new FlotaSvc().getFlota(operacion);
                if (resp.typeResult == ResultTypes.success)
                {
                    foreach (Flota flota in resp.result as List<Flota>)
                    {
                        if (flota.tractor != null)
                        {
                            listaUnidades.Add(flota.tractor);
                        }
                    }
                    cbxUnidades.ItemsSource = listaUnidades.OrderBy(s => s.clave);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                chcTodos.IsChecked = false;
                cbxUnidades.ItemsSource = null;
                if (sender != null)
                {
                    empresa = ctrEmpresas.empresaSelected;
                    if (empresa.clave == 5)
                    {
                        var resp = new UnidadTransporteSvc().getTractores(ctrEmpresas.empresaSelected.clave);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            cbxUnidades.ItemsSource = resp.result as List<UnidadTransporte>;
                            chcTodos.IsChecked = true;
                        }
                        else
                        {
                            cbxUnidades.ItemsSource = null;
                        }
                        stpOperacion.Visibility = Visibility.Hidden;
                        ctrZonaOperativa.Visibility = Visibility.Hidden;
                    }
                    else if (empresa.clave == 2)
                    {
                        stpOperacion.Visibility = Visibility.Hidden;
                        ctrZonaOperativa.Visibility = Visibility.Visible;
                        buscarTcByFlotaAtlante();
                    }
                    else if(empresa.clave == 1)
                    {
                        ctrZonaOperativa.Visibility = Visibility.Visible;
                        stpOperacion.Visibility = Visibility.Visible;

                        if (ctrZonaOperativa.cbxZonaOpetativa == null) return;

                        ctrZonaOperativa.cbxZonaOpetativa.SelectedItem = null;

                        //if (ctrZonaOperativa.cbxZonaOpetativa.SelectedItem == null) return;
                        ctrZonaOperativa.cbxZonaOpetativa.SelectedIndex = 0;
                        
                    }

                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void BtnBuscar_Click(object sender, RoutedEventArgs e)
        {
            buscarResultados();
        }
        List<SemaforoMtto> listaSemaforo = new List<SemaforoMtto>();
        private void buscarResultados()
        {
            try
            {
                lvlResultados.Items.Clear();
                Cursor = Cursors.Wait;

                if (cbxUnidades.SelectedItems.Count <= 0) return;

                List<string> listaUnidades = cbxUnidades.SelectedItems.Cast<UnidadTransporte>().ToList().Select(s => s.clave).ToList();

                var resp = new SemaforoMttoSvc().getSemaforoMtto(listaUnidades);
                if (resp.typeResult == ResultTypes.success)
                {
                    listaSemaforo = (resp.result as List<SemaforoMtto>);
                    mapLista(listaSemaforo);
                }
                else
                {
                    lvlResultados.Items.Clear();
                    ImprimirMensaje.imprimir(resp);
                }

            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapLista(List<SemaforoMtto> listaSemaforo)
        {
            lvlResultados.Items.Clear();
            foreach (SemaforoMtto semaforo in listaSemaforo)
            {
                ListViewItem lvl = new ListViewItem();
                semaforo.listaServicios = lista;
                lvl.Content = semaforo;
                lvl.FontWeight = FontWeights.Bold;
                //lvl.Foreground = Brushes.SteelBlue;

                switch (semaforo.getColorSemaforo)
                {
                    case ColorSemaforo.NINGUNO:
                        break;
                    case ColorSemaforo.VERDE:
                        lvl.Background = Brushes.Green;
                        break;
                    case ColorSemaforo.AMARILLO:
                        lvl.Background = Brushes.Yellow;
                        break;
                    case ColorSemaforo.NARANJA:
                        lvl.Background = Brushes.Orange;
                        break;
                    case ColorSemaforo.ROJO:
                        lvl.Background = Brushes.Red;
                        break;
                    default:
                        break;
                }

                ContextMenu context = new ContextMenu();
                MenuItem menu = new MenuItem { Header = "Agregar cita mantenimiento", Tag = semaforo };
                menu.Click += Menu_Click;
                context.Items.Add(menu);

                lvl.ContextMenu = context;

                lvlResultados.Items.Add(lvl);
            }
            ctrCantServA.valor = listaSemaforo.FindAll(s => s.servicio.descipcion.ToUpper() == "SERVICIO A").Count();
            ctrCantServAA.valor = listaSemaforo.FindAll(s => s.servicio.descipcion.ToUpper() == "SERVICIO AA").Count();
            ctrCantServB.valor = listaSemaforo.FindAll(s => s.servicio.descipcion.ToUpper() == "SERVICIO B").Count();
            ctrCantServBA.valor = listaSemaforo.FindAll(s => s.servicio.descipcion.ToUpper() == "SERVICIO BA").Count();
            ctrCantServC.valor = listaSemaforo.FindAll(s => s.servicio.descipcion.ToUpper() == "SERVICIO C").Count();

            ctrCantVerde.valor = lvlResultados.Items.Cast<ListViewItem>().ToList().FindAll(S => S.Background == Brushes.Green).Count();
            ctrCantAmarillo.valor = lvlResultados.Items.Cast<ListViewItem>().ToList().FindAll(S => S.Background == Brushes.Yellow).Count();
            ctrCantNaranja.valor = lvlResultados.Items.Cast<ListViewItem>().ToList().FindAll(S => S.Background == Brushes.Orange).Count();
            ctrCantRojo.valor = lvlResultados.Items.Cast<ListViewItem>().ToList().FindAll(S => S.Background == Brushes.Red).Count();

            ctrTotalUnidades.valor = listaSemaforo.Count();

        }

        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                MenuItem menu = sender as MenuItem;
                SemaforoMtto semaforo = menu.Tag as SemaforoMtto;

                var select = new selectCitaMantenimiento(mainWindow);
                var s = select.agregarCita(semaforo);
            }
        }

        private void ChcTodos_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    CheckBox check = sender as CheckBox;
                    switch (check.IsChecked.Value)
                    {
                        case true:
                            foreach (var item in cbxUnidades.Items)
                            {
                                cbxUnidades.SelectedItems.Add(item);
                            }
                            break;
                        case false:
                            cbxUnidades.SelectedItems.Clear();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void Ctr_GotFocus(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                controlEnteros control = sender as controlEnteros;
                //control.BorderThickness = new Thickness(3);
                switch (control.Name)
                {
                    case "ctrCantServA":
                        mapLista(listaSemaforo.FindAll(s => s.servicio.descipcion.ToUpper() == "SERVICIO A"));
                        break;
                    case "ctrCantServAA":
                        mapLista(listaSemaforo.FindAll(s => s.servicio.descipcion.ToUpper() == "SERVICIO AA"));
                        break;
                    case "ctrCantServB":
                        mapLista(listaSemaforo.FindAll(s => s.servicio.descipcion.ToUpper() == "SERVICIO B"));
                        break;
                    case "ctrCantServBA":
                        mapLista(listaSemaforo.FindAll(s => s.servicio.descipcion.ToUpper() == "SERVICIO BA"));
                        break;
                    case "ctrCantServC":
                        mapLista(listaSemaforo.FindAll(s => s.servicio.descipcion.ToUpper() == "SERVICIO C"));
                        break;
                    case "ctrCantVerde":
                        mapLista(lvlResultados.Items.Cast<ListViewItem>().ToList().FindAll(s => s.Background == Brushes.Green).ToList().Select(s => s.Content as SemaforoMtto).ToList());
                        break;
                    case "ctrCantAmarillo":
                        mapLista(lvlResultados.Items.Cast<ListViewItem>().ToList().FindAll(s => s.Background == Brushes.Yellow).ToList().Select(s => s.Content as SemaforoMtto).ToList());
                        break;
                    case "ctrCantNaranja":
                        mapLista(lvlResultados.Items.Cast<ListViewItem>().ToList().FindAll(s => s.Background == Brushes.Orange).ToList().Select(s => s.Content as SemaforoMtto).ToList());
                        break;
                    case "ctrCantRojo":
                        mapLista(lvlResultados.Items.Cast<ListViewItem>().ToList().FindAll(s => s.Background == Brushes.Red).ToList().Select(s => s.Content as SemaforoMtto).ToList());
                        break;
                    default:
                        break;
                }
            }
        }

        private void Ctr_LostFocus(object sender, RoutedEventArgs e)
        {
            //if (sender != null)
            //{
            //    controlEnteros control = sender as controlEnteros;
            //    control.BorderThickness = new Thickness(1);                
            //}
            mapLista(listaSemaforo);
        }

        private void ChcTodosOperacion_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                cbxOperacion.SelectedItems.Clear();
                Cursor = Cursors.Wait;
                if (sender != null)
                {
                    CheckBox check = sender as CheckBox;
                    switch (check.IsChecked.Value)
                    {
                        case true:
                            foreach (var item in cbxOperacion.Items)
                            {
                                cbxOperacion.SelectedItems.Add(item);
                            }
                            break;
                        case false:
                            cbxOperacion.SelectedItems.Clear();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
