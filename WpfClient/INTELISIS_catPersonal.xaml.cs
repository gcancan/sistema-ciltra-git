﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Interfaces;
using Core.Models;
using CoreINTELISIS.INTERFACES;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para INTELISIS_catPersonal.xaml
    /// </summary>
    public partial class INTELISIS_catPersonal : UserControl
    {
        SincronizadorINTELISIS pantalla = null;
        public INTELISIS_catPersonal(SincronizadorINTELISIS pantalla)
        {
            InitializeComponent();
            this.pantalla = pantalla;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            pantalla.stpContenedor.Children.Clear();
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                lvlPersonal.ItemsSource = null;
                lblContador.Content = 0;
                lblActualizados.Content = 0;
                lblNuevos.Content = 0;
                lblActualizados.Content = 0;
                Cursor = Cursors.Wait;
                var resp = new PersonalINTELISISSvc().getAllPersonal();
                if (resp.typeResult == ResultTypes.success)
                {
                    List<Personal> listaPersona = resp.result as List<Personal>;
                    llenarLista(listaPersona);
                    lblContador.Content = listaPersona.Count();
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void llenarLista(List<Personal> listaPersona)
        {
            try
            {
                lvlPersonal.ItemsSource = null;

                this.lvlPersonal.ItemsSource = listaPersona;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlPersonal.ItemsSource);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtBuscador.Text) ||
            ((((Personal)item).nombre.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void TxtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (lvlPersonal.ItemsSource == null) return;

            CollectionViewSource.GetDefaultView(this.lvlPersonal.ItemsSource).Refresh();
        }
        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                if (lvlPersonal.ItemsSource != null)
                {
                    List<Personal> listaPersonas = (lvlPersonal.ItemsSource as List<Personal>).FindAll(s => s.isSeleccionado);
                    if (listaPersonas.Count > 0)
                    {
                        sincronizarPersonas(listaPersonas);
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        public void sincronizarPersonas(List<Personal> listaPersonal)
        {
            try
            {
                Cursor = Cursors.Wait;
                lblActualizados.Content = 0;
                lblNuevos.Content = 0;

                var resp = new OperadorSvc().sincronizarPersonal(listaPersonal, pantalla.mainWindow.usuario.nombreUsuario);
                lblActualizados.Content = resp.noActualizados;
                lblNuevos.Content = resp.noNuevos;
                ImprimirMensaje.imprimir(resp);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void chcSeleccionado_Checked(object sender, RoutedEventArgs e)
        {
            if (lvlPersonal.ItemsSource != null)
            {
                lblSeleccionados.Content = lvlPersonal.ItemsSource.Cast<Personal>().ToList().Count(S => S.isSeleccionado);
            }
            else
            {
                lblActualizados.Content = 0;
            }
        }
    }
}
