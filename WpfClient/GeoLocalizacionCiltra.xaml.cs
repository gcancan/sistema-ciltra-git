﻿namespace WpfClient
{
    using CoreFletera.Models;
    using Microsoft.Maps.MapControl.WPF;
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Markup;
    using System.Windows.Media;

    public partial class GeoLocalizacionCiltra : Window
    {
        private ParadaEquipo paradas;
        private Location location;
        private Location locationFinal;
        private string[] coordenadas;
        private string[] coordenadasFinales;
        private MapMode modeMap;
        public GeoLocalizacionCiltra()
        {
            this.paradas = null;
            this.location = null;
            this.modeMap = null;
            this.InitializeComponent();
        }

        public GeoLocalizacionCiltra(ParadaEquipo paradas)
        {
            this.paradas = null;
            this.location = null;
            this.modeMap = null;
            this.InitializeComponent();
            this.paradas = paradas;
            //this.coor1.IsEnabled = false;
            //this.coor2.IsEnabled = false;
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            this.centrar();
        }

        private void centrar()
        {
            this.mapCiltra.Center = this.location;
            this.mapCiltra.ZoomLevel = 16.5;
        }

        private void chcSatelite_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.chcSatelite.IsChecked.Value)
                {
                    this.mapCiltra.Mode = new AerialMode(true);
                }
                else
                {
                    this.mapCiltra.Mode = new RoadMode();
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void crearMarcador(ParadaEquipo paradas)
        {
            try
            {
                Image element = new Image
                {
                    Width = 50.0,
                    Height = 50.0,
                    Stretch = Stretch.Fill
                };
                Image element2 = new Image
                {
                    Width = 50.0,
                    Height = 50.0,
                    Stretch = Stretch.Fill
                };
                element.Source = (paradas.estatus == "PNA") ? this.imaMarcadorPincheta.Source : this.imaMarcadorTE.Source;
                element2.Source = (paradas.estatus == "PNA") ? this.imaMarcadorPincheta.Source : this.imaMarcadorTE.Source;

                element.ToolTip = string.Format("PUNTO INICIAL {0}", paradas.coordenadas);
                element2.ToolTip = string.Format("PUNTO FINAL {0}", paradas.coordenadasFinales);

                MapLayer layer = new MapLayer();
                MapLayer layer2 = new MapLayer();
                layer.AddChild(element, this.location, PositionOrigin.BottomCenter);
                layer2.AddChild(element2, locationFinal, PositionOrigin.BottomCenter);
                //layer.AddChild(;
                this.mapCiltra.Children.Add(layer);
                this.mapCiltra.Children.Add(layer2);
                this.centrar();
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.paradas != null)
                {
                    char[] separator = new char[] { ',' };

                    this.coordenadas = this.paradas.coordenadas.Split(separator);
                    this.coor1.Text = this.coordenadas[0].Trim();
                    this.coor2.Text = this.coordenadas[1].Trim();

                    this.coordenadasFinales = this.paradas.coordenadasFinales.Split(separator);
                    this.coor1Final.Text = this.coordenadasFinales[0].Trim();
                    this.coor2Final.Text = this.coordenadasFinales[1].Trim();

                    this.location = new Location(new Location(Convert.ToDouble(this.coor1.Text), Convert.ToDouble(this.coor2.Text)));
                    this.locationFinal = new Location(new Location(Convert.ToDouble(this.coor1Final.Text), Convert.ToDouble(this.coor2Final.Text)));

                    this.crearMarcador(this.paradas);
                }
                this.modeMap = this.mapCiltra.Mode;
                chcSatelite.IsChecked = true;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                base.IsEnabled = false;
            }
        }
    }
}
