﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para reporteExistenciasCombustible.xaml
    /// </summary>
    public partial class reporteExistenciasCombustible : ViewBase
    {
        private List<GrupoResumenKardexCombustible> listaGrupo = new List<GrupoResumenKardexCombustible>();
        public reporteExistenciasCombustible()
        {
            InitializeComponent();
        }
        private void armarControles(List<GrupoResumenKardexCombustible> listaGrupo)
        {
            try
            {
                this.listaGrupo = listaGrupo;
                this.tapExistencias.Items.Clear();
                foreach (GrupoResumenKardexCombustible combustible in listaGrupo)
                {
                    TabItem newItem = new TabItem
                    {
                        Header = combustible.empresa
                    };
                    StackPanel panel = new StackPanel();
                    StackPanel element = new StackPanel
                    {
                        Orientation = Orientation.Horizontal
                    };
                    StackPanel panel3 = new StackPanel
                    {
                        Orientation = Orientation.Horizontal
                    };
                    Label label1 = new Label
                    {
                        Content = "Existencia Inicial:",
                        FontWeight = FontWeights.Bold,
                        Foreground = Brushes.SteelBlue
                    };
                    panel3.Children.Add(label1);
                    TextBox box1 = new TextBox
                    {
                        Text = combustible.lista[0].cantInicial.ToString("N4"),
                        Width = 120.0,
                        FontWeight = FontWeights.Bold,
                        Foreground = Brushes.SteelBlue,
                        IsEnabled = false
                    };
                    panel3.Children.Add(box1);
                    element.Children.Add(panel3);
                    StackPanel panel4 = new StackPanel
                    {
                        Orientation = Orientation.Horizontal
                    };
                    Label label2 = new Label
                    {
                        Content = "Entradas:",
                        FontWeight = FontWeights.Bold,
                        Foreground = Brushes.SteelBlue
                    };
                    panel4.Children.Add(label2);
                    TextBox box2 = new TextBox
                    {
                        Text = combustible.lista.Sum<ResumenKardexCombustible>(s => s.entradas).ToString("N4"),
                        Width = 120.0,
                        FontWeight = FontWeights.Bold,
                        Foreground = Brushes.SteelBlue,
                        IsEnabled = false
                    };
                    panel4.Children.Add(box2);
                    element.Children.Add(panel4);
                    StackPanel panel5 = new StackPanel
                    {
                        Orientation = Orientation.Horizontal
                    };
                    Label label3 = new Label
                    {
                        Content = "Salidas:",
                        FontWeight = FontWeights.Bold,
                        Foreground = Brushes.SteelBlue
                    };
                    panel5.Children.Add(label3);
                    TextBox box3 = new TextBox
                    {
                        Text = combustible.lista.Sum<ResumenKardexCombustible>(s => s.salidas).ToString("N4"),
                        Width = 120.0,
                        FontWeight = FontWeights.Bold,
                        Foreground = Brushes.SteelBlue,
                        IsEnabled = false
                    };
                    panel5.Children.Add(box3);
                    element.Children.Add(panel5);
                    StackPanel panel6 = new StackPanel
                    {
                        Orientation = Orientation.Horizontal
                    };
                    Label label4 = new Label
                    {
                        Content = "Existencia Actual:",
                        FontWeight = FontWeights.Bold,
                        Foreground = Brushes.SteelBlue
                    };
                    panel6.Children.Add(label4);
                    TextBox box4 = new TextBox
                    {
                        Text = combustible.lista[combustible.lista.Count - 1].cantFinal.ToString("N4"),
                        Width = 120.0,
                        FontWeight = FontWeights.Bold,
                        Foreground = Brushes.SteelBlue,
                        IsEnabled = false
                    };
                    panel6.Children.Add(box4);
                    element.Children.Add(panel6);
                    panel.Children.Add(element);
                    ListView view = new ListView
                    {
                        Height = 410.0,
                        HorizontalAlignment = HorizontalAlignment.Left,
                        VerticalAlignment = VerticalAlignment.Top,
                        Background = null
                    };
                    GridView view2 = new GridView();
                    GridViewColumnCollection columns = view2.Columns;
                    GridViewColumn item = new GridViewColumn
                    {
                        Header = "ENTRADAS",
                        Width = 120.0
                    };
                    Binding binding1 = new Binding("entradas")
                    {
                        StringFormat = "N4"
                    };
                    item.DisplayMemberBinding = binding1;
                    view2.Columns.Add(item);
                    GridViewColumn column2 = new GridViewColumn
                    {
                        Header = "SALIDAS",
                        Width = 120.0
                    };
                    Binding binding2 = new Binding("salidas")
                    {
                        StringFormat = "N4"
                    };
                    column2.DisplayMemberBinding = binding2;
                    view2.Columns.Add(column2);
                    GridViewColumn column3 = new GridViewColumn
                    {
                        Header = "EXISTENCIA",
                        Width = 120.0
                    };
                    Binding binding3 = new Binding("cantFinal")
                    {
                        StringFormat = "N4"
                    };
                    column3.DisplayMemberBinding = binding3;
                    view2.Columns.Add(column3);
                    GridViewColumn column4 = new GridViewColumn
                    {
                        Header = "D\x00cdA",
                        Width = 120.0
                    };
                    Binding binding4 = new Binding("dia")
                    {
                        StringFormat = "dd/MM/yyyy"
                    };
                    column4.DisplayMemberBinding = binding4;
                    view2.Columns.Add(column4);
                    view.View = view2;
                    view.ItemsSource = combustible.lista;
                    panel.Children.Add(view);
                    newItem.Content = panel;
                    this.tapExistencias.Items.Add(newItem);
                }
                this.tapExistencias.SelectedIndex = 0;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }
        List<DateTime> listaFechas = new List<DateTime>();
        List<ExistenciaDiario> listaExistenciasPegaso = new List<ExistenciaDiario>();
        List<ExistenciaDiario> listaExistenciasGlobal = new List<ExistenciaDiario>();
        List<GrupoExistenciaDiario> listaGrupoExistencias = new List<GrupoExistenciaDiario>();
        List<ExistenciaDiario> listaExistenciasPegasoGlobal = new List<ExistenciaDiario>();
        List<ExistenciaDiario> listaExistencias = new List<ExistenciaDiario>();
        ZonaOperativa zona = null;
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;

                stpFechas.Children.Clear();
                stpExistenciasEmpresa.Children.Clear();
                listaFechas = new List<DateTime>();
                listaExistencias = new List<ExistenciaDiario>();
                listaGrupoExistencias = new List<GrupoExistenciaDiario>();
                listaExistenciasPegaso = new List<ExistenciaDiario>();
                listaExistenciasGlobal = new List<ExistenciaDiario>();
                listaExistenciasPegasoGlobal = new List<ExistenciaDiario>();

                zona = ctrZonaOperativa.zonaOperativa;

                TimeSpan tiempo = ctrFechas.fechaFinal - ctrFechas.fechaInicial;


                for (int i = 0; i <= tiempo.Days; i++)
                {
                    DateTime dtp = ctrFechas.fechaInicial.AddDays(i);
                    if (dtp > DateTime.Now) continue;
                    listaFechas.Add(ctrFechas.fechaInicial.AddDays(i));
                }

                var respExis = new KardexCombustibleSvc().getExisteciaDiarias((this.txtTanque.Tag as TanqueCombustible).idTanqueCombustible, listaFechas, ctrEmpresa.empresaSelected.clave, chcTodasEmpresa.IsChecked.Value);
                if (respExis.typeResult == ResultTypes.success)
                {
                    listaExistencias = respExis.result as List<ExistenciaDiario>;

                    listaGrupoExistencias = (from v in listaExistencias
                                             group v by v.empresa.clave into grupo
                                             select new GrupoExistenciaDiario
                                             {
                                                 empresa = (from s in grupo select s.empresa).First(),
                                                 listaExistencias = grupo.ToList<ExistenciaDiario>()
                                             }).ToList();
                    addControlesFEchas(listaFechas);


                    if (listaGrupoExistencias.Count > 1)
                    {

                        stpExistenciasEmpresa.Children.Add(new controlExistenciaEmpresas(listaGrupoExistencias.Find(s => s.empresa.clave == 2)));

                        listaExistenciasPegaso = listaExistencias.FindAll(s => s.empresa.clave == 1);
                        stpExistenciasEmpresa.Children.Add(new controlExistenciaEmpresas(listaGrupoExistencias.Find(s => s.empresa.clave == 1)));

                        listaExistenciasGlobal = listaExistencias.FindAll(s => s.empresa.clave == 5);
                        stpExistenciasEmpresa.Children.Add(new controlExistenciaEmpresas(listaGrupoExistencias.Find(s => s.empresa.clave == 5)));

                        stpExistenciasEmpresa.Children.Add(new controlExistenciaEmpresas(listaExistencias, zona));


                        listaExistenciasPegasoGlobal.AddRange(listaExistenciasPegaso);
                        listaExistenciasPegasoGlobal.AddRange(listaExistenciasGlobal);

                        stpExistenciasEmpresa.Children.Add(new controlExistenciaPegasoGlobal(listaExistenciasPegasoGlobal));
                    }
                    else if (listaGrupoExistencias.Count == 1)
                    {
                        stpExistenciasEmpresa.Children.Add(new controlExistenciaEmpresas(listaGrupoExistencias.First()));
                    }
                    //foreach (var item in listaGrupo)
                    //{
                    //    stpExistenciasEmpresa.Children.Add(new controlExistenciaEmpresas(item));
                    //}
                }

                return;
                OperationResult resp = new KardexCombustibleSvc().getResumenKardexCombustible(this.ctrFechas.fechaInicial, this.ctrFechas.fechaFinal, (this.txtTanque.Tag as TanqueCombustible).idTanqueCombustible, this.chcTodasEmpresa.IsChecked.Value ? 0 : this.ctrEmpresa.empresaSelected.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<ResumenKardexCombustible> list = resp.result as List<ResumenKardexCombustible>;
                    List<GrupoResumenKardexCombustible> listaGrupo = (from p in list
                                                                      group p by p.idEmpresa into g
                                                                      select new GrupoResumenKardexCombustible
                                                                      {
                                                                          empresa = (from s in g select s.empresa).First<string>(),
                                                                          lista = (from s in g select s).ToList<ResumenKardexCombustible>()
                                                                      }).ToList<GrupoResumenKardexCombustible>();
                    this.armarControles(listaGrupo);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void addControlesFEchas(List<DateTime> listaFechas)
        {
            try
            {
                foreach (var fecha in listaFechas)
                {
                    Label lblFecha = new Label
                    {
                        Content = fecha.ToString("dd/MM/yyyy"),
                        Width = 75,
                        BorderThickness = new Thickness(1),
                        BorderBrush = Brushes.Black,
                        FontWeight = FontWeights.Bold,
                        HorizontalContentAlignment = HorizontalAlignment.Right,
                        FontSize = 10,
                        VerticalAlignment = VerticalAlignment.Center
                    };
                    stpFechas.Children.Add(lblFecha);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private void btnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                //new ReportView().generarReporteExistenciaCombustible(this.listaGrupo);
                new ReportView().generarReporteExistenciaCombustible(listaFechas, listaExistencias, listaGrupoExistencias, zona);
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.ctrZonaOperativa.zonaOperativa != null)
            {
                this.getTanquesDiesel(this.ctrZonaOperativa.zonaOperativa);
            }
        }
        private void getTanquesDiesel(ZonaOperativa zonaOperativa)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult resp = new TanqueCombustibleSvc().getTanqueByZonaOperativa(zonaOperativa);
                if (resp.typeResult == ResultTypes.success)
                {
                    TanqueCombustible tanque = resp.result as TanqueCombustible;
                    this.txtTanque.Tag = tanque;
                    this.txtTanque.Text = tanque.nombre;
                    //buscarFechaInicio(tanque.idTanqueCombustible);

                    buscarUltimoCierre(tanque.idTanqueCombustible);
                }
                else
                {
                    if (resp.typeResult != ResultTypes.recordNotFound)
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                    this.txtTanque.Tag = null;
                    this.txtTanque.Clear();
                    mapFechasCierre(null);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        private void buscarUltimoCierre(int idTanqueCombustible)
        {
            try
            {
                var resp = new CargaCombustibleSvc().getUltimoCierreProcesoCombustible(idTanqueCombustible);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapFechasCierre(resp.result as CierreProcesoCombustible);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    mapFechasCierre(null);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void mapFechasCierre(CierreProcesoCombustible cierreProcesoCombustible)
        {
            if (cierreProcesoCombustible != null)
            {
                txtUltimaFechaCierre.Text = cierreProcesoCombustible.fechaFin.ToString("dd/MM/yy HH:ss");
                txtUsuario.Text = cierreProcesoCombustible.usuario;
            }
            else
            {
                txtUltimaFechaCierre.Clear();
                txtUsuario.Clear();
            }
        }

        void buscarFechaInicio(int idTanque)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new SuministroCombustibleSvc().getNivelesIniciales(idTanque);
                if (resp.typeResult == ResultTypes.success)
                {
                    List<NivelInicial> lista = resp.result as List<NivelInicial>;
                    txtUltimaFechaCierre.Text = lista[0].fecha.ToString("dd/MM/yy HH:ss");
                }
                else
                {
                    txtUltimaFechaCierre.Clear();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += new SelectionChangedEventHandler(this.CbxZonaOpetativa_SelectionChanged);
            this.ctrZonaOperativa.cargarControl(base.mainWindow.zonaOperativa, base.mainWindow.usuario.idUsuario);
            bool habilitado = false;
            if (new PrivilegioSvc().consultarPrivilegio("isAdmin", base.mainWindow.usuario.idUsuario).typeResult == ResultTypes.success)
            {
                habilitado = true;
            }
            else
            {
                this.chcTodasEmpresa.IsEnabled = false;
            }
            this.ctrEmpresa.loaded(base.mainWindow.empresa, habilitado);
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
        }
    }
}
