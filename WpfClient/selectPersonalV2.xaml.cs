﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectPersonalV2.xaml
    /// </summary>
    public partial class selectPersonalV2 : Window
    {
        public selectPersonalV2()
        {
            InitializeComponent();
        }
        public selectPersonalV2(List<Personal> listaPersonal)
        {
            this.listaPersonal = listaPersonal;
            InitializeComponent();
        }
        List<Personal> listaPersonal = new List<Personal>();
        Empresa empresa = null;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
        public Personal getPersonalByDepartamento(int idDepto)
        {
            try
            {
                getAllPersonalByDepartamento(idDepto);
                bool? resp = ShowDialog();
                if (resp.Value && lvlPersonal.SelectedItem != null)
                {
                    return (lvlPersonal.SelectedItem as ListViewItem).Content as Personal;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Personal getPersonalActivo()
        {
            try
            {
                getAllPersonalActivo();
                bool? resp = ShowDialog();
                if (resp.Value && lvlPersonal.SelectedItem != null)
                {
                    return (lvlPersonal.SelectedItem as ListViewItem).Content as Personal;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Personal getPersonal()
        {
            try
            {
                mapPersonal(this.listaPersonal);
                bool? resp = ShowDialog();
                if (resp.Value && lvlPersonal.SelectedItem != null)
                {
                    return (lvlPersonal.SelectedItem as ListViewItem).Content as Personal;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Personal getPersonalByEmpresa(Empresa empresa)
        {
            try
            {
                this.empresa = empresa;
                getAllPersonalByEmpresa();
                bool? resp = ShowDialog();
                if (resp.Value && lvlPersonal.SelectedItem != null)
                {
                    return (lvlPersonal.SelectedItem as ListViewItem).Content as Personal;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void getAllPersonalByEmpresa()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new OperadorSvc().getPersonal_v2ByIdEmpresa(empresa.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapPersonal(resp.result as List<Personal>);
                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void getAllPersonalByDepartamento(int idDepto)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new OperadorSvc().getPersonal_v2ByDepartamento(idDepto);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapPersonal(resp.result as List<Personal>);
                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void getAllPersonalActivo()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new OperadorSvc().getPersonal_v2Activos();
                if (resp.typeResult == ResultTypes.success)
                {
                    mapPersonal(resp.result as List<Personal>);
                }
                else
                {
                    imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapPersonal(List<Personal> listPersonal)
        {
            try
            {
                lvlPersonal.ItemsSource = null;
                List<ListViewItem> list = new List<ListViewItem>();
                foreach (Personal col in listPersonal)
                {
                    ListViewItem item = new ListViewItem
                    {
                        Content = col
                    };
                    item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                    list.Add(item);
                }
                this.lvlPersonal.ItemsSource = list;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlPersonal.ItemsSource);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtBuscador.Text) ||
            ((((Personal)(item as ListViewItem).Content).nombre.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(this.lvlPersonal.ItemsSource).Refresh();
        }
    }
}
