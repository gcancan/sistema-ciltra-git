﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using Core.Interfaces;


namespace WpfClient
{
    /// <summary>
    /// Interaction logic for editUnidadTransporte.xaml
    /// </summary>
    public partial class editUnidadTransporte : ViewBase
    {
        public editUnidadTransporte()
        {
            InitializeComponent();
        }

        public override OperationResult guardar()
        {

            UnidadTransporte uT = mapForm();
            if (uT != null)
            {
                //OperationResult svc = new UnidadTransporteSvc().saveUnidadTransporte(uT);
                //var X =  guardarSvc(uT);
                //return ((OperationResult)X);
                //if (svc.typeResult == ResultTypes.success)
                //{
                //    uT.idUnidadTransporte = svc.identity;
                //    mapForm(uT);
                //    //gridSecundario.IsEnabled = false;
                //    UnidadTransporte s = (UnidadTransporte)svc.result;
                //    return svc;
                //    //MessageBox.Show(string.Format(svc.mensaje), "Correcto", MessageBoxButton.OK, MessageBoxImage.Information);
                //}
                //else
                //{
                //    return svc;
                //    //MessageBox.Show(string.Format(svc.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                //}
                return base.guardar();
            }
            else
            {
                return new OperationResult { mensaje = "Error al construir el objeto" };
                //MessageBox.Show(string.Format("Error al construir el objeto"), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        //public async Task<OperationResult> guardarSvc(UnidadTransporte u)
        //{
            //try
            //{

            //    using (var client = new HttpClient())
            //    {
            //        client.BaseAddress = new Uri("http://localhost:56543/");
            //        client.DefaultRequestHeaders.Accept.Clear();
            //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //        var postData = new List<KeyValuePair<string, string>>();
            //        postData.Add(new KeyValuePair<string, string>("idUnidadTransporte", u.idUnidadTransporte.ToString()));
            //        postData.Add(new KeyValuePair<string, string>("placas", u.placas.ToString()));
            //        //postData.Add(new KeyValuePair<string, string>("idUnidadTransporte", u.idUnidadTransporte.ToString()));
            //        //postData.Add(new KeyValuePair<string, string>("idUnidadTransporte", u.idUnidadTransporte.ToString()));
            //        //postData.Add(new KeyValuePair<string, string>("idUnidadTransporte", u.idUnidadTransporte.ToString()));
            //        //postData.Add(new KeyValuePair<string, string>("idUnidadTransporte", u.idUnidadTransporte.ToString()));
            //        //postData.Add(new KeyValuePair<string, string>("idUnidadTransporte", u.idUnidadTransporte.ToString()));
            //        //postData.Add(new KeyValuePair<string, string>("idUnidadTransporte", u.idUnidadTransporte.ToString()));


            //        var content = new FormUrlEncodedContent(postData);
            //        HttpResponseMessage response = await client.PostAsync("api/UnidadTransporte/insertUnidadTransporte/", content);
            //        //if (response.IsSuccessStatusCode)
            //        //{
            //        string product = await response.Content.ReadAsStringAsync();
            //        //var s = new Newtonsoft.Json.JsonConverterJsonConvert();
            //        response.EnsureSuccessStatusCode();
            //        OperationResult resp = Newtonsoft.Json.JsonConvert.DeserializeObject<OperationResult>(product);

            //        return resp;
            //        //}
            //    }
            //}
            //catch (Exception w)
            //{
            //    return new OperationResult { valor = 1, mensaje = w.Message, result = null };
            //}
        //}


        public override OperationResult eliminar()
    {

            //UnidadTransporte uT = mapForm();
            //if (uT != null)
            //{
            //    OperationResult svc = new UnidadTransporteSvc().deleteUnidadTransporte(uT);

            //    if (svc.typeResult == ResultTypes.success)
            //    {
            //        uT.idUnidadTransporte = null;
            //        mapForm(null);
            //        //gridSecundario.IsEnabled = true;
            //        base.nuevo();
            //        return svc;
            //        //MessageBox.Show(string.Format(svc.mensaje), "Correcto", MessageBoxButton.OK, MessageBoxImage.Information);
            //    }
            //    else
            //    {
            //        return svc;
            //        //MessageBox.Show(string.Format(svc.mensaje), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //    }
            //}
            //else
            //{
            //    return new OperationResult { mensaje = "Error al construir el objeto" };
            //    //MessageBox.Show(string.Format("Error al construir el objeto"), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            //}
            return base.eliminar();
    }

    public override void nuevo()
    {
        mapForm(null);
        base.nuevo();
    }

    public override void buscar()
    {
        selectUnidadTransporte ventana = new selectUnidadTransporte();
        var result = ventana.selectUnidadTransporteFind();
        if (result != null)
        {
            mapForm(result);
            base.guardar();
        }
    }

    private UnidadTransporte mapForm()
    {
        try
        {
            return new UnidadTransporte
            {
                //idUnidadTransporte = txtId.Text == "" ? 0 : Convert.ToInt32(txtId.Text),
                //placas = txtPlacas.Text,
                //modelo = Convert.ToInt32(txtModelo.Text),
                //numSerie = txtNoSerei.Text,
                ////marca = txtMarca.Text,
                //tipoMotor = txtTipoMotor.Text,
                //tipoCaja = txtTipoCaja.Text,
                //diferencialInter = txtDiferencialInter.Text,
                //pasoDiferencialInter = txtPasoDiferencialInter.Text,
                //diferencialMotriz = txtDiferencialMotriz.Text,
                //pasoDiferencialMotriz = txtPasoDiferencialMotriz.Text,
                //noMotor = txtNoMotor.Text,
                //vin = txtVIN.Text,
                //serieECM = txtserieECM.Text,
                //RFID = txtRFID.Text
            };
        }
        catch (Exception)
        {
            return null;
        }
    }
    private void mapForm(UnidadTransporte uT)
    {
        try
        {
            if (uT != null)
            {
                //txtId.Text = uT.idUnidadTransporte.ToString();
                //txtPlacas.Text = uT.placas.ToString();
                //txtModelo.Text = uT.modelo.ToString();
                //txtNoSerei.Text = uT.numSerie;
                //txtMarca.Text = uT.marca;
                //txtTipoMotor.Text = uT.tipoMotor;
                //txtTipoCaja.Text = uT.tipoCaja;
                //txtDiferencialInter.Text = uT.diferencialInter;
                //txtPasoDiferencialInter.Text = uT.pasoDiferencialInter;
                //txtDiferencialMotriz.Text = uT.pasoDiferencialMotriz;
                //txtPasoDiferencialMotriz.Text = uT.pasoDiferencialMotriz;
                //txtNoMotor.Text = uT.noMotor;
                //txtVIN.Text = uT.vin;
                //txtserieECM.Text = uT.serieECM;
                //txtRFID.Text = uT.RFID;
            }
            else
            {
                txtId.Text = "";
                txtPlacas.Clear();
                txtModelo.Clear();
                txtNoSerei.Clear();
                txtMarca.Clear();
                txtTipoMotor.Clear();
                txtTipoCaja.Clear();
                txtDiferencialInter.Clear();
                txtPasoDiferencialInter.Clear();
                txtDiferencialMotriz.Clear();
                txtPasoDiferencialMotriz.Clear();
                txtNoMotor.Clear();
                txtVIN.Clear();
                txtserieECM.Clear();
                txtRFID.Clear();
                gridSecundario.IsEnabled = true;
            }

        }
        catch (Exception e)
        {
            MessageBox.Show(string.Format(e.Message), "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }



    private void ViewBase_Loaded(object sender, RoutedEventArgs e)
    {
        base.nuevo();
    }
}
}
