﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using Core.Models;
using Core.Interfaces;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editAbrirNivelesTanque.xaml
    /// </summary>
    public partial class editAbrirNivelesTanque : ViewBase
    {
        public editAbrirNivelesTanque()
        {
            InitializeComponent();
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            nuevo();
            ctrZonasOperativas.cbxZonaOpetativa.SelectionChanged += CbxZonaOpetativa_SelectionChanged;
            ctrZonasOperativas.cargarControl(mainWindow.zonaOperativa, mainWindow.usuario.idUsuario);
        }
        public override void nuevo()
        {
            base.nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
        }
        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            txtClave.valor = 0;
            txtClave.Tag = null;
            txtFechaFin.Clear();
            txtFechaInicio.Clear();
            txtPersonalInicia.Clear();
            txtPersonalTermina.Clear();
            ctrDespachado.valor = 0;

            if (ctrZonasOperativas.zonaOperativa != null)
            {
                getNivelesByZonaOperativa(ctrZonasOperativas.zonaOperativa);
            }
        }

        private void getNivelesByZonaOperativa(ZonaOperativa zonaOperativa)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new NivelesTanqueSvc().getNivelTanqueByZonaOperativa(zonaOperativa);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapNivelTanque(resp.result as NivelesTanque);
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    buscarTanqueCombustible(zonaOperativa);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapNivelTanque(NivelesTanque nivelesTanque)
        {
            if (nivelesTanque != null)
            {
                txtClave.Tag = nivelesTanque;
                txtClave.valor = nivelesTanque.idNivel;
                txtFechaInicio.Text = nivelesTanque.fechaInicio.ToString("dd/MM/yyyy HH:mm:ss");
                txtFechaFin.Text = nivelesTanque.fechaFinal == null ? string.Empty : nivelesTanque.fechaFinal.Value.ToString("dd/MM/yyyy HH:mm:ss");
                mapTanqueCombustible(nivelesTanque.tanqueCombustible, false);
                stpDetallesNiveles.Children.Clear();
                foreach (var item in nivelesTanque.listaDetalles)
                {
                    mapListaDetalleNiveles(item);
                }
                var respPer = new OperadorSvc().getPersonalALLorById(nivelesTanque.idPersonaAbre);
                if (respPer.typeResult == ResultTypes.success)
                {
                    txtPersonalInicia.Text = (respPer.result as List<Personal>)[0].nombre;
                }
                if (nivelesTanque.idPersonaCierra != null)
                {
                    var respPerCierra = new OperadorSvc().getPersonalALLorById(nivelesTanque.idPersonaCierra.Value);
                    if (respPer.typeResult == ResultTypes.success)
                    {
                        txtPersonalTermina.Text = (respPerCierra.result as List<Personal>)[0].nombre;
                    }
                }
                else
                {
                    txtPersonalTermina.Clear();
                }
                getResumenCarga(nivelesTanque.fechaInicio, (nivelesTanque.fechaFinal == null ? DateTime.Now : (DateTime)nivelesTanque.fechaFinal));

                var suma = nivelesTanque.listaDetalles.Sum(s => s.nivelFinal - s.nivelInicial);
                ctrDespachado.valor = suma.Value;

                if (nivelesTanque.fechaFinal == null)
                {
                    btnAbrir.IsEnabled = false;
                    btnCerrar.IsEnabled = true;
                    imaSemaforoVerde.Visibility = Visibility.Visible;
                    imaSemaforoRojo.Visibility = Visibility.Hidden;
                }
                else
                {
                    btnAbrir.IsEnabled = true;
                    btnCerrar.IsEnabled = false;
                    imaSemaforoVerde.Visibility = Visibility.Hidden;
                    imaSemaforoRojo.Visibility = Visibility.Visible;
                }

            }
            else
            {
                txtClave.Tag = null;
                txtClave.valor = 0;
                txtFechaInicio.Clear();
                ctrDespachado.valor = 0;
                txtFechaFin.Clear();
                mapTanqueCombustible(null);
                stpDetallesNiveles.Children.Clear();
                txtPersonalInicia.Clear();
                txtPersonalTermina.Clear();
                llenarListas(new List<ResumenCargasInternas>());
                lvlCargas.ItemsSource = null;
            }
        }

        private void getResumenCarga(DateTime fechaInicio, DateTime fechaFinal)
        {
            var resp = new NivelesTanqueSvc().getResumenCargasInternas(fechaInicio, fechaFinal, ctrZonasOperativas.zonaOperativa.idZonaOperativa);
            if (resp.typeResult == ResultTypes.success)
            {
                llenarListas(resp.result as List<ResumenCargasInternas>);
            }
            else if (resp.typeResult != ResultTypes.recordNotFound)
            {
                ImprimirMensaje.imprimir(resp);
            }
            else
            {
                llenarListas(new List<ResumenCargasInternas>());
            }
        }
        List<ResumenCargasInternas> listResumen = new List<ResumenCargasInternas>();
        private void llenarListas(List<ResumenCargasInternas> list)
        {
            listResumen = list;
            lvlCargas.ItemsSource = null;

            lvlCargas.ItemsSource = list;
            CollectionView viewDet = (CollectionView)CollectionViewSource.GetDefaultView(lvlCargas.ItemsSource);
            PropertyGroupDescription groupDescriptionDet = new PropertyGroupDescription("empresa");
            viewDet.GroupDescriptions.Add(groupDescriptionDet);
            ctrSumaCargas.valor = list.Sum(s => s.lts);
        }

        private void buscarTanqueCombustible(ZonaOperativa zonaOperativa)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new TanqueCombustibleSvc().getTanqueByZonaOperativa(zonaOperativa);
                if (resp.typeResult == ResultTypes.success)
                {
                    //txtClave.Tag = null;
                    mapTanqueCombustible(resp.result as TanqueCombustible);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    mapNivelTanque(null);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void mapTanqueCombustible(TanqueCombustible tanqueCombustible, bool mapDet = true)
        {
            if (tanqueCombustible != null)
            {
                //txtClave.Tag = null;
                txtTanque.Text = tanqueCombustible.nombre;
                txtTanque.Tag = tanqueCombustible;

                if (mapDet)
                {
                    stpDetallesNiveles.Children.Clear();
                    foreach (BombasCombustible item in tanqueCombustible.listaBombas)
                    {
                        DetalleNivelesTanque detalle = new DetalleNivelesTanque
                        {
                            idDetalleNivel = 0,
                            idNivel = 0,
                            bombasCombustible = item,
                            nivelInicial = 0,
                            nivelFinal = null
                        };
                        mapListaDetalleNiveles(detalle);
                    }
                }

            }
            else
            {
                txtClave.Tag = null;
                txtTanque.Tag = null;
                txtTanque.Clear();
                stpDetallesNiveles.Children.Clear();
            }
        }

        void mapListaDetalleNiveles(DetalleNivelesTanque detalle)
        {
            try
            {
                Label lblDetalleNivel = new Label() { Tag = detalle, FontWeight = FontWeights.Medium };
                StackPanel stpLbl = new StackPanel { Orientation = Orientation.Horizontal };

                Label lblNumBomba = new Label() { Margin = new Thickness(2), Width = 80, HorizontalContentAlignment = HorizontalAlignment.Center, Content = detalle.bombasCombustible.numero };
                stpLbl.Children.Add(lblNumBomba);

                controlDecimal ctrNivelInicial = new controlDecimal() { Name = "ctrNivelInicial", valor = detalle.nivelInicial, Width = 120, Margin = new Thickness(2), IsEnabled = detalle.nivelInicial == 0 };
                ctrNivelInicial.txtDecimal.Tag = lblDetalleNivel;
                ctrNivelInicial.txtDecimal.TextChanged += TxtDecimal_TextChangedNivelInicial;
                stpLbl.Children.Add(ctrNivelInicial);

                controlDecimal ctrFinal = new controlDecimal() { Name = "ctrFinal", Tag = lblDetalleNivel, Width = 120, valor = detalle.nivelFinal == null ? 0 : detalle.nivelFinal.Value, Margin = new Thickness(2), IsEnabled = detalle.nivelFinal == null };
                ctrFinal.txtDecimal.Tag = lblDetalleNivel;
                ctrFinal.txtDecimal.TextChanged += TxtDecimal_TextChanged;
                stpLbl.Children.Add(ctrFinal);

                controlDecimal ctrDespachado = new controlDecimal() { Name = "ctrDespachado", valor = detalle.totalDespachado, Width = 120, Margin = new Thickness(2), IsEnabled = false };
                stpLbl.Children.Add(ctrDespachado);

                lblDetalleNivel.Content = stpLbl;
                stpDetallesNiveles.Children.Add(lblDetalleNivel);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void TxtDecimal_TextChangedNivelInicial(object sender, TextChangedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            Label label = txt.Tag as Label;
            actualizarNivelInicial(label);
        }

        private void TxtDecimal_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            Label label = txt.Tag as Label;
            actualizarNivelFinal(label);
        }

        private void actualizarNivelFinal(Label label)
        {
            List<object> lista = (label.Content as StackPanel).Children.Cast<object>().ToList();
            decimal valorFinal = 0;
            foreach (object item in lista)
            {
                if (item is controlDecimal)
                {
                    var ctr = (item as controlDecimal);
                    if (ctr.Name == "ctrFinal")
                    {
                        valorFinal = ctr.valor;
                        (label.Tag as DetalleNivelesTanque).nivelFinal = valorFinal;
                    }
                    else if (ctr.Name == "ctrDespachado")
                    {
                        decimal despachado = (label.Tag as DetalleNivelesTanque).totalDespachado;
                        if (despachado > 0)
                        {
                            ctr.txtDecimal.Text = despachado.ToString();
                        }
                        else
                        {
                            ctr.txtDecimal.Text = "0";
                        }
                    }
                }
            }
            actualizarDespacho();
        }

        private void actualizarDespacho()
        {
            ctrDespachado.valor = getListaDetallesNivelTanque().Sum(s => s.totalDespachado);
        }

        private void actualizarNivelInicial(Label label)
        {
            List<object> lista = (label.Content as StackPanel).Children.Cast<object>().ToList();
            decimal valor = 0;
            foreach (object item in lista)
            {
                if (item is controlDecimal)
                {
                    var ctr = (item as controlDecimal);
                    if (ctr.Name == "ctrNivelInicial")
                    {
                        valor = ctr.valor;
                        break;
                    }
                }
            }
            (label.Tag as DetalleNivelesTanque).nivelInicial = valor;
        }

        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                var val = validar();
                if (val.typeResult != ResultTypes.success)
                {
                    return val;
                }
                
                NivelesTanque nivelesTanque = mapForm();

                List<DetalleNivelesTanque> lista = getListaDetallesNivelTanque();
                //foreach (var item in lista)
                //{
                //    if (item.nivelFinal < item.nivelInicial)
                //    {
                //        return new OperationResult(ResultTypes.warning, "LOS NIVELES FINALES NO PUEDEN SER MENOR A LOS NIVELES INICIALES");
                //    }
                //}

                if (nivelesTanque != null)
                {
                    var resp = new NivelesTanqueSvc().saveNivelTanques(ref nivelesTanque);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mainWindow.habilitar = true;
                       
                        if (nivelesTanque.fechaFinal != null)
                        {
                            bool imp = new ReportView().imprimirCierreContadores(nivelesTanque, listResumen);
                        }
                        mapNivelTanque(nivelesTanque);
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LA INFORMACIÓN DE LA PANTALLA");
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        OperationResult validar()
        {
            OperationResult validacion = new OperationResult(ResultTypes.success, "");
            if (txtTanque.Tag == null)
            {
                validacion.valor = 2;
                validacion.mensaje += "NO SE ESPECIFICO EL TANQUE" + Environment.NewLine;
            }
            var lista = getListaDetallesNivelTanque();
            if (txtClave.Tag == null)
            {
                if (lista.FindAll(s => s.nivelInicial == 0).Count != 0)
                {
                    validacion.valor = 2;
                    validacion.mensaje += "LOS CONTADORES INICIALES NO PUEDES SER 0" + Environment.NewLine;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(txtFechaFin.Text))
                {
                    var con0 = lista.FindAll(s => (s.nivelFinal == null ? 0 : s.nivelFinal) <= 0);
                    if (con0.Count > 0)
                    {
                        validacion.valor = 2;
                        validacion.mensaje += "LOS CONTADORES FINALES SON OBLIGATORIOS" + Environment.NewLine;
                    }

                    if (lista.FindAll(s => (s.nivelFinal == null ? 0 : s.nivelFinal) < s.nivelInicial).Count > 0)
                    {
                        validacion.valor = 2;
                        validacion.mensaje += "LOS CONTADORES FINALES NO PUEDEN SER MENORES A LOS INICIALES" + Environment.NewLine;
                    }

                    if (lista.FindAll(s => s.totalDespachado <= 0).Count != 0)
                    {
                        //validacion.valor = 2;
                        //validacion.mensaje += "EL TOTAL DESPACHADO NO PUEDE SER 0" + Environment.NewLine;
                    }
                }

            }
            return validacion;
        }
        NivelesTanque mapForm()
        {
            try
            {
                if (txtClave.Tag == null)
                {
                    NivelesTanque nivelesTanque = new NivelesTanque
                    {
                        idNivel = 0,
                        tanqueCombustible = txtTanque.Tag as TanqueCombustible,
                        fecha = DateTime.Now,
                        fechaInicio = DateTime.Now,
                        fechaFinal = null,
                        idPersonaAbre = mainWindow.usuario.personal.clave,
                        idPersonaCierra = null,

                    };
                    nivelesTanque.listaDetalles = new List<DetalleNivelesTanque>();
                    nivelesTanque.listaDetalles = getListaDetallesNivelTanque();
                    return nivelesTanque;
                }
                else
                {
                    NivelesTanque nivelesTanque = txtClave.Tag as NivelesTanque;
                    if (nivelesTanque.fechaFinal == null)
                    {
                        nivelesTanque.fechaFinal = DateTime.Now;
                        nivelesTanque.idPersonaCierra = mainWindow.usuario.personal.clave;
                        nivelesTanque.listaDetalles = getListaDetallesNivelTanque();
                        return nivelesTanque;
                    }
                    else
                    {
                        NivelesTanque nivelesTanqueNuevo = new NivelesTanque
                        {
                            idNivel = 0,
                            tanqueCombustible = txtTanque.Tag as TanqueCombustible,
                            fecha = DateTime.Now,
                            fechaInicio = DateTime.Now,
                            fechaFinal = null,
                            idPersonaAbre = mainWindow.usuario.personal.clave,
                            idPersonaCierra = null,

                        };
                        nivelesTanqueNuevo.listaDetalles = new List<DetalleNivelesTanque>();

                        foreach (var item in getListaDetallesNivelTanque())
                        {
                            nivelesTanqueNuevo.listaDetalles.Add(new DetalleNivelesTanque
                            {
                                idDetalleNivel = 0,
                                bombasCombustible = item.bombasCombustible,
                                idNivel = 0,
                                nivelInicial = item.nivelFinal.Value,
                                nivelFinal = null
                            });
                        }

                        return nivelesTanqueNuevo;
                    }

                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private List<DetalleNivelesTanque> getListaDetallesNivelTanque()
        {
            if (stpDetallesNiveles.Children.Count == 0)
            {
                return null;
            }
            else
            {
                List<DetalleNivelesTanque> lista = new List<DetalleNivelesTanque>();
                List<Label> list = stpDetallesNiveles.Children.Cast<Label>().ToList();
                foreach (Label item in list)
                {
                    lista.Add(item.Tag as DetalleNivelesTanque);
                }
                return lista;
            }
        }

        private void BtnAbrir_Click(object sender, RoutedEventArgs e)
        {
            ImprimirMensaje.imprimir(guardar());
        }

        private void BtnCerrar_Click(object sender, RoutedEventArgs e)
        {
          ImprimirMensaje.imprimir(  guardar());
        }
    }
}
