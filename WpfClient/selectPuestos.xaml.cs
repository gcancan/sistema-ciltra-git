﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using Core.Interfaces;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectPuestos.xaml
    /// </summary>
    public partial class selectPuestos : Window
    {
        public selectPuestos()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
        public Puesto obtenerPuesto()
        {
            try
            {
                getAllPuestos();
                bool? result = ShowDialog();
                if (result.Value && lvlPuestos.SelectedItem != null)
                {
                    return (lvlPuestos.SelectedItem as ListViewItem).Content as Puesto;
                }
                return null;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }            
        }
        private void getAllPuestos()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new PuestoSvc().getALLPuestosById();
                if (resp.typeResult == ResultTypes.success)
                {
                    llenarLista(resp.result as List<Puesto>);
                }
                else
                {
                    imprimir(resp);
                    IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void llenarLista(List<Puesto> listaPuesto)
        {
            try
            {
                lvlPuestos.ItemsSource = null;
                List<ListViewItem> list = new List<ListViewItem>();
                foreach (Puesto puesto in listaPuesto)
                {
                    ListViewItem item = new ListViewItem
                    {
                        Content = puesto
                    };
                    item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                    list.Add(item);
                }
                this.lvlPuestos.ItemsSource = list;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlPuestos.ItemsSource);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }
        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtBuscador.Text) ||
            ((((Puesto)(item as ListViewItem).Content).nombrePuesto.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }
        private void txtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(this.lvlPuestos.ItemsSource).Refresh();
        }
    }
}
