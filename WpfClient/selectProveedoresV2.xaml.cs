﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectProveedoresV2.xaml
    /// </summary>
    public partial class selectProveedoresV2 : Window
    {
        public selectProveedoresV2()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
        public ProveedorCombustible getAllProveedor()
        {
            try
            {
                buscarTodosProveedores();
                bool? resp = ShowDialog();
                if (resp.Value && lvlProveedores.SelectedItem != null)
                {
                    return (lvlProveedores.SelectedItem as ListViewItem).Content as ProveedorCombustible;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        void buscarTodosProveedores()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new ProveedorSvc().getAllProveedoresV2();
                if (resp.typeResult == ResultTypes.success)
                {
                    mapListaProveedores(resp.result as List<ProveedorCombustible>);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapListaProveedores(List<ProveedorCombustible> listaProveedores)
        {
            try
            {
                lvlProveedores.ItemsSource = null;
                List<ListViewItem> list = new List<ListViewItem>();
                foreach (ProveedorCombustible pro in listaProveedores)
                {
                    ListViewItem item = new ListViewItem
                    {
                        Content = pro
                    };
                    item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                    list.Add(item);
                }
                this.lvlProveedores.ItemsSource = list;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlProveedores.ItemsSource);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private bool UserFilter(object item) =>
           (string.IsNullOrEmpty(this.txtBuscador.Text) ||
           ((((ProveedorCombustible)(item as ListViewItem).Content).nombre.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(this.lvlProveedores.ItemsSource).Refresh();
        }
    }
}
