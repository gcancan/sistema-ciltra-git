﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Runtime.InteropServices;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Markup;

    public partial class ReportesPersonalizadosViajes : Window, IComponentConnector
    {
        private List<Viaje> listCartaPorte;
        private Cliente cliente;
        private Usuario usuario;
        private DateTime fechaInicio;
        private DateTime fechaFin;
        private bool mostrarImportes;

        public ReportesPersonalizadosViajes()
        {
            this.listCartaPorte = new List<Viaje>();
            this.cliente = new Cliente();
            this.usuario = new Usuario();
            this.mostrarImportes = false;
            this.InitializeComponent();
        }

        public ReportesPersonalizadosViajes(List<Viaje> listCartaPorte, Usuario usuario)
        {
            this.listCartaPorte = new List<Viaje>();
            this.cliente = new Cliente();
            this.usuario = new Usuario();
            this.mostrarImportes = false;
            this.InitializeComponent();
            this.listCartaPorte = listCartaPorte;
            this.usuario = usuario;
        }

        public ReportesPersonalizadosViajes(List<Viaje> listCartaPorte, Usuario usuario, Cliente cliente, DateTime fechaInicio, DateTime fechaFin, bool mostrarImportes = false)
        {
            this.listCartaPorte = new List<Viaje>();
            this.cliente = new Cliente();
            this.usuario = new Usuario();
            this.mostrarImportes = false;
            this.InitializeComponent();
            this.listCartaPorte = listCartaPorte;
            this.cliente = cliente;
            this.usuario = usuario;
            this.fechaInicio = fechaInicio;
            this.fechaFin = fechaFin;
            this.mostrarImportes = mostrarImportes;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtNamePerfil.Text.Trim()))
            {
                PerfilReporte perfilReporte = new PerfilReporte
                {
                    nombrePerfil = this.txtNamePerfil.Text.Trim(),
                    usuraio = this.usuario,
                    tipoReporte = enumPerfilRporte.VIAJES,
                    listDetalles = this.mapListaDetalles()
                };
                if (perfilReporte.listDetalles.Count == 0)
                {
                    MessageBox.Show("Se requiere al menos una opci\x00f3n", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
                else
                {
                    OperationResult result = new PerfilReporteSvc().savePerfilReporte(ref perfilReporte);
                    if (result.typeResult == ResultTypes.error)
                    {
                        MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
                    }
                    else if (result.typeResult == ResultTypes.warning)
                    {
                        MessageBox.Show(result.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                    else if (result.typeResult == ResultTypes.success)
                    {
                        MessageBox.Show(result.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                        this.cbxPerfiles.Items.Add((PerfilReporte)result.result);
                    }
                }
            }
            else
            {
                MessageBox.Show("Se requiere del nombre del perfil", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void cbxPerfiles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.chxFolio.IsChecked = false;
            this.chxNoViaje.IsChecked = false;
            this.chxChofer.IsChecked = false;
            this.chxTC.IsChecked = false;
            this.chxFechaViaje.IsChecked = false;
            this.chxFechaFin.IsChecked = false;
            this.chxTolva1.IsChecked = false;
            this.chxTolvar2.IsChecked = false;
            this.chxKM.IsChecked = false;
            this.chxToneladas.IsChecked = false;
            this.chxIva.IsChecked = false;
            this.chxRetencion.IsChecked = false;
            this.chxImporte.IsChecked = false;
            this.chxTotal.IsChecked = false;
            this.chxTipoViaje.IsChecked = false;
            this.chxZona.IsChecked = false;
            this.chxDestino.IsChecked = false;
            this.chxPrecio.IsChecked = false;
            this.chcOrden.IsChecked = false;
            this.chcProducto.IsChecked = false;
            if (this.cbxPerfiles.SelectedItem != null)
            {
                PerfilReporte selectedItem = (PerfilReporte)this.cbxPerfiles.SelectedItem;
                this.mapPantalla(selectedItem);
            }
        }

        public void generar()
        {
            try
            {
                base.Cursor = Cursors.Wait;
                if (this.chxAgrupado.IsChecked.Value ? this.generarReportePersonalizadoDetallado() : this.generarReportePersonalizado())
                {
                    MessageBox.Show("Se creo correctamente el reporte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                }
                else
                {
                    MessageBox.Show("Ocurrio un error al intentar crear el reporte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Hand);
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }

        private void Generar_Click(object sender, RoutedEventArgs e)
        {
            this.generar();
        }

        public bool generarReportePersonalizado()
        {
            try
            {
                bool flag = new ReportView().documentoReporteViajesPersonalizado(this.listCartaPorte, this.chxFolio.IsChecked.Value, this.chxNoViaje.IsChecked.Value, this.chxChofer.IsChecked.Value, this.chxTC.IsChecked.Value, this.chxTolva1.IsChecked.Value, this.chxTolvar2.IsChecked.Value, this.chxFechaViaje.IsChecked.Value, this.chxFechaFin.IsChecked.Value, this.chxKM.IsChecked.Value, this.chxToneladas.IsChecked.Value, this.chxIva.IsChecked.Value, this.chxRetencion.IsChecked.Value, this.chxImporte.IsChecked.Value, this.chxTotal.IsChecked.Value, this.chxTipoViaje.IsChecked.Value, this.fechaInicio, this.fechaFin, this.cliente, this.chxZona.IsChecked.Value, this.chxCliente.IsChecked.Value);
                return (flag && flag);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                return false;
            }
        }

        public bool generarReportePersonalizadoDetallado()
        {
            bool flag3;
            try
            {
                bool flag = new ReportView().documentoReporteViajesPersonalizadosDetallado(this.listCartaPorte, this.chxFolio.IsChecked.Value, this.chxNoViaje.IsChecked.Value, this.chxChofer.IsChecked.Value, this.chxTC.IsChecked.Value, this.chxTolva1.IsChecked.Value, this.chxTolvar2.IsChecked.Value, this.chxFechaViaje.IsChecked.Value, this.chxFechaFin.IsChecked.Value, this.chxKM.IsChecked.Value, this.chxToneladas.IsChecked.Value, this.chxIva.IsChecked.Value, this.chxRetencion.IsChecked.Value, this.chxImporte.IsChecked.Value, this.chxTotal.IsChecked.Value, this.chxTipoViaje.IsChecked.Value, this.chxDestino.IsChecked.Value, this.fechaInicio, this.fechaFin, this.cliente, this.chxZona.IsChecked.Value, this.chxCliente.IsChecked.Value, this.chxPrecio.IsChecked.Value, this.chcOrden.IsChecked.Value, this.chcProducto.IsChecked.Value, false, decimal.Zero, decimal.Zero);
                if (flag)
                {
                    return flag;
                }
                flag3 = false;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                flag3 = false;
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
            return flag3;
        }
        private void mapComboPerfiles(List<PerfilReporte> list)
        {
            foreach (PerfilReporte reporte in list)
            {
                this.cbxPerfiles.Items.Add(reporte);
            }
        }
        private List<DetallesPerfilReporte> mapListaDetalles()
        {
            List<DetallesPerfilReporte> list = new List<DetallesPerfilReporte>();
            if (this.chxFolio.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "FOLIO"
                };
                list.Add(item);
            }
            if (this.chxNoViaje.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "NOVIAJE "
                };
                list.Add(item);
            }
            if (this.chxChofer.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "CHOFER"
                };
                list.Add(item);
            }
            if (this.chxTC.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "TC"
                };
                list.Add(item);
            }
            if (this.chxFechaViaje.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "FECHAVIAJE"
                };
                list.Add(item);
            }
            if (this.chxFechaFin.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "FECHAFIN"
                };
                list.Add(item);
            }
            if (this.chxTolva1.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "TOLVA1"
                };
                list.Add(item);
            }
            if (this.chxTolvar2.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "TOLVA2"
                };
                list.Add(item);
            }
            if (this.chxKM.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "KM"
                };
                list.Add(item);
            }
            if (this.chxToneladas.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "TONELADAS"
                };
                list.Add(item);
            }
            if (this.chxIva.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "IVA"
                };
                list.Add(item);
            }
            if (this.chxRetencion.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "RETENCION"
                };
                list.Add(item);
            }
            if (this.chxImporte.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "IMPORTE"
                };
                list.Add(item);
            }
            if (this.chxTotal.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "TOTAL"
                };
                list.Add(item);
            }
            if (this.chxTipoViaje.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "TIPOVIAJE"
                };
                list.Add(item);
            }
            if (this.chxZona.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "ZONA"
                };
                list.Add(item);
            }
            if (this.chxDestino.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "DESTINO"
                };
                list.Add(item);
            }
            if (this.chxPrecio.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "PRECIO"
                };
                list.Add(item);
            }
            if (this.chcOrden.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "ORDEN"
                };
                list.Add(item);
            }
            if (this.chcProducto.IsChecked.Value)
            {
                DetallesPerfilReporte item = new DetallesPerfilReporte
                {
                    nombreDetalle = "PRODUCTO"
                };
                list.Add(item);
            }
            return list;
        }

        private void mapPantalla(PerfilReporte perfilReporte)
        {

            foreach (var item in perfilReporte.listDetalles)
            {
                switch (item.nombreDetalle)
                {
                    case "FOLIO":
                        chxFolio.IsChecked = true;
                        continue;
                    case "NOVIAJE":
                        chxNoViaje.IsChecked = true;
                        continue;
                    case "CHOFER":
                        chxChofer.IsChecked = true;
                        continue;
                    case "TC":
                        chxTC.IsChecked = true;
                        continue;
                    case "FECHAVIAJE":
                        chxFechaViaje.IsChecked = true;
                        continue;
                    case "FECHAFIN":
                        chxFechaFin.IsChecked = true;
                        continue;
                    case "TOLVA1":
                        chxTolva1.IsChecked = true;
                        continue;
                    case "TOLVA2":
                        chxTolvar2.IsChecked = true;
                        continue;
                    case "KM":
                        chxKM.IsChecked = true;
                        continue;
                    case "TONELADAS":
                        chxToneladas.IsChecked = true;
                        continue;
                    case "IVA":
                        chxIva.IsChecked = true;
                        continue;
                    case "RETENCION":
                        chxRetencion.IsChecked = true;
                        continue;
                    case "IMPORTE":
                        chxImporte.IsChecked = true;
                        continue;
                    case "TOTAL":
                        chxTotal.IsChecked = true;
                        continue;
                    case "TIPOVIAJE":
                        chxTipoViaje.IsChecked = true;
                        continue;
                    case "ZONA":
                        chxZona.IsChecked = true;
                        continue;
                    case "DESTINO":
                        chxDestino.IsChecked = true;
                        continue;
                    case "PRECIO":
                        chxPrecio.IsChecked = true;
                        continue;
                    case "ORDEN":
                        chcOrden.IsChecked = true;
                        continue;
                    case "PRODUCTO":
                        chcProducto.IsChecked = true;
                        continue;
                }
            }
        }



        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            OperationResult result = new PerfilReporteSvc().getPerfilesReportes(this.usuario.idUsuario, enumPerfilRporte.VIAJES);
            if (result.typeResult == ResultTypes.error)
            {
                MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
            }
            else if (result.typeResult == ResultTypes.warning)
            {
                MessageBox.Show(result.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else if ((result.typeResult != ResultTypes.recordNotFound) && (result.typeResult == ResultTypes.success))
            {
                this.mapComboPerfiles((List<PerfilReporte>)result.result);
            }
            this.chxImporte.Visibility = this.mostrarImportes ? Visibility.Visible : Visibility.Collapsed;
            this.chxIva.Visibility = this.mostrarImportes ? Visibility.Visible : Visibility.Collapsed;
            this.chxRetencion.Visibility = this.mostrarImportes ? Visibility.Visible : Visibility.Collapsed;
            this.chxTotal.Visibility = this.mostrarImportes ? Visibility.Visible : Visibility.Collapsed;
            this.chxPrecio.Visibility = this.mostrarImportes ? Visibility.Visible : Visibility.Collapsed;
        }
    }
}
