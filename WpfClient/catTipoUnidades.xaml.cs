﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para catTipoUnidades.xaml
    /// </summary>
    public partial class catTipoUnidades : ViewBase
    {
        public catTipoUnidades()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ctrClave.txtEntero.IsReadOnly = true;
            ctrClave.txtEntero.Foreground = Brushes.SteelBlue;

            cbxNoEjes.Items.Add(1);
            cbxNoEjes.Items.Add(2);
            cbxNoEjes.Items.Add(3);
            cbxNoEjes.Items.Add(4);

            cbxNoRefacciones.Items.Add(0);
            cbxNoRefacciones.Items.Add(1);
            cbxNoRefacciones.Items.Add(2);
            cbxNoRefacciones.Items.Add(3);
            cbxNoRefacciones.SelectedIndex = 0;

            cbxClasificacion.Items.Add(enumClasificacionTipoUnidad.TRACTOR);
            cbxClasificacion.Items.Add(enumClasificacionTipoUnidad.DOLLY);
            cbxClasificacion.Items.Add(enumClasificacionTipoUnidad.REMOLQUE);
            cbxClasificacion.SelectedIndex = 0;

            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            cbxNoEjes.SelectedItem = null;
            mapForm(null);
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            cargarControles();
        }
        private void cbxNoEjes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cargarControles();
        }
        void cargarControles()
        {
            try
            {
                stpConfigEjes.Children.Clear();
                if (chcEjeDireccional.IsChecked.Value)
                {
                    DescripcionLlantas descripcion = new DescripcionLlantas
                    {
                        idDescripcionLlantas = 0,
                        noEje = 1,
                        tipoEjeLlanta = enumTipoEjeLlanta.DIRECCION,
                        cantLlantas = 2
                    };

                    StackPanel stpConfig = new StackPanel { Orientation = Orientation.Horizontal, Tag = descripcion };
                    Label lblEje = new Label
                    {
                        Content = "EJE 1:",
                        Width = 60,
                        Margin = new Thickness(2),
                        VerticalAlignment = VerticalAlignment.Center,
                        Foreground = Brushes.SteelBlue,
                        FontWeight = FontWeights.Bold
                    };
                    Label lblTipo = new Label
                    {
                        Content = "DIRECCIÓN",
                        Width = 180,
                        Margin = new Thickness(2),
                        VerticalAlignment = VerticalAlignment.Center,
                        Foreground = Brushes.SteelBlue,
                        FontWeight = FontWeights.Bold
                    };
                    Label lblNoLlantas = new Label
                    {
                        Content = "2",
                        Width = 180,
                        Margin = new Thickness(2),
                        VerticalAlignment = VerticalAlignment.Center,
                        Foreground = Brushes.SteelBlue,
                        FontWeight = FontWeights.Bold
                    };
                    stpConfig.Children.Add(lblEje);
                    stpConfig.Children.Add(lblTipo);
                    stpConfig.Children.Add(lblNoLlantas);

                    stpConfigEjes.Children.Add(stpConfig);
                }
                if (cbxNoEjes.SelectedItem != null)
                {
                    int noEjes = Convert.ToInt32(cbxNoEjes.SelectedItem);
                    for (int i = 1; i <= noEjes; i++)
                    {
                        DescripcionLlantas descripcion = new DescripcionLlantas
                        {
                            idDescripcionLlantas = 0,
                            noEje = (i + 1),
                            tipoEjeLlanta = enumTipoEjeLlanta.ARRASTRE,
                            cantLlantas = 2
                        };
                        StackPanel stpConfig = new StackPanel { Orientation = Orientation.Horizontal, Tag = descripcion };
                        Label lblEje = new Label
                        {
                            Content = string.Format("EJE {0}:", (i + 1).ToString()),
                            Width = 60,
                            Margin = new Thickness(2),
                            VerticalAlignment = VerticalAlignment.Center,
                            Foreground = Brushes.SteelBlue,
                            FontWeight = FontWeights.Bold
                        };

                        ComboBox cbxTipoEjeLlanta = new ComboBox
                        {
                            Height = 22,
                            Width = 180,
                            Margin = new Thickness(2),
                            FontWeight = FontWeights.Bold,
                            Tag = stpConfig
                        };
                        cbxTipoEjeLlanta.Items.Add(enumTipoEjeLlanta.ARRASTRE);
                        cbxTipoEjeLlanta.Items.Add(enumTipoEjeLlanta.TRACCION);
                        cbxTipoEjeLlanta.SelectedIndex = 0;
                        cbxTipoEjeLlanta.SelectionChanged += CbxTipoEjeLlanta_SelectionChanged;

                        ComboBox cbxNoLlanta = new ComboBox
                        {
                            Height = 22,
                            Width = 60,
                            Margin = new Thickness(2),
                            FontWeight = FontWeights.Bold,
                            Tag = stpConfig
                        };
                        cbxNoLlanta.Items.Add(2);
                        cbxNoLlanta.Items.Add(4);
                        cbxNoLlanta.Items.Add(6);
                        cbxNoLlanta.Items.Add(8);
                        cbxNoLlanta.SelectedIndex = 0;
                        cbxNoLlanta.SelectionChanged += CbxNoLlanta_SelectionChanged;

                        stpConfig.Children.Add(lblEje);
                        stpConfig.Children.Add(cbxTipoEjeLlanta);
                        stpConfig.Children.Add(cbxNoLlanta);

                        stpConfigEjes.Children.Add(stpConfig);
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        void cargarControles(List<DescripcionLlantas> listaDescripciones)
        {
            try
            {
                stpConfigEjes.Children.Clear();
                foreach (var descripcion in listaDescripciones)
                {
                    if (descripcion.tipoEjeLlanta == enumTipoEjeLlanta.DIRECCION)
                    {
                        StackPanel stpConfig = new StackPanel { Orientation = Orientation.Horizontal, Tag = descripcion };
                        Label lblEje = new Label
                        {
                            Content = "EJE 1:",
                            Width = 60,
                            Margin = new Thickness(2),
                            VerticalAlignment = VerticalAlignment.Center,
                            Foreground = Brushes.SteelBlue,
                            FontWeight = FontWeights.Bold
                        };
                        Label lblTipo = new Label
                        {
                            Content = "DIRECCIÓN",
                            Width = 180,
                            Margin = new Thickness(2),
                            VerticalAlignment = VerticalAlignment.Center,
                            Foreground = Brushes.SteelBlue,
                            FontWeight = FontWeights.Bold
                        };
                        Label lblNoLlantas = new Label
                        {
                            Content = "2",
                            Width = 180,
                            Margin = new Thickness(2),
                            VerticalAlignment = VerticalAlignment.Center,
                            Foreground = Brushes.SteelBlue,
                            FontWeight = FontWeights.Bold
                        };
                        stpConfig.Children.Add(lblEje);
                        stpConfig.Children.Add(lblTipo);
                        stpConfig.Children.Add(lblNoLlantas);

                        stpConfigEjes.Children.Add(stpConfig);
                    }
                    else
                    {

                        StackPanel stpConfig = new StackPanel { Orientation = Orientation.Horizontal, Tag = descripcion };
                        Label lblEje = new Label
                        {
                            Content = string.Format("EJE {0}:", (descripcion.noEje).ToString()),
                            Width = 60,
                            Margin = new Thickness(2),
                            VerticalAlignment = VerticalAlignment.Center,
                            Foreground = Brushes.SteelBlue,
                            FontWeight = FontWeights.Bold
                        };

                        ComboBox cbxTipoEjeLlanta = new ComboBox
                        {
                            Height = 22,
                            Width = 180,
                            Margin = new Thickness(2),
                            FontWeight = FontWeights.Bold,
                            Tag = stpConfig
                        };
                        cbxTipoEjeLlanta.Items.Add(enumTipoEjeLlanta.ARRASTRE);
                        cbxTipoEjeLlanta.Items.Add(enumTipoEjeLlanta.TRACCION);
                        cbxTipoEjeLlanta.SelectedItem = descripcion.tipoEjeLlanta;
                        cbxTipoEjeLlanta.SelectionChanged += CbxTipoEjeLlanta_SelectionChanged;

                        ComboBox cbxNoLlanta = new ComboBox
                        {
                            Height = 22,
                            Width = 60,
                            Margin = new Thickness(2),
                            FontWeight = FontWeights.Bold,
                            Tag = stpConfig
                        };
                        cbxNoLlanta.Items.Add(2);
                        cbxNoLlanta.Items.Add(4);
                        cbxNoLlanta.Items.Add(6);
                        cbxNoLlanta.Items.Add(8);
                        cbxNoLlanta.SelectedItem = descripcion.cantLlantas;
                        cbxNoLlanta.SelectionChanged += CbxNoLlanta_SelectionChanged;

                        stpConfig.Children.Add(lblEje);
                        stpConfig.Children.Add(cbxTipoEjeLlanta);
                        stpConfig.Children.Add(cbxNoLlanta);

                        stpConfigEjes.Children.Add(stpConfig);
                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void CbxNoLlanta_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender != null)
            {
                ComboBox cbxNollanta = sender as ComboBox;
                DescripcionLlantas descripcion = (cbxNollanta.Tag as StackPanel).Tag as DescripcionLlantas;
                descripcion.cantLlantas = (int)cbxNollanta.SelectedItem;
            }
        }
        private void CbxTipoEjeLlanta_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender != null)
            {
                ComboBox cbxTipo = sender as ComboBox;
                DescripcionLlantas descripcion = (cbxTipo.Tag as StackPanel).Tag as DescripcionLlantas;
                descripcion.tipoEjeLlanta = (enumTipoEjeLlanta)cbxTipo.SelectedItem;
            }
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;

                var val = validar();
                if (val.typeResult != ResultTypes.success)
                {
                    return val;
                }

                TipoUnidad tipoUnidad = mapForm();
                var resp = new TipoUnidadSvc().saveTipoUnidad(ref tipoUnidad);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(tipoUnidad);
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.editar | ToolbarCommands.cerrar);
                }
                return resp;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        OperationResult validar()
        {
            try
            {
                OperationResult result = new OperationResult { valor = 0, mensaje = ("PARA CONTINUAR NECESITA:" + Environment.NewLine) };

                if (string.IsNullOrEmpty(txtDescripcion.Text.Trim()))
                {
                    result.valor = 3;
                    result.mensaje += " * Proporcionar la descripción" + Environment.NewLine;
                }
                //if (ctrCargaMax.valor == 0)
                //{
                //    result.valor = 3;
                //    result.mensaje += " * Proporcionar la capacidad de carga" + Environment.NewLine;
                //}
                if (cbxClasificacion.SelectedItem == null)
                {
                    result.valor = 3;
                    result.mensaje += " * Seleccionar una clasificación" + Environment.NewLine;
                }

                if (getListaDescripcion().Count == 0)
                {
                    result.valor = 3;
                    result.mensaje += " * Especificar al menos un eje con sus llantas" + Environment.NewLine;
                }

                return result;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        private void mapForm(TipoUnidad tipoUnidad)
        {
            try
            {
                if (tipoUnidad != null)
                {
                    ctrClave.Tag = tipoUnidad;
                    ctrClave.valor = tipoUnidad.clave;
                    txtDescripcion.Text = tipoUnidad.descripcion;
                    ctrCargaMax.valor = tipoUnidad.TonelajeMax;
                    chcCombustible.IsChecked = tipoUnidad.bCombustible;
                    cbxClasificacion.SelectedItem = cbxClasificacion.Items.Cast<enumClasificacionTipoUnidad>().ToList().Find(s => s.ToString() == tipoUnidad.clasificacion);

                    var ejeDireccional = tipoUnidad.listaDescripcionLlantas.Find(s => s.tipoEjeLlanta == enumTipoEjeLlanta.DIRECCION);
                    chcEjeDireccional.IsChecked = ejeDireccional != null;

                    cbxNoEjes.SelectedItem = tipoUnidad.listaDescripcionLlantas.Count(s => s.tipoEjeLlanta != enumTipoEjeLlanta.DIRECCION);
                    cbxNoRefacciones.SelectedItem = tipoUnidad.cantRefacciones;

                    cargarControles(tipoUnidad.listaDescripcionLlantas);
                    chcActivo.IsChecked = tipoUnidad.activo;
                }
                else
                {
                    ctrClave.Tag = null;
                    ctrClave.valor = 0;
                    txtDescripcion.Clear();
                    ctrCargaMax.valor = 0;
                    chcCombustible.IsChecked = false;
                    cbxClasificacion.SelectedIndex = 0;
                    chcEjeDireccional.IsChecked = false;
                    cbxNoEjes.SelectedItem = null;
                    cbxNoRefacciones.SelectedIndex = 0;
                    chcActivo.IsChecked = true;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        TipoUnidad mapForm()
        {
            try
            {
                TipoUnidad tipoUnidad = new TipoUnidad
                {
                    clave = ctrClave.Tag == null ? 0 : (ctrClave.Tag as TipoUnidad).clave,
                    descripcion = txtDescripcion.Text.Trim(),
                    clasificacion = cbxClasificacion.SelectedItem.ToString(),
                    bCombustible = chcCombustible.IsChecked.Value,
                    TonelajeMax = ctrCargaMax.valor,
                    cantRefacciones = (int)cbxNoRefacciones.SelectedItem,
                    listaDescripcionLlantas = new List<DescripcionLlantas>(),
                    activo = chcActivo.IsChecked.Value
                };
                tipoUnidad.listaDescripcionLlantas = getListaDescripcion();

                tipoUnidad.NoEjes = tipoUnidad.listaDescripcionLlantas.Count().ToString();
                tipoUnidad.NoLlantas = tipoUnidad.listaDescripcionLlantas.Sum(s => s.cantLlantas).ToString();

                return tipoUnidad;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        List<DescripcionLlantas> getListaDescripcion()
        {
            List<DescripcionLlantas> list = new List<DescripcionLlantas>();
            foreach (var item in stpConfigEjes.Children.Cast<StackPanel>().ToList())
            {
                list.Add(item.Tag as DescripcionLlantas);
            }
            return list;
        }
        public override void buscar()
        {
            try
            {
                TipoUnidad tipoUnidad = new selectTipoUnidad().getAllTipoUnidad("");
                if (tipoUnidad != null)
                {
                    mapForm(tipoUnidad);
                    base.buscar();
                }
                else
                {
                    nuevo();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new TipoUnidadSvc().getAllTipoUnidades();
                if (resp.typeResult == ResultTypes.success)
                {
                    var lista = resp.result as List<TipoUnidad>;
                    new ReportView().exportarListaTipoUnidades(lista);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
