﻿using Core.Models;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para seleccionarMotivoCancelacion.xaml
    /// </summary>
    public partial class seleccionarMotivoCancelacion : Window
    {
        public seleccionarMotivoCancelacion()
        {
            InitializeComponent();
        }
        public seleccionarMotivoCancelacion(enumProcesos tipoProceso)
        {
            InitializeComponent();
            this.tipoProceso = tipoProceso;
            
        }
        enumProcesos tipoProceso;
        controlMotivosCancelacion ctrMotivo;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //ctrMotivo.cbxMotivos.SelectionChanged += CbxMotivos_SelectionChanged;
            //ctrMotivo.txtObservacion.TextChanged += TxtObservacion_TextChanged;
            ctrMotivo = new controlMotivosCancelacion(tipoProceso);
            stpMotivos.Children.Add(ctrMotivo);
        }

        private void TxtObservacion_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender != null)
            {
                TextBox txt = sender as TextBox;
                this.strMotivo = txt.Text;
            }
        }

        private void CbxMotivos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender != null)
            {
                ComboBox cbx = sender as ComboBox;
                MotivoCancelacion motivo = cbx.SelectedItem as MotivoCancelacion;
                this.motivo = motivo;
            }
        }

        private void BtnAceptar_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
        public OperationResult obtenerMotivo()
        {
            try
            {
                 bool? resp = ShowDialog();
                if (resp.Value)
                {
                    return new OperationResult() {result = ctrMotivo.motivoCancelacion, mensaje = ctrMotivo.obsMotivo, valor = 0 };
                }
                else
                {
                    return new OperationResult() { result = null, mensaje = "", valor = 3 };
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex); 
            }
        }
        public MotivoCancelacion motivo = null;
        public string strMotivo = string.Empty;
        private void BtnCancelar_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
      
    }
}
