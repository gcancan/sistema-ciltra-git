﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editPagoPorModalidad.xaml
    /// </summary>
    public partial class editPagoPorModalidad : ViewBase
    {
        public editPagoPorModalidad()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            cargarClientes(mainWindow.empresa.clave);
            nuevo();
        }
        public override void nuevo()
        {
            base.nuevo();
            mapForm(null);
        }
        private void cargarClientes(int clave)
        {
            OperationResult resp = new ClienteSvc().getClientesALLorById(clave, 0, true);
            if (resp.typeResult == ResultTypes.success)
            {
                cbxCliente.ItemsSource = resp.result as List<Cliente>;
            }
        }

        private void cbxCliente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbxCliente.SelectedItem != null)
            {
                Cliente cliente = (Cliente)cbxCliente.SelectedItem;
                buscarClasificaciones(cliente);
                buscarPagosPorModalidad(cliente);
            }
            else
            {
                llenarListaClasificaciones(new List<ClasificacionRutas>());
            }
        }

        private void buscarPagosPorModalidad(Cliente cliente)
        {
            var resp = new PagoPorModalidadSvc().getPagoModalidadByIdCliente(cliente.clave);
            if (resp.typeResult == ResultTypes.success)
            {
                llenarListaPagosModalidad(resp.result as List<PagoPorModalidad>);
            }
            else if (resp.typeResult == ResultTypes.recordNotFound)
            {
                llenarListaPagosModalidad(new List<PagoPorModalidad>());
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
                llenarListaPagosModalidad(new List<PagoPorModalidad>());
            }
        }

        private void llenarListaPagosModalidad(List<PagoPorModalidad> list)
        {
            lvlPagos.Items.Clear();
            foreach (var item in list)
            {
                addLista(item);
            }
        }

        private void addLista(PagoPorModalidad pago)
        {
            ListViewItem lvl = new ListViewItem();
            lvl.Content = pago;
            lvl.MouseDoubleClick += Lvl_MouseDoubleClick1; ; ;
            lvlPagos.Items.Add(lvl);
        }

        private void Lvl_MouseDoubleClick1(object sender, MouseButtonEventArgs e)
        {
            ListViewItem lvl = (ListViewItem)sender;
            PagoPorModalidad clasif = lvl.Content as PagoPorModalidad;
            mapForm(clasif);
        }

        void buscarClasificaciones(Cliente cliente)
        {
            var res = new ClasificacionRutasSvc().getClasificacionRutas(cliente.clave);
            if (res.typeResult == ResultTypes.success)
            {
                llenarListaClasificaciones(res.result as List<ClasificacionRutas>);
            }
            else if (res.typeResult == ResultTypes.recordNotFound)
            {
                llenarListaClasificaciones(new List<ClasificacionRutas>());
            }
            else 
            {
                ImprimirMensaje.imprimir(res);
                llenarListaClasificaciones(new List<ClasificacionRutas>());
            }
        }
        private void llenarListaClasificaciones(List<ClasificacionRutas> list)
        {
            lvlClasificaciones.Items.Clear();
            foreach (ClasificacionRutas item in list)
            {
                addLista(item);
            }
        }

        private void addLista(ClasificacionRutas clasificacion)
        {
            ListViewItem lvl = new ListViewItem();
            lvl.Content = clasificacion;
            lvl.MouseDoubleClick += Lvl_MouseDoubleClick; ;
            lvlClasificaciones.Items.Add(lvl);
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListViewItem lvl = (ListViewItem)sender;
            ClasificacionRutas clasif = lvl.Content as ClasificacionRutas;
            txtClasificacion.Tag = clasif;
            txtClasificacion.Text = clasif.clasificacion.ToString();
            txtClave.Clear();
            txtClave.Tag = null;
            ctrNoViaje.limpiar();
            ctr24.valor = 0;
            ctr30.valor = 0;
            ctr35.valor = 0;
            ctrFull.valor = 0;
            gridDatos.IsEnabled = true;
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
        }

        public override OperationResult guardar()
        {
            PagoPorModalidad pagoXmodailidad = mapForm();
            var resp = new PagoPorModalidadSvc().savePagoPorModalidad(ref pagoXmodailidad);
            if (resp.typeResult == ResultTypes.success)
            {
                mapForm(pagoXmodailidad);
                mainWindow.habilitar = true;
            }
            return resp;
        }
        public override OperationResult eliminar()
        {
            PagoPorModalidad pagoXmodailidad = mapForm();
            pagoXmodailidad.estatus = false;
            var resp = new PagoPorModalidadSvc().savePagoPorModalidad(ref pagoXmodailidad);
            if (resp.typeResult == ResultTypes.success)
            {
                mapForm(null);               
            }
            return resp;
        }

        private void mapForm(PagoPorModalidad pagoXmodailidad)
        {
            if (pagoXmodailidad != null)
            {
                txtClave.Tag = pagoXmodailidad;
                txtClave.Text = pagoXmodailidad.idPagoPorModalidad.ToString();
                txtClasificacion.Tag = pagoXmodailidad.clasificacion;
                txtClasificacion.Text = pagoXmodailidad.clasificacion.clasificacion.ToString();
                ctrNoViaje.valor = (int)pagoXmodailidad.noViaje;
                ctr24.valor = pagoXmodailidad.sencillo_24;
                ctr30.valor = pagoXmodailidad.sencillo_30;
                ctr35.valor = pagoXmodailidad.sencillo_35;
                ctrFull.valor = pagoXmodailidad.full;
                gridDatos.IsEnabled = false;
                mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.editar | ToolbarCommands.eliminar | ToolbarCommands.cerrar);
                buscarPagosPorModalidad(pagoXmodailidad.clasificacion.cliente);
            }
            else
            {
                txtClave.Tag = null;
                txtClave.Clear();
                txtClasificacion.Tag = null;
                txtClasificacion.Clear();
                ctrNoViaje.limpiar();
                ctr24.valor = 0;
                ctr30.valor = 0;
                ctr35.valor = 0;
                ctrFull.valor = 0;
                gridDatos.IsEnabled = true;
                mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            }
        }

        private PagoPorModalidad mapForm()
        {
            try
            {
                return new PagoPorModalidad
                {
                    idPagoPorModalidad = txtClave.Tag == null ? 0 : (txtClave.Tag as PagoPorModalidad).idPagoPorModalidad,
                    clasificacion = txtClasificacion.Tag as ClasificacionRutas,
                    noViaje = ctrNoViaje.valor,
                    sencillo_24 = ctr24.valor,
                    sencillo_30 = ctr30.valor,
                    sencillo_35 = ctr35.valor,
                    full = ctrFull.valor,
                    estatus = true
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
