﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Models;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para addComentarios.xaml
    /// </summary>
    public partial class addComentarios : MetroWindow
    {
        public addComentarios()
        {
            InitializeComponent();
        }
        string usuario = string.Empty;
        public ComentarioViaje addComentario(string usuario, int idViaje)
        {
            try
            {
                this.usuario = usuario;
                this.idViaje = idViaje;
                bool? resp = ShowDialog();
                if (resp.Value && comentario != null)
                {
                    return comentario;
                }
                return null;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }
        ComentarioViaje comentario = null;
        int idViaje = 0;
        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtComentario.Text.Trim())) return;
            try
            {
                Cursor = Cursors.Wait;
                comentario = new ComentarioViaje
                {
                    idViaje = this.idViaje,
                    comentario = txtComentario.Text.Trim(),
                    usuario = this.usuario,
                    fecha = DateTime.Now
                };
                var resp = new CartaPorteSvc().saveComentarioViajes(comentario.idViaje, comentario.usuario, comentario.comentario, comentario.fecha);
                if (resp.typeResult == ResultTypes.success)
                {
                    DialogResult = true;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

    }
}
