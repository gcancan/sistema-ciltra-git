﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using CoreFletera.Interfaces;
    using CoreFletera.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Runtime.CompilerServices;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Markup;

    public partial class editLlenadoCombustibleExtra : WpfClient.ViewBase
    {
        private bool afectaKardex = true;
        private decimal valorDisel = 0M;
        public editLlenadoCombustibleExtra()
        {
            this.InitializeComponent();
        }

        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.ctrZonaOperativa.zonaOperativa != null)
            {
                this.getNivelesByZonaOperativa(this.ctrZonaOperativa.zonaOperativa);
            }
        }

        private void chcExtra_Checked(object sender, RoutedEventArgs e)
        {
            this.limpiarExtra(true);
        }

        private void chcExtra_Unchecked(object sender, RoutedEventArgs e)
        {
            this.limpiarExtra(false);
        }

        private void ctrEnteros_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    base.Cursor = Cursors.Wait;
                    if (e.Key == Key.Enter)
                    {
                        OperationResult resp = new CargaCombustibleSvc().getCargaCombustibleExtra(this.ctrEnteros.valor);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            this.mapForm((CargaCombustibleExtra)resp.result);
                        }
                        else
                        {
                            ImprimirMensaje.imprimir(resp);
                        }
                    }
                }
                catch (Exception exception)
                {
                    ImprimirMensaje.imprimir(exception);
                }
                finally
                {
                    base.Cursor = Cursors.Arrow;
                }
            }
        }

        private void getNivelesByZonaOperativa(ZonaOperativa zonaOperativa)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult resp = new NivelesTanqueSvc().getNivelTanqueByZonaOperativa(zonaOperativa);
                NivelesTanque nivelesTanque = resp.result as NivelesTanque;
                if (resp.typeResult == ResultTypes.success)
                {
                    if (nivelesTanque.fechaFinal.HasValue)
                    {
                        ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "NO ESTAN ABIERTOS LOS NIVELES", null));
                        base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    }
                    else
                    {
                        this.mapNivelTanque(nivelesTanque);
                        base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
                    }
                }
                else
                {
                    this.mapNivelTanque(null);
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void getTiposCombustible()
        {
            try
            {
                OperationResult result = new TipoCombustibleSvc().getTiposCombustibleAllOrById(0);
                if (result.typeResult == ResultTypes.success)
                {
                    TipoCombustible combustible = ((List<TipoCombustible>)result.result).Find(s => s.tipoCombustible.ToUpper() == "DIESEL");
                    this.valorDisel = combustible.ultimoPrecio;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        public override OperationResult guardar()
        {
            OperationResult result2;
            try
            {
                base.Cursor = Cursors.Wait;
                CargaCombustibleExtra carga = this.mapForm();
                KardexCombustible kardexCombustible = null;
                if (this.afectaKardex)
                {
                    kardexCombustible = new KardexCombustible
                    {
                        idInventarioDiesel = 0,
                        cantidad = carga.ltCombustible.Value,
                        empresa = carga.unidadTransporte.empresa,
                        unidadTransporte = carga.unidadTransporte,
                        idOperacion = new int?(carga.idCargaCombustibleExtra),
                        tanque = (this.txtClave.Tag as NivelesTanque).tanqueCombustible,
                        personal = carga.despachador,
                        tipoMovimiento = eTipoMovimiento.SALIDA,
                        tipoOperacion = TipoOperacionDiesel.ANTICIPO
                    };
                }
                OperationResult result = new CargaCombustibleSvc().saveCargaCombustibleEXtra(ref carga, ref kardexCombustible);
                if (result.typeResult == ResultTypes.success)
                {
                    this.mapForm(carga);
                }
                result2 = result;
            }
            catch (Exception exception)
            {
                result2 = new OperationResult(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return result2;
        }
        private void limpiarExtra(bool valor)
        {
            this.gboxExtra.IsEnabled = valor;
            if (!valor)
            {
                this.txtTicket.Clear();
                this.txtProveedor.SelectedItem = null;
                this.txtPrecio.valor = decimal.Zero;
                this.txtFolioImpreso.Clear();
            }
        }

        private CargaCombustibleExtra mapForm()
        {
            try
            {
                if (this.ctrEnteros.Tag == null)
                {
                    return null;
                }
                CargaCombustibleExtra tag = this.ctrEnteros.Tag as CargaCombustibleExtra;
                tag.fechaCombustible = new DateTime?(DateTime.Now);
                tag.ltCombustible = new decimal?(this.ctrCantidad.valor);
                if (this.chcExtra.IsChecked.Value)
                {
                    tag.externo = true;
                    tag.folioImpreso = this.txtFolioImpreso.Text;
                    tag.ticket = this.txtTicket.Text;
                    tag.proveedor = this.txtProveedor.SelectedItem as ProveedorCombustible;
                    tag.precioCombustible = new decimal?(this.txtPrecio.valor);
                }
                else
                {
                    tag.externo = false;
                    tag.folioImpreso = string.Empty;
                    tag.ticket = string.Empty;
                    tag.proveedor = null;
                    tag.precioCombustible = new decimal?(this.valorDisel);
                }
                tag.despachador = this.ctrDespachador.personal;
                return tag;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void mapForm(CargaCombustibleExtra carga)
        {
            try
            {
                if (carga != null)
                {
                    this.ctrEnteros.Tag = carga;
                    this.ctrEnteros.valor = carga.idCargaCombustibleExtra;
                    this.ctrEnteros.IsEnabled = false;
                    this.txtFecha.Text = carga.fecha.ToString();
                    this.txtTipoCombustible.Text = carga.tipoOrdenCombustible.nombreTipoOrdenCombustible;
                    this.ctrUnidades.unidadTransporte = carga.unidadTransporte;
                    this.txtEmpresa.Text = carga.unidadTransporte.empresa.nombre;
                    this.ctrViaje.valor = !carga.numViaje.HasValue ? 0 : carga.numViaje.Value;
                    this.ctrOperador.personal = carga.chofer;
                    if (carga.fechaCombustible.HasValue)
                    {
                        base.mainWindow.scrollContainer.IsEnabled = false;
                        base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                    }
                    else
                    {
                        base.mainWindow.scrollContainer.IsEnabled = true;
                        base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
                    }
                }
                else
                {
                    this.ctrEnteros.Tag = null;
                    this.ctrEnteros.valor = 0;
                    this.ctrEnteros.IsEnabled = true;
                    this.txtFecha.Clear();
                    this.txtTipoCombustible.Clear();
                    this.ctrUnidades.limpiar();
                    this.txtEmpresa.Clear();
                    this.ctrViaje.valor = 0;
                    this.ctrOperador.limpiar();
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void mapNivelTanque(NivelesTanque nivelesTanque)
        {
            if (nivelesTanque != null)
            {
                this.txtClave.Tag = nivelesTanque;
                this.txtClave.valor = nivelesTanque.idNivel;
                this.txtFechaInicio.Text = nivelesTanque.fechaInicio.ToString("dd/MM/yyyy HH:mm:ss");
            }
            else
            {
                this.txtClave.Tag = null;
                this.txtClave.valor = 0;
                this.txtFechaInicio.Clear();
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            this.limpiarExtra(false);
            this.mapForm(null);
        }       

        private void txtProveedor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.txtProveedor.SelectedItem != null)
            {
                this.txtPrecio.valor = ((ProveedorCombustible)this.txtProveedor.SelectedItem).precio;
            }
            else
            {
                this.txtPrecio.valor = decimal.Zero;
                this.limpiarExtra(false);
            }
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.getTiposCombustible();
            OperationResult result = new CargaCombustibleSvc().getProveedor(0);
            if (result.typeResult == ResultTypes.success)
            {
                this.txtProveedor.ItemsSource = (List<ProveedorCombustible>)result.result;
            }
            this.nuevo();
            this.chcExtra.IsEnabled = false;
            if (new PrivilegioSvc().consultarPrivilegio("CAPTURA_CARGAS_EXTERNAS", base.mainWindow.inicio._usuario.idUsuario).typeResult == ResultTypes.success)
            {
                this.chcExtra.IsChecked = true;
                this.afectaKardex = false;
            }
            else
            {
                this.ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += new SelectionChangedEventHandler(this.CbxZonaOpetativa_SelectionChanged);
                this.ctrZonaOperativa.cargarControl(base.mainWindow.zonaOperativa, base.mainWindow.usuario.idUsuario);
            }
            this.ctrDespachador.loaded(eTipoBusquedaPersonal.DESPACHADORES, null);
        }


    }
}
