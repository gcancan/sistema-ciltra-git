﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static WpfClient.ImprimirMensaje;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editCasetas.xaml
    /// </summary>
    public partial class editCasetas : ViewBase
    {
        public editCasetas()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                if (!cargarEstados())
                    return;

                if (!cargarCasetas())
                    return;

                nuevo();
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        public override void nuevo()
        {
            base.nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
            mapForm(null);
        }
        private bool cargarEstados()
        {
            try
            {
                OperationResult resp = new EstadoSvc().getEstadosAllorById();
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxEstados.ItemsSource = resp.result as List<Estado>;
                    return true;
                }
                else
                {
                    imprimir(resp);
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        private bool cargarCasetas()
        {
            try
            {
                OperationResult resp = new CasetaSvc().getCasetasByIdEstado();
                if (resp.typeResult == ResultTypes.success)
                {
                    lvlCasetas.ItemsSource = resp.result as List<Caseta>;
                    CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlCasetas.ItemsSource);
                    PropertyGroupDescription groupDescription = new PropertyGroupDescription("estado.nombre");
                    view.GroupDescriptions.Add(groupDescription);
                    view.Filter = UserFilter;
                    return true;
                }
                else
                {
                    imprimir(resp);
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private void txtChanged(object sender, TextChangedEventArgs e)
        {
            if (lvlCasetas.ItemsSource != null)
            {
                CollectionViewSource.GetDefaultView(lvlCasetas.ItemsSource).Refresh();
            }
        }
        private bool UserFilter(object item)
        {
            return (item as Caseta).via.IndexOf(txtFiltro.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                    (item as Caseta).nombreCaseta.IndexOf(txtFiltro.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        private void lvlCasetas_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
            {
                Caseta caseta = (sender as ListView).SelectedItem as Caseta;
                mapForm(caseta);
            }
        }
        private void lvlCasetas_Selected(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                Caseta caseta = (sender as ListView).SelectedItem as Caseta;
                mapForm(caseta);
            }
        }
        private void lvlCasetas_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender != null)
            {
                Caseta caseta = (sender as ListView).SelectedItem as Caseta;
                mapForm(caseta);
            }
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                Caseta caseta = mapForm();
                if (caseta == null)
                {
                    return new OperationResult(ResultTypes.error, "Ocurrio un error al leer la información");
                }
                else
                {
                    OperationResult resp = new CasetaSvc().saveCaseta(ref caseta);
                    if (resp.typeResult == ResultTypes.success)
                    {                        
                        if (caseta.estatus)
                        {
                            mapForm(caseta);
                        }
                        else
                        {
                            mapForm(null);
                        }
                        mainWindow.habilitar = true;
                        cargarCasetas();                        
                    }
                    return resp;
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapForm(Caseta caseta)
        {
            try
            {
                if (caseta != null)
                {
                    ctrClave.Tag = caseta;
                    ctrClave.valor = caseta.idCaseta;
                    txtVia.Text = caseta.via;
                    txtNombreCaseta.Text = caseta.nombreCaseta;
                    //dtpFechaVigencia.SelectedDate = caseta.vigencia;
                    int i = 0;
                    foreach (Estado estado in cbxEstados.Items)
                    {
                        if (estado.idEstado == caseta.estado.idEstado)
                        {
                            cbxEstados.SelectedIndex = i;
                            break;
                        }
                        i++;
                    }
                    ctr3Ejes.valor = caseta.costo3Ejes;
                    ctr5Ejes.valor = caseta.costo5Ejes;
                    ctr6Ejes.valor = caseta.costo6Ejes;
                    ctr9Ejes.valor = caseta.costo9Ejes;
                    chcActivo.IsChecked = caseta.estatus;
                    txtPaginaFacturacion.Text = caseta.paginaFacturacion;
                }
                else
                {
                    ctrClave.Tag = null;
                    ctrClave.limpiar();
                    txtVia.Clear();
                    txtNombreCaseta.Clear();
                    //dtpFechaVigencia.SelectedDate = null;
                    cbxEstados.SelectedItem = null;
                    ctr3Ejes.valor = 0;
                    ctr5Ejes.valor = 0;
                    ctr6Ejes.valor = 0;
                    ctr9Ejes.valor = 0;
                    chcActivo.IsChecked = true;
                    txtPaginaFacturacion.Clear();
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private Caseta mapForm()
        {
            try
            {
                Caseta caseta = new Caseta
                {
                    idCaseta = ctrClave.Tag == null ? 0 : (ctrClave.Tag as Caseta).idCaseta,
                    via = txtVia.Text,
                    nombreCaseta = txtNombreCaseta.Text,
                    estado = cbxEstados.SelectedItem as Estado,
                    estatus = chcActivo.IsChecked.Value,
                    vigencia = null,// dtpFechaVigencia.SelectedDate.Value,
                    costo3Ejes = ctr3Ejes.valor,
                    costo5Ejes = ctr5Ejes.valor,
                    costo6Ejes = ctr6Ejes.valor,
                    costo9Ejes = ctr9Ejes.valor,
                    paginaFacturacion = txtPaginaFacturacion.Text
                };
                return caseta;
            }
            catch (Exception ex)
            {
                imprimir(ex);
                return null;
            }
        }

        private void TxtPaginaFacturacion_TextChanged(object sender, TextChangedEventArgs e)
        {
            //txtDireccion.Text = txtPaginaFacturacion.Text;
        }

        private void BtnIr_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txtPaginaFacturacion.Text)) return;

            System.Diagnostics.Process.Start(txtPaginaFacturacion.Text);
        }

        private void BtnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;

                List<Caseta> listaCasetas = lvlCasetas.ItemsSource as List<Caseta>;
                new ReportView().exportarListaCasetas(listaCasetas);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
