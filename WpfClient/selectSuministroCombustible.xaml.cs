﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectSuministroCombustible.xaml
    /// </summary>
    public partial class selectSuministroCombustible : Window
    {
        public selectSuministroCombustible()
        {
            InitializeComponent();
        }
        private void txtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(this.lvlSuministros.ItemsSource).Refresh();
        }
        public SuministroCombustible GetSuministroCombustible(TanqueCombustible tanque)
        {
            try
            {
                getAllSuministros(tanque);
                bool? result = ShowDialog();
                if (result.Value && lvlSuministros.SelectedItem != null)
                {
                    return (lvlSuministros.SelectedItem as ListViewItem).Content as SuministroCombustible;
                }
                return null;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex) ;
                return null;
            }
        }
        TanqueCombustible tanque = null;
        private void getAllSuministros(TanqueCombustible tanque)
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new SuministroCombustibleSvc().getSuministrosCombustible(tanque.idTanqueCombustible);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapSuministros(resp.result as List<SuministroCombustible>);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void mapSuministros(List<SuministroCombustible> listSuministros)
        {
            this.lvlSuministros.Items.Clear();
            List<ListViewItem> list = new List<ListViewItem>();
            foreach (SuministroCombustible origen in listSuministros)
            {
                ListViewItem item = new ListViewItem
                {
                    Content = origen
                };
                item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                list.Add(item);
            }
            this.lvlSuministros.ItemsSource = list;
            CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlSuministros.ItemsSource);
            defaultView.Filter = new Predicate<object>(this.UserFilter);
        }
        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            base.DialogResult = true;
        }
        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtBuscador.Text) ||
            ((((Origen)(item as ListViewItem).Content).nombreOrigen.ToString().IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0) ||
            (((Origen)(item as ListViewItem).Content).clasificacion.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));
    }
}
