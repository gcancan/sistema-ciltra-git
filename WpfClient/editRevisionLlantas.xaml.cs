﻿namespace WpfClient
{
    using Core.Interfaces;
    using Core.Models;
    using CoreFletera.Interfaces;
    using CoreFletera.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Markup;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;


    public partial class EditRevisionLlantas : ViewBase
    {
        private List<Llanta> listaUsadas = new List<Llanta>();  
        public EditRevisionLlantas()
        {
            InitializeComponent();
        }
        public void agregarImagenLlanta()
        {
        }

        private void btnDesmontar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ctrKmActual.valor < ctrKmAnterior.valor)
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "EL KM ACTUAL TIENE QUE SER MAYOR AL ANTERIOR"
                    };
                    ImprimirMensaje.imprimir(resp);
                }
                else
                {
                    base.Cursor = Cursors.Wait;
                    DateTime now = DateTime.Now;
                    List<MovimientoInternoLlanta> listaMovimientos = new List<MovimientoInternoLlanta>();
                    List<StackPanel> list2 = this.stpDiagrama.Children.Cast<StackPanel>().ToList<StackPanel>();
                    foreach (StackPanel panel in list2)
                    {
                        List<CheckBox> list3 = panel.Children.Cast<CheckBox>().ToList<CheckBox>();
                        foreach (CheckBox box in list3)
                        {
                            if ((box.Tag != null) && box.IsChecked.Value)
                            {
                                MovimientoInternoLlanta llanta2 = new MovimientoInternoLlanta
                                {
                                    llanta = box.Tag as Llanta,
                                    fecha = now,
                                    tipoMovimiento = enumTipoMovimientoLlanta.SALIDA,
                                    tipoUbicacionAnt = EnumTipoUbicacionLlanta.UNIDAD_TRANSPORTE,
                                    almacenLlantasOrigen = EnumAlmacenLlantas.NINGUNA,
                                    idUbicacionAnt = this.ctrUnidadTrans.unidadTransporte.clave,
                                    posicionAnt = new int?((box.Tag as Llanta).posicion),
                                    profundidadAnterior = (box.Tag as Llanta).profundidadActual,
                                    kmAnterior = this.ctrKmAnterior.valor,
                                    tipoUbicacionActual = EnumTipoUbicacionLlanta.ALMACEN,
                                    almacenLlantasDestino = EnumAlmacenLlantas.USADAS
                                };
                                llanta2.idUbicacionActual = 2.ToString();
                                llanta2.posicionActual = null;
                                llanta2.kmActual = this.ctrKmActual.valor;
                                llanta2.tipoDocumento = TipoDocumentoLlantas.SERVICIO_INTERNO;
                                llanta2.idDocumento = string.Empty;
                                llanta2.usuario = base.mainWindow.usuario;
                                CondicionLlanta llanta1 = new CondicionLlanta
                                {
                                    idCondicion = 2
                                };
                                llanta2.condicionLlanta = llanta1;
                                MovimientoInternoLlanta item = llanta2;
                                StackPanel content = box.Content as StackPanel;
                                foreach (object obj2 in content.Children.Cast<object>().ToList<object>())
                                {
                                    if (obj2 is StackPanel)
                                    {
                                        StackPanel panel3 = obj2 as StackPanel;
                                        if (panel3.Name == "stpProfuncidad")
                                        {
                                            foreach (object obj3 in panel3.Children.Cast<object>().ToList<object>())
                                            {
                                                if (obj3 is controlEnteros)
                                                {
                                                    item.profundidadActual = (obj3 as controlEnteros).valor;
                                                }
                                            }
                                        }
                                    }
                                }
                                listaMovimientos.Add(item);
                            }
                        }
                    }
                    OperationResult resp = new MovimientoInternoLlantaSvc().guardarMovimientoInterno(listaMovimientos);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        UnidadTransporte unidadTransporte = this.ctrUnidadTrans.unidadTransporte;
                        this.nuevo();
                        this.ctrUnidadTrans.unidadTransporte = unidadTransporte;
                    }
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void btnPilaDesechos_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.ctrKmActual.valor < this.ctrKmAnterior.valor)
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "EL KM ACTUAL TIENE QUE SER MAYOR AL ANTERIOR"
                    };
                    ImprimirMensaje.imprimir(resp);
                }
                else
                {
                    base.Cursor = Cursors.Wait;
                    DateTime now = DateTime.Now;
                    List<MovimientoInternoLlanta> listaMovimientos = new List<MovimientoInternoLlanta>();
                    List<StackPanel> list2 = this.stpDiagrama.Children.Cast<StackPanel>().ToList<StackPanel>();
                    foreach (StackPanel panel in list2)
                    {
                        List<CheckBox> list3 = panel.Children.Cast<CheckBox>().ToList<CheckBox>();
                        foreach (CheckBox box in list3)
                        {
                            if ((box.Tag != null) && box.IsChecked.Value)
                            {
                                MovimientoInternoLlanta llanta2 = new MovimientoInternoLlanta
                                {
                                    llanta = box.Tag as Llanta,
                                    fecha = now,
                                    tipoMovimiento = enumTipoMovimientoLlanta.SALIDA,
                                    tipoUbicacionAnt = EnumTipoUbicacionLlanta.UNIDAD_TRANSPORTE,
                                    almacenLlantasOrigen = EnumAlmacenLlantas.NINGUNA,
                                    idUbicacionAnt = this.ctrUnidadTrans.unidadTransporte.clave,
                                    posicionAnt = new int?((box.Tag as Llanta).posicion),
                                    profundidadAnterior = (box.Tag as Llanta).profundidadActual,
                                    kmAnterior = this.ctrKmAnterior.valor,
                                    tipoUbicacionActual = EnumTipoUbicacionLlanta.ALMACEN,
                                    almacenLlantasDestino = EnumAlmacenLlantas.DESECHOS
                                };
                                llanta2.idUbicacionActual = 4.ToString();
                                llanta2.posicionActual = null;
                                llanta2.kmActual = this.ctrKmActual.valor;
                                llanta2.tipoDocumento = TipoDocumentoLlantas.SERVICIO_INTERNO;
                                llanta2.idDocumento = string.Empty;
                                llanta2.usuario = base.mainWindow.usuario;
                                CondicionLlanta llanta1 = new CondicionLlanta
                                {
                                    idCondicion = 4
                                };
                                llanta2.condicionLlanta = llanta1;
                                MovimientoInternoLlanta item = llanta2;
                                StackPanel content = box.Content as StackPanel;
                                foreach (object obj2 in content.Children.Cast<object>().ToList<object>())
                                {
                                    if (obj2 is StackPanel)
                                    {
                                        StackPanel panel3 = obj2 as StackPanel;
                                        if (panel3.Name == "stpProfuncidad")
                                        {
                                            foreach (object obj3 in panel3.Children.Cast<object>().ToList<object>())
                                            {
                                                if (obj3 is controlEnteros)
                                                {
                                                    item.profundidadActual = (obj3 as controlEnteros).valor;
                                                }
                                            }
                                        }
                                    }
                                }
                                listaMovimientos.Add(item);
                            }
                        }
                    }
                    OperationResult resp = new MovimientoInternoLlantaSvc().guardarMovimientoInterno(listaMovimientos);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        UnidadTransporte unidadTransporte = this.ctrUnidadTrans.unidadTransporte;
                        this.nuevo();
                        this.ctrUnidadTrans.unidadTransporte = unidadTransporte;
                    }
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void cargarDiagrama(DiagramaLLantas diagrama, UnidadTransporte unidad)
        {
            this.eje1.Children.Clear();
            this.eje2.Children.Clear();
            this.eje3.Children.Clear();
            this.eje4.Children.Clear();
            List<StackPanel> list = this.stpDiagrama.Children.Cast<StackPanel>().ToList<StackPanel>();
            string currentDirectory = Directory.GetCurrentDirectory();
            int posicion = 0;
            for (int i = 1; i <= diagrama.numEjes; i++)
            {
                int llantas = diagrama.listaDetalles[i - 1].llantas;
                for (int j = 1; j <= llantas; j++)
                {
                    posicion++;
                    Border border = new Border
                    {
                        BorderThickness = new Thickness(0.1)
                    };
                    Image element = new Image
                    {
                        Height = 40.0
                    };
                    BitmapImage image2 = new BitmapImage(new Uri(currentDirectory + @"\Recursos\BalanceNew.png"));
                    CheckBox box = new CheckBox();
                    StackPanel panel = new StackPanel
                    {
                        Orientation = Orientation.Horizontal,
                        Width = 220.0
                    };
                    StackPanel panel2 = new StackPanel
                    {
                        Name = "stpProfuncidad",
                        VerticalAlignment = VerticalAlignment.Center,
                        HorizontalAlignment = HorizontalAlignment.Center
                    };
                    Label label = new Label
                    {
                        Content = "PROF ACTUAL",
                        FontWeight = FontWeights.Bold,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        Width = 100.0,
                        FontSize = 11.0
                    };
                    controlEnteros enteros = new controlEnteros
                    {
                        Width = 100.0,
                        Name = "ctrProfundidad",
                        FontSize = 11.0
                    };
                    enteros.txtEntero.Tag = box;
                    enteros.txtEntero.TextChanged += new TextChangedEventHandler(this.TxtDecimal_TextChanged);
                    panel2.Children.Add(label);
                    panel2.Children.Add(enteros);
                    panel.Children.Add(panel2);
                    StackPanel panel3 = new StackPanel
                    {
                        Orientation = Orientation.Vertical
                    };
                    Label label3 = new Label
                    {
                        Content = posicion,
                        HorizontalContentAlignment = HorizontalAlignment.Center,
                        FontWeight = FontWeights.Bold,
                        FontSize = 14.0,
                        Foreground = Brushes.SteelBlue
                    };
                    panel3.Children.Add(label3);
                    element.Source = image2;
                    panel3.Children.Add(element);
                    Label label2 = new Label
                    {
                        FontWeight = FontWeights.Bold,
                        HorizontalContentAlignment = HorizontalAlignment.Center
                    };
                    OperationResult result = new LlantaSvc().getLLantaByPosAndUnidadTrans(posicion, unidad);
                    if (result.typeResult == ResultTypes.success)
                    {
                        Llanta newItem = (Llanta)result.result;
                        enteros.valor = Convert.ToInt32(newItem.profundidadActual);
                        label2.Content = newItem.clave;
                        box.Tag = newItem;
                        this.cbxLlanta.Items.Add(newItem);
                        StackPanel panel4 = new StackPanel
                        {
                            Orientation = Orientation.Vertical
                        };
                        Label label5 = new Label
                        {
                            Content = "INFORMACI\x00d3N DE LA LLANTA",
                            FontWeight = FontWeights.Bold,
                            FontSize = 16.0
                        };
                        panel4.Children.Add(label5);
                        StackPanel panel5 = new StackPanel();
                        StackPanel panel6 = new StackPanel
                        {
                            Orientation = Orientation.Horizontal
                        };
                        Label label6 = new Label
                        {
                            Width = 120.0,
                            Content = "UBICACI\x00d3N:",
                            FontWeight = FontWeights.Bold
                        };
                        panel6.Children.Add(label6);
                        Label label7 = new Label
                        {
                            Content = newItem.unidadTransporte.clave + " POS: " + newItem.posicion.ToString()
                        };
                        panel6.Children.Add(label7);
                        panel5.Children.Add(panel6);
                        panel6 = new StackPanel
                        {
                            Orientation = Orientation.Horizontal
                        };
                        Label label8 = new Label
                        {
                            Width = 120.0,
                            Content = "MARCA:",
                            FontWeight = FontWeights.Bold
                        };
                        panel6.Children.Add(label8);
                        Label label9 = new Label
                        {
                            Content = newItem.marca.descripcion
                        };
                        panel6.Children.Add(label9);
                        panel5.Children.Add(panel6);
                        panel6 = new StackPanel
                        {
                            Orientation = Orientation.Horizontal
                        };
                        Label label10 = new Label
                        {
                            Width = 120.0,
                            Content = "TIPO LLANTA:",
                            FontWeight = FontWeights.Bold
                        };
                        panel6.Children.Add(label10);
                        Label label11 = new Label
                        {
                            Content = newItem.tipoLlanta.nombre
                        };
                        panel6.Children.Add(label11);
                        panel5.Children.Add(panel6);
                        panel6 = new StackPanel
                        {
                            Orientation = Orientation.Horizontal
                        };
                        Label label12 = new Label
                        {
                            Width = 120.0,
                            Content = "DISE\x00d1O:",
                            FontWeight = FontWeights.Bold
                        };
                        panel6.Children.Add(label12);
                        Label label13 = new Label
                        {
                            Content = newItem.diseño
                        };
                        panel6.Children.Add(label13);
                        panel5.Children.Add(panel6);
                        panel6 = new StackPanel
                        {
                            Orientation = Orientation.Horizontal
                        };
                        Label label14 = new Label
                        {
                            Width = 120.0,
                            Content = "MEDIDA:",
                            FontWeight = FontWeights.Bold
                        };
                        panel6.Children.Add(label14);
                        Label label15 = new Label
                        {
                            Content = newItem.medida
                        };
                        panel6.Children.Add(label15);
                        panel5.Children.Add(panel6);
                        panel6 = new StackPanel
                        {
                            Orientation = Orientation.Horizontal
                        };
                        Label label16 = new Label
                        {
                            Width = 120.0,
                            Content = "PROFUNDIDAD:",
                            FontWeight = FontWeights.Bold
                        };
                        panel6.Children.Add(label16);
                        Label label17 = new Label
                        {
                            Content = newItem.profundidad
                        };
                        panel6.Children.Add(label17);
                        panel5.Children.Add(panel6);
                        panel6 = new StackPanel
                        {
                            Orientation = Orientation.Horizontal
                        };
                        Label label18 = new Label
                        {
                            Width = 120.0,
                            Content = "KM:",
                            FontWeight = FontWeights.Bold
                        };
                        panel6.Children.Add(label18);
                        Label label19 = new Label
                        {
                            Content = newItem.km
                        };
                        panel6.Children.Add(label19);
                        panel5.Children.Add(panel6);
                        panel6 = new StackPanel
                        {
                            Orientation = Orientation.Horizontal
                        };
                        Label label20 = new Label
                        {
                            Width = 120.0,
                            Content = "FECHA:",
                            FontWeight = FontWeights.Bold
                        };
                        panel6.Children.Add(label20);
                        Label label21 = new Label
                        {
                            Content = newItem.fecha
                        };
                        panel6.Children.Add(label21);
                        panel5.Children.Add(panel6);
                        panel6 = new StackPanel
                        {
                            Orientation = Orientation.Horizontal
                        };
                        Label label22 = new Label
                        {
                            Width = 120.0,
                            Content = "RFID:",
                            FontWeight = FontWeights.Bold
                        };
                        panel6.Children.Add(label22);
                        Label label23 = new Label
                        {
                            Content = newItem.rfid
                        };
                        panel6.Children.Add(label23);
                        panel5.Children.Add(panel6);
                        panel4.Children.Add(panel5);
                        ToolTip tip1 = new ToolTip
                        {
                            AllowDrop = true,
                            Content = panel4
                        };
                        box.ToolTip = tip1;
                        if (newItem.profundidadActual <= 4M)
                        {
                            panel.Background = Brushes.Red;
                        }
                        else if ((newItem.profundidadActual > 4M) && (newItem.profundidadActual <= 9M))
                        {
                            panel.Background = Brushes.Orange;
                        }
                        else
                        {
                            panel.Background = Brushes.GreenYellow;
                        }
                    }
                    else
                    {
                        panel2.IsEnabled = false;
                        ContextMenu menu = new ContextMenu();
                        MenuItem newItem = new MenuItem
                        {
                            Header = "INVENTARIAR LLANTA"
                        };
                        newItem.Tag = posicion;
                        newItem.Click += new RoutedEventHandler(this.ItemAgregar_Click);
                        menu.Items.Add(newItem);
                        MenuItem item2 = new MenuItem
                        {
                            Header = "MONTAR LLANTA"
                        };
                        item2.Tag = posicion;
                        item2.Click += new RoutedEventHandler(this.ItemMontar_Click);
                        menu.Items.Add(item2);
                        box.ContextMenu = menu;
                        label2.Content = "SIN ASIGNAR";
                    }
                    panel3.Children.Add(label2);
                    panel.Children.Add(panel3);
                    box.Content = panel;
                    box.MouseDoubleClick += new MouseButtonEventHandler(this.LblLLanta_MouseDoubleClick);
                    box.BorderThickness = new Thickness(1.0);
                    box.BorderBrush = Brushes.SteelBlue;
                    box.Width = 220.0;
                    box.HorizontalAlignment = HorizontalAlignment.Center;
                    box.VerticalAlignment = VerticalAlignment.Center;
                    box.Margin = new Thickness(2.0);
                    list[i - 1].Children.Add(box);
                }
            }
        }

        private void cargarDiagramaUsadas(List<Llanta> listaUsadas)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                lvlUsadas.ItemsSource = listaUsadas;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void cargarLLantas(UnidadTransporte unidadTransporte)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult resp = new DiagramasLLantasSvc().getDiagramaByTipoUnidadTrans(unidadTransporte.tipoUnidad);
                if (resp.typeResult > ResultTypes.success)
                {
                    ImprimirMensaje.imprimir(resp);
                }
                else
                {
                    this.cargarDiagrama((DiagramaLLantas)resp.result, unidadTransporte);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        public void cargarLlantasAlmacenUsadas()
        {
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult result = new LlantaSvc().getLlantasEnAlmacen(2, "%");
                if (result.typeResult == ResultTypes.success)
                {
                    this.listaUsadas = result.result as List<Llanta>;
                    this.cargarDiagramaUsadas(this.listaUsadas);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void cbxLlanta_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = (ComboBox)sender;
            Llanta selectedItem = (Llanta)box.SelectedItem;
            this.txtPos.Text = selectedItem.posicion.ToString();
            this.seleccionarImagen(selectedItem);
        }

        private void CtrUnidadTrans_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (ctrUnidadTrans.DataContext != null)
            {
                OperationResult result = new RegistroKilometrajeSvc().getUltimoKMVerificacion(this.ctrUnidadTrans.unidadTransporte.clave);
                if (result.typeResult == ResultTypes.success)
                {
                    this.ctrKmAnterior.valor = Convert.ToDecimal(result.result);
                }
                this.cargarLLantas(this.ctrUnidadTrans.unidadTransporte);
            }
            else
            {
                this.eje1.Children.Clear();
                this.eje2.Children.Clear();
                this.eje3.Children.Clear();
                this.eje4.Children.Clear();
                this.cbxLlanta.Items.Clear();
            }
        }

        public override OperationResult guardar()
        {
            OperationResult result2;
            try
            {
                base.Cursor = Cursors.Wait;
                RevisionLlantas revisionLlantas = this.mapForm();
                OperationResult result = new RevisionLlantaSvc().saveRevisionLlanta(ref revisionLlantas);
                if (result.typeResult == ResultTypes.success)
                {
                    this.ctrFolio.valor = revisionLlantas.idRevisionLlanta;
                    UnidadTransporte unidadTransporte = this.ctrUnidadTrans.unidadTransporte;
                    this.nuevo();
                    this.ctrUnidadTrans.unidadTransporte = unidadTransporte;
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                    this.cargarLlantasAlmacenUsadas();
                }
                result2 = result;
            }
            catch (Exception exception)
            {
                result2 = new OperationResult(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return result2;
        }
        private void ItemAgregar_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            int posicionLlanta = Convert.ToInt32(item.Tag);
            if (new editInventarioLlantas(this.ctrUnidadTrans.unidadTransporte.empresa, this.ctrUnidadTrans.unidadTransporte, posicionLlanta, base.mainWindow).crearInventarioLlanta() != null)
            {
                this.cargarLLantas(this.ctrUnidadTrans.unidadTransporte);
            }
        }

        private void ItemMontar_Click(object sender, RoutedEventArgs e)
        {
            MenuItem item = sender as MenuItem;
            int num = Convert.ToInt32(item.Tag);
            Llanta llanta = new SeleccionarLlanta(this.listaUsadas).getLlanta();
            if (llanta != null)
            {
                List<MovimientoInternoLlanta> listaMovimientos = new List<MovimientoInternoLlanta>();
                MovimientoInternoLlanta llanta1 = new MovimientoInternoLlanta
                {
                    llanta = llanta,
                    fecha = DateTime.Now,
                    tipoMovimiento = enumTipoMovimientoLlanta.ENTRADA,
                    tipoUbicacionAnt = EnumTipoUbicacionLlanta.ALMACEN,
                    almacenLlantasOrigen = EnumAlmacenLlantas.USADAS
                };
                llanta1.idUbicacionAnt = 2.ToString();
                llanta1.posicionAnt = null;
                llanta1.profundidadAnterior = llanta.profundidad;
                llanta1.kmAnterior = decimal.Zero;
                llanta1.tipoUbicacionActual = EnumTipoUbicacionLlanta.UNIDAD_TRANSPORTE;
                llanta1.almacenLlantasDestino = EnumAlmacenLlantas.NINGUNA;
                llanta1.profundidadActual = llanta.profundidad;
                llanta1.idUbicacionActual = this.ctrUnidadTrans.unidadTransporte.clave;
                llanta1.posicionActual = new int?(num);
                llanta1.kmActual = decimal.Zero;
                llanta1.tipoDocumento = TipoDocumentoLlantas.SERVICIO_INTERNO;
                llanta1.idDocumento = string.Empty;
                llanta1.usuario = base.mainWindow.usuario;
                CondicionLlanta llanta2 = new CondicionLlanta
                {
                    idCondicion = 2
                };
                llanta1.condicionLlanta = llanta2;
                listaMovimientos.Add(llanta1);
                OperationResult resp = new MovimientoInternoLlantaSvc().guardarMovimientoInterno(listaMovimientos);
                if (resp.typeResult == ResultTypes.success)
                {
                    UnidadTransporte unidadTransporte = this.ctrUnidadTrans.unidadTransporte;
                    this.nuevo();
                    this.ctrUnidadTrans.unidadTransporte = unidadTransporte;
                    this.cargarLlantasAlmacenUsadas();
                }
                ImprimirMensaje.imprimir(resp);
            }
        }

        private void LblLLanta_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender == null) return;

            if (sender is Label)
            {
                Label label = (Label)sender;
                if (label.Tag != null)
                {
                    Llanta tag = (Llanta)label.Tag;
                    int num = 0;
                    foreach (Llanta llanta2 in (IEnumerable)this.cbxLlanta.Items)
                    {
                        if (llanta2.clave == tag.clave)
                        {
                            this.cbxLlanta.SelectedIndex = num;
                            break;
                        }
                        num++;
                    }
                }
            }
        }

        private RevisionLlantas mapForm()
        {
            try
            {
                RevisionLlantas llantas = new RevisionLlantas
                {
                    fechaRevision = DateTime.Now,
                    idEmpresa = this.ctrUnidadTrans.unidadTransporte.empresa.clave,
                    idOrdenServ = null,
                    idUnidadTrans = this.ctrUnidadTrans.unidadTransporte.clave,
                    idRevisionLlanta = 0,
                    usuario = base.mainWindow.usuario.nombreUsuario,
                    km = this.ctrKmActual.valor
                };
                llantas.listaDetalles = new List<DetalleRevisionLlantas>();
                List<StackPanel> list = this.stpDiagrama.Children.Cast<StackPanel>().ToList<StackPanel>();
                foreach (StackPanel panel in list)
                {
                    List<CheckBox> list2 = panel.Children.Cast<CheckBox>().ToList<CheckBox>();
                    foreach (CheckBox box in list2)
                    {
                        if (box.Tag != null)
                        {
                            DetalleRevisionLlantas item = new DetalleRevisionLlantas
                            {
                                llanta = box.Tag as Llanta,
                                profundidadAnterior = (box.Tag as Llanta).profundidadActual,
                                kmAnterior = this.ctrKmAnterior.valor,
                                kmNuevo = this.ctrKmActual.valor
                            };
                            StackPanel content = box.Content as StackPanel;
                            foreach (object obj2 in content.Children.Cast<object>().ToList<object>())
                            {
                                if (obj2 is StackPanel)
                                {
                                    StackPanel panel3 = obj2 as StackPanel;
                                    if (panel3.Name == "stpProfuncidad")
                                    {
                                        foreach (object obj3 in panel3.Children.Cast<object>().ToList<object>())
                                        {
                                            if (obj3 is controlEnteros)
                                            {
                                                item.profundidadNueva = (obj3 as controlEnteros).valor;
                                            }
                                        }
                                    }
                                }
                            }
                            llantas.listaDetalles.Add(item);
                        }
                    }
                }
                return llantas;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            this.ctrFolio.limpiar();
            this.ctrUnidadTrans.limpiar();
            this.ctrKmAnterior.valor = decimal.Zero;
            this.ctrKmActual.valor = decimal.Zero;
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
        }

        private void seleccionarImagen(Llanta det)
        {
        }

        private void TxtDecimal_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox box = sender as TextBox;
            CheckBox tag = box.Tag as CheckBox;
            decimal num = Convert.ToDecimal(box.Text);
            if (tag.Content != null)
            {
                StackPanel content = tag.Content as StackPanel;
                if (num <= 3M)
                {
                    content.Background = Brushes.Red;
                }
                else if ((num > 3M) && (num <= 5M))
                {
                    content.Background = Brushes.Orange;
                }
                else
                {
                    content.Background = Brushes.GreenYellow;
                }
            }
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ctrUnidadTrans.DataContextChanged += CtrUnidadTrans_DataContextChanged;
            ctrUnidadTrans.loaded(etipoUniadBusqueda.TODAS, null);
            this.nuevo();
            this.cargarLlantasAlmacenUsadas();
        }
    }
}
