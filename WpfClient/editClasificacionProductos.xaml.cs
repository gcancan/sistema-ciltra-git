﻿using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editClasificacionProductos.xaml
    /// </summary>
    public partial class editClasificacionProductos : ViewBase
    {
        public editClasificacionProductos()
        {
            InitializeComponent();
        }
        public bool cargarClasificaciones()
        {
            try
            {
                this.lvlClasificaciones.Items.Clear();
                OperationResult resp = new ClasificacionProductosSvc().getAllClasificacionProductos();
                if (resp.typeResult == ResultTypes.success)
                {
                    foreach (ClasificacionProductos productos in resp.result as List<ClasificacionProductos>)
                    {
                        ListViewItem newItem = new ListViewItem
                        {
                            Content = productos
                        };
                        newItem.Selected += new RoutedEventHandler(this.Lvl_Selected);
                        newItem.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                        this.lvlClasificaciones.Items.Add(newItem);
                    }
                    return true;
                }
                if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                    return false;
                }
                return true;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return false;
            }
        }

        public override OperationResult guardar()
        {
            try
            {
                ClasificacionProductos clasificacion = this.mapform();
                OperationResult result = new ClasificacionProductosSvc().saveClasificacionProductos(ref clasificacion);
                if (result.typeResult == ResultTypes.success)
                {
                    base.mainWindow.habilitar = true;
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
                    this.mapform(clasificacion);
                    this.cargarClasificaciones();
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }
        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
            {
                ListViewItem item = sender as ListViewItem;
                this.mapform(item.Content as ClasificacionProductos);
            }
        }

        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                ListViewItem item = sender as ListViewItem;
                this.mapform(item.Content as ClasificacionProductos);
            }
        }

        private ClasificacionProductos mapform()
        {
            try
            {
                return new ClasificacionProductos
                {
                    idClasificacionProducto = (this.ctrClave.Tag == null) ? 0 : (this.ctrClave.Tag as ClasificacionProductos).idClasificacionProducto,
                    descripcion = this.txtDescripcion.Text.Trim(),
                    diesel = this.chcDiesel.IsChecked.Value
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void mapform(ClasificacionProductos clasificacion)
        {
            try
            {
                if (clasificacion != null)
                {
                    this.ctrClave.Tag = clasificacion;
                    this.ctrClave.valor = clasificacion.idClasificacionProducto;
                    this.txtDescripcion.Text = clasificacion.descripcion;
                    this.chcDiesel.IsChecked = new bool?(clasificacion.diesel);
                }
                else
                {
                    this.ctrClave.Tag = null;
                    this.ctrClave.valor = 0;
                    this.txtDescripcion.Clear();
                    this.chcDiesel.IsChecked = false;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
            this.mapform(null);
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.cargarClasificaciones())
                {
                }
                this.nuevo();
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }
    }
}
