﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para ReporteCartaPortesV2.xaml
    /// </summary>
    public partial class ReporteCartaPortesV2 : ViewBase
    {
        public ReporteCartaPortesV2()
        {
            InitializeComponent();
        }
        private async void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            await Task.Run(async () => await this.buscarCartasPortes());
            await Task.Run(async () => await this.llenarLista());
        }

        public async Task buscarCartasPortes()
        {
            try
            {
                Dispatcher.Invoke(() =>
                {
                    popMensaje.IsOpen = true;
                    lblMensaje.Content = "BUSCANDO REGISTROS...";
                    this.lvlCp.ItemsSource = null;
                    btnBuscar.IsEnabled = false;
                });

                this.listaReporte = new List<ReporteCartaPorte>();
                this.listaReporteViaje = new List<ReporteCartaPorteViajes>();

                //base.Cursor = Cursors.Wait;
                int idEmpresa = 0;
                List<string> listaTractores = new List<string>();
                List<string> listaOperadores = new List<string>();
                List<string> listaRutas = new List<string>();
                List<string> listaClientes = new List<string>();
                List<string> listaModalidades = new List<string>();
                List<string> listaEstatus = new List<string>();
                bool todosOperadores = false;
                bool todosTractores = false;
                Dispatcher.Invoke(() =>
                {
                    idEmpresa = ctrEmpresa.empresaSelected.clave;
                    this.fechaInicio = new DateTime?(Convert.ToDateTime($"{this.dtpFechaInicio.SelectedDate.Value.Day}/{this.dtpFechaInicio.SelectedDate.Value.Month}/{this.dtpFechaInicio.SelectedDate.Value.Year} {this.horaInicio.Value}:{this.minutoInicio.Value}:00"));
                    this.fechaFin = new DateTime?(Convert.ToDateTime($"{this.dtpFechaFin.SelectedDate.Value.Day}/{this.dtpFechaFin.SelectedDate.Value.Month}/{this.dtpFechaFin.SelectedDate.Value.Year} {this.horaFin.Value}:{this.minutoFin.Value}:59"));
                    listaTractores = (from s in this.cbxTractores.SelectedItems.Cast<UnidadTransporte>().ToList<UnidadTransporte>() select s.clave).ToList<string>();
                    listaOperadores = (from s in this.cbxOperadores.SelectedItems.Cast<Personal>().ToList<Personal>() select s.clave.ToString()).ToList<string>();
                    listaClientes = (from s in this.cbxClientes.SelectedItems.Cast<Cliente>().ToList<Cliente>() select s.clave.ToString()).ToList<string>();
                    listaRutas = (from s in this.cbxDestinos.SelectedItems.Cast<Zona>().ToList<Zona>() select s.clave.ToString()).ToList<string>();
                    listaModalidades = (from s in this.cbxModalidad.SelectedItems.Cast<Modalidad>().ToList<Modalidad>() select s.nombre.ToString()).ToList<string>();
                    listaEstatus = this.cbxEstatus.SelectedItems.Cast<string>().ToList<string>();
                    todosOperadores = chcTodosOperadores.IsChecked.Value;
                    todosTractores = chcTodosTractores.IsChecked.Value;
                });
                OperationResult resp = new ReporteCartaPorteSvc().getReporteCartaPorteV3(idEmpresa, this.fechaInicio.Value, this.fechaFin.Value, listaTractores, listaOperadores,
                    listaClientes, listaRutas, listaModalidades, listaEstatus, todosOperadores, todosTractores);
                if (resp.typeResult == ResultTypes.success)
                {
                    this.listaReporte = resp.result as List<ReporteCartaPorte>;

                    listaReporte = listaReporte.OrderBy(s => s.fechaInicio).ToList();

                    List<GrupoRuta> listaGrupos = (from v in listaReporte
                                                   group v by v.cartaPorte into grupo
                                                   select new GrupoRuta
                                                   {
                                                       folio = (from s in grupo select s.folio).First(),
                                                       cartaPorte = (from s in grupo select s.cartaPorte).First(),
                                                       idOrigen = (from s in grupo select s.idOrigen).First(),
                                                       idDestino = (from s in grupo select s.idDestino).First(),
                                                       kmOrigen = (from s in grupo select s.kmOrigen).First(),
                                                       kmOrigenDestino = (from s in grupo select s.kmOrigenDestino).First(),
                                                       kmDestino = (from s in grupo select s.kmDestino).First(),
                                                       //km = (from s in grupo select s.km).First()
                                                   }).ToList();



                    foreach (ReporteCartaPorte reporteCp in listaReporte)
                    {
                        reporteCp.listaGrupo = new List<GrupoRuta>();
                        reporteCp.listaGrupo = listaGrupos.FindAll(s => s.folio == reporteCp.folio);
                    }
                    foreach (ReporteCartaPorte reporteCp in listaReporte)
                    {
                        if (reporteCp.vale == 36517)
                        {

                        }
                        reporteCp.sumatoriaKm = listaReporte.FindAll(s => s.vale == reporteCp.vale).Sum(z => z.km);
                    }

                    listaReporteViaje = (from v in this.listaReporte
                                         group v by v.folio into grupoViaje
                                         select new ReporteCartaPorteViajes
                                         {
                                             lista = grupoViaje.ToList<ReporteCartaPorte>(),
                                             fechaInicioCarga = (from s in grupoViaje select s.fechaInicioCarga).First(),
                                             fechaInicio = (from s in grupoViaje select s.fechaInicio).First(),
                                             fechaFin = (from s in grupoViaje select s.fechaFin).First(),
                                             fechaPrograma = (from s in grupoViaje select s.fechaPrograma).First(),
                                             isReEnviar = (from s in grupoViaje select s.isReEnviar).First(),
                                             imputable = (from s in grupoViaje select s.imputable).First()
                                         }).ToList();

                   
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    await Dispatcher.Invoke(async () =>
                    {
                        listaReporte = new List<ReporteCartaPorte>();
                        listaReporteViaje = new List<ReporteCartaPorteViajes>();
                        //await Task.Run(async () => await this.llenarLista());
                    });

                }
                else
                {
                    await Dispatcher.Invoke(async () =>
                    {
                        listaReporte = new List<ReporteCartaPorte>();
                        listaReporteViaje = new List<ReporteCartaPorteViajes>();
                        //await Task.Run(async () => await this.llenarLista());
                    });
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                //base.Cursor = Cursors.Arrow;
                Dispatcher.Invoke(() =>
                {
                    popMensaje.IsOpen = false;
                    btnBuscar.IsEnabled = true;
                });
                //await Dispatcher.Invoke(async () =>
                //{
                //    await Task.Run(async () => await this.llenarLista());
                //});
            }
        }
        private async Task llenarLista()
        {
            try
            {
                Dispatcher.Invoke(() =>
                {

                    popMensaje.IsOpen = true;
                    lblMensaje.Content = "MOSTRANDO RESULTADO...";

                    //base.Cursor = Cursors.Wait;
                    this.lvlCp.ItemsSource = null;
                    //this.lvlCp.ItemsSource = this.listaReporte;
                    this.txtSubTotal.Text = this.listaReporte.Sum<ReporteCartaPorte>(s => s.subTotal).ToString("C4");
                    this.txtContViajesSencillo.Text = Convert.ToInt32((from s in this.listaReporte where s.modalidad == "SENCILLO" group s by s.folio).Count<IGrouping<int, ReporteCartaPorte>>()).ToString("N0");
                    this.txtContViajesFull.Text = Convert.ToInt32((from s in this.listaReporte where s.modalidad == "FULL" group s by s.folio).Count<IGrouping<int, ReporteCartaPorte>>()).ToString("N0");
                    this.txtContTC.Text = Convert.ToInt32((from s in this.listaReporte group s by s.tc).Count<IGrouping<string, ReporteCartaPorte>>()).ToString("N0");
                    this.txtDias.Text = !this.fechaFin.HasValue ? "0" : Convert.ToInt32((this.fechaFin.Value - this.fechaInicio.Value).TotalDays).ToString("N0");
                    this.txtDescarga.Text = this.listaReporte.Sum<ReporteCartaPorte>(s => s.toneladas).ToString("N4");
                    if (this.rbnViaje.IsChecked.Value)
                    {

                        this.lvlCp.ItemsSource = this.listaReporteViaje;
                        foreach (var item in listaReporteViaje)
                        {

                        }

                        //CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(lvlCp.ItemsSource);
                        //PropertyGroupDescription item = new PropertyGroupDescription("encabezado");
                        //defaultView.GroupDescriptions.Add(item);
                    }
                    else
                    {
                        this.lvlCp.ItemsSource = this.listaReporte;
                        //CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(lvlCp.ItemsSource);
                        //defaultView.GroupDescriptions.Clear();
                    }
                });
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                Dispatcher.Invoke(() =>
                {
                    popMensaje.IsOpen = false;
                });
            }
        }
        private void btnQuitarPerfil_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                if (this.cbxPerfiles.SelectedItem != null)
                {
                    PerfilReporte selectedItem = this.cbxPerfiles.SelectedItem as PerfilReporte;
                    selectedItem.usuraio = base.mainWindow.usuario;
                    selectedItem.tipoReporte = enumPerfilRporte.VIAJES_V2;
                    selectedItem.activo = false;
                    OperationResult resp = new PerfilReporteSvc().savePerfilReporte(ref selectedItem);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        this.cargarPerfiles();
                        this.txtNombrePerfil.Clear();
                        this.cbxPerfiles.SelectedItem = null;
                    }
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        Dictionary<string, string> dictionaryStr = new Dictionary<string, string>
        {
            {"FOLIO","folio"},
            {"EMPRESA","empresa"},
            {"CLIENTE","cliente"},
            {"OPERADOR","operador"},
            {"ESTATUS","estatus"},
            {"MODALIDAD","modalidad"},
            {"TC","tc"}
        };
        private async void btnExportar_Click(object sender, RoutedEventArgs e)
        {
            await Task.Run(async () => exportar());
        }
        private async Task exportar()
        {
            try
            {
                Dispatcher.Invoke(() =>
                {
                    popMensaje.IsOpen = true;
                    lblMensaje.Content = "EXPORTANDO REGISTROS...";
                    btnExportar.IsEnabled = false;
                });
                //base.Cursor = Cursors.Wait;
                //List<ReporteViaje> list = (from v in this.listaReporte
                //                           group v by v.folio into grupoViaje
                //                           select new ReporteViaje
                //                           {
                //                               folio = (from s in grupoViaje select s.folio).First(),
                //                               listaCp = grupoViaje.ToList<ReporteCartaPorte>(),
                //                               empresa = (from s in grupoViaje select s.empresa).First(),
                //                               cliente = (from s in grupoViaje select s.cliente).First(),
                //                               fechaInicio = (from s in grupoViaje select s.fechaInicio).First(),
                //                               fechaFin = (from s in grupoViaje select s.fechaFin).First(),
                //                               fechaPrograma = (from s in grupoViaje select s.fechaPrograma).First(),
                //                               capacidadIns = (from s in grupoViaje select s.capacidadIns).First(),

                //                               fechaEntradaPlantaCSI = (from s in grupoViaje select s.fechaEntradaPlantaCSI ?? null).First() ?? null,
                //                               fechaSalidaPlantaCSI = (from s in grupoViaje select s.fechaSalidaPlantaCSI ?? null).First() ?? null,
                //                               duracion = (from s in grupoViaje select s.duracion ?? string.Empty).First() ?? string.Empty,
                //                               tiempoTab = (from s in grupoViaje select s.tiempoTab ?? string.Empty).First() ?? string.Empty,
                //                               diferenciaTiempo = (from s in grupoViaje select s.diferenciaTiempo ?? string.Empty).First() ?? string.Empty,
                //                               duracionCierreEntrada = (from s in grupoViaje select s.duracionCierreEntrada ?? string.Empty).First() ?? string.Empty,
                //                               duracionDespachoSalida = (from s in grupoViaje select s.duracionDespachoSalida ?? string.Empty).First() ?? string.Empty,
                //                               strvFalso = (from s in grupoViaje select s.strvFalso).First(),

                //                               utilizado = (from s in grupoViaje select s.utilizado).First(),
                //                               tc = (from s in grupoViaje select s.tc).First(),
                //                               rem1 = (from s in grupoViaje select s.rem1).First(),
                //                               rem2 = (from s in grupoViaje select s.rem2).First(),
                //                               operador = (from s in grupoViaje select s.operador).First(),
                //                               operadorFin = (from s in grupoViaje select s.operadorFin).First(),
                //                               operadorAyuda = (from s in grupoViaje select s.operadorAyuda).First(),
                //                               operadorCap = (from s in grupoViaje select s.operadorCap).First(),
                //                               estatus = (from s in grupoViaje select s.estatus).First(),
                //                               modalidad = (from s in grupoViaje select s.modalidad).First(),
                //                               cobroXKm = (from s in grupoViaje select s.cobroXKm).First(),
                //                               usuario = (from s in grupoViaje select s.usuario).First(),
                //                               usuarioFin = (from s in grupoViaje select s.usuarioFin).First(),
                //                               usuarioAjusta = (from s in grupoViaje select s.usuarioAjusta).First()
                //                           }).ToList<ReporteViaje>();

                var resp = listString;
                var respList = dictionaryStr;

                Dispatcher.Invoke(() =>
                {
                    if (rbnBoleta.IsChecked.Value)
                    {
                        new ReportView().reporteCartaPorteByBoletas(listaReporte, listString, dictionaryStr);
                    }
                    else
                    {
                        new ReportView().reporteCartaPorteByViajesV2(listaReporteViaje, listString, dictionaryStr);
                        //new ReportView().reporteCartaPorteByViaje(list, listString, dictionaryStr);
                    }
                });
            }   
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                Dispatcher.Invoke(() =>
                {
                    popMensaje.IsOpen = false;
                    btnExportar.IsEnabled = true;
                });
            }
        }
        private List<string> listString = new List<string>
        {
            "FOLIO",
            "EMPRESA",
            "CLIENTE",
            "OPERADOR",
            "ESTATUS",
            "MODALIDAD",
            "TC"
        };
        private DateTime? fechaInicio = null;
        private DateTime? fechaFin = null;
        private List<ReporteCartaPorte> listaReporte = new List<ReporteCartaPorte>();
        private List<ReporteCartaPorteViajes> listaReporteViaje = new List<ReporteCartaPorteViajes>();
        private void btnGuardarPerfil_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                if (string.IsNullOrEmpty(this.txtNombrePerfil.Text.Trim()))
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "NO SE PROPORCIONO EL NOMBRE DEL PERFIL"
                    };
                    ImprimirMensaje.imprimir(resp);
                }
                else
                {
                    PerfilReporte perfilReporte = new PerfilReporte
                    {
                        nombrePerfil = this.txtNombrePerfil.Text.Trim(),
                        usuraio = base.mainWindow.usuario,
                        tipoReporte = enumPerfilRporte.VIAJES_V2,
                        activo = true
                    };
                    perfilReporte.cargarlista(this.listString);
                    if (perfilReporte.listDetalles.Count == 0)
                    {
                        System.Windows.MessageBox.Show("Se requiere al menos una opci\x00f3n", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                    else
                    {
                        OperationResult resp = new PerfilReporteSvc().savePerfilReporte(ref perfilReporte);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            this.cargarPerfiles();
                        }
                        ImprimirMensaje.imprimir(resp);
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        public bool cargarPerfiles()
        {
            this.cbxPerfiles.Items.Clear();
            OperationResult resp = new PerfilReporteSvc().getPerfilesReportes(base.mainWindow.usuario.idUsuario, enumPerfilRporte.VIAJES_V2);
            if (resp.typeResult == ResultTypes.success)
            {
                foreach (PerfilReporte reporte in resp.result as List<PerfilReporte>)
                {
                    this.cbxPerfiles.Items.Add(reporte);
                }
                return true;
            }
            if (resp.typeResult != ResultTypes.recordNotFound)
            {
                ImprimirMensaje.imprimir(resp);
                return false;
            }
            return true;
        }
        private void cargarChecColumnas(List<DetallesPerfilReporte> listDetalles)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                List<CheckBox> list = this.wrapColumnas.Children.Cast<CheckBox>().ToList<CheckBox>();
                foreach (CheckBox chc in list)
                {
                    if (chc.Visibility == Visibility.Visible)
                    {
                        if (listDetalles.Find(s => s.nombreDetalle == chc.Content.ToString()) != null)
                        {
                            chc.IsChecked = true;
                        }
                        else
                        {
                            chc.IsChecked = false;
                        }
                    }
                    else
                    {
                        chc.IsChecked = false;
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                OperationResult result2;
                if (this.ctrEmpresa.empresaSelected != null)
                {
                    base.Cursor = Cursors.Wait;
                    this.chcTodosClientes.IsChecked = false;
                    this.chcTodosOperadores.IsChecked = false;
                    this.chcTodosEstatus.IsChecked = false;
                    this.chcTodosTractores.IsChecked = false;
                    this.chcTodosModalidades.IsChecked = false;
                    this.chcTodosDestinos.IsChecked = false;
                    OperationResult result = new ClienteSvc().getClientesALLorById(this.ctrEmpresa.empresaSelected.clave, 0, true);
                    if (result.typeResult == ResultTypes.success)
                    {
                        this.cbxClientes.ItemsSource = result.result as List<Cliente>;

                        cbxClientes.SelectedItem = cbxClientes.ItemsSource.Cast<Cliente>().ToList().Find(s => s.clave == mainWindow.usuario.cliente.clave);

                        goto Label_00EF;
                    }
                    ImprimirMensaje.imprimir(result);
                    base.IsEnabled = false;
                }
                return;
                Label_00EF:
                result2 = new OperadorSvc().getOperadoresALLorById(0, this.ctrEmpresa.empresaSelected.clave);
                if (result2.typeResult == ResultTypes.success)
                {
                    this.cbxOperadores.ItemsSource = result2.result as List<Personal>;
                }
                else
                {
                    ImprimirMensaje.imprimir(result2);
                    base.IsEnabled = false;
                    return;
                }
                OperationResult resp = new UnidadTransporteSvc().getTractores(this.ctrEmpresa.empresaSelected.clave, "%");
                if (resp.typeResult == ResultTypes.success)
                {
                    this.cbxTractores.ItemsSource = resp.result as List<UnidadTransporte>;
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    base.IsEnabled = false;
                    return;
                }
                OperationResult result4 = new ZonasSvc().getDestinosByIdEmpresa(this.ctrEmpresa.empresaSelected.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    this.cbxDestinos.ItemsSource = result4.result as List<Zona>;
                }
                else
                {
                    ImprimirMensaje.imprimir(result4);
                    base.IsEnabled = false;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void cbxPerfiles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender != null) && (this.cbxPerfiles.SelectedItem != null))
            {
                PerfilReporte selectedItem = this.cbxPerfiles.SelectedItem as PerfilReporte;
                this.txtNombrePerfil.Text = selectedItem.nombrePerfil;
                this.cargarChecColumnas(selectedItem.listDetalles);
            }
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                CheckBox box;
                base.Cursor = Cursors.Wait;
                if (sender != null)
                {
                    box = sender as CheckBox;
                    switch (box.Name)
                    {
                        case "chcTodosClientes":
                            if (box.IsChecked.Value)
                            {
                                cbxClientes.SelectAll();
                                this.cbxClientes.IsEnabled = false;
                            }
                            else
                            {
                                this.cbxClientes.SelectedItems.Clear();
                                this.cbxClientes.IsEnabled = true;
                            }
                            break;

                        case "chcTodosOperadores":
                            if (box.IsChecked.Value)
                            {
                                //foreach (object obj3 in this.cbxOperadores.ItemsSource)
                                //{
                                //    this.cbxOperadores.SelectedItems.Add(obj3);
                                //}
                                cbxOperadores.SelectAll();
                                this.cbxOperadores.IsEnabled = false;
                            }
                            else
                            {
                                this.cbxOperadores.SelectedItems.Clear();
                                this.cbxOperadores.IsEnabled = true;
                            }
                            break;

                        case "chcTodosTractores":
                            if (box.IsChecked.Value)
                            {
                                //foreach (object obj4 in this.cbxTractores.ItemsSource)
                                //{
                                //    this.cbxTractores.SelectedItems.Add(obj4);
                                //}
                                cbxTractores.SelectAll();
                                this.cbxTractores.IsEnabled = false;
                            }
                            else
                            {
                                this.cbxTractores.SelectedItems.Clear();
                                this.cbxTractores.IsEnabled = true;
                            }
                            break;

                        case "chcTodosModalidades":
                            if (box.IsChecked.Value)
                            {
                                //foreach (object obj5 in this.cbxModalidad.ItemsSource)
                                //{
                                //    this.cbxModalidad.SelectedItems.Add(obj5);
                                //}
                                cbxModalidad.SelectAll();
                                this.cbxModalidad.IsEnabled = false;
                            }
                            else
                            {
                                this.cbxModalidad.SelectedItems.Clear();
                                this.cbxModalidad.IsEnabled = true;
                            }
                            break;

                        case "chcTodosDestinos":
                            if (box.IsChecked.Value)
                            {
                                //foreach (object obj6 in this.cbxDestinos.ItemsSource)
                                //{
                                //    this.cbxDestinos.SelectedItems.Add(obj6);
                                //}
                                cbxDestinos.SelectAll();
                                this.cbxDestinos.IsEnabled = false;
                            }
                            else
                            {
                                this.cbxDestinos.SelectedItems.Clear();
                                this.cbxDestinos.IsEnabled = true;
                            }
                            break;

                        case "chcTodosEstatus":
                            goto Label_03A4;
                    }
                }
                return;
                Label_03A4:
                if (box.IsChecked.Value)
                {
                    //foreach (object obj7 in this.cbxEstatus.ItemsSource)
                    //{
                    //    this.cbxEstatus.SelectedItems.Add(obj7);
                    //}
                    cbxEstatus.SelectAll();
                    this.cbxEstatus.IsEnabled = false;
                }
                else
                {
                    this.cbxEstatus.SelectedItems.Clear();
                    this.cbxEstatus.IsEnabled = true;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void CheckBox_CheckedColom(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                if (sender != null)
                {
                    CheckBox chc = sender as CheckBox;
                    GridView view = (GridView)lvlCp.View;
                    if (chc.IsChecked.Value)
                    {
                        string[] strArray = chc.Tag.ToString().Split(',');
                        if (strArray.Length > 1)
                        {
                            GridViewColumn item = new GridViewColumn
                            {
                                Header = chc.Content
                            };
                            Binding binding1 = new Binding(strArray[0])
                            {
                                StringFormat = strArray[1]
                            };
                            item.DisplayMemberBinding = binding1;
                            item.Width = Convert.ToInt32(chc.DataContext);
                            view.Columns.Add(item);
                        }
                        else
                        {
                            GridViewColumn item = new GridViewColumn
                            {
                                Header = chc.Content,
                                DisplayMemberBinding = new Binding(strArray[0]),
                                Width = Convert.ToInt32(chc.DataContext)
                            };
                            view.Columns.Add(item);
                        }
                        this.listString.Add(chc.Content.ToString());
                        dictionaryStr.Add(chc.Content.ToString(), strArray[0]);
                    }
                    else
                    {
                        this.listString.Remove(chc.Content.ToString());
                        dictionaryStr.Remove(chc.Content.ToString());
                        List<GridViewColumn> list = view.Columns.Cast<GridViewColumn>().ToList<GridViewColumn>();
                        view.Columns.Remove(list.Find(s => s.Header.ToString() == chc.Content.ToString()));
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        private void rbnViaje_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                this.llenarLista();
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        private void Expander_Collapsed(object sender, RoutedEventArgs e)
        {
            this.lvlCp.Height += 130.0;
        }

        private void Expander_Expanded(object sender, RoutedEventArgs e)
        {
            this.lvlCp.Height -= 100.0;
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                if (!this.cargarPerfiles())
                {
                    base.IsEnabled = false;
                }
                this.dtpFechaInicio.SelectedDate = new DateTime?(DateTime.Now);
                this.dtpFechaFin.SelectedDate = new DateTime?(DateTime.Now);
                this.ctrEmpresa.cbxEmpresa.SelectionChanged += new SelectionChangedEventHandler(this.CbxEmpresa_SelectionChanged);
                this.ctrEmpresa.loaded(base.mainWindow.empresa, new PrivilegioSvc().consultarPrivilegio("isAdmin", base.mainWindow.usuario.idUsuario).typeResult == ResultTypes.success);
                List<Modalidad> list1 = new List<Modalidad>();
                Modalidad item = new Modalidad
                {
                    nombre = "SENCILLO",
                    letra = "S"
                };
                list1.Add(item);
                Modalidad modalidad2 = new Modalidad
                {
                    nombre = "FULL",
                    letra = "F"
                };
                list1.Add(modalidad2);
                this.cbxModalidad.ItemsSource = list1;
                List<string> list2 = new List<string> {
                    "ACTIVO",
                    "CANCELADO",
                    "FINALIZADO",
                    "LISTO PARA FACTURAR",
                    "EXTERNO",
                    "FACTURADO"
                };
                this.cbxEstatus.ItemsSource = list2;
                if (new PrivilegioSvc().consultarPrivilegio("MOSTRAR_IMPORTES_REPORTES", base.mainWindow.inicio._usuario.idUsuario).typeResult == ResultTypes.success)
                {
                    this.chcPrecioVar.Visibility = Visibility.Visible;
                    this.chcSubtotalVar.Visibility = Visibility.Visible;
                    this.chcPrecioFijo.Visibility = Visibility.Visible;
                    this.chcSubTotal.Visibility = Visibility.Visible;
                    this.chcIva.Visibility = Visibility.Visible;
                    this.chcRetencion.Visibility = Visibility.Visible;
                    this.stpSubTotal.Visibility = Visibility.Visible;
                    chcImporte.Visibility = Visibility.Visible;
                }
                else
                {
                    this.chcPrecioVar.Visibility = Visibility.Collapsed;
                    this.chcSubtotalVar.Visibility = Visibility.Collapsed;
                    this.chcPrecioFijo.Visibility = Visibility.Collapsed;
                    this.chcSubTotal.Visibility = Visibility.Collapsed;
                    this.chcIva.Visibility = Visibility.Collapsed;
                    this.chcRetencion.Visibility = Visibility.Collapsed;
                    this.stpSubTotal.Visibility = Visibility.Collapsed;
                    chcImporte.Visibility = Visibility.Collapsed;
                }
                base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                this.rbnViaje.IsChecked = true;

                stpClientes.IsEnabled = (new PrivilegioSvc().consultarPrivilegio("ADMIN_COMBO_CLIENTES", mainWindow.usuario.idUsuario)).typeResult == ResultTypes.success;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            popColumnas.IsOpen = !popColumnas.IsOpen;
        }
    }
}
