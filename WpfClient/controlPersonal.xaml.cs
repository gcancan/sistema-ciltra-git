﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ctr = MahApps.Metro.Controls;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for controlPersonal.xaml
    /// </summary>
    public partial class controlPersonal : UserControl
    {
        public controlPersonal()
        {
            InitializeComponent();
        }

        private eTipoBusquedaPersonal tipoBusqueda;
        private Empresa empresa;
        public void loaded(eTipoBusquedaPersonal tipoBusqueda, Empresa empresa)
        {
            this.tipoBusqueda = tipoBusqueda;
            this.empresa = empresa;
            this.idCliente = 0;
            limpiar();
        }
        int idDepartamento = 0;
        public void loadedByDepartamento(int idDepartamento)
        {
            this.tipoBusqueda = eTipoBusquedaPersonal.DEPARTAMENTO;
            this.idDepartamento = idDepartamento;
            limpiar();
        }
        public void loadedLisPersonal(List<Personal> listaPersonal)
        {
            this.listaPersonal = listaPersonal;
            limpiar();
        }
        List<Personal> listaPersonal = new List<Personal>();
        public void loadedAllPersonalActivo()
        {
            this.tipoBusqueda = eTipoBusquedaPersonal.ALL_PERSONAL_ACTIVO;
            limpiar();
        }
        private int idCliente = 0;
        public void loaded(eTipoBusquedaPersonal tipoBusqueda, int idCliente)
        {
            this.tipoBusqueda = tipoBusqueda;
            this.idCliente = idCliente;
            limpiar();
        }
        public string marcaAgua
        {
            set
            {
                ctr.TextBoxHelper.SetWatermark(txtPersonal, value);
            }
        }
        public void limpiar()
        {
            mapTxt(null);
        }

        private void txtPersonal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                abrirBuscador();
            }
        }

        public void abrirBuscador()
        {

            string clave = txtPersonal.Text.Trim();

            if (listaPersonal.Count > 0)
            {
                var personal = new selectPersonalV2(this.listaPersonal).getPersonal();
                mapTxt(personal);
            }
            else if (tipoBusqueda == eTipoBusquedaPersonal.DEPARTAMENTO)
            {
                var personal = new selectPersonalV2().getPersonalByDepartamento(this.idDepartamento);
                mapTxt(personal);
            }
            else if (tipoBusqueda == eTipoBusquedaPersonal.ALL_PERSONAL_ACTIVO)
            {
                var personal = new selectPersonalV2().getPersonalActivo();
                mapTxt(personal);
            }
            else
            {
                List<Personal> listPersonal = buscarPersonal(clave);

                if (listPersonal == null) return;


                if (listPersonal.Count == 1)
                {
                    mapTxt(listPersonal[0]);
                }
                else
                {
                    selectPersonal buscardor = new selectPersonal(clave, empresa == null ? 0 : empresa.clave);

                    Personal per = new Personal();
                    switch (tipoBusqueda)
                    {
                        case eTipoBusquedaPersonal.TODOS:
                            per = buscardor.buscarAllPersonal();
                            break;
                        case eTipoBusquedaPersonal.OPERADORES:
                            if (idCliente == 0)
                            {
                                per = buscardor.buscarPersonal();
                            }
                            else
                            {
                                per = buscardor.buscarOperadores(idCliente);
                            }
                            break;
                        case eTipoBusquedaPersonal.DESPACHADORES:
                            per = buscardor.buscarAllDespachador();
                            break;
                        case eTipoBusquedaPersonal.LAVADORES:
                            per = buscardor.buscarAllLavador();
                            break;
                        case eTipoBusquedaPersonal.LLANTEROS:
                            per = buscardor.buscarAllLlanteros();
                            break;
                        case eTipoBusquedaPersonal.MECANICOS:
                            per = buscardor.buscarAllMecanicos();
                            break;
                        default:
                            break;
                    }
                    mapTxt(per);
                }
            }
        }

        public Personal personal
        {
            get
            {
                return (Personal)txtPersonal.Tag;
            }
            set
            {
                mapTxt(value);
            }
        }

        private List<Personal> buscarPersonal(string clave)
        {
            OperationResult resp = new OperationResult();
            switch (tipoBusqueda)
            {
                case eTipoBusquedaPersonal.TODOS:
                    resp = new OperadorSvc().getPersonalByNombre(clave, empresa == null ? 0 : empresa.clave);
                    break;
                case eTipoBusquedaPersonal.OPERADORES:
                    resp = new OperadorSvc().getOperadoresByNombre(clave, (empresa == null ? 0 : empresa.clave), (idCliente));
                    break;
                case eTipoBusquedaPersonal.DESPACHADORES:
                    resp = new OperadorSvc().getDespachadoresByNombre(clave);
                    break;
                case eTipoBusquedaPersonal.LAVADORES:
                    resp = new OperadorSvc().getLavadoresByNombre(clave);
                    break;
                case eTipoBusquedaPersonal.LLANTEROS:
                    resp = new OperadorSvc().getLlanterosByNombre(clave);
                    break;
                case eTipoBusquedaPersonal.MECANICOS:
                    resp = new OperadorSvc().getMecanicosByNombre(clave);
                    break;
                default:
                    break;
            }
            return (List<Personal>)resp.result;
        }
        private bool _validacion { get; set; }
        public bool validacion
        {
            set
            {
                _validacion = value;
            }
        }
        private void mapTxt(Personal personal)
        {
            if (personal != null)
            {
                txtPersonal.Text = personal.nombre;
                txtPersonal.Tag = personal;
                txtPersonal.IsEnabled = false;
                DataContext = personal;
                Background = _validacion ? Brushes.Green : null;
            }
            else
            {
                txtPersonal.Clear();
                txtPersonal.Tag = personal;
                txtPersonal.IsEnabled = true;
                DataContext = null;
                Background = null;
            }
            DataContext = personal;
        }
    }
}
