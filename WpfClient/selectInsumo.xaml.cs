﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoreFletera.Models;
using Core.Models;
using CoreFletera.Interfaces;
using Core.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for selectInsumo.xaml
    /// </summary>
    public partial class selectInsumo : Window
    {
        string parametro = string.Empty;
        public selectInsumo(string parametro = "")
        {
            InitializeComponent();
            this.parametro = parametro;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtFind.Text = parametro;
            lvlInsumos.Focus();
        }

        public ProductoInsumo buscarTodos()
        {
            try
            {
                buscarAllInsumos();
                bool? resp = ShowDialog();
                if (resp.Value && lvlInsumos.SelectedItem != null)
                {
                    return (ProductoInsumo)((ListViewItem)lvlInsumos.SelectedItem).Content;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }

        public ProductoInsumo buscarTodos(bool taller = true, bool lavadero = true, bool llantera = true)
        {
            try
            {
                buscarAllInsumos(taller, lavadero, llantera);
                bool? resp = ShowDialog();
                if (resp.Value && lvlInsumos.SelectedItem != null)
                {
                    return (ProductoInsumo)((ListViewItem)lvlInsumos.SelectedItem).Content;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }

        private void buscarAllInsumos(bool taller = true, bool lavadero = true, bool llantera = true)
        {
            try
            {
                OperationResult resp = new OperacionSvc().getInsumosByNombre("", taller, lavadero, llantera);
                if (resp.typeResult == ResultTypes.success)
                {
                    llenarLista((List<ProductoInsumo>)resp.result);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                    DialogResult = false;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                DialogResult = false;
            }
        }

        private void llenarLista(List<ProductoInsumo> result)
        {
            lvlInsumos.Items.Clear();
            List<ListViewItem> listLvl = new List<ListViewItem>();
            foreach (var item in result)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvl.MouseDoubleClick += Lvl_MouseDoubleClick;
                listLvl.Add(lvl);
            }
            lvlInsumos.ItemsSource = listLvl;
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlInsumos.ItemsSource);
            view.Filter = UserFilter;
        }
        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((ProductoInsumo)(item as ListViewItem).Content).clave.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((ProductoInsumo)(item as ListViewItem).Content).nombre.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }
        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlInsumos.ItemsSource).Refresh();
        }

        private void lvlInsumos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }
    }
}
