﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using CoreFletera.Interfaces;
using CoreFletera.Models;
namespace WpfClient
{
    /// <summary>
    /// Interaction logic for ReporteHistorialUnidad.xaml
    /// </summary>
    public partial class ReporteHistorialUnidad : ViewBase
    {
        public ReporteHistorialUnidad()
        {
            InitializeComponent();
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            ctrEmpresas.cbxEmpresa.SelectionChanged += CbxEmpresa_SelectionChanged;
            ctrEmpresas.loaded(mainWindow.empresa, true);
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
        }

        public override void nuevo()
        {
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
            ctrUnidades.limpiar();
        }

        private void CbxEmpresa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ctrEmpresas.empresaSelected != null)
            {
                OperationResult resp = new UnidadTransporteSvc().getTractores(ctrEmpresas.empresaSelected.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    ctrUnidades.loaded(etipoUniadBusqueda.TRACTOR, ctrEmpresas.empresaSelected);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
        }

        private void btnImprimir_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime fechaInicial = ctrFechas.fechaInicial;
                DateTime fechaFinal = ctrFechas.fechaFinal;
                bool isMes = ctrFechas.isMes;
                UnidadTransporte unidad = ctrUnidades.unidadTransporte;
                if (unidad != null)
                {
                    Cursor = Cursors.Wait;
                    OperationResult resp = new KardexEntSalUnidSvc().getKardexEntSalByFechas(fechaInicial, fechaFinal, unidad.clave);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        ReportView reporteador = new ReportView();
                        if (reporteador.ReporteKardexEntSal(fechaInicial, fechaFinal, (List<KardexEntSalUnid>)resp.result, unidad, isMes, ctrEmpresas.empresaSelected));
                        {
                            reporteador.ShowDialog();
                        }
                    }
                    else
                    {
                        ImprimirMensaje.imprimir(resp);
                    }
                }
                else
                {
                    ImprimirMensaje.imprimir(new OperationResult { valor = 2, mensaje = "No se selecciono unidad de transporte" });
                }
                
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
                ctrUnidades.limpiar();
            }
        }
    }
}