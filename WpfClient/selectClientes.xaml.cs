﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectClientes.xaml
    /// </summary>
    public partial class selectClientes : Window
    {
        public selectClientes()
        {
            InitializeComponent();
        }
        private string parametro = string.Empty;
        public selectClientes(string parametro)
        {
            InitializeComponent();
            this.parametro = parametro;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtBuscador.Text = this.parametro;
        }
        public Cliente getAllClientesByEmpresa(int idEmpresa)
        {
            try
            {
                buscarClientesByEmpresa(idEmpresa);
                bool? resp = ShowDialog();
                if (resp.Value && lvlClientes.SelectedItem != null)
                {
                    return (lvlClientes.SelectedItem as ListViewItem).Content as Cliente;
                }
                else return null;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }
        private void buscarClientesByEmpresa(int idEmpresa)
        {
            try
            {
                Cursor = Cursors.Arrow;
                var resp = new ClienteSvc().getAllClienteByEmpresa(idEmpresa);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapLista(resp.result as List<Cliente>);
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
            
        }

        private void mapLista(List<Cliente> listaCliente)
        {
            try
            {
                lvlClientes.ItemsSource = null;
                List<ListViewItem> list = new List<ListViewItem>();
                foreach (Cliente col in listaCliente)
                {
                    ListViewItem item = new ListViewItem
                    {
                        Content = col
                    };
                    item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                    list.Add(item);
                }
                this.lvlClientes.ItemsSource = list;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlClientes.ItemsSource);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtBuscador.Text) ||
            ((((Cliente)(item as ListViewItem).Content).nombre.IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }
        private void TxtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(this.lvlClientes.ItemsSource).Refresh();
        }

    }
}
