﻿using Core.Models;
using Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for avisoViajesActivos.xaml
    /// </summary>
    public partial class avisoViajesActivos : Window
    {
        public avisoViajesActivos()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Opacity = .92;
        }
        string idTractor = string.Empty;
        string persona = string.Empty;
        public void mostrarLista(List<Viaje> listaViajes, string idTractor, string persona)
        {
            this.persona = persona;
            this.idTractor = idTractor;
            lvlViajes.ItemsSource = listaViajes;
            ShowDialog();
        }

        private void btnImprimir_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                ReportView reporte = new ReportView();
                reporte.imprimirViajesActivos(lvlViajes.ItemsSource as List<Viaje>, idTractor, persona ,new Util().getRutasImpresora("VIAJES_ATLANTE"));
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
