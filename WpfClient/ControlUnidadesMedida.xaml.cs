﻿using Core.Interfaces;
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para ControlUnidadesMedida.xaml
    /// </summary>
    public partial class ControlUnidadesMedida : UserControl
    {
        public ControlUnidadesMedida()
        {
            InitializeComponent();
        }
        private string um = string.Empty;
        public ControlUnidadesMedida(string um)
        {
            InitializeComponent();
            this.um = um;
            
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var resp = new ProductoSvc().getAllUnidadesMedida();
            if (resp.typeResult == ResultTypes.success)
            {
                cbxUnidadMedida.ItemsSource = resp.result as List<UnidadMedida>;
            }

            if (!string.IsNullOrEmpty(um))
            {
                cbxUnidadMedida.SelectedItem = cbxUnidadMedida.ItemsSource.Cast<UnidadMedida>().ToList().Find(s => s.medida == um);
            }
            if (cbxUnidadMedida.SelectedItem == null)
            {
                cbxUnidadMedida.SelectedIndex = 0;
            }

            //unidadMedida = this.um;
        }
        public string unidadMedida
        {
            get
            {
                if (cbxUnidadMedida.SelectedItem == null)
                {
                    return string.Empty;
                }
                return (cbxUnidadMedida.SelectedItem as UnidadMedida).medida;
            }
            set
            {
                if (string.IsNullOrEmpty(um))
                {
                    cbxUnidadMedida.SelectedItem = cbxUnidadMedida.ItemsSource.Cast<UnidadMedida>().ToList().Find(s => s.medida == value);
                }
                if (cbxUnidadMedida.SelectedItem == null)
                {
                    cbxUnidadMedida.SelectedIndex = 0;
                }
            }
        }
    }
}
