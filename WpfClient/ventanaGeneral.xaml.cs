﻿namespace WpfClient
{
    using Core.Models;
    using CoreFletera.Models;
    using System;
    using System.CodeDom.Compiler;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Runtime.InteropServices;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Markup;

    public partial class ventanaGeneral : Window
    {
        private DateTime fechaInicial;
        private Empresa empresa;
        private DateTime fechaFinal;
        private MainWindow mainWindow;
        private string tractor;
        private Empresa empresaSelected;
        private int idCliente;
        private BitacoraLavadero bitacora;
       
        public ventanaGeneral()
        {
            this.idCliente = 0;
            this.InitializeComponent();
        }

        public ventanaGeneral(BitacoraLavadero bitacora)
        {
            this.idCliente = 0;
            this.bitacora = bitacora;
            this.InitializeComponent();
        }

        public ventanaGeneral(MainWindow mainWindow)
        {
            this.idCliente = 0;
            this.mainWindow = mainWindow;
            this.InitializeComponent();
        }

        public ventanaGeneral(Empresa empresa, DateTime fechaInicial, DateTime fechaFinal, string tractor)
        {
            this.idCliente = 0;
            this.empresa = empresa;
            this.fechaInicial = fechaInicial;
            this.fechaFinal = fechaFinal;
            this.tractor = tractor;
            this.InitializeComponent();
        }

        public ventanaGeneral(MainWindow mainWindow, Empresa empresa, DateTime fechaInicial, DateTime fechaFinal, string tractor, int idCliente = 0)
        {
            this.idCliente = 0;
            this.InitializeComponent();
            this.idCliente = idCliente;
            this.empresa = empresa;
            this.fechaInicial = fechaInicial;
            this.fechaFinal = fechaFinal;
            this.tractor = tractor;
            this.mainWindow = mainWindow;
        }

        internal void abrirBitacoraLavadero()
        {
            reporteLavadero lavadero = new reporteLavadero(this.empresa, this.tractor, this.fechaInicial, this.fechaFinal);
            this.controlVista.Content = lavadero;
            base.ShowDialog();
        }

        internal void abrirCargasCombustible()
        {
            ReporteBitacoraCargaCombustible combustible = new ReporteBitacoraCargaCombustible(this.empresa, this.fechaInicial, this.fechaFinal, this.tractor);
            this.controlVista.Content = combustible;
            base.ShowDialog();
        }

        internal void abrirDetalleLavadero()
        {
            detalleLavadero lavadero = new detalleLavadero(this.bitacora);
            this.controlVista.Content = lavadero;
            base.ShowDialog();
        }

        internal void abrirInfoViajes(int idViaje)
        {
            controlViajes viajes = new controlViajes(idViaje, this.mainWindow);
            this.controlVista.Content = viajes;
            base.ShowDialog();
        }

        internal void abrirViajes()
        {
            exportarExcel excel = new exportarExcel(this.mainWindow, this.empresa, this.fechaInicial, this.fechaFinal, this.tractor, this.idCliente);
            base.WindowState = WindowState.Maximized;
            base.SizeToContent = SizeToContent.Manual;
            this.controlVista.Content = excel;
            base.ShowDialog();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }
    }
}
