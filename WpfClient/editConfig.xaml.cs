﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Utils;
using System.Net.Mail;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for editConfig.xaml
    /// </summary>

    public partial class editConfig : Window
    {
        Puerto puerto = new Puerto();
        SqlConnectionStringBuilder con = new SqlConnectionStringBuilder();
        SqlConnectionStringBuilder con2 = new SqlConnectionStringBuilder();
        SqlConnectionStringBuilder con3 = new SqlConnectionStringBuilder();
        SqlConnection sql = new SqlConnection();
        public editConfig()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //foreach (var item in puerto.getPuertos())
            //{
            //    cbxPuertos.Items.Add(item);
            //}
            //cbxDataBits.Items.Add("8");
            //cbxDataBits.SelectedIndex = 0;

            //cbxTiempo.Items.Add(5);
            //cbxTiempo.Items.Add(10);
            //cbxTiempo.Items.Add(15);
            //cbxTiempo.Items.Add(20);
            //cbxTiempo.Items.Add(25);
            //cbxTiempo.Items.Add(30);

            cbxHabilitatSSL.Items.Add("Si");
            cbxHabilitatSSL.Items.Add("No");

            loadConfig();
        }

        private void loadConfig()
        {
            con = new Util().getConectionString();
            txtNameServer.Text = con.DataSource;
            txtNameDB.Text = con.InitialCatalog;
            txtNameUser.Text = con.UserID;
            txtPass.Password = con.Password;

            //con2 = new Util().getConectionStringComercial();
            //txtNameServerComercial.Text = con2.DataSource;
            //txtNameDBComercial.Text = con2.InitialCatalog;
            //txtNameUserComercial.Text = con2.UserID;
            //txtPassComercial.Password = con2.Password;

            //con3 = new Util().getConectionStringComercialATLAS();
            //txtNameServerComercialAtlas.Text = con3.DataSource;
            //txtNameDBComercialAtlas.Text = con3.InitialCatalog;
            //txtNameUserComercialAtlas.Text = con3.UserID;
            //txtPassComercialAtlas.Password = con3.Password;

            //var x = puerto.abrirPuerto();
            //cbxPuertos.SelectedItem = puerto.RFID.PortName;
            //txtBaudRate.Text = puerto.RFID.BaudRate.ToString();
            //puerto.RFID.Close();

            SmtpClient smtp = new Util().getSmtpClient();
            txtHost.Text = smtp.Host;
            txtPuerto.Text = smtp.Port.ToString();

            var path = System.Windows.Forms.Application.StartupPath;
            string archivo = path + @"\config.ini";

            txtUsuario.Text = new Util().Read("CONFIGURACION_CORREO", "USUARIO", archivo);
            txtPassCorreo.Password = new Util().Read("CONFIGURACION_CORREO", "PASS", archivo);

            cbxHabilitatSSL.SelectedIndex = new Util().Read("CONFIGURACION_CORREO", "SSL", archivo) == "true" ? 0 : 1;

            //cbxTiempo.SelectedItem = new Util().getTiempoEspera();
        }

        private void btnProbarConexion_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Util u = new Util();
                SqlConnection s = new SqlConnection();
                s.ConnectionString = new SqlConnectionStringBuilder
                {
                    DataSource = txtNameServer.Text.Trim(),
                    InitialCatalog = txtNameDB.Text.Trim(),
                    UserID = txtNameUser.Text.Trim(),
                    Password = txtPass.Password.Trim()
                }.ToString();

                s.Open();
                MessageBox.Show("Conexion Exitosa");
                s.Close();
            }
            catch (Exception eSQL)
            {
                ImprimirMensaje.imprimir(eSQL);
                //MessageBox.Show(eSQL.Message);
            }
        }

        private void btnProbarConexionComercial_Click(object sender, RoutedEventArgs e)
        {
            //try
            //{
            //    Util u = new Util();
            //    SqlConnection s = new SqlConnection();
            //    s.ConnectionString = new SqlConnectionStringBuilder
            //    {
            //        DataSource = txtNameServerComercial.Text.Trim(),
            //        InitialCatalog = txtNameDBComercial.Text.Trim(),
            //        UserID = txtNameUserComercial.Text.Trim(),
            //        Password = txtPassComercial.Password.Trim()
            //    }.ToString();

            //    s.Open();
            //    MessageBox.Show("Conexion Exitosa");
            //    s.Close();
            //}
            //catch (Exception eSQL)
            //{
            //    ImprimirMensaje.imprimir(eSQL);
            //    //MessageBox.Show(eSQL.Message);
            //}
        }

        private void btnProbarConexionPuerto_Click(object sender, RoutedEventArgs e)
        {
            //try
            //{
            //    int x;
            //    if (!int.TryParse(txtBaudRate.Text.Trim(), out x))
            //    {
            //        MessageBox.Show(string.Format("La velocidad de los datos no es valida"), "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
            //        return;
            //    }
            //    SerialPort serial = new SerialPort()
            //    {
            //        PortName = cbxPuertos.SelectedItem.ToString(),
            //        BaudRate = Convert.ToInt32(txtBaudRate.Text),
            //        DataBits = Convert.ToInt32(cbxDataBits.SelectedItem),
            //        Parity = Parity.None,
            //        StopBits = StopBits.One,
            //        ReadTimeout = 300
            //    };
            //    serial.Open();
            //    MessageBox.Show("Conexion Exitosa");
            //    serial.Close();
            //}
            //catch (Exception com)
            //{
            //    MessageBox.Show(com.Message);
            //}
        }

        string cadena;
        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                cadena = "";
            }
            else
            {
                cadena += e.Key;
                if (cadena.ToUpper() == "ADMINSYSTEM")
                {
                    gridPrincipal.IsEnabled = true;
                }
            }
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (!validar())
            {
                MessageBox.Show(string.Format("Aun faltan campos por completar o no son validos"), "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            guardar();
            this.Close();
        }

        private void guardar()
        {
            var path = System.Windows.Forms.Application.StartupPath;
            Util u = new Util();

            string archivo = path + @"\config.ini";
            u.Write("CONECTION_STRING", "SERVER_NAME", txtNameServer.Text.Trim(), archivo);
            u.Write("CONECTION_STRING", "DB_NAME", txtNameDB.Text.Trim(), archivo);
            u.Write("CONECTION_STRING", "USER", txtNameUser.Text.Trim(), archivo);
            u.Write("CONECTION_STRING", "PASSWORD", txtPass.Password.Trim(), archivo);

            //u.Write("CONECTION_STRING_COMERCIAL", "SERVER_NAME", txtNameServerComercial.Text.Trim(), archivo);
            //u.Write("CONECTION_STRING_COMERCIAL", "DB_NAME", txtNameDBComercial.Text.Trim(), archivo);
            //u.Write("CONECTION_STRING_COMERCIAL", "USER", txtNameUserComercial.Text.Trim(), archivo);
            //u.Write("CONECTION_STRING_COMERCIAL", "PASSWORD", txtPassComercial.Password.Trim(), archivo);

            //u.Write("CONECTION_STRING_COMERCIAL_ATLAS", "SERVER_NAME", txtNameServerComercialAtlas.Text.Trim(), archivo);
            //u.Write("CONECTION_STRING_COMERCIAL_ATLAS", "DB_NAME", txtNameDBComercialAtlas.Text.Trim(), archivo);
            //u.Write("CONECTION_STRING_COMERCIAL_ATLAS", "USER", txtNameUserComercialAtlas.Text.Trim(), archivo);
            //u.Write("CONECTION_STRING_COMERCIAL_ATLAS", "PASSWORD", txtPassComercialAtlas.Password.Trim(), archivo);

            //u.Write("CONECTION_STRING_TALLER", "SERVER_NAME", txtNameServerComercialAtlas.Text.Trim(), archivo);
            //u.Write("CONECTION_STRING_TALLER", "DB_NAME", txtNameDBComercialAtlas.Text.Trim(), archivo);
            //u.Write("CONECTION_STRING_TALLER", "USER", txtNameUserComercialAtlas.Text.Trim(), archivo);
            //u.Write("CONECTION_STRING_TALLER", "PASSWORD", txtPassComercialAtlas.Password.Trim(), archivo);

            //u.Write("PUERTO COM", "COM", cbxPuertos.SelectedItem == null ? "COM3" : cbxPuertos.SelectedItem.ToString(), archivo);
            //int x;
            //int.TryParse(txtBaudRate.Text.Trim(), out x);
            //u.Write("PUERTO COM", "BAUD_RADE", x.ToString(), archivo);
            //u.Write("PUERTO COM", "DATA_BITS", cbxDataBits.SelectedItem.ToString(), archivo);

            //u.Write("LAPSO TIEMPO", "TIEMPO", cbxTiempo.SelectedItem.ToString(), archivo);

            u.Write("CONFIGURACION_CORREO", "HOST", txtHost.Text.Trim(), archivo);
            u.Write("CONFIGURACION_CORREO", "PUERTO", txtPuerto.Text.Trim(), archivo);
            u.Write("CONFIGURACION_CORREO", "USUARIO", txtUsuario.Text.Trim(), archivo);
            u.Write("CONFIGURACION_CORREO", "PASS", txtPassCorreo.Password.Trim(), archivo);
            u.Write("CONFIGURACION_CORREO", "SSL", cbxHabilitatSSL.SelectedItem.ToString() == "Si" ? "true" : "false", archivo);


        }

        private bool validar()
        {
            try
            {
                if (string.IsNullOrEmpty(txtNameServer.Text))
                {
                    return false;
                }
                if (string.IsNullOrEmpty(txtNameDB.Text))
                {
                    return false;
                }
                if (string.IsNullOrEmpty(txtNameUser.Text))
                {
                    return false;
                }
                if (string.IsNullOrEmpty(txtPass.Password))
                {
                    return false;
                }

                //if (string.IsNullOrEmpty(txtNameServerComercial.Text))
                //{
                //    return false;
                //}
                //if (string.IsNullOrEmpty(txtNameDBComercial.Text))
                //{
                //    return false;
                //}
                //if (string.IsNullOrEmpty(txtNameUserComercial.Text))
                //{
                //    return false;
                //}
                //if (string.IsNullOrEmpty(txtPassComercial.Password))
                //{
                //    return false;
                //}

                //if (cbxPuertos.SelectedItem == null)
                //{
                //    return false;
                //}
                //if (string.IsNullOrEmpty(txtBaudRate.Text))
                //{
                //    return false;
                //}
                //if (cbxDataBits.SelectedItem == null)
                //{
                //    return false;
                //}
                //int x;
                //var b = int.TryParse(txtBaudRate.Text.Trim(), out x);
                //if (!b)
                //{
                //    return false;
                //}
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnPruebaCorreo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SmtpClient smtp = new SmtpClient
                {
                    Host = txtHost.Text.Trim(),
                    Port = Convert.ToInt32(txtPuerto.Text.Trim()),
                    EnableSsl = cbxHabilitatSSL.SelectedItem.ToString() == "Si" ? true : false,
                    Credentials = new System.Net.NetworkCredential(txtUsuario.Text.Trim(), txtPassCorreo.Password)
                };
                EnvioCorreo correoPrueba = new EnvioCorreo();
                MessageBox.Show(  correoPrueba.envioCorreoPrueba(smtp, txtUsuario.Text.Trim()));

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void btnProbarConexionComercialAtlas_Click(object sender, RoutedEventArgs e)
        {
            //try
            //{
            //    Util u = new Util();
            //    SqlConnection s = new SqlConnection();
            //    s.ConnectionString = new SqlConnectionStringBuilder
            //    {
            //        DataSource = txtNameServerComercialAtlas.Text.Trim(),
            //        InitialCatalog = txtNameDBComercialAtlas.Text.Trim(),
            //        UserID = txtNameUserComercialAtlas.Text.Trim(),
            //        Password = txtPassComercialAtlas.Password.Trim()
            //    }.ToString();

            //    s.Open();
            //    MessageBox.Show("Conexion Exitosa");
            //    s.Close();
            //}
            //catch (Exception eSQL)
            //{
            //    ImprimirMensaje.imprimir(eSQL);
            //    //MessageBox.Show(eSQL.Message);
            //}
        }
    }

}
