﻿using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editInventarioLlantas.xaml
    /// </summary>
    public partial class editInventarioLlantas : Window
    {
        private EnumTipoUbicacionLlanta tipoUbicacionLlanta;
        private Empresa empresa;
        private UnidadTransporte unidarTrans;
        private int? posicionLlanta;
        private MainWindow mainWindow;
        private bool flotante;
        private Llanta nuevaLlanta;
        public editInventarioLlantas()
        {
            tipoUbicacionLlanta = EnumTipoUbicacionLlanta.UNIDAD_TRANSPORTE;
            empresa = null;
            unidarTrans = null;
            posicionLlanta = null;
            mainWindow = null;
            flotante = false;
            nuevaLlanta = null;
            InitializeComponent();
        }

        public editInventarioLlantas(Empresa empresa, UnidadTransporte unidarTrans, int posicionLlanta, MainWindow mainWindow)
        {
            tipoUbicacionLlanta = EnumTipoUbicacionLlanta.UNIDAD_TRANSPORTE;
            this.empresa = null;
            this.unidarTrans = null;
            this.posicionLlanta = null;
            this.mainWindow = null;
            flotante = false;
            nuevaLlanta = null;
            InitializeComponent();
            this.empresa = empresa;
            this.unidarTrans = unidarTrans;
            this.posicionLlanta = new int?(posicionLlanta);
            this.mainWindow = mainWindow;
        }
        private void btnCambiar_Click(object sender, RoutedEventArgs e)
        {
            this.ctrUnidadTrans.limpiar();
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                if (this.validarForm())
                {
                    Llanta llanta = this.mapForm();
                    if ((llanta != null) && (new LlantaSvc().saveLlanta(ref llanta, this.mainWindow.usuario.nombreUsuario).typeResult == ResultTypes.success))
                    {
                        this.nuevaLlanta = llanta;
                        if (this.flotante)
                        {
                            base.DialogResult = true;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void cbxTipoProductoLlanta_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.cbxTipoProductoLlanta.SelectedItem != null)
            {
                LLantasProducto selectedItem = this.cbxTipoProductoLlanta.SelectedItem as LLantasProducto;
                this.txtMarca.Text = selectedItem.marca.descripcion;
                this.txtDiseño.Text = selectedItem.diseño.descripcion;
                this.txtMedida.Text = (selectedItem.diseño.medida == null) ? string.Empty : selectedItem.diseño.medida.medida;
                this.txtProfundidad.Text = selectedItem.diseño.profundidad.ToString();
                this.txtTipoLlanta.Text = (selectedItem.diseño.tipoLlanta == null) ? string.Empty : selectedItem.diseño.tipoLlanta.nombre;
            }
            else
            {
                this.txtMarca.Clear();
                this.txtDiseño.Clear();
                this.txtMedida.Clear();
                this.txtProfundidad.Clear();
            }
        }

        private void CbxTipoUbicacion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.cbxTipoUbicacion.SelectedItem != null)
            {
                switch (((EnumTipoUbicacionLlanta)this.cbxTipoUbicacion.SelectedItem))
                {
                    case EnumTipoUbicacionLlanta.ALMACEN:
                        this.stpAlmacen.Visibility = Visibility.Visible;
                        this.stpProveedor.Visibility = Visibility.Collapsed;
                        this.stpUnidad.Visibility = Visibility.Collapsed;
                        break;

                    case EnumTipoUbicacionLlanta.PROVEEDOR:
                        this.stpAlmacen.Visibility = Visibility.Collapsed;
                        this.stpProveedor.Visibility = Visibility.Visible;
                        this.stpUnidad.Visibility = Visibility.Collapsed;
                        break;

                    case EnumTipoUbicacionLlanta.UNIDAD_TRANSPORTE:
                        this.stpAlmacen.Visibility = Visibility.Collapsed;
                        this.stpProveedor.Visibility = Visibility.Collapsed;
                        this.stpUnidad.Visibility = Visibility.Visible;
                        break;

                    case EnumTipoUbicacionLlanta.PILA_DESECHOS:
                        this.stpAlmacen.Visibility = Visibility.Collapsed;
                        this.stpProveedor.Visibility = Visibility.Visible;
                        this.stpUnidad.Visibility = Visibility.Collapsed;
                        break;
                }
            }
        }

        private void chcRevitalizado_Checked(object sender, RoutedEventArgs e)
        {
            this.cbxDiseñoActual.IsEnabled = true;
        }

        private void chcRevitalizado_Unchecked(object sender, RoutedEventArgs e)
        {
            this.cbxDiseñoActual.IsEnabled = false;
            this.cbxDiseñoActual.SelectedItem = null;
        }

        public Llanta crearInventarioLlanta()
        {
            try
            {
                this.flotante = true;
                if (base.ShowDialog().Value)
                {
                    return this.nuevaLlanta;
                }
                return null;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return null;
            }
        }

        private void ctrEmpresa_Loaded(object sender, RoutedEventArgs e)
        {
            this.ctrEmpresa.loaded(this.empresa, this.empresa == null);
            this.cbxTipoUbicacion.SelectionChanged += new SelectionChangedEventHandler(this.CbxTipoUbicacion_SelectionChanged);
            this.cbxTipoUbicacion.ItemsSource = Enum.GetValues(typeof(EnumTipoUbicacionLlanta));
            this.cbxAlmacen.ItemsSource = Enum.GetValues(typeof(EnumAlmacenLlantas));
            if (this.flotante)
            {
                this.cbxTipoUbicacion.SelectedItem = this.tipoUbicacionLlanta;
                this.cbxTipoUbicacion.IsEnabled = false;
                this.ctrUnidadTrans.unidadTransporte = this.unidarTrans;
                this.selectPosicion.Value = this.posicionLlanta;
                this.stpUnidad.IsEnabled = false;
            }
            else
            {
                this.cbxTipoUbicacion.SelectedIndex = 0;
            }
            OperationResult resp = new LlantaSvc().getAllLLantasProducto();
            if (resp.typeResult == ResultTypes.success)
            {
                this.cbxTipoProductoLlanta.ItemsSource = resp.result as List<LLantasProducto>;
            }
            else
            {
                ImprimirMensaje.imprimir(resp);
                base.IsEnabled = false;
                return;
            }
            OperationResult result2 = new ProveedorSvc().getAllProveedores();
            if (result2.typeResult == ResultTypes.success)
            {
                this.cbxProveedor.ItemsSource = result2.result as List<Proveedor>;
            }
            else
            {
                ImprimirMensaje.imprimir(result2);
                base.IsEnabled = false;
                return;
            }
            OperationResult result3 = new DiseñosLlantaSvc().getDiseñosLlantaAllOrById(0);
            if (result3.typeResult == ResultTypes.success)
            {
                this.cbxDiseñoActual.ItemsSource = result3.result as List<Diseño>;
            }
            else
            {
                ImprimirMensaje.imprimir(result3);
                base.IsEnabled = false;
                return;
            }
            OperationResult result4 = new LlantaSvc().getCondicionLlanta();
            if (result4.typeResult == ResultTypes.success)
            {
                this.cbxCondicionLlanta.ItemsSource = result4.result as List<CondicionLlanta>;
            }
            else
            {
                ImprimirMensaje.imprimir(result4);
                base.IsEnabled = false;
            }
        }
        private Llanta mapForm()
        {
            try
            {
                EnumTipoUbicacionLlanta selectedItem = (EnumTipoUbicacionLlanta)this.cbxTipoUbicacion.SelectedItem;
                LLantasProducto producto = this.cbxTipoProductoLlanta.SelectedItem as LLantasProducto;
                Llanta llanta2 = new Llanta
                {
                    idLlanta = (this.ctrUnidadTrans.Tag == null) ? 0 : (this.ctrUnidadTrans.Tag as Llanta).idLlanta,
                    clave = this.txtFolioLlanta.Text,
                    idEmpresa = this.ctrEmpresa.empresaSelected.clave,
                    productoLLanta = producto,
                    tipoLlanta = producto.diseño.tipoLlanta,
                    factura = this.txtFactura.Text,
                    fechaFactura = this.dtpFechaCompra.SelectedDate.Value,
                    fecha = DateTime.Now,
                    TipoUbicacionLlanta = selectedItem,
                    costo = this.ctrCosto.valor,
                    rfid = string.Empty,
                    posicion = (selectedItem == EnumTipoUbicacionLlanta.UNIDAD_TRANSPORTE) ? this.selectPosicion.Value.Value : 0,
                    año = this.dtpFechaCompra.SelectedDate.Value.Year,
                    mes = this.dtpFechaCompra.SelectedDate.Value.Month,
                    condicionLlanta = this.cbxCondicionLlanta.SelectedItem as CondicionLlanta,
                    profundidadActual = this.txtProfundidadActual.valor,
                    estatus = this.chcActivo.IsChecked.Value ? "ACTIVO" : "BAJA",
                    revitalizada = this.chcRevitalizado.IsChecked.Value,
                    objDiseño = this.chcRevitalizado.IsChecked.Value ? (this.cbxDiseñoActual.SelectedItem as Diseño) : null,
                    observaciones = this.txtObesevaciones.Text,
                    unidadTransporte = this.ctrUnidadTrans.unidadTransporte,
                    DOT = this.ctrDot.valor
                };
                LLantasProducto producto2 = this.cbxTipoProductoLlanta.SelectedItem as LLantasProducto;
                return llanta2;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                return null;
            }
        }
        public bool validarForm()
        {
            if (this.ctrEmpresa.empresaSelected == null)
            {
                ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "SELECCIONAR UNA EMPRESA", null));
                return false;
            }
            if (this.cbxTipoUbicacion.SelectedItem == null)
            {
                ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "SELECCIONAR EL TIPO DE UBICACI\x00d3N DE LA LLANTA", null));
                return false;
            }
            switch (((EnumTipoUbicacionLlanta)this.cbxTipoUbicacion.SelectedItem))
            {
                case EnumTipoUbicacionLlanta.ALMACEN:
                    if (this.cbxAlmacen.SelectedItem != null)
                    {
                        break;
                    }
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "SELECCIONAR UN ALMACEN", null));
                    return false;

                case EnumTipoUbicacionLlanta.PROVEEDOR:
                    if (this.cbxProveedor.SelectedItem != null)
                    {
                        break;
                    }
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "SELECCIONAR UN PROVEEDOR", null));
                    return false;

                case EnumTipoUbicacionLlanta.UNIDAD_TRANSPORTE:
                    if (this.ctrUnidadTrans.unidadTransporte != null)
                    {
                        if (!this.selectPosicion.Value.HasValue)
                        {
                            ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "PROPORCIONAR LA POSICI\x00d3N DE LA LLANTA", null));
                            return false;
                        }
                        break;
                    }
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "PROPORCIONAR UNA UNIDAD DE TRANSPORTE", null));
                    return false;

                case EnumTipoUbicacionLlanta.PILA_DESECHOS:
                    if (this.cbxAlmacen.SelectedItem != null)
                    {
                        break;
                    }
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "SELECCIONAR UN ALMACEN", null));
                    return false;
            }
            if (string.IsNullOrEmpty(this.txtFolioLlanta.Text.Trim()))
            {
                ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "EL FOLIO DE LA LLANTA NO PUEDE SER VAC\x00cdO", null));
                this.txtFolioLlanta.Focus();
                return false;
            }
            if (this.ctrDot.valor <= 0)
            {
                ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "SE REQUIERE ESPECIFICAR EL DOT", null));
                this.txtFolioLlanta.Focus();
                return false;
            }
            if (this.cbxTipoProductoLlanta.SelectedItem == null)
            {
                ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "SELECCIONAR UNA CLASIFICACI\x00d3N DE LA LLANTA", null));
                return false;
            }
            LLantasProducto selectedItem = this.cbxTipoProductoLlanta.SelectedItem as LLantasProducto;
            if (selectedItem.diseño.medida == null)
            {
                ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "EL PRODUCTO DE LLANTA QUE SELECCIONO NO TIENE MEDIDA ESPECIFICADA", null));
                return false;
            }
            if (selectedItem.diseño.profundidad <= 0)
            {
                ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "EL PRODUCTO DE LLANTA QUE SELECCIONO NO TIENE PROFUNDIDAD ESPECIFICADA", null));
                return false;
            }
            if (selectedItem.diseño.tipoLlanta == null)
            {
                ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "EL PRODUCTO DE LLANTA QUE SELECCIONO NO TIENE TIPO DE LLANTA ESPECIFICADA", null));
                return false;
            }
            if (this.txtProfundidadActual.valor <= decimal.Zero)
            {
                ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "LA PROFUNDIDAD ACTUAL NO PUEDE SER 0 O MENOS DE 0", null));
                return false;
            }
            if (this.chcRevitalizado.IsChecked.Value && (this.cbxDiseñoActual.SelectedItem == null))
            {
                ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "SI LA LLANTA ES REVITALIZADA, SELECCIONE UN DISE\x00d1O", null));
                return false;
            }
            if (this.cbxCondicionLlanta.SelectedItem == null)
            {
                ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "SELECCIONAR LA CONDICION ACTUAL DE LA LLANTA", null));
                return false;
            }
            return true;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.dtpFechaCompra.SelectedDate = new DateTime?(DateTime.Now);
            this.ctrDot.establecerMaxDigitos(4);
        }

    }
}
