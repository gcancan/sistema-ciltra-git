﻿using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para catMedidasLlantas.xaml
    /// </summary>
    public partial class catMedidasLlantas : ViewBase
    {
        public catMedidasLlantas()
        {
            InitializeComponent();
        }
        public override OperationResult guardar()
        {
            try
            {
                if (string.IsNullOrEmpty(this.txtMedida.Text.Trim()))
                {
                    return new OperationResult(ResultTypes.success, "SE NECESITA PROPORCIONAR LA MEDIDA", null);
                }
                MedidaLlanta medidaLlanta = this.mapForm();
                OperationResult result = new MedidaLlantaSvc().saveMedidaLlantas(ref medidaLlanta);
                if (result.typeResult == ResultTypes.success)
                {
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                    this.mapForm(medidaLlanta);
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }
        public MedidaLlanta mapForm()
        {
            try
            {
                return new MedidaLlanta
                {
                    idMedidaLlanta = (this.ctrClave.Tag == null) ? 0 : (this.ctrClave.Tag as MedidaLlanta).idMedidaLlanta,
                    medida = this.txtMedida.Text.Trim()
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void mapForm(MedidaLlanta medidaLlanta)
        {
            try
            {
                if (medidaLlanta != null)
                {
                    this.ctrClave.Tag = medidaLlanta;
                    this.ctrClave.valor = medidaLlanta.idMedidaLlanta;
                    this.txtMedida.Text = medidaLlanta.medida;
                }
                else
                {
                    this.ctrClave.Tag = null;
                    this.ctrClave.limpiar();
                    this.txtMedida.Clear();
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        public override void nuevo()
        {
            base.nuevo();
            this.mapForm(null);
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            this.nuevo();
        }
    }
    
}
