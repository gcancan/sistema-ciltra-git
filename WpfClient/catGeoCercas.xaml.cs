﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Markup;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para catGeoCercas.xaml
    /// </summary>
    public partial class catGeoCercas : ViewBase
    {
        public catGeoCercas()
        {
            InitializeComponent();
        }
        List<GeoCerca> list = new List<GeoCerca>();
        public bool cargarGeoCercas()
        {
            bool flag2;
            try
            {
                this.lvlGeoCercas.ItemsSource = null;
                base.Cursor = Cursors.Wait;
                OperationResult resp = new GeoCercaSvc().getGeoCerca();
                if (resp.typeResult == ResultTypes.success)
                {
                    list = resp.result as List<GeoCerca>;
                    List<ListViewItem> listViewItem = new List<ListViewItem>();
                    foreach (GeoCerca cerca in list)
                    {
                        ListViewItem newItem = new ListViewItem
                        {
                            Content = cerca
                        };
                        newItem.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                        newItem.Selected += new RoutedEventHandler(this.Lvl_Selected);
                        //this.lvlGeoCercas.Items.Add(newItem);
                        listViewItem.Add(newItem);
                    }
                    lvlGeoCercas.ItemsSource = listViewItem;

                    CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlGeoCercas.ItemsSource);
                    //PropertyGroupDescription groupDescription = new PropertyGroupDescription("nameClasificacion");
                    //view.GroupDescriptions.Add(groupDescription);

                    view.Filter = UserFilter;
                    return true;
                }
                ImprimirMensaje.imprimir(resp);
                flag2 = false;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                flag2 = false;
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return flag2;
        }
        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtBuscador.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((GeoCerca)(item as ListViewItem).Content).nombre.ToString().IndexOf(txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }
        public bool cargarGranjas()
        {
            bool flag2;
            try
            {
                this.cbxGranja.ItemsSource = null;
                base.Cursor = Cursors.Wait;

                flag2 = true;
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
                flag2 = false;
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return flag2;
        }
        private void cbxClasificacion_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                if (this.cbxClasificacion.SelectedItem != null)
                {
                    if (((enumClasificacionGeoCerca)this.cbxClasificacion.SelectedItem) == enumClasificacionGeoCerca.GRANJA)
                    {
                        this.cbxGranja.SelectedItem = null;
                        this.cbxGranja.IsEnabled = true;
                    }
                    else
                    {
                        this.cbxGranja.SelectedItem = null;
                        this.cbxGranja.IsEnabled = false;
                    }
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        public override OperationResult eliminar()
        {
            OperationResult result3;
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult result = this.validar();
                if (result.typeResult > ResultTypes.success)
                {
                    return result;
                }
                GeoCerca geoCerca = this.mapForm();
                if (geoCerca == null)
                {
                    return new OperationResult
                    {
                        valor = 1,
                        mensaje = "ERROR AL LEER INFORMACI\x00d3N DE LA PANTALLA"
                    };
                }
                geoCerca.activo = false;
                OperationResult result2 = new GeoCercaSvc().saveGeoCerca(ref geoCerca);
                if (result2.typeResult == ResultTypes.success)
                {
                    base.mainWindow.habilitar = true;
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.eliminar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
                    this.mapForm(null);
                    this.cargarGeoCercas();
                }
                result3 = result2;
            }
            catch (Exception exception)
            {
                result3 = new OperationResult(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return result3;
        }
        public override OperationResult guardar()
        {
            OperationResult result3;
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult result = this.validar();
                if (result.typeResult != ResultTypes.success)
                {
                    return result;
                }
                GeoCerca geoCerca = this.mapForm();
                if (geoCerca == null)
                {
                    return new OperationResult
                    {
                        valor = 1,
                        mensaje = "ERROR AL LEER INFORMACI\x00d3N DE LA PANTALLA"
                    };
                }
                OperationResult result2 = new GeoCercaSvc().saveGeoCerca(ref geoCerca);
                if (result2.typeResult == ResultTypes.success)
                {
                    base.mainWindow.habilitar = true;
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.eliminar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
                    this.mapForm(geoCerca);
                    this.cargarGeoCercas();
                }
                result3 = result2;
            }
            catch (Exception exception)
            {
                result3 = new OperationResult(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
            return result3;
        }
        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender != null)
            {
                ListViewItem item = sender as ListViewItem;
                this.mapForm(item.Content as GeoCerca);
            }
        }
        private void Lvl_Selected(object sender, RoutedEventArgs e)
        {
            if (sender != null)
            {
                ListViewItem item = sender as ListViewItem;
                this.mapForm(item.Content as GeoCerca);
            }
        }
        private GeoCerca mapForm()
        {
            try
            {
                return new GeoCerca
                {
                    idGeoCerca = (this.ctrClave.Tag == null) ? 0 : (this.ctrClave.Tag as GeoCerca).idGeoCerca,
                    nombre = this.txtNombre.Text.Trim(),
                    zona = (((enumClasificacionGeoCerca)this.cbxClasificacion.SelectedItem) == enumClasificacionGeoCerca.GRANJA) ? (this.cbxGranja.SelectedItem as Zona) : null,
                    clasificacion = (enumClasificacionGeoCerca)this.cbxClasificacion.SelectedItem,
                    activo = true
                };
            }
            catch (Exception)
            {
                return null;
            }
        }
        private void mapForm(GeoCerca geoCerca)
        {
            if (geoCerca != null)
            {
                this.ctrClave.Tag = geoCerca;
                this.ctrClave.valor = geoCerca.idGeoCerca;
                this.txtNombre.Text = geoCerca.nombre;
                
                if (geoCerca.clasificacion == enumClasificacionGeoCerca.GRANJA)
                {
                    Cliente cliente = geoCerca.zona == null ? null : geoCerca.zona.cliente;
                    this.cbxCliente.SelectedItem = cliente == null ? null : cbxCliente.ItemsSource.Cast<Cliente>().ToList().Find(s => s.clave == cliente.clave);
                    this.cbxGranja.SelectedItem = this.cbxGranja.ItemsSource.Cast<Zona>().ToList<Zona>().Find(s => s.clave == geoCerca.zona.clave);
                }
                else
                {
                    this.cbxCliente.SelectedItem = null;
                    this.cbxGranja.SelectedItem = null;
                }
                this.cbxClasificacion.SelectedItem = this.cbxClasificacion.ItemsSource.Cast<enumClasificacionGeoCerca>().ToList<enumClasificacionGeoCerca>().Find(s => s == geoCerca.clasificacion);
            }
            else
            {
                this.ctrClave.Tag = null;
                this.ctrClave.valor = 0;
                this.txtNombre.Clear();
                this.cbxClasificacion.SelectedItem = null;
                this.cbxGranja.SelectedItem = null;
                this.cbxGranja.SelectedItem = null;
            }
        }
        public override void nuevo()
        {
            base.nuevo();
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.eliminar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
            this.mapForm(null);
        }
        public OperationResult validar()
        {
            try
            {
                OperationResult result = new OperationResult
                {
                    valor = 0,
                    mensaje = "PARA PROCEDER SE REQUIERE" + Environment.NewLine
                };
                if (string.IsNullOrEmpty(this.txtNombre.Text.Trim()))
                {
                    result.valor = 1;
                    result.mensaje = result.mensaje + "  * El nombre de la GeoCerca" + Environment.NewLine;
                }
                if ((((enumClasificacionGeoCerca)this.cbxClasificacion.SelectedItem) == enumClasificacionGeoCerca.GRANJA) && (this.cbxGranja.SelectedItem == null))
                {
                    result.valor = 1;
                    result.mensaje = result.mensaje + "  * Si la GeoCerca es de tipo granja se necesita elegir una" + Environment.NewLine;
                }
                return result;
            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
        }
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {          

            try
            {
                base.Cursor = Cursors.Wait;
                var resp = new ClienteSvc().getClientesALLorById(mainWindow.empresa.clave, 0, true);
                if (resp.typeResult == ResultTypes.success)
                {
                    cbxCliente.ItemsSource = resp.result as List<Cliente>;
                }
                if (!this.cargarGranjas())
                {
                    base.IsEnabled = false;
                }
                else if (!this.cargarGeoCercas())
                {
                    base.IsEnabled = false;
                }
                else
                {
                    this.cbxClasificacion.ItemsSource = Enum.GetValues(typeof(enumClasificacionGeoCerca));
                    this.nuevo();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }
        private void txtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlGeoCercas.ItemsSource).Refresh();
        }
        private void cbxCliente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                this.cbxGranja.ItemsSource = null;
                if (sender != null)
                {
                    Cliente cliente = (sender as ComboBox).SelectedItem as Cliente;
                    OperationResult resp = new ZonasSvc().getZonasALLorById(0, cliente.clave.ToString());
                    if (resp.typeResult == ResultTypes.success)
                    {
                        this.cbxGranja.ItemsSource = resp.result as List<Zona>;

                    }
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void btnExportar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                new ReportView().exportarGeoCercas(list);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
    }
}
