﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para addViaticos.xaml
    /// </summary>
    public partial class addViaticos : Window
    {
        private decimal viatico = 0;
        public addViaticos(decimal viatico)
        {
            this.viatico = viatico;
            InitializeComponent();
        }

        public decimal getViatico()
        {
            try
            {
                bool? resp = ShowDialog();
                if (resp.Value)
                {
                    return respViatico;
                }
                else
                {
                    return viatico;
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return viatico;
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ctrDecimal.valor = viatico;
            ctrDecimal.txtDecimal.Focus();
            ctrDecimal.txtDecimal.KeyDown += TxtDecimal_KeyDown;
        }

        private void TxtDecimal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }

        public decimal respViatico
        {
            get
            {
                return ctrDecimal.valor;
            }
        }
        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
