﻿using Core.Interfaces;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using Xceed.Wpf.Toolkit;

namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editCargaCombustibleV2.xaml
    /// </summary>
    public partial class editCargaCombustibleV2 : ViewBase
    {
        private int claveEmpresa = 0;
        private bool afectaKardex = true;
        private bool inicio = true;
        private decimal valorDisel = 0M;
        private bool llenarLista = false;
        private UnidadTransporte unidad = new UnidadTransporte();
        public editCargaCombustibleV2()
        {
            InitializeComponent();
        }
        private void abilitarControles(bool v)
        {
            this.cbxLts.IsEnabled = v;
            this.cbxSellos.IsEnabled = v;
            this.cbxTiempos.IsEnabled = v;
            this.cbxKMs.IsEnabled = v;
        }
        private void actualizarLitrosCargados()
        {

            decimal result = 0M;
            decimal litrosCargados = 0M;
            if (decimal.TryParse(this.txtCombustibleCargados.Text.Trim(), out litrosCargados))
            {
                this.txtSumaLitros.valor = (litrosCargados + this.txtLitros.valor) + this.sumAnticiposOperativos.valor;
            }
            else
            {
                this.txtSumaLitros.valor = this.txtLitros.valor + this.sumAnticiposOperativos.valor;
            }
            decimal.TryParse(this.txtCombustibleUtilizado.Text.Trim(), out result);

            decimal rendimiiento = 0M;
            if (decimal.TryParse(this.txtKmRecorridos.Text, out decimal kmRecorridos))
            {
                if (this.txtSumaLitros.valor == decimal.Zero)
                {
                    rendimiiento = Convert.ToDecimal("0.0");
                }
                else
                {
                    try
                    {
                        rendimiiento = isCR ? (this.txtSumaLitros.valor / kmRecorridos) : (kmRecorridos / this.txtSumaLitros.valor);
                    }
                    catch (Exception)
                    {
                        rendimiiento = 0;
                    }
                    
                }
            }
            else
            {
                rendimiiento = Convert.ToDecimal("0.0");
            }
            this.txtRendimiento.valor = decimal.Round(rendimiiento, 4);
            if (rendimiiento < 1.4M)
            {
                this.txtRendimiento.txtDecimal.Background = Brushes.Red;
            }
            else if ((rendimiiento >= 1.4M) && (rendimiiento <= 2.8M))
            {
                this.txtRendimiento.txtDecimal.Background = Brushes.Green;
            }
            else
            {
                this.txtRendimiento.txtDecimal.Background = Brushes.Gold;
            }

            if (!this.chcECM.IsChecked.Value)
            {
                this.txtCombustibleUtilizado.Text = "0";// this.txtCombustibleCargados.Text;
                txtRelleno.valor = 0;
            }
            else
            {
                this.txtRelleno.valor = this.txtSumaLitros.valor - (string.IsNullOrEmpty(txtCombustibleUtilizado.Text) ? 0 : Convert.ToDecimal(txtCombustibleUtilizado.Text));
            }
        }

        private void addValesCarga(List<ValeCarga> result)
        {
            try
            {
                foreach (ValeCarga carga in result)
                {
                    ListViewItem newItem = new ListViewItem
                    {
                        Content = carga
                    };
                    this.lvlVales.Items.Add(newItem);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
        }

        private void btnVerPanel_Click(object sender, RoutedEventArgs e)
        {
            new viewPanelOSTransito(1).abrirPanel();
        }

        private List<Personal> buscarDespachador(string nombre = "%")
        {
            OperationResult result = new OperadorSvc().getDespachadoresByNombre(nombre);
            switch (result.typeResult)
            {
                case ResultTypes.success:
                    return (List<Personal>)result.result;

                case ResultTypes.error:
                    System.Windows.MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
                    return null;

                case ResultTypes.warning:
                    System.Windows.MessageBox.Show(result.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return null;

                case ResultTypes.recordNotFound:
                    return new List<Personal>();
            }
            return null;
        }

        private void buscarExistencias()
        {
            try
            {
                if (this.txtClave.Tag != null)
                {
                    NivelesTanque tag = this.txtClave.Tag as NivelesTanque;
                    base.Cursor = Cursors.Wait;
                    OperationResult resp = new CargaCombustibleSvc().getExistenciasDieselActual(tag.tanqueCombustible.idTanqueCombustible);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        Dictionary<string, decimal> result = resp.result as Dictionary<string, decimal>;
                        List<Existencias> list = new List<Existencias>();
                        foreach (KeyValuePair<string, decimal> pair in result)
                        {
                            Existencias item = new Existencias
                            {
                                empresa = pair.Key,
                                existencia = pair.Value
                            };
                            list.Add(item);
                        }
                        this.lvlExistencias.ItemsSource = list;
                        lblSumaExistencias.Content = list.Sum(s => s.existencia).ToString("N4");
                    }
                    else
                    {
                        this.lvlExistencias.ItemsSource = null;
                        ImprimirMensaje.imprimir(resp);
                        lblSumaExistencias.Content = decimal.Zero.ToString("N4");
                    }
                }
                else
                {
                    this.lvlExistencias.ItemsSource = null;
                    lblSumaExistencias.Content = decimal.Zero.ToString("N4");
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }


        internal class Existencias
        {
            public string empresa { get; set; }
            public decimal existencia { get; set; }
        }

        private List<Personal> buscarPersonal(string nombre)
        {
            OperationResult result = new OperadorSvc().getOperadoresByNombre(nombre, 0);
            switch (result.typeResult)
            {
                case ResultTypes.success:
                    return (List<Personal>)result.result;

                case ResultTypes.error:
                    System.Windows.MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
                    return null;

                case ResultTypes.warning:
                    System.Windows.MessageBox.Show(result.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return null;

                case ResultTypes.recordNotFound:
                    return new List<Personal>();
            }
            return null;
        }

        private List<UnidadTransporte> buscarUnidades(string clave)
        {
            OperationResult result = new UnidadTransporteSvc().getUnidadesALLorById(this.claveEmpresa, clave);
            switch (result.typeResult)
            {
                case ResultTypes.success:
                    return (List<UnidadTransporte>)result.result;

                case ResultTypes.error:
                    System.Windows.MessageBox.Show(result.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Hand);
                    return null;

                case ResultTypes.warning:
                    System.Windows.MessageBox.Show(result.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    return null;

                case ResultTypes.recordNotFound:
                    return new List<UnidadTransporte>();
            }
            return null;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ReportView view = new ReportView();
            view.createValeGasolina((CargaCombustible)this.txtFolio.Tag, "");
            view.ShowDialog();
        }

        private void CbxZonaOpetativa_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.ctrZonaOperativa.zonaOperativa != null)
            {
                this.getNivelesByZonaOperativa(this.ctrZonaOperativa.zonaOperativa);
                this.buscarExistencias();
            }
        }

        private void chcExtra_Checked(object sender, RoutedEventArgs e)
        {
            this.limpiarExtra(true);
        }

        private void chcExtra_Unchecked(object sender, RoutedEventArgs e)
        {
            this.limpiarExtra(false);
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                CheckBox box = sender as CheckBox;
                calcularKmRecorridos();
                if (box.IsChecked.Value)
                {
                    this.cbxTiempos.IsEnabled = true;
                    this.txtTiempoTotal.Clear();
                    this.txtTiempoPTO.Clear();
                    this.txtTiempoEst.Clear();
                    this.cbxLts.IsEnabled = true;
                    this.txtCombustibleUtilizado.Clear();
                    this.txtCombustibleEST.Clear();
                    this.txtCombustiblePTO.Clear();
                    this.txtKmRecorridos.IsEnabled = true;
                    this.txtKmRecorridos.Clear();
                    txtRelleno.valor = txtSumaLitros.valor;
                }
                else
                {
                    this.cbxTiempos.IsEnabled = false;
                    this.txtTiempoTotal.Text = "000:00:00";
                    this.txtTiempoPTO.Text = "00:00:00";
                    this.txtTiempoEst.Text = "00:00:00";
                    this.cbxLts.IsEnabled = false;
                    if (string.IsNullOrEmpty(this.txtCombustibleCargados.Text.Trim()))
                    {
                        this.txtCombustibleUtilizado.Text = "0";
                    }
                    else
                    {
                        this.txtCombustibleUtilizado.Text = this.txtCombustibleCargados.Text;
                    }
                    this.txtCombustibleEST.Text = "0";
                    this.txtCombustiblePTO.Text = "0";
                    this.txtKmRecorridos.IsEnabled = false;
                    this.txtKmRecorridos.Text = "0";
                    txtRelleno.valor = 0;
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void completarClave(ref string clave)
        {
            if (clave.Length < 3)
            {
                for (int i = 0; i <= (3 - clave.Length); i++)
                {
                    clave = "0" + clave;
                }
            }
        }

        private void getNivelesByZonaOperativa(ZonaOperativa zonaOperativa)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                OperationResult resp = new NivelesTanqueSvc().getNivelTanqueByZonaOperativa(zonaOperativa);
                NivelesTanque nivelesTanque = resp.result as NivelesTanque;
                if (resp.typeResult == ResultTypes.success)
                {
                    if (nivelesTanque.fechaFinal.HasValue)
                    {
                        this.imprimirMensaje(new OperationResult(ResultTypes.warning, "NO ESTAN ABIERTOS LOS NIVELES", null));
                        base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    }
                    else
                    {
                        this.mapNivelTanque(nivelesTanque);
                        base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
                    }
                }
                else if (resp.typeResult == ResultTypes.recordNotFound)
                {
                    this.mapNivelTanque(null);
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "ESTA ZONA NO TIENE UN TANQUE ASIGNADO"));
                }
                else

                {
                    this.mapNivelTanque(null);
                    base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        private void getTiposCombustible()
        {
            try
            {
                OperationResult result = new TipoCombustibleSvc().getTiposCombustibleAllOrById(0);
                if (result.typeResult == ResultTypes.success)
                {
                    TipoCombustible combustible = ((List<TipoCombustible>)result.result).Find(s => s.tipoCombustible.ToUpper() == "DIESEL");
                    this.valorDisel = combustible.ultimoPrecio;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        enumTipoOrdServ enumTipoOrd = enumTipoOrdServ.COMBUSTIBLE;
        public override OperationResult guardar()
        {

            try
            {
                base.Cursor = Cursors.Wait;
                //System.Threading.Thread.Sleep(2000);

                if (this.enumTipoOrd == enumTipoOrdServ.COMBUSTIBLE)
                {
                    decimal existencia = Convert.ToDecimal(lblSumaExistencias.Content);

                    decimal val = 0;
                    if (!decimal.TryParse(txtCombustibleCargados.Text, out val))
                    {
                        return new OperationResult(ResultTypes.warning, "LOS LITROS CARGADOS NO SON VALIDOS");
                    }

                    if (val > existencia)
                    {
                        return new OperationResult(ResultTypes.warning, "SALDO DE INVENTARIO INSUFICIENTE");
                    }
                    if (val <= 0)
                    {
                        return new OperationResult(ResultTypes.warning, "LOS LITROS CARGADOS TIENEN QUE SER MAYOR A 0");
                    }

                    OperationResult result = this.validar();
                    if (result.typeResult > ResultTypes.success)
                    {
                        return result;
                    }
                    CargaCombustible carga = this.mapForm();
                    if (carga != null)
                    {
                        confirmacionCargaCombustible combustible2 = new confirmacionCargaCombustible(carga);
                        if (combustible2.ShowDialog().Value)
                        {
                            KardexCombustible kardexCombustible = this.mapKardex();
                            if ((!carga.isExterno && (kardexCombustible == null)) && this.afectaKardex)
                            {
                                return new OperationResult(ResultTypes.warning, "Ocurrio un error al leer informaci\x00f3n de la pantalla", null);
                            }
                            base.Cursor = Cursors.Wait;
                            OperationResult result3 = new OperacionSvc().saveCargaGasolina(ref carga, ((UnidadTransporte)this.txtTractor.Tag).empresa.clave, kardexCombustible, false);
                            if (result3.typeResult == ResultTypes.success)
                            {
                                this.txtFolio.Tag = (CargaCombustible)result3.result;

                                if (chcImprimir.IsChecked.Value)
                                {
                                    ReportView view = new ReportView();
                                    if (((UnidadTransporte)this.txtTractor.Tag).empresa.clave == 1)
                                    {
                                        view.createValeGasolinaNuevo2((CargaCombustible)this.txtFolio.Tag, "");
                                    }
                                    else
                                    {
                                        view.createValeGasolinaNuevo((CargaCombustible)this.txtFolio.Tag, "");
                                    }
                                }
                                base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.nuevo);
                                this.buscarExistencias();
                            }
                            return result3;
                        }
                        return new OperationResult
                        {
                            valor = 2,
                            mensaje = "REVISAR LOS DATOS PROPORCIONADOS"
                        };
                    }
                    else
                    {
                        return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS DATOS DE LA PANTALLA");
                    }
                }
                else
                {
                    var val = validarCargaExtra();
                    if (val.typeResult != ResultTypes.success) return val;

                    CargaCombustibleExtra carga = mapFormExtra();
                    if (carga != null)
                    {
                        KardexCombustible kardexCombustible = null;
                        if (this.afectaKardex)
                        {
                            kardexCombustible = new KardexCombustible
                            {
                                idInventarioDiesel = 0,
                                cantidad = carga.ltCombustible.Value,
                                empresa = carga.unidadTransporte.empresa,
                                unidadTransporte = carga.unidadTransporte,
                                idOperacion = new int?(carga.idCargaCombustibleExtra),
                                tanque = (this.txtClave.Tag as NivelesTanque).tanqueCombustible,
                                personal = carga.despachador,
                                tipoMovimiento = eTipoMovimiento.SALIDA,
                                tipoOperacion = TipoOperacionDiesel.ANTICIPO
                            };
                        }
                        OperationResult result = new CargaCombustibleSvc().saveCargaCombustibleEXtra(ref carga, ref kardexCombustible);
                        if (result.typeResult == ResultTypes.success)
                        {
                            this.mapFormCargaExtra(carga);

                        }
                        return result;
                    }
                    else
                    {
                        return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS DATOS DE LA PANTALLA");
                    }

                }

            }
            catch (Exception exception)
            {
                return new OperationResult(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
            }
        }
        private CargaCombustibleExtra mapFormExtra()
        {
            try
            {
                if (this.txtFolio.Tag == null)
                {
                    return null;
                }
                CargaCombustibleExtra tag = this.txtFolio.Tag as CargaCombustibleExtra;
                tag.fechaCombustible = new DateTime?(DateTime.Now);
                tag.ltCombustible = new decimal?(Convert.ToDecimal(this.txtCombustibleCargados.Text.Trim()));
                if (this.chcExtra.IsChecked.Value)
                {
                    tag.externo = true;
                    tag.folioImpreso = this.txtFolioImpreso.Text;
                    tag.ticket = this.txtTicket.Text;
                    tag.proveedor = this.txtProveedor.SelectedItem as ProveedorCombustible;
                    tag.precioCombustible = new decimal?(this.txtPrecio.valor);
                }
                else
                {
                    tag.externo = false;
                    tag.folioImpreso = string.Empty;
                    tag.ticket = string.Empty;
                    tag.proveedor = null;
                    tag.precioCombustible = new decimal?(this.valorDisel);
                }
                tag.despachador = this.txtDespachador.Tag as Personal;
                tag.chofer = ctrOperador.personal;
                return tag;
            }
            catch (Exception)
            {
                return null;
            }
        }
        private void imprimirMensaje(OperationResult resp)
        {
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    System.Windows.MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Asterisk);
                    break;

                case ResultTypes.error:
                    System.Windows.MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Hand);
                    break;

                case ResultTypes.warning:
                    System.Windows.MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    break;

                case ResultTypes.recordNotFound:
                    System.Windows.MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    break;
            }
        }
        private void limpiarExtra(bool valor)
        {
            this.gboxExtra.IsEnabled = valor;
            if (!valor)
            {
                this.txtTicket.Clear();
                this.txtProveedor.SelectedItem = null;
                this.txtLitros.valor = decimal.Zero;
                this.txtPrecio.valor = decimal.Zero;
                this.txtFolioImpreso.Clear();
            }
        }

        private void llenarTabla(CargaCombustibleExtra result)
        {
            if (!result.fechaCombustible.HasValue)
            {
                OperationResult resp = new OperationResult
                {
                    valor = 2,
                    mensaje = "ESTE ACTICIPO NO ESTA FINALIZADO"
                };
                ImprimirMensaje.imprimir(resp);
            }
            else
            {
                MasterOrdServ masterOrdServ = ((CargaCombustible)this.txtFolio.Tag).masterOrdServ;
                if ((result.tipoOrdenCombustible.tipoUnidad.ToUpper() == "TC") && (masterOrdServ.tractor.clave != result.unidadTransporte.clave))
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 2,
                        mensaje = "ESTE ACTICIPO NO CORRESPONDE PARA EL TRACTOR " + masterOrdServ.tractor.clave
                    };
                    ImprimirMensaje.imprimir(resp);
                }
                else
                {
                    int? nullable2;
                    if (result.tipoOrdenCombustible.tipoUnidad.ToUpper() == "RE")
                    {
                        if (masterOrdServ.tolva2 == null)
                        {
                            if (masterOrdServ.tolva1.clave != result.unidadTransporte.clave)
                            {
                                OperationResult resp = new OperationResult
                                {
                                    valor = 2,
                                    mensaje = "ESTE ACTICIPO NO CORRESPONDE PARA EL REMOLQUE " + masterOrdServ.tolva1.clave
                                };
                                ImprimirMensaje.imprimir(resp);
                                return;
                            }
                        }
                        else if ((masterOrdServ.tolva1.clave != result.unidadTransporte.clave) && (masterOrdServ.tolva2.clave != result.unidadTransporte.clave))
                        {
                            OperationResult resp = new OperationResult
                            {
                                valor = 2,
                                mensaje = "ESTE ACTICIPO NO CORRESPONDE PARA NINGUNO DE LOS REMOLQUES " + masterOrdServ.tolva1.clave + "/" + masterOrdServ.tolva2.clave
                            };
                            ImprimirMensaje.imprimir(resp);
                            return;
                        }
                    }
                    if (result.idCargaCombustible.HasValue || (((nullable2 = result.idCargaCombustible).GetValueOrDefault() > 0) ? nullable2.HasValue : false))
                    {
                        OperationResult resp = new OperationResult
                        {
                            valor = 2,
                            mensaje = "ESTE ACTICIPO YA FUE UTILIZADO PARA EL VALE DE COMBUSTIBLE " + result.idCargaCombustible.ToString()
                        };
                        ImprimirMensaje.imprimir(resp);
                    }
                    else if (!this.listCargasExtra.Exists(element => element.idCargaCombustibleExtra == result.idCargaCombustibleExtra))
                    {
                        if (result.tipoOrdenCombustible.operativo)
                        {
                            this.sumAnticiposOperativos.valor += result.ltCombustible.Value;
                        }
                        ListViewItem newItem = new ListViewItem
                        {
                            Content = result
                        };
                        this.lvlValesExtra.Items.Add(newItem);
                    }
                }
            }
        }

        private void mapDespachador(Personal personal)
        {
            if (personal != null)
            {
                this.txtDespachador.Tag = personal;
                this.txtDespachador.Text = personal.nombre;
                this.txtDespachador.IsEnabled = false;
                if (this.unidad != null)
                {
                    if (this.unidad.tipoUnidad.clasificacion.ToUpper() == "TRACTOR")
                    {
                        this.txtKm.Focus();
                    }
                    else
                    {
                        this.txtCombustibleCargados.Focus();
                    }
                }
            }
            else
            {
                this.txtDespachador.Tag = null;
                this.txtDespachador.Text = "";
                this.txtDespachador.IsEnabled = true;
            }
        }

        private CargaCombustible mapForm()
        {
            try
            {
                CargaCombustible carga = (CargaCombustible)this.txtFolio.Tag;
                carga.fechaCombustible = null;
                carga.ltCombustibleUtilizado = string.IsNullOrEmpty(this.txtCombustibleUtilizado.Text.Trim()) ? decimal.Zero : Convert.ToDecimal(this.txtCombustibleUtilizado.Text.Trim());
                carga.ltCombustiblePTO = string.IsNullOrEmpty(this.txtCombustiblePTO.Text.Trim()) ? decimal.Zero : Convert.ToDecimal(this.txtCombustiblePTO.Text.Trim());
                carga.ltCombustibleEST = string.IsNullOrEmpty(this.txtCombustibleEST.Text.Trim()) ? decimal.Zero : Convert.ToDecimal(this.txtCombustibleEST.Text.Trim());
                carga.ltCombustible = Convert.ToDecimal(this.txtCombustibleCargados.Text.Trim());
                carga.tiempoTotal = this.txtTiempoTotal.Text;
                carga.tiempoPTO = this.txtTiempoPTO.Text;
                carga.tiempoEST = this.txtTiempoEst.Text;
                carga.kmDespachador = string.IsNullOrEmpty(this.txtKm.Text.Trim()) ? decimal.Zero : Convert.ToDecimal(this.txtKm.Text.Trim());
                carga.fechaCombustible = new DateTime?(DateTime.Now);
                carga.kmRecorridos = (string.IsNullOrEmpty(this.txtKmRecorridos.Text.Trim()) ? decimal.Zero : Convert.ToDecimal(this.txtKmRecorridos.Text.Trim()));
                carga.ecm = this.chcECM.IsChecked.Value;
                carga.idChofer = ctrOperador.personal.clave;
                carga.unidadTransporte = txtTractor.Tag as UnidadTransporte;
                if (carga.kmRecorridos <= decimal.Zero)
                {
                    decimal result = 0M;
                    if (decimal.TryParse(this.txtUltKM.Text, out result) && (result > decimal.Zero))
                    {
                        carga.kmRecorridos = !chcHubodometro.IsChecked.Value ? 0 : carga.kmDespachador - result;
                    }
                }
                carga.despachador = (Personal)this.txtDespachador.Tag;
                carga.sello1 = new int?(string.IsNullOrEmpty(this.txtTanqueIzq.Text.Trim()) ? 0 : Convert.ToInt32(this.txtTanqueIzq.Text.Trim()));
                carga.sello2 = new int?(string.IsNullOrEmpty(this.txtTanqueDer.Text.Trim()) ? 0 : Convert.ToInt32(this.txtTanqueDer.Text.Trim()));
                int? nullable = null;
                carga.selloReserva = string.IsNullOrEmpty(this.txtTanqueRes.Text.Trim()) ? nullable : new int?(Convert.ToInt32(this.txtTanqueRes.Text.Trim()));
                carga.listCP = new List<CartaPorte>();
                foreach (ListViewItem item in (IEnumerable)this.lvlCP.Items)
                {
                    carga.listCP.Add((CartaPorte)item.Content);
                }
                carga.usuarioCrea = base.mainWindow.inicio._usuario.nombreUsuario;
                carga.listCombustibleExtra = this.listCargasExtra;
                if (this.chcExtra.IsChecked.Value)
                {
                    carga.ticketExtra = this.txtTicket.Text.Trim();
                    carga.proveedorExtra = this.txtProveedor.SelectedItem as ProveedorCombustible;
                    carga.proveedorExtra.precio = this.txtPrecio.valor;
                    carga.ltsExtra = this.txtLitros.valor;
                    carga.folioImpresoExtra = this.txtFolioImpreso.Text.Trim();
                }
                else
                {
                    carga.ticketExtra = string.Empty;
                    carga.proveedorExtra = null;
                    carga.ltsExtra = decimal.Zero;
                    carga.folioImpresoExtra = string.Empty;
                }
                if (this.chcExterno.IsChecked.Value)
                {
                    carga.isExterno = true;
                    carga.ticket = this.txtTiketCarga.Text;
                    carga.proveedor = (ProveedorCombustible)this.txtProveedorCarga.SelectedItem;
                    carga.precioCombustible = this.txtPrecioCarga.valor;
                    carga.folioImpreso = this.txtFolioImpresoCarga.Text;
                    carga.observacioses = this.txtObservaciones.Text;
                }
                carga.sumaLitrosExtra = this.sumAnticiposOperativos.valor;
                carga.zonaOperativa = ctrZonaOperativa.zonaOperativa;
                carga.usuarioCrea = mainWindow.usuario.nombreUsuario;
                carga.masterOrdServ.chofer = ctrOperador.personal;
                carga.isCR = this.isCR;
                carga.isHubodometo = chcHubodometro.IsChecked.Value;
                return carga;
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void mapForm(CargaCombustible carga)
        {
            if (carga == null)
            {
                this.txtFolio.IsEnabled = true;
                this.mapOperador(null);
                this.mapUnidades(TipoUnidad.TRACTOR, null);
                this.unidad = null;
                this.txtCombustibleUtilizado.Clear();
                this.txtCombustiblePTO.Clear();
                this.txtCombustibleEST.Clear();
                this.txtCombustibleCargados.Clear();
                this.abilitarControles(true);
                this.txtTiempoTotal.Clear();
                this.txtTiempoPTO.Clear();
                this.txtTiempoEst.Clear();
                this.txtKmRecorridos.Clear();
                this.txtKm.Clear();
                this.mapDespachador(null);
                this.txtTanqueIzq.Clear();
                this.txtTanqueDer.Clear();
                this.txtTanqueRes.Clear();
                this.txtFecha.Clear();
                this.txtFolio.Clear();
                this.txtFolio.Focus();
                this.lvlCP.Items.Clear();
                this.llenarLista = false;
                this.lvlValesExtra.Items.Clear();
                this.limpiarExtra(false);
                this.lvlCarga.Items.Clear();
                this.txtProveedorCarga.SelectedItem = null;
                this.txtFolioImpresoCarga.Clear();
                this.txtObservaciones.Clear();
                this.chcExterno.IsChecked = false;
                this.txtUltKM.Clear();
                this.sumAnticiposOperativos.valor = decimal.Zero;
                this.expCargaCombExtra.IsExpanded = false;
                this.chcHubodometro.IsChecked = true;
            }
            else
            {
                this.txtFolio.Tag = carga;
                this.txtFolio.IsEnabled = false;
                this.mapOperador(carga.masterOrdServ.chofer);
                foreach (TipoOrdenServicio servicio in carga.masterOrdServ.listTiposOrdenServicio)
                {
                    if (servicio.idOrdenSer == carga.idOrdenServ)
                    {
                        this.unidad = servicio.unidadTrans;
                    }
                }
                if (this.unidad.tipoUnidad.descripcion.Contains("TRACTO"))
                {
                    if (new CargaCombustibleSvc().findUltimaCargaByIdUnidad(this.unidad.clave).typeResult == ResultTypes.recordNotFound)
                    {
                        if (!carga.fechaCombustible.HasValue)
                        {
                            this.cbxCP.IsEnabled = true;
                            this.llenarLista = true;
                        }
                        else
                        {
                            this.llenarLista = false;
                        }
                    }
                }
                else
                {
                    this.llenarLista = false;
                }
                this.mapList(carga.listCP);
                this.abilitarControles(this.unidad.tipoUnidad.clasificacion.ToUpper() == "TRACTOR");
                this.mapUnidades(TipoUnidad.TRACTOR, this.unidad);
                this.chcECM.IsChecked = new bool?(carga.ecm);
                this.txtCombustibleUtilizado.Text = (carga.ltCombustibleUtilizado <= decimal.Zero) ? "" : carga.ltCombustibleUtilizado.ToString();
                this.txtCombustiblePTO.Text = (carga.ltCombustiblePTO <= decimal.Zero) ? "" : carga.ltCombustiblePTO.ToString();
                this.txtCombustibleEST.Text = (carga.ltCombustibleEST <= decimal.Zero) ? "" : carga.ltCombustibleEST.ToString();
                this.txtCombustibleCargados.Text = (carga.ltCombustible <= decimal.Zero) ? "" : carga.ltCombustible.ToString();
                this.txtTiempoTotal.Text = (carga.tiempoTotal.Length == 8) ? $"0{carga.tiempoTotal}" : carga.tiempoTotal;
                this.txtTiempoPTO.Text = carga.tiempoPTO;
                this.txtTiempoEst.Text = carga.tiempoEST;
                this.txtKmRecorridos.Text = (carga.kmRecorridos == decimal.Zero) ? "0" : carga.kmRecorridos.ToString("N2");
                this.txtKm.Text = (carga.kmDespachador <= decimal.Zero) ? "0" : carga.kmDespachador.ToString("N2");
                this.mapDespachador(carga.despachador == null ? mainWindow.usuario.personal : carga.despachador);
                txtTanqueIzq.Text = carga.sello1 <= 0 ? "" : carga.sello1.ToString();
                txtTanqueDer.Text = carga.sello2 <= 0 ? "" : carga.sello2.ToString();
                txtTanqueRes.Text = carga.selloReserva <= 0 ? "" : carga.selloReserva.ToString();
                this.txtFecha.Text = carga.masterOrdServ.fechaCreacion.ToString();
                ListViewItem newItem = new ListViewItem
                {
                    Foreground = Brushes.SteelBlue,
                    Content = carga
                };
                this.lvlCarga.Items.Add(newItem);
                if (!carga.fechaCombustible.HasValue)
                {
                    //if (!ReglasUbicacion.validar(EstatusUbicacion.GASOLINERA, this.unidad))
                    //{
                    //    this.nuevo();
                    //    return;
                    //}
                    if ((this.ultimoKm.typeResult == ResultTypes.success) || (this.ultimoKm.typeResult == ResultTypes.recordNotFound))
                    {
                        this.txtUltKM.Text = this.ultimoKm.result.ToString();
                        this.txtUltKM.IsEnabled = false;
                    }
                    else
                    {
                        this.txtUltKM.Text = "0";
                        this.txtUltKM.IsEnabled = true;
                    }
                }
                else
                {
                    OperationResult result3 = new RegistroKilometrajeSvc().getUltimoKM(this.unidad.clave, Convert.ToDateTime(carga.fechaCombustible));
                    if (result3.typeResult == ResultTypes.success)
                    {
                        this.txtUltKM.Text = ((decimal)result3.result).ToString("N2");
                        this.txtUltKM.IsEnabled = false;
                    }
                    else
                    {
                        this.txtUltKM.Text = "";
                        this.txtUltKM.IsEnabled = true;
                    }
                }
                this.txtDespachador.Focus();
                if (carga.proveedorExtra != null)
                {
                    this.expCargaExtra.IsExpanded = true;
                    this.chcExtra.IsChecked = true;
                    int num3 = 0;
                    foreach (ProveedorCombustible combustible in (IEnumerable)this.txtProveedor.Items)
                    {
                        if (combustible.idProveedor == carga.proveedorExtra.idProveedor)
                        {
                            this.txtProveedor.SelectedIndex = num3;
                            break;
                        }
                        num3++;
                    }
                    this.txtPrecio.valor = carga.proveedorExtra.precio;
                    this.txtLitros.valor = carga.ltsExtra;
                    this.txtTicket.Text = carga.ticketExtra;
                    this.txtFolioImpreso.Text = carga.folioImpresoExtra;
                }
                if (!carga.fechaCombustible.HasValue)
                {
                    this.chcExterno.IsChecked = new bool?(!this.afectaKardex);
                }
                else
                {
                    this.chcExterno.IsChecked = new bool?(carga.isExterno);
                }
                if (carga.isExterno)
                {
                    this.expExterno.IsExpanded = true;
                    int num4 = 0;
                    foreach (ProveedorCombustible combustible2 in (IEnumerable)this.txtProveedorCarga.Items)
                    {
                        if (combustible2.idProveedor == carga.proveedor.idProveedor)
                        {
                            this.txtProveedorCarga.SelectedIndex = num4;
                            break;
                        }
                        num4++;
                    }
                    this.txtTiketCarga.Text = carga.ticket;
                    this.txtPrecioCarga.valor = carga.precioCombustible;
                    this.txtFolioImpresoCarga.Text = carga.folioImpreso;
                    this.txtObservaciones.Text = carga.observacioses;
                }
                this.txtPrecioCombustible.valor = (carga.precioCombustible <= decimal.Zero) ? this.valorDisel : carga.precioCombustible;
                this.sumAnticiposOperativos.valor = carga.sumaLitrosExtra;
                if (carga.sumaLitrosExtra > decimal.Zero)
                {
                    this.expCargaCombExtra.IsExpanded = true;
                }
                if (unidad.clave.Contains("CR"))
                {
                    lblKmInicial.Content = "H. INICIAL:";
                    lblKmFinal.Content = "H. FINAL:";
                    lblKmRecorridos.Content = "H. OPERATIVAS:";
                    chcECM.IsChecked = false;
                    chcECM.IsEnabled = false;
                    stpSellos.Visibility = Visibility.Hidden;
                    isCR = true;
                    cbxKMs.IsEnabled = true;
                    cbxSellos.IsEnabled = true;
                    stpSellos.IsEnabled = false;
                }
                else
                {
                    lblKmInicial.Content = "KM INICIAL:";
                    lblKmFinal.Content = "KM FINAL:";
                    lblKmRecorridos.Content = "KM RECORRIDOS:";
                    chcECM.IsEnabled = true;
                    stpSellos.Visibility = Visibility.Visible;
                    isCR = false;
                    cbxKMs.IsEnabled = true;
                    stpSellos.IsEnabled = true;
                }
            }
        }
        bool isCR = false;
        private KardexCombustible mapKardex()
        {
            try
            {
                return new KardexCombustible
                {
                    idInventarioDiesel = 0,
                    cantidad = Convert.ToDecimal(this.txtCombustibleCargados.Text),
                    empresa = (this.txtFolio.Tag as CargaCombustible).masterOrdServ.tractor.empresa,
                    idOperacion = 0,
                    tipoMovimiento = eTipoMovimiento.SALIDA,
                    tipoOperacion = TipoOperacionDiesel.CARGA,
                    unidadTransporte = (this.txtFolio.Tag as CargaCombustible).masterOrdServ.tractor,
                    tanque = (this.txtClave.Tag as NivelesTanque).tanqueCombustible,
                    personal = this.txtDespachador.Tag as Personal
                };
            }
            catch (Exception)
            {
                return null;
            }
        }

        private void mapList(List<CartaPorte> listDetalles)
        {
            foreach (CartaPorte porte in listDetalles)
            {
                ListViewItem newItem = new ListViewItem
                {
                    Content = porte
                };
                this.lvlCP.Items.Add(newItem);
            }
        }

        private void mapNivelTanque(NivelesTanque nivelesTanque)
        {
            if (nivelesTanque != null)
            {
                this.txtClave.Tag = nivelesTanque;
                this.txtClave.valor = nivelesTanque.idNivel;
                this.txtFechaInicio.Text = nivelesTanque.fechaInicio.ToString("dd/MM/yyyy HH:mm:ss");
            }
            else
            {
                this.txtClave.Tag = null;
                this.txtClave.valor = 0;
                this.txtFechaInicio.Clear();
            }
        }

        private void mapOperador(Personal operador)
        {
            if (operador != null)
            {
                this.txtOperador.Tag = operador;
                this.txtOperador.Text = operador.nombre;
                this.txtOperador.IsEnabled = false;
            }
            else
            {
                this.txtOperador.Tag = null;
                this.txtOperador.Text = "";
                this.txtOperador.IsEnabled = true;
            }
        }

        private void mapUnidades(TipoUnidad tipo, UnidadTransporte Unidad)
        {
            if (tipo == TipoUnidad.TRACTOR)
            {
                if (Unidad != null)
                {
                    this.txtTractor.Tag = Unidad;
                    this.txtTractor.Text = Unidad.clave;
                    this.txtTractor.IsEnabled = false;
                    this.txtOperador.Focus();
                }
                else
                {
                    this.txtTractor.Tag = null;
                    this.txtTractor.Clear();
                    this.txtTractor.IsEnabled = true;
                }
            }
        }
        private enum TipoUnidad
        {
            TRACTOR,
            TOLVA,
            DOLLY,
            TOLVA_2
        }

        public override void nuevo()
        {
            this.txtCombustibleCargados.IsEnabled = true;
            this.txtPrecioCombustible.valor = this.valorDisel;
            this.expExterno.IsExpanded = false;
            this.expCargaExtra.IsExpanded = false;
            base.nuevo();
            base.mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
            this.mapForm(null);
            this.txtFolio.Focus();
            base.mainWindow.scrollContainer.IsEnabled = true;
            this.txtFolio.Focus();
            this.txtRendimiento.txtDecimal.Background = Brushes.Red;
            if (!this.inicio)
            {
                this.buscarExistencias();
            }
            this.chcExterno.IsChecked = new bool?(!this.afectaKardex);
            this.chcECM.IsChecked = true;
            mapDespachador(mainWindow.usuario.personal);
            ctrOperador.limpiar();
        }
        private void txtCombustibleCargados_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && !string.IsNullOrEmpty(this.txtCombustibleCargados.Text.Trim()))
            {
                this.txtTiempoTotal.Focus();
            }
        }

        private void txtCombustibleCargados_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.actualizarLitrosCargados();
        }

        private void txtCombustibleEST_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && !string.IsNullOrEmpty(this.txtCombustibleEST.Text.Trim()))
            {
                this.txtCombustibleCargados.Focus();
            }
        }

        private void txtCombustiblePTO_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && !string.IsNullOrEmpty(this.txtCombustiblePTO.Text.Trim()))
            {
                this.txtCombustibleEST.Focus();
            }
        }

        private void txtCombustibleUtilizado_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && !string.IsNullOrEmpty(this.txtCombustibleUtilizado.Text.Trim()))
            {
                this.txtCombustiblePTO.Focus();
            }
        }

        private void TxtDecimal_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.actualizarLitrosCargados();
        }

        private void txtDespachador_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string str = this.txtDespachador.Text.Trim();
                List<Personal> list = new List<Personal>();
                if (string.IsNullOrEmpty(str))
                {
                    list = this.buscarDespachador("%");
                }
                else
                {
                    list = this.buscarDespachador(str);
                }
                if (list.Count == 1)
                {
                    this.mapDespachador(list[0]);
                }
                else if (list.Count == 0)
                {
                    Personal personal = new selectPersonal(str, 0).buscarAllDespachador();
                    this.mapDespachador(personal);
                }
                else
                {
                    Personal personal = new selectPersonal(str, 0).buscarAllDespachador();
                    this.mapDespachador(personal);
                }
            }
        }

        private void txtFolio_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                int num = 0;

                if (int.TryParse(this.txtFolio.Text.Trim(), out num))
                {
                    var respOS = new OperacionSvc().getOrdenServById(num);
                    if (respOS.typeResult == ResultTypes.success)
                    {
                        TipoOrdenServicio tipoOS = respOS.result as TipoOrdenServicio;
                        if (tipoOS.idTipoOrdenServicio == 1 || tipoOS.idTipoOrdenServicio == 6)
                        {
                            mapFormOS(tipoOS);
                        }
                        else
                        {
                            ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "ESTA ORDEN DE SERVICIO NO ES PARA COMBUSTIBLE"));
                            return;
                        }
                    }
                    else
                    {
                        imprimirMensaje(respOS);
                    }
                    return;

                    //OperationResult resp = new OperacionSvc().getSalidasEntradasByIdOrdenServ(Convert.ToInt32(this.txtFolio.Text.Trim()));
                    //if (resp.typeResult == ResultTypes.success)
                    //{
                    //    CargaCombustible result = (CargaCombustible)resp.result;
                    //    this.mapForm(result);
                    //    if (((CargaCombustible)resp.result).fechaCombustible.HasValue)
                    //    {
                    //        base.mainWindow.scrollContainer.IsEnabled = false;
                    //        base.mainWindow.enableToolBarCommands(ToolbarCommands.nuevo);
                    //    }
                    //}
                    //else
                    //{
                    //    this.imprimirMensaje(resp);
                    //}
                }
                else
                {
                    System.Windows.MessageBox.Show("El folio de la orden de servicio no es valido", "Aviso", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }
        private void mapFormOS(TipoOrdenServicio tipoOrdenServicio)
        {
            try
            {
                if (tipoOrdenServicio != null)
                {
                    if (tipoOrdenServicio.idTipoOrdenServicio == 1)
                    {
                        var respChofer = new OperadorSvc().getOperadoresALLorById(tipoOrdenServicio.idChofer);
                        if (respChofer.typeResult == ResultTypes.success)
                        {
                            ctrOperador.personal = (respChofer.result as List<Personal>)[0];
                        }
                        else
                        {
                            ImprimirMensaje.imprimir(respChofer);
                            return;
                        }

                        this.enumTipoOrd = enumTipoOrdServ.COMBUSTIBLE;
                        stpCapturaKm.Visibility = Visibility.Visible;
                        chcECM.IsChecked = true;
                        stpControlesCargasExtras.Visibility = Visibility.Visible;
                        //cbxCP.Visibility = Visibility.Visible;
                        stpSumasLitros.Visibility = Visibility.Visible;
                        cbxSellos.Visibility = Visibility.Visible;
                        cbxTiempos.Visibility = Visibility.Visible;
                        stpLitros.Visibility = Visibility.Visible;

                        OperationResult resp = new OperacionSvc().getSalidasEntradasByIdOrdenServ(tipoOrdenServicio.idOrdenSer);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            CargaCombustible result = (CargaCombustible)resp.result;
                            this.mapForm(result);
                            if (((CargaCombustible)resp.result).fechaCombustible.HasValue)
                            {
                                base.mainWindow.scrollContainer.IsEnabled = false;
                                base.mainWindow.enableToolBarCommands(ToolbarCommands.nuevo);
                            }
                        }
                        else if (resp.typeResult == ResultTypes.recordNotFound)
                        {
                            CargaCombustible cargaCombustible = new CargaCombustible
                            {
                                idOrdenServ = tipoOrdenServicio.idOrdenSer,
                                masterOrdServ = new MasterOrdServ
                                {
                                    chofer = new Personal { nombre = tipoOrdenServicio.chofer },
                                    fechaCreacion = tipoOrdenServicio.fechaCaptura,
                                    listTiposOrdenServicio = new List<TipoOrdenServicio> { tipoOrdenServicio },
                                    tractor = tipoOrdenServicio.unidadTrans
                                },
                                listCP = new List<CartaPorte>(),
                                listViajes = new List<Viaje>(),
                                tiempoEST = "00:00:00",
                                tiempoPTO = "00:00:00",
                                tiempoTotal = "000:00:00"
                                //fechaEntrada = 
                            };
                            mapOperador(new Personal { nombre = tipoOrdenServicio.chofer });
                            this.unidad = tipoOrdenServicio.unidadTrans;
                            mapUnidades(TipoUnidad.TRACTOR, unidad);
                            mapForm(cargaCombustible);
                        }
                        else
                        {
                            this.imprimirMensaje(resp);
                        }
                    }
                    else if (tipoOrdenServicio.idTipoOrdenServicio == 6)
                    {
                        this.enumTipoOrd = enumTipoOrdServ.COMBUSTIBLE_EXTRA;
                        stpCapturaKm.Visibility = Visibility.Hidden;
                        chcECM.IsChecked = false;
                        stpControlesCargasExtras.Visibility = Visibility.Hidden;
                        cbxCP.Visibility = Visibility.Hidden;
                        stpSumasLitros.Visibility = Visibility.Hidden;
                        cbxSellos.Visibility = Visibility.Hidden;
                        cbxTiempos.Visibility = Visibility.Hidden;
                        stpLitros.Visibility = Visibility.Hidden;

                        OperationResult resp = new CargaCombustibleSvc().getCargaCombustibleExtraByOrdenServ(tipoOrdenServicio.idOrdenSer);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            CargaCombustibleExtra cargaCombustibleExtra = (CargaCombustibleExtra)resp.result;
                            cargaCombustibleExtra.masterOrdServ = new MasterOrdServ
                            {
                                chofer = new Personal { nombre = tipoOrdenServicio.chofer },
                                fechaCreacion = tipoOrdenServicio.fechaCaptura,
                                listTiposOrdenServicio = new List<TipoOrdenServicio> { tipoOrdenServicio },
                                tractor = tipoOrdenServicio.unidadTrans
                            };
                            mapFormCargaExtra(cargaCombustibleExtra);
                            if (cargaCombustibleExtra.fechaCombustible != null)
                            {
                                base.mainWindow.scrollContainer.IsEnabled = false;
                                base.mainWindow.enableToolBarCommands(ToolbarCommands.nuevo);
                            }
                        }
                        else
                        {
                            ImprimirMensaje.imprimir(resp);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                imprimirMensaje(new OperationResult(ex));
            }
        }

        private void mapFormCargaExtra(CargaCombustibleExtra result)
        {
            if (result != null)
            {
                txtFolio.Tag = result;
                txtFolio.IsEnabled = false;
                ListViewItem newItem = new ListViewItem
                {
                    Foreground = Brushes.SteelBlue,
                    Content = result
                };
                this.lvlCarga.Items.Add(newItem);
                mapUnidades(TipoUnidad.TRACTOR, result.unidadTransporte);
                txtCombustibleCargados.Text = result.ltCombustible.Value.ToString("N4");
                txtCombustibleCargados.IsEnabled = result.ltCombustible == 0;
                ctrOperador.personal = result.chofer;
            }
            else
            {
                txtFolio.Tag = null;
                txtFolio.IsEnabled = true;
                txtFolio.Clear();
                lvlCarga.Items.Clear();
                mapUnidades(TipoUnidad.TRACTOR, null);
                txtCombustibleUtilizado.Clear();
            }
        }

        private void txtFolioViaje_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                try
                {
                    if (e.Key == Key.Enter)
                    {
                        OperationResult resp = new CartaPorteSvc().getCartaPorteById(Convert.ToInt32(this.txtFolioViaje.Text.Trim()));
                        if (resp.typeResult == ResultTypes.success)
                        {
                            if (((List<Viaje>)resp.result)[0].tractor.clave == ((UnidadTransporte)this.txtTractor.Tag).clave)
                            {
                                this.mapList(((List<Viaje>)resp.result)[0].listDetalles);
                            }
                            else
                            {
                                OperationResult result1 = new OperationResult
                                {
                                    valor = 2,
                                    mensaje = "Este viaje no corresponde al tractor seleccionado"
                                };
                                this.imprimirMensaje(result1);
                            }
                        }
                        else
                        {
                            this.imprimirMensaje(resp);
                        }
                        resp = new CartaPorteSvc().getCartaPorteById(Convert.ToInt32(this.txtFolioViaje.Text.Trim()));
                        if (resp.typeResult == ResultTypes.success)
                        {
                            resp = new CartaPorteSvc().getValesCargaByIdViaje(((List<Viaje>)resp.result)[0]);
                            if (resp.typeResult == ResultTypes.success)
                            {
                                this.addValesCarga((List<ValeCarga>)resp.result);
                            }
                            else if (resp.typeResult != ResultTypes.recordNotFound)
                            {
                                this.imprimirMensaje(resp);
                            }
                        }
                        this.txtFolioViaje.Clear();
                    }
                }
                catch (Exception exception)
                {
                    OperationResult resp = new OperationResult
                    {
                        valor = 1,
                        mensaje = exception.Message
                    };
                    this.imprimirMensaje(resp);
                }
            }
            finally
            {
            }
        }

        private void txtKm_GotFocus(object sender, RoutedEventArgs e)
        {
            if (this.txtKm.Text == "0")
            {
                this.txtKm.Clear();
            }
        }

        private void txtKm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.txtKmRecorridos.Focus();
            }
            //actualizarLitrosCargados();
           
        }
        void calcularKmRecorridos()
        {
            decimal kmIInicial = 0m;
            decimal kmIFinal = 0m;
            if (!chcECM.IsChecked.Value)
            {
                decimal.TryParse(txtUltKM.Text, out kmIInicial);
                decimal.TryParse(txtKm.Text, out kmIFinal);
                decimal kmRecorrido = (kmIFinal - kmIInicial);
                txtKmRecorridos.Text = kmRecorrido <= 0 ? "0" : kmRecorrido.ToString();
            }
            else
            {
                txtKmRecorridos.Text = "0";
            }
        }
        private void txtKm_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtKm.Text.Trim()))
            {
                this.txtKm.Text = "0";
            }
        }

        private void txtKmRecorridos_GotFocus(object sender, RoutedEventArgs e)
        {
            if (this.txtKmRecorridos.Text == "0")
            {
                this.txtKmRecorridos.Clear();
            }
        }

        private void txtKmRecorridos_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && !string.IsNullOrEmpty(this.txtKmRecorridos.Text.Trim()))
            {
                this.txtCombustibleUtilizado.Focus();
            }
        }

        private void txtKmRecorridos_LostFocus(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(this.txtKmRecorridos.Text.Trim()))
            {
                this.txtKmRecorridos.Text = "0";
            }
        }

        private void txtKmRecorridos_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.actualizarLitrosCargados();
        }

        private void txtOperador_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string str = this.txtOperador.Text.Trim();
                if (!string.IsNullOrEmpty(str))
                {
                    List<Personal> list = this.buscarPersonal(str);
                    if (list.Count == 1)
                    {
                        this.mapOperador(list[0]);
                    }
                    else if (list.Count == 0)
                    {
                        Personal operador = new selectPersonal(str, ((UnidadTransporte)this.txtTractor.Tag).empresa.clave).buscarPersonal();
                        this.mapOperador(operador);
                    }
                    else
                    {
                        Personal operador = new selectPersonal(str, ((UnidadTransporte)this.txtTractor.Tag).empresa.clave).buscarPersonal();
                        this.mapOperador(operador);
                    }
                }
            }
        }

        private void txtProveedor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.txtProveedor.SelectedItem != null)
            {
                this.txtPrecio.valor = ((ProveedorCombustible)this.txtProveedor.SelectedItem).precio;
            }
            else
            {
                this.txtPrecio.valor = decimal.Zero;
            }
        }

        private void txtProveedorCarga_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.txtProveedorCarga.SelectedItem != null)
            {
                this.txtPrecioCarga.valor = ((ProveedorCombustible)this.txtProveedorCarga.SelectedItem).precio;
            }
            else
            {
                this.txtPrecioCarga.valor = decimal.Zero;
            }
        }

        private void txtTanqueDer_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && !string.IsNullOrEmpty(this.txtTanqueDer.Text.Trim()))
            {
                this.txtTanqueRes.Focus();
            }
        }

        private void txtTanqueIzq_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && !string.IsNullOrEmpty(this.txtTanqueIzq.Text.Trim()))
            {
                this.txtTanqueDer.Focus();
            }
        }

        private void txtTiempoEst_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && !string.IsNullOrEmpty(this.txtTiempoEst.Text.Trim()))
            {
                this.txtTanqueIzq.Focus();
            }
        }

        private void txtTiempoPTO_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && !string.IsNullOrEmpty(this.txtTiempoPTO.Text.Trim()))
            {
                this.txtTiempoEst.Focus();
            }
        }

        private void txtTiempoTotal_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Key == Key.Enter) && !string.IsNullOrEmpty(this.txtTiempoTotal.Text.Trim()))
            {
                this.txtTiempoPTO.Focus();
            }
        }

        private void txtTractor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string str = this.txtTractor.Text.Trim();
                if (!string.IsNullOrEmpty(str))
                {
                    this.completarClave(ref str);
                    UnidadTransporte transporte = new UnidadTransporte();
                    List<UnidadTransporte> list = this.buscarUnidades("TC" + str);
                    if (list.Count == 1)
                    {
                        this.mapUnidades(TipoUnidad.TRACTOR, list[0]);
                        transporte = list[0];
                    }
                    else if (list.Count == 0)
                    {
                        UnidadTransporte unidad = new selectUnidadTransporte("TC", this.claveEmpresa, str).buscarUnidades();
                        this.mapUnidades(TipoUnidad.TRACTOR, unidad);
                        transporte = unidad;
                    }
                    else
                    {
                        UnidadTransporte unidad = new selectUnidadTransporte(list).selectUnidadTransporteFind();
                        this.mapUnidades(TipoUnidad.TRACTOR, unidad);
                        transporte = unidad;
                    }
                }
            }
        }

        private void txtValeExtra_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                base.Cursor = Cursors.Wait;
                if (e.Key == Key.Enter)
                {
                    int num = 0;
                    if (int.TryParse(this.txtValeExtra.Text.Trim(), out num))
                    {
                        OperationResult resp = new CargaCombustibleSvc().getCargaCombustibleExtra(num);
                        if (resp.typeResult == ResultTypes.success)
                        {
                            this.llenarTabla((CargaCombustibleExtra)resp.result);
                        }
                        else
                        {
                            ImprimirMensaje.imprimir(resp);
                        }
                    }
                    this.txtValeExtra.Clear();
                }
            }
            catch (Exception exception)
            {
                ImprimirMensaje.imprimir(exception);
            }
            finally
            {
                base.Cursor = Cursors.Arrow;
            }
        }

        public OperationResult validar()
        {
            OperationResult result = new OperationResult();
            try
            {
                CargaCombustible carga = this.txtFolio.Tag as CargaCombustible;

                if (this.unidad == null)
                {
                    return new OperationResult
                    {
                        valor = 3,
                        mensaje = "Es necesario contar con una orden de servicio"
                    };
                }
                if (ctrOperador.personal == null)
                {
                    return new OperationResult
                    {
                        valor = 3,
                        mensaje = "Es necesario el nombre del operador"
                    };
                }
                if (this.unidad.tipoUnidad.clasificacion.ToUpper() == "TRACTOR")
                {
                    if (this.txtFolio.Tag == null)
                    {
                        return new OperationResult
                        {
                            valor = 3,
                            mensaje = "Es necesario contar con una orden de servicio"
                        };
                    }
                    decimal num = 0M;
                    if (!string.IsNullOrEmpty(this.txtCombustibleUtilizado.Text.Trim()))
                    {
                        if (!decimal.TryParse(this.txtCombustibleUtilizado.Text.Trim(), out num))
                        {
                            return new OperationResult
                            {
                                valor = 3,
                                mensaje = "El valor proporcionado para el combustible utilizado no es valido"
                            };
                        }
                    }
                    else
                    {
                        return new OperationResult
                        {
                            valor = 3,
                            mensaje = "Todos los Valores son obligatorios"
                        };
                    }
                    if (!string.IsNullOrEmpty(this.txtCombustiblePTO.Text.Trim()))
                    {
                        if (!decimal.TryParse(this.txtCombustiblePTO.Text.Trim(), out num))
                        {
                            return new OperationResult
                            {
                                valor = 3,
                                mensaje = "El valor proporcionado para el combustible PTO no es valido"
                            };
                        }
                    }
                    else
                    {
                        return new OperationResult
                        {
                            valor = 3,
                            mensaje = "Todos los Valores son obligatorios"
                        };
                    }
                    if (!string.IsNullOrEmpty(this.txtCombustibleEST.Text.Trim()))
                    {
                        if (!decimal.TryParse(this.txtCombustibleEST.Text.Trim(), out num))
                        {
                            return new OperationResult
                            {
                                valor = 3,
                                mensaje = "El valor proporcionado para el combustible EST no es valido"
                            };
                        }
                    }
                    else
                    {
                        return new OperationResult
                        {
                            valor = 3,
                            mensaje = "Todos los Valores son obligatorios"
                        };
                    }
                    if (!string.IsNullOrEmpty(this.txtCombustibleCargados.Text.Trim()))
                    {
                        if (!decimal.TryParse(this.txtCombustibleCargados.Text.Trim(), out num))
                        {
                            return new OperationResult
                            {
                                valor = 3,
                                mensaje = "El valor proporcionado para el combustible EST no es valido"
                            };
                        }
                    }
                    else
                    {
                        return new OperationResult
                        {
                            valor = 3,
                            mensaje = "Todos los Valores son obligatorios"
                        };
                    }
                    char[] separator = new char[] { ':' };
                    string[] strArray = this.txtTiempoTotal.Text.Split(separator);
                    try
                    {
                        TimeSpan span = new TimeSpan(Convert.ToInt32(strArray[0]), Convert.ToInt32(strArray[1]), Convert.ToInt32(strArray[2]));
                    }
                    catch (Exception)
                    {
                        return new OperationResult
                        {
                            valor = 3,
                            mensaje = "El valor del tiempo Total no es valido"
                        };
                    }
                    char[] chArray2 = new char[] { ':' };
                    strArray = this.txtTiempoPTO.Text.Split(chArray2);
                    try
                    {
                        TimeSpan span2 = new TimeSpan(Convert.ToInt32(strArray[0]), Convert.ToInt32(strArray[1]), Convert.ToInt32(strArray[2]));
                    }
                    catch (Exception)
                    {
                        return new OperationResult
                        {
                            valor = 3,
                            mensaje = "El valor del tiempo PTO no es valido"
                        };
                    }
                    char[] chArray3 = new char[] { ':' };
                    strArray = this.txtTiempoEst.Text.Split(chArray3);
                    try
                    {
                        TimeSpan span3 = new TimeSpan(Convert.ToInt32(strArray[0]), Convert.ToInt32(strArray[1]), Convert.ToInt32(strArray[2]));
                    }
                    catch (Exception)
                    {
                        return new OperationResult
                        {
                            valor = 3,
                            mensaje = "El valor del tiempo EST no es valido"
                        };
                    }
                    if (!string.IsNullOrEmpty(this.txtKmRecorridos.Text.Trim()))
                    {
                        if (!decimal.TryParse(this.txtKmRecorridos.Text.Trim(), out num))
                        {
                            return new OperationResult
                            {
                                valor = 3,
                                mensaje = "El valor proporcionado para los kilometros recorridos no es valido"
                            };
                        }
                    }
                    else
                    {
                        return new OperationResult
                        {
                            valor = 3,
                            mensaje = "Todos los Valores son obligatorios"
                        };
                    }
                    if (!string.IsNullOrEmpty(this.txtKm.Text.Trim()))
                    {
                        if (!decimal.TryParse(this.txtKm.Text.Trim(), out num))
                        {
                            return new OperationResult
                            {
                                valor = 3,
                                mensaje = "El valor proporcionado para los kilometros Totales no es valido"
                            };
                        }
                        OperationResult ultimoKm = this.ultimoKm;
                        if (ultimoKm.typeResult == ResultTypes.success)
                        {
                            decimal num3 = (decimal)ultimoKm.result;
                            if (num < num3)
                            {
                                if (chcHubodometro.IsChecked.Value)
                                {
                                    return new OperationResult
                                    {
                                        valor = 3,
                                        mensaje = "El kilometraje Total no es Valido por que es menor al ultimo KM"
                                    };
                                }
                                
                            }
                            if (num > (num3 + 5000M))
                            {
                                return new OperationResult
                                {
                                    valor = 3,
                                    mensaje = "El kilometraje Total no es Valido, ecxede el valor maximo permitido"
                                };
                            }
                        }
                    }
                    else
                    {
                        return new OperationResult
                        {
                            valor = 3,
                            mensaje = "Todos los Valores son obligatorios"
                        };
                    }
                    if (this.txtDespachador.Tag == null)
                    {
                        return new OperationResult
                        {
                            valor = 3,
                            mensaje = "No se ha seleccionado el despachador"
                        };
                    }
                    int num2 = 0;
                    if (carga.masterOrdServ.listTiposOrdenServicio[0].unidadTrans.empresa.clave != 5)
                    {
                        if (!int.TryParse(this.txtTanqueIzq.Text.Trim(), out num2))
                        {
                            return new OperationResult
                            {
                                valor = 3,
                                mensaje = "El sello del tanque Izq no es valido"
                            };
                        }
                        if (!int.TryParse(this.txtTanqueDer.Text.Trim(), out num2))
                        {
                            return new OperationResult
                            {
                                valor = 3,
                                mensaje = "El sello del tanque Der no es valido"
                            };
                        }
                    }
                    
                    if (!string.IsNullOrEmpty(this.txtTanqueRes.Text.Trim()) && !int.TryParse(this.txtTanqueRes.Text.Trim(), out num2))
                    {
                        return new OperationResult
                        {
                            valor = 3,
                            mensaje = "El sello del tanque de reserva no es valido"
                        };
                    }
                }
                else
                {
                    if (this.txtDespachador.Tag == null)
                    {
                        return new OperationResult
                        {
                            valor = 3,
                            mensaje = "No se ha seleccionado el despachador"
                        };
                    }
                    decimal num4 = 0M;
                    if (!string.IsNullOrEmpty(this.txtCombustibleCargados.Text.Trim()))
                    {
                        if (!decimal.TryParse(this.txtCombustibleCargados.Text.Trim(), out num4))
                        {
                            return new OperationResult
                            {
                                valor = 3,
                                mensaje = "El valor proporcionado para el combustible EST no es valido"
                            };
                        }
                    }
                    else
                    {
                        return new OperationResult
                        {
                            valor = 3,
                            mensaje = "Todos los Valores son obligatorios"
                        };
                    }
                }
                if (this.chcExterno.IsChecked.Value)
                {
                    if (string.IsNullOrEmpty(this.txtTiketCarga.Text.Trim()))
                    {
                        return new OperationResult
                        {
                            valor = 3,
                            mensaje = "Se requiere proporcionar el ticket de la carga Externa"
                        };
                    }
                    if (this.txtProveedorCarga.SelectedItem == null)
                    {
                        return new OperationResult
                        {
                            valor = 3,
                            mensaje = "Se requiere seleccionar el proveedor de la carga Externa"
                        };
                    }
                    if (this.txtPrecioCarga.valor < decimal.One)
                    {
                        return new OperationResult
                        {
                            valor = 3,
                            mensaje = "Se requiere Proporcionar el precio de la carga Externa"
                        };
                    }
                    if (string.IsNullOrEmpty(this.txtFolioImpresoCarga.Text.Trim()))
                    {
                        return new OperationResult
                        {
                            valor = 3,
                            mensaje = "Se requiere proporcionar el Folio Impreso de la carga Externa"
                        };
                    }
                }
                return new OperationResult { valor = 0 };
            }
            catch (Exception exception4)
            {
                return new OperationResult
                {
                    mensaje = exception4.Message,
                    valor = 1
                };
            }
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            OperationResult result = new CargaCombustibleSvc().getProveedor(0);
            if (result.typeResult == ResultTypes.success)
            {
                this.txtProveedor.ItemsSource = (List<ProveedorCombustible>)result.result;
                this.txtProveedorCarga.ItemsSource = (List<ProveedorCombustible>)result.result;
            }
            base.mainWindow.enableToolBarCommands(ToolbarCommands.guardar | ToolbarCommands.nuevo);
            this.txtLitros.txtDecimal.TextChanged += new TextChangedEventHandler(this.TxtDecimal_TextChanged);
            this.sumAnticiposOperativos.txtDecimal.TextChanged += new TextChangedEventHandler(this.TxtDecimal_TextChanged);
            this.limpiarExtra(false);
            this.getTiposCombustible();
            this.nuevo();
            this.inicio = false;
            if (new PrivilegioSvc().consultarPrivilegio("CAPTURA_CARGAS_EXTERNAS", base.mainWindow.inicio._usuario.idUsuario).typeResult == ResultTypes.success)
            {
                this.expExterno.IsEnabled = true;
                this.stpZonaOperativa.IsEnabled = false;
                this.afectaKardex = false;
                this.chcExterno.IsChecked = true;
                this.chcExterno.IsEnabled = false;
            }
            else
            {
                this.ctrZonaOperativa.cbxZonaOpetativa.SelectionChanged += new SelectionChangedEventHandler(this.CbxZonaOpetativa_SelectionChanged);
                this.ctrZonaOperativa.cargarControl(base.mainWindow.zonaOperativa, base.mainWindow.usuario.idUsuario);
            }

        }

        private OperationResult ultimoKm =>
            new RegistroKilometrajeSvc().getUltimoKM(((UnidadTransporte)this.txtTractor.Tag).clave);

        private List<CargaCombustibleExtra> listCargasExtra
        {
            get
            {
                List<CargaCombustibleExtra> list = new List<CargaCombustibleExtra>();
                foreach (ListViewItem item in (IEnumerable)this.lvlValesExtra.Items)
                {
                    list.Add((CargaCombustibleExtra)item.Content);
                }
                return list;
            }
        }

        private void BtnCambiarDespachador_Click(object sender, RoutedEventArgs e)
        {
            mapDespachador(null);
        }
        OperationResult validarCargaExtra()
        {
            try
            {
                if (string.IsNullOrEmpty(txtCombustibleCargados.Text.Trim()))
                {
                    return new OperationResult(ResultTypes.warning, "PROPORCIONAR LOS LITROS CARGADOS");
                }
                else
                {
                    decimal i = 0;
                    if (!decimal.TryParse(txtCombustibleCargados.Text.Trim(), out i))
                    {
                        return new OperationResult(ResultTypes.warning, "LOS LITROS CARGADOS NO SON VALIDOS");
                    }
                }

                if (!afectaKardex)
                {
                    if (string.IsNullOrEmpty(txtTiketCarga.Text.Trim()))
                    {
                        return new OperationResult(ResultTypes.warning, "PROPORCIONAR EL TICKET DE CARGA");
                    }
                    if (txtProveedorCarga.SelectedItem == null)
                    {
                        return new OperationResult(ResultTypes.warning, "SE REQUIERE ELEGIR EL PROVEEDOR");
                    }
                    if (txtPrecioCarga.valor == 0)
                    {
                        return new OperationResult(ResultTypes.warning, "EL PRECIO NO PUEDE SER CERO");
                    }
                    if (string.IsNullOrEmpty(txtFolioImpresoCarga.Text.Trim()))
                    {
                        return new OperationResult(ResultTypes.warning, "EL FOLIO IMPRESO ES OBLIGATORIO");
                    }
                }
                return new OperationResult(ResultTypes.success, "");
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }

        private void BtnCambiarOperador_Click(object sender, RoutedEventArgs e)
        {
            ctrOperador.limpiar();
        }

        private void TxtUltKM_TextChanged(object sender, TextChangedEventArgs e)
        {
            calcularKmRecorridos();
        }

        private void ChcHubodometro_Checked(object sender, RoutedEventArgs e)
        {
            this.txtKm.IsEnabled = true;            
        }

        private void ChcHubodometro_Unchecked(object sender, RoutedEventArgs e)
        {
            this.txtKm.IsEnabled = false;
            this.txtKm.Text = "0";
        }
    }
}
