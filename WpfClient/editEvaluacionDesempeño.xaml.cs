﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para editEvaluacionDesempeño.xaml
    /// </summary>
    public partial class editEvaluacionDesempeño : ViewBase
    {
        public editEvaluacionDesempeño()
        {
            InitializeComponent();
        }

        private void btnCambiarPersonal_Click(object sender, RoutedEventArgs e)
        {
            mapPersonalEvaluado(null);
        }
        public override void nuevo()
        {
            base.nuevo();
            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar | ToolbarCommands.buscar);
            mapForm(null);
        }
        bool privilegioEditar = false;
        List<Personal> listaPersonal = new List<Personal>();
        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            if (mainWindow.usuario.personal.departamento == null)
            {
                ImprimirMensaje.imprimir(new OperationResult(ResultTypes.warning, "TU USUARIO NO TIENE ASIGNADO UN DEPARTAMENTO, FAVOR DE CONMUNICARSE CON RECURSOS HUMANOS"));
                return;
            }

            cargarListaPersonal();

            ctrPersonal.DataContextChanged += CtrPersonal_DataContextChanged;
            if (new PrivilegioSvc().consultarPrivilegio("Editar_Evaluacion_Desempeño", mainWindow.usuario.idUsuario).typeResult == ResultTypes.success)
            {
                privilegioEditar = true;
            }

            if (new PrivilegioSvc().consultarPrivilegio("EVALUAR_TODOS_LOS_DEPARTAMENTOS", mainWindow.usuario.idUsuario).typeResult == ResultTypes.success)
            {
                todosDepartamentos = true;
                ctrPersonal.loadedAllPersonalActivo();
            }
            else
            {
                if (this.listaPersonal.Count > 0)
                    ctrPersonal.loadedLisPersonal(this.listaPersonal);
                else
                    ctrPersonal.loadedByDepartamento(mainWindow.usuario.personal.departamento.clave);
            }
            //MOSTRAR_RESULTADOS_EVALUACIONES
            mostrarResultados = new PrivilegioSvc().consultarPrivilegio("MOSTRAR_RESULTADOS_EVALUACIONES", mainWindow.usuario.idUsuario).typeResult == ResultTypes.success;
            txtResultado.Visibility = mostrarResultados ? Visibility.Visible : Visibility.Collapsed;
            nuevo();
        }

        private void cargarListaPersonal()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new OperadorSvc().getPersonal_v2ByIdPersonal(mainWindow.usuario.personal.clave);
                if (resp.typeResult == ResultTypes.success)
                {
                    listaPersonal = resp.result as List<Personal>;
                }
                else if (resp.typeResult != ResultTypes.recordNotFound)
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        bool todosDepartamentos = false;
        bool mostrarResultados = false;
        private void CtrPersonal_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            mapPersonalEvaluado(ctrPersonal.personal);
        }

        void mapPersonalEvaluado(Personal personal)
        {
            if (personal != null)
            {
                txtFechaIngreso.Text = personal.fechaIngreso == null ? string.Empty : personal.fechaIngreso.Value.ToString("dd/MM/yyyy");
                txtAntiguedad.Text = personal.fechaIngreso == null ? string.Empty : (DateTime.Now.Year - personal.fechaIngreso.Value.Year).ToString();
            }
            else
            {
                txtFechaIngreso.Clear();
                txtAntiguedad.Clear();
            }
        }
        void cargarDescripciones()
        {
            try
            {
                Cursor = Cursors.Arrow;
                var resp = new EvaluacionDesempeñoSvc().buscarDescripcionesEvaluacion();
                if (resp.typeResult == ResultTypes.success)
                {
                    List<DescripcionEvaluacion> listaDescripcion = resp.result as List<DescripcionEvaluacion>;
                    llenarListaDescripcion(listaDescripcion);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void llenarListaDescripcion(List<DescripcionEvaluacion> listaDescripcion)
        {
            try
            {
                stpDetalleEvaluacion.Children.Clear();
                foreach (var item in listaDescripcion)
                {
                    DetalleEvaluacionDesempeño detalle = new DetalleEvaluacionDesempeño
                    {
                        idDetalleEvaluacionDesempeño = 0,
                        descripcionEvaluacion = item,
                        porcentaje = item.porcentaje,
                        valoracion = 0,
                        contCalf = contCal
                    };
                    llenarControlesDetalleEvaluacion(detalle);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        int contCal => stpDefCalf.Children.Count;
        void llenarControlesDetalleEvaluacion(DetalleEvaluacionDesempeño detalle, bool habilitar = true)
        {
            try
            {
                StackPanel stpcontroles = new StackPanel { Name = "stpControles", Orientation = Orientation.Horizontal, Tag = detalle, IsEnabled = habilitar };
                stpcontroles.Children.Add(new TextBox
                {
                    BorderBrush = Brushes.Black,
                    BorderThickness = new Thickness(1),
                    Width = 300,
                    Height = double.NaN,
                    TextWrapping = TextWrapping.Wrap,
                    Background = Brushes.SteelBlue,
                    Foreground = Brushes.White,
                    VerticalContentAlignment = VerticalAlignment.Center,
                    FontWeight = FontWeights.Bold,
                    Text = detalle.descripcionEvaluacion.factorCalificacion.factorCalificacion
                });
                stpcontroles.Children.Add(new TextBox
                {
                    BorderBrush = Brushes.Black,
                    BorderThickness = new Thickness(1),
                    Width = 500,
                    Height = double.NaN,
                    TextWrapping = TextWrapping.Wrap,
                    Text = detalle.descripcionEvaluacion.descripcion
                });

                ComboBox cbx = new ComboBox
                {
                    ItemsSource = getListaCombo(),
                    Width = 60,
                    VerticalAlignment = VerticalAlignment.Center,
                    HorizontalAlignment = HorizontalAlignment.Center,
                    Margin = new Thickness(4),
                    HorizontalContentAlignment = HorizontalAlignment.Center,
                    FontWeight = FontWeights.Bold,
                    Tag = detalle
                };
                cbx.SelectionChanged += Cbx_SelectionChanged;
                if (detalle.idDetalleEvaluacionDesempeño > 0)
                {
                    cbx.SelectedValue = detalle.valoracion;
                }

                stpcontroles.Children.Add(cbx);

                stpDetalleEvaluacion.Children.Add(stpcontroles);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private void Cbx_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cbx = sender as ComboBox;
            DetalleEvaluacionDesempeño detalle = cbx.Tag as DetalleEvaluacionDesempeño;
            detalle.valoracion = (int)cbx.SelectedItem;
            calcularResultado();
        }

        List<int> getListaCombo()
        {
            List<int> lista = new List<int>();
            for (int i = 1; i <= contCal; i++)
            {
                lista.Add(i);
            }
            return lista;
        }
        private void btnVerClassCal_Click(object sender, RoutedEventArgs e)
        {
            new visualizarDefinicionesCalificaciones().ShowDialog();
        }
        List<DetalleEvaluacionDesempeño> listaDetalles
        {
            get
            {
                if (stpDetalleEvaluacion.Children.Count == 0)
                {
                    return new List<DetalleEvaluacionDesempeño>();
                }
                else
                {
                    return stpDetalleEvaluacion.Children.Cast<StackPanel>().ToList().Select(s => s.Tag as DetalleEvaluacionDesempeño).ToList();
                }
            }
        }
        void calcularResultado()
        {
            txtResultado.Text = (listaDetalles.Sum(s => s.resultado) / 100).ToString("P2"); ;
        }
        public override OperationResult guardar()
        {
            try
            {
                Cursor = Cursors.Wait;
                OperationResult val = validar();
                if (val.typeResult != ResultTypes.success) return val;
                EvaluacionDesempeño evaluacion = mapForm();
                if (evaluacion != null)
                {
                    var resp = new EvaluacionDesempeñoSvc().saveEvaluacionDesempeño(ref evaluacion);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        mapForm(evaluacion);
                        mainWindow.habilitar = true;
                        //mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                    }
                    return resp;
                }
                else
                {
                    return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER ");
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }

        private void mapForm(EvaluacionDesempeño evaluacion)
        {
            try
            {
                if (evaluacion != null)
                {
                    ctrFolio.Tag = evaluacion;
                    ctrFolio.valor = evaluacion.idEvaluacionDesempeño;
                    dtpFechaEvaluacion.SelectedDate = evaluacion.fecha;
                    ctrPersonal.personal = evaluacion.personalEvaluada;
                    txtPersonaEvalua.personal = evaluacion.personalEvalua;
                    stpDetalleEvaluacion.Children.Clear();
                    foreach (var item in evaluacion.listaDetalles)
                    {
                        llenarControlesDetalleEvaluacion(item, evaluacion.estatus == "ACTIVO");
                    }
                    calcularResultado();
                    stpDatosGenerales.IsEnabled = false;
                    if (privilegioEditar && evaluacion.estatus.ToUpper() == "FINALIZADO")
                    {
                        stpComentarios.Visibility = Visibility.Visible;

                    }
                    txtMejora.Text = evaluacion.factoresMejora;
                    txtObs.Text = evaluacion.observaciones;
                    txtCompromiso.Text = evaluacion.compromiso;
                    txtComentarios.Text = evaluacion.comentarios;
                    if (evaluacion.estatus == "ACTIVO")
                    {
                        btnEnviar.IsEnabled = true;
                        mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
                    }
                    else
                    {
                        btnEnviar.IsEnabled = false;
                        if (privilegioEditar)
                        {
                            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.cerrar);
                        }
                        else
                        {
                            mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.cerrar);
                        }
                    }

                }
                else
                {
                    ctrFolio.Tag = null;
                    ctrFolio.valor = 0;
                    dtpFechaEvaluacion.SelectedDate = DateTime.Now;
                    ctrPersonal.personal = null;
                    txtPersonaEvalua.personal = mainWindow.usuario.personal;
                    cargarDescripciones();
                    stpDatosGenerales.IsEnabled = true;
                    //if (!privilegioEditar)
                    //{
                    stpComentarios.Visibility = Visibility.Collapsed;
                    //}
                    txtMejora.Text = string.Empty;
                    txtObs.Text = string.Empty;
                    txtCompromiso.Text = string.Empty;
                    txtComentarios.Text = string.Empty;
                    btnEnviar.IsEnabled = false;
                    calcularResultado();
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private EvaluacionDesempeño mapForm()
        {
            try
            {
                EvaluacionDesempeño evaluacion = new EvaluacionDesempeño
                {
                    idEvaluacionDesempeño = ctrFolio.Tag == null ? 0 : (ctrFolio.Tag as EvaluacionDesempeño).idEvaluacionDesempeño,
                    fecha = DateTime.Now,
                    listaDetalles = listaDetalles,
                    personalEvalua = txtPersonaEvalua.personal,
                    personalEvaluada = ctrPersonal.personal,
                    conDelCalf = contCal,
                    factoresMejora = txtMejora.Text,
                    observaciones = txtObs.Text,
                    compromiso = txtCompromiso.Text,
                    comentarios = txtComentarios.Text
                };

                if (cerrarViaje)
                {
                    evaluacion.estatus = "FINALIZADO";
                    evaluacion.usuarioCierra = mainWindow.usuario.nombreUsuario;
                    evaluacion.fechaCierra = DateTime.Now;
                }
                else
                {
                    evaluacion.estatus = ctrFolio.Tag == null ? "ACTIVO" : (ctrFolio.Tag as EvaluacionDesempeño).estatus;
                    evaluacion.usuarioCierra = ctrFolio.Tag == null ? string.Empty : (ctrFolio.Tag as EvaluacionDesempeño).usuarioCierra;
                    evaluacion.fechaCierra = ctrFolio.Tag == null ? null : (ctrFolio.Tag as EvaluacionDesempeño).fechaCierra;
                }

                return evaluacion;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }
        }

        private OperationResult validar()
        {
            try
            {
                OperationResult val = new OperationResult(ResultTypes.success, "");
                if (ctrPersonal.personal == null)
                {
                    return new OperationResult(ResultTypes.error, "SE REQUIERE EL NOMBRE DE LA PERSONA A EVALUAR");
                }
                foreach (var detalle in listaDetalles)
                {
                    if (detalle.valoracion <= 0)
                    {
                        return new OperationResult(ResultTypes.warning, string.Format("FALTO PROPORCIONAR ALGUNA VALORACIÓN"));
                    }
                }
                return val;
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
        public override void buscar()
        {
            try
            {
                EvaluacionDesempeño evaluacion = null;
                if (todosDepartamentos)
                {
                    evaluacion = new selectEvaluacionDesempeño(mostrarResultados).getAllEvaluacion();
                }
                else
                {
                    if (listaPersonal.Count > 0)
                        evaluacion = new selectEvaluacionDesempeño(mostrarResultados).getEvaluacionByIdPersonal(mainWindow.personal.clave);
                    else
                        evaluacion = new selectEvaluacionDesempeño(mostrarResultados).getEvaluacionByDepartamento(mainWindow.personal.departamento.clave);
                }

                if (evaluacion != null)
                {
                    mainWindow.enableToolBarCommands(ToolbarCommands.nuevo | ToolbarCommands.guardar | ToolbarCommands.buscar | ToolbarCommands.cerrar);
                    mapForm(evaluacion);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        bool cerrarViaje = false;
        private void btnEnviar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                cerrarViaje = true;
                ImprimirMensaje.imprimir(guardar());
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                cerrarViaje = false;
            }
        }
    }
}
