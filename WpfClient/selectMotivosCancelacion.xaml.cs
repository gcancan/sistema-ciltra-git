﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static WpfClient.ImprimirMensaje;
using Core.Models;
using CoreFletera.Interfaces;
using CoreFletera.Models;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectMotivosCancelacion.xaml
    /// </summary>
    public partial class selectMotivosCancelacion : Window
    {
        public selectMotivosCancelacion()
        {
            InitializeComponent();
        }
        private string parametro = string.Empty;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Cursor = Cursors.Wait;
                txtFind.Text = parametro;
                lvlMotivos.Focus();
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }           
        }
        private void txtFind_TextChanged(object sender, TextChangedEventArgs e)
        {
            CollectionViewSource.GetDefaultView(lvlMotivos.ItemsSource).Refresh();
        }
        public MotivoCancelacion getMotivoCancelacion()
        {
            try
            {
                getAllMootivosCancelacion();
                bool? resp = ShowDialog();
                if (resp.Value && lvlMotivos.SelectedItem != null)
                {
                    MotivoCancelacion motivo = (lvlMotivos.SelectedItem as ListViewItem).Content as MotivoCancelacion;
                    var respDet = new MotivoCancelacionSvc().getProcesosBiIdMOtivoCancelacion(motivo.idMotivoCancelacion);
                    if (respDet.typeResult == ResultTypes.success)
                    {
                        motivo.listaProcesos = respDet.result as List<enumProcesos>;
                    }
                    return motivo;
                }
                    
                else
                    return null;
                
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        void getAllMootivosCancelacion()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new MotivoCancelacionSvc().getAllMotivosCancelacion();
                if (resp.typeResult == ResultTypes.success)
                {
                    mapLista(resp.result as List<MotivoCancelacion>);
                }
                else
                {
                    imprimir(resp);
                    gridTodo.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void mapLista(List<MotivoCancelacion> list)
        {
            lvlMotivos.ItemsSource = null;
            try
            {
                List<ListViewItem> listLvl = new List<ListViewItem>();
                foreach (var item in list)
                {
                    ListViewItem lvl = new ListViewItem();
                    lvl.Content = item;
                    lvl.MouseDoubleClick += Lvl_MouseDoubleClick; ;
                    listLvl.Add(lvl);
                }
                lvlMotivos.ItemsSource = listLvl;
                CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvlMotivos.ItemsSource);
                view.Filter = UserFilter;
            }
            catch (Exception ex)
            {
                imprimir(ex);
            }
        }

        private bool UserFilter(object item)
        {
            if (String.IsNullOrEmpty(txtFind.Text))
                return true;
            else
                //return ((item as UnidadTransporte).descripcionUni.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                return ((MotivoCancelacion)(item as ListViewItem).Content).idMotivoCancelacion.ToString().IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                        ((MotivoCancelacion)(item as ListViewItem).Content).descripcion.IndexOf(txtFind.Text, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void lvlActividades_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                DialogResult = true;
            }
        }

        private void GridTodo_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
