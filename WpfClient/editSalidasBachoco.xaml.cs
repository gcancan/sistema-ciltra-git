﻿using Core.Interfaces;
using Core.Models;
using Core.Utils;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for editSalidasBachoco.xaml
    /// </summary>
    public partial class editSalidasBachoco : ViewBase
    {
        List<Zona> _listZonas = new List<Zona>();
        int claveEmpresa = 0;
        crearDocumento doc;

        public DateTime MyTime { get; set; }

        public editSalidasBachoco()
        {
            InitializeComponent();
            MyTime = DateTime.Now;
            DataContext = this;
        }

        private void ViewBase_Loaded(object sender, RoutedEventArgs e)
        {
            var respPrivilegio = new PrivilegioSvc().consultarPrivilegio("APLICAR_FECHA_VIAJE", mainWindow.inicio._usuario.idUsuario);
            if (respPrivilegio.typeResult == ResultTypes.success)
            {
                txtFecha.IsEnabled = true;
            }
            else
            {
                txtFecha.IsEnabled = false;
            }
            txtFecha.Value = DateTime.Now;
            //bool respB = false;
            //doc = new crearDocumento(ref respB);
            //if (!respB)
            //{
            //    controles.IsEnabled = false;
            //    mainWindow.enableToolBarCommands(ToolbarCommands.cerrar);
            //    return;
            //}


            //var respB = false;
            //doc = new crearDocumento(ref respB);
            //if (respB)
            //{
            //var resp = new UnidadTransporteSvc().getUnidadesALLorById();
            //List<UnidadTransporte> list = (List<UnidadTransporte>)resp.result;
            //mapCombos(list);
            claveEmpresa = mainWindow.inicio._usuario.personal.empresa.clave;
            //var respOperador = new OperadorSvc().getOperadoresALLorById();
            //List<Personal> listOperadores = (List<Personal>)respOperador.result;
            //mapCombosOperador(listOperadores);

            var respCliente = new ClienteSvc().getClientesALLorById(mainWindow.inicio._usuario.personal.empresa.clave, 0, true);
            List<Cliente> listCliente = (List<Cliente>)respCliente.result;
            mapComboClientes(listCliente);
            cbxClientes.SelectedIndex = 2;
            //cbxClientes.IsEnabled = false;

            //}
            //else
            //{
            //    controles.IsEnabled = false;
            //}
            nuevo();
            chcBoxImprimir.IsChecked = new Util().isActivoImpresion();
        }

        //private void mapCombos(List<UnidadTransporte> list)
        //{
        //    foreach (UnidadTransporte item in list)
        //    {
        //        //if (item.tipoUnidad.clasificacion.ToUpper() == "DOLLY")
        //        //{
        //        //    //cbxDolly.Items.Add(item);
        //        //}
        //        //else if (item.tipoUnidad.clasificacion.ToUpper() == "TRACTOR")
        //        //{
        //        //    cbxCamion.Items.Add(item);
        //        //}
        //        //if (item.tipoUnidad.clasificacion.ToUpper() == "TOLVA")
        //        //{
        //        //    //cbxTolva1.Items.Add(item);
        //        //    //cbxTolva2.Items.Add(item);
        //        //}
        //    }
        //}

        private void mapComboClientes(List<Cliente> listCliente)
        {
            foreach (Cliente item in listCliente)
            {
                cbxClientes.Items.Add(item);
            }
        }

        //private void mapCombosOperador(List<Personal> listOperadores)
        //{
        //    foreach (Personal item in listOperadores)
        //    {
        //        cbxChofer.Items.Add(item);
        //    }
        //}

        public override void nuevo()
        {

            base.nuevo();
            mapForm(null);
            establecerRutaXML();
            limpiarDetalles();
            txtClaveTractor.Focus();
            //txtHoraImp.Text = DateTime.Now.ToString();
            //mapCamposTractor(null);
            //mapCamposTolva1(null);
            //mapCamposDolly(null);

            mainWindow.enableToolBarCommands(ToolbarCommands.cerrar | ToolbarCommands.guardar | ToolbarCommands.nuevo);
            txtFecha.Value = DateTime.Now;
            controles.IsEnabled = true;
        }
        List<Producto> listaProductos = new List<Producto>();
        private void cbxClientes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender != null)
            {
                if (cbxClientes.SelectedItem != null)
                {
                    Cliente cliente = (Cliente)cbxClientes.SelectedItem;
                    var respZonas = new ZonasSvc().getZonasALLorById(0, ((Cliente)cbxClientes.SelectedItem).clave.ToString());
                    _listZonas = (List<Zona>)respZonas.result;
                    //lvlProductos.Items.Clear();
                    ////txtFraccion.Tag = cliente;
                    ////txtFraccion.Text = cliente.fraccion.descripcion;

                    var resp = new ProductoSvc().getALLProductosByIdORidFraccion(cliente.clave);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        listaProductos = (List<Producto>)resp.result;
                    }
                    else
                    {
                        listaProductos = new List<Producto>();
                    }
                    lvlDetalles.Items.Clear();
                    lvlDetallesNew.Items.Clear();
                    //txtRetencion.Text = cliente.impuesto.valor.ToString("0.00");
                    //txtFlete.Text = cliente.impuestoFlete.valor.ToString("0.00");
                }
            }
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            if (txtFolio.Tag != null)
            {
                if (((Viaje)txtFolio.Tag).listDetalles.Count == 0)
                {
                    MessageBox.Show("Se necesita tener al menos una carga para cerrar el viaje", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning); return;
                }
                if (!chxTimbrar.IsChecked.Value)
                {
                    var ruta = new Util().getRutaXML();
                    if (string.IsNullOrEmpty(ruta))
                    {
                        MessageBox.Show("Para proceder se requiere la ruta del archivo XML", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    //var respCliente = new SDKconsultasSvc().buscarDatosCliente(viajeBachoco.cliente.clave);
                    //if (respCliente.typeResult != ResultTypes.success)
                    //{
                    //    return respCliente;
                    //}
                    ComercialCliente cli = new ComercialCliente
                    {
                        nombre = ((Cliente)cbxClientes.SelectedItem).nombre,
                        calle = ((Cliente)cbxClientes.SelectedItem).domicilio,
                        rfc = ((Cliente)cbxClientes.SelectedItem).rfc,
                        colonia = ((Cliente)cbxClientes.SelectedItem).colonia,
                        estado = ((Cliente)cbxClientes.SelectedItem).estado,
                        municipio = ((Cliente)cbxClientes.SelectedItem).municipio,
                        pais = ((Cliente)cbxClientes.SelectedItem).pais,
                        telefono = ((Cliente)cbxClientes.SelectedItem).telefono,
                        cp = ((Cliente)cbxClientes.SelectedItem).cp
                    };
                    ComercialEmpresa emp = new ComercialEmpresa
                    {
                        nombre = ((Empresa)mainWindow.inicio._usuario.personal.empresa).nombre,
                        calle = (mainWindow.inicio._usuario.personal.empresa).direccion,
                        rfc = (mainWindow.inicio._usuario.personal.empresa).rfc,
                        colonia = (mainWindow.inicio._usuario.personal.empresa).colonia,
                        estado = (mainWindow.inicio._usuario.personal.empresa).estado,
                        municipio = (mainWindow.inicio._usuario.personal.empresa).municipio,
                        pais = (mainWindow.inicio._usuario.personal.empresa).pais,
                        telefono = (mainWindow.inicio._usuario.personal.empresa).telefono,
                        cp = (mainWindow.inicio._usuario.personal.empresa).cp
                    };

                    var datosFactura = ObtenerDatosFactura.obtenerDatos(ruta);
                    if (datosFactura == null)
                    {
                        MessageBox.Show("Ocurrio un error al intentar obtener datos del XML", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    OperationResult resp = new CartaPorteSvc().activarViajeBachoco(((Viaje)txtFolio.Tag).NumGuiaId, eEstatus.ACTIVO);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        if (new Util().isActivoImpresion())
                        {
                            var listaAgrupada = agruparByZona(((List<Viaje>)resp.result)[0].listDetalles);
                            foreach (var item in listaAgrupada)
                            {
                                imprimirReportes(cli, emp, datosFactura, item, ((List<Viaje>)resp.result)[0]);
                            }
                        }
                        MessageBox.Show("Operación Guadada con exito", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                        nuevo();
                    }
                    else
                    {
                        MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {


                }

            }
        }

        public override OperationResult guardar()
        {
            bool isSave = false;
            try
            {
                Cursor = Cursors.Wait;
                lvlDetallesNew.Focus();
                var resp = validarForm();
                if (resp.typeResult != ResultTypes.success)
                {
                    return resp;
                }
                Viaje viajeBachoco = mapForm();
                if (viajeBachoco != null)
                {
                    if (!chxTimbrar.IsChecked.Value)
                    {
                        var ruta = new Util().getRutaXML();
                        if (string.IsNullOrEmpty(ruta))
                        {
                            return new OperationResult { valor = 2, mensaje = "Para proceder se requiere la ruta del archivo XML" };
                        }
                        //var respCliente = new SDKconsultasSvc().buscarDatosCliente(viajeBachoco.cliente.clave);
                        //if (respCliente.typeResult != ResultTypes.success)
                        //{
                        //    return respCliente;
                        //}
                        ComercialCliente cli = new ComercialCliente
                        {
                            nombre = ((Cliente)cbxClientes.SelectedItem).nombre,
                            calle = ((Cliente)cbxClientes.SelectedItem).domicilio,
                            rfc = ((Cliente)cbxClientes.SelectedItem).rfc,
                            colonia = ((Cliente)cbxClientes.SelectedItem).colonia,
                            estado = ((Cliente)cbxClientes.SelectedItem).estado,
                            municipio = ((Cliente)cbxClientes.SelectedItem).municipio,
                            pais = ((Cliente)cbxClientes.SelectedItem).pais,
                            telefono = ((Cliente)cbxClientes.SelectedItem).telefono,
                            cp = ((Cliente)cbxClientes.SelectedItem).cp
                        };
                        ComercialEmpresa emp = new ComercialEmpresa
                        {
                            nombre = ((Empresa)mainWindow.inicio._usuario.personal.empresa).nombre,
                            calle = (mainWindow.inicio._usuario.personal.empresa).direccion,
                            rfc = (mainWindow.inicio._usuario.personal.empresa).rfc,
                            colonia = (mainWindow.inicio._usuario.personal.empresa).colonia,
                            estado = (mainWindow.inicio._usuario.personal.empresa).estado,
                            municipio = (mainWindow.inicio._usuario.personal.empresa).municipio,
                            pais = (mainWindow.inicio._usuario.personal.empresa).pais,
                            telefono = (mainWindow.inicio._usuario.personal.empresa).telefono,
                            cp = (mainWindow.inicio._usuario.personal.empresa).cp
                        };
                        //var respEmpresa = new SDKconsultasSvc().buscarDatosEmpresa(3);
                        //if (respEmpresa.typeResult != ResultTypes.success)
                        //{
                        //    return respEmpresa;
                        //}
                        var datosFactura = ObtenerDatosFactura.obtenerDatos(ruta);
                        if (datosFactura == null)
                        {
                            return new OperationResult { valor = 1, mensaje = "Ocurrio un error al intentar obtener datos del XML" };
                        }
                        int ordenServicio = 0;
                        OperationResult save = new CartaPorteSvc().saveCartaPorte_V2(ref viajeBachoco, ref ordenServicio);
                        if (save.typeResult == ResultTypes.success)
                        {
                            buscarViaje(viajeBachoco.NumGuiaId);
                            isSave = true;
                            return save;
                        }
                        else
                        {
                            return save;
                        }
                    }
                    else
                    {
                        //var respB = false;
                        //crearDocumento doc = new crearDocumento(ref respB);
                        //if (respB)
                        //{
                        List<string> listDoc = new List<string>();
                        listDoc = doc.llenarEstructuraDocumento(ref viajeBachoco);
                        //doc.cerrar();
                        if (listDoc != null)
                        {
                            int ordenServicio = 0;
                            OperationResult save = new CartaPorteSvc().saveCartaPorte(viajeBachoco, ref ordenServicio);
                            if (save.typeResult == ResultTypes.success)
                            {
                                mapForm((Viaje)save.result);
                                abrirDocs(listDoc);
                                isSave = true;
                                return save;
                            }
                            else
                            {
                                return save;
                            }
                        }

                        //}
                        else
                        {
                            return new OperationResult { valor = 1, result = null, mensaje = "Error al realizar la operación" };
                        }
                    }
                }
                else
                {
                    return new OperationResult { valor = 1, mensaje = "Error al construir el objeto" };
                }
            }
            catch (Exception e)
            {
                return new OperationResult { valor = 1, mensaje = e.Message };
            }
            finally
            {
                Cursor = Cursors.Arrow;
                if (isSave)
                {
                    mainWindow.habilitar = true;
                    mainWindow.scrollContainer.IsEnabled = true;
                    controles.IsEnabled = false;
                }
            }
        }
        private void buscarViaje(int numGuiaId)
        {
            try
            {
                var resp = new CartaPorteSvc().getCartaPorteById(numGuiaId);
                if (resp.typeResult == ResultTypes.success)
                {
                    mapForm(((List<Viaje>)resp.result)[0]);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }

        private List<List<CartaPorte>> agruparByZona(List<CartaPorte> listDetalles)
        {
            var grupo = new AgruparCartaPortes();
            grupo.agruparByZona(listDetalles);
            return grupo.superLista;
        }

        private void imprimirReportes(ComercialCliente respCliente, ComercialEmpresa respEmpresa, DatosFacturaXML datosFactura, List<CartaPorte> listaAgrupada, Viaje viaje)
        {
            try
            {
                Cursor = Cursors.Wait;
                ReportView reporte = new ReportView(mainWindow);
                bool resp = reporte.cartaPorteFalso(respCliente, respEmpresa, datosFactura, listaAgrupada, viaje);
                if (resp)
                {
                    //reporte.Show();
                    //MessageBox.Show("Imprimiendo carta porte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    MessageBox.Show("Ocurrio un error al intentar imprimir la Carta Porte", "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void abrirDocs(List<string> listDoc)
        {
            try
            {
                foreach (var item in listDoc)
                {
                    //System.Windows.Forms.FileDialog fil;
                    //MessageBox.Show(item);
                    System.Diagnostics.Process.Start(item);
                    //MessageBox.Show(item);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private OperationResult validarForm()
        {
            string cadena = "Para Proceder el guardado es necesario" + Environment.NewLine;
            bool error = false;
            if (cbxClientes.SelectedItem == null)
            {
                error = true;
                cadena += "* Elegir un cliente" + Environment.NewLine;
            }
            if (txtClaveTractor.Tag == null)
            {
                error = true;
                cadena += "* Prorcionar la clave del tractor" + Environment.NewLine;
            }
            if (txtClaveTolva1.Tag == null)
            {
                error = true;
                cadena += "* Prorcionar la clave del remolque 1" + Environment.NewLine;
            }

            if (txtClaveTolva2.Tag != null || txtClaveDolly.Tag != null)
            {
                if (txtClaveDolly.Tag == null)
                {
                    error = true;
                    cadena += "* Prorcionar la clave del Dolly" + Environment.NewLine;
                }
                if (txtClaveTolva2.Tag == null)
                {
                    error = true;
                    cadena += "* Prorcionar la clave de la tolva 2" + Environment.NewLine;
                }
            }
            if (!string.IsNullOrEmpty(txtTara.Text.Trim()))
            {
                decimal dec = 0m;
                if (!decimal.TryParse(txtTara.Text.Trim(), out dec))
                {
                    error = true;
                    cadena += "* Prorcionar un valor valido para la tara del viaje" + Environment.NewLine;
                }
            }
            else
            {
                error = true;
                cadena += "* Prorcionar la tara del viaje" + Environment.NewLine;
            }
            if (txtChofer.Tag == null)
            {
                error = true;
                cadena += "* Elegir un chofer" + Environment.NewLine;
            }

            if (validarLista())
            {
                error = true;
                cadena += "* Existen datos incorrectos en la lista o estan incompletos" + Environment.NewLine;
            }


            //if (Convert.ToDecimal(txtTara.Text.Trim()) < 100)
            //{
            //    cadena += "* La captura de la tara es en Kms" + Environment.NewLine;
            //    error = true;
            //}

            return new OperationResult { valor = error ? 2 : 0, mensaje = cadena };
        }

        private bool validarLista()
        {
            bool error = false;
            foreach (ListViewItem item in lvlDetalles.Items)
            {
                CartaPorte c = (CartaPorte)item.Content;
                if (c.zonaSelect == null)
                {
                    var s = c.horaImpresion;
                    error = true;
                    break;
                }
                if (c.producto == null)
                {
                    error = true;
                    break;
                }
                if (c.remision == "0" || string.IsNullOrEmpty(c.remision))
                {
                    error = true;
                    break;
                }
                if (c.pesoBruto == 0)
                {
                    error = true;
                    break;
                }
            }
            return error; ;
        }

        private void establecerFecha()
        {
            //if (string.IsNullOrEmpty(txtFecha.Text))
            //{
            //    var hora = DateTime.Now;
            //    var t = hora.TimeOfDay;

            //    string Time = (DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() + " 7:00");
            //    //Time = Time.ToString("dd/MM/yyyy hh:mm:ss tt");
            //    DateTime s = Convert.ToDateTime(Time);
            //    if (hora < s)
            //    {
            //        txtFecha.Text = DateTime.Now.AddDays(-1).ToString();
            //    }
            //    else
            //    {
            //        txtFecha.Text = DateTime.Now.ToString();
            //    }
            //}
        }

        private void mapForm(Viaje viajeBachoco)
        {
            if (viajeBachoco != null)
            {
                txtFolio.Tag = viajeBachoco;
                txtFolio.Text = viajeBachoco.NumGuiaId.ToString();
                txtFecha.Value = viajeBachoco.FechaHoraViaje;
                lvlDetalles.Items.Clear();
                int i = 0;
                foreach (Cliente item in cbxClientes.Items)
                {
                    if (item.clave == viajeBachoco.cliente.clave)
                    {
                        cbxClientes.SelectedIndex = i;
                        break;
                    }
                    i++;
                }
                mapListaNew(viajeBachoco.listDetalles);
                txtTara.Text = viajeBachoco.tara < 100 ? (viajeBachoco.tara * 1000).ToString() : viajeBachoco.tara.ToString();
                mapCamposTolva1(viajeBachoco.remolque);
                mapCamposDolly(viajeBachoco.dolly);
                mapCamposTolva2(viajeBachoco.remolque2);
                
                
                //txtKm.Text = viajeBachoco.km.ToString();
               
                i = 0;
                mapChofer(viajeBachoco.operador);
                mapChoferCapacitacion(viajeBachoco.operadorCapacitacion);
                //foreach (var item in cbxChofer.Items)
                //{
                //    if (((Personal)item).clave == viajeBachoco.operador.clave)
                //    {
                //        cbxChofer.SelectedIndex = i;
                //    }
                //    i++;
                //}
                general.IsEnabled = false;
                btnCerrar.IsEnabled = true;
                btnCancelar.IsEnabled = true;
            }
            else
            {
                txtFolio.Tag = null;
                txtFolio.Clear();
                txtFecha.Value = null;
                lvlDetalles.Items.Clear();
                lvlDetallesNew.Items.Clear();
                txtTara.Clear();
                mapCamposTractor(null);
                mapCamposTolva1(null);
                mapCamposDolly(null);
                mapCamposTolva2(null);

                mapChofer(null);
                mapChoferCapacitacion(null);
                general.IsEnabled = true;
                btnCerrar.IsEnabled = false;
                btnCancelar.IsEnabled = false;
                txtKm.Clear();
            }
            limpiarDetalles();
        }

        private void mapListaNew(List<CartaPorte> listaCartaPortes)
        {
            lvlDetallesNew.Items.Clear();
            foreach (var item in listaCartaPortes)
            {
                ListViewItem lvl = new ListViewItem();
                lvl.Content = item;
                lvlDetallesNew.Items.Add(lvl);
            }
        }

        private Viaje mapForm()
        {
            try
            {
                establecerFecha();
                Viaje viaje = new Viaje();
                viaje.cliente = ((Cliente)cbxClientes.SelectedItem);
                viaje.NumGuiaId = txtFolio.Tag == null ? 0 : ((Viaje)txtFolio.Tag).NumGuiaId;
                viaje.SerieGuia = "B";
                viaje.IdEmpresa = mainWindow.inicio._usuario.personal.empresa.clave;

                DateTime fc = (DateTime)txtFecha.Value;
                if (fc.Hour < 7)
                {
                    DateTime newfc = fc.AddDays(-1);
                    viaje.fechaBachoco = newfc;
                }
                else
                {
                    viaje.fechaBachoco = fc;
                }
                viaje.FechaHoraViaje = Convert.ToDateTime(txtFecha.Text);
                viaje.tractor = ((UnidadTransporte)txtClaveTractor.Tag);
                viaje.remolque = ((UnidadTransporte)txtClaveTolva1.Tag);
                viaje.dolly = txtClaveDolly.Tag == null ? null : ((UnidadTransporte)txtClaveDolly.Tag);
                viaje.remolque2 = txtClaveTolva2.Tag == null ? null : ((UnidadTransporte)txtClaveTolva2.Tag);
                viaje.tara = Convert.ToDecimal(txtTara.Text);
                viaje.operador = ((Personal)txtChofer.Tag);
                viaje.operadorCapacitacion = ((Personal)txtChoferCapacitacion.Tag);
                viaje.UserViaje = mainWindow.inicio._usuario.idUsuario;
                viaje.EstatusGuia = eEstatus.INICIADO.ToString();
                viaje.listDetalles = new List<CartaPorte>();
                viaje.tipoViaje = establecerTipoViaje();
                //viaje.km = Convert.ToDecimal(txtKm.Text.Trim());
                foreach (ListViewItem item in lvlDetalles.Items)
                {
                    viaje.listDetalles.Add((CartaPorte)item.Content);
                }

                return viaje;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private char establecerTipoViaje()
        {
            UnidadTransporte x = (UnidadTransporte)txtClaveTolva1.Tag;

            if (txtClaveTolva1.Tag != null || txtClaveTolva2.Tag != null)
            {
                return 'F';
            }
            else if (x.tipoUnidad.descripcion == "TOLVA 35 TON")
            {
                return 'F';
            }
            return 'S';
        }

        private void addNewDetalle_Click(object sender, RoutedEventArgs e)
        {
            addDetalle();
        }

        private void addDetalle()
        {
            if (!string.IsNullOrEmpty(txtTara.Text.Trim()))
            {
                decimal dec = 0m;
                if (!decimal.TryParse(txtTara.Text.Trim(), out dec))
                {
                    MessageBox.Show("El valor de la tara no es valido", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }
            else
            {
                MessageBox.Show("No se ha proporcionado la tara del viaje", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                txtTara.Focus();
                return;
            }

            if (!validarCampos())
            {
                return;
            }


            establecerFecha();
            string fecha = DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString() +
                " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":00";

            try
            {
                CartaPorte cP = new CartaPorte();
                cP.zonaSelect = (Zona)txtDestino.Tag;
                cP.producto = (Producto)txtProducto.Tag;
                cP.PorcIVA = ((Cliente)cbxClientes.SelectedItem).impuesto.valor;
                cP.PorcRetencion = ((Cliente)cbxClientes.SelectedItem).impuestoFlete.valor;
                cP.tara = obtenerPesoBruto();
                if ((Convert.ToDecimal(txtPesoBruto.Text.Trim()) / 1000) < cP.tara)
                {
                    MessageBox.Show("El peso bruto no puede ser menor a la ultima tara", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtPesoBruto.Focus();
                    return;
                }
                cP.pesoBruto = Convert.ToDecimal(txtPesoBruto.Text.Trim()) / 1000;
                cP.tipoPrecio = EstablecerPrecios.establecerPrecio(
                                                                        (UnidadTransporte)txtClaveTolva1.Tag,
                                                                        (UnidadTransporte)txtClaveDolly.Tag,
                                                                        (UnidadTransporte)txtClaveTolva2.Tag, mainWindow.empresa,
                                                                        (Cliente)cbxClientes.SelectedItem
                                                                     );
                cP.tipoPrecioChofer = EstablecerPrecios.establecerPrecioChofer(
                                                                                    (UnidadTransporte)txtClaveTolva1.Tag,
                                                                                    (UnidadTransporte)txtClaveDolly.Tag,
                                                                                    (UnidadTransporte)txtClaveTolva2.Tag, mainWindow.empresa
                                                                                );
                cP.horaImpresion = MyTime;
                cP.horaCaptura = DateTime.Now;
                cP.horaLlegada = Convert.ToDateTime(txtFecha.Text.Trim());
                cP.remision = txtRemision.Text.Trim();
                cP.observaciones = txtObservaciones.Text.Trim();
                ListViewItem lvl = new ListViewItem();
                lvl.Content = cP;
                lvlDetalles.Items.Add(lvl);
                limpiarDetalles();
                txtProducto.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private bool validarCampos()
        {
            if (txtProducto.Tag == null)
            {
                txtProducto.Focus(); return false;
            }
            if (txtDestino.Tag == null)
            {
                txtDestino.Focus(); return false;
            }
            decimal dec = 0m;
            if (!decimal.TryParse(txtPesoBruto.Text.Trim(), out dec))
            {
                txtPesoBruto.Clear();
                txtPesoBruto.Focus();
            }
            int i = 0;
            if (!int.TryParse(txtRemision.Text.Trim(), out i))
            {
                txtRemision.Clear();
                txtRemision.Focus();
            }
            return true;
        }

        private decimal obtenerPesoBruto()
        {
            if (lvlDetallesNew.Items.Count == 0)
            {
                if (lvlDetalles.Items.Count == 0)
                {
                    return Convert.ToDecimal(txtTara.Text.Trim()) / 1000;
                }
                else
                {
                    CartaPorte c = (CartaPorte)((ListViewItem)lvlDetalles.Items[lvlDetalles.Items.Count - 1]).Content;
                    return c.pesoBruto;
                }
            }
            else
            {
                if (lvlDetalles.Items.Count == 0)
                {
                    CartaPorte c = (CartaPorte)((ListViewItem)lvlDetallesNew.Items[lvlDetallesNew.Items.Count - 1]).Content;
                    return c.pesoBruto;
                }
                else
                {
                    CartaPorte c = (CartaPorte)((ListViewItem)lvlDetalles.Items[lvlDetalles.Items.Count - 1]).Content;
                    return c.pesoBruto;
                }

            }
        }

        private TipoPrecio establecerPrecio()
        {
            UnidadTransporte x = (UnidadTransporte)txtClaveTolva1.Tag;

            if (txtClaveDolly.Tag != null || txtClaveTolva2.Tag != null)
            {
                return TipoPrecio.FULL;
            }
            else if (x.tipoUnidad.descripcion == "TOLVA 35 TON")
            {
                return TipoPrecio.SENCILLO35;
            }
            else
            {
                return TipoPrecio.SENCILLO;
            }
        }

        private void txtClaveTractor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string clave = txtClaveTractor.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                completarClave(ref clave);

                List<UnidadTransporte> ListUnidadT = buscarUnidades("TC" + clave);
                if (ListUnidadT.Count == 1)
                {
                    mapCamposTractor(ListUnidadT[0], clave);
                }
                else
                if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("TC", claveEmpresa, "TC" + clave);
                    var s = buscardor.buscarUnidades();
                    validarTractor(s, clave);
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    validarTractor(s, clave);
                }
            }
        }

        private void validarTractor(UnidadTransporte s, string clave)
        {
            if (s != null)
            {
                clave = s.clave.Replace("TC", "");
                mapCamposTractor(s, clave);
            }
        }

        private void mapCamposTractor(UnidadTransporte unidadTransporte, string clave = "")
        {
            if (unidadTransporte != null)
            {
                txtClaveTractor.Text = clave;
                txtClaveTractor.Tag = unidadTransporte;
                txtDescripcioTractor.Content = unidadTransporte.tipoUnidad.descripcion;
                txtClaveTractor.IsEnabled = false;
                txtClaveTolva1.Focus();
                OperationResult resp = new CartaPorteSvc().getCartaPorteByIdCamion(unidadTransporte.clave);
                switch (resp.typeResult)
                {
                    case ResultTypes.success:
                        mapForm(((List<Viaje>)resp.result)[0]);
                        txtProducto.Focus();
                        break;
                    case ResultTypes.error:
                        MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        break;
                    case ResultTypes.warning:
                        MessageBox.Show(resp.mensaje, "Advertencia", MessageBoxButton.OK, MessageBoxImage.Warning);
                        break;
                    case ResultTypes.recordNotFound:
                        break;
                }
            }
            else
            {
                txtClaveTractor.Text = "";
                txtClaveTractor.Tag = null;
                txtDescripcioTractor.Content = "";
                txtClaveTractor.IsEnabled = true;
            }
        }

        private void completarClave(ref string clave)
        {
            if (clave.Length < 3)
            {
                for (int i = 0; i <= (3 - clave.Length); i++)
                {
                    clave = "0" + clave;
                }
            }
        }
        private List<UnidadTransporte> buscarUnidades(string clave)
        {
            OperationResult resp = new UnidadTransporteSvc().getUnidadesALLorById(claveEmpresa, clave);
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    return (List<UnidadTransporte>)resp.result;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return null;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return null;
                case ResultTypes.recordNotFound:
                    return new List<UnidadTransporte>();

                default:
                    return null;
            }
        }

        private void txtClaveTolva1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string clave = txtClaveTolva1.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                completarClave(ref clave);
                List<UnidadTransporte> ListUnidadT = buscarUnidades("TV" + clave);
                if (ListUnidadT.Count == 1)
                {
                    mapCamposTolva1(ListUnidadT[0], clave);
                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("TV", claveEmpresa, "TC" + clave);
                    var s = buscardor.buscarUnidades();
                    validarTolva1(s, clave);
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    validarTolva1(s, clave);
                }
            }
        }
        private void validarTolva1(UnidadTransporte s, string clave)
        {
            if (s != null)
            {
                clave = s.clave.Replace("TV", "");
                mapCamposTolva1(s, clave);
            }
        }
        private void mapCamposTolva1(UnidadTransporte unidadTransporte, string clave = "")
        {
            if (unidadTransporte != null)
            {
                if (clave == "")
                {
                    txtClaveTolva1.Text = unidadTransporte.clave.Replace("TV", "");
                }
                else
                {
                    txtClaveTolva1.Text = clave;
                }
                txtClaveTolva1.Tag = unidadTransporte;
                txtDescripcioTolva1.Content = unidadTransporte.tipoUnidad.descripcion;
                txtClaveTolva1.IsEnabled = false;
                txtClaveDolly.Focus();
            }
            else
            {
                txtClaveTolva1.Text = "";
                txtClaveTolva1.Tag = null;
                txtDescripcioTolva1.Content = "";
                txtClaveTolva1.IsEnabled = true;
            }
        }

        private void txtClaveDolly_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string clave = txtClaveDolly.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    txtChofer.Focus();
                    return;
                }
                completarClave(ref clave);
                List<UnidadTransporte> ListUnidadT = buscarUnidades("DL" + clave);
                if (ListUnidadT.Count == 1)
                {
                    mapCamposDolly(ListUnidadT[0], clave);
                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("DL", claveEmpresa, "TC" + clave);
                    var s = buscardor.buscarUnidades();
                    validarDolly(s, clave);
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    validarDolly(s, clave);
                }
            }
        }

        private void validarDolly(UnidadTransporte s, string clave)
        {
            if (s != null)
            {
                clave = s.clave.Replace("DL", "");
                mapCamposDolly(s, clave);
            }
        }

        private void mapCamposDolly(UnidadTransporte unidadTransporte, string clave = "")
        {
            if (unidadTransporte != null)
            {
                if (clave == "")
                {
                    txtClaveDolly.Text = unidadTransporte.clave.Replace("DL", "");
                }
                else
                {
                    txtClaveDolly.Text = clave;
                }
                txtClaveDolly.Tag = unidadTransporte;
                txtDescripcioDolly.Content = unidadTransporte.tipoUnidad.descripcion;
                txtClaveDolly.IsEnabled = false;
                txtClaveTolva2.Focus();
            }
            else
            {
                txtClaveDolly.Text = "";
                txtClaveDolly.Tag = null;
                txtDescripcioDolly.Content = "";
                txtClaveDolly.IsEnabled = true;
            }
        }

        private void txtClaveTolva2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {

                string clave = txtClaveTolva2.Text.Trim();
                if (string.IsNullOrEmpty(clave))
                {
                    txtChofer.Focus();
                    return;
                }
                completarClave(ref clave);
                List<UnidadTransporte> ListUnidadT = buscarUnidades("TV" + clave);
                if (ListUnidadT.Count == 1)
                {
                    mapCamposTolva2(ListUnidadT[0], clave);
                }
                else if (ListUnidadT.Count == 0)
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte("TV", claveEmpresa, "TC" + clave);
                    var s = buscardor.buscarUnidades();
                    validarTolva2(s, clave);
                }
                else
                {
                    selectUnidadTransporte buscardor = new selectUnidadTransporte(ListUnidadT);
                    var s = buscardor.selectUnidadTransporteFind();
                    validarTolva2(s, clave);
                }
            }
        }

        private void validarTolva2(UnidadTransporte s, string clave)
        {
            if (s != null)
            {
                clave = s.clave.Replace("TV", "");
                mapCamposTolva2(s, clave);
            }
        }

        private void mapCamposTolva2(UnidadTransporte unidadTransporte, string clave = "")
        {
            if (unidadTransporte != null)
            {
                if (clave == "")
                {
                    txtClaveTolva2.Text = unidadTransporte.clave.Replace("TV", "");
                }
                else
                {
                    txtClaveTolva2.Text = clave;
                }
                txtClaveTolva2.Tag = unidadTransporte;
                txtDescripcioTolva2.Content = unidadTransporte.tipoUnidad.descripcion;
                txtClaveTolva2.IsEnabled = false;
                txtChofer.Focus();
            }
            else
            {
                txtClaveTolva2.Text = "";
                txtClaveTolva2.Tag = null;
                txtDescripcioTolva2.Content = "";
                txtClaveTolva2.IsEnabled = true;
            }
        }



        private void addNewZona_Click(object sender, RoutedEventArgs e)
        {
            addNewDestino destino = new addNewDestino((Cliente)cbxClientes.SelectedItem);
            var a = destino.addNewZona();
            _listZonas.Add(a);

            foreach (ListViewItem item in lvlDetalles.Items)
            {
                ((CartaPorte)item.Content).listZonas = _listZonas;
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            if (txtFolio.Tag != null)
            {
                string motivo = Interaction.InputBox("Escribir el motivo de la cancelación", "Motivo de Cancelación");
                if (!string.IsNullOrEmpty(motivo.Trim()))
                {
                    OperationResult resp = new CartaPorteSvc().cancelarViajeBachoco(((Viaje)txtFolio.Tag).NumGuiaId, eEstatus.CANCELADO, motivo.ToUpper());
                    if (resp.typeResult == ResultTypes.success)
                    {
                        MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Information);
                        mapForm((Viaje)resp.result);
                    }
                    else
                    {
                        MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Se necesita especificar el motivo de la cancelación", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }
        }

        private void btnCargaArchivoXML_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();
            //dialog
            dialog.Filter = "|*.XML";
            dialog.Title = "Elige el archivo XML para subir";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var RUTA = new Util().setRutaXML(dialog.FileName);
                txtRutaXML.Text = RUTA;
            }
        }

        private void establecerRutaXML()
        {
            var RUTA = new Util().getRutaXML();
            if (System.IO.File.Exists(RUTA))
            {
                txtRutaXML.Text = RUTA;
            }
            else
            {
                txtRutaXML.Text = new Util().setRutaXML("");
            }
        }

        private void txtChofer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string clave = txtChofer.Text.Trim();
                //completarClave(ref clave);
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                List<Personal> listPersonal = buscarPersonal(clave);
                if (listPersonal.Count == 1)
                {
                    mapChofer(listPersonal[0]);
                }
                else if (listPersonal.Count == 0)
                {
                    selectPersonal buscardor = new selectPersonal(clave, claveEmpresa);
                    var per = buscardor.buscarPersonal();
                    mapChofer(per);
                }
                else
                {
                    selectPersonal buscardor = new selectPersonal(clave, claveEmpresa);
                    var per = buscardor.buscarPersonal();
                    mapChofer(per);
                }
            }

        }
        private void mapChofer(Personal personal)
        {
            if (personal != null)
            {
                txtChofer.Text = personal.nombre;
                txtChofer.Tag = personal;
                txtChofer.IsEnabled = false;
                //txtTara.Focus();
                txtChoferCapacitacion.Focus();
            }
            else
            {
                txtChofer.Text = "";
                txtChofer.Tag = null;
                txtChofer.IsEnabled = true;
            }
        }
        private List<Personal> buscarPersonal(string nombre)
        {
            OperationResult resp = new OperadorSvc().getOperadoresByNombre(nombre);
            switch (resp.typeResult)
            {
                case ResultTypes.success:
                    return (List<Personal>)resp.result;
                case ResultTypes.error:
                    MessageBox.Show(resp.mensaje, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return null;
                case ResultTypes.warning:
                    MessageBox.Show(resp.mensaje, "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return null;
                case ResultTypes.recordNotFound:
                    return new List<Personal>();

                default:
                    return null;
            }
        }

        private void txtProducto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                List<Producto> listProducto = new List<Producto>();
                var mySearch = this.listaProductos.FindAll(S => S.descripcion.IndexOf(txtProducto.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                                                           S.clave_externo.IndexOf(txtProducto.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                                                        
                foreach (var item in mySearch)
                {
                    listProducto.Add(item);
                }

                selectProducto buscador = new selectProducto(txtProducto.Text);
                switch (listProducto.Count)
                {
                    //case 0:
                    //    var resp = buscador.buscarZona(_listZonas);
                    //    mapDestinos(resp);
                    //    break;
                    case 1:
                        mapProductos(listProducto[0]);
                        break;
                    default:
                        var resp = buscador.buscarProductos(this.listaProductos);
                        mapProductos(resp);
                        break;
                }
            }
        }

        private void mapProductos(Producto producto)
        {
            if (producto != null)
            {
                txtProducto.Tag = producto;
                txtProducto.Text = producto.descripcion;
                txtProducto.IsEnabled = false;
                txtDestino.Focus();
            }
            else
            {
                txtProducto.Tag = null;
                txtProducto.Clear();
                txtProducto.IsEnabled = true;
            }
        }

        private void txtDestino_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                List<Zona> listZonas = new List<Zona>();
                var mySearch = _listZonas.FindAll(S => S.descripcion.IndexOf(txtDestino.Text, StringComparison.OrdinalIgnoreCase) >= 0 ||
                S.claveExtra.ToString().IndexOf(txtDestino.Text, StringComparison.OrdinalIgnoreCase) >= 0);
                foreach (var item in mySearch)
                {
                    listZonas.Add(item);
                }

                selectDestinos buscador = new selectDestinos(txtDestino.Text);
                switch (listZonas.Count)
                {
                    //case 0:
                    //    var resp = buscador.buscarZona(_listZonas);
                    //    mapDestinos(resp);
                    //    break;
                    case 1:
                        mapDestinos(listZonas[0]);
                        break;
                    default:
                        var resp = buscador.buscarZona(_listZonas);
                        mapDestinos(resp);
                        break;
                }
            }
        }

        void limpiarDetalles()
        {
            mapDestinos(null);
            mapProductos(null);
            txtRemision.Clear();
            MyTime = DateTime.Now;
            txtPesoBruto.Clear();
            txtObservaciones.Clear();
            //txtVolProgramado.Clear();
            //txtVolDescarga.Clear();
        }

        private void mapDestinos(Zona zona)
        {
            if (zona != null)
            {
                txtDestino.Tag = zona;
                txtDestino.Text = zona.descripcion;
                txtDestino.IsEnabled = false;
                txtPesoBruto.Focus();
            }
            else
            {
                txtDestino.Tag = null;
                txtDestino.Clear();
                txtDestino.IsEnabled = true;
            }
        }

        private void txtPesoBruto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (string.IsNullOrEmpty(txtPesoBruto.Text.Trim()))
                {
                    MessageBox.Show("Se requiere proporcionar el peso bruto", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                decimal v = 0;
                if (!decimal.TryParse(txtPesoBruto.Text.Trim(), out v))
                {
                    MessageBox.Show("El valor proporcionado para el peso bruto no es valido", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtTara.Clear();
                    return;
                }
                if (Convert.ToDecimal(txtPesoBruto.Text.Trim()) < 100)
                {
                    MessageBox.Show("La captura del peso bruto es en Kms", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                txtRemision.Focus();
            }
        }

        private void txtRemision_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtHoraImp.Focus();
            }
        }

        private void txtHoraImp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                txtObservaciones.Focus();
            }
        }

        private void txtObservaciones_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                addDetalle();
            }
        }

        private void txtTara_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (string.IsNullOrEmpty(txtTara.Text.Trim()))
                {
                    MessageBox.Show("Se requiere proporcionar la tara", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                decimal v = 0;
                if (!decimal.TryParse(txtTara.Text.Trim(), out v))
                {
                    MessageBox.Show("El valor proporcionado para la tara no es valido", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtTara.Clear();
                    return;
                }
                if (Convert.ToDecimal(txtTara.Text.Trim()) < 100)
                {
                    MessageBox.Show("La captura de la tara es en Kms", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                txtProducto.Focus();
            }
        }

        private void txtKm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (string.IsNullOrEmpty(txtKm.Text.Trim()))
                {
                    MessageBox.Show("Se requiere proporcionar el kilometraje", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                decimal v = 0;
                if (!decimal.TryParse(txtKm.Text.Trim(), out v))
                {
                    MessageBox.Show("El valor proporcionado para el kilometraje no es valido", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                    txtKm.Clear();
                    return;
                }
                var val = new RegistroKilometrajeSvc().getUltimoRegistroValido(((UnidadTransporte)txtClaveTractor.Tag).clave);
                if (val.typeResult == ResultTypes.success)
                {
                    if ((Convert.ToDecimal(txtKm.Text.Trim()) - ((RegistroKilometraje)val.result).kmFinal) >= 10000)
                    {
                        MessageBox.Show("El kilometraje Total no es Valido, ecxede el valor maximo permitido", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                    if (((RegistroKilometraje)val.result).kmFinal > Convert.ToDecimal(txtKm.Text.Trim()))
                    {
                        MessageBox.Show("El kilometraje Total no es Valido por que es menor al ultimo KM", "Aviso", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }
                txtTara.Focus();
            }

        }

        private void txtChoferCapacitacion_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string clave = txtChoferCapacitacion.Text.Trim();
                //completarClave(ref clave);
                if (string.IsNullOrEmpty(clave))
                {
                    return;
                }
                List<Personal> listPersonal = buscarPersonal(clave);
                if (listPersonal.Count == 1)
                {
                    mapChoferCapacitacion(listPersonal[0]);
                }
                else if (listPersonal.Count == 0)
                {
                    selectPersonal buscardor = new selectPersonal(clave, claveEmpresa);
                    var per = buscardor.buscarPersonal();
                    mapChoferCapacitacion(per);
                }
                else
                {
                    selectPersonal buscardor = new selectPersonal(clave, claveEmpresa);
                    var per = buscardor.buscarPersonal();
                    mapChoferCapacitacion(per);
                }
            }
        }
        private void mapChoferCapacitacion(Personal personal)
        {
            if (personal != null)
            {
                txtChoferCapacitacion.Text = personal.nombre;
                txtChoferCapacitacion.Tag = personal;
                txtChofer.IsEnabled = false;
                txtRemision.Focus();
                txtTara.Focus();
            }
            else
            {
                txtChoferCapacitacion.Text = "";
                txtChoferCapacitacion.Tag = null;
                txtChoferCapacitacion.IsEnabled = true;
            }
        }

        string archivo = System.Windows.Forms.Application.StartupPath + @"\config.ini";
        private void chcBoxImprimir_Checked(object sender, RoutedEventArgs e)
        {
            new Util().Write("IMPRIMIR", "ACTIVO", "true", archivo);
        }

        private void chcBoxImprimir_Unchecked(object sender, RoutedEventArgs e)
        {
            new Util().Write("IMPRIMIR", "ACTIVO", "false", archivo);
        }
    }
}
