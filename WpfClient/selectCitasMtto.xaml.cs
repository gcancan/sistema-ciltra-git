﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
namespace WpfClient
{
    /// <summary>
    /// Lógica de interacción para selectCitasMtto.xaml
    /// </summary>
    public partial class selectCitasMtto : Window
    {
        public selectCitasMtto()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
        public CitaMantenimiento buscarCitaMantenimiento()
        {
            try
            {
                getAllCitasMatto();
                bool? result = ShowDialog();
                if (result.Value && lvlCitasMtto.SelectedItem != null)
                {
                    return (lvlCitasMtto.SelectedItem as ListViewItem).Content as CitaMantenimiento;
                }
                return null;
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
                return null;
            }            
        }
        private void getAllCitasMatto()
        {
            try
            {
                Cursor = Cursors.Wait;
                var resp = new CitaMantenimientoSvc().getAllCitasMatto();
                if (resp.typeResult == ResultTypes.success)
                {
                    llenarLista(resp.result as List<CitaMantenimiento>);
                }
                else
                {
                    ImprimirMensaje.imprimir(resp);
                }
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
            finally
            {
                Cursor = Cursors.Arrow;
            }
        }
        private void llenarLista(List<CitaMantenimiento> listaCitas)
        {
            try
            {
                lvlCitasMtto.ItemsSource = null;
                List<ListViewItem> list = new List<ListViewItem>();
                foreach (CitaMantenimiento liquidacion in listaCitas)
                {
                    ListViewItem item = new ListViewItem
                    {
                        Content = liquidacion
                    };

                    item.MouseDoubleClick += new MouseButtonEventHandler(this.Lvl_MouseDoubleClick);
                    list.Add(item);
                }
                this.lvlCitasMtto.ItemsSource = list;
                CollectionView defaultView = (CollectionView)CollectionViewSource.GetDefaultView(this.lvlCitasMtto.ItemsSource);
                defaultView.Filter = new Predicate<object>(this.UserFilter);
            }
            catch (Exception ex)
            {
                ImprimirMensaje.imprimir(ex);
            }
        }
        private bool UserFilter(object item) =>
            (string.IsNullOrEmpty(this.txtBuscador.Text) ||
            ((((CitaMantenimiento)(item as ListViewItem).Content).unidad.clave.ToString().IndexOf(this.txtBuscador.Text, StringComparison.OrdinalIgnoreCase) >= 0)));

        private void Lvl_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            DialogResult = true;
        }

        private void txtBuscador_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (lvlCitasMtto.ItemsSource == null) return;
            CollectionViewSource.GetDefaultView(this.lvlCitasMtto.ItemsSource).Refresh();
        }
    }
}
