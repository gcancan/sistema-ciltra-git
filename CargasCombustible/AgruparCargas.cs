﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;

namespace CargasCombustible
{
    internal static class AgruparCargas
    {
        internal static List<GrupoCargas> agrupar (List<CargasCombustibleCOM> lista)
        {
            try
            {
                List<GrupoCargas> listaGrupo = new List<GrupoCargas>();
                foreach (CargasCombustibleCOM carga in lista)
                {
                    bool encontro = false;
                    foreach (var grupo in listaGrupo)
                    {                        
                        if (grupo.empresa == carga.empresa && grupo.fecha == carga.fecha)
                        {
                            grupo.sumalts += carga.lts;
                            grupo.listaCargas.Add(carga);
                            encontro = true;
                            break;
                        }
                    }
                    if (!encontro)
                    {
                        listaGrupo.Add(new GrupoCargas(carga));
                    }

                }
                return listaGrupo;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
