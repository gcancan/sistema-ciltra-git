﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Core.Models;
using CoreFletera.Models;
using Core.Interfaces;

namespace CargasCombustible
{
    class CrearDocumento
    {
        public CrearDocumento()
        {

        }
        public CrearDocumento(ref bool respuesta)
        {
            try
            {
                if (iniciaSDK())
                {
                    respuesta = true;
                    //if (abrirEmpresa())
                    //{
                    //    respuesta = true;
                    //}
                    //else
                    //{
                    //    respuesta = false;
                    //}
                }
                else
                {
                    respuesta = false;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        #region Iniciar SDK
        RegistryKey KeySistema = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Computación en Acción, SA CV\CONTPAQ I COMERCIAL");

        private int lResult = 0;
        public bool iniciaSDK()
        {
            try
            {
                object lEntrada = KeySistema.GetValue("DirectorioBase");

                lResult = Declaraciones.SetCurrentDirectory(lEntrada.ToString());

                if (lResult != 0)
                {
                    var y = Declaraciones.fInicializaSDK();
                    var s = Declaraciones.fSetNombrePAQ("CONTPAQ I COMERCIAL");
                    return true;
                }
                else
                {
                    //txtMensajes.Text = "Hubo un problema";
                    Declaraciones.MuestraError(lResult);
                    return false;
                }
            }
            catch (System.ArithmeticException ex)
            {
                MessageBox.Show("Ocurrio un error al tratar de iniciar el SDK." + Environment.NewLine
                    + ex.Message, "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return false;
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrio un error al tratar de iniciar el SDK." + Environment.NewLine
                    + e.Message, "Error",
                     MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return false;
            }
        }//Fin método Abrir Conexión 
        #endregion

        #region Abrir Empresa
        public bool abrirEmpresa(string empresa)
        {
            
            lResult = Declaraciones.fAbreEmpresa(@"C:\Compac\Empresas\" + empresa);
            if (lResult == 0)
            {
                //MessageBox.Show("Se Abrio empresa");
                return true;

            }
            else
            {
                //Se verifica el error
                Declaraciones.MuestraError(lResult);
                return false;

                //txtMensajes.Text = "No fue posible Inciar";
            }

        }//Fin abrir empresa 

        internal bool realizarSalidaAlm(GrupoCargas grupo)
        {
            try
            {
                if (abrirEmpresa(grupo.empresa))
                {
                    var r = llenarEstructura(grupo);
                    return r;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        StringBuilder lSerieDocto = new StringBuilder(12);
        double lFolio;
        public bool llenarEstructura(DateTime fecha, CargaCombustibleByDia cargaCombustible)
        {
            Declaraciones.tDocumento ltDocto = new Declaraciones.tDocumento();
            Declaraciones.tMovimiento ltMovto = new Declaraciones.tMovimiento();
            int lIdDocto = 0;
            int LIdMovto = 0;
            //DateTime fecha = Convert.ToDateTime("18/02/2017");
            lSerieDocto.Append("");
            if (hayError(Declaraciones.fSiguienteFolio("CDIESEL", lSerieDocto, ref lFolio)))
            {
                return false;
            }
            else
            {
                //MessageBox.Show(lFolio.ToString());
                ltDocto.aCodConcepto = "CDIESEL";
                ltDocto.aSerie = lSerieDocto.ToString();
                //ltDocto.aFolio = lFolio;
                ltDocto.aFecha = fecha.ToString("MM/dd/yyyy");
                ltDocto.aSistemaOrigen = 0; //0 = AdminPAQ ¿Cual es comercial????
                ltDocto.aNumMoneda = 1;
                ltDocto.aTipoCambio = 1;
                ltDocto.aImporte = 0;
                ltDocto.aDescuentoDoc1 = 0;
                ltDocto.aDescuentoDoc2 = 0;
                ltDocto.aAfecta = 1;

                if (hayError(Declaraciones.fAltaDocumento(ref lIdDocto, ref ltDocto)))
                {
                    return false;
                }
                else
                {
                    ltMovto.aCodAlmacen = "D150";
                    ltMovto.aConsecutivo = 1;
                    ltMovto.aCodProdSer = "DIESEL";
                    ltMovto.aUnidades = (double)cargaCombustible.lts;
                    //ltMovto.aPrecio = 0;
                    //double.Parse((item.zonaSelect.costoFull /* + item.ImpRetencion */).ToString()) :
                    //double.Parse((item.zonaSelect.costoSencillo /* + item.ImpRetencion */).ToString());
                    ltMovto.aCosto = 0;

                    //ltMovto.

                    if (hayError(Declaraciones.fAltaMovimiento(lIdDocto, ref LIdMovto, ref ltMovto)))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
        }

        public bool llenarEstructura(GrupoCargas grupo)
        {
            Declaraciones.tDocumento ltDocto = new Declaraciones.tDocumento();
            Declaraciones.tMovimiento ltMovto = new Declaraciones.tMovimiento();
            int lIdDocto = 0;
            int LIdMovto = 0;
            //DateTime fecha = Convert.ToDateTime("18/02/2017");
            lSerieDocto.Append("");
            if (hayError(Declaraciones.fSiguienteFolio("CDIESEL", lSerieDocto, ref lFolio)))
            {
                return false;
            }
            else
            {
                //MessageBox.Show(lFolio.ToString());
                ltDocto.aCodConcepto = "CDIESEL";
                ltDocto.aSerie = lSerieDocto.ToString();
                //ltDocto.aFolio = lFolio;
                ltDocto.aFecha = grupo.fecha.ToString("MM/dd/yyyy");
                ltDocto.aSistemaOrigen = 205; //0 = AdminPAQ ¿Cual es comercial????
                ltDocto.aNumMoneda = 1;
                ltDocto.aTipoCambio = 1;
                ltDocto.aImporte = 0;
                ltDocto.aDescuentoDoc1 = 0;
                ltDocto.aDescuentoDoc2 = 0;
                ltDocto.aAfecta = 1;

                if (hayError(Declaraciones.fAltaDocumento(ref lIdDocto, ref ltDocto)))
                {
                    return false;
                }
                else
                {
                    ltMovto.aCodAlmacen = "D150";
                    ltMovto.aConsecutivo = 1;
                    ltMovto.aCodProdSer = "DIESEL";
                    ltMovto.aUnidades = (double)grupo.sumalts;
                    ltMovto.aCosto = 0;
                    if (hayError(Declaraciones.fAltaMovimiento(lIdDocto, ref LIdMovto, ref ltMovto)))
                    {
                        return false;
                    }
                    else
                    {
                        var resp = new CargaCombustibleSvc().aplicarFolioSalidaAlmacen(grupo.listaCargas, lIdDocto);
                        if (resp.typeResult != ResultTypes.success)
                        {
                            MessageBox.Show(resp.mensaje);
                        }
                        return true;
                    }
                }
            }
        }

        private bool hayError(int error)
        {
            bool resultado;
            if (error != 0)
            {
                Declaraciones.MuestraError(error);
                resultado = true;
            }
            else
            {
                resultado = false;
            }
            return resultado;
        }
    }
}
