﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.Interfaces;
using Core.Interfaces;

namespace CargasCombustible
{
    public partial class FormCargaCombustible : Form
    {
        public FormCargaCombustible()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            iniciarSDK();
        }
        CrearDocumento doc = new CrearDocumento();
        private bool iniciarSDK()
        {
            //bool valor = false;
            //doc = new CrearDocumento(ref valor);
            //if (valor)
            //{
            //    this.Enabled = valor;
            //    return valor;
            //}
            //else
            //{
            //    return false;
            //}
            return new CrearDocumento().iniciaSDK();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //if (doc.llenarEstructura())
            //{
            //    MessageBox.Show("Salida de almacen exitosa");
            //}            
        }

        void cerrarSDK()
        {
            Declaraciones.fCierraEmpresa();
            Declaraciones.fTerminaSDK();
        }

        private void FormCargaCombustible_FormClosing(object sender, FormClosingEventArgs e)
        {
            cerrarSDK();
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            var x = new CrearDocumento().abrirEmpresa("adAtlante");
        }

        private void FormCargaCombustible_Load(object sender, EventArgs e)
        {
            if (iniciarSDK())
            {
                btnIniciar.Enabled = true;
                TimerProceso.Start();
            }
        }

        private void TimerProceso_Tick(object sender, EventArgs e)
        {
            TimerProceso.Stop();
            subirCargas();
            //subirInformacion();
            TimerProceso.Start();
        }

        private void subirCargas()
        {
            try
            {
                var resp = new CargaCombustibleSvc().getCargasCom();
                if (resp.typeResult == ResultTypes.success)
                {
                    var lista = AgruparCargas.agrupar(resp.result as List<CargasCombustibleCOM>);
                    if (lista != null)
                    {
                        foreach (GrupoCargas grupo in lista)
                        {
                            var r = new CrearDocumento().realizarSalidaAlm(grupo);
                            if (r)
                            {
                                Invoke((Action)(() =>
                                {
                                    ListViewItem lvl = new ListViewItem(new string[] { grupo.fecha.ToString("dd/MM/yy"), grupo.sumalts.ToString(), grupo.empresa });
                                    lvlCargas.Items.Add(lvl);
                                    lvlCargas.Refresh();
                                }));
                            }
                            //break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void subirInformacion()
        {
            //MessageBox.Show("INICIANDO.....");
            OperationResult resp = new CargaCombustibleSvc().getFechas();
            if (resp.typeResult == ResultTypes.success)
            {
                foreach (var item in (List<DateTime>)resp.result)
                {
                    OperationResult respCargas = new CargaCombustibleSvc().getLtsCombustibleByDia(item);
                    if (respCargas.typeResult == ResultTypes.success)
                    {
                        foreach (var cc in (List<CargaCombustibleByDia>)respCargas.result)
                        {
                            //if (cc.empresa != "adPegaso")
                            //{
                            //    MessageBox.Show(cc.empresa + "  " + item.ToShortDateString());
                            //}
                            if (new CrearDocumento().abrirEmpresa(cc.empresa))
                            {
                                if (new CrearDocumento().llenarEstructura(item, cc))
                                {
                                    Declaraciones.fCierraEmpresa();
                                    OperationResult rFecha = new CargaCombustibleSvc().insertFechaCargaCombustible(item);
                                    if (rFecha.typeResult == ResultTypes.success)
                                    {
                                        Invoke((Action)(() =>
                                        {
                                            ListViewItem lvl = new ListViewItem(new string[] { item.ToString("dd/MM/yy"), cc.lts.ToString(), cc.empresa });
                                            lvlCargas.Items.Add(lvl);
                                            lvlCargas.Refresh();
                                        }));
                                    }
                                    else
                                    {
                                        //MessageBox.Show(rFecha.mensaje);
                                        return;
                                    }
                                }
                                else
                                    return;
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                    else
                    {
                        //MessageBox.Show(respCargas.mensaje);
                    }
                }
            }
            else
            {
                MessageBox.Show(resp.mensaje);
            }
        }
    }
}
