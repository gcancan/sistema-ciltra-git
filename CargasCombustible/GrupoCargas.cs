﻿using CoreFletera.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargasCombustible
{
    internal class GrupoCargas
    {
        public DateTime fecha { get; set; }
        public string empresa { get; set; }
        public decimal sumalts { get; set; }
        public List<CargasCombustibleCOM> listaCargas { get; set; }

        internal GrupoCargas(CargasCombustibleCOM carga)
        {
            fecha = carga.fecha;
            empresa = carga.empresa;
            sumalts = carga.lts;
            if (listaCargas == null)
            {
                listaCargas = new List<CargasCombustibleCOM>();
            }
            listaCargas.Add(carga);
        }
    }

}
