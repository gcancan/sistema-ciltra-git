﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreINTELISIS.SERVICIOS;
namespace CoreINTELISIS.INTERFACES
{
    public class ClienteINTELISISSvc : IClienteINTELISIS
    {
        public OperationResult getAllClientes()
             => new ClienteServiceINTELISIS().getAllClientes();
    }
}
