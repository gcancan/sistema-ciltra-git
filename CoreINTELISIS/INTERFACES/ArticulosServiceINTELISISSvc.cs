﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreINTELISIS.SERVICIOS;
namespace CoreINTELISIS.INTERFACES
{
    public class ArticulosServiceINTELISISSvc : IArticulosServiceINTELISIS
    {
        public OperationResult getAllArticulos() =>
            new ArticulosServiceINTELISIS().getAllArticulos();
    }
}
