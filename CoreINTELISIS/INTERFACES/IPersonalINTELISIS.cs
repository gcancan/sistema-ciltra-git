﻿using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreINTELISIS.INTERFACES
{
    interface IPersonalINTELISIS
    {
        OperationResult getAllPersonal();
    }
}
