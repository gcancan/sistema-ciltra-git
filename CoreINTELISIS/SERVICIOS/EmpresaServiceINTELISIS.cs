﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CoreFletera.Models;
using Core.Models;
using System.Data;
using System.Data.SqlClient;
using Core.Utils;
using CoreFletera.BusinessLogic;
namespace CoreINTELISIS.SERVICIOS
{
    public class EmpresaServiceINTELISIS
    {
        public OperationResult getAllEmpresasIntelisis()
        {
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactoryINTELISIS())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "CILTRA_sp_getAllEmpresas"))
					{
						List<Empresa> listaEmpresa = new List<Empresa>();
						con.Open();
						using (SqlDataReader reader = comad.ExecuteReader())
						{
							while (reader.Read())
							{
								Empresa empresa = mapComunesINTELISIS.mapEmpresa(reader);
								if (empresa == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");

								listaEmpresa.Add(empresa);
							}
						}
						return new OperationResult(comad, listaEmpresa);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
        }
    }
}
