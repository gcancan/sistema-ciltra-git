﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreINTELISIS.SERVICIOS
{
    internal class PersonalServiceINTELISIS
    {
        internal OperationResult getAllPersonal()
        {
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactoryINTELISIS())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "CILTRA_sp_getAllPersonal"))
					{
						con.Open();
						List<Personal> lista = new List<Personal>();
						using (SqlDataReader reader = comad.ExecuteReader())
						{
							while (reader.Read())
							{
								Personal personal = mapComunesINTELISIS.mapPersonal(reader);
								if (personal == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
								lista.Add(personal);
							}
						}
						return new OperationResult(comad, lista);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
        }
    }
}
