﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using System.Data;
using System.Data.SqlClient;
namespace CoreINTELISIS.SERVICIOS
{
    public class DepartamentoServiceINTELISIS
    {
        public OperationResult getAllDepartamentos()
        {
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactoryINTELISIS())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "CILTRA_sp_getAllDepartamentos"))
					{
						con.Open();
						List<Departamento> lista = new List<Departamento>();
						using (SqlDataReader reader = comad.ExecuteReader())
						{
							while (reader.Read())
							{
								lista.Add(new Departamento 
									{
									clave = 0,
									descripcion = (string)reader["departamento_nombre"],
									activo = Convert.ToBoolean(reader["departamento_activo"])
									});
							}
						}
						return new OperationResult(comad, lista);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
        }
    }
}
