﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
using CoreFletera.BusinessLogic;
namespace CoreINTELISIS.SERVICIOS
{
    public class UENServiceINTELISIS
    {
        public OperationResult getAllUEN()
        {
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactoryINTELISIS())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "CILTRA_sp_getAllUEN"))
					{
						con.Open();
						List<UEN> listaUEN = new List<UEN>();
						using (SqlDataReader reader = comad.ExecuteReader())
						{
							while (reader.Read())
							{
								UEN uen = mapComunesINTELISIS.mapUEN(reader);

								if (uen == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");

								listaUEN.Add(uen);
							}							
						}
						return new OperationResult(comad, listaUEN);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
        }
    }
}
