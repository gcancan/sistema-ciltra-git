﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using System.Data.SqlClient;
namespace CoreINTELISIS.SERVICIOS
{
    public static class mapComunesINTELISIS
    {
        public static Empresa mapEmpresa(SqlDataReader reader)
        {
            try
            {
                return new Empresa
                {
                    clave = 0,
                    cveINTELISIS = (string)reader["Empresa"],
                    nombre = (string)reader["razonSocial"],
                    direccion = (string)reader["direccion"],
                    noExterior = (string)reader["numeroExt"],
                    noInterior = (string)reader["numeroInt"],
                    colonia = (string)reader["colonia"],
                    municipio = (string)reader["poblacion"],
                    estado = (string)reader["estado"],
                    pais = (string)reader["pais"],
                    cp = (string)reader["cp"],
                    telefono = (string)reader["telefono"],
                    fax = (string)reader["fax"],
                    rfc = (string)reader["RFC"],
                    activo = Convert.ToBoolean((int)reader["activo"])
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal static UnidadTransporte mapUnidadTransporte(SqlDataReader reader)
        {
            try
            {
                UnidadTransporte unidad = new UnidadTransporte
                {
                    
                    cveINTELISIS = (int)reader["unidad_cveINTELISIS"],
                    empresa = new Empresa { cveINTELISIS = (string)reader["unidad_cveEmpresa"] },
                    tipoUnidad = new TipoUnidad
                    {
                        cveINTELISIS = (string)reader["unidad_cveArticulo"],
                        descripcion = (string)reader["unidad_Descripcion"]
                    },
                    clave = (string)reader["unidad_cveUnidad"],
                    estatus = (string)reader["unidad_Estatus"],
                    fechaAdquisicion = DBNull.Value.Equals(reader["unidad_FechaAdquisicion"]) ? null : (DateTime?)reader["unidad_FechaAdquisicion"],
                    placas = (string)reader["unidad_placas"],
                    documento = (string)reader["unidad_factura"],
                    anio = (int)reader["unidad_anio"],
                    serie = (string)reader["unidad_serie"]
                };
                return unidad;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal static Personal mapPersonal(SqlDataReader reader)
        {
            try
            {
                Personal personal = new Personal
                {
                    clave = 0,
                    cveEmpresaINTELISIS = (string)reader["personal_empresa"],
                    cveINTELISIS = (string)reader["personal_clave"],
                    cveAgente = DBNull.Value.Equals(reader["personal_cveAgente"]) ? null : (string)reader["personal_cveAgente"],
                    apellidoPaterno = (string)reader["personal_apellidoPat"],
                    apellidoMaterno = (string)reader["personal_apellidoMat"],
                    nombres = (string)reader["personal_nombres"],
                    calle = (string)reader["personal_direccion"],
                    tipo = (string)reader["personal_tipo"],
                    numero = (string)reader["personal_numero"],
                    numeroInt = (string)reader["personal_numeroInt"],
                    coloniaStr = (string)reader["personal_colonia"],
                    delegacion = (string)reader["personal_delegacion"],
                    ciudad = (string)reader["personal_poblacion"],
                    estado = (string)reader["personal_estado"],
                    pais = (string)reader["personal_pais"],
                    cp = Convert.ToInt32(reader["personal_cp"]),
                    telefono = (string)reader["personal_telefono"],
                    correoElectronico = (string)reader["personal_email"],
                    fechaNacimiento = DBNull.Value.Equals(reader["personal_fechaNacimiento"]) ? null : (DateTime?)reader["personal_fechaNacimiento"],
                    fechaIngreso = DBNull.Value.Equals(reader["personal_ingreso"]) ? null : (DateTime?)reader["personal_ingreso"],
                    genero = (string)reader["personal_genero"],
                    estadoCivil = (string)reader["personal_estadoCivil"],
                    departamentoStr = (string)reader["personal_departamento"],
                    puestoStr = (string)reader["personal_puesto"],
                    cvePlaza = (string)reader["personal_plaza"],
                    estatus = Convert.ToBoolean(reader["personal_activo"]) ? "ACTIVO" : "BAJA"
                };

                var datos = new DocumentacionPersonal
                {
                    CURP = (string)reader["personal_curp"],
                    RFC = (string)reader["personal_RFC"],
                    INE = (string)reader["personal_IMSS"],
                    banco = (string)reader["personal_banco"],
                    cuenta = (string)reader["personal_noCuenta"],
                };
                personal.documentacionPersonal = datos;
                return personal;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal static Plaza mapPlaza(SqlDataReader reader)
        {
            try
            {
                return new Plaza
                {
                    idPlaza = 0,
                    descripcion = (string)reader["plaza_descripcion"],
                    departamento = (string)reader["plaza_departamento"],
                    puesto = (string)reader["plaza_puesto"],
                    cveCliente = DBNull.Value.Equals(reader["plaza_personal"]) ? null : (string)reader["plaza_personal"],
                    cveINTELISIS = (string)reader["plaza_clave"],
                    nombrePersonal = DBNull.Value.Equals(reader["plaza_personalNombre"]) ? null : (string)reader["plaza_personalNombre"],
                    activo = Convert.ToBoolean(reader["plaza_activo"])
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal static TipoUnidad mapTipoUnidad(SqlDataReader reader)
        {
            try
            {
                return new TipoUnidad
                {
                    clave = 0,
                    cveINTELISIS = (string)reader["art_clave"],
                    descripcion = (string)reader["art_descripcion"],
                    clasificacion = (string)reader["art_clasificacion"],
                    TonelajeMax = (decimal)reader["art_capacidad"],
                    activo = Convert.ToBoolean(reader["art_activo"]),
                    bCombustible = Convert.ToBoolean(reader["art_combustible"]),
                    NoEjes =  ((int)reader["art_ejes"]).ToString(),
                    NoLlantas= ((int)reader["art_noLlantas"]).ToString()
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal static SucursalCliente mapSucusalCliente(SqlDataReader reader)
        {
            try
            {
                return new SucursalCliente
                {
                    idSucursalCliente = 0,
                    cveCliente = (string)reader["sucCliente_Clienteclave"],
                    cveINTELISIS = (int)reader["sucCliente_clave"],
                    nombreCliente = (string)reader["sucCliente_nombreCliente"],
                    nombre = (string)reader["sucCliente_nombre"],
                    direccion = (string)reader["sucCliente_direccion"],
                    activo = Convert.ToBoolean(reader["sucCliente_activo"])
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal static UEN mapUEN(SqlDataReader reader)
        {
            try
            {
                return new UEN()
                {
                    idUEN = 0,
                    nombre = (string)reader["UEN_nombre"],
                    activo = Convert.ToBoolean(reader["UEN_activo"]),
                    cveINTELISIS = (int)reader["UEN_clave"]
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal static Cliente mapCliente(SqlDataReader reader)
        {
            try
            {
                return new Cliente
                {
                    clave = 0,
                    cveINTELISIS = (string)reader["cliente_cve"],
                    nombre = (string)reader["cliente_nombre"],
                    alias = (string)reader["cliente_nombreCorto"],
                    domicilio = (string)reader["cliente_direccion"],
                    direccionNumenro = (string)reader["cliente_noInt"],
                    direccionNumeroInt = (string)reader["cliente_noExt"],
                    entreCalles = (string)reader["cliente_entreCalles"],
                    delegacion = (string)reader["cliente_delegacion"],
                    colonia = (string)reader["cliente_colonia"],
                    municipio = (string)reader["cliente_poblacion"],
                    estado = (string)reader["cliente_estado"],
                    pais = (string)reader["cliente_pais"],
                    cp = (string)reader["cliente_cp"],
                    rfc = (string)reader["cliente_RFC"],
                    telefono = (string)reader["cliente_telefonos"],
                    fax = (string)reader["cliente_fax"],
                    activo = Convert.ToBoolean(reader["cliente_activo"])
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal static ZonaOperativa mapZonaOperativa(SqlDataReader reader)
        {
            try
            {
                return new ZonaOperativa
                {
                    cveINTELISIS = (int)reader["sucursal_clave"],
                    nombre = (string)reader["sucursal_nombre"],
                    prefijo = (string)reader["sucursal_prefijo"],
                    activo = Convert.ToBoolean(reader["sucursal_activo"]),
                    idZonaOperativa = 0
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
