﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using CoreFletera.BusinessLogic; 
using System.Data.SqlClient;
namespace CoreINTELISIS.SERVICIOS
{
    public class SucursalClienteServiceINTELISIS
    {
        public OperationResult getAllSucursalCliente()
        {
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactoryINTELISIS())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "CILTRA_sp_getAllSucursalesCliente"))
					{
						con.Open();
						List<SucursalCliente> listaSucursales = new List<SucursalCliente>();
						using (SqlDataReader reader = comad.ExecuteReader())
						{
							while (reader.Read())
							{
								SucursalCliente sucursal = mapComunesINTELISIS.mapSucusalCliente(reader);
								if (sucursal == null) return new OperationResult(ResultTypes.error, "ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");

								listaSucursales.Add(sucursal);
							}
						}
						return new OperationResult(comad, listaSucursales);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
        }
    }
}
