﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using System.Data;
using CoreFletera.BusinessLogic;
using System.Data.SqlClient;
namespace CoreINTELISIS.SERVICIOS
{
    public class SucursalServiceINTELISIS
    {
        public OperationResult getAllSucursales()
        {
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactoryINTELISIS())
				{
					using (SqlCommand conmad = CrearSqlCommand.getCommand(con, "CILTRA_sp_getAllSucursales"))
					{
						con.Open();
						List<ZonaOperativa> listaZonaOperativas = new List<ZonaOperativa>();
						using (SqlDataReader reader = conmad.ExecuteReader())
						{
							while (reader.Read())
							{
								ZonaOperativa zona = mapComunesINTELISIS.mapZonaOperativa(reader);

								if (zona == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");

								listaZonaOperativas.Add(zona);
							}							
						}
						return new OperationResult(conmad, listaZonaOperativas);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
        }
    }
}
