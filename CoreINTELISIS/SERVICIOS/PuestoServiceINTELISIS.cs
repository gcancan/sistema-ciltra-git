﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using System.Data;
using System.Data.SqlClient;
namespace CoreINTELISIS.SERVICIOS
{
    internal class PuestoServiceINTELISIS
    {
        public OperationResult getAllPuestos()
        {
            try
            {
                using (SqlConnection con = BoundedContextFactory.ConnectionFactoryINTELISIS())
                {
                    using (SqlCommand comad = CrearSqlCommand.getCommand(con, "CILTRA_sp_getAllPuestos"))
                    {
                        con.Open();
                        List<Puesto> listaPuestos = new List<Puesto>();
                        using (SqlDataReader reader = comad.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                listaPuestos.Add(
                                    new Puesto
                                    {
                                        idPuesto = 0,
                                        activo = true,
                                        nombrePuesto = (string)reader["puesto_nombre"]
                                    });
                            }
                        }
                        return new OperationResult(comad, listaPuestos);
                    }
                }
            }
            catch (Exception ex)
            {
                return new OperationResult(ex);
            }
        }
    }
}
