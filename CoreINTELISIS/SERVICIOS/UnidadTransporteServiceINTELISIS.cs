﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
using System.Data;
using Core.Utils;
using System.Data.SqlClient;
namespace CoreINTELISIS.SERVICIOS
{
    public class UnidadTransporteServiceINTELISIS
    {
        public OperationResult getAllUnidadesTransporte()
        {
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactoryINTELISIS())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "CILTRA_gelAllUnidadesTransporte"))
					{
						con.Open();
						List<UnidadTransporte> listaUnidades = new List<UnidadTransporte>();
						using (SqlDataReader reader = comad.ExecuteReader())
						{
							while (reader.Read())
							{
								UnidadTransporte unidad = mapComunesINTELISIS.mapUnidadTransporte(reader);
								if (unidad == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
								listaUnidades.Add(unidad);
							}
							
						}
						return new OperationResult(comad, listaUnidades);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
        }
    }
}
