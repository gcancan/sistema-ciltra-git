﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using CoreFletera.BusinessLogic;
using Core.Utils;
using System.Data;
using System.Data.SqlClient;
namespace CoreINTELISIS.SERVICIOS
{
    public class ClienteServiceINTELISIS
    {
        public OperationResult getAllClientes()
        {
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactoryINTELISIS())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "CILTRA_sp_getAllClientes"))
					{
						con.Open();
						List<Cliente> listaCliente = new List<Cliente>();
						using (SqlDataReader reader = comad.ExecuteReader())
						{
							while (reader.Read())
							{
								Cliente cliente = mapComunesINTELISIS.mapCliente(reader);

								if (cliente == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");

								listaCliente.Add(cliente);
							}
						}
						return new OperationResult(comad, listaCliente);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
        }
    }
}
