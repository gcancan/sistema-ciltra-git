﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Models;
using CoreFletera.Models;
using Core.Utils;
using CoreFletera.BusinessLogic;
using System.Data;
using System.Data.SqlClient;
namespace CoreINTELISIS.SERVICIOS
{
    public class PlazaServiceINTELISIS
    {
        public OperationResult getAllPlazas()
        {
			try
			{
				using (SqlConnection con = BoundedContextFactory.ConnectionFactoryINTELISIS())
				{
					using (SqlCommand comad = CrearSqlCommand.getCommand(con, "CILTRA_sp_getAllPlazas"))
					{
						con.Open();
						List<Plaza> lista = new List<Plaza>();
						using (SqlDataReader reader = comad.ExecuteReader())
						{
							while (reader.Read())
							{
								Plaza plaza = mapComunesINTELISIS.mapPlaza(reader);
								if (plaza == null) return new OperationResult(ResultTypes.error, "OCURRIO UN ERROR AL LEER LOS RESULTADOS DE LA BUSQUEDA");
								lista.Add(plaza);
							}							
						}
						return new OperationResult(comad, lista);
					}
				}
			}
			catch (Exception ex)
			{
				return new OperationResult(ex);
			}
        }
    }
}
