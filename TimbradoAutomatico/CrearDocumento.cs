﻿
using Core.Interfaces;
using Core.Models;
using Core.Utils;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TimbradoAutomatico
{
    class CrearDocumento
    {
        public CrearDocumento()
        {

        }
        public CrearDocumento(ref bool respuesta)
        {
            try
            {
                if (iniciaSDK())
                {
                    respuesta = true;
                }
                else
                {
                    respuesta = false;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

        }

        public bool cerrar()
        {
            try
            {
                Declaraciones.fCierraEmpresa();
                Declaraciones.fTerminaSDK();
                KeySistema.Close();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }
        //string szRegKeySistema = @"SOFTWARE\\Computación en Acción, SA CV\\CONTPAQ I COMERCIAL";
        RegistryKey KeySistema = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\Computación en Acción, SA CV\CONTPAQ I COMERCIAL");

        private int lResult = 0;
        #region Iniciar SDK
        private bool iniciaSDK()
        {
            try
            {
                object lEntrada = KeySistema.GetValue("DirectorioBase");

                lResult = Declaraciones.SetCurrentDirectory(lEntrada.ToString());

                if (lResult != 0)
                {
                    var y = Declaraciones.fInicializaSDK();
                    var s = Declaraciones.fSetNombrePAQ("CONTPAQ I COMERCIAL");
                    return true;
                }
                else
                {
                    //txtMensajes.Text = "Hubo un problema";
                    Declaraciones.MuestraError(lResult);
                    return false;
                }
            }
            catch (System.ArithmeticException ex)
            {
                MessageBox.Show("Ocurrio un error al tratar de iniciar el SDK." + Environment.NewLine
                    + ex.Message, "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return false;
            }
            catch (Exception e)
            {
                MessageBox.Show("Ocurrio un error al tratar de iniciar el SDK." + Environment.NewLine
                    + e.Message, "Error",
                     MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                return false;
            }
        }//Fin método Abrir Conexión 
        #endregion
        string path = Application.StartupPath;
        string archivo = "";
        string empresa = "";
        #region Abrir Empresa
        private bool abrirEmpresa(string empresa)
        {
            //se abre la empresa
            //archivo = path + @"\config.ini";
            //empresa = new Util().Read("CONECTION_STRING_COMERCIAL", "DB_NAME", archivo);


            lResult = Declaraciones.fAbreEmpresa(@"C:\Compac\Empresas\" + empresa);
            if (lResult == 0)
            {
                return true;
                //txtMensajes.Text = "Se Abrio empresa";
            }
            else
            {
                //Se verifica el error
                Declaraciones.MuestraError(lResult);
                return false;

                //txtMensajes.Text = "No fue posible Inciar";
            }

        }//Fin abrir empresa 
        #endregion

        double lFolio;
        StringBuilder lSerieDocto = new StringBuilder(12);

        #region crear Documento

        #region Paso 2 Llenar Estructura

        public bool timbrarCartaPortes(List<Viaje> listaViajes)
        {
            try
            {
                foreach (var item in listaViajes)
                {
                    var resp = new EmpresaSvc().getEmpresasALLorById(item.IdEmpresa);
                    if (resp.typeResult == ResultTypes.success)
                    {
                        Empresa empresa = ((List<Empresa>)resp.result)[0];
                        if (abrirEmpresa(empresa.nombreComercial))
                        {
                            llenarEstructuraDocumento(item);
                            Declaraciones.fCierraEmpresa();
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool llenarEstructuraDocumento(Viaje viaje)
        {
            List<string> listArchivos = new List<string>();
            Declaraciones.tDocumento ltDocto = new Declaraciones.tDocumento();
            Declaraciones.tMovimiento ltMovto = new Declaraciones.tMovimiento();
            int lIdDocto = 0;
            int LIdMovto = 0;
            #region datosEncabezado
            DateTime fecha = viaje.FechaHoraViaje;
            string claveCliente = viaje.cliente.clave.ToString();
            string chofer = viaje.operador.clave.ToString();
            #endregion

            lSerieDocto.Append("");
            if (!GuardarAgente.guardarAgente(viaje.operador.clave.ToString(), viaje.operador.nombre))
            {
                return false;
            }
            else
            {
                foreach (var items in viaje.listDetalles)
                {
                    if (items.fCartaPorte != 0)
                    {
                        //MessageBox.Show("El viaje " + items.numGuiaId.ToString() + " consecutivo " + items.consecutivo + " ya se timbro con folio " + items.folioCartaPorte.ToString());
                        continue;
                    }
                    if (!validarExistenProductos(items))
                    {
                        return false;
                    }

                    if (hayError(Declaraciones.fSiguienteFolio("101", lSerieDocto, ref lFolio)))
                    {
                        return false;
                    }
                    else
                    {
                        //MessageBox.Show(lFolio.ToString());
                        ltDocto.aCodConcepto = "101";
                        ltDocto.aSerie = lSerieDocto.ToString();
                        //ltDocto.aFolio = lFolio;
                        ltDocto.aFecha = fecha.ToString("MM/dd/yyyy");
                        ltDocto.aCodigoCteProv = claveCliente;
                        ltDocto.aCodigoAgente = chofer;
                        ltDocto.aSistemaOrigen = 205; //0 = AdminPAQ ¿Cual es comercial????
                        ltDocto.aNumMoneda = 1;
                        ltDocto.aTipoCambio = 1;
                        ltDocto.aImporte = 0;
                        ltDocto.aDescuentoDoc1 = 0;
                        ltDocto.aDescuentoDoc2 = 0;
                        ltDocto.aAfecta = 1;

                        if (hayError(Declaraciones.fAltaDocumento(ref lIdDocto, ref ltDocto)))
                        {
                            return false;
                        }
                        else
                        {
                            //foreach (var item in viaje.listDetalles)
                            //{
                            ltMovto.aCodAlmacen = "1";
                            ltMovto.aConsecutivo = 1;
                            ltMovto.aCodProdSer = items.zonaSelect.clave.ToString();
                            ltMovto.aUnidades = double.Parse(items.volumenDescarga.ToString());
                            ltMovto.aPrecio = 0;
                            //double.Parse((item.zonaSelect.costoFull /* + item.ImpRetencion */).ToString()) :
                            //double.Parse((item.zonaSelect.costoSencillo /* + item.ImpRetencion */).ToString());
                            ltMovto.aCosto = 0;

                            //ltMovto.

                            if (hayError(Declaraciones.fAltaMovimiento(lIdDocto, ref LIdMovto, ref ltMovto)))
                            {
                                return false;
                            }
                            else
                            {
                                //var resp = new SDKconsultasSvc().updateMovimiento(LIdMovto.ToString(), items.remision.ToString(), items.producto.descripcion);
                                //MessageBox.Show(resp.mensaje+ " - " + LIdMovto.ToString());
                                if (hayError(Declaraciones.fEmitirDocumento("101", "", lFolio, "12345678a", "")))
                                {

                                    return false;
                                }
                                else
                                {
                                    //MessageBox.Show("El documento se timbro");
                                    if (hayError(Declaraciones.fEntregEnDiscoXML("101", "", lFolio, 1, "TransportistasNew2.rdl")))
                                    {

                                    }
                                    else
                                    {
                                        if (hayError(Declaraciones.fEntregEnDiscoXML("101", "", lFolio, 0, "TransportistasNew2.rdl")))
                                        {
                                            return false;
                                        }
                                        else
                                        {
                                            //var archivo = string.Format("C:\\Compac\\Empresas\\{0}\\XML_SDK\\", empresa) + lFolio.ToString() + ".pdf";
                                            //listArchivos.Add(archivo);

                                        }
                                    }
                                    items.Consecutivo = (int)lFolio;
                                    CartaPorteSvc svc = new CartaPorteSvc();
                                    svc.upDateDetalles(items);
                                    return true;
                                }
                            }
                            //}//Fin else Movimientos
                        }//Fin else Documentos
                    }//Fin siguiente Fólio
                }
            }


            return true;
            //if (hayError(Declaraciones.fAfectaDocto(ref ltDocto, true)))
            //{
            //    //Hay Error
            //}
            //else
            //{
            //    MessageBox.Show("Documento Creado. Folio: " + ltDocto.aFolio.ToString());
            //}

        }//Fin Método Llenar Estructura        
        #endregion

        private bool validarCliente(Cliente cliente)
        {
            //try
            //{
            //    var resp = existeCliente(cliente.rfc);
            //    switch (resp.typeResult)
            //    {
            //        case ResultTypes.success:
            //            return true;
            //        case ResultTypes.recordNotFound:
            //            return InsertarCliente(cliente);   //SOF060324K  
            //        default:
            //            listaMensajes.Add(DateTime.Now.ToString() + " " + resp.mensaje);
            //            return false;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    listaMensajes.Add(DateTime.Now.ToString() + " " + ex.Message);
            //    return false;
            //}
            return false;
        }
        public bool llenarEstructuraDocumento_v2(Viaje viaje)
        {
            List<string> listArchivos = new List<string>();
            Declaraciones.tDocumento ltDocto = new Declaraciones.tDocumento();
            Declaraciones.tMovimiento ltMovto = new Declaraciones.tMovimiento();
            int lIdDocto = 0;
            int LIdMovto = 0;
            #region datosEncabezado
            DateTime fecha = viaje.FechaHoraViaje;
            string claveCliente = viaje.cliente.clave.ToString();
            string chofer = viaje.operador.clave.ToString();
            #endregion

            lSerieDocto.Append("");
            if (!GuardarAgente.guardarAgente(viaje.operador.clave.ToString(), viaje.operador.nombre))
            {
                return false;
            }
            else
            {
                foreach (var items in viaje.listDetalles)
                {
                    if (items.fCartaPorte == 0)
                    {
                        if (!validarExistenProductos(items))
                        {
                            return false;
                        }

                        if (hayError(Declaraciones.fSiguienteFolio("101", lSerieDocto, ref lFolio)))
                        {
                            return false;
                        }
                        else
                        {
                            //MessageBox.Show(lFolio.ToString());
                            ltDocto.aCodConcepto = "101";
                            ltDocto.aSerie = lSerieDocto.ToString();
                            //ltDocto.aFolio = lFolio;
                            ltDocto.aFecha = fecha.ToString("MM/dd/yyyy");
                            ltDocto.aCodigoCteProv = claveCliente;
                            ltDocto.aCodigoAgente = chofer;
                            ltDocto.aSistemaOrigen = 205; //0 = AdminPAQ ¿Cual es comercial????
                            ltDocto.aNumMoneda = 1;
                            ltDocto.aTipoCambio = 1;
                            ltDocto.aImporte = 0;
                            ltDocto.aDescuentoDoc1 = 0;
                            ltDocto.aDescuentoDoc2 = 0;
                            ltDocto.aAfecta = 1;

                            if (hayError(Declaraciones.fAltaDocumento(ref lIdDocto, ref ltDocto)))
                            {
                                return false;
                            }
                            else
                            {
                                //foreach (var item in viaje.listDetalles)
                                //{
                                ltMovto.aCodAlmacen = "1";
                                ltMovto.aConsecutivo = 1;
                                ltMovto.aCodProdSer = items.zonaSelect.clave.ToString();
                                ltMovto.aUnidades = double.Parse(items.volumenDescarga.ToString());
                                ltMovto.aPrecio = 0;
                                //double.Parse((item.zonaSelect.costoFull /* + item.ImpRetencion */).ToString()) :
                                //double.Parse((item.zonaSelect.costoSencillo /* + item.ImpRetencion */).ToString());
                                ltMovto.aCosto = 0;

                                //ltMovto.

                                if (hayError(Declaraciones.fAltaMovimiento(lIdDocto, ref LIdMovto, ref ltMovto)))
                                {
                                    return false;
                                }
                                else
                                {
                                    //var resp = new SDKconsultasSvc().updateMovimiento(LIdMovto.ToString(), items.remision.ToString(), items.producto.descripcion);
                                    //MessageBox.Show(resp.mensaje+ " - " + LIdMovto.ToString());
                                    if (hayError(Declaraciones.fEmitirDocumento("101", "", lFolio, "12345678a", "")))
                                    {

                                        return false;
                                    }
                                    else
                                    {
                                        //MessageBox.Show("El documento se timbro");
                                        if (hayError(Declaraciones.fEntregEnDiscoXML("101", "", lFolio, 1, "TransportistasNew2.rdl")))
                                        {

                                        }
                                        else
                                        {
                                            if (hayError(Declaraciones.fEntregEnDiscoXML("101", "", lFolio, 0, "TransportistasNew2.rdl")))
                                            {
                                                return false;
                                            }
                                            else
                                            {
                                                //var archivo = string.Format("C:\\Compac\\Empresas\\{0}\\XML_SDK\\", empresa) + lFolio.ToString() + ".pdf";
                                                //listArchivos.Add(archivo);

                                            }
                                        }
                                        items.Consecutivo = (int)lFolio;
                                        CartaPorteSvc svc = new CartaPorteSvc();
                                        svc.upDateDetalles(items);
                                        return true;
                                    }
                                }
                                //}//Fin else Movimientos
                            }//Fin else Documentos
                        }
                    }
                    else if (items.timbrado == false)
                    {

                    }
                    //Fin siguiente Fólio
                }
            }


            return true;
            //if (hayError(Declaraciones.fAfectaDocto(ref ltDocto, true)))
            //{
            //    //Hay Error
            //}
            //else
            //{
            //    MessageBox.Show("Documento Creado. Folio: " + ltDocto.aFolio.ToString());
            //}

        }
        #region Paso 2 Llenar Estructura
        //public List<string> llenarEstructuraDocumento(ref ViajeBachoco viaje)
        //{
        //    List<string> listArchivos = new List<string>();
        //    Declaraciones.tDocumento ltDocto = new Declaraciones.tDocumento();
        //    Declaraciones.tMovimiento ltMovto = new Declaraciones.tMovimiento();
        //    int lIdDocto = 0;
        //    int LIdMovto = 0;
        //    #region datosEncabezado
        //    DateTime fecha = viaje.fechaViaje;
        //    string claveCliente = viaje.cliente.clave.ToString();
        //    string chofer = viaje.chofer.clave.ToString();
        //    #endregion

        //    lSerieDocto.Append("");
        //    if (!GuardarAgente.guardarAgente(viaje.chofer.clave.ToString(), viaje.chofer.nombre))
        //    {
        //        return null;
        //    }
        //    else
        //    {
        //        foreach (var items in viaje.listaCartaPortes)
        //        {
        //            if (items.fFolio != 0)
        //            {
        //                continue;
        //            }
        //            if (!validarExistenProductos(items))
        //            {
        //                return null;
        //            }

        //            if (hayError(Declaraciones.fSiguienteFolio("101", lSerieDocto, ref lFolio)))
        //            {
        //                return null;
        //            }
        //            else
        //            {
        //                //MessageBox.Show(lFolio.ToString());
        //                ltDocto.aCodConcepto = "101";
        //                ltDocto.aSerie = lSerieDocto.ToString();
        //                //ltDocto.aFolio = lFolio;
        //                ltDocto.aFecha = fecha.ToString("MM/dd/yyyy");
        //                ltDocto.aCodigoCteProv = claveCliente;
        //                ltDocto.aCodigoAgente = chofer;
        //                ltDocto.aSistemaOrigen = 0; //0 = AdminPAQ ¿Cual es comercial????
        //                ltDocto.aNumMoneda = 1;
        //                ltDocto.aTipoCambio = 1;
        //                ltDocto.aImporte = 0;
        //                ltDocto.aDescuentoDoc1 = 0;
        //                ltDocto.aDescuentoDoc2 = 0;
        //                ltDocto.aAfecta = 1;

        //                if (hayError(Declaraciones.fAltaDocumento(ref lIdDocto, ref ltDocto)))
        //                {
        //                    return null;
        //                }
        //                else
        //                {
        //                    //foreach (var item in viaje.listDetalles)
        //                    //{
        //                    ltMovto.aCodAlmacen = "1";
        //                    ltMovto.aConsecutivo = 1;

        //                    ltMovto.aCodProdSer = items.destino.clave.ToString();
        //                    ltMovto.aUnidades = double.Parse(items.volumenCarga.ToString());
        //                    ltMovto.aPrecio = 0;
        //                    ltMovto.aCosto = 0;

        //                    //ltMovto.

        //                    if (hayError(Declaraciones.fAltaMovimiento(lIdDocto, ref LIdMovto, ref ltMovto)))
        //                    {
        //                        return null;
        //                    }
        //                    else
        //                    {
        //                        var resp = new SDKconsultasSvc().updateMovimiento(LIdMovto.ToString(), items.remision.ToString(), items.producto.descripcion);
        //                        //MessageBox.Show(resp.mensaje+ " - " + LIdMovto.ToString());
        //                        if (hayError(Declaraciones.fEmitirDocumento("101", "", lFolio, "12345678a", "")))
        //                        {
        //                            return null;
        //                        }
        //                        else
        //                        {
        //                            //MessageBox.Show("El documento se timbro");
        //                            if (hayError(Declaraciones.fEntregEnDiscoXML("101", "", lFolio, 1, "")))
        //                            {

        //                            }
        //                            else
        //                            {
        //                                if (hayError(Declaraciones.fEntregEnDiscoXML("101", "", lFolio, 0, "")))
        //                                {
        //                                    return null;
        //                                }
        //                                else
        //                                {
        //                                    var archivo = "C:\\Compac\\Empresas\\adPrueba\\XML_SDK\\" + lFolio.ToString() + ".pdf";
        //                                    listArchivos.Add(archivo);
        //                                    items.fFolio = (int)lFolio;
        //                                }
        //                            }
        //                        }
        //                    }
        //                    //}//Fin else Movimientos
        //                }//Fin else Documentos
        //            }//Fin siguiente Fólio
        //        }
        //    }


        //    return listArchivos;
        //    //if (hayError(Declaraciones.fAfectaDocto(ref ltDocto, true)))
        //    //{
        //    //    //Hay Error
        //    //}
        //    //else
        //    //{
        //    //    MessageBox.Show("Documento Creado. Folio: " + ltDocto.aFolio.ToString());
        //    //}

        //}//Fin Método Llenar Estructura        
        #endregion

        private bool hayError(int error)
        {
            bool resultado;

            if (error != 0)
            {
                Declaraciones.MuestraError(error);
                resultado = true;
            }
            else
            {
                resultado = false;
            }

            return resultado;
        }

        #endregion

        private bool validarExistenProductos(CartaPorte cartaPorte)
        {

            OperationResult resp = new SDKconsultasSvc().existeProductoComercial("RUTA" + cartaPorte.zonaSelect.clave.ToString());
            if (resp.typeResult == ResultTypes.error)
            {
                MessageBox.Show(resp.mensaje + Environment.NewLine + "Servicio: " + cartaPorte.zonaSelect.descripcion);
                return false;
            }
            if (resp.typeResult == ResultTypes.warning)
            {
                MessageBox.Show(resp.mensaje + Environment.NewLine + "Servicio: " + cartaPorte.zonaSelect.descripcion);
                return false;
            }
            if (resp.typeResult == ResultTypes.recordNotFound)
            {
                //MessageBox.Show(cartaPorte.destino.idDestino.ToString() + " - " + cartaPorte.destino.descripcion);
                //return true;
                return !GuardarServicio.guardarSDKSinMensaje(cartaPorte.zonaSelect, eAction.NUEVO);

            }
            if (resp.typeResult == ResultTypes.success)
            {
                return true;
            }

            return true;
        }
    }
}
