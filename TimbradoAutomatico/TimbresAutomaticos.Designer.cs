﻿namespace TimbradoAutomatico
{
    partial class TimbresAutomaticos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvlTimbres = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // lvlTimbres
            // 
            this.lvlTimbres.Location = new System.Drawing.Point(21, 12);
            this.lvlTimbres.Name = "lvlTimbres";
            this.lvlTimbres.Size = new System.Drawing.Size(435, 226);
            this.lvlTimbres.TabIndex = 0;
            this.lvlTimbres.UseCompatibleStateImageBehavior = false;
            // 
            // TimbresAutomaticos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 254);
            this.Controls.Add(this.lvlTimbres);
            this.Name = "TimbresAutomaticos";
            this.Text = "Timbres Automaticos";
            this.Load += new System.EventHandler(this.TimbresAutomaticos_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvlTimbres;
    }
}

