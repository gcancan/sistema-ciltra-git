﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using time = System.Timers;
using System.Windows.Forms;
using Core.Interfaces;
using Core.Models;

namespace TimbradoAutomatico
{
    public partial class TimbresAutomaticos : Form
    {
        public TimbresAutomaticos()
        {
            InitializeComponent();
        }
        time.Timer tiempo = new time.Timer { Interval = 1000 };
        private void TimbresAutomaticos_Load(object sender, EventArgs e)
        {
            if (!iniciarSDK())
            {
                this.Enabled = false;
                return;
            }
            else
            {
                tiempo.Elapsed += Tiempo_Elapsed;
                tiempo.Start();
            }
        }
        int contador = 0;
        private void Tiempo_Elapsed(object sender, time.ElapsedEventArgs e)
        {
            contador++;
            if (contador > 5)
            {
                Invoke((Action)(() =>
                {
                    tiempo.Stop();
                    contador = 0;
                    busqueda();
                    MessageBox.Show("Terminado");
                    tiempo.Start();
                }));
            }
        }
        public List<Viaje> lista = new List<Viaje>();
        private bool busqueda()
        {
            tiempo.Stop();
            contador = 0;
            OperationResult resp = new CartaPorteSvc().getViajesParaTimbrar();
            if (resp.typeResult == ResultTypes.success)
            {
                lista = (List<Viaje>)resp.result;

                doc.timbrarCartaPortes(lista);
                //foreach (var item in lista)
                //{
                //    //MessageBox.Show("Timbrando " + item.NumGuiaId.ToString());
                //    var _x = item;
                //    if (doc.llenarEstructuraDocumento(_x))
                //    {
                //        //foreach (var item2 in _x.listDetalles)
                //        //{

                //        //}
                //        lvlTimbres.Items.Add("SI: " + item.NumGuiaId.ToString());
                //    }
                //    else
                //    {
                //        lvlTimbres.Items.Add("NO: " + item.NumGuiaId.ToString());
                //    }
                //}

                return true;
            }
            else
            {
                return false;
            }
        }
        CrearDocumento doc = new CrearDocumento();
        private bool iniciarSDK()
        {
            bool valor = false;
            doc = new CrearDocumento(ref valor);
            if (valor)
            {
                this.Enabled = valor;
                return valor;
            }
            else
            {
                return false;
            }
        }
    }
}
