﻿
using Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TimbradoAutomatico
{
    public static class GuardarServicio
    {
        internal static Declaraciones.tProduto ltProducto = new Declaraciones.tProduto();
        internal static int lidProducto = 0;
        public static bool guardarSDK(Zona destino, eAction action)
        {
            try
            {
                ltProducto.cCodigoProducto = destino.clave.ToString();
                ltProducto.cNombreProducto = destino.descripcion;
                ltProducto.cTextoExtra1 = destino.descripcion;
                ltProducto.cFechaAltaProducto = DateTime.Now.ToString("MM/dd/yyyy");
                //Campos para poder dar Alta Unidad de Medida
                //ltProducto.cCodigoUnidadBase = "";
                ltProducto.cControlExistencia = 0;
                ltProducto.cTipoProducto = 3;

                //MessageBox.Show(action.ToString());
                switch (action)
                {
                    case eAction.NUEVO:
                        if (hayError(Declaraciones.fAltaProducto(ref lidProducto, ref ltProducto)))
                        {
                            return true;
                        }
                        else
                        {
                            MessageBox.Show("Servicio agregado al sistema CONTPAQ I COMERCIAL",
                                "Alta del servicio.",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                            return false;
                        }
                    case eAction.EDITAR:
                        int lResul = Declaraciones.fActualizaProducto(destino.clave.ToString(), ref ltProducto);
                        if (lResul == 3)
                        {
                            return guardarSDK(destino, eAction.NUEVO);
                        }
                        if (hayError(lResul))
                        {
                            //MessageBox.Show(lResul.ToString());                            
                            return true;
                        }
                        else
                        {
                            MessageBox.Show("Servicio actualizado en el sistema CONTPAQ I COMERCIAL",
                                "Editar el servicio.",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                            return false;
                        }
                }

                return false;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }

        public static bool guardarSDKSinMensaje(Zona destino, eAction action)
        {
            try
            {
                ltProducto.cCodigoProducto = "RUTA" + destino.clave.ToString();
                ltProducto.cNombreProducto = destino.descripcion;
                ltProducto.cTextoExtra1 = destino.descripcion;
                //ltProducto.cFechaAltaProducto = DateTime.Now.ToString("MM/dd/yyyy");
                //Campos para poder dar Alta Unidad de Medida
                //ltProducto.cCodigoUnidadBase = "";
                ltProducto.cControlExistencia = 0;
                ltProducto.cTipoProducto = 3;

                //MessageBox.Show(action.ToString());
                switch (action)
                {
                    case eAction.NUEVO:
                        if (hayError(Declaraciones.fAltaProducto(ref lidProducto, ref ltProducto)))
                        {
                            return true;
                        }
                        else
                        {
                            Declaraciones.fEditaProducto();
                            Declaraciones.fSetDatoProducto("CCLAVESAT", "78101805");
                            Declaraciones.fGuardaProducto();
                            //MessageBox.Show("Servicio agregado al sistema CONTPAQ I COMERCIAL",
                            //    "Alta del servicio.",
                            //    MessageBoxButton.OK,
                            //    MessageBoxImage.Information);
                            return false;
                        }
                    case eAction.EDITAR:
                        int lResul = Declaraciones.fActualizaProducto(destino.clave.ToString(), ref ltProducto);
                        if (lResul == 3)
                        {
                            return guardarSDK(destino, eAction.NUEVO);
                        }
                        if (hayError(lResul))
                        {
                            //MessageBox.Show(lResul.ToString());                            
                            return true;
                        }
                        else
                        {
                            //MessageBox.Show("Servicio actualizado en el sistema CONTPAQ I COMERCIAL",
                            //    "Editar el servicio.",
                            //    MessageBoxButton.OK,
                            //    MessageBoxImage.Information);
                            return false;
                        }
                }

                return false;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;
            }
        }

        public static bool eliminarServicio(Zona destino)
        {
            if (hayError(Declaraciones.fBuscaProducto(destino.clave.ToString())))
            {
                return false;
            }
            else
            {
                if (hayError(Declaraciones.fEliminarProducto(destino.clave.ToString())))
                {
                    return true;
                }
                else
                {
                    MessageBox.Show("Servicio eliminado del sistema CONTPAQ I COMERCIAL",
                                "Editar el servicio.",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);

                    return false;
                }
            }
        }

        internal static bool hayError(int error)
        {
            bool resultado;

            if (error != 0)
            {
                Declaraciones.MuestraError(error);
                resultado = true;
            }
            else
            {
                resultado = false;
            }
            return resultado;
        }
    }
}
