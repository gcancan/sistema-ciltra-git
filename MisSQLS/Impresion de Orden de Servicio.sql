USE DBFleteraPrueba

SELECT * FROM dbo.CabOrdenServicio ORDER BY idOrdenSer DESC
SELECT * FROM dbo.DetRecepcionOS ORDER BY idOrdenTrabajo DESC
SELECT * FROM dbo.DetOrdenServicio ORDER BY idOrdenActividad DESC


/* sin orden de trabajo */
SELECT cab.idOrdenSer,cab.IdEmpresa,idUnidadTrans,idTipoOrden,cab.idPadreOrdSer,Kilometraje,
CAB.idTipOrdServ,TOS.NomTipOrdServ, TOS.bOrdenTrab,
CAB.idTipoServicio,TS.NomTipoServicio,
DET.idOrdenActividad, DET.idOrdenSer,
DET.idServicio,SER.idTipoServicio, SER.NomServicio,
DET.idActividad,ACT.NombreAct,
DET.idOrdenTrabajo, DET.Estatus, emp.RazonSocial,CAB.fechaCaptura,
pad.idTractor,ISNULL(pad.idTolva1,'') AS idTolva1, ISNULL(pad.idDolly,'') AS idDolly, isnull(pad.idTolva2,'') AS idTolva2,
OPE.NombreCompleto AS NomOperador, AUT.NombreCompleto AS NomAutoriza
FROM dbo.CabOrdenServicio CAB
INNER JOIN dbo.DetOrdenServicio DET ON cab.idOrdenSer = det.idOrdenSer
INNER JOIN dbo.CatTipoOrdServ TOS ON CAB.idTipOrdServ = TOS.idTipOrdServ
INNER JOIN dbo.CatTipoServicio TS ON CAB.idTipoServicio = TS.idTipoServicio
LEFT JOIN dbo.CatServicios SER ON DET.idServicio = SER.idServicio
LEFT JOIN dbo.CatActividades ACT ON DET.idActividad = ACT.idActividad
INNER JOIN dbo.CatEmpresas emp ON cab.IdEmpresa = emp.idEmpresa
INNER JOIN DBO.MASTERORDSERV PAD ON CAB.IDPADREORDSER = PAD.idPadreOrdSer
INNER JOIN dbo.CatPersonal OPE ON PAD.idChofer = OPE.idPersonal
INNER JOIN dbo.CatPersonal AUT ON CAB.idEmpleadoAutoriza = AUT.idPersonal
WHERE cab.idOrdenSer = 271



/* Con orden de trabajo */
SELECT cab.idOrdenSer,CAB.IdEmpresa,CAB.idUnidadTrans,CAB.idTipoOrden,CAB.idPadreOrdSer,CAB.Kilometraje,
CAB.idTipOrdServ,TOS.NomTipOrdServ, TOS.bOrdenTrab,
CAB.idTipoServicio,TS.NomTipoServicio,
DET.idOrdenActividad, DET.idOrdenSer,
DET.idServicio,SER.idTipoServicio, SER.NomServicio,
DET.idActividad,ACT.NombreAct,
DET.idOrdenTrabajo, DET.Estatus,
TRA.idOrdenTrabajo, TRA.NotaRecepcion,TRA.idDivision, DIV.NomDivision
FROM dbo.CabOrdenServicio CAB
INNER JOIN dbo.DetOrdenServicio DET ON cab.idOrdenSer = det.idOrdenSer
INNER JOIN dbo.CatTipoOrdServ TOS ON CAB.idTipOrdServ = TOS.idTipOrdServ
INNER JOIN dbo.CatTipoServicio TS ON CAB.idTipoServicio = TS.idTipoServicio
LEFT JOIN dbo.CatServicios SER ON DET.idServicio = SER.idServicio
LEFT JOIN dbo.CatActividades ACT ON DET.idActividad = ACT.idActividad
INNER JOIN dbo.DetRecepcionOS TRA ON cab.idOrdenSer = tra.idOrdenSer AND det.idOrdenTrabajo = tra.idOrdenTrabajo
INNER JOIN dbo.CatDivision DIV ON TRA.idFamilia = DIV.idDivision
WHERE cab.idOrdenSer = 273


