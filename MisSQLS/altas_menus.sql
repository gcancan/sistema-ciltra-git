--SELECT * FROM dbo.menusAplicacion 37, 39
USE DBFleteraPrueba
DECLARE @clave NVARCHAR(MAX) = 'MENU_SERVICIOS_SOLORDENSERV';
IF NOT EXISTS ( SELECT  *
                FROM    dbo.menusAplicacion
                WHERE   clave = @clave )
    BEGIN
        INSERT  INTO dbo.menusAplicacion
                ( idPadre ,
                  clave ,
                  nombre ,
                  descripcion
                )
        VALUES  ( 39  , -- idPadre - int
                  @clave , -- clave - nvarchar(40)
                  N'Solicitud Orden de Servicio' , -- nombre - nvarchar(max)
                  N'Menu para acceder a la solicitud de Orden de Servicio'  -- descripcion - nvarchar(max)
                );
    END;
    
IF NOT EXISTS ( SELECT  *
                FROM    dbo.privilegios
                WHERE   clave = @clave )
    INSERT  INTO dbo.privilegios
            ( clave ,
              descripcion
            )
    VALUES  ( @clave , -- clave - nvarchar(max)
              N'Privilegio para acceder a la solicitud de Orden de Servicio'  -- descripcion - nvarchar(max)
            );
IF NOT EXISTS ( SELECT  *
                FROM    dbo.privilegiosUsuario
                WHERE   idPrivilegio = ( SELECT idPrivilegio
                                         FROM   dbo.privilegios
                                         WHERE  clave = @clave
                                       )
                        AND idUsuario = ( SELECT    idUsuario
                                          FROM      dbo.usuariosSys
                                          WHERE     nombreUsuario = 'admin'
                                        ) )
    INSERT  INTO dbo.privilegiosUsuario
            ( idPrivilegio ,
              idUsuario
            )
    VALUES  ( ( SELECT  idPrivilegio
                FROM    dbo.privilegios
                WHERE   clave = @clave
              ) , -- idPrivilegio - int
              ( SELECT  idUsuario
                FROM    dbo.usuariosSys
                WHERE   nombreUsuario = 'admin'
              )  -- idUsuario - int
            );