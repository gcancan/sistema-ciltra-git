/*
Analisis Nuevos Ordenes de Servicio
*/

USE DBFleteraPrueba

sp_help RelServicioActividad

SELECT * FROM dbo.CatTipoOrdServ
SELECT * FROM dbo.CatServicios
SELECT * FROM dbo.CatActividades
SELECT * FROM dbo.CatTipoServicio

/*PARA RELACIONAR LOS SERVICIOS FIJOS CON LAS ACTIVIDADES */
SELECT * FROM dbo.RelServicioActividad rel
INNER JOIN dbo.CatServicios ser ON rel.idServicio = ser.idServicio  
INNER JOIN dbo.CatActividades act ON rel.idActividad = act.idActividad

sp_help RelTipOrdServ
SELECT * FROM RelTipOrdServ
/*

*/




/* PARA MANEJO DE SERVICIOS*/
SELECT * FROM dbo.MasterOrdServ

SELECT * FROM dbo.CabOrdenServicio
WHERE idOrdenSer = 240

SELECT * FROM dbo.DetOrdenServicio
WHERE idOrdenSer = 240

SELECT * FROM dbo.DetRecepcionOS
WHERE idOrdenSer = 240

SELECT * FROM dbo.DetOrdenActividadProductos
WHERE idOrdenSer = 240


SELECT * FROM dbo.CatUnidadTrans
SELECT * FROM dbo.CatPersonal

sp_help CabOrdenServicio
sp_help DetRecepcionOS
sp_help DetOrdenServicio

SELECT * FROM dbo.CatTipoOrdServ
SELECT * FROM dbo.RelTipOrdServ
TRUNCATE TABLE dbo.RelTipOrdServ


SELECT rel.idTipOrdServ,
rel.NomCatalogo,
rel.NombreId,
rel.IdRelacion
FROM dbo.RelTipOrdServ rel
WHERE rel.idTipOrdServ = 1

SELECT * FROM catservicios 
WHERE idservicio = 3

SELECT * FROM dbo.CatTipoOrdServ
SELECT * FROM dbo.CatTipoServicio
SELECT * FROM dbo.RelServicioActividad

SELECT * FROM dbo.CatServicios
SELECT * FROM dbo.CatTipoOrdServ
SELECT * FROM dbo.RelTipOrdServ
/*
INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (1,'CatServicios','idServicio',3)

INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (2,'CatTipoServicio','idTipoServicio',1)
INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (2,'CatTipoServicio','idTipoServicio',2)
INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (2,'CatServicios','idServicio',1)
INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (2,'CatServicios','idServicio',2)

INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (2,'CatActividades','idActividad',1)
INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (2,'CatActividades','idActividad',2)
INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (2,'CatActividades','idActividad',3)
INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (2,'CatActividades','idActividad',4)
INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (2,'CatActividades','idActividad',5)
INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (2,'CatActividades','idActividad',6)
INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (2,'CatActividades','idActividad',7)

INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (3,'CatTipoServicio','idTipoServicio',1)
INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (3,'CatTipoServicio','idTipoServicio',2)
INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (3,'CatServicios','idServicio',5)
INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (3,'CatServicios','idServicio',6)
INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (3,'CatServicios','idServicio',7)
INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (3,'CatServicios','idServicio',8)

INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (4,'CatServicios','idServicio',4)
INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (4,'CatTipoServicio','idTipoServicio',1)
INSERT INTO dbo.RelTipOrdServ(idTipOrdServ,NomCatalogo,  NombreId,IdRelacion) VALUES (4,'CatTipoServicio','idTipoServicio',2)

5-8
*/

sp_help CatTipoServicio
sp_help CatTipoOrdServ

SELECT * FROM dbo.CatTipoOrdServ


SELECT * FROM dbo.CatTipoServicio

SELECT * FROM dbo.RelTipOrdServ

SELECT * FROM dbo.CatDivision

SELECT * FROM dbo.CatTipoOrdServ
SELECT * FROM dbo.RelTipOrdServ
SELECT * FROM dbo.CatServicios WHERE idTipoServicio = 1
SELECT * FROM dbo.CatServicios WHERE idTipoServicio = 2

SELECT rel.idServicio,
ser.idTipoServicio,
ser.NomServicio,
ser.Costo,
act.NombreAct,
act.CostoManoObra,
act.DuracionHoras
FROM dbo.RelServicioActividad rel
INNER JOIN dbo.CatServicios ser ON rel.idServicio = ser.idServicio
INNER JOIN dbo.CatActividades act ON rel.idActividad = act.idActividad
WHERE rel.idServicio = 1

SELECT * FROM dbo.CatActividades
SELECT * FROM dbo.CatServicios
SELECT * FROM dbo.CatTipoServicio
SELECT * FROM dbo.RelServicioActividad
SELECT * FROM dbo.DetRecepcionOS

SELECT * FROM dbo.RelActividadProducto

SELECT * FROM dbo.CatTipoOrdServ

SELECT * FROM dbo.RelTipOrdServ WHERE idTipOrdServ = 2



sp_help CatTipoOrdServ

SELECT rel.idServicio,
ser.idTipoServicio,
ser.NomServicio,
ser.Costo,
act.NombreAct,
act.CostoManoObra,
act.DuracionHoras
FROM dbo.RelServicioActividad rel
INNER JOIN dbo.CatServicios ser ON rel.idServicio = ser.idServicio
INNER JOIN dbo.CatActividades act ON rel.idActividad = act.idActividad
WHERE rel.idServicio = 1


SELECT ser.idServicio,
ser.idTipoServicio,
ser.NomServicio,
ser.Descripcion,
ser.CadaKms,
ser.CadaTiempoDias,
ser.Estatus,
ser.Costo
FROM dbo.CatServicios ser
WHERE ser.idTipoServicio = 1

SELECT * FROM dbo.CatServicios
WHERE idTipoServicio = 1

SELECT * FROM dbo.RelTipOrdServ

SELECT div.idDivision,
div.NomDivision
FROM dbo.CatDivision div
WHERE div.idDivision > 0
ORDER BY idDivision

SELECT * FROM dbo.CatServicios

SELECT rel.idServicio,
ser.idTipoServicio,
ser.NomServicio,
ser.Costo,
act.NombreAct,
act.CostoManoObra,
act.DuracionHoras,
rel.idActividad,
fam.idDivision,
div.NomDivision
FROM dbo.RelServicioActividad rel
INNER JOIN dbo.CatServicios ser ON rel.idServicio = ser.idServicio
INNER JOIN dbo.CatActividades act ON rel.idActividad = act.idActividad
INNER JOIN dbo.CatFamilia fam ON act.idFamilia = fam.idFamilia
INNER JOIN dbo.CatDivision div ON fam.idDivision = div.idDivision 
WHERE rel.idServicio = 1

SELECT * FROM dbo.CatServicios
SELECT * FROM dbo.CatActividades



SELECT act.idActividad,
act.NombreAct,
act.CostoManoObra,
act.idFamilia,
fam.NomFamilia,
fam.idDivision,
div.NomDivision,
act.DuracionHoras
FROM dbo.CatActividades Act
INNER JOIN dbo.CatFamilia fam ON act.idFamilia = fam.idFamilia
INNER JOIN dbo.CatDivision div ON fam.idDivision = div.idDivision

SELECT * FROM dbo.CatTipoOrdServ



SELECT rel.idTipOrdServ, rel.NomCatalogo, rel.NombreId, rel.IdRelacion FROM dbo.RelTipOrdServ rel  WHERE rel.idTipOrdServ = 2

SELECT * FROM dbo.CatTipoOrdServ WHERE idTipOrdServ = 3
SELECT * FROM dbo.RelTipOrdServ WHERE idTipOrdServ = 3

SELECT * FROM dbo.CatActividades
SELECT * FROM dbo.CatServicios
SELECT * FROM dbo.CatTipoServicio



SELECT rel.idTipOrdServ, rel.NomCatalogo, rel.NombreId, rel.IdRelacion FROM dbo.RelTipOrdServ rel  WHERE rel.idTipOrdServ = 3
SELECT ser.idServicio, ser.idTipoServicio, ser.NomServicio, ser.Descripcion, ser.CadaKms, ser.CadaTiempoDias, 
ser.Estatus, ser.Costo FROM dbo.CatServicios ser  where  ser.idTipoServicio = 1




SELECT * FROM dbo.CatTipoOrdServ
SELECT * FROM dbo.RelTipOrdServ

SELECT * FROM dbo.CatServicios WHERE idServicio = 4

SELECT * FROM dbo.RelServicioActividad
SELECT * FROM dbo.CatActividades


SELECT * FROM dbo.RelServicioActividad
WHERE idServicio = 4

SELECT * FROM dbo.CatActividades

SP_HELP  CatActividades



SELECT rel.idTipOrdServ, rel.NomCatalogo, rel.NombreId, rel.IdRelacion FROM dbo.RelTipOrdServ rel  WHERE rel.idTipOrdServ = 3
SELECT * FROM dbo.CatTipoServicio


SELECT * FROM dbo.CatServicios
sp_help CatServicios

sp_help catdivision


SELECT * FROM dbo.CatDivision
SELECT * FROM dbo.CatServicios

/*
UPDATE dbo.CatServicios SET idDivision = 8
WHERE idservicio IN (5,6,7,8)
*/

SELECT ts.idTipoServicio,ts.NomTipoServicio FROM dbo.CatTipoServicio ts

SELECT ser.idServicio,
ser.NomServicio,
tser.NomTipoServicio,
div.NomDivision,
ser.Estatus
FROM dbo.CatServicios ser
INNER JOIN dbo.CatTipoServicio tser ON ser.idTipoServicio = tser.idTipoServicio
INNER JOIN dbo.CatDivision div ON ser.idDivision = div.idDivision



select uni.idUnidadTrans, uni.descripcionUni, uni.idTipoUnidad, uni.idMarca, uni.idEmpresa, 
uni.idSucursal, uni.idAlmacen, uni.Estatus, isnull(uni.UltOT,0) as UltOT  
FROM CatUnidadTrans uni where uni.idUnidadTrans = 'TV4'

SELECT * FROM dbo.CatUnidadTrans

SELECT uni.idUnidadTrans, uni.descripcionUni, uni.idTipoUnidad, uni.idMarca, uni.idEmpresa, 
uni.idSucursal, uni.idAlmacen, uni.Estatus, isnull(uni.UltOT,0) as UltOT  
FROM CatUnidadTrans uni where uni.idUnidadTrans like '%TV4%'

SELECT ser.idServicio, ser.idTipoServicio, ser.NomServicio, ser.Descripcion, ser.CadaKms, ser.CadaTiempoDias, 
ser.Estatus, ser.Costo, ser.idDivision, div.NomDivisionFROM dbo.CatServicios ser 
INNER JOIN CatDivision DIV ON SER.IDDIVISION = DIV.IDDIVISION  where  ser.idTipoServicio = 1


SELECT * FROM dbo.CatTipoOrdServ  WHERE idTipOrdServ = 2
SELECT * FROM dbo.RelTipOrdServ WHERE idTipOrdServ = 2 ORDER BY NomCatalogo

SELECT * FROM dbo.CatActividades WHERE idactividad IN (1,2,3,4,5,6,7)
SELECT * FROM CatServicios WHERE idServicio IN (1,2)
SELECT * FROM dbo.CatTipoServicio WHERE idTipoServicio IN (1,2)

SELECT * FROM dbo.RelServicioActividad


